<?php
	
class PB_adminpage_cinema_manage_list_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$cinema_list_ = pb_cinema_list(array(
			'keyword' => $keyword_,
			'orderby' => "order by cinema_name asc",
			'limit' => array($offset_, $per_page_),
		));

		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_cinema_list(array(
				'keyword' => $keyword_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $cinema_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){
		return array(
			"cinema_name" => "상영관명",
			"addr" => "주소",
			"button_area" => "",

		);
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){
			case "cinema_name" :
				ob_start();

				?>
				<a href="<?=pb_adminpage_url("cinema-manage/edit/".$item_->cinema_id)?>"> <?= $item_->cinema_name ?></a>
				<div class="site-url"><?=$item_->domain.$item_->path?></div>

				<?php

				return ob_get_clean();

				return $item_->cinema_name;
			break;
			case "addr" :

				ob_start();
				?>

				<div><?=$item_->addr?></div>
				<div><?=$item_->addr_dtl?></div>

				<?php

				return ob_get_clean();

			case "button_area" :

				ob_start();
			?>
				
				<a href="<?=pb_cinema_site_url($item_)?>" class="btn btn-sm btn-default" target="_blank">사이트방문</a>

			<?php


				return ob_get_clean();
			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 상영관이 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-cinema-manage-table', function(){
	global $pb_board;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>
<div class="form-margin"></div>
<div class="search-frame">
	<div class="row">
		<div class="col-xs-3 col-sm-8">
			<a href="<?=pb_adminpage_url('cinema-manage/add')?>" class="btn btn-default btn-sm">상영관등록</a>
		</div>
		<div class="col-xs-9 col-sm-4">
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">검색</button>
				</span>
			</div>
		</div>
	</div>
</div>

<?php }); ?>
<div class="adminpage-cinema-manage-list-frame">
	
	<form id="pb-adminpage-cinema-manage-table-form" method="get" action="<?=pb_adminpage_url('cinema-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_cinema_manage_list_table("pb-adminpage-cinema-manage-table", "pb-adminpage-cinema-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>