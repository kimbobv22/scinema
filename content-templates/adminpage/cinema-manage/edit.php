<?php

if(is_subdomain_install()){
	$newdomain_ = '.' . preg_replace( '|^www\.|', '', get_network()->domain ).get_network()->path;
} else {
	$newdomain_ = get_network()->domain.get_network()->path;
}

global $pb_cinema;
$is_new_ = false;

if(!isset($pb_cinema)){
	$pb_cinema = (object)array(
		'cinema_id' => null,
		'cinema_name' => null,
		'cinema_type' => "00001",
		'cinema_status' => "00001",
		'cinema_desc' => null,
		'seattable' => null,
		'addr' => null,
		'addr_dtl' => null,
		'addr_postcode' => null,
		'addr_lat' => "",
		'addr_lng' => "",
		'area_id' => null,
		'fee_info' => null,
		'biz_no' => null,
		'phone1' => null,
		'phone2' => null,
		'faxnum' => null,
		'domain' => null,
		'admin_login' => null,
		'ticket_ref_type' => null,
		'xticket_ref_code' => null,
		'naver_synckey' => null,
		'logo_image_url' => null,
		'search_desc' => null,
		'replace_location_html_yn' => null,
	);

	$is_new_ = true;
}else{

	switch_to_blog( $pb_cinema->cinema_id );
	$pb_cinema->search_desc = get_bloginfo('description');
	restore_current_blog();
	
}

?>
<script type="text/javascript">
var _pb_cinema_manage_localvar = {
};
</script>
<div class="adminpage-cinema-manage-edit-frame"><form method="POST" id="pb-cinema-manage-edit-form" data-add-new='<?=$is_new_ ? "Y" : "N" ?>'>

	<input type="hidden" name="cinema_id" value="<?=$pb_cinema->cinema_id?>">
	<div class="row">
		<div class="col col-xs-12 text-right">
			<a href="<?=pb_adminpage_url('cinema-manage')?>" class="btn btn-default">목록으로</a>
		</div>
	</div>
	
	<div class="form-margin-xxs"></div>

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-cinema-manage-edit-form-cinema_name">상영관명</label>
						<input type="text" name="cinema_name" class="form-control" required placeholder="상영관명 입력" data-error="상영관명을 입력하세요" id="pb-cinema-manage-edit-form-cinema_name" value="<?=$pb_cinema->cinema_name?>">
						<div class="help-block with-errors"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<label for="pb-cinema-manage-edit-form-domain">상영관도메인</label>
					<div class="form-group row has-feedback">

					<?php if($is_new_){ ?>
						<div class="col-xs-12">
						<?php if(is_subdomain_install()){ ?>
						<div class="input-group">
							<input type="text" name="domain" class="form-control" required placeholder="도메인" data-error="도메인을 입력하세요" data-remote-error="사용중인 도메인입니다." id="pb-cinema-manage-edit-form-domain" data-remote="<?=pb_ajax_url('cinema-check-domain', array(
								'key' => 'domain',
							))?>" data-alphanum="Y" data-alphanum-error="영문숫자로만 입력하세요">
							<span class="input-group-addon"><?=$newdomain_?></span>
						</div>

						<?php }else{ ?>
							<div class="input-group">
								<span class="input-group-addon"><?=$newdomain_?></span>
								<input type="text" name="domain" class="form-control" required placeholder="도메인" data-error="도메인을 입력하세요" data-remote-error="사용중인 도메인입니다." id="pb-cinema-manage-edit-form-domain" data-remote="<?=pb_ajax_url('cinema-check-domain', array(
									'key' => 'domain',
								))?>"  data-alphanum="Y" data-alphanum-error="영문숫자로만 입력하세요">
							</div>
						<?php } ?>
							<div class="help-block with-errors"></div>
							<div class="clearfix"></div>
						</div>
						

					<?php }else{ ?>

						<div class="col-xs-9">
							<p class="form-control-static"><?=$pb_cinema->domain.$pb_cinema->path?></p>
							<div class="clearfix"></div>
						</div>
						
						<div class="col-xs-3">
							<a class="btn btn-default btn-block" href="http://<?=pb_append_url($pb_cinema->domain.$pb_cinema->path,'wp-admin')?>">관리</a>	
							<div class="clearfix"></div>
						</div>
						
					<?php } ?>
						
						
						
						
					</div>

					
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-cinema-manage-edit-form-cinema_type">상영관분류</label>
						<select class="form-control" required data-error="상영관분류를 선택하세요" id="pb-cinema-manage-edit-form-cinema_type" name="cinema_type">
							<?= pb_gcode_make_options("C0001", $pb_cinema->cinema_type) ?>
						</select>
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-cinema-manage-edit-form-cinema_status">등록상태</label>
						<select class="form-control" required data-error="등록상태를 선택하세요" id="pb-cinema-manage-edit-form-cinema_status" name="cinema_status">
							<?= pb_gcode_make_options("C0003", $pb_cinema->cinema_status) ?>
						</select>

						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-cinema-manage-edit-form-area_id">상영관지역</label>
						<select class="form-control" required data-error="지역을 선택하세요" id="pb-cinema-manage-edit-form-area_id" name="area_id">
							<option value="">-지역선택-</option>
							<?= pb_gcode_make_options("C0005", $pb_cinema->area_id) ?>
						</select>

						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group has-feedback">
						<label for="pb-cinema-manage-edit-form-admin_email">상영관관리자</label>
						

						<div class="input-group">
							<input type="text" name="admin_login" class="form-control" required placeholder="아이디 또는 이메일 입력" data-error="관리자정보를 입력하세요"  id="pb-cinema-manage-edit-form-admin_email" value="<?=$pb_cinema->admin_login?>">
							<span class="input-group-addon">@scinema.org</span>
						</div>

						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback">
						
						<div class="input-group">
							<span class="input-group-addon">암호</span>
							<input type="text" name="admin_pass" class="form-control" placeholder="관리자암호 입력" data-error="관리자암호를 입력하세요"  id="pb-cinema-manage-edit-form-admin_pass">
							
						</div>
						<small class="help-block">*입력하지 않으면 변경되지 않습니다.</small>
						<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
						<div class="help-block with-errors"></div>
					</div>
				
				</div>
			</div>

			<div class="form-group">
				<label for="">상영관로고</label>
				<input type="hidden" name="logo_image_url" id="pb-cinema-manage-edit-form-logo_image_url" data-default-value="<?=$pb_cinema->logo_image_url?>">
				<small class="help-block">*상영시간표에 노출되는 로고입니다.</small>

				<div class="help-block with-errors"></div>
			</div>

			

			<div class="row">
				<div class="col-xs-12 col-sm-4">
					
						<label for="pb-cinema-manage-edit-form-addr_postcode">우편번호</label>
						<div class="form-group">
							<div class="input-group">
								<input type="text" name="addr_postcode" class="form-control readonly" required placeholder="우편번호 검색" data-error="우편번호를 검색하세요" id="pb-cinema-manage-edit-form-addr_postcode" value="<?=$pb_cinema->addr_postcode?>" data-validate="true" >
								<span class="input-group-btn">
									<button class="btn btn-default" type="button" onclick="_pb_cinema_manage_edit_find_postcode();">검색</button>
								</span>
							</div>
							<div class="help-block with-errors"></div>
							<div class="clearfix"></div>
						</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-xs-12 col-sm-8">
					<div class="form-group">
						<label for="pb-cinema-manage-edit-form-addr">주소</label>
						<input type="text" name="addr" class="form-control readonly" required placeholder="주소 검색" data-error="주소를 검색해주세요"  id="pb-cinema-manage-edit-form-addr" value="<?=$pb_cinema->addr?>" data-validate="true">
						<div class="form-margin-xxs"></div>
						<input type="text" name="addr_dtl" class="form-control" required placeholder="상세주소 검색" data-error="상세주소를 검색해주세요" value="<?=$pb_cinema->addr_dtl?>" id="pb-cinema-manage-edit-form-addr_dtl">

						<div class="form-margin-xxs"></div>
						<div class="daum-map-frame <?=strlen($pb_cinema->addr_lat) ? "" : "hidden"?>">
							<div class="daum-map" id="pb-cinema-maps"></div>
						</div>
						
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
					<input type="hidden" name="addr_lat" value="<?=$pb_cinema->addr_lat?>">
					<input type="hidden" name="addr_lng" value="<?=$pb_cinema->addr_lng?>">
				</div>
			</div>

			<hr>
			<div class="checkbox ">
				<label><input type="checkbox" name="replace_location_html_yn" id="pb-cinema-manage-edit-form-replace_location_html_yn" value="Y" <?=checked($pb_cinema->replace_location_html_yn, 'Y')?> > 오시는길 직접작성
				</label>
			</div>
			<small class="help-block">*검색한 주소로 자동표기를 원하지 않을 경우 체크</small>
		
			<div data-replace-location-html-group class="<?=$pb_cinema->replace_location_html_yn !== "Y" ? "hidden" : "" ?>">
				<div class="form-group">
					<label for="pb-cinema-manage-edit-form-replace_location_html">오시는길</label>

					<div class="text-left" style="margin-bottom: -30px; position: relative; z-index: 9; width: 100px;">
						<a href="#" class="btn btn-default btn-sm" id="pb-cinema-manage-edit-form-add-image-btn-for-location">이미지추가</a>
					</div>

					<?php
					
					wp_editor(stripslashes($pb_cinema->replace_location_html),"pb-cinema-manage-edit-form-replace_location_html", array(
						'media_buttons' => false,
						'quicktags' => true,
						'wpautop' => false,
						'textarea_name' => 'replace_location_html',
					));

					?>
					<div class="help-block with-errors"></div>
				</div>
			</div>
		

			<hr>

			<div class="form-group">
				<label for="pb-cinema-manage-edit-form-phone1_1">연락처1</label>
				<input type="text" name="phone1" class="form-control" required placeholder="연락처 입력" data-error="연락처를 입력해주세요" value="<?=$pb_cinema->phone1?>">
				<div class="help-block with-errors"></div>
			</div>
			<div class="form-group">
				<label for="pb-cinema-manage-edit-form-phone1_1">연락처2</label>
				<input type="text" name="phone2" class="form-control" placeholder="연락처 입력" value="<?=$pb_cinema->phone2?>">
			</div>

			<hr>

			<div class="form-group">
				<label for="pb-cinema-manage-edit-form-cinema_desc">영화관소개</label>

				<div class="text-left" style="margin-bottom: -30px; position: relative; z-index: 9; width: 100px;">
					<a href="#" class="btn btn-default btn-sm" id="pb-cinema-manage-edit-form-add-image-btn">이미지추가</a>
				</div>

				<?php

				
				wp_editor(stripslashes($pb_cinema->cinema_desc),"pb-cinema-manage-edit-form-cinema_desc", array(
					'media_buttons' => false,
					'quicktags' => true,
					'wpautop' => false,
					'textarea_name' => 'cinema_desc',
				));

				?>
				<div class="help-block with-errors"></div>
			</div>

			<div class="form-group hidden">
				<label for="">좌석배치도</label>
				<input type="hidden" name="seattable" id="pb-cinema-manage-edit-form-seattable" value="<?=htmlentities(stripslashes($pb_cinema->seattable))?>">
				<div class="help-block with-errors"></div>
			</div>

			<hr>

			<div class="form-group">
				<label for="pb-cinema-manage-edit-form-phone1_1">네이버검색 SYNC키</label>
				<input type="text" name="naver_synckey" class="form-control" placeholder="싱크키 입력" value="<?=$pb_cinema->naver_synckey?>">
			</div>
			<div class="form-group">
				<label for="pb-cinema-manage-edit-form-phone1_1">사이트한줄설명</label>
				<input type="text" name="search_desc" class="form-control" placeholder="설명 입력" value="<?=$pb_cinema->search_desc?>">
				<small class="help-block">*검색 등록 시, 사이트제목 하단에 표기되는 설명입니다.</small>
			</div>

			<hr>

			<div class="form-group">

				<label for="pb-cinema-manage-edit-form-ticket_ref_type">티켓연동방식</label>
				<select class="form-control" required data-error="연동방식을 선택하세요" id="pb-cinema-manage-edit-form-ticket_ref_type" name="ticket_ref_type">
					<option value="">-연동방식선택-</option>
					<?php 

					$tmp_ticket_ref_type_list_ = pb_gcode_dtl_list("C0007");
					$ticket_ref_type_list_ = array();

					foreach($tmp_ticket_ref_type_list_ as $row_data_){
						$ticket_ref_type_list_[$row_data_->code_did] = $row_data_->code_dnm;
					}

					foreach($ticket_ref_type_list_ as $code_did_ => $code_dnm_){ ?>

						<option value="<?=$code_did_?>" <?=selected($code_did_, $pb_cinema->ticket_ref_type)?> ><?=$code_dnm_?></option>

					<?php }
				?>
					</select>
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>
			</div>

			<div class="form-group">
				<label for="pb-cinema-manage-edit-form-xticket_ref_code">티켓연동코드</label>
				<input type="text" name="xticket_ref_code" class="form-control" placeholder="연동코드 입력" value="<?=$pb_cinema->xticket_ref_code?>">
			</div>

		</div>
	</div>
	<div class="button-area text-right">
		<?php if($is_new_){ ?>
		<button type="submit" class="btn btn-primary">등록하기</button>
		<?php }else{  ?>
		<button type="submit" class="btn btn-primary">수정하기</button>
		<?php } ?>
	</div>

	

</form></div>