<?php
	
	$cinema_list_ = pb_cinema_list();
?>
<div class="adminpage-movie-screen-bulk-write-frame"><form id="pb-movie-screen-bulk-write-form">
	
	<div class="form-group">
	
		<label for="pb-movie-screen-edit-form-cinema_id">상영영화관</label>
		<select name="cinema_id" class="form-control " required="" data-required-error="영화관을 선택하세요" id="pb-movie-screen-edit-form-cinema_id">
			
			<option value="">-영화관 선택-</option>
			<?php foreach($cinema_list_ as $row_data_){ ?>
			<option value="<?=$row_data_->cinema_id?>"><?=$row_data_->cinema_name?></option>
			<?php } ?>
		
		</select>
	</div>

	<table class="table " id="pb-movie-screen-bulk-write-form-table">
		
		<thead>
			<tr>
				<th>
					개봉영화선택
				</th>
				<th>
					상영시작일자
				</th>
				<th>
					상영종료일자
				</th>
				<th>
					XTicket연동코드
				</th>
			</tr>
		</thead>
		<tbody>
			<?php for($row_index_ = 0; $row_index_< 15; ++$row_index_){ ?>
			
			<tr data-row-index="<?=$row_index_?>">
				<td>
					<select class="form-control input-sm" name="" data-open-id-select data-column-name="open_id"></select>
				</td>
				<td>
					<div class="input-group date input-group-sm" data-format="YYYY-MM-DD" data-locale="ko" data-datepicker>
						<input type="text" value="" placeholder="상영시작일자 입력" class="form-control " data-column-name="screen_srt_date">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</td>
				<td>
					<div class="input-group date input-group-sm" data-format="YYYY-MM-DD" data-locale="ko" data-datepicker>
						<input type="text" value="" placeholder="상영종료일자 입력" class="form-control " data-column-name="screen_end_date">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</td>
				<td>
					<select class="form-control input-sm" data-column-name="xticket_ref_code"></select>
				</td>
			</tr>

			<?php } ?>
		</tbody>

	</table>

	<div class="text-right">
		<button type="submit" class="btn btn-primary">일괄등록하기</button>
	</div>
		
</form></div>