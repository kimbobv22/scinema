<?php


?>
<div class="adminpage-push-msg-manage-edit-frame"><form method="POST" id="pb-push-msg-manage-edit-form" data-redirect-url='<?=pb_adminpage_url("push-msg-new-movie-manage")?>'>

	<input type="hidden" name="push_type" value="00003">

	<div class="row">

		<div class="col-xs-6">
			<h4 class="sidemenupage-subtitle">앱신작알림 작성</h4>
		</div>
		<div class="col-xs-6 text-right">
			<a href="<?=pb_adminpage_url('push-msg-new-movie-manage')?>" class="btn btn-default">목록으로</a>
		</div>

	</div>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<?php if(pb_cinema_current_is_head_office()){ ?>
				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<div class="form-group">
							<label for="pb-push-msg-manage-edit-form-cinema_id">대상영화관</label>
							<select class="form-control" name="cinema_id" id="pb-push-msg-manage-edit-form-scinema_id">
								<option value="" >-영화관선택-</option>
								<?php pb_cinema_make_options(array(
									'include_head_office' => false,
									'cinema_type' => array(PB_CINEMA_CTG_NOR, PB_CINEMA_CTG_PNDM),
									'orderby' => ' ORDER BY cinema_name asc '
								)); ?>
							</select>
						</div>
					</div>
					<div class="col-sm-8 col-xs-12">
						<div class="form-group">
							<label for="pb-push-msg-manage-edit-form-msg_title">제목</label>
							<input type="text" name="msg_title" class="form-control" required placeholder="제목 입력" data-error="제목을 입력하세요" id="pb-push-msg-manage-edit-form-msg_title" value="">
							<div class="help-block with-errors"></div>
						</div>
					</div>
				</div>
			
				
			<?php }else{ ?>
				<input type="hidden" name="cinema_id" value="<?=pb_current_cinema_id()?>">

				<div class="form-group">
					<label for="pb-push-msg-manage-edit-form-msg_title">제목</label>
					<input type="text" name="msg_title" class="form-control" required placeholder="제목 입력" data-error="제목을 입력하세요" id="pb-push-msg-manage-edit-form-msg_title" value="">
					<div class="help-block with-errors"></div>
				</div>

			<?php } ?>


			

			<div class="form-group">
				<label for="pb-push-msg-manage-edit-form-msg_short_txt">한줄 설명</label>
				<input type="text" name="msg_short_txt" class="form-control" required placeholder="한줄 설명 입력" data-error="한줄 설명을 입력하세요" id="pb-push-msg-manage-edit-form-msg_short_txt" value="">
				<div class="help-block with-errors"></div>
				<small class="help-block">*푸시알림 제목하단에 표기됩니다.</small>
			</div>

			<div class="form-group">
				<label for="pb-push-msg-manage-edit-form-dvc_type">디바이스</label>
				<select class="form-control" name="dvc_type" id="pb-push-msg-manage-edit-form-dvc_type">
					<option value="-1" >전체</option>
					<?= pb_gcode_make_options("DV001", "") ?>
				</select>
			</div>

		



			<input type="hidden" name="default_screen_data" value="">

			<div class="form-group">
				<input type="hidden" name="movie_screen_id" value="">
				<label for="">개봉영화선택</label>
				<div class="input-group">
					<input type="text" class="form-control readonly" placeholder="검색하여 선택" name="screen_id_name" value="" required data-error="개봉영화를 선택하세요">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" onclick="pb_push_msg_new_movie_find_screen_id();">검색하기</button>
					  </span>
				</div>

				<div class="with-errors help-block"></div>
				<div class="clearfix"></div>
				
			</div>

			<div class="pb-movie-screen-item-wrap hidden" id="pb-movie-screen-item-preview">
				<div class="pb-movie-screen-item">
					<div class="col poster-image-col">
						<div class="poster-image light">
							<img src="{$image_url}" class="image" data-image="image_url">
						</div>
					</div>
					<div class="col info-col">
						<div class="movie-name" data-column="movie_name">{$movie_name}</div>
						<div class="subinfo-list">
							<div class="subinfo-item" data-column="publisher_name">{$publisher_name}</div>
							<div class="subinfo-item" data-column="genre">{$genre}</div>
							<div class="subinfo-item" data-column="level_name">{$level_name}</div>
							<div class="subinfo-item"><span data-column="open_date_ymd">{$open_date_ymd}</span> 개봉</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="button-area text-right">

		<button type="submit" class="btn btn-primary">작성하기</button>
		
	</div>

	

</form></div>



<div class="pb-find-movie-screen-id-modal modal" id="pb-find-movie-screen-id-modal"><form method="get" id="pb-find-movie-screen-id-modal-form">
	<input type="hidden" name="cinema_id" value="">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">개봉영화 검색</h4>
			</div>
			<div class="modal-body">
				<div class="search-condition">
					<div class="input-group input-group-lg">
						<input type="search" class="form-control" placeholder="검색어 입력" name="keyword">
						<span class="input-group-btn">
						<button class="btn btn-default" type="submit">검색</button>
					  </span>
					</div>

				

				</div>	

				<?php
					$movie_screen_id_table_ = new PB_push_msg_new_movie_screen_id_table("pb-movie-screen-id-table", "pb-movie-screen-id-table");
					$movie_screen_id_table_->set_ajax(true);
					echo $movie_screen_id_table_->html();
				?>
				

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</form></div>
