<?php

global $pb_push_msg;
$is_new_ = false;

if(!isset($pb_push_msg)){
	$pb_push_msg = (object)array(
		'ID' => null,

		'push_type' => null,
		'push_type_name' => null,

		'dvc_type' => null,
		'dvc_type_name' => null,

		'cinema_id' => null,
		'cinema_name' => null,

		'movie_open_id' => null,
		'movie_open_title' => null,

		'feature_image_url' => null,
		'msg_short_txt' => null,
		'msg_title' => null,

		'total_ios' => null,
		'total_and' => null,

		'success_ios' => null,
		'success_and' => null,

		'fail_ios' => null,
		'fail_and' => null,

	);

	$is_new_ = true;
}

?>
<div class="adminpage-push-msg-manage-edit-frame"><form method="POST" id="pb-push-msg-manage-edit-form"  data-redirect-url='<?=pb_adminpage_url("push-msg-notice-manage")?>'>

	<input type="hidden" name="ID" value="<?=$pb_push_msg->ID?>">

	<div class="row">
		<div class="col col-xs-6">
			<h4 class="sidemenupage-subtitle">메세지 상세</h4>
		</div>

		<div class="col-xs-6 text-right">
			<a href="<?=pb_adminpage_url('push-msg-notice-manage')?>" class="btn btn-default">목록으로</a>
		</div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-body">


			<div class="row">

				<div class="form-group col-sm-6 col-xs-12">
					<label>제목</label>
					<div class="wrap">
						<?=$pb_push_msg->msg_title?>
					</div>
				</div>

				<div class="form-group col-sm-6 col-xs-12">
					<label>디바이스종류</label>
					<div class="wrap">
						<?=$pb_push_msg->dvc_type_name?>
					</div>
				</div>
				
			</div>
			
			<div class="form-group">
				<label>지점</label>
				<div class="wrap">
					<?=$pb_push_msg->cinema_name?>
				</div>
			</div>

			<div class="form-group">
				<label>대표이미지</label>
				<div class="wrap">
					<?php if(strlen($pb_push_msg->feature_image_url_str)){ ?>
						<img src="<?=$pb_push_msg->feature_image_url_str?>" style="max-width: 300px">
					<?php }else{?>
						없음
					<?php } ?>
				</div>
			</div>

			<div class="form-group">
				<label>푸시내용</label>

				<div class="wrap">
					<?=$pb_push_msg->msg_html?>
				</div>
			</div>

			<hr>

			<label>총횟수</label>
			<table class="table table-bordered table-sm">
				<thead>
					<tr class="active">
						<td>전체</td>
						<td>iOS</td>
						<td>Android</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?=number_format($pb_push_msg->total_ios + $pb_push_msg->total_and)?></td>
						<td><?=number_format($pb_push_msg->total_ios)?></td>
						<td><?=number_format($pb_push_msg->total_and)?></td>
					</tr>
				</tbody>
			</table>

			<label>성공횟수</label>
			<table class="table table-bordered table-sm">
				<thead>
					<tr class="active">
						<td>전체</td>
						<td>iOS</td>
						<td>Android</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?=number_format($pb_push_msg->success_ios + $pb_push_msg->success_and)?></td>
						<td><?=number_format($pb_push_msg->success_ios)?></td>
						<td><?=number_format($pb_push_msg->success_and)?></td>
					</tr>
				</tbody>
			</table>

			<label>실패횟수</label>
			<table class="table table-bordered table-sm">
				<thead>
					<tr class="active">
						<td>전체</td>
						<td>iOS</td>
						<td>Android</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?=number_format($pb_push_msg->fail_ios + $pb_push_msg->fail_and)?></td>
						<td><?=number_format($pb_push_msg->fail_ios)?></td>
						<td><?=number_format($pb_push_msg->fail_and)?></td>
					</tr>
				</tbody>
			</table>
			
		</div>
	</div>
	<div class="button-area text-right">
		<button type="button" class="btn btn-black" onclick="_pb_push_msg_manage_delete(<?=$pb_push_msg->ID?>);">삭제하기</button>
		<a href="javascript:pb_push_msg_manage_resend(<?=$pb_push_msg->ID?>)" class="btn btn-default">알림재발송</a>
		<a href="<?=pb_adminpage_url('push-msg-notice-manage')?>" class="btn btn-default">목록으로</a>
	</div>

	

</form></div>