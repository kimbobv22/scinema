<?php


$is_new_ = false;


?>
<div class="adminpage-push-msg-manage-edit-frame"><form method="POST" id="pb-push-msg-manage-edit-form" data-redirect-url='<?=pb_adminpage_url("push-msg-notice-manage")?>'>

	<input type="hidden" name="push_type" value="00001">

	<div class="row">

		<div class="col-xs-6">
			<h4 class="sidemenupage-subtitle">앱공지사항 작성</h4>
		</div>

		<div class="col-xs-6 text-right">
			<a href="<?=pb_adminpage_url('push-msg-notice-manage')?>" class="btn btn-default">목록으로</a>
		</div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<?php if(pb_cinema_current_is_head_office()){ ?>
				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<div class="form-group">
							<label for="pb-push-msg-manage-edit-form-cinema_id">대상영화관</label>
							<select class="form-control" name="cinema_id" id="pb-push-msg-manage-edit-form-scinema_id">
								<option value="-1" >전체</option>
								<?php pb_cinema_make_options(array(
									'include_head_office' => false,
									'cinema_type' => array(PB_CINEMA_CTG_NOR, PB_CINEMA_CTG_PNDM),
									'orderby' => ' ORDER BY cinema_name asc '
								)); ?>
							</select>
						</div>
					</div>
					<div class="col-sm-8 col-xs-12">
						<div class="form-group">
							<label for="pb-push-msg-manage-edit-form-msg_title">제목</label>
							<input type="text" name="msg_title" class="form-control" required placeholder="제목 입력" data-error="제목을 입력하세요" id="pb-push-msg-manage-edit-form-msg_title" value="">
							<div class="help-block with-errors"></div>
						</div>
					</div>
				</div>
			
				
			<?php }else{ ?>
				<input type="hidden" name="cinema_id" value="<?=pb_current_cinema_id()?>">

				<div class="form-group">
					<label for="pb-push-msg-manage-edit-form-msg_title">제목</label>
					<input type="text" name="msg_title" class="form-control" required placeholder="제목 입력" data-error="제목을 입력하세요" id="pb-push-msg-manage-edit-form-msg_title" value="">
					<div class="help-block with-errors"></div>
				</div>

			<?php } ?>


			

			<div class="form-group">
				<label for="pb-push-msg-manage-edit-form-msg_short_txt">한줄 설명</label>
				<input type="text" name="msg_short_txt" class="form-control" required placeholder="한줄 설명 입력" data-error="한줄 설명을 입력하세요" id="pb-push-msg-manage-edit-form-msg_short_txt" value="">
				<div class="help-block with-errors"></div>
				<small class="help-block">*푸시알림 제목하단에 표기됩니다.</small>
			</div>

			<div class="form-group">
				<label for="pb-push-msg-manage-edit-form-dvc_type">디바이스</label>
				<select class="form-control" name="dvc_type" id="pb-push-msg-manage-edit-form-dvc_type">
					<option value="-1" >전체</option>
					<?= pb_gcode_make_options("DV001", "") ?>
				</select>
			</div>

			

				

			<div class="form-group">
				<label for="">대표이미지</label>
				<input type="hidden" name="feature_image_url" id="pb-push-msg-manage-edit-form-feature_image_url" data-default-value="">

				<div class="help-block with-errors"></div>
				<small class="help-block">*공지사항 상단에 표기됩니다.</small>
			</div>
		

			<div class="form-group">
				<label for="pb-push-msg-manage-edit-form-msg_html">내용작성</label>

				

				<div class="popup-html-wrap"><div class="wrap">
					<?php wp_editor("","msg_html", array(
						'media_buttons' => false,
						'quicktags' => false,
						'wpautop' => false,
					)) ?>
				</div></div>

					
					
			</div>
			
		</div>
	</div>
	<div class="button-area text-right">

		<button type="submit" class="btn btn-primary">작성하기</button>
		
	</div>

	

</form></div>
