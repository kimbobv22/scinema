<?php 
class PB_adminpage_push_dvc_manage_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		$cinema_id_ = null;
		if(!pb_cinema_current_is_head_office()){
			$cinema_id_ = pb_current_cinema_id();
		}else{
			$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : "";
		}
		
		$msg_list_ = pb_push_msg_list(array(
			'keyword' => $keyword_,
			'cinema_id' => $cinema_id_,
			'push_type' => '00001',
			'limit' => array($offset_, $per_page_),
			'orderby' => 'ORDER BY REG_DATE DESC '
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_push_msg_list(array(
				'keyword' => $keyword_,
				'cinema_id' => $cinema_id_,
				'push_type' => '00001',
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $msg_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){

		$columns_ = array(
			
			
			// "push_type" => "알림 종류",
			"cinema_id" => "영화관",

			"msg_title" => "제목",
			
			"dvc_type" => "발송디바이스",

			"total_count" => "총 횟수",
			"success_count" => "성공 횟수",
			"fail_count" => "실패 횟수",

			"reg_date_ymdhi" => "등록일시",

			"btn_area" => "",
		);

		if(!pb_cinema_current_is_head_office()){
			unset($columns_['cinema_id']);
		}

		return $columns_;
	}

	function column_value($item_, $column_name_){

		$row_index_ = $this->current_row();

		switch($column_name_){

			case "cinema_id" :

				if($item_->cinema_id == PB_CINEMA_HEAD_OFFICE_ID){
					return "본사";
				}else if(strlen($item_->cinema_id === "-1")){
					return "공통";
				}else{ 
					return $item_->cinema_name;
				}

				break;
		

			case "push_type" :
				return $item_->push_type_name;

			case "dvc_type" :
				return $item_->dvc_type_name;

			case "msg_title" :
				ob_start();

				?>
				<a href="<?=pb_adminpage_url('push-msg-notice-manage/detail/'.$item_->ID)?>"><?=$item_->msg_title?></a>
				<small class="help-block visible-xs"><?=$item_->reg_date_ymdhi?></small>
				<?php

				return ob_get_clean();

			case "total_count" :
				return $item_->total_ios + $item_->total_and;
			case "success_count" :
				return $item_->success_ios + $item_->success_and;
			case "fail_count" :
				return $item_->fail_ios + $item_->fail_and;

			case "reg_date_ymdhi" :
				return $item_->reg_date_ymdhi;

			case "btn_area" :
				ob_start();

				?>

				<input type="button" class="btn btn-default btn-xs" name="resend_btn" onclick="pb_push_msg_manage_resend(<?=$item_->ID?>)" value="재전송">

				<?php

				return ob_get_clean();
			default : 
				return '';
			break;
		}
	}

	function _column_classes($column_name_){
		switch($column_name_){

			case "total_count" :
			case "success_count" :
			case "fail_count" :
			case "dvc_type" : 	
			case "reg_date_ymdhi" : 	
				return "hidden-xs text-center";

			case "reg_date_ymdhi" : 
				return "text-center";
			default : 
				return '';
			break;
		}
	}	

	function column_header_classes($column_name_){
		return $this->_column_classes($column_name_);
	}
	function column_body_classes($column_name_, $items_){
		return $this->_column_classes($column_name_);
	}

	function norowdata(){
		return "검색된 공지사항이 없습니다.";	
	}
	
}


$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null; 

?>

<div class="adminpage-push-dvc-manage-frame">
	<div class="search-frame">
		<div class="row">
			<div class="col-xs-3 col-sm-8">
				<a href="<?=pb_adminpage_url('push-msg-notice-manage/add')?>" class="btn btn-default btn-sm">작성하기</a>
			</div>
			<div class="col-xs-9 col-sm-4">
				<div class="input-group input-group-sm">
					<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">검색</button>
					</span>
				</div>
			</div>
		</div>
	</div>
	<hr>
	
	<form id="pb-adminpage-push-dvc-manage-table-form" method="get" action="<?=pb_adminpage_url('push-msg-notice-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_push_dvc_manage_table("pb-adminpage-push-msg-manage-table", "pb-adminpage-push-msg-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>