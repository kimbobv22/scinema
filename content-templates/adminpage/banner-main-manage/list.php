<?php

class PB_adminpage_banner_main_manage_table extends PBListTable{
	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$banner_main_list_ = pb_banner_main_list(array(
			'keyword' => $keyword_,
			'limit' => array($offset_, $per_page_),
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_banner_main_list(array(
				'keyword' => $keyword_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,

            'items' => $banner_main_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

    function columns(){
		return array(
            "banner_main_title" => "메인배너명",
            "srt_date" => "게시일자",
			"hide_yn" => "숨김처리",
			"banner_kind" => "메인배너타입",
			"button_area" => "",

		);
	}

	function column_value($item_, $column_name_){
		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){

			case "banner_main_title" :
				return $item_->banner_title;
			break;
            
            case "srt_date" :
                return $item_->srt_date.'<br/>~'.$item_->end_date;	

            case "hide_yn" :
                ob_start()
                ?>
                <input type="checkbox" data-banner-hide-yn="<?=$item_->ID?>" value="Y" <?=checked($item_->hide_yn, "Y")?> >
                <?php
                return ob_get_clean();

			case "button_area" :
				ob_start();
				?>
				<a href="<?=pb_adminpage_url("banner-main-manage/edit/".$item_->ID)?>" class="btn btn-sm btn-default">수정하기</a>
				<?php
				return ob_get_clean();
			
			case "banner_kind" :
				return $item_->banner_kind;
                		
			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 메인배너가 없습니다.";	
	}
}

add_filter('pb-listtable-before-pagenav-pb-adminpage-banner-main-manage-table', function(){
	global $pb_board;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>

<div class="form-margin"></div>
<div class="search-frame">
    <div class="row">
		<div class="col-xs-3 col-sm-8">
			<?php if(pb_is_admin()){ ?>
				<a href="<?=pb_adminpage_url("banner-main-manage/add")?>" class="btn btn-default btn-sm">배너추가</a>
			<?php } ?>
		</div>
		<div class="col-xs-9 col-sm-4">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">검색</button>
				</span>
			</div>
		</div>
	</div>
</div>
<?php }); ?>

<div class="adminpage-banner-main-manage-frame">
	<h4 class="adminpage-subtitle">메인배너관리</h4>
	<form id="pb-adminpage-banner-main-manage-table-form" method="get" action="<?=pb_adminpage_url('banner-main-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_banner_main_manage_table("pb-adminpage-banner-main-manage-table", "pb-adminpage-banner-main-manage-table");
			echo $list_table_->html();
		?>
	</form>
</div>