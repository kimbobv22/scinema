<?php
global $pb_banner_main;
$is_new_ = false;

if(!isset($pb_banner_main)){
	$pb_banner_main = (object)array(
		'ID' => null,
		'banner_title' => null,
		'banner_html' => null,
        'banner_kind' => null,
        
        'banner_left_color' => null,
        'banner_right_color' => null,

        'image_url' => null,
        'link_url' => null,

        'link_target_yn' => 'N',
		'hide_yn' => 'N',

		'srt_date' => null,
		'end_date' => null,
	);
	$is_new_ = true;
}
?>
<div class="adminpage-banner-main-manage-edit-frame">
    <form method="POST" id="pb-banner-main-manage-edit-form" data-add-new='<?=$is_new_ ? "Y" : "N" ?>'>
    	<input type="hidden" name="ID" value="<?=$pb_banner_main->ID?>">
       	<div class="row">
            <?php if($is_new_){ ?>
                <h4 class="col col-xs-6 sidemenupage-subtitle">배너등록</h4>
            <?php }else{ ?>
                <h4 class="col col-xs-6 sidemenupage-subtitle">배너수정</h4>
            <?php } ?>
            <div class="col col-xs-6 text-right">
                <a href="<?=pb_adminpage_url('banner-main-manage')?>" class="btn btn-default">목록으로</a>
            </div>
	    </div>
       	<div class="panel panel-default">
	    	<div class="panel-body">
                <div class="row">
                    <div class="col col-sm-8 col-xs-12">
                        <div class="form-group">
                            <label for="pb-banner-main-manage-edit-form-banner_title">메인배너명</label>	
                            <input type="text" name="banner_title" class="form-control" required placeholder="배너명 입력" data-error="배너명을 입력하세요" id="pb-banner-main-manage-edit-form-banner_title" value="<?=$pb_banner_main->banner_title?>">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="pb-banner-main-manage-edit-form-banner_kind">메인배너타입</label>	
                            <select class="form-control" name="banner_kind" id="pb-banner-main-manage-edit-form-banner_kind">
						    	<option value="main" <?=selected($pb_banner_main->banner_kind, "main")?> >메인</option>
                                <option value="event" <?=selected($pb_banner_main->banner_kind, "event")?> >이벤트</option>
						    </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>메인배너 왼쪽컬러</label>
                            <input class="color color-picker form-control" value="<?=$pb_banner_main->banner_left_color?>" name="banner_left_color" data-color-item />
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>메인배너 오른쪽컬러</label>
                            <input class="color color-picker form-control" value="<?=$pb_banner_main->banner_right_color?>" name="banner_right_color" data-color-item />
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
	            	<label for="pb-banner-main-manage-edit-form-banner_image_url">메인배너 이미지</label>
		            <input type="hidden" id="pb-banner-main-manage-edit-form-banner_image_url" data-error="배너이미지를 선택하세요" data-column-name="banner_image_url" data-image-url="<?=$pb_banner_main->image_url?>">
		            <div class="help-block with-errors"></div>
	            </div>
                <div class="form-group">
                    <label for="pb-banner-main-manage-edit-form-link_url">메인배너 링크url</label>	
                    <div class="input-group">
                        <input type="text" name="link_url" class="form-control" required placeholder="링크url" data-error="링크 url 입력하세요" id="pb-banner-main-manage-edit-form-link_url" value="<?=$pb_banner_main->link_url?>">
                        <span class="input-group-addon checkbox">
                            <label>
                                <input type="checkbox" id="pb-banner-main-manage-edit-form-link_target_yn" aria-label="..." name="link_target_yn" value="Y" <?=checked ($pb_banner_main->link_target_yn, "Y")?>> 새창으로 열기 
                            </label>
                        </span>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
    			<div class="form-group">
				    <label>메인배너 내용</label>
					<div class="popup-html-wrap">
					    <?php wp_editor(stripslashes($pb_banner_main->banner_html),"pb-banner-manage-edit-form-banner_html", array(
                            'media_buttons' => false,
                            'quicktags' => true,
                            'wpautop' => false,
                            'textarea_name' => 'banner_html',
					    )) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="form-group">                      
                            <label for="pb-banner-main-manage-edit-form-srt_date">게시일자</label>
                            <div class="row">
                                <div class="col-xs-6 col-phone">
                                    <div class="input-with-icon sm">
                                        <input type='text' class="form-control" id="pb-banner-main-manage-edit-form-srt_date" name="srt_date" placeholder="시작일자" value="<?=$pb_banner_main->srt_date?>" />
                                        <i class="icon glyphicon glyphicon-calendar" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="input-with-icon sm">
                                        <input type='text' class="form-control" id="pb-banner-main-manage-edit-form-end_date" name="end_date" placeholder="종료일자" value="<?=$pb_banner_main->end_date?>" />
                                        <i class="icon glyphicon glyphicon-calendar" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="pb-banner-main-manage-edit-form-hide_yn">숨김처리</label>                           
                            <div class="checkbox">
                                <label><input type="checkbox" name="hide_yn" id="pb-banner-main-manage-edit-form-hide_yn" value="Y" <?=checked($pb_banner_main->hide_yn, "Y")?>> 팝업을 숨깁니다.
                            </label>
                            </div>
                        </div>
                    </div>
                </div>
			</div>		
		</div>
        <div class="button-area text-right">
            <?php if(!$is_new_){ ?>
                <button type="button" class="btn btn-black" onclick="_pb_banner_main_manage_delete();">배너삭제</button>
            <?php } ?>
            <?php if($is_new_){ ?>
                <button type="submit" class="btn btn-primary">등록하기</button>
            <?php }else{ ?>
                <button type="submit" class="btn btn-primary">수정하기</button>
            <?php } ?>		
        </div>
    </form>
</div>