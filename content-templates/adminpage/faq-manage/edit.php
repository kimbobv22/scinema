<?php 

class PB_adminpage_faq_manage_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_cinema;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$faq_list_ = pb_faq_list(array(
			'cinema_id' => $pb_cinema->cinema_id,
			'keyword' => $keyword_,
			'limit' => array($offset_, $per_page_),
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_faq_list(array(
				'cinema_id' => $pb_cinema->cinema_id,
				'keyword' => $keyword_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $faq_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){
		return array(
			"faq_content" => "등록FAQ",
			"button_area" => "",

		);
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){

			case "faq_content" :

			$faq_desc_ = $item_->faq_desc;

			if(mb_strlen($faq_desc_) > 50){
				$faq_desc_ = mb_substr($item_->faq_desc, 0, 50)."...";
			}

				ob_start();

				?>

				<div class="faq-title">Q. <?=$item_->faq_title?></div>
				<div class="faq-desc">A. <?=$faq_desc_?></div>

				<?php

				return ob_get_clean();
			break;
		
			case "button_area" :

				ob_start();

				$item_->faq_title = stripslashes($item_->faq_title);
				$item_->faq_desc = stripslashes($item_->faq_desc)

				?>

				<input type="hidden" name="_faq_data" value="<?=htmlentities(json_encode($item_))?>" data-faq-data-id="<?=$item_->ID?>">
				
				<a href="javascript:_pb_faq_manage_edit(<?=$item_->ID?>);" class="btn btn-sm btn-default">수정</a>				
				<a href="javascript:_pb_faq_manage_delete(<?=$item_->ID?>);" class="btn btn-sm btn-default">삭제</a>

				<?php

				return ob_get_clean();
			
			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 FAQ가 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-faq-manage-table', function(){
	global $pb_board;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>

<div class="form-margin"></div>
<div class="search-frame">
	<div class="row">
		<div class="col-xs-3 col-sm-8">
			<a class="btn btn-default btn-sm" href="javascript:_pb_faq_manage_add();">FAQ등록</a>
		</div>
		<div class="col-xs-9 col-sm-4"></div>
	</div>
</div>

<?php });
	
	global $pb_cinema;



 ?>

<div class="adminpage-faq-manage-frame">
	<div class="row">
		<div class="col col-xs-6">
			<?php if(pb_cinema_current_is_head_office()){ ?>
			<h4 class="adminpage-subtitle"><?=$pb_cinema->cinema_name?> FAQ내역</h4>
			<?php }else{ ?>
			<h4 class="adminpage-subtitle">FAQ내역</h4>
			<?php } ?>
		</div>

		<div class="col col-xs-6 text-right">
			<a href="<?=pb_adminpage_url('faq-manage')?>" class="btn btn-default">목록으로</a>
		</div>
	</div>
	
	
	<form id="pb-adminpage-faq-manage-table-form" method="get" action="<?=pb_adminpage_url('faq-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_faq_manage_table("pb-adminpage-faq-manage-table", "pb-adminpage-faq-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>

<div class="pb-faq-manage-edit-modal modal" id="pb-faq-manage-edit-popup"><form id="pb-faq-manage-edit-popup-form" method="post">
	<input type="hidden" name="cinema_id" value="<?=$pb_cinema->cinema_id?>">
	<input type="hidden" name="faq_id" value="">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="text-edit">FAQ수정</span>
					<span class="text-add">FAQ추가</span>
				</h4>
			</div>
			<div class="modal-body">
				
				<div class="form-group">
					<label for="pb-faq-manage-edit-form-faq_title">질문</label>
					<input type="text" name="faq_title" class="form-control" required placeholder="FAQ명 입력" data-error="FAQ명을 입력해주세요" value="" id="pb-faq-manage-edit-form-faq_title">
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					<label for="pb-faq-manage-edit-form-faq_desc">답변</label>
					<textarea name="faq_desc" class="form-control" required placeholder="답변 입력" data-error="답변을 입력해주세요" id="pb-faq-manage-edit-form-faq_desc"></textarea>
					<div class="help-block with-errors"></div>
				</div>

			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-default">
					<span class="text-edit">수정하기</span>
					<span class="text-add">추가하기</span>
				</button>
			</div>
		</div>
	</div>
</form></div>