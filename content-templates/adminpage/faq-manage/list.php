<?php 


class PB_adminpage_faq_mst_manage_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$faq_list_ = pb_faq_mst_list(array(
			'keyword' => $keyword_,
			'limit' => array($offset_, $per_page_),
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_faq_mst_list(array(
				'keyword' => $keyword_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $faq_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){
		return array(
			"cinema_name" => "영화관",
			"faq_cnt" => "등록FAQ수",
			"button_area" => "",

		);
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){

			case "cinema_name" :
				return $item_->cinema_name;
			break;

			case "faq_cnt" :
				return strlen($item_->faq_cnt) ? $item_->faq_cnt : 0;
			break;
		
			case "button_area" :

				ob_start();

				?>

				<a href="<?=pb_adminpage_url("faq-manage/edit/".$item_->cinema_id)?>" class="btn btn-sm btn-default">수정하기</a>

				<?php

				return ob_get_clean();
			
			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 영화관이 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-faq-mst-manage-table', function(){
	global $pb_board;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>

<div class="form-margin"></div>
<div class="search-frame">
	<div class="row">
		
		<div class="col-xs-9 col-sm-4">
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">검색</button>
				</span>
			</div>
		</div>
	</div>
</div>

<?php }); ?>

<div class="adminpage-faq-manage-frame">
	<h4 class="adminpage-subtitle">영화관내역</h4>
	<form id="pb-adminpage-faq-manage-table-form" method="get" action="<?=pb_adminpage_url('faq-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_faq_mst_manage_table("pb-adminpage-faq-mst-manage-table", "pb-adminpage-faq-mst-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>