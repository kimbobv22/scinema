<?php

	$tmp_cinema_list_ = pb_cinema_list(array(
		'orderby' => "order by cinema_name asc"
	));
	
	$is_head_office_ = pb_cinema_current_is_head_office();

	if(!$is_head_office_){
		$cinema_id_ = pb_current_cinema_id();
	}else{
		$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
		if(!strlen($cinema_id_)){
			$cinema_id_ = $tmp_cinema_list_[0]->cinema_id;
		}	
	}

	$std_yyyymm_ = isset($_GET["std_yyyymm"]) ? $_GET["std_yyyymm"] : date("Y-m");
	$std_yyyymm_num_ = (int)date("Ym", strtotime($std_yyyymm_));

	$srt_datetime_ = strtotime($std_yyyymm_."-01");

	$str_srt_yyyymmdd_ = date("Y-m-d", $srt_datetime_);
	$srt_yyyymmdd_ = date("Y-m-d", $srt_datetime_);
	$end_yyyymmdd_ = date("Y-m-t", strtotime($srt_yyyymmdd_));

	$srt_day_num_ = date('w', $srt_datetime_);

	if($srt_day_num_ != 3){
		$temp_yyyymmdd_ = date_create($srt_yyyymmdd_);
		$diff_ = $srt_day_num_ - 3;
		date_sub($temp_yyyymmdd_, date_interval_create_from_date_string($diff_.' days'));
		$srt_yyyymmdd_ = $temp_yyyymmdd_->format('Y-m-d');
		$srt_datetime_ = strtotime($srt_yyyymmdd_);
	}
	
?>
<div class="adminpage-movie-screen-status-frame">
	
	<form id="pb-adminpage-movie-screen-status-form" method="get" action="<?=pb_adminpage_url('movie-screen-status')?>">
		<div class="search-frame">
			<div class="row">
				<?php if($is_head_office_){ ?>
				<div class="col-xs-12 col-sm-4">
					<label>영화관</label>
					<select class="form-control input-sm" name="cinema_id" id="pb-movie-screen-search-form-cinema-id">

						<?php foreach($tmp_cinema_list_ as $row_data_){ ?>
						<option value="<?=$row_data_->cinema_id?>" <?=selected($row_data_->cinema_id, $cinema_id_)?> ><?=$row_data_->cinema_name?></option>
						<?php } ?>

					</select>
					<div class="form-margin-xs visible-xs"></div>
				</div>
				<?php }else{ ?>
					<div class="col-xs-12 col-sm-4">
						<input type="hidden" name="cinema_id" value="<?=$cinema_id_?>">
					</div>
				<?php } ?>
				<div class="col-xs-8 col-sm-4">
					<label>기준월</label>
					<div class='input-group date input-group-sm' id='screen-std-date'>
						<input type='text' class="form-control" name="std_yyyymm" value="<?=$std_yyyymm_?>" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 text-right">
					<label>&nbsp;</label><br/>
					<button type="submit" class="btn btn-sm btn-default">검색하기</button>
				</div>
			</div>
		</div>
	</form>

	<hr class="div-line">

	<div class="table-responsive">
		<table class="table table-bordered pb-movie-screen-status-table">
			<thead>
				<tr class="active">
					<th class="text-center">
						상영종영작<br/>
						수
					</th>
					<th class="text-center">
						상영시작작<br/>
						목
					</th>
					<th class="text-center">
						상영계속작<br/>
						금
					</th>
				</tr>
			</thead>

			<tbody>
				<?php
					while(true){
						?> <tr> <?php

						$over_mm_ = false;

						$week_target_date_ = date_create($srt_yyyymmdd_);
						$week_srt_diff_ = (int)$week_target_date_->format('w');
						$week_end_diff_ = 6-$week_srt_diff_;

						$week_srt_yyyymmdd_ = date_create($srt_yyyymmdd_);
						$week_end_yyyymmdd_ = date_create($srt_yyyymmdd_);

						date_sub($week_srt_yyyymmdd_, date_interval_create_from_date_string($week_srt_diff_.' days'));
						date_add($week_end_yyyymmdd_, date_interval_create_from_date_string($week_end_diff_.' days'));

						$week_srt_yyyymmdd_ = $week_srt_yyyymmdd_->format("Y-m-d");
						$week_end_yyyymmdd_ = $week_end_yyyymmdd_->format("Y-m-d");

						for($col_index_=0;$col_index_<3; ++$col_index_){
							
							$target_date_ = date_create($week_target_date_->format("Y-m-d"));
							
							date_add($target_date_, date_interval_create_from_date_string($col_index_.' days'));

							$target_mm_ = $target_date_->format('m');
							$target_yyyymm_ = (int)$target_date_->format('Ym');


							$mm_str_ = $target_date_->format('d')." 일";
							if($target_yyyymm_ !== $std_yyyymm_num_){
								$mm_str_ = $target_mm_."/".$mm_str_;
							}

							$query_conditions_ = array();

							if($col_index_ == 0){
								$query_conditions_ = array(
									'cinema_id' => $cinema_id_,
									'for_movie_screen_status_ended' => array($week_srt_yyyymmdd_,$week_end_yyyymmdd_),
								);
							}else if($col_index_ == 1){
								$query_conditions_ = array(
									'cinema_id' => $cinema_id_,
									'for_movie_screen_status_started' => array($week_srt_yyyymmdd_,$week_end_yyyymmdd_),
								);
							}else if($col_index_ == 2){
								$query_conditions_ = array(
									'cinema_id' => $cinema_id_,
									'for_movie_screen_status_screening' => array($week_srt_yyyymmdd_,$week_end_yyyymmdd_),
								);
							}

							$movie_list_ = pb_movie_screen_list($query_conditions_);


							?>

							<td class="status-<?=$col_index_?>">
								<div class="mm-title"><?=$mm_str_?></div>
								<ul class="movie-list">
									<?php foreach($movie_list_ as $row_data_){ ?>
										<li>
											<?=$row_data_->movie_name?>
										 | <small><?=$row_data_->publisher_name?></small>
										 <br/><small><?=$row_data_->screen_srt_date_ymd?>~
										 <?php if(strlen($row_data_->screen_end_date_ymd)){ ?>
											<?=$row_data_->screen_end_date_ymd?>
										 <?php } ?>
										 </small>

										</li>
									<?php } ?>
								</ul>
							</td>

							<?php

							$over_mm_ = ((int)$target_yyyymm_) > ((int)$std_yyyymm_num_);
						}
					?> </tr> <?php

					if($over_mm_) break;

					$target_date_ = date_create($srt_yyyymmdd_);
					date_add($target_date_, date_interval_create_from_date_string('7 days'));
					$srt_yyyymmdd_ = $target_date_->format("Y-m-d");
				} ?>
			</tbody>

		</table>
	</div>
</div>