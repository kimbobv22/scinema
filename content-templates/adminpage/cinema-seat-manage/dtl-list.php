<?php 

class PB_adminpage_cinema_seat_manage_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_cinema;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$cinema_seat_list = pb_cinema_seat_list(array(
			'cinema_id' => $pb_cinema->cinema_id,
			'keyword' => $keyword_,
			'limit' => array($offset_, $per_page_),
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_cinema_seat_list(array(
				'cinema_id' => $pb_cinema->cinema_id,
				'keyword' => $keyword_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $cinema_seat_list,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){
		return array(
			"seat_content" => "배치도명",
			"status" => "상태",
			"button_area" => "",

		);
	}

	function column_value($item_, $column_name_){

		global $pb_cinema;
		$row_index_ = $this->current_row();

		switch($column_name_){

			case "seat_content" :

				ob_start();

				?>

				<div class="seat-name"><?=$item_->seat_name?></div>
				<small class="seat-desc help-block"><?=$item_->seat_desc?></small>

				<?php

				return ob_get_clean();
			break;

			case "status" :

				return $item_->status_name;
			break;
		
			case "button_area" :

				ob_start();

				?>
				
				<?php if(pb_cinema_current_is_head_office()){ ?>
					<a href="<?=pb_make_url(pb_adminpage_url("cinema-seat-manage/dtl/".$pb_cinema->cinema_id), array(
					'seat_id' => $item_->seat_id
					))?>" class="btn btn-sm btn-default">수정</a>
				<?php }else{ ?>
					<a href="<?=pb_make_url(pb_adminpage_url("cinema-seat-manage"), array(
					'seat_id' => $item_->seat_id
					))?>" class="btn btn-sm btn-default">수정</a>
				<?php } ?>
								
				<a href="javascript:_pb_cinema_seat_manage_delete(<?=$item_->ID?>);" class="btn btn-sm btn-default">삭제</a>

				<?php

				return ob_get_clean();
			
			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 좌석배치도가 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-cinema-seat-manage-table', function(){
	global $pb_cinema;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>

<div class="form-margin"></div>
<div class="search-frame">
	<div class="row">
		<div class="col-xs-3 col-sm-8">

			<?php if(pb_cinema_current_is_head_office()){ ?>
				<a class="btn btn-default btn-sm" href="<?=pb_make_url(pb_adminpage_url("cinema-seat-manage/dtl/".$pb_cinema->cinema_id), array(
				'new' => "Y"
				))?>">좌석배치도등록</a>

			<?php }else{ ?>
				<a class="btn btn-default btn-sm" href="<?=pb_make_url(pb_adminpage_url("cinema-seat-manage/"), array(
				'new' => "Y"
				))?>">좌석배치도등록</a>
			<?php } ?>
		</div>
		<div class="col-xs-9 col-sm-4"></div>
	</div>
</div>

<?php });
	
	global $pb_cinema;
?>

<div class="adminpage-cinema-seat-manage-frame">

	<div class="row">
		<div class="col col-xs-6">
			<?php if(pb_cinema_current_is_head_office()){ ?>
			<h4 class="adminpage-subtitle"><?=$pb_cinema->cinema_name?> 좌석배치도내역</h4>
			<?php }else{ ?>
			<h4 class="adminpage-subtitle">좌석배치도내역</h4>
			<?php } ?>
		</div>

		<div class="col col-xs-6 text-right">
			<a href="<?=pb_adminpage_url('cinema-seat-manage')?>" class="btn btn-default">목록으로</a>
		</div>
	</div>
	
	<form id="pb-adminpage-cinema-seat-manage-table-form" method="get" action="<?=pb_adminpage_url('cinema-seat-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_cinema_seat_manage_table("pb-adminpage-cinema-seat-manage-table", "pb-adminpage-cinema-seat-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>