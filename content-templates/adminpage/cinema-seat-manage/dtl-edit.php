<?php 
	
	global $pb_cinema, $pb_cinema_seat;

	$seat_data_ = $pb_cinema_seat->seat_data;
	$seat_data_string_ = "";

	if(empty($pb_cinema_seat->seat_data)){
		$seat_data_ = array(
			'screen' => array(
				'margin' => array(
					'left' => 2,
					'right' => 0,
				)
			),
			'schema' => array(
				'row' => 4,
				'column' => 7,
			),
			'seattable' => array(),
		);


		$label_array_ = array("A", "B", "C", "D");

		for($row_index_=0; $row_index_<4; ++$row_index_){
			$seat_data_['seattable'][] = array(
				array(
					'seat_id' => "",
					'seat_name' => $label_array_[$row_index_],
					'seat_type' => "00003",
				),
				array(
					'seat_id' => "",
					'seat_name' => "",
					'seat_type' => "00005",
				),
				array(
					'seat_id' => "",
					'seat_name' => "",
					'seat_type' => "00001",
				),
				array(
					'seat_id' => "",
					'seat_name' => "",
					'seat_type' => "00001",
				),
				array(
					'seat_id' => "",
					'seat_name' => "",
					'seat_type' => "00001",
				),
				array(
					'seat_id' => "",
					'seat_name' => "",
					'seat_type' => "00001",
				),
				array(
					'seat_id' => "",
					'seat_name' => "",
					'seat_type' => "00001",
				),
			);
		}

		$seat_data_string_ = json_encode($seat_data_);
	}else{

		$seat_data_string_ = json_encode($pb_cinema_seat->seat_data);
		// $seat_data_ = json_decode($seat_data_string_, true);

	}

?>
<script type="text/javascript">
var _pb_cinema_seattable_default_data = <?=$seat_data_string_?>;
</script>
<div class="page-cinema-seat-manage-frame">

	<div class="seattable-form-frame"><form class="pb-seattable-form" id="pb-seattable-form">

		<input type="hidden" name="seat_id" value="<?=$pb_cinema_seat->seat_id?>">
		<input type="hidden" name="cinema_id" value="<?=$pb_cinema->cinema_id?>">

		<div class="row">
			<div class="col col-xs-6">
				<h4>배치도정보</h4>
			</div>

			<div class="col col-xs-6 text-right">
				<a href="<?=pb_adminpage_url('cinema-seat-manage/dtl/' . $pb_cinema->cinema_id)?>" class="btn btn-default">목록으로</a>
			</div>
		</div>

		<div class="row">
			
			<div class="col-xs-12 col-sm-6">
				<div class="form-group" >
					<label class="" for="pb-cinema-seat-manage-form-seat_name">배치도명</label>
					<input type="text" class="form-control" id="pb-cinema-seat-manage-form-seat_name" placeholder="배치도명 입력" name="seat_name" required data-error="배치도명을 입력하세요" value="<?=$pb_cinema_seat->seat_name?>">
					<div class="help-block with-errors"></div>	
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="form-group" >
					<label class="" for="pb-cinema-seat-manage-form-status">상태</label>
					<select class="form-control" name="status" id="pb-cinema-seat-manage-form-status" required data-error="상태를 선택하세요">
						<option value="">-상태선택-</option>
						<?= pb_gcode_make_options('CS001', $pb_cinema_seat->status)?>
					</select>
					<div class="help-block with-errors"></div>	
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<div class="form-group" >
			<label class="" for="pb-cinema-seat-manage-form-seat_desc">배치도설명</label>
			<input type="text" class="form-control" id="pb-cinema-seat-manage-form-seat_desc" placeholder="배치도설명 입력" name="seat_desc" required data-error="배치도설명을 입력하세요" value="<?=$pb_cinema_seat->seat_desc?>">
			<div class="help-block with-errors"></div>	
			<div class="clearfix"></div>
		</div>

		<div class="form-group" >
			<label class="" for="pb-cinema-seat-manage-form-sort_num">노출순서</label>
			<input type="text" class="form-control" id="pb-cinema-seat-manage-form-sort_num" placeholder="노출순서 입력" name="sort_num" data-error="노출순서를 입력하세요" value="<?=$pb_cinema_seat->sort_num?>">
			<div class="help-block with-errors"></div>	
			<div class="clearfix"></div>
		</div>

		<hr>
	

		<label>배치도정보</label>
		<div class="row">
			<div class="col-xs-6 col-sm-6">
				<div class="input-group ">
					<span class="input-group-addon" >행</span>
					<input type="number" class="form-control" name="seattable_row" value="<?=$seat_data_['schema']['row']?>" data-seattable-schema-field required data-error="배치도 행을 입력하세요">
					
				</div>
				<div class="form-margin-xs"></div>
			</div>
			<div class="col-xs-6 col-sm-6">
				<div class="input-group ">
					<span class="input-group-addon" >열</span>
					<input type="number" class="form-control" name="seattable_column" value="<?=$seat_data_['schema']['column']?>" data-seattable-schema-field required data-error="배치도 열을 입력하세요">
					
				</div>
				<div class="form-margin-xs"></div>
			</div>
		</div>

		<label class="" for="">스크린여백</label>
			
		<div class="row">
			<div class="col-xs-6 col-sm-6">
				<div class="input-group ">
					<span class="input-group-addon" >좌</span>
					<input type="number" class="form-control" name="seattable_screen_margin_left" value="<?=$seat_data_['screen']['margin']['left']?>" data-seattable-screen-margin-field data-number-error="정수로만 입력하세요">
					
				</div>
				<div class="form-margin-xs"></div>
			</div>
			<div class="col-xs-6 col-sm-6">
				<div class="input-group ">
					<span class="input-group-addon" >우</span>
					<input type="number" class="form-control" name="seattable_screen_margin_right" value="<?=$seat_data_['screen']['margin']['right']?>" data-seattable-screen-margin-field data-number-error="정수로만 입력하세요">
				</div>
				<div class="form-margin-xs"></div>
			</div>
		</div>

		<div class="form-margin"></div>

		<div class="row">
			
			<div class="pb-seattable" id="pb-seattable-manager" data-default-value="<?=$seat_data_string_?>"><div class="seattable-wrap">
				<div class="screen-frame"><div class="screen">
					<span class="text">SCREEN</span>
				</div></div>

				<div class="seattable-row-list"></div>
			</div></div>
		</div>
		<small class="help-block">* 각 좌석마다 명칭을 입력할 수 있습니다.</small>

		<hr>

		<div class="checkbox ">
			<label><input type="checkbox" name="replace_image_yn" id="pb-cinema-seat-manage-form-replace_image_yn" value="Y" <?=checked($pb_cinema_seat->replace_image_yn, 'Y')?> > 이미지로 좌석배치도 대체
			</label>
		</div>
	
		<div data-replace-image-group class="<?=$pb_cinema_seat->replace_image_yn !== "Y" ? "hidden" : "" ?>">
			<input type="hidden" name="logo_image_url" id="pb-cinema-seat-manage-form-replace_image_url" data-default-value="<?=$pb_cinema_seat->replace_image_url?>">
		</div>
		<hr>

		<div class="text-right">
			<?php if(strlen($pb_cinema_seat->seat_id)){ ?>
				<button type="button" class="btn btn-black" onclick="pb_cinema_seat_manage_delete();">삭제하기</button>
				<button type="submit" class="btn btn-primary">수정하기</button>
			<?php }else{ ?>
				<button type="submit" class="btn btn-primary">등록하기</button>
			<?php } ?>
			
		</div>

	</form></div>

</div>