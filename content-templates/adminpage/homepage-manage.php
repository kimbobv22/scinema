<?php
	$pb_homepage_manage_map_ = pb_homepage_manage_map();

	$pb_homepage_manage_map_['address'] = empty($pb_homepage_manage_map_['address']) ? "" : $pb_homepage_manage_map_['address'];

	$pb_homepage_manage_map_['contact1'] = empty($pb_homepage_manage_map_['contact1']) ? "" : $pb_homepage_manage_map_['contact1'];

	$pb_homepage_manage_map_['email'] = empty($pb_homepage_manage_map_['email']) ? "" : $pb_homepage_manage_map_['email'];

	$pb_homepage_manage_map_['bizinfo'] = empty($pb_homepage_manage_map_['bizinfo']) ? "" : $pb_homepage_manage_map_['bizinfo'];

	$pb_homepage_manage_map_['copyrights'] = empty($pb_homepage_manage_map_['copyrights']) ? "" : $pb_homepage_manage_map_['copyrights'];
?>

<div class="adminpage-homepage-manage-frame"><form id="pb-homepage-manage-form">

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title" for="pb-homepage-manage-form-home_feature_title">디바이스 최신버전 관리</h3>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<label>안드로이드 최소버전</label>
				<input type="text" name="android_min_version" class="form-control" placeholder="버전 입력" data-error="안드로이드 버전을 입력하세요" id="pb-homepage-manage-form-android_min_version" data-validate="true" value="<?=stripslashes($pb_homepage_manage_map_['android_min_version'])?>">
			</div>

			<div class="form-group">
				<label>안드로이드 최대버전</label>
				<input type="text" name="android_max_version" class="form-control" placeholder="버전 입력" data-error="안드로이드 버전을 입력하세요" id="pb-homepage-manage-form-android_max_version" data-validate="true" value="<?=stripslashes($pb_homepage_manage_map_['android_max_version'])?>">
			</div>

			<div class="form-group">
				<label>IOS 최소버전</label>
				<input type="text" name="ios_min_version" class="form-control" placeholder="버전 입력" data-error="아이폰 버전을 입력하세요" id="pb-homepage-manage-form-ios_min_version" data-validate="true" value="<?=stripslashes($pb_homepage_manage_map_['ios_min_version'])?>">
			</div>

			<div class="form-group">
				<label>IOS 최대버전</label>
				<input type="text" name="ios_max_version" class="form-control" placeholder="버전 입력" data-error="아이폰 버전을 입력하세요" id="pb-homepage-manage-form-ios_max_version" data-validate="true" value="<?=stripslashes($pb_homepage_manage_map_['ios_max_version'])?>">
			</div>

		</div>
	</div><!-- ./panel panel-default -->

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-heading">
				<h3 class="panel-title" for="pb-homepage-manage-form-home_feature_title">본사 홈페이지 푸터 관리</h3>
			</div>
		</div><!--./panel-heading -->

		<div class="panel-body">
			<div class="form-group">
				<label>주소</label>
				<input type="text" name="address" class="form-control" placeholder="주소" id="pb-homepage-manage-form-address" value="<?=stripslashes($pb_homepage_manage_map_['address'])?>">
			</div>

			<div class="form-group">
				<label>연락처</label>
				<input type="text" name="contact1" class="form-control" placeholder="연락처" id="pb-homepage-manage-form-contact1" value="<?=stripslashes($pb_homepage_manage_map_['contact1'])?>">
			</div>

			<div class="form-group">
				<label>이메일</label>
				<input type="text" name="email" class="form-control" placeholder="이메일" id="pb-homepage-manage-form-email" value="<?=stripslashes($pb_homepage_manage_map_['email'])?>">
			</div>

			<div class="form-group">
				<label>사업자번호</label>
				<input type="text" name="bizinfo" class="form-control" placeholder="사업자번호" id="pb-homepage-manage-form-bizinfo" value="<?=stripslashes($pb_homepage_manage_map_['bizinfo'])?>">
			</div>

			<div class="form-group">
				<label>카피라이트</label>
				<input type="text" name="copyrights" class="form-control" placeholder="카피라이트" id="pb-homepage-manage-form-copyrights" value="<?=stripslashes($pb_homepage_manage_map_['copyrights'])?>">
			</div>
		</div><!-- ./panel-body -->
	</div><!-- ./panel panel-default -->

	

	<div class="text-right">
		<button type="submit" class="btn btn-primary">변경사항저장</button>	
	</div>
	



</form></div>