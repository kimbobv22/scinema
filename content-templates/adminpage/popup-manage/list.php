<?php 
class PB_adminpage_popup_manage_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		$cinema_id_ = null;
		if(!pb_cinema_current_is_head_office()){
			$cinema_id_ = pb_current_cinema_id();
		}
		
		$popup_list_ = pb_popup_list(array(
			'keyword' => $keyword_,
			'cinema_id' => $cinema_id_,
			'limit' => array($offset_, $per_page_),
			'orderby' => 'ORDER BY SRT_DATE DESC '
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_popup_list(array(
				'keyword' => $keyword_,
				'cinema_id' => $cinema_id_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $popup_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){

		$columns_ = array(
			"_checkbox" => '',
			"cinema_name" => '영화관',
			"popup_title" => "팝업명",
			"srt_date" => "게시일자",
			"hide_yn" => "숨김처리",
			"sort_num" => "순서",
		);

		if(!pb_cinema_current_is_head_office()){
			unset($columns_['cinema_name']);
		}

		return $columns_;
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){

			case "_checkbox" : 

				ob_start();

				?>

				<input type="checkbox" name="popup_id" value="<?=$item_->ID?>">

				<?php

				return ob_get_clean();

			break;
			case "cinema_name" :

				if($item_->cinema_id == PB_CINEMA_HEAD_OFFICE_ID){
					return "본사";
				}else if(strlen($item_->cinema_id === "-1")){
					return "공통";
				}else{ 
					return $item_->cinema_name;
				}

				break;
		
			case "popup_title" :
				ob_start();

			?>

			<a href="<?=pb_adminpage_url('popup-manage/edit/'.$item_->ID)?>"><?=$item_->popup_title?></a>

			<?php


				return ob_get_clean();
			case "srt_date" :
				return $item_->srt_date.'<br/>~'.$item_->end_date;	
			case "sort_num" :
				return $item_->sort_num;
			case "hide_yn" :
				ob_start()
?>

<input type="checkbox" data-popup-hide-yn="<?=$item_->ID?>" value="Y" <?=checked($item_->hide_yn, "Y")?> >

<?php

				return ob_get_clean();

			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 팝업이 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-popup-manage-table', function(){
	global $pb_board;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>

<div class="form-margin"></div>
<div class="search-frame">
	<div class="row">
		<div class="col-xs-3 col-sm-8">
			<a href="<?=pb_adminpage_url('popup-manage/add')?>" class="btn btn-default btn-sm">팝업등록</a>
		</div>
		<div class="col-xs-9 col-sm-4">
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">검색</button>
				</span>
			</div>
		</div>
	</div>
</div>

<?php }); ?>

<div class="adminpage-popup-manage-frame">
	<h4 class="sidemenupage-subtitle">팝업내역</h4>
	<div class="text-right" style="margin-top: -35px; margin-bottom: 10px;">
		<button class="btn btn-sm btn-default" disabled onclick="pb_popup_manage_bulk_delete();" id="pb-popup-manage-bulk-delete-btn">선택삭제</button>
	</div>
	<form id="pb-adminpage-popup-manage-table-form" method="get" action="<?=pb_adminpage_url('popup-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_popup_manage_table("pb-adminpage-popup-manage-table", "pb-adminpage-popup-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>