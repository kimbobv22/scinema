<?php

global $pb_popup;
$is_new_ = false;

if(!isset($pb_popup)){
	$pb_popup = (object)array(
		'ID' => null,
		'cinema_id' => null,
		'popup_title' => null,
		'popup_html' => null,

		'srt_date' => null,
		'end_date' => null,

		'size_width' => null,
		'size_height' => null,

		'sort_num' => null,
		'only_home_yn' => 'N',

		'hide_yn' => 'N',
	);

	$is_new_ = true;
}

?>
<div class="adminpage-popup-manage-edit-frame"><form method="POST" id="pb-popup-manage-edit-form" data-add-new='<?=$is_new_ ? "Y" : "N" ?>'>

	<input type="hidden" name="ID" value="<?=$pb_popup->ID?>">

	<div class="row">
		<?php if($is_new_){ ?>
			<h4 class="col col-xs-6 sidemenupage-subtitle">팝업등록</h4>
		<?php }else{ ?>
			<h4 class="col col-xs-6 sidemenupage-subtitle">팝업수정</h4>
		<?php } ?>

		<div class="col col-xs-6 text-right">
			<a href="<?=pb_adminpage_url('popup-manage')?>" class="btn btn-default">목록으로</a>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="form-group">
				<label for="pb-popup-manage-edit-form-popup_title">팝업명</label>
				
				

				<div class="input-group">
					<input type="text" name="popup_title" class="form-control" required placeholder="팝업명 입력" data-error="팝업명을 입력하세요" id="pb-popup-manage-edit-form-popup_title" value="<?=$pb_popup->popup_title?>">
					<span class="input-group-addon">

						<input type="checkbox" aria-label="..." name="only_home_yn" value="Y" <?=$pb_popup->only_home_yn ? "checked" : ""?> > 홈화면에만 띄우기
					</span>
				</div>
				<div class="help-block with-errors"></div>
			</div>

			<div class="row">

			<?php if(pb_cinema_current_is_head_office()){ ?>
				<div class="col col-sm-5 col-xs-12">
					<div class="form-group">
						<label for="pb-popup-manage-edit-form-cinema_id">영화관</label>
						<select class="form-control" name="cinema_id" id="pb-popup-manage-edit-form-scinema_id">
							<option value="-1" <?=selected($pb_popup->cinema_id, "-1")?> >공통</option>
							<?php pb_cinema_make_options(array('include_head_office' => true), $pb_popup->cinema_id); ?>
						</select>
					</div>
				</div>
				<div class="col col-sm-7 col-xs-12">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6 col-xs-12 ">
								<label for="pb-popup-manage-edit-form-size_width">팝업넓이</label>
								<div class="input-group">
									<input type="number" class="form-control" placeholder="넓이" name="size_width" id="pb-popup-manage-edit-form-size_width" data-max="600" data-max-error="최대넓이를 초과하였습니다." data-number="Y" value="<?=$pb_popup->size_width?>">
									<span class="input-group-addon">PX</span>
								</div>
								<p class="help-block"><small>*비워두면 350px로 설정됩니다.</small></p>
							</div>
							<div class="col-sm-6 col-xs-12">
								<label for="pb-popup-manage-edit-form-size_height">팝업높이</label>
								<div class="input-group">
									<input type="number" class="form-control" placeholder="높이" name="size_height" id="pb-popup-manage-edit-form-size_height" data-number="Y" value="<?=$pb_popup->size_height?>">
									<span class="input-group-addon">PX</span>
								</div>
								<p class="help-block"><small>*비워두면 400px로 설정됩니다.</small></p>
							</div>
						</div>
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>
			<?php }else{ ?>

				<input type="hidden" name="cinema_id" value="<?=pb_current_cinema_id()?>">
				<div class="col col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6 col-xs-12 ">
								<label for="pb-popup-manage-edit-form-size_width">팝업넓이</label>
								<div class="input-group">
									<input type="number" class="form-control" placeholder="넓이" name="size_width" id="pb-popup-manage-edit-form-size_width" data-max="600" data-max-error="최대넓이를 초과하였습니다." data-number="Y" value="<?=$pb_popup->size_width?>">
									<span class="input-group-addon">PX</span>
								</div>
								<p class="help-block"><small>*비워두면 350px로 설정됩니다.</small></p>
							</div>
							<div class="col-sm-6 col-xs-12">
								<label for="pb-popup-manage-edit-form-size_height">팝업높이</label>
								<div class="input-group">
									<input type="number" class="form-control" placeholder="높이" name="size_height" id="pb-popup-manage-edit-form-size_height" data-number="Y" value="<?=$pb_popup->size_height?>">
									<span class="input-group-addon">PX</span>
								</div>
								<p class="help-block"><small>*비워두면 400px로 설정됩니다.</small></p>
							</div>
						</div>
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<?php } ?>
		

			<div class="form-group">
				<label for="pb-popup-manage-edit-form-brach_desc">팝업내용</label>
				<div class="text-right" style="margin-top:-25px">
					<a href="#" data-toggle="modal" data-target="#pb-popup-manage-imageupload-popup" class="file-insert-btn btn btn-sm btn-default btn-fileupload"><i class="label-icon glyphicon glyphicon-plus"></i>이미지업로드</a>
					<div class="form-margin-xxs"></div>
				</div>

				

				<div class="popup-html-wrap"><div class="wrap">
					<?php wp_editor(stripslashes($pb_popup->popup_html),"popup_html", array(
						'media_buttons' => false,
						'quicktags' => false,
						'wpautop' => false,
					)) ?>
				</div></div>

					
					
			</div>



			<div class="form-group">
				<label for="pb-popup-manage-edit-form-srt_date">게시일자</label>
				<div class="row">
					<div class="col-xs-6 col-phone">
						<div class="input-with-icon sm">
							<input type='text' class="form-control" id="pb-popup-manage-edit-form-srt_date" name="srt_date" placeholder="시작일자" value="<?=$pb_popup->srt_date?>" />
							<i class="icon glyphicon glyphicon-calendar" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="input-with-icon sm">
							<input type='text' class="form-control" id="pb-popup-manage-edit-form-end_date" name="end_date" placeholder="종료일자" value="<?=$pb_popup->end_date?>" />
							<i class="icon glyphicon glyphicon-calendar" aria-hidden="true"></i>
						</div>
					</div>
				</div>
				<div class="help-block with-errors"></div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-popup-manage-edit-form-sort_num">순서</label>
						<input type="number" name="sort_num" class="form-control" placeholder="숫자로 입력" data-error="숫자로 입력하세요" id="pb-popup-manage-edit-form-sort_num" value="<?=$pb_popup->sort_num?>" data-number="Y">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-popup-manage-edit-form-sort_num">숨김처리</label>
						
						<div class="checkbox ">
							<label><input type="checkbox" name="hide_yn" id="pb-popup-manage-edit-form-hide_yn" value="Y" <?=checked($pb_popup->hide_yn, "Y")?> > 팝업을 숨깁니다.
						</label>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="button-area text-right">
		<?php if(!$is_new_){ ?>
			<button type="button" class="btn btn-black" onclick="_pb_popup_manage_delete();">팝업삭제</button>
		<?php } ?>
		<button type="button" class="btn btn-default" onclick="pb_popup_manage_preview();">미리보기</button>

		<?php if($is_new_){ ?>
			<button type="submit" class="btn btn-primary">등록하기</button>
		<?php }else{ ?>
			<button type="submit" class="btn btn-primary">수정하기</button>
		<?php } ?>	
		
	</div>

	

</form></div>

<div class="pb-imageupload-dropzone-modal modal" id="pb-popup-manage-imageupload-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<input id="pb-popup-manage-upload-image" type="file" name="files[]" multiple accept="image/*">
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>