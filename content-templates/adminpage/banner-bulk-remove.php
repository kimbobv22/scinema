<?php
/*
 작성일자 : 2019.07.11
 작성자 : 나하나
 설명 : 관리자 배너 일괄삭제 - 일괄등록한 내용에 대해서 일괄 삭제 기능
*/
?>
<?php 

class PB_adminpage_banner_bulk_remove_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);
		
		$banner_list_ = pb_banner_grp_list(array(
			'limit' => array($offset_, $per_page_),
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_banner_grp_list(array(
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			'items' => $banner_list_,
			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){
		return array(
			"banner_image" => "등록배너",
			"cinema_count" => "등록영화관수",
			"button_area" => "",
		);
	}

	function column_body_classes($column_name_, $item_){
		if($column_name_ === "cinema_count"){
			return "text-center";
		}
		return "";
	}

	function column_value($item_, $column_name_){
		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){
			case "banner_image" :
				ob_start();
				?>
				<img src="<?=$item_->banner_image_url?>" class="banner-image">
				<?php
				return ob_get_clean();

			case "cinema_count" :
				return strlen($item_->cinema_cnt) ? $item_->cinema_cnt : 0;

			case "button_area" :
				ob_start();
				?>
				<input type="hidden" name="_banner_data" value="<?=htmlentities(json_encode($item_))?>" data-banner-data-id="<?=$row_index_?>">
				
				<a href="javascript:_pb_banner_check_remove(<?=$row_index_?>);" class="btn btn-md btn-info">선택삭제</a>&nbsp;&nbsp;		
				<a href="javascript:pb_banner_manage_bulk_all_delete(<?=$row_index_?>);" class="btn btn-md btn-primary">일괄삭제</a>
				<?php
				return ob_get_clean();
			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 배너가 없습니다.";	
	}
}
?>
<div class="adminpage-banner-manage-frame">
	<h4 class="adminpage-subtitle">배너리스트</h4>	
	<form id="pb-adminpage-banner-manage-table-form" method="get">
		<?php
			$list_table_ = new PB_adminpage_banner_bulk_remove_table("pb-adminpage-banner-manage-table", "pb-adminpage-banner-manage-table");
			echo $list_table_->html();
		?>
	</form>
</div>

<!-- 선택삭제 모달창 -->
<div class="pb-banner-remove-modal modal" id="pb-banner-remove-popup">
<form id="pb-banner-remove-popup-form" method="post">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="text-edit">배너선택삭제</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="pb-banner-remove-popup-form-banner_image_url">배너이미지</label>
					<img name="banner_image_url" class="form-control banner_image" id="pb-banner-remove-popup-form-banner_image_url">
				</div>

				<div class="form-group">
					<label for="pb-banner-remove-popup-form-cinema-list">등록영화관</label>
					<div id="pb-banner-remove-popup-form-cinema-list"></div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-default">
					<span class="text-edit">삭제하기</span>
				</button>
			</div>
		</div>
	</div>
</form></div>