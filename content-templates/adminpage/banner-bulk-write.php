<?php
	
	$cinema_list_ = pb_cinema_list(array("orderby" => "order by cinema_name asc"));

	$screening_list_ = pb_movie_open_list(array(
		'on_screening' => true,
		'status' => '00001',
		'on_screening_at_branch' => true,
		'cinema_id' => pb_current_cinema_id(),
	));

?>
<div class="adminpage-banner-bulk-write-frame"><form id="pb-banner-bulk-write-form">
	
	<div class="form-group">
	
		<label for="pb-banner-bulk-write-form-cinema_id">상영영화관</label>
		<select name="cinema_id" class="form-control " required="" data-required-error="영화관을 선택하세요" id="pb-banner-bulk-write-form-cinema_id">
			
			<option value="">-영화관 선택-</option>
			<option value="common">공통(전체)</option>
			<?php foreach($cinema_list_ as $row_data_){ ?>
			<option value="<?=$row_data_->cinema_id?>"><?=$row_data_->cinema_name?></option>
			<?php } ?>
		
		</select>
		<small class="help-block">*개봉영화가 먼저 등록되어 있어야 정상등록됩니다.</small>

		<div class="form-margin"></div>
		<div data-all-cinema-type>
			<label>반영할 영화관 구분</label><br/>
			<?php

				$cinema_types_ = pb_gcode_dtl_list('C0001', array("only_use" => true));

				foreach($cinema_types_ as $row_data_){ ?>

				<label class="checkbox-inline">
					<input type="checkbox" name="cinema_type" value="<?=$row_data_->code_did?>" checked> <?=$row_data_->code_dnm?>
				</label>

				<?php }
			?>
		</div>

	</div>

	<input type="hidden" id="pb-banner-bulk-form-banner_image_url" data-error="배너이미지를 선택하세요" data-column-name="banner_image_url">

	<div class="text-right">
		<button type="submit" class="btn btn-primary">일괄등록하기</button>
	</div>
		
</form></div>