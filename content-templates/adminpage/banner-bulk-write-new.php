<?php
/*
 작성일자 : 2019.04.29
 작성자 : 나하나
 설명 : 관리자 배너 일괄등록 - 전체 외 원하는 상영관만 선택하는 기능 front
*/
	$cinema_list_ = pb_cinema_list(array(
		"orderby" => "order by cinema_name asc",
		"cinema_status" => "00001",
		"cinema_type" => array("00001","00005","00007"),
		));
?>
<div class="adminpage-banner-bulk-write-frame">
<form id="pb-banner-bulk-write-form">
	<div class="form-group">
		<label for="pb-banner-bulk-write-form-cinema_id">영화관선택</label>
		<div class="help-block">*배너를 등록할 영화관을 선택하여 주시기 바랍니다.</div>      
		<div class="form-group">
		<span class="button-checkbox">
			<button type="button" class="btn" data-color="info">전체선택/해제</button>
			<input type="checkbox" class="hidden" id="allCheck"/>
    	</span>
		</div>
        <div class="well">
			<?php foreach($cinema_list_ as $index => $row_data_){ ?>
			<div class="box-group solid">
				<input type="checkbox" name="cinema_id_list" id="checkbox_solid_border_<?=$index?>" value="<?=$row_data_->cinema_id?>"/>
				<label for="checkbox_solid_border_<?=$index?>"><?=$row_data_->cinema_name?></label>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class="form-group">
		<label for="pb-banner-bulk-form-banner_image_url">배너이미지</label>
		<input type="hidden" id="pb-banner-bulk-form-banner_image_url" data-error="배너이미지를 선택하세요" data-column-name="banner_image_url">
		<div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
		<label for="pb-banner-bulk-form-link_url">배너링크URL</label>
		<input type="text" name="link_url" class="form-control" placeholder="링크 입력 (예시:movie/00001) 미입력시 상영시간표로 자동셋팅 됩니다" data-error="링크를 입력해주세요" value="" id="pb-banner-bulk-form-link_url">
		<div class="help-block with-errors"></div>
	</div>

	<div class="text-right">
		<button type="submit" class="btn btn-primary">일괄등록하기</button>
	</div>	
</form>
</div>