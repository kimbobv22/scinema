<?php 

class PB_adminpage_banner_manage_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_cinema;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$banner_list_ = pb_banner_list(array(
			'cinema_id' => $pb_cinema->cinema_id,
			'keyword' => $keyword_,
			'limit' => array($offset_, $per_page_),
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_banner_list(array(
				'cinema_id' => $pb_cinema->cinema_id,
				'keyword' => $keyword_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $banner_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){
		return array(
			"_checkbox" => "",
			"banner_image" => "등록배너",
			"button_area" => "",

		);
	}

	function column_body_classes($column_name_, $item_){

		if($column_name_ === "_checkbox"){
			return "text-center";
		}
		return "";
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){

			case "_checkbox" :

				ob_start();

				?>

				<input type="checkbox" name="banner_id" value="<?=$item_->ID?>">

				<?php

				return ob_get_clean();

			case "banner_image" :

				ob_start();

				?>

				<img src="<?=$item_->banner_image_url?>" class="banner-image">

				<?php

				return ob_get_clean();
			break;
		
			case "button_area" :

				ob_start();

				?>

				<input type="hidden" name="_banner_data" value="<?=htmlentities(json_encode($item_))?>" data-banner-data-id="<?=$item_->ID?>">
				
				<a href="javascript:_pb_banner_manage_edit(<?=$item_->ID?>);" class="btn btn-sm btn-default">수정</a>				
				<a href="javascript:_pb_banner_manage_delete(<?=$item_->ID?>);" class="btn btn-sm btn-default">삭제</a>

				<?php

				return ob_get_clean();
			
			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 배너가 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-banner-manage-table', function(){
	global $pb_board;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>

<div class="form-margin"></div>
<div class="search-frame">
	<div class="row">
		<div class="col-xs-3 col-sm-8">
			<a class="btn btn-default btn-sm" href="javascript:_pb_banner_manage_add();">배너등록</a>
		</div>
		<div class="col-xs-9 col-sm-4"></div>
	</div>
</div>

<?php });
	
	global $pb_cinema;



 ?>

<div class="adminpage-banner-manage-frame">
	<?php if(pb_cinema_current_is_head_office()){ ?>
	<h4 class="adminpage-subtitle"><?=$pb_cinema->cinema_name?> 배너내역</h4>
	<?php }else{ ?>
	<h4 class="adminpage-subtitle">배너내역</h4>
	<?php } ?>
	
	<div class="text-right" style="margin-top: -35px; margin-bottom: 10px;">
		<button class="btn btn-sm btn-default" disabled onclick="pb_banner_manage_bulk_delete();" id="pb-banner-manage-bulk-delete-btn">선택삭제</button>
	</div>
	
	<form id="pb-adminpage-banner-manage-table-form" method="get" action="<?=pb_adminpage_url('banner-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_banner_manage_table("pb-adminpage-banner-manage-table", "pb-adminpage-banner-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>

<div class="pb-banner-manage-edit-modal modal" id="pb-banner-manage-edit-popup"><form id="pb-banner-manage-edit-popup-form" method="post">
	<input type="hidden" name="cinema_id" value="<?=$pb_cinema->cinema_id?>">
	<input type="hidden" name="banner_id" value="">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="text-edit">배너수정</span>
					<span class="text-add">배너추가</span>
				</h4>
			</div>
			<div class="modal-body">
				
				<div class="form-group">
					<label for="pb-banner-manage-edit-form-banner_title">배너명</label>
					<input type="text" name="banner_title" class="form-control" required placeholder="배너명 입력" data-error="배너명을 입력해주세요" value="" id="pb-banner-manage-edit-form-banner_title">
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					<label for="pb-banner-manage-edit-form-banner_image_url">배너이미지</label>
					<input type="hidden" name="banner_image_url" class="form-control" id="pb-banner-manage-edit-form-banner_image_url" required data-validate="true" data-error="배너이미지를 선택하세요">
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					<label for="pb-banner-manage-edit-form-link_url">배너링크URL</label>
					<input type="text" name="link_url" class="form-control" required placeholder="링크 입력" data-error="링크를 입력해주세요" value="" id="pb-banner-manage-edit-form-link_url">
					<div class="help-block with-errors"></div>
				</div>

			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-default">
					<span class="text-edit">수정하기</span>
					<span class="text-add">추가하기</span>
				</button>
			</div>
		</div>
	</div>
</form></div>