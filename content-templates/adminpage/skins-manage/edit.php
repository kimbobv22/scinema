<?php
	$skins_options_ = pb_skins_options();

	$header_skins_list_ = pb_skins_header_list();
	$footer_skins_list_ = pb_skins_footer_list();
	$sidewidget_skins_list_ = pb_skins_sidewidget_list();
	$homepage_skins_list_ = pb_skins_homepage_list();

	$color_options_ = $skins_options_['colors'];
?>

<div class="adminpage-skins-manage-edit-frame"><form method="POST" id="pb-skins-manage-edit-form">
	
	<div class="panel panel-default">
		<div class="panel-body">

			<h3>레이아웃</h3>
			<div class="form-group">
				<label >헤더</label>
				
				<div>
				<?php foreach($header_skins_list_ as $skin_name_ => $header_data_){ ?>
				<label class="radio-inline radio-header">
					<input type="radio" name="header" value="<?=$skin_name_?>" <?=checked($skins_options_['header'], $skin_name_)?> data-skin-part-item>
					<span class="item-block">
						<img src="<?=$header_data_['screenshot_url']?>">
						<span class="text"><?=$header_data_['name']?></span>
					</span>
				</label>
				<?php } ?>
				</div>
				
				<div class="help-block with-errors"></div>
			</div>

			<div data-part-options-wrap="header" class="well"></div>

			<hr>

			<div class="form-group">
				<label >푸터</label>
				
				<div>
				<?php foreach($footer_skins_list_ as $skin_name_ => $footer_data_){ ?>
				<label class="radio-inline radio-footer">
					<input type="radio" name="footer" value="<?=$skin_name_?>" <?=checked($skins_options_['footer'], $skin_name_)?> data-skin-part-item>
					<span class="item-block">
						<img src="<?=$footer_data_['screenshot_url']?>">
						<span class="text"><?=$footer_data_['name']?></span>
					</span>
				</label>
				<?php } ?>
				</div>
				
				<div class="help-block with-errors"></div>
			</div>

			<div data-part-options-wrap="footer" class="well"></div>

			<hr>

			<div class="form-group">
				<label >사이드위젯</label>
				
				<div>
				<?php foreach($sidewidget_skins_list_ as $skin_name_ => $sidewidget_data_){ ?>
				<label class="radio-inline radio-sidewidget">
					<input type="radio" name="sidewidget" value="<?=$skin_name_?>" <?=checked($skins_options_['sidewidget'], $skin_name_)?> data-skin-part-item>
					<span class="item-block">
						<img src="<?=$sidewidget_data_['screenshot_url']?>">
						<span class="text"><?=$sidewidget_data_['name']?></span>
					</span>
				</label>
				<?php } ?>
				</div>
				
				<div class="help-block with-errors"></div>
			</div>

			<div data-part-options-wrap="sidewidget" class="well"></div>

			<hr>

			<div class="form-group">
				<label >홈화면</label>
				
				<div>
				<?php foreach($homepage_skins_list_ as $skin_name_ => $homepage_data_){ ?>
				<label class="radio-inline radio-homepage">
					<input type="radio" name="homepage" value="<?=$skin_name_?>" <?=checked($skins_options_['homepage'], $skin_name_)?> data-skin-part-item>
					<span class="item-block">
						<img src="<?=$homepage_data_['screenshot_url']?>">
						<span class="text"><?=$homepage_data_['name']?></span>
					</span>
				</label>
				<?php } ?>
				</div>
				
				<div class="help-block with-errors"></div>
			</div>

			<div data-part-options-wrap="homepage" class="well"></div>

			<hr>
	
			<h3>로고관련</h3>

			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label >로고(밝은색)</label>
						<input type="hidden" value="<?=$skins_options_['header-white-logo-url']?>" id="header-white-logo-url-hidden" />
						<input type="hidden" value="<?=$skins_options_['header-white-logo-url']?>" name="header-white-logo-url" />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label >로고(어두운색)</label>
						<input type="hidden" value="<?=$skins_options_['header-black-logo-url']?>" id="header-black-logo-url-hidden" />
						<input type="hidden" value="<?=$skins_options_['header-black-logo-url']?>" name="header-black-logo-url" />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label >파비콘</label>
						<input type="hidden" value="<?=$skins_options_['favicon-url']?>" id="favicon-url-hidden" />
						<input type="hidden" value="<?=$skins_options_['favicon-url']?>" name="favicon-url" />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label >푸터로고</label>
						<input type="hidden" value="<?=$skins_options_['footer-logo-url']?>" id="footer-logo-url-hidden" />
						<input type="hidden" value="<?=$skins_options_['footer-logo-url']?>" name="footer-logo-url" />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<hr>

			<div class="form-group">
				<h3>홈화면 링크 슬라이더</h3>

				<input type="hidden" name="homepage-slider" id="pb-skins-homepage-slider-editor" value="<?=htmlentities(json_encode($skins_options_['homepage-slider']))?>">
				
				<small class="help-block">*스킨에 따라서 표기가 안될 수 있습니다.</small>
			</div>
		
			<hr>

			<h3>컬러</h3>
	
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트색</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary']?>" name="primary" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트색(어두운)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-dark']?>" name="primary-dark" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트색(더어두운)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-darker']?>" name="primary-darker" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트색(밝은)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-light']?>" name="primary-light" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트색(더밝은)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-lighter']?>" name="primary-lighter" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트색(비활성화)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-disabled']?>" name="primary-disabled" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트보색</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-inverse']?>" name="primary-inverse" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트보색(어두운)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-inverse-dark']?>" name="primary-inverse-dark" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트보색(더어두운)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-inverse-darker']?>" name="primary-inverse-darker" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트보색(밝은)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-inverse-light']?>" name="primary-inverse-light" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트보색(더밝은)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-inverse-lighter']?>" name="primary-inverse-lighter" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >포인트보색(비활성화)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['primary-inverse-disabled']?>" name="primary-inverse-disabled" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >보조색</label>
						<input class="color color-picker form-control" value="<?=$color_options_['secondary']?>" name="secondary" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >보조색(어두운)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['secondary-dark']?>" name="secondary-dark" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >보조색(더어두운)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['secondary-darker']?>" name="secondary-darker" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >보조색(밝은)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['secondary-light']?>" name="secondary-light" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >보조색(더밝은)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['secondary-lighter']?>" name="secondary-lighter" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >보조색(비활성화)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['secondary-disabled']?>" name="secondary-disabled"  data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>

			<hr>

			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >서브메뉴 배경색</label>
						<input class="color color-picker form-control" value="<?=$color_options_['submenu-back']?>" name="submenu-back" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
					<DIV class="clearfix"></DIV>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >서브메뉴 폰트색</label>
						<input class="color color-picker form-control" value="<?=$color_options_['submenu-font']?>" name="submenu-font" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<label >서브메뉴 폰트색(밝은)</label>
						<input class="color color-picker form-control" value="<?=$color_options_['submenu-font-light']?>" name="submenu-font-light" data-color-item />
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
	<div class="button-area text-right">
	<input type="file" name="files" id="pb-skins-options-upload-btn">
		<button type="submit" class="btn btn-primary">저장하기</button>
	</div>

	

</form></div>
