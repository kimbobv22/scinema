<?php 
class PB_adminpage_push_dvc_manage_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		$cinema_id_ = null;
		if(!pb_cinema_current_is_head_office()){
			$cinema_id_ = pb_current_cinema_id();
		}else{
			$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : "";
		}
		
		$push_list_ = pb_push_dvc_list(array(
			'keyword' => $keyword_,
			'cinema_id' => $cinema_id_,
			'limit' => array($offset_, $per_page_),
			'orderby' => 'ORDER BY REG_DATE DESC '
		));
		
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_push_dvc_list(array(
				'keyword' => $keyword_,
				'cinema_id' => $cinema_id_,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $push_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){

		$columns_ = array(
			
			"ID" => "ID",
			"cinema_name" => '영화관',
			"dvc_type_name" => "기기구분",
			"dvc_model" => "기기모델",
			"dvc_version" => "기기버젼",
			"reg_date_ymdhi" => "등록일시",
		);

		if(!pb_cinema_current_is_head_office()){
			unset($columns_['cinema_name']);
		}

		return $columns_;
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){

			case "cinema_name" :

				if($item_->cinema_id == PB_CINEMA_HEAD_OFFICE_ID){
					return "본사";
				}else if(strlen($item_->cinema_id === "-1")){
					return "공통";
				}else{ 
					return $item_->cinema_name;
				}

				break;

			case "ID" :
				return $item_->ID;
		
			case "dvc_type_name" :
				return $item_->dvc_type_name;

			case "dvc_model" :
				return $item_->dvc_model;
			case "dvc_version" :
				return $item_->dvc_version;

			case "reg_date_ymdhi" :
				return $item_->reg_date_ymdhi;
			default : 
				return '';
			break;
		}
	}
	
	function _column_classes($column_name_){
		switch($column_name_){

			case "dvc_model" :
			case "dvc_version" :
				return "hidden-xs";

			default : 
				return '';
			break;
		}
	}	

	function column_header_classes($column_name_){
		return $this->_column_classes($column_name_);
	}
	function column_body_classes($column_name_, $items_){
		return $this->_column_classes($column_name_);
	}

	function norowdata(){
		return "검색된 기기가 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-push-dvc-manage-table', function(){
	
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
?>

<div class="form-margin"></div>
<div class="search-frame">
	<div class="row">
		<div class="col-xs-3 col-sm-8">
			<a href="#" onclick="javascript:pb_adminpage_download_excel('<?=pb_adminpage_url('push-dvc-manage/list/'.$pb_adminpage_action_data)?>', '#pb-adminpage-push-dvc-manage-table-form')" class="btn btn-default btn-sm">엑셀다운로드</a>
		</div>
		<div class="col-xs-9 col-sm-4">
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">검색</button>
				</span>
			</div>
		</div>
	</div>
</div>

<?php }); ?>

<div class="adminpage-push-dvc-manage-frame">
	<h4 class="sidemenupage-subtitle">앱기기등록내역</h4>
	
	<form id="pb-adminpage-push-dvc-manage-table-form" method="get" action="<?=pb_adminpage_url('push-dvc-manage')?>">
		<?php
			$list_table_ = new PB_adminpage_push_dvc_manage_table("pb-adminpage-push-dvc-manage-table", "pb-adminpage-push-dvc-manage-table");
			echo $list_table_->html();
		?>
	</form>

</div>