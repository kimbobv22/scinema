<?php

class PB_notice_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		$notice_list_ = pb_notice_list(array(
			'cinema_id' => pb_current_cinema_id(),
			'with_common' => true,
			'limit' => array($offset_, $per_page_),
		));

		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_notice_list(array(
				'cinema_id' => pb_current_cinema_id(),
				'with_common' => true,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $notice_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	
	function columns(){
		return array(
			"notice_title" => "제목",
			"reg_date" => "작성일자",
		);
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){
			case "notice_title" :
				ob_start();
?>

				<a href="<?=pb_notice_view_url($item_->ID)?>">
					<?=$item_->notice_title?>
				</a>

<?php
				
				return ob_get_clean();

			case "reg_date" :
				ob_start();
?>
				
				<div class="date-item reg-date"><i class="icon fa fa-clock-o" aria-hidden="true"></i> <?=$item_->reg_date_ymd?></div>
				

<?php
					
				return ob_get_clean();

			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 공지사항이 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-notice-table', function(){
		global $pb_board;
		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
	?>
	
	<div class="form-margin"></div>
	<div class="search-frame">
		<div class="row">
			<div class="col-xs-3 col-sm-8">
				<?php if(pb_is_admin()){ ?>
					<a href="<?=pb_adminpage_url("notice-manage/add")?>" class="btn btn-default btn-sm">글쓰기</a>
				<?php } ?>
			</div>
			<div class="col-xs-9 col-sm-4">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">검색</button>
					</span>
				</div>
			</div>
		</div>
	</div>

	<?php
});

?>

<div class="pb-notice-list-frame">
	
	<div class="pb-notice-top-label">
		<h3 class="notice-title">공지사항</h3>
	</div>
	<form id="pb-notice-list-form" method="get" action="<?=pb_notice_list_url()?>">
		
		
		<?php
			$list_table_ = new PB_notice_table("pb-notice-table", "pb-notice-table");
			echo $list_table_->html();
		?>
	</form>

</div>