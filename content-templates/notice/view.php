<?php

	global $pb_notice;

	$attachment_files_ = json_decode(stripslashes($pb_notice->attachments), true);
	if(gettype($attachment_files_) !== "array"){
		$attachment_files_ = array();
	}
?>
<div class="pb-notice-view-frame">
	
	<div class="pb-notice-top-label">
		<h3 class="notice-title">공지사항</h3>
		<a href="<?=pb_notice_list_url()?>" class="list-btn btn btn-default btn-xs">목록으로</a>
	</div>
	<div class="notice-content-wrap pb-notice-view">
		<div class="pb-notice-header">
			<h3 class="title"><?=htmlentities(stripslashes($pb_notice->notice_title))?></h3>
			<div class="subinfo">
				
				<div class="item">
					<i class="icon fa fa-clock-o" aria-hidden="true"></i><?=$pb_notice->reg_date_ymd?>
				</div>
				
			</div>
		</div>

		<div class="pb-notice-body">
			<?=stripslashes($pb_notice->notice_html)?>
		</div>
		
		<?php if(count($attachment_files_) > 0){ ?>
		<div class="pb-notice-footer">
			<ul class="attachment-files">
			<?php foreach($attachment_files_ as $attachment_file_){ ?>
				<li><a href="<?=pb_filedownload_url($attachment_file_['url'], $attachment_file_['name'])?>" target="_blank"><i class="fa fa-file-o" aria-hidden="true"></i> <?=$attachment_file_['name']?></i></a></li>
			<?php } ?>


			</ul>
		</div>
		<?php } ?>
		<?php if(pb_is_admin()) {
				$notice_cinma_id = $pb_notice->cinema_id;
				if($notice_cinma_id != '-1' or pb_cinema_current_is_head_office()){	
		?>
		<div class="notice-content-bottom-label text-right">
			<a href="<?=pb_adminpage_url("notice-manage/edit/".$pb_notice->ID)?>" class="btn btn-sm btn-default">수정</a>
		</div>
		<?php }} ?>
	</div>
</div>