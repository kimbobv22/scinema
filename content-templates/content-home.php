<?php
//20170625 nhn 디트릭스 영화포스터로 가져오기
	$screening_list_ = pb_dtryx_movie_now_list('all','small','');
	$scheduled_list_ = pb_dtryx_movie_plan_list('all','small','');

	$main_banner_list_= pb_banner_main_list(array(
		'in_date' => true,
		'banner_kind' => 'main',
		'hide_yn' => 'N',
		'orderby' => 'ORDER BY srt_date ASC, end_date asc, reg_date asc'
	));
?>
<div class="page-home-frame">
	<!-- 메인배너 -->
	<div class="main-banner-slider-wrap">
		<div class="swiper-container" id="pb-home-main-banner-slider">
			<ul class="swiper-wrapper main-visual-slide" id="pb-home-main-banner-slider">
				<!-- <li class="swiper-slide">
					<span class="bg-right" style="background-color: #d37848"></span>   
					<span class="bg-left" style="background-color: #a45d6d"></span>   
					<a href="https://scinema.org/filmfestival">
					<span class="bg-content" style="background-image:url(<?=PB_THEME_DIR_URL?>img/home/banner/main-full-fallff3.jpg);">
						<span class ="youtube-con">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/n0hNtv4Fs1w?rel=0&autoplay=0&howsearch=0" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</span>
					</span>
					</a>
				</li> -->
				<li class="swiper-slide">
					<span class="bg-right" style="background-color: #eca117"></span>   
					<span class="bg-left" style="background-color: #986621"></span>   
					<span class="bg-content" style="background-image:url(<?=PB_THEME_DIR_URL?>img/home/banner/main1-190809.jpg);">
						<p class = "info">
							<span class="title">작은영화관 App</span>           
							<span class="sub">지금 바로 새로워진</span>           
							<span class="sub">작은영화관 앱을 만나보세요</span>           
							<a href="https://apps.apple.com/kr/app/작은영화관/id1470560758" target="_blank">
								<span class="boxinfo boxbtn">
									<i class="fa fa-apple" aria-hidden="true"></i> Apple store 다운로드
								</span>
							</a>
							<a href="https://play.google.com/store/apps/details?id=com.dtryx.scinema" target="_blank">
								<span class="boxinfo boxbtn">
									<i class="fa fa-android" aria-hidden="true"></i>
									Google Play 다운로드
								</span>
							</a>   
						</p>
					</span>
				</li>

				<?php foreach($main_banner_list_ as $row_data_){ 
					if (($row_data_->link_target_yn) === 'Y'){
						$newlink = '_blank';
					}
					else{ $newlink = '_self'; }
				?>
				<li class="swiper-slide">
					<span class="bg-left" style="background-color: <?=$row_data_->banner_left_color?>"></span>   
					<span class="bg-right" style="background-color: <?=$row_data_->banner_right_color?>"></span>   
					<a class="bg-content" href="<?=$row_data_->link_url?>" target="<?=$newlink ?>" style="background-image:url(<?=$row_data_->image_url?>);"> 
						<div>
							<?=stripslashes($row_data_->banner_html)?>
						</div>
					</a>
				</li>	
				<?php } ?>
			</ul>
			<div class="swiper-pagination"></div>
		</div>
	</div>

	<!-- 현재상영작/상영예정작 -->
	<div class="movie-chart-wrap">
		<div class="container">
		<ul class="movie-chart-type-menu" id="pb-movie-chart-type-menu">
			<li class="actived"><a href="#" data-movie-slider-tab="#pb-screening-movie-tab">현재상영작</a></li>
			<li><a href="#" data-movie-slider-tab="#pb-scheduled-movie-tab">상영예정작</a></li>
		</ul>
		<div class="movie-slider-wrap" id="pb-screening-movie-tab">
			<div class="swiper-container" id="pb-home-movie-chart-slider1">
				<div class="swiper-wrapper">
				<?php 
					pb_dtryx_movie_draw_light($screening_list_);
				?>
				</div>
			</div>
			<div class="sc-swiper">
				<div class="swiper-button-next swiper-button-black"></div><!-- 다음 버튼 (오른쪽에 있는 버튼) -->
				<div class="swiper-button-prev swiper-button-black"></div><!-- 이전 버튼 -->
			</div>
		</div>
		<div class="movie-slider-wrap hidden" id="pb-scheduled-movie-tab">
			<div class="swiper-container" id="pb-home-movie-chart-slider2">
				<div class="swiper-wrapper">
				<?php 
					pb_dtryx_movie_draw_light($scheduled_list_);
				?>
				</div>
			</div>
			<div class="sc-swiper">
				<div class="swiper-button-next swiper-button-black"></div><!-- 다음 버튼 (오른쪽에 있는 버튼) -->
				<div class="swiper-button-prev swiper-button-black"></div><!-- 이전 버튼 -->
			</div>
		</div>
		</div>
	</div>

	<!-- 언론보도 공지사항 이벤트-->
	<div class="homeboard-wrap"><div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-5">
				
				<div class="homeboard-item news-board">
					<?php
						$notice_list_ = pb_notice_list(array(
							'cinema_id' => pb_current_cinema_id(),
							'with_common' => true,
							'limit' => array(0,5),
						));
					?>
					<div class="homeboard-header">
						<div class="left"><h3>공지사항</h3></div>
						<div class="right"><a href="<?=pb_notice_list_url()?>"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></div>
					</div>
					<div class="homeboard-body">
						<ul class="homeboard-table">
							<?php foreach($notice_list_ as $row_data_){ ?>
							<li>
								<div class="post-title"><a href="<?=pb_notice_view_url($row_data_->ID)?>"><?=htmlentities(stripcslashes($row_data_->notice_title))?></a></div>
								<div class="wrt-date"><?=$row_data_->reg_date_ymd?></div>
							</li>
							<?php }?>

							<?php if(count($notice_list_) <= 0){ ?>

								<li class="no-post">최근 공지사항이 없습니다.</li>

							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="homeboard-item media-board">
					<?php
						$press_list_ = pb_press_list(array(
							'cinema_id' => pb_current_cinema_id(),
							'with_common' => true,
							'limit' => array(0,5),
						));
					?>
					<div class="homeboard-header">
						<div class="left"><h3>언론보도</h3></div>
						<div class="right"><a href="<?=pb_press_list_url()?>"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></div>
					</div>
					<div class="homeboard-body">
						<ul class="homeboard-table">
							<?php foreach($press_list_ as $row_data_){ ?>
							<li>
								<div class="post-title"><a href="<?=pb_press_view_url($row_data_->ID)?>"><?=htmlentities(stripcslashes($row_data_->press_title))?></a></div>
								<div class="wrt-date"><?=$row_data_->reg_date_ymd?></div>
							</li>
							<?php }?>

							<?php if(count($press_list_) <= 0){ ?>

								<li class="no-post">최근 언론보도가 없습니다.</li>

							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3">
				<div class="customer-info-frame">
					<!-- <a class="customer-info-event" href="https://oc.scinema.org">
						<img src="<?=PB_THEME_DIR_URL?>img/home/banner/home-event.jpg">
					</a> -->
					<!-- <a class="customer-info-event" href="<?=pb_cinema_list_url()?>">
						<img src="<?=PB_THEME_DIR_URL?>img/home/banner/home-event2.jpg">
					</a> -->
					<div class="swiper-container" id="pb-home-main-event-slider">
						<div class="swiper-wrapper">
						<?php 
						$main_event_list_= pb_banner_main_list(array(
							'in_date' => true,
							'banner_kind' => 'event',
							'hide_yn' => 'N',
							'orderby' => 'ORDER BY srt_date ASC, end_date asc, reg_date asc',
							'limit' => array(0,4),
						));
						foreach($main_event_list_ as $row_data_){
							if (($row_data_->link_target_yn) === 'Y'){
								$event_newlink = '_blank';
							}
							else{ $event_newlink = '_self'; }
						?>
						<div class="swiper-slide">
							<a class="customer-info-event" href="<?=$row_data_->link_url?>" target ="<?=$event_newlink?>">
								<img src="<?=$row_data_->image_url?>">
							</a>
						</div>
						<?php }?>
						</div>
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
		</div>
	</div></div>
	<!-- 파트너 배너 -->
	<div class="partner-slider-wrap"><div class="container">
		<div class="swiper-container" id="pb-home-partner-slider">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<a href="http://www.mcst.go.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner001.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.kofic.or.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner002.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.socialenterprise.or.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner003.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.culture.go.kr/wday/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner004.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.cjenm.com/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner005.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.lotteent.com/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner006.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://showbox.co.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner007.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.its-new.co.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner008.jpg">
					</a>
				</div>
			</div>
		</div>
	</div></div>
	<!-- 작은영화관 찾기 -->
	<div class="cinema-list-wrap"><div class="container">

		<h3 class="cinema-list-title"><i class="fa fa-search" aria-hidden="true"></i> 작은영화관 찾기</h3>
		<div class="cinema-list">

			<?php
				$cinema_area_list_ = pb_gcode_dtl_list("C0005");
			?>
			<?php foreach($cinema_area_list_ as $row_data_){ ?>
			<div class="cinema-item">
				<div class="area"><?=$row_data_->code_dnm?></div>
				<div class="links">
				<?php 
					$cinema_list_ = pb_cinema_list(array(
						'area_id'=>$row_data_->code_did,
						"cinema_status" => '00001',
						'cinema_type' => PB_CINEMA_CTG_NOR
					)); 
					$linebreak_count_ = 0;
					foreach($cinema_list_ as $sub_row_data_){

						++$linebreak_count_;

						$linebreak_ = $linebreak_count_ > 4;
				?>
					<a href="https://<?=$sub_row_data_->domain.$sub_row_data_->path?>" target="_blank"><?=$sub_row_data_->cinema_name?></a>
				<?php 
						$result_ = next($cinema_list_);
						if($result_ == true){
				?>
						<span class="divchar"></span>
				<?php
						}

						if($linebreak_){
							$linebreak_count_ = 0; 
							
						}
					
					}

					if(count($cinema_list_) <= 0){
				?>
					<span class="comingsoon">예정</span>
				<?php
					}
				?>
				</div>
			</div>
			<?php }?>
		</div>
	</div></div>
</div>