<?php
	// $screening_list_ = pb_movie_open_list(array(
	// 	'on_screening' => true,
	// 	'status' => '00001',
	// 	'on_screening_at_branch' => true,
	// 	'orderby' => 'ORDER BY DATEDIFF(NOW(), open_date) ASC',
	// 	'cinema_id' => pb_current_cinema_id(),
	// ));

	// $scheduled_list_ = pb_movie_open_list(array(
	// 	'on_scheduled' => true,
	// 	'status' => '00001',
	// 	'orderby' => 'ORDER BY open_date ASC',
	// 	'cinema_id' => pb_current_cinema_id(),
	// ));

	$screening_list_ = pb_dtryx_movie_now_list('all','small','');
	$scheduled_list_ = pb_dtryx_movie_plan_list('all','small','');

?>
<div class="page-home-frame">
	
	<div class="movie-chart-wrap"><div class="container">
		<ul class="movie-chart-type-menu" id="pb-movie-chart-type-menu">
			<li class="actived"><a href="#" data-movie-slider-tab="#pb-screening-movie-tab">현재상영작</a></li>
			<li><a href="#" data-movie-slider-tab="#pb-scheduled-movie-tab">상영예정작</a></li>
		</ul>
		<div class="movie-slider-wrap" id="pb-screening-movie-tab">
			<div class="swiper-container" id="pb-home-movie-chart-slider1">
				<div class="swiper-wrapper">
				<?php 
					pb_dtryx_movie_draw_dark($screening_list_);
				?>
					<!-- <?php foreach($screening_list_ as $row_data_){ ?>
						<div class="swiper-slide" >
							<div class="pb-movie-item dark">
								<a class="poster-image" href="<?=pb_movie_view_url($row_data_->ID)?>">
									<img src="<?=$row_data_->image_url?>" class="image">
								</a>
								<div class="movie-info">
									<span class="movie-name">
										<?php pb_movie_draw_level_badge($row_data_->level); ?>
										<?=$row_data_->movie_name?></span>
								</div>
							</div>
						</div>
						<?php } ?> -->
				</div>
			</div>
			<div class="sc-swiper">
				<div class="swiper-button-next swiper-button-white"></div><!-- 다음 버튼 (오른쪽에 있는 버튼) -->
				<div class="swiper-button-prev swiper-button-white"></div><!-- 이전 버튼 -->
			</div>
		</div>
		<div class="movie-slider-wrap hidden" id="pb-scheduled-movie-tab">
			<div class="swiper-container" id="pb-home-movie-chart-slider2">
				<div class="swiper-wrapper">
				<?php 
					pb_dtryx_movie_draw_dark($scheduled_list_);
				?>
					<!-- <?php foreach($scheduled_list_ as $row_data_){ ?>
						<div class="swiper-slide" >
							<div class="pb-movie-item dark">
								<a class="poster-image" href="<?=pb_movie_view_url($row_data_->ID)?>">
									<img src="<?=$row_data_->image_url?>" class="image">
								</a>
								<div class="movie-info">
									
									<span class="movie-name">
										<?php pb_movie_draw_level_badge($row_data_->level); ?>
										<?=$row_data_->movie_name?></span>
								</div>
							</div>
						</div>
						<?php } ?> -->
				</div>
			</div>
			<div class="sc-swiper">
				<div class="swiper-button-next swiper-button-white"></div><!-- 다음 버튼 (오른쪽에 있는 버튼) -->
				<div class="swiper-button-prev swiper-button-white"></div><!-- 이전 버튼 -->
			</div>
		</div>
	</div></div>

	
	<div class="homeboard-wrap"><div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-5">
				
				<div class="homeboard-item news-board">
					<?php
						$notice_list_ = pb_notice_list(array(
							'cinema_id' => pb_current_cinema_id(),
							'with_common' => true,
							'limit' => array(0,5),
						));
					?>
					<div class="homeboard-header">
						<div class="left"><h3>공지사항</h3></div>
						<div class="right"><a href="<?=pb_notice_list_url()?>"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></div>
					</div>
					<div class="homeboard-body">
						<ul class="homeboard-table">
							<?php foreach($notice_list_ as $row_data_){ ?>
							<li>
								<div class="post-title"><a href="<?=pb_notice_view_url($row_data_->ID)?>"><?=htmlentities(stripcslashes($row_data_->notice_title))?></a></div>
								<div class="wrt-date"><?=$row_data_->reg_date_ymd?></div>
							</li>
							<?php }?>

							<?php if(count($notice_list_) <= 0){ ?>

								<li class="no-post">최근 공지사항이 없습니다.</li>

							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<div class="homeboard-item media-board">
					<?php
						$press_list_ = pb_press_list(array(
							'cinema_id' => pb_current_cinema_id(),
							'with_common' => true,
							'limit' => array(0,5),
						));
					?>
					<div class="homeboard-header">
						<div class="left"><h3>언론기사</h3></div>
						<div class="right"><a href="<?=pb_press_list_url()?>"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></div>
					</div>
					<div class="homeboard-body">
						<ul class="homeboard-table">
							<?php foreach($press_list_ as $row_data_){ ?>
							<li>
								<div class="post-title"><a href="<?=pb_press_view_url($row_data_->ID)?>"><?=htmlentities(stripcslashes($row_data_->press_title))?></a></div>
								<div class="wrt-date"><?=$row_data_->reg_date_ymd?></div>
							</li>
							<?php }?>

							<?php if(count($press_list_) <= 0){ ?>

								<li class="no-post">최근 언론보도가 없습니다.</li>

							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3">
				<div>
				aaaaa 
				</div>
			</div>
		</div>
	</div></div>

	<div class="partner-slider-wrap"><div class="container">
		<div class="swiper-container" id="pb-home-partner-slider">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<a href="http://www.mcst.go.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner001.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.kofic.or.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner002.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.socialenterprise.or.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner003.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.culture.go.kr/wday/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner004.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.cjenm.com/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner005.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.lotteent.com/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner006.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://showbox.co.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner007.jpg">
					</a>
				</div>
				<div class="swiper-slide">
					<a href="http://www.its-new.co.kr/" class="partner-item" target="_blank">
						<img src="<?=PB_THEME_DIR_URL?>img/home/partner008.jpg">
					</a>
				</div>
			</div>
		</div>
	</div></div>

	<div class="cinema-list-wrap"><div class="container">

		<h3 class="cinema-list-title"><i class="fa fa-search" aria-hidden="true"></i> 작은영화관 찾기</h3>
		<div class="cinema-list">

			<?php
				$cinema_area_list_ = pb_gcode_dtl_list("C0005");
			?>
			<?php foreach($cinema_area_list_ as $row_data_){ ?>
			<div class="cinema-item">
				<div class="area"><?=$row_data_->code_dnm?></div>
				<div class="links">
				<?php 
					$cinema_list_ = pb_cinema_list(array(
						'area_id'=>$row_data_->code_did,
						"cinema_status" => '00001',
						'cinema_type' => PB_CINEMA_CTG_NOR
					)); 
					$linebreak_count_ = 0;
					foreach($cinema_list_ as $sub_row_data_){

						++$linebreak_count_;

						$linebreak_ = $linebreak_count_ > 4;
				?>
					<a href="https://<?=$sub_row_data_->domain.$sub_row_data_->path?>" target="_blank"><?=$sub_row_data_->cinema_name?></a>
				<?php 
						$result_ = next($cinema_list_);
						if($result_ == true){
				?>
						<span class="divchar"></span>
				<?php
						}

						if($linebreak_){
							$linebreak_count_ = 0; 
							
						}
					
					}

					if(count($cinema_list_) <= 0){
				?>
					<span class="comingsoon">예정</span>
				<?php
					}
				?>
				</div>
			</div>
			<?php }?>
		</div>
	</div></div>


</div>