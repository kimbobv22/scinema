<?php
	global $user_resetpass_user, $user_resetpass_vkey;
?>
<div class="page-resetpass-frame"><div class="container-xs">
	<form id="pb-user-resetpass-form" class="user-resetpass-form pb-border-form" autocomplete="off" data-login-url="<?=pb_user_login_url()?>">
		<input type="hidden" name="ID" value="<?=$user_resetpass_user->ID?>">
		<input type="hidden" name="vkey" value="<?=$user_resetpass_vkey?>">

		<div class="welcome-frame">
			<div class="image-col">
				<img src="<?=PB_THEME_DIR_URL?>img/user/welcome-logo.jpg" class="welcome-logo">
			</div>
			<div class="content-col">
				<h2 class="title"><span class="text-primary">비밀번호</span> 재설정</h2>
				<p class="help-block">비밀번호 재설정을 위한 화면입니다.</p>
			</div>
		</div>
		<div class="form-margin-lg"></div>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="pb-user-resetpass-form-user-email">가입이메일</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?=$user_resetpass_user->user_email?></p>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="pb-user-resetpass-form-user-pass">비밀번호</label>
					<div class="col-sm-5 col-xs-12">
						<input type="password" class="form-control" id="pb-user-resetpass-form-user-pass" placeholder="비밀번호 입력" name="user_pass" required data-error="비밀번호를 입력하세요">
					</div>
					<div class="col-sm-5 col-xs-12">
						<div class="form-margin-xxs visible-xs"></div>
						<input type="password" class="form-control" id="pb-user-resetpass-form-user-pass-c" placeholder="비밀번호 다시입력" name="user_pass_c" required data-match="#pb-user-resetpass-form-user-pass" data-error="비밀번호를 확인하세요">
					</div>
					<div class="col-sm-10 col-xs-12 col-xs-offset-0 col-sm-offset-2">
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

		</div>

		<div class="text-right">
			<button type="submit" class="btn btn-primary btn-lg">비밀번호 재설정</button>
		</div>

	</form>
</div></div>