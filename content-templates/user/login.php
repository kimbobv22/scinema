<?php

	$redirect_url_ = isset($_GET['redirect_url']) ? $_GET['redirect_url'] : home_url();

	$cinema_data_ = pb_cinema(pb_current_cinema_id());

	if($cinema_data_->cinema_type === PB_CINEMA_CTG_NOR){
		$cinema_name_ = "작은영화관";
	}else{
		$cinema_name_ = $cinema_data_->cinema_name;
	}
?>
<div class="page-login-frame"><div class="container-xs">

	<div class="login-form-frame">
	
	<?php if($cinema_data_->cinema_type === PB_CINEMA_CTG_NOR){ ?>
		<div class="image-col">
			<img src="<?=PB_THEME_DIR_URL?>img/user/welcome-logo.jpg" class="welcome-logo">
		</div>
	<?php } ?>
		<div class="content-col">
			
			<div class="title-wrap">
				<h2><span class="text-primary hidden-xs"><?=$cinema_name_?></span> 로그인</h2>
				<p class="help-block"><?=$cinema_name_?> 로그인으로 <br class="visible-xs" />회원서비스를 이용하실 수 있습니다.</p>
			</div>
			<div class="form-margin"></div>
			
				<form id="pb-user-login-form" class="user-login-form pb-border-form" data-redirect-url="<?=$redirect_url_?>" autocomplete="off">
					<div class="login-input-wrap">

						<div class="input-col">
							<div class="form-group">
								<div class="input-with-icon lg">
									<input type="text" class="form-control" id="pb-user-login-form-user-login" placeholder="아이디 또는 이메일" name="login_or_email" required >
									<i class="icon fa fa-user" aria-hidden="true"></i>
								</div>
								<div class="form-margin-xxs"></div>
								<div class="input-with-icon lg">
									<input type="password" class="form-control input-lg" id="pb-user-login-form-user-pass" placeholder="비밀번호" name="user_pass" required >
									<i class="icon fa fa-lock" aria-hidden="true"></i>
								</div>

								<div class="clearfix"></div>
							</div>	

							<div class="help-block input-footer">
								<div class="pull-left">
									<a href="<?=pb_user_signup_url()?>">회원가입</a>
									<a href="#" data-toggle="modal" data-target="#pb-user-findpass-modal">비밀번호찾기</a>
								</div>

								<div class="pull-right">
									<label class="checkbox-inline "><input type="checkbox" name="remember" value="Y" checked> 로그인유지</label>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="button-col">
							<div class="form-group">
								<button type="submit" class="btn btn-lg btn-block btn-primary">로그인</button>
								<a href="<?=pb_user_signup_url()?>" class="btn btn-lg btn-block btn-default visible-xs">회원가입</a>
							</div>
							<div class="form-group visible-xs text-center">
								<a href="#" data-toggle="modal" data-target="#pb-user-findpass-modal">비밀번호찾기</a>
							</div>
						</div>
						
					</div>
				</form>
				
		</div>
	</div>
</div></div>
<div class="modal fade " tabindex="-1" role="dialog" id="pb-user-findpass-modal">
	<div class="modal-dialog " role="document"><div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">비밀번호찾기</h4>
		</div>
		<div class="modal-body"><form id="pb-user-findpass-form" class="user-findpass-form pb-border-form" >
			
			<div class="form-group"><div class="col-xs-12">
				<div class="input-with-icon lg">
					<input type="mail" class="form-control" id="pb-user-findpass-form-user-email" placeholder="가입하신 이메일 입력" name="user_email" required >
					<i class="icon fa fa-envelope" aria-hidden="true"></i>
				</div>
				<p class="help-block text-center">가입하신 이메일로 <br class="visible-xs" />암호를 재설정할 수 있습니다.</p>
			
			</div></div>

			<div class="form-div-border"></div>

			<div class="form-margin-xs"></div>
			<button type="submit" class="btn btn-primary btn-block btn-lg">전송하기</button>
			</div>
		</form></div>
		
	</div></div>
</div>