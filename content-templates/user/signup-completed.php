<?php
	pb_session_remove(PB_USER_SIGNUP_COMPLETED);
?>
<div class="page-signup-completed-frame"><div class="wrap">
	

		<div class="completed-frame">
			<div class="image-col">
				<img src="<?=PB_THEME_DIR_URL?>img/user/welcome-logo.jpg" class="welcome-logo">
			</div>
			<div class="content-col">
				<h2 class="title"><span class="text-primary hidden-xs">작은영화관 </span>통합회원가입 성공!</h2>
				<p class="help-block">작은영화관 최신정보 및 영화예매 등 회원혜택을 바로 이용할 수 있습니다.</p>
			</div>

		</div>
		<p class="text-center">
			<a href="<?=home_url()?>" class="btn btn-lg btn-primary">홈으로 이동</a>
		</p>
			
		
</div></div>