<?php
	
	if(pb_session_get(PB_USER_SIGNUP_COMPLETED)){
		get_template_part('content-templates/user/signup-completed');
		return;
	}

	$need_auth_myself_ = pb_user_need_auth_myself();
	$need_auth_email_ = pb_user_need_auth_email();

	$for_app_ = isset($_REQUEST['for_app']) ? $_REQUEST['for_app']: "N";

	$cinema_data_ = pb_cinema(pb_current_cinema_id());

	if($cinema_data_->cinema_type === PB_CINEMA_CTG_NOR){
		$cinema_name_ = "작은영화관";
	}else{
		$cinema_name_ = $cinema_data_->cinema_name;
	}
?>
<div class="page-signup-frame"><div class="wrap">
	<?php if($for_app_ === "Y"){ ?>
	<style type="text/css">
		.signup-close-btn{
			position: fixed;
			font-size: 30px;
			right: 20px;
			top: 10px;
		}
	</style>
	<a href="/mobile/close-cancel" class="signup-close-btn"><i class="fa fa-times"></i></a>
	<?php } ?>
	
	<form id="pb-user-signup-form" class="user-signup-form pb-border-form" autocomplete="off">
		<input type="hidden" name="for_app" value="<?=$for_app_?>">

		<div class="welcome-frame">
		<?php if($cinema_data_->cinema_type === PB_CINEMA_CTG_NOR){ ?>
			<div class="image-col">
				<img src="<?=PB_THEME_DIR_URL?>img/user/welcome-logo.jpg" class="welcome-logo">
			</div>
		<?php } ?>
			<div class="content-col">
				<h2 class="title"><span class="text-primary"><?=$cinema_name_?> </span>회원가입<span class="hidden-xs">을 환영합니다!</span></h2>
				<p class="help-block"><?=$cinema_name_?> 회원가입을 통해 <br class="visible-xs" />영화예매, 최신뉴스를 확인하실 수 있습니다.</p>
			</div>
		</div>
			
		<div class="agreement-frame container-xs">
				
			<h4>이용약관</h4>
			<div class="agreement-box">
				<?php 

					ob_start();
					get_template_part('content-templates/user/agreement'); 

					$agreement_html_ = ob_get_clean();

    
				    if($current_cinema_->cinema_type === PB_CINEMA_CTG_NOR){
				        $target_map_ = array(
				            "회사명" => '작은영화관사회적협동조합',
				            "작은영화관명" => '작은영화관',
				        );
				    }else{
				        $target_map_ = array(
				            "회사명" => $cinema_name_,
				            "작은영화관명" => $cinema_name_,
				        );
				    }

				    echo pb_map_string($agreement_html_, $target_map_);

				?>
			</div>
			<div class="form-group"><div class="col-xs-12"><div class="checkbox with-top text-right">
				<label><input type="checkbox" name="agreement01" required data-error="이용약관 동의가 필요합니다"> 이용약관에 동의합니다.</label>
				<div class="help-block with-errors"></div>
				<div class="clearfix"></div>
			</div></div></div>

			<h4>개인정보 수집 및 이용에 대한 안내</h4>
			<table class="table table-bordered privacy-info-table">
				<tbody>
					<tr class="target-purpose">
						<th>수집목적</th>
						<td>이용자 식별 및 본인 여부 확인</td>
					</tr>
					
					<tr class="target-entry">
						<th>수집항목</th>
						<td>아이디, 비밀번호, 이름, 생년월일, 이메일, 연락처</td>
					</tr>
					<tr class="retention-period">
						<th>보유기간</th>
						<td>회원 탈퇴 시 까지</td>
					</tr>
				</tbody>
			</table>
			<div class="form-group"><div class="col-xs-12"><div class="checkbox with-top text-right">
				<label><input type="checkbox" name="agreement02" required data-error="개인정보수집 동의가 필요합니다"> 개인정보수집에 동의합니다.</label>
				<div class="help-block with-errors"></div>
				<div class="clearfix"></div>
			</div></div></div>

		</div>

		<div class="input-form-wrap container-xs">
			<h4>가입정보입력</h4>
			<div class="panel panel-default"><div class="panel-body">


			<div class="form-group">
				<label class="col-sm-2 control-label" for="pb-user-signup-form-user-login">아이디</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="pb-user-signup-form-user-login" placeholder="아이디 입력" name="user_login" required data-remote="<?=pb_ajax_url('user-check-exists-user-login', array(
							'reverse' => "Y",
							'key' => 'user_login',
						))?>" data-error="아이디를 입력하세요" data-remote-error="이미 사용하고 있는 아이디입니다">
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>
				</div>
			</div>

			<?php if(!$need_auth_email_){ ?>
				<input type="hidden" name="need_auth_email" value="N">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="pb-user-signup-form-user-email">이메일</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="pb-user-signup-form-user-email" placeholder="이메일 입력" name="user_email" required data-error="이메일을 입력하여 주세요" data-remote="<?=pb_ajax_url('user-check-exists-email', array(
							'reverse' => "Y",
							'key' => 'user_email',
						))?>" data-remote-error="이미 가입한 이메일입니다">
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
				</div>
			<?php }else{ ?>
				<input type="hidden" name="need_auth_email" value="Y">
				<div class="form-group" data-email-auth-group>
					<label class="col-sm-2 col-xs-12 control-label" for="pb-user-signup-form-user-email">이메일</label>
					<div class="col-sm-8 col-xs-8">
						<input type="email" class="form-control" id="pb-user-signup-form-user-email" placeholder="이메일 입력" name="user_email" required data-error="이메일을 입력하여 주세요" data-remote="<?=pb_ajax_url('user-check-exists-email', array(
							'reverse' => "Y",
							'key' => 'user_email',
						))?>" data-remote-error="이미 가입한 이메일입니다">
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-2 col-xs-4"">
						<button type="button" class="btn btn-default  btn-block btn-send-autcode btn-ajax" disabled>
							<span class="normal">인증요청</span>
							<span class="loading"><i class="fa fa-circle-o-notch fa-spin"></i></span>
							<span class="complete"><i class="fa fa-check text-success"></i> 인증됨</span>
						</button>
					</div>
				</div>

				<div class="form-group hidden" data-email-authcode-group >
					<label class="col-sm-2 col-xs-12 control-label" for="pb-user-signup-form-authcode-email">인증코드</label>
					<div class="col-sm-8 col-xs-8">
						<input type="text" class="form-control hidden" id="pb-user-signup-form-authcode-email" placeholder="인증코드 입력" name="authcode_email" required data-error="인증코드를 입력하여 주세요" maxlength="5">
						<input type="hidden" name="authcode_email_confirmed" value="N">
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-2 col-xs-4"">
						<button type="button" class="btn btn-default  btn-block btn-check-autcode btn-ajax" disabled>
							<span class="normal">인증하기</span>
							<span class="loading"><i class="fa fa-circle-o-notch fa-spin"></i></span>
						</button>
					</div>
				</div>

			<?php } ?>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="pb-user-signup-form-user-pass">비밀번호</label>
				<div class="col-sm-5 col-xs-12">
					<input type="password" class="form-control" id="pb-user-signup-form-user-pass" placeholder="비밀번호 입력" name="user_pass" required data-error="비밀번호를 입력하세요">
				</div>
				<div class="col-sm-5 col-xs-12">
					<div class="form-margin-xxs visible-xs"></div>
					<input type="password" class="form-control" id="pb-user-signup-form-user-pass-c" placeholder="비밀번호 다시입력" name="user_pass_c" required data-match="#pb-user-signup-form-user-pass" data-error="비밀번호를 확인하세요">
				</div>
				<div class="col-sm-10 col-xs-12 col-xs-offset-0 col-sm-offset-2">
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>
				</div>
			</div>
			

			<div class="form-div-border"></div>
	
		<?php if($need_auth_myself_){ ?>
			<input type="hidden" name="need_auth_myself" value="Y">
			<div class="form-group" data-auth-myself-group>
				<label class="col-sm-2 col-xs-12 control-label" >본인인증</label>
				<div class="col-sm-10 col-xs-12 ">
					<button type="button" class="btn btn-default btn-ajax btn-auth-myself">
						<span class="normal">인증요청</span>
						<span class="loading"><i class="fa fa-circle-o-notch fa-spin"></i></span>
						<span class="complete"><i class="fa fa-check"></i> 인증완료</span>
					</button>
				</div>
			</div>

			<div class="hidden-xs hidden" data-auth-myself-result-group>
				<div class="form-group">
					<label class="col-sm-2 col-xs-3 control-label">이름</label>
					<div class="col-sm-10 col-xs-9">
						<p class="form-control-static input-sm">김형연</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-xs-3 control-label">성별</label>
					<div class="col-sm-10 col-xs-9">
						<p class="form-control-static input-sm">남자</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-xs-3 control-label">휴대전화</label>
					<div class="col-sm-10 col-xs-9">
						<p class="form-control-static input-sm">010-2867-5791</p>
					</div>
				</div>
			</div>

			<div class="form-div-border"></div>
		<?php }else{ ?>
				<input type="hidden" name="need_auth_myself" value="N">

				<div class="form-group" >
					<label class="col-sm-2 col-xs-12 control-label" for="pb-user-signup-form-full_name">이름</label>
					<div class="col-sm-10 col-xs-12 ">
						<input type="text" class="form-control" id="pb-user-signup-form-full_name" placeholder="이름 입력" name="full_name" required data-error="이름을 입력하세요">
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
					
				</div>

				<div class="form-group" >
					<label class="col-sm-2 col-xs-12 control-label" >성별</label>
					<div class="col-sm-10 col-xs-12 ">
						<label class="radio-inline ">
							<input type="radio" name="gender" id="pb-user-signup-form-gender-00001" value="00001" required data-error="성별을 선택하세요"> 남자
						</label>
						<label class="radio-inline ">
							<input type="radio" name="gender" id="pb-user-signup-form-gender-00003" value="00003" required data-error="성별을 선택하세요"> 여자
						</label>
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
				</div>

				<div class="form-group" >
					<label class="col-sm-2 col-xs-12 control-label" for="pb-user-signup-form-birth_yyyymmdd">생년월일</label>
					<div class="col-sm-10 col-xs-12 ">
						<div class='input-group date' id='pb-user-signup-form-birth_yyyymmdd-wrap'>
							<input type="text" class="form-control" id="pb-user-signup-form-birth_yyyymmdd" placeholder="생년월일 입력" name="birth_yyyymmdd" required data-error="생년월일을 입력하세요">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>
					
				</div>

				<div class="form-group" >
					<label class="col-sm-2 col-xs-12 control-label" >휴대전화</label>
					<div class="col-sm-2 col-xs-4 col-sm-phone">
						<select class="form-control" name="phone1_1" data-error="앞자리를 선택하세요" required id="pb-user-signup-form-phone1_1">
							<option value="">-앞자리-</option>
							<?= pb_gcode_make_options('Z0003') ?>
						</select>
					</div>
					<div class="col-sm-4 col-xs-4 col-sm-phone">
						<input type="number" name="phone1_2" class="form-control" maxlength="4" data-error="중간자리를 입력하세요" required id="pb-user-signup-form-phone1_2">
					</div>
					<div class="col-sm-4 col-xs-4">
						<input type="number" name="phone1_3" class="form-control" maxlength="4" data-error="마지막자리를 입력하세요" required id="pb-user-signup-form-phone1_2">
					</div>
					<div class="col-sm-10 col-xs-12 col-xs-offset-0 col-sm-offset-2">
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
					</div>

				</div>
				
				<div class="form-div-border"></div>
	
		<?php }?>
			
			

			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label" >홍보수신</label>
				<div class="col-sm-10 col-xs-12 ">
					<label class="checkbox-inline "><input type="checkbox" name="mailing_recv_yn" value="Y" checked> 이메일 수신동의</label>
					<label class="checkbox-inline"><input type="checkbox" name="sms_recv_yn" value="Y"> SMS 수신동의</label>
					<small class="help-block">*필수정보 및 공지사항은 수신될 수 있습니다.</small>
				</div>
			</div>

			</div></div>		

			<div class="form-margin"></div>

			<div class="text-right">
				<button type="submit" class="btn btn-lg btn-primary">회원가입하기</button>
			</div>

		</div>

	</form>
	
</div></div>