<?php
	$need_auth_email_ = pb_user_need_auth_email();
	$need_auth_myself_ = pb_user_need_auth_myself();

	$user_data_ = pb_user(get_current_user_id());
	$birth_yyyymmdd_ = date('Ymd', strtotime($user_data_->birth_yyyymmdd));
?>
<div class="page-mypage-change-myinfo-frame"><form id="pb-user-change-myinfo-form" class="user-change-myinfo-form pb-border-form" autocomplete="off">

	<div class="panel panel-default"><div class="panel-body">

	<div class="form-group">
		<label class="col-sm-2 control-label">아이디</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?=$user_data_->user_login?></p>
			<div class="clearfix"></div>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">이메일</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?=$user_data_->user_email?></p>
			<div class="clearfix"></div>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label" for="pb-user-change-myinfo-form-user-pass">비밀번호</label>
		<div class="col-sm-10 col-xs-12">
			<a href="#" class="btn btn-default" data-toggle="modal" data-target="#pb-user-change-pass-modal">비밀번호변경</a>
		</div>
		
	</div>
	

	<div class="form-div-border"></div>

<?php if($need_auth_myself_){ ?>

	<div class="hidden-xs hidden" data-auth-myself-result-group>
		<div class="form-group">
			<label class="col-sm-2 col-xs-3 control-label">이름</label>
			<div class="col-sm-10 col-xs-9">
				<p class="form-control-static input-sm">김형연</p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 col-xs-3 control-label">성별</label>
			<div class="col-sm-10 col-xs-9">
				<p class="form-control-static input-sm">남자</p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 col-xs-3 control-label">휴대전화</label>
			<div class="col-sm-10 col-xs-9">
				<p class="form-control-static input-sm">010-2867-5791</p>
			</div>
		</div>
	</div>

	<div class="form-div-border"></div>
<?php }else{ ?>
		<input type="hidden" name="need_auth_myself" value="N">

		<div class="form-group" >
			<label class="col-sm-2 col-xs-12 control-label" for="pb-user-change-myinfo-form-full_name">이름</label>
			<div class="col-sm-10 col-xs-12 ">
				<input type="text" class="form-control" id="pb-user-change-myinfo-form-full_name" placeholder="이름 입력" name="full_name" required data-error="이름을 입력하세요" value="<?=$user_data_->full_name?>">
				<div class="help-block with-errors"></div>
				<div class="clearfix"></div>
			</div>
			
		</div>

		<div class="form-group" >
			<label class="col-sm-2 col-xs-12 control-label" >성별</label>
			<div class="col-sm-10 col-xs-12 ">
				<label class="radio-inline ">
					<input type="radio" name="gender" id="pb-user-change-myinfo-form-gender-00001" value="00001" required data-error="성별을 선택하세요" <?=checked($user_data_->gender, '00001')?> > 남자
				</label>
				<label class="radio-inline ">
					<input type="radio" name="gender" id="pb-user-change-myinfo-form-gender-00003" value="00003" required data-error="성별을 선택하세요" <?=checked($user_data_->gender, '00003')?> > 여자
				</label>
				<div class="help-block with-errors"></div>
				<div class="clearfix"></div>
			</div>
		</div>

		<div class="form-group" >
			<label class="col-sm-2 col-xs-12 control-label" for="pb-user-change-myinfo-form-birth_yyyymmdd">생년월일</label>
			<div class="col-sm-10 col-xs-12 ">
				<div class='input-group date' id='pb-user-change-myinfo-form-birth_yyyymmdd-wrap'>
					<input type="text" class="form-control" id="pb-user-change-myinfo-form-birth_yyyymmdd" placeholder="생년월일 입력" name="birth_yyyymmdd" required data-error="생년월일을 입력하세요" value="<?=$birth_yyyymmdd_?>">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
				
				<div class="help-block with-errors"></div>
				<div class="clearfix"></div>
			</div>
			
		</div>

		<div class="form-group" >
			<label class="col-sm-2 col-xs-12 control-label" >휴대전화</label>
			<div class="col-sm-2 col-xs-4 col-sm-phone">
				<select class="form-control" name="phone1_1" data-error="앞자리를 선택하세요" required id="pb-user-change-myinfo-form-phone1_1" >
					<option value="">-앞자리-</option>
					<?= pb_gcode_make_options('Z0003', $user_data_->phone1_1) ?>
				</select>
			</div>
			<div class="col-sm-4 col-xs-4 col-sm-phone">
				<input type="number" name="phone1_2" class="form-control" maxlength="4" data-error="중간자리를 입력하세요" required id="pb-user-change-myinfo-form-phone1_2" value="<?=$user_data_->phone1_2?>">
			</div>
			<div class="col-sm-4 col-xs-4">
				<input type="number" name="phone1_3" class="form-control" maxlength="4" data-error="마지막자리를 입력하세요" required id="pb-user-change-myinfo-form-phone1_2" value="<?=$user_data_->phone1_3?>">
			</div>
			<div class="col-sm-10 col-xs-12 col-xs-offset-0 col-sm-offset-2">
				<div class="help-block with-errors"></div>
				<div class="clearfix"></div>
			</div>

		</div>
		
		<div class="form-div-border"></div>

<?php }?>
	
	

	<div class="form-group">
		<label class="col-sm-2 col-xs-12 control-label" >홍보수신</label>
		<div class="col-sm-10 col-xs-12 ">
			<label class="checkbox-inline "><input type="checkbox" name="mailing_recv_yn" value="Y" <?=checked($user_data_->mailing_recv_yn, "Y")?> > 이메일 수신동의</label>
			<label class="checkbox-inline"><input type="checkbox" name="sms_recv_yn" value="Y" <?=checked($user_data_->sms_recv_yn, "Y")?> > SMS 수신동의</label>
			<small class="help-block">*필수정보 및 공지사항은 수신될 수 있습니다.</small>
		</div>
	</div>

	</div></div>		

	<div class="form-margin"></div>
	
	<div class="row">
		<div class="col-xs-5">
			<a href="#" class="text-danger" data-toggle="modal" data-target="#pb-user-leave-modal">회원탈퇴하기</a>
		</div>
		<div class="col-xs-7">
			<div class="text-right">
				<button type="submit" class="btn btn-lg btn-primary">회원정보수정</button>
			</div>
		</div>
	</div>
	
	
</form></div>

<div class="modal fade " tabindex="-1" role="dialog" id="pb-user-change-pass-modal">
	<div class="modal-dialog " role="document"><div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">비밀번호변경</h4>
		</div>
		<div class="modal-body"><form id="pb-user-change-pass-form" class="user-change-pass-form pb-border-form" >
			
			<div class="form-group" >
				<label class="col-sm-3 col-xs-12 control-label" for="pb-user-change-pass-form-current-pass">기존 비밀번호</label>
				<div class="col-sm-9 col-xs-12 ">
					<input type="password" class="form-control" id="pb-user-change-pass-form-current-pass" placeholder="기존 비밀번호 입력" name="current_pass" required data-error="기존 비밀번호를 입력하세요" >
					<p class="help-block"><small>비밀번호 변경을 위해서 기존 비밀번호입력이 필요합니다</small></p>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="form-div-border"></div>

			<div class="form-group" >
				<label class="col-sm-3 col-xs-12 control-label" for="pb-user-change-pass-form-new-pass">새로운 비밀번호</label>
				<div class="col-sm-9 col-xs-12 ">
					<input type="password" class="form-control" id="pb-user-change-pass-form-new-pass" placeholder="새로운 비밀번호 입력" name="new_pass" required data-error="새로운 비밀번호를 입력하세요" >
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="form-group" >
				<div class="col-sm-9 col-xs-12 col-sm-offset-3 col-xs-offset-0 ">
					<input type="password" class="form-control" id="pb-user-change-pass-form-new-pass-c" placeholder="비밀번호 다시입력" name="new_pass_c" required data-match="#pb-user-change-pass-form-new-pass" data-error="비밀번호를 확인하세요">
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<div class="form-div-border"></div>

			<div class="form-margin-xs"></div>

			<button type="submit" class="btn btn-primary btn-block btn-lg">변경하기</button>

		</form></div>
		
	</div></div>
</div>

<div class="modal fade " tabindex="-1" role="dialog" id="pb-user-change-myinfo-pass-modal">
	<div class="modal-dialog " role="document"><div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">비밀번호입력</h4>
		</div>
		<div class="modal-body"><form id="pb-user-change-myinfo-pass-form" class="user-change-myinfo-pass-form pb-border-form" >
			
			<div class="form-group" ><div class="col-xs-12">
				<div class="input-with-icon">
					<input type="password" class="form-control" id="pb-user-change-myinfo-pass-form-user-pass" placeholder="비밀번호 입력" name="user_pass" required data-error="비밀번호를 입력하세요" >
					<label for="pb-user-change-myinfo-pass-form-user-pass" class="icon fa fa-lock"></label>
				</div>
			
				<p class="help-block text-center"><small>회원정보 변경을 위해서 <br class="visible-xs" />비밀번호 입력이 필요합니다</small></p>
				<div class="clearfix"></div>
			</div></div>
			<div class="form-div-border"></div>
			
			<div class="form-margin-xs"></div>
			<button type="submit" class="btn btn-primary btn-block btn-lg">변경하기</button>

		</form></div>
		
	</div></div>
</div>

<div class="modal fade " tabindex="-1" role="dialog" id="pb-user-leave-modal">
	<div class="modal-dialog " role="document"><div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">회원탈퇴</h4>
		</div>
		<div class="modal-body"><form id="pb-user-leave-form" class="user-leave-form pb-border-form" data-home-url="<?=home_url()?>">
			
			<h5>보유정보폐기동의</h5>
			<div class="agreement-box">
				가입 시, 기입한 모든 정보(이메일,이름,연락처 등)가 폐기됩니다.
			</div>
			<div class="form-group"><div class="col-xs-12"><div class="checkbox with-top text-right">
				<label><input type="checkbox" name="agreement01" required data-error="보유정보폐기 동의가 필요합니다"> 보유정보폐기에 동의합니다.</label>
				<div class="help-block with-errors"></div>
				<div class="clearfix"></div>
			</div></div></div>
			<div class="form-div-border"></div>

			<div class="form-group" ><div class="col-xs-12">
				<div class="input-with-icon">
					<input type="password" class="form-control" id="pb-user-change-myinfo-pass-form-user-pass" placeholder="비밀번호 입력" name="user_pass" required data-error="비밀번호를 입력하세요" >
					<label for="pb-user-change-myinfo-pass-form-user-pass" class="icon fa fa-lock"></label>
				</div>
			
				<p class="help-block text-center"><small>회원탈퇴를 위해서 <br class="visible-xs" />비밀번호 입력하여 주세요</small></p>
				<div class="clearfix"></div>
			</div></div>
			<div class="form-div-border"></div>
			
			<div class="form-margin-xs"></div>
			<button type="submit" class="btn btn-black btn-block btn-lg">탈퇴하기</button>

		</form></div>
		
	</div></div>
</div>