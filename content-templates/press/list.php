<?php

class PB_press_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		$press_list_ = pb_press_list(array(
			'cinema_id' => pb_current_cinema_id(),
			'with_common' => true,
			'limit' => array($offset_, $per_page_),
		));

		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_press_list(array(
				'cinema_id' => pb_current_cinema_id(),
				'with_common' => true,
				'just_count' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $press_list_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	
	function columns(){
		return array(
			"press_title" => "보도자료",
			"reg_date" => "작성일자",
		);
	}

	function column_value($item_, $column_name_){

		$press_html_short_ = strip_tags(stripslashes($item_->press_html));		

		if(mb_strlen($press_html_short_) > 100){
			$press_html_short_ = mb_substr($press_html_short_,0, 100)."...";
		}
		

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){
			case "press_title" :
				ob_start();
?>
				

				<div class="post-title-row">
					<?php if(strlen($item_->feature_image_url)){ ?>
					<div class="feature-image-col">
						<a href="<?=pb_press_view_url($item_->ID)?>"><img src="<?=$item_->feature_image_url?>" class="feature-image"></a>
						<div class="clearfix"></div>
					</div>
					<?php } ?>
					<div class="post-title-col">
						<a href="<?=pb_press_view_url($item_->ID)?>" class="press-title"><?=stripslashes($item_->press_title)?></a>
						<div class="press-html"><?=$press_html_short_?></div>
					</div>
				</div>
				

<?php
				
				return ob_get_clean();

			case "reg_date" :
				ob_start();
?>
				
				<div class="date-item reg-date"><i class="icon fa fa-clock-o" aria-hidden="true"></i> <?=$item_->reg_date_ymd?></div>
				

<?php
					
				return ob_get_clean();

			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 언론보도가 없습니다.";	
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-press-table', function(){
		global $pb_board;
		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
	?>
	
	<div class="form-margin"></div>
	<div class="search-frame">
		<div class="row">
			<div class="col-xs-3 col-sm-8">
				<?php if(pb_is_admin()){ ?>
					<a href="<?=pb_adminpage_url("press-manage/add")?>" class="btn btn-default btn-sm">글쓰기</a>
				<?php } ?>
			</div>
			<div class="col-xs-9 col-sm-4">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">검색</button>
					</span>
				</div>
			</div>
		</div>
	</div>

	<?php
});

?>

<div class="pb-press-list-frame">
	
	<div class="pb-press-top-label">
		<h3 class="press-title">언론보도</h3>
	</div>
	<form id="pb-press-list-form" method="get" action="<?=pb_press_list_url()?>">
		
		
		<?php
			$list_table_ = new PB_press_table("pb-press-table", "pb-press-table");
			echo $list_table_->html();
		?>
	</form>

</div>