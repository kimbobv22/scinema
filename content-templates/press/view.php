<?php

	global $pb_press;

	$attachment_files_ = json_decode(stripslashes($pb_press->attachments), true);
	if(gettype($attachment_files_) !== "array"){
		$attachment_files_ = array();
	}
?>
<div class="pb-press-view-frame">
	
	<div class="pb-press-top-label">
		<h3 class="press-title">언론보도</h3>
		<a href="<?=pb_press_list_url()?>" class="list-btn btn btn-default btn-xs">목록으로</a>
	</div>
	<div class="press-content-wrap pb-press-view">
		<div class="pb-press-header">
			<h3 class="title"><?=stripslashes($pb_press->press_title)?></h3>
			<div class="subinfo">
				
				<div class="item">
					<i class="icon fa fa-clock-o" aria-hidden="true"></i><?=$pb_press->reg_date_ymd?>
				</div>
				
			</div>
		</div>

		<div class="pb-press-body">
			<?=stripslashes($pb_press->press_html)?>
		</div>
		
		<?php if(count($attachment_files_) > 0){ ?>
		<div class="pb-press-footer">
			<ul class="attachment-files">
			<?php foreach($attachment_files_ as $attachment_file_){ ?>
				<li><a href="<?=$attachment_file_['url']?>" target="_blank"><i class="fa fa-file-o" aria-hidden="true"></i> <?=$attachment_file_['name']?></i></a></li>
			<?php } ?>
			</ul>
		</div>
		<?php } ?>

		<?php if(pb_is_admin()){ ?>
		<div class="press-content-bottom-label text-right">
			<a href="<?=pb_adminpage_url("press-manage/edit/".$pb_press->ID)?>" class="btn btn-sm btn-default">수정</a>
		</div>
		<?php } ?>

	</div>
</div>