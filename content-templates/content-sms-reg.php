<?php
	
	$default_data_ = null;
	$has_registered_ = false;

	if(is_user_logged_in()){

		$default_data_ = pb_sms_reg_list(array(
			'cinema_id' => pb_current_cinema_id(),
			'user_id' => get_current_user_id(),
		));

		if(count($default_data_) > 0){
			$default_data_ = $default_data_[0];
			$has_registered_ = true;
		}else{

			$user_data_ = pb_user(get_current_user_id());


			$default_data_ = (object)array(
				'customer_name' => $user_data_->full_name,
				'gender' => $user_data_->gender,
				'phone1_1' => $user_data_->phone1_1,
				'phone1_2' => $user_data_->phone1_2,
				'phone1_3' => $user_data_->phone1_3,

				'birth_year' => substr($user_data_->birth_yyyymmdd, 0, 4),
			);
		}

	}

	if(empty($default_data_)){
		$default_data_ = (object)array(
			'customer_name' => null,
			'gender' => null,
			'phone1_1' => null,
			'phone1_2' => null,
			'phone1_3' => null,

			'birth_year' => null,
		);
	}
	
?>
<div class="page-sms-reg-frame"><form id="pb-sms-reg-form">
	
	<h3>SMS수신등록 <small>신작영화정보 등 다양한 정보를 받아볼 수 있습니다.</small></h3>

	<div class="panel panel-default">
		<div class="panel-body">

			<?php if($has_registered_){ ?>

				<h4>수신등록정보</h4>

				<div class="register-info-frame">
					<div class="register-item">
						<div class="subject">이름</div>
						<div class="content"><?=$default_data_->customer_name?></div>
					</div>
					<div class="register-item">
						<div class="subject">성별</div>
						<div class="content"><?=$default_data_->gender_name?></div>
					</div>
					<div class="register-item">
						<div class="subject">휴대전화번호</div>
						<div class="content"><?=$default_data_->phone1_1?>-<?=$default_data_->phone1_2?>-<?=$default_data_->phone1_3?></div>
					</div>
					<div class="register-item">
						<div class="subject">출생년도</div>
						<div class="content"><?=$default_data_->birth_year?></div>
					</div>
					<div class="register-item">
						<div class="subject">수신등록일자</div>
						<div class="content"><?=$default_data_->reg_date?></div>
					</div>
				</div>

			<?php }else{ ?>

			<h4>개인정보입력</h4>
			
			<div class="row">
				<div class="col-xs-12 col-sm-7">
					<label class="" for="pb-sms-reg-form-customer_name">이름</label>
					<div class="form-group" >
						<input type="text" class="form-control" id="pb-sms-reg-form-customer_name" placeholder="이름 입력" name="customer_name" required data-error="이름을 입력하세요" value="<?=$default_data_->customer_name?>" >
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-5">

					<label class="" for="pb-sms-reg-form-gender">성별</label>
					<div class="form-group" >
						

						<div class="col-sm-10 col-xs-12 ">
							<label class="radio-inline ">
								<input type="radio" name="gender" id="pb-sms-reg-form-gender-00001" value="00001" required data-error="성별을 선택하세요" <?=checked($default_data_->gender, '00001')?> > 남자
							</label>
							<label class="radio-inline ">
								<input type="radio" name="gender" id="pb-sms-reg-form-gender-00003" value="00003" required data-error="성별을 선택하세요" <?=checked($default_data_->gender, '00003')?> > 여자
							</label>
						</div>
						
						<div class="col-xs-12">
							<div class="help-block with-errors"></div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
						
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-xs-12 col-sm-7">
					<label class="" >휴대전화번호</label>
					<div class="form-group" >
						<div class="col-sm-4 col-xs-4 col-sm-phone">
							<select class="form-control" name="phone1_1" data-error="앞자리를 선택하세요" required id="pb-sms-reg-form-phone1_1">
								<option value="">-앞자리-</option>
								<?= pb_gcode_make_options('Z0003', $default_data_->phone1_1) ?>
							</select>
						</div>
						<div class="col-sm-4 col-xs-4 col-sm-phone">
							<input type="number" name="phone1_2" class="form-control" maxlength="4" data-error="중간자리를 입력하세요" required id="pb-sms-reg-form-phone1_2" value="<?=$default_data_->phone1_2?>">
						</div>
						<div class="col-sm-4 col-xs-4">
							<input type="number" name="phone1_3" class="form-control" maxlength="4" data-error="마지막자리를 입력하세요" required id="pb-sms-reg-form-phone1_2" value="<?=$default_data_->phone1_3?>">

						</div>
						<div class="col-xs-12">
							<div class="help-block with-errors"></div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>

					
				</div>
				<div class="col-xs-12 col-sm-5">
					<label class="" >출생년도</label>
					<select class="form-control" name="birth_year" data-error="출생년도를 선택하세요" required id="pb-sms-reg-form-birth_year">
						<option value="">-출생년도 선택-</option>

						<?php 
						
						$srt_year_ = strtotime('-5 year');
						$srt_year_ = date("Y", $srt_year_);
						
						for($row_index_=0; $row_index_ < 90; ++$row_index_){

							$srt_year_ -= 1;
						?>
							
							<option value="<?=$srt_year_?>" <?=selected($srt_year_, $default_data_->birth_year)?>><?=$srt_year_?></option>

						<?php } ?>
					</select>
				</div>
			</div>

			<hr>

			<h4>개인정보 수집 및 이용에 대한 안내</h4>
			<table class="table table-bordered privacy-info-table">
				<tbody>
					<tr class="target-purpose">
						<th>수집목적</th>
						<td>이용자 식별 및 정보발송</td>
					</tr>
					
					<tr class="target-entry">
						<th>수집항목</th>
						<td>휴대전화번호, 이름, 성별, 출생년도, 연락처</td>
					</tr>
					<tr class="retention-period">
						<th>보유기간</th>
						<td>수신해지 시 까지</td>
					</tr>
				</tbody>
			</table>
			<div class="form-group"><div class="col-xs-12"><div class="checkbox with-top text-right">
				<label><input type="checkbox" name="agreement02" required data-error="개인정보수집 동의가 필요합니다"> 개인정보수집에 동의합니다.</label>
				<div class="clearfix"></div>
			</div></div></div>

			<?php } ?>

		</div>

	</div>
	<div class="row ">
		<div class="col-sm-3 col-sm-push-9 col-xs-12 col-xs-push-0">

			<?php if($has_registered_){ ?>
				<a href="javascript:pb_sms_reg_deregister();" class="btn btn-danger btn-lg btn-block">수신해지</a>	
			<?php }else{ ?>
				<button type="submit" class="btn btn-primary btn-lg btn-block">수신등록</button>	
				

			<?php } ?>
		</div>
		
	</div>
			
	
</form></div>