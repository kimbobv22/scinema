<?php
	
	$area_id_ = isset($_GET['area_id']) ? $_GET['area_id'] : null; 
	$keyword_ = isset($_GET['keyword']) ? $_GET['keyword'] : null; 

	$t_area_list_ = pb_gcode_dtl_list("C0005");
	$area_list_ = null;

	if(strlen($area_id_)){
		$area_list_ = array();

		foreach($t_area_list_ as $row_data_){
			if($row_data_->code_did === $area_id_){
				$area_list_[] = $row_data_;				
			}
		}
	}else{
		$area_list_ = $t_area_list_;
	}

	if(is_subdomain_install()){
		$newdomain_ = '.' . preg_replace( '|^www\.|', '', get_network()->domain ).get_network()->path;
	} else {
		$newdomain_ = get_network()->domain.get_network()->path;
	}


?>
<div class="page-cinema-list-frame">

	<div class="cinema-list-frame">
		

	<form id="pb-cinema-list-form" method="get" action="<?=pb_cinema_list_url()?>">

		<div class="cinema-frame-row">
			<div class="map-col hidden-xs">
				<div class="korea-map-frame">
					<div id="pb-korea-map"  >
						<a href="#" class="link link-1001" data-location-link="1001">인천</a>
						<a href="#" class="link link-1011" data-location-link="1011">경기</a>
						<a href="#" class="link link-1021" data-location-link="1021">강원</a>
						<a href="#" class="link link-1031" data-location-link="1031">충북</a>
						<a href="#" class="link link-1041" data-location-link="1041">충남</a>
						<a href="#" class="link link-1051" data-location-link="1051">전북</a>
						<a href="#" class="link link-1061" data-location-link="1061">전남</a>
						<a href="#" class="link link-1071" data-location-link="1071">경북</a>
						<a href="#" class="link link-1081" data-location-link="1081">경남</a>
					</div>
				</div>
			</div>
			<div class="list-col">
				<div class="form-margin"></div>
				<div class="search-frame">
					<div class="row">
						<div class="col-xs-3 col-sm-3">
							<select class="form-control" name="area_id">
								<option value="">지역선택</option>
								<?php foreach($t_area_list_ as $area_data_){ ?>
									<option value="<?=$area_data_->code_did?>" <?=selected($area_data_->code_did, $area_id_)?>><?=$area_data_->code_dnm?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-xs-9 col-sm-9">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="" name="keyword" value="<?=$keyword_?>">
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit">검색</button>
								</span>
							</div>
						</div>
					</div>
				</div>
				<hr>

				<?php

					$has_count_ = false;

				foreach($area_list_ as $area_data_){

					$cinema_list_ = pb_cinema_list(array("area_id" => $area_data_->code_did, 'cinema_status' => '00001', 'cinema_type' => PB_CINEMA_CTG_NOR, "keyword" => $keyword_));

					if(count($cinema_list_) <= 0){
						 continue;
					}

					$has_count_ = true;
					?>
				<h3><?=$area_data_->code_dnm?></h3>
					
				
				<table class="table cinema-table">
				
					<tbody>
					<?php foreach($cinema_list_ as $cinema_data_){ ?>
								
								<tr>
									<td class="cinema-td-item">
										<div class="cinema-name"><?=$cinema_data_->cinema_name?></div>
										<div class="address">[<?=$cinema_data_->addr_postcode?>] <?=$cinema_data_->addr?> <?=$cinema_data_->addr_dtl?></div>
										<div class="phone-num"><?=$cinema_data_->phone1?></div>

										<a class="homepage" href="<?=get_site_url($cinema_data_->cinema_id)?>"><?=get_site_url($cinema_data_->cinema_id)?></a>

									</td>
								</tr>
							

					<?php } ?>
					</tbody>
				</table>

				<?php } 

					if($has_count_ <= 0){
						?> <div class="text-center help-block">검색된 영화관이 없습니다.</div> <?php
					}

				?>
			</div>
		</div>
		
		
		
	</form>
			
			
			
	</div>
</div>

