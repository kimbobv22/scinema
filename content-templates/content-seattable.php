<?php
	
	$cinema_data_ = pb_cinema(pb_current_cinema_id());
	$cinema_seat_data_list_ = pb_cinema_seat_list(array(
		'cinema_id' => $cinema_data_->cinema_id,
		'status' => '00001',
	));

	$seatitem_width_ = 46;
?>
<div class="page-seattable-frame <?=count($cinema_seat_data_list_) > 0 ? "has-seattable" : "no-seattable" ?>">

	
	<?php if(count($cinema_seat_data_list_) > 0){

		foreach($cinema_seat_data_list_ as $row_data_){

			$seattable_info_data_ = $row_data_->seat_data;

			$schema_ = $seattable_info_data_["schema"];

			$seattable_data_ = $seattable_info_data_['seattable'];
			$seattable_total_count_ = $schema_['column'];

			$screen_data_ = $seattable_info_data_['screen'];
			$screen_margin_ = $screen_data_['margin'];
			$margin_unit_ = (int)((1 / $seattable_total_count_) * 100);
		?>

		<div class="seattable-info-frame">
			<h3 class="seat-name"><?=$row_data_->seat_name?> <small><?=$row_data_->seat_desc?></small></h3>

			<?php if($row_data_->replace_image_yn !== "Y"){ ?>

				<div class="pb-seattable"><div class="seattable-wrap">
				<div class="screen-frame" style="width:<?=$seattable_total_count_*$seatitem_width_?>px;"><div class="screen" style="margin-left: <?=$screen_margin_['left'] * $margin_unit_?>%; margin-right: <?=$screen_margin_['right'] * $margin_unit_?>%;">
					<span class="text">SCREEN</span>
				</div></div>

				<div class="seattable-row-list">
					
					<?php foreach($seattable_data_ as $row_data_){ ?>
						<div class="seattable-row">

							<?php foreach($row_data_ as $col_data_){ ?>
								<div class="seattable-col">
									<div class="seat-item seat-type-<?=$col_data_['seat_type']?>">
										<div class="seat-name"><?=$col_data_['seat_name']?></div>
									</div>
								</div>
							<?php } ?>
							

						</div>
					<?php } ?>

				</div>
			</div></div>
			<small class="help-block">*좌석배치도가 클 경우 상화좌우로 스크롤하여 확인할 수 있습니다.</small>

			<?php }else{ ?>
				<img src="<?=$row_data_->replace_image_url?>" class="seattable-image">
			<?php } ?>

		</div>
			
		<?php }
		
	}else{ ?>
		<div class="no-seattable">
			<i class="fa fa-exclamation-triangle icon"></i>
			<h3 class="title">좌석배치도 준비중</h3>
			<div class="subject">현재 좌석배치도를 준비중입니다. 이용에 불편드려 죄송합니다.</div>
		</div>
	<?php } ?>

</div>