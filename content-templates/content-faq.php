<?php
	
	$faq_list_ = pb_faq_list(array(
		'cinema_id' => pb_current_cinema_id(),
	));
?>
<div class="page-faq-frame">
	
		
<div class="panel-group" id="pb-faq-list-panel-group" role="tablist" aria-multiselectable="true">
	
	<?php if(count($faq_list_) > 0){

		foreach($faq_list_ as $row_data_){ ?>

	<div class="panel panel-default faq-panel">
		<div class="panel-heading" role="tab" id="faq-item-<?=$row_data_->ID?>">
			<h4 class="panel-title">
				<span class="icon">Q</span>
				<a role="button" data-toggle="collapse" data-parent="#pb-faq-list-panel-group" href="#faq-item-<?=$row_data_->ID?>-body" aria-expanded="true" aria-controls="collapseOne">
					<?=$row_data_->faq_title?>
				</a>
			</h4>
		</div>
		<div id="faq-item-<?=$row_data_->ID?>-body" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				<?=stripslashes(nl2br($row_data_->faq_desc))?>
			</div>
		</div>
	</div>

		<?php }

	}else{ ?>
	<?php } ?>

</div>


</div>

<?php if(pb_is_admin()){


	if(pb_cinema_current_is_head_office()){ ?>
		<a href="<?=pb_adminpage_url("faq-manage/edit/".pb_current_cinema_id())?>" class="btn btn-sm btn-default">수정하기</a>
	<?php }else{ ?>
		<a href="<?=pb_adminpage_url("faq-manage")?>" class="btn btn-sm btn-default">수정하기</a>
	<?php }

?>

<?php } ?>