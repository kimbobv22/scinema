<?php

	
	$sidemenu_data_ = pb_sidemenu_data(pb_current_sidemenu_id());
	

	?>
		<h3 class="sidemenu-title"><?=$sidemenu_data_['title']?></h3>
		
	<ul class="sidemenu-list">

	<?php

		foreach($sidemenu_data_['menu_list'] as $row_data_){


			?>
			<li class="<?=$row_data_['active'] ? "active" : ""?>">
				<a href="<?=$row_data_['url']?>"><?=$row_data_['title']?>
				<i class="icon fa fa-angle-right" aria-hidden="true"></i>
				
			</a>
				
			<?php 

			if(isset($row_data_['children']) && count($row_data_['children']) > 0){ ?>

				<ul class="sidemenu-list child-list">
			<?php foreach($row_data_['children'] as $sub_row_data_){ ?>
					<li class="<?=$sub_row_data_['active'] ? "active" : ""?>"><a href="<?=$sub_row_data_['url']?>">
						<?=$sub_row_data_['title']?>
						<i class="icon fa fa-angle-right" aria-hidden="true"></i>
					</a></li>
			<?php } ?>

				</ul>
			<?php }

				

			?>


			</li>
		<?php }	

?> </ul> <?php

?>