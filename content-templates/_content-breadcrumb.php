<?php
	

	$sidemenu_data_ = pb_sidemenu_data(pb_current_sidemenu_id());
	$mainmenu_list_ = pb_mainmenu_list();

	if($sidemenu_data_['type'] === PB_SIDEMENU_TYPE_SUB){

		$sidemenu_path_ = $sidemenu_data_['sidemenu_path'];

		// print_r($sidemenu_data_['menu_list']);

		// $prev_children_ = count($sidemenu_data_) > 1 ? null : $sidemenu_data_['menu_list'];
		$prev_children_ = null;
		foreach($sidemenu_path_ as $path_item_){
			// $path_data_ = pb_sidemenu_data($path_item_->object_id, false);

			$prev_children_ = array();

			if($path_item_->menu_item_parent > 0){

				foreach($mainmenu_list_ as $mainmenu_item_){
					if($mainmenu_item_->menu_item_parent == $path_item_->menu_item_parent){
						$prev_children_[] = $mainmenu_item_;
					}
				}
			}

			if(count($prev_children_) > 0){ ?>

			<li class="dropdown">
				<a href="#" data-toggle="dropdown"><?=$path_item_->title?> <i class="icon fa fa-angle-down" aria-hidden="true"></i></a>
				<ul class="dropdown-menu">
					<?php foreach($prev_children_ as $child_data_){ ?>
						<li><a href="<?=$child_data_->url?>"><?=$child_data_->title?></a></li>
					<?php } ?>
				</ul>

			</li>

			<?php }else{ ?>
				<li class=""><a href="<?=$path_item_->url?>"><?=$path_item_->title?></a></li>
			<?php }
		}

	?>
		
	<?php  }else{

		$current_menu_item_ = pb_current_sidemenu_item();
	 ?>
		
		<li class=""><?=$current_menu_item_->title?></li>

	<?php }

?>