<?php
	
	//$cinema_data_ = pb_cinema(pb_current_cinema_id());
?>
<div class="page-location-frame">
	
	
	<div class="maps-frame">
		<div id="map" style="width:100%;height:350px;"></div>
	</div>

	<script>
	var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
	    mapOption = { 
	        center: new daum.maps.LatLng(37.47605, 126.88313), // 지도의 중심좌표
	        level: 3 // 지도의 확대 레벨
	    };

	// 지도를 표시할 div와  지도 옵션으로  지도를 생성합니다
	var map = new daum.maps.Map(mapContainer, mapOption); 

	// 지도에 마커를 생성하고 표시한다
	var marker = new daum.maps.Marker({
	    position: new daum.maps.LatLng(37.47605, 126.88313), // 마커의 좌표
	    map: map // 마커를 표시할 지도 객체
	});
	</script>

	<div class="form-margin"></div>

	<table class="table table-bordered">
		
		<tr>
			<TH class="text-center">주소</TH>
			<td>
				<strong>[08589]</strong> 서울시 금천구 가산디지털1로 119 SK트윈테크타워 B동 507호 작은영화관사회적협동조합
			</td>
		</tr>
		<tr>
			<TH class="text-center">연락처</TH>
			<td>
				070-4820-5719
			</td>
		</tr>
	</table>


</div>