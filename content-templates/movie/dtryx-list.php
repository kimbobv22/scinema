<?php
	class PB_movie_list_table extends PBListGrid{

        function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 100;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;
		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
        global $pb_movie_list_action;
		$current_cinema_ = pb_current_cinema();
		
		global $cinema_id_;
		if(pb_cinema_current_is_head_office()){
			$cinema_id_ = 'all';
		}
		else {
			$cinema_id_ = $current_cinema_->xticket_ref_code;
		}
		$movie_open_list_ = null;
		$movie_open_count_ = 0;

		//현재상영작 
        if($pb_movie_list_action === PB_MOVIE_SCREENING_LIST_SLUG){
			$movie_open_list_ = pb_dtryx_movie_now_list($cinema_id_,'small','');
            $movie_open_count_ = pb_dtryx_movie_now_list($cinema_id_,'small','count');

        //상영예정작            
		}else if($pb_movie_list_action === PB_MOVIE_SCHEDULED_LIST_SLUG){
			$movie_open_list_ = pb_dtryx_movie_plan_list($cinema_id_,'small','');
			$movie_open_count_ = pb_dtryx_movie_plan_list($cinema_id_,'small','count');
		}

		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,		
			'total_count' => $movie_open_count_,
			"hide_pagenav" => true,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,
			'items' => $movie_open_list_,
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function render_item($screen_data_){
	?>
		    <div class="pb-movie-item light list-in-xs line-in-xs-shadow">
				<div class="col poster-image-col">
					<a class="poster-image light" href="<?=pb_movie_view_url($screen_data_["ID"])?>">
						<?php if(strlen($screen_data_["image_url"])){ ?>
						<img src="<?=$screen_data_["image_url"]?>" class="image">
						<?php } ?>
					</a>
						
				</div>
				<div class="col poster-info">
					<div class="movie-info">
						<a class="movie-name" href="<?=pb_movie_view_url($screen_data_["ID"])?>">
							<?php pb_movie_draw_level_badge($screen_data_["level"]); ?>
							<?=$screen_data_["movie_name"]?></a>
					</div>
					<div class="sub-info">
						<div class="sub-item left"><?=$screen_data_["open_date"]?> 개봉</div>
					</div>					
				</div>
			</div>
			<div class="clearfix"></div>		
		<?php
	}
	function noitemdata(){
		return "검색된 영화가 없습니다.";	
	}
}

global $pb_movie_list_action;
$movie_title_ = "";
if($pb_movie_list_action === PB_MOVIE_SCREENING_LIST_SLUG){
	$movie_title_ = "현재상영작";
	$form_url_ = pb_movie_screening_list_url();
}else{
	$movie_title_ = "상영예정작";
	$form_url_ = pb_movie_scheduled_list_url();
}
?>

<div class="page-movie-list-frame">
	<div class="title-wrap">
		<h3 class="title"><?=$movie_title_?></h3>
	</div>
	<div class="movie-list-frame">
    	<form id="pb-movie-list-form" method="get" action="<?=$form_url_?>">
	    <?php
			$list_table_ = new PB_movie_list_table("pb-movie-list-table", "pb-movie-list-table");
			echo $list_table_->html();
		?>
	    </form>
	</div>
</div>