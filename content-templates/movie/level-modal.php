<div class="modal fade" tabindex="-1" role="dialog" id="pb-movie-level-info-popup"><div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">영화등급안내</h4>
		</div>
		<div class="modal-body">
			<table class="table">
				<tbody>
					<tr>
						<th width="75px" class="text-center">
							<span class="movie-rate-badge movie-rate-badge-all large"></span>
						</th>
						<td>
							모든 연령의 관람객이 관람할 수 있는 영화 
						</td>

					</tr>
					<tr>
						<th class="text-center">
							<span class="movie-rate-badge movie-rate-badge-00001 large"></span>
						</th>
						<td>
							만 12세 미만의 관람객은 관람할 수 없는 영화. 보호자 동반시 12세 미만 관람가능 
						</td>

					</tr>
					<tr>
						<th class="text-center">
							<span class="movie-rate-badge movie-rate-badge-00003 large"></span>
						</th>
						<td>
							만 15세 미만의 관람객은 관람할 수 없는 영화. 보호자 동반시 15세 미만 관람가능 
						</td>

					</tr>
					<tr>
						<th class="text-center">
							<span class="movie-rate-badge movie-rate-badge-00005 large"></span>
						</th>
						<td>
							만 18세 미만의 관람객은 관람할 수 없는 영화. 보호자 동반하여도 18세 미만 (영, 유아 포함)은 관람불가. <br/><small>영화 및 비디오물 진흥에 관한 법률 제 29조와 초, 중등 교육법 제 2조 규정에 의해 만 18세 이상 고등학생은 관람할 수 없음</small>
						</td>

					</tr>
				</tbody>
			</table>

		</div>
	</div>
</div></div>