<?php  
    $current_cinema_ = pb_current_cinema();
		if(pb_cinema_current_is_head_office()){
			$cinema_id_ = 'all';
		}
		else {
			$cinema_id_ = $current_cinema_->xticket_ref_code;
		}
    $screen_id_ = get_query_var("_pb_movie_view_screen_id"); 
		global $pb_moive_screen_data_;
		
		if(is_user_logged_in()){
			$user_data_ = pb_user(get_current_user_id());
			$s_user_id_ = $user_data_->user_login;
			$s_user_name_ = $user_data_->full_name;
			$s_phone_num_ = $user_data_->phone1_1.$user_data_->phone1_2.$user_data_->phone1_3;
			$s_user_birthday_ = $user_data_->birth_yyyymmdd;
			if(isset($s_user_birthday_)) $s_user_birthday_ = substr($s_user_birthday_,2,8);
			else $s_user_birthday_ = "";
			$s_user_idx_ = $user_data_->ID;
		}
		else {
			$s_user_id_ = "";
			$s_user_name_ = "";
			$s_phone_num_ = "";
			$s_user_birthday_ = "";
			$s_user_idx_ = "";
		}
?>
<div class="pb-movie-view-frame">
	<div class="title-wrap">
		<h3 class="title">영화정보</h3>
		<div class="movie-level-info">
			<a class="text-gray movie-level-info-btn" data-toggle="modal" href="#" data-target="#pb-movie-level-info-popup">
				<i class="fa fa-info-circle"></i> 영화등급안내
			</a>
		</div>
	</div>

	<div class="poster-image-wrap">
		<div class="col col-poster-image">
			<div class="poster-image light">
				<?php if($pb_moive_screen_data_["image_url"]){ ?>
					<img src="<?=$pb_moive_screen_data_["image_url"]?>" class="image">
				<?php } ?>

			</div>
		</div>
		<div class="col col-movie-info">
			<h3 class="movie-title"><?php pb_movie_draw_level_badge($pb_moive_screen_data_["level"], "large"); ?> <?=$pb_moive_screen_data_["movie_name"]?></h3>

			<div class="subinfo-list">
				<div class="subinfo-item">
					<div class="subject">개봉일</div>
					<div class="content-text"><?=$pb_moive_screen_data_["open_date"]?></div>
				</div>
				<div class="subinfo-item">
					<div class="subject">감독</div>
					<div class="content-text"><?=$pb_moive_screen_data_["director"]?></div>
				</div>
				<div class="subinfo-item">
					<div class="subject">배우</div>
					<div class="content-text"><?=$pb_moive_screen_data_["main_actors"]?></div>
				</div>

				<div class="subinfo-item">
					<div class="subject">장르</div>
					<div class="content-text"><?=$pb_moive_screen_data_["genre"]?></div>
				</div>

				<div class="subinfo-item">
					<div class="subject">러닝타임</div>
					<div class="content-text"><?=$pb_moive_screen_data_["running_time"]?> 분</div>
				</div>

				<div class="subinfo-item">
					<div class="subject">줄거리</div>
					<div class="content-text">
                    <br>
                    <?=stripslashes($pb_moive_screen_data_["story"])?>
                    </div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="form-margin-lg"></div>
	<div class="title-wrap">
		<h3 class="title">상영시간표</h3>
	</div>
	 <?php pb_dtryx_draw_timetable(); ?>
	<!-- <small class="help-block text-right">*예매/취소 가능시간은 상영시작 20분전 까지</small> -->
</div>

<?php get_template_part('content-templates/movie/level-modal');	 ?>

<script>
  var RegionCd = 'all';             /*all:전체*/

  /*영화관정보*/
  var CinemaCd = '<?=$cinema_id_?>';			/*영화관정보가 있을 경우엔 세팅*/
  var MovieCd  = '<?=$screen_id_?>';      /*선택영화코드*/

	/*회원로그인정보 세팅부분*/
  DTRYX.Common.MemberNm     = '<?=$s_user_name_?>';       /*회원이름*/
  DTRYX.Common.MobileNoFull = '<?=$s_phone_num_?>';       /*회원연락처*/
  DTRYX.Common.BirthdayDT   = '<?=$s_user_birthday_?>';   /*회원생년월일*/
  DTRYX.Common.MemberGuID   = '<?=$s_user_id_?>';       /*회원고유아이디*/
  DTRYX.Common.PublicID     = '<?=$s_user_idx_?>';		/*IPIN/CI값 없을 경우 공백*/
  initTimetable(RegionCd, CinemaCd, MovieCd);
</script>