<?php
	
	$pb_movie_list_action = get_query_var("_pb_movie_list_action");

	$current_title_ = null;

	if($pb_movie_list_action === PB_MOVIE_SCREENING_LIST_SLUG){
		$current_title_ = "현재상영작";
	}else if($pb_movie_list_action === PB_MOVIE_SCHEDULED_LIST_SLUG){
		$current_title_ = "상영예정작";
	}else if($pb_movie_list_action === PB_MOVIE_VIEW_SLUG){
		$current_title_ = "상세정보";
	}


?>

<li class="">영화정보</li>
<li class="dropdown">
	<a href="#" data-toggle="dropdown"><?=$current_title_?> <i class="icon fa fa-angle-down" aria-hidden="true"></i></a>
	<ul class="dropdown-menu">
		<li><a href="<?=pb_movie_screening_list_url()?>">현재상영작</a></li>
		<li><a href="<?=pb_movie_scheduled_list_url()?>">상영예정작</a></li>
	</ul>

</li>


<?php

?>