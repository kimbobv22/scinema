<?php
	global $pb_moive_screen_data;

?>
<div class="pb-movie-view-frame">
	
	<div class="title-wrap">
		<h3 class="title">영화정보</h3>
		<div class="movie-level-info">
			<a class="text-gray movie-level-info-btn" data-toggle="modal" href="#" data-target="#pb-movie-level-info-popup">
				<i class="fa fa-info-circle"></i> 영화등급안내
			</a>
		</div>
	</div>

	<div class="poster-image-wrap">
		<div class="col col-poster-image">
			<div class="poster-image light">
				<?php if($pb_moive_screen_data->image_url){ ?>
					<img src="<?=$pb_moive_screen_data->image_url?>" class="image">
				<?php } ?>

			</div>
		</div>
		<div class="col col-movie-info">
			<h3 class="movie-title"><?php pb_movie_draw_level_badge($pb_moive_screen_data->level, "large"); ?> <?=$pb_moive_screen_data->movie_name?></h3>

			<div class="subinfo-list">
				<div class="subinfo-item">
					<div class="subject">개봉일</div>
					<div class="content"><?=$pb_moive_screen_data->open_date_ymd?></div>
				</div>
				<div class="subinfo-item">
					<div class="subject">감독</div>
					<div class="content"><?=$pb_moive_screen_data->director?></div>
				</div>
				<div class="subinfo-item">
					<div class="subject">배우</div>
					<div class="content"><?=$pb_moive_screen_data->main_actors?></div>
				</div>

				<div class="subinfo-item">
					<div class="subject">장르</div>
					<div class="content"><?=$pb_moive_screen_data->genre?></div>
				</div>

				<div class="subinfo-item">
					<div class="subject">러닝타임</div>
					<div class="content"><?=$pb_moive_screen_data->running_time?> 분</div>
				</div>

				<div class="subinfo-item">
					<div class="subject">줄거리</div>
					<div class="content"><?=stripslashes($pb_moive_screen_data->story)?></div>
				</div>
				<hr>
				<div class="subinfo-item">
					<div class="subject"></div>
					<div class="content">
						<a href="http://ticket.yd21.go.kr/rsvc/rsv_mv.html?b_id=rainbow" target="_blank" class="btn btn-lg btn-primary">예매하러가기</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- <iframe src="http://ticket.yd21.go.kr/rsvc/rsv_mv.html?b_id=rainbow" width="100%;"></iframe> -->

</div>

<?php get_template_part('content-templates/movie/level-modal');	 ?>