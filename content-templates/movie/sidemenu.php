<?php
	
	$sidemenu_data_ = pb_sidemenu_data(get_the_ID());
	$pb_movie_list_action = get_query_var("_pb_movie_list_action");

	?>
		<h3 class="sidemenu-title">영화정보</h3>
		
	<ul class="sidemenu-list">

		<li class="<?=$pb_movie_list_action === PB_MOVIE_SCREENING_LIST_SLUG ? "active" : ""?>">
				<a href="<?=pb_movie_screening_list_url()?>">현재상영작
				<i class="icon fa fa-angle-right" aria-hidden="true"></i>
			</a>
		</li>
		<li class="<?=$pb_movie_list_action === PB_MOVIE_SCHEDULED_LIST_SLUG ? "active" : ""?>">
				<a href="<?=pb_movie_scheduled_list_url()?>">상영예정작
				<i class="icon fa fa-angle-right" aria-hidden="true"></i>
			</a>
		</li>
	</ul> <?php

?>