<?php  
    $current_cinema_ = pb_current_cinema();
    $cinema_id_ = $current_cinema_->xticket_ref_code; 

		if(is_user_logged_in()){
			$user_data_ = pb_user(get_current_user_id());
			$s_user_id_ = $user_data_->user_login;
			$s_user_name_ = $user_data_->full_name;
			$s_phone_num_ = $user_data_->phone1_1.$user_data_->phone1_2.$user_data_->phone1_3;
			$s_user_birthday_ = $user_data_->birth_yyyymmdd;
			if(isset($s_user_birthday_)) $s_user_birthday_ = substr($s_user_birthday_,2,8);
			else $s_user_birthday_ = "";
			$s_user_idx_ = $user_data_->ID;
		}
		else {
			# code...
			$s_user_id_ = "";
			$s_user_name_ = "";
			$s_phone_num_ = "";
			$s_user_birthday_ = "";
			$s_user_idx_ = "";
		}
?>
<div class="pb-movie-timetable-frame">
	<div class="title-wrap">
		<h3 class="title">상영시간표</h3>
	</div>
	 <?php pb_dtryx_draw_timetable(); ?>
</div>

<script>
  var RegionCd = 'all';             /*all:전체*/ 
  var CinemaCd = '<?=$cinema_id_?>';		/*영화관정보가 있을 경우엔 세팅*/
  var MovieCd  = '';         /*선택영화코드*/

	/*회원로그인정보 세팅부분*/
  DTRYX.Common.MemberNm     = '<?=$s_user_name_?>';       /*회원이름*/
  DTRYX.Common.MobileNoFull = '<?=$s_phone_num_?>';       /*회원연락처*/
  DTRYX.Common.BirthdayDT   = '<?=$s_user_birthday_?>';       /*회원생년월일*/
  DTRYX.Common.MemberGuID   = '<?=$s_user_id_?>';       /*회원고유아이디*/
  DTRYX.Common.PublicID     = '<?=$s_user_idx_?>';		/*IPIN/CI값 없을 경우 공백*/
  initTimetable(RegionCd, CinemaCd, MovieCd);
</script>