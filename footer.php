<?php do_action('pb_body_container_after'); ?>
</div></div>

<footer class="pb-footer" id="pb-footer"><div class="container">
	

	<div class="footer-container-row"><div class="row">
		<div class="col-md-3  col-sm-12 col-xs-12">
			<img src="<?=PB_THEME_DIR_URL?>img/footer-logo.png" class="footer-logo">
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12">
			<div class="footer-menu-wrap">
				<ul class="footer-menu">
					<li><a href="#">광고문의</a></li>
					<li><a href="#">개인정보처리방침</a></li>
					<li><a href="<?=pb_email_policy_url()?>">이메일주소무단수집거부</a></li>
					<li><a href="<?=admin_url()?>">업무지원시스템</a></li>
				</ul>
				<div class="dropdown footer-menu-select">
					<button class="btn btn-default dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					기타메뉴
					<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li><a href="#">광고문의</a></li>
						<li><a href="#">개인정보처리방침</a></li>
						<li><a href="<?=pb_email_policy_url()?>">이메일주소무단수집거부</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?=admin_url()?>">업무지원시스템</a></li>
					</ul>
				</div>
			</div>

			<div class="company-info-wrap">
				<ul>
					<li>작은영화관사회적협동조합 | 070-4820-5719 02-6008-6001(FAX)</li>
					<li>08506 서울시 금천구 가산디지털1로 131 BYC 하이시티 B동 703호</li>
					<li>개인정보관리 책임자 나하나 | 이메일 hnzzang@scinema.org</li>
					<li>Copyrightⓒ 2016 작은영화관 사회적협동조합 All Rights Reserved</li>
				</ul>
				
			</div>
		</div>
		<div class="col-md-3 col-sm-12 col-xs-12 "><div class="footer-logo-wrap">
			<img src="<?=PB_THEME_DIR_URL?>img/footer-logo.png" class="footer-logo-mobile">
			<img src="<?=PB_THEME_DIR_URL?>img/footer-social-enterprise-logo.png" class="footer-social-enterprise-logo">
		</div></div>

	</div></div>

</div></footer>
<?php wp_footer(); ?>
</body>
</html>