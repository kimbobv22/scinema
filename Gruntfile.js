module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify-es');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  
  grunt.initConfig({
    
    clean: {
      dist: [
        'lib/dist',
        'lib/dev/css',
      ]
    },

    concat : {
       'defaults-main' : {
        src: [
          'lib/dev/concat-lib/src/defaults/underscore.js',
          'lib/dev/concat-lib/src/defaults/moment.js',
          'lib/dev/concat-lib/src/defaults/cryptico.js',
          

          'lib/dev/concat-lib/src/defaults/bootstrap.js',
          'lib/dev/concat-lib/src/bootstrap/bootstrap-validator.js',
          'lib/dev/concat-lib/src/bootstrap/bootstrap-number-input.js',
          'lib/dev/concat-lib/src/bootstrap/bootstrap-datetimepicker.js',
          'lib/dev/concat-lib/src/bootstrap/bootstrap-multiselect.js',
          

          'lib/dev/concat-lib/src/defaults/wnumb.js',
          'lib/dev/concat-lib/src/defaults/aos.js',
          'lib/dev/concat-lib/src/defaults/colors.js',
          

          // 'lib/dev/concat-lib/src/defaults/load-image.js',
          // 'lib/dev/concat-lib/src/defaults/canvas-to-blob.js',
          
        ],
        dest: 'lib/dev/concat-lib/dist/defaults-main.js'
      },
      'defaults-admin' : {
        src: [
          'lib/dev/concat-lib/src/defaults/underscore.js',
          'lib/dev/concat-lib/src/defaults/moment.js',
          'lib/dev/concat-lib/src/defaults/cryptico.js',
          'lib/dev/concat-lib/src/defaults/wnumb.js'
        ],
        dest: 'lib/dev/concat-lib/dist/defaults-admin.js'
      },
      
      'jquery-default-plugins-main' : {
        src: [

          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.cookie.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.blockUI.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.swiper.js',
          // 'lib/dev/concat-lib/src/jquery-default-plugins/jquery.masonry.js',\
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.selectize.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.handsontable.js',

          
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.ui.widget.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.iframe-transport.js',

          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.fileupload/jquery.fileupload.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.fileupload/jquery.fileupload-process.js',
          
          // 'lib/dev/concat-lib/src/jquery-default-plugins/jquery.colorpicker.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.minicolors.js',
          
          
        ],
        dest: 'lib/dev/concat-lib/dist/jquery-default-plugins-main.js'
      },

      'jquery-default-plugins-admin' : {
        src: [

          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.cookie.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.blockUI.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.easytabs.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.swiper.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.magnific.js',

          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.validate/jquery.validate.js',
          'lib/dev/concat-lib/src/jquery-default-plugins/jquery.validate/jquery.validate.add.js',

        ],
        dest: 'lib/dev/concat-lib/dist/jquery-default-plugins-admin.js'
      },
      'pb-main' : {
        src: [
          'lib/dev/concat-lib/src/pb/modules/pb.mainmodule.js',
          'lib/dev/concat-lib/src/pb/modules/pb.crypt.js',
          'lib/dev/concat-lib/src/pb/modules/pb.utils.js',
          'lib/dev/concat-lib/src/pb/modules/pb.listtable.js',
          'lib/dev/concat-lib/src/pb/modules/pb.listgrid.js',
          'lib/dev/concat-lib/src/pb/modules/pb.address.js',
          'lib/dev/concat-lib/src/pb/modules/pb.fileupload.js',
          'lib/dev/concat-lib/src/pb/modules/pb.fileupload.imageuploader.js',
          'lib/dev/concat-lib/src/pb/modules/pb.fileupload.fileuploader.js',
          'lib/dev/concat-lib/src/pb/modules/pb.fileupload.slideuploader.js',
          'lib/dev/concat-lib/src/pb/modules/pb.fileupload.clientslideruploader.js',
          
          'lib/dev/concat-lib/src/pb/modules/pb.user.js',
          'lib/dev/concat-lib/src/pb/modules/pb.movie.js',
          'lib/dev/concat-lib/src/pb/modules/pb.movie.timetable.js',

          'lib/dev/concat-lib/src/pb/pb.main.js',

        ],
        dest: 'lib/dev/concat-lib/dist/pb-main.js'
      },
      'pb-admin' : {
        src: [
          'lib/dev/concat-lib/src/pb/modules/pb.mainmodule.js',
          'lib/dev/concat-lib/src/pb/modules/pb.crypt.js',
          'lib/dev/concat-lib/src/pb/modules/pb.utils.js',
          'lib/dev/concat-lib/src/pb/modules/pb.listtable.js',
          'lib/dev/concat-lib/src/pb/modules/pb.address.js',

          'lib/dev/concat-lib/src/pb/pb.admin.js',

        ],
        dest: 'lib/dev/concat-lib/dist/pb-admin.js'
      },
      'concat-all-main' : {
        src: [
          'lib/dev/concat-lib/dist/defaults-main.js',
          'lib/dev/concat-lib/dist/jquery-default-plugins-main.js',
          'lib/dev/concat-lib/dist/pb-main.js',
        ],
        dest: 'lib/dev/comp-lib/all-main.js'
      },
       'concat-all-admin' : {
        src: [
          'lib/dev/concat-lib/dist/defaults-admin.js',
          'lib/dev/concat-lib/dist/jquery-default-plugins-admin.js',
          'lib/dev/concat-lib/dist/pb-admin.js',
        ],
        dest: 'lib/dev/comp-lib/all-admin.js'
      }

    },
    
    uglify: {
      build: {
        files: [{
          expand: true,
          cwd: 'lib/dev',
          src: [
            '**/*.js',
            '!**/concat-lib/**',
          ],
          dest: 'lib/dist'
        }]
      }
    },

    less : {
      build : {
        options : {
          ieCompat : true,
          paths: ["lib/dev/css/"]
        },
        files: [{
          expand: true,
          cwd: 'lib/dev/less',
          src: ['*.less'],
          dest: 'lib/dev/css/',
          rename : function(dest, src){
            return dest + src.replace('.less','.css');
          }
        },
        {
          expand: true,
          cwd: 'lib/dev/less/pages',
          src: ['**/*.less'],
          dest: 'lib/dev/css/pages/',
          rename : function(dest, src){
            return dest + src.replace('.less','.css');
          }
        },
        {
          expand: true,
          cwd: 'lib/dev/less/adminpage',
          src: ['*.less'],
          dest: 'lib/dev/css/adminpage/',
          rename : function(dest, src){
            return dest + src.replace('.less','.css');
          }
        }]
      }
    },

    cssmin : {
      minify: {
        expand: true,
        cwd: 'lib/dev/',
        src: [
          '**/*.css',
          '!**/concat-lib/**'
        ],
        dest: 'lib/dist/'
      }
    },

    copy : {
      main :{
        files: [{
          expand: true,
          cwd: 'lib/dev',
          src: [
            '**/*',
            '!**/*.css',
            '!**/*.js',
            '!**/*.less',
            '!**/concat-lib/**',
            '!**/less/**'],
          dest: 'lib/dist'
        }]
      }
    },

    watch: {
      js : {
        files: [
          'lib/dev/concat-lib/**/*.js',
          'lib/dev/js/**/*.js',
        ],
        tasks: ['dist-js']
      },
      css : {
        files: [
          'lib/dev/concat-lib/**/*.css',
        ],
        tasks: ['dist-css']
      },
      less : {
        files: [
          'lib/dev/less/**/*.less'
        ],
        tasks: ['dist-css']
      }
    },
  });

  grunt.registerTask('dist-js', [
    'concat:defaults-main',
    'concat:jquery-default-plugins-main',
    'concat:pb-main',
    
    'concat:concat-all-main',

    'concat:defaults-admin',
    'concat:jquery-default-plugins-admin',
    'concat:pb-admin',
    
    'concat:concat-all-admin',
    
    'uglify']);
  grunt.registerTask('dist-css', ['less',
    'cssmin',
  ]);

  grunt.registerTask('dist', ['clean','dist-js','dist-css','copy']);
  grunt.registerTask('default', ['dist']);

};
