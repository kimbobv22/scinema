<?php

if(!is_admin()) return;

/***
Modules
**/
include_once(PB_THEME_LIB_DIR_PATH . 'modules/class.PBAction.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/class.PBWPTable.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-settings.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-crypt.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-crontab.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-map.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-mail.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-sms.php');
// include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-social.php');

/***
Common
**/
include_once(PB_THEME_LIB_DIR_PATH . 'modules/gcode/class.PB-admin-gcode.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/paygate/class.PB-admin-paygate.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/posts/class.PB-admin-posts.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/user/class.PB-admin-user.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/adminpage/class.PB-admin-adminpage.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/client/class.PB-admin-client.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-admin-movie.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/popup/class.PB-admin-popup.php');


?>