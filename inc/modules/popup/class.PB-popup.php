<?php

function pb_popup_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
					 {$wpdb->pbpopup}.ID ID
					,{$wpdb->pbpopup}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_name cinema_name

					,{$wpdb->pbpopup}.popup_title popup_title
					,{$wpdb->pbpopup}.popup_html popup_html

					,{$wpdb->pbpopup}.size_width size_width 
					,{$wpdb->pbpopup}.size_height size_height

					,{$wpdb->pbpopup}.srt_date srt_date
					,{$wpdb->pbpopup}.end_date end_date

					,{$wpdb->pbpopup}.only_home_yn only_home_yn
					,{$wpdb->pbpopup}.hide_yn hide_yn

					,{$wpdb->pbpopup}.sort_num sort_num

					,{$wpdb->pbpopup}.reg_date reg_date
					,DATE_FORMAT({$wpdb->pbpopup}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

					,{$wpdb->pbpopup}.mod_date mod_date
					,DATE_FORMAT({$wpdb->pbpopup}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

				
		     FROM {$wpdb->pbpopup}

		     LEFT OUTER JOIN {$wpdb->pbcinema}
		     ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbpopup}.cinema_id

		      ";

	$query_ .= apply_filters('pb_popup_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbpopup}.ID")." ";
	}
	if(isset($conditions_['cinema_id'])){ //정상적으로 상영관ID가 들어온 경우
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbpopup}.cinema_id")." ";
	}
	if(isset($conditions_['for_common']) && $conditions_['for_common'] === true){ //공통팝업체크
		$query_ .= " AND {$wpdb->pbpopup}.cinema_id = -1 ";
	}

	if(isset($conditions_['in_date']) && $conditions_['in_date'] === true){
		$query_ .= " AND NOW() BETWEEN {$wpdb->pbpopup}.srt_date AND {$wpdb->pbpopup}.end_date ";	
	}

	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_popup_list_keyword', array(
			"{$wpdb->pbpopup}.popup_title",
			
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_popup_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_popup_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY sort_num ASC, reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	return apply_filters('pb_popup_list', $wpdb->get_results($query_));	
}

function pb_popup($popup_id_){
	if(!strlen($popup_id_)) return null;
	$result_ = pb_popup_list(array('ID' => $popup_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_popup_fields($data_){
	return pb_format_mapping(apply_filters("pb_popup_fields_map",array(

		// 'ID' => '%s',
		'cinema_id' => '%d',
		'popup_title' => '%s',
		'popup_html' => '%s',
		'only_home_yn' => '%s',
		'hide_yn' => '%s',

		'size_width' => '%s',
		'size_height' => '%s',

		'srt_date' => '%s',
		'end_date' => '%s',
		
		'sort_num' => '%d',
		

	)), $data_);
}

function pb_popup_add($data_){
	
	$insert_data_ = _pb_parse_popup_fields($data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbpopup}", $insert_value_, $insert_format_);
	if(!$result_) return $result_;

	$popup_id_ = $wpdb->insert_id;

	do_action('pb_popup_added', $popup_id_);

	return $popup_id_;
}

function pb_popup_update($popup_id_, $update_data_){
	$update_data_ = _pb_parse_popup_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];

	$update_value_['mod_date']= current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbpopup}", $update_value_, array("ID" => $popup_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_popup_updated', $popup_id_);

	return $result_;
}

function pb_popup_remove($popup_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbpopup}", array("ID" => $popup_id_), array("%d"));

	if(!$result_) return $result_;
	do_action('pb_popup_removed', $popup_id_);
}

function pb_popup_allowed($obj_ = null){
	if(gettype($obj_) !== "object"){
		$obj_ = pb_popup($obj_);
	}

	return apply_filters('pb_popup_allowed', true, $obj_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/popup/class.PB-popup-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/popup/class.PB-popup-ajax.php');

?>