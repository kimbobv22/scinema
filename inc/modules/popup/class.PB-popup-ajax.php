<?php


//팝업 업데이트
pb_add_ajax('popup-update', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	
	$popup_data_ = isset($_POST['popup_data']) ? $_POST['popup_data'] : null;

	$popup_data_['hide_yn'] = isset($popup_data_['hide_yn']) ? $popup_data_['hide_yn'] : "N";
	
	if(empty($_POST['popup_data'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}

	$popup_id_ = null;
	
	if(!strlen($popup_data_['ID'])){ // new
		$popup_id_ = pb_popup_add($popup_data_);

		echo json_encode(array(
			'success' => true,
			'popup_id' => $popup_id_,
			'success_title' => '팝업추가완료',
			'success_message' => '팝업추가가 완료되었습니다.',
			'redirect_url' => pb_adminpage_url('popup-manage/edit/'.$popup_id_),
		));
		die();
	}else{	

		pb_popup_update($popup_data_['ID'], $popup_data_);

		echo json_encode(array(
			'success' => true,
			'popup_id' => $popup_data_['ID'],
			'success_title' => '팝업수정완료',
			'success_message' => '팝업수정이 완료되었습니다.',
			'redirect_url' => pb_adminpage_url('popup-manage/edit/'.$popup_data_['ID']),
		));
		die();
	}	
});

pb_add_ajax('popup-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$popup_id_ = isset($_POST['popup_id']) ? $_POST['popup_id'] : null;

	if(!strlen($popup_id_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된요청',
			'error_message' => '필수값이 누락되었습니다.',
		));
		die();	
	}

	pb_popup_remove($popup_id_);

	echo json_encode(array(
		'success' => true,
		'redirect_url' => pb_adminpage_url('popup-manage'),
	));
	die();	
});

pb_add_ajax('popup-switch-hide-yn', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$popup_id_ = isset($_POST['popup_id']) ? $_POST['popup_id'] : null;
	$yn_ = isset($_POST['yn']) ? $_POST['yn'] : "N";

	pb_popup_update($popup_id_, array("hide_yn" => $yn_));

	echo json_encode(array(
		'success' => true,
	));
	die();	

});

pb_add_ajax('popup-bulk-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$popup_ids_ = isset($_POST['popup_ids']) ? $_POST['popup_ids'] : null;

	foreach($popup_ids_ as $popup_id_){
		pb_popup_remove($popup_id_);	
	}

	echo json_encode(array(
		'success' => true,
		'redirect_url' => pb_adminpage_url('popup-manage'),
	));
	die();	
});

?>