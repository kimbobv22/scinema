<?php


add_action('init', function(){

	global $wpdb;

	$wpdb->pbpopup = "{$wpdb->base_prefix}popup";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}popup` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`cinema_id` BIGINT(20)  NOT NULL COMMENT 'ID',
		
		`popup_title` varchar(200)  DEFAULT NULL COMMENT '팝업명',
		`popup_html` longtext  DEFAULT NULL COMMENT '팝업HTML',

		`size_width` varchar(10)  DEFAULT NULL COMMENT '넓이PX',
		`size_height` varchar(10)  DEFAULT NULL COMMENT '높이PX',

		`only_home_yn` varchar(1)  DEFAULT 'Y' COMMENT '홈화면에서만 띄우기',
		`hide_yn` varchar(1)  DEFAULT 'Y' COMMENT '숨기기',

		`srt_date` datetime  COMMENT '게시시작일자',
		`end_date` datetime  COMMENT '게시종료일자',
		
		`sort_num` bigint(10)  DEFAULT NULL COMMENT '순서',
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='팝업';";

	return $args_;
},10, 3);


add_filter('pb_adminpage_list', function($results_){

	$results_['popup-manage'] = array(
		'title' => '팝업관리' ,
		'content-template' => "popup-manage/list",
		'subcontent-templates' => array(
			'edit' => "popup-manage/edit",
			'add' => "popup-manage/edit",
		),
		'lvl' => 1,
		'sort_num' => 20,
	);

	return $results_;
});

add_action('wp_enqueue_scripts', function(){
	wp_enqueue_script("jquery-ui-core");
	wp_enqueue_script("jquery-ui-draggable");

	wp_enqueue_script("pb-popup", (pb_library_url() . 'js/pb-popup.js'), array("pb-all-main", 'jquery-ui-draggable'));
});

add_filter('pb_popup_allowed', function($result_, $popup_){

	if($popup_->only_home_yn === "Y"){
		$current_url_ = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$current_url_ = rtrim($current_url_, '/') . '/';

		$home_url_ = home_url();
		$home_url_ = rtrim($home_url_, '/') . '/';
		
		return ($result_ && $home_url_ === $current_url_);	
	}


	return $result_;
	
},10,2);
add_filter('pb_popup_allowed', function($result_, $popup_){
	return ($result_ && $popup_->hide_yn !== "Y");
},10,2);

add_action('pb_adminpage_template', function(){
	add_filter('pb_popup_allowed', function($result_, $popup_){ return false; }, 10, 2);
});


add_action('wp_footer', function(){

	$now_cinemaid = get_current_blog_id();
	//echo $now_cinemaid;

	if ($now_cinemaid != '36' && $now_cinemaid != '32' && $now_cinemaid != '8' && $now_cinemaid != '38' && $now_cinemaid != '1') {
		$common_popup_list_ = pb_popup_list(array('in_date' => true, 'for_common' => true));
	}	
	$common_popup_list_ = isset($common_popup_list_) ? $common_popup_list_ : array();

	$cinema_popup_list_ = pb_popup_list(array('in_date' => true, 'cinema_id' => get_current_blog_id()));
	$cinema_popup_list_ = isset($cinema_popup_list_) ? $cinema_popup_list_ : array();

	$popup_list_ = array_merge($common_popup_list_, $cinema_popup_list_);
?>
<script type="text/javascript">
jQuery(document).ready(function(){

<?php
	$z_index_ = 2002;

	foreach($popup_list_ as $row_data_){
		++$z_index_;

		if(pb_popup_allowed($row_data_)){
			$row_data_->popup_html = stripslashes($row_data_->popup_html);
			?> PB.popup.layout(<?=json_encode($row_data_)?>, null, <?=$z_index_?>); <?php 
		}
	}
?>	
});
</script>
<?php 
	

});

?>