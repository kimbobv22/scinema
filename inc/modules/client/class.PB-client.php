<?php

//거래처내역 가져오기
function pb_client_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
					 {$wpdb->pbclient}.client_id client_id

					,{$wpdb->pbclient}.client_name client_name
					,{$wpdb->pbclient}.client_ctg client_ctg
					,".pb_query_gcode_dtl_name("CL001", "{$wpdb->pbclient}.client_ctg")." client_ctg_name

					,{$wpdb->pbclient}.client_status client_status
					,".pb_query_gcode_dtl_name("CL003", "{$wpdb->pbclient}.client_status")." client_status_name

					,{$wpdb->pbclient}.cinema_id cinema_id

					,{$wpdb->pbclient}.manager_name manager_name
					,{$wpdb->pbclient}.manager_phone1 manager_phone1
					,{$wpdb->pbclient}.manager_phone2 manager_phone2
					,{$wpdb->pbclient}.manager_email manager_email

					,{$wpdb->pbclient}.biz_no biz_no
					,{$wpdb->pbclient}.biz_boss_name biz_boss_name
					,{$wpdb->pbclient}.biz_addr biz_addr
					,{$wpdb->pbclient}.biz_ctg biz_ctg
					,{$wpdb->pbclient}.biz_event biz_event

					,{$wpdb->pbclient}.bank_code bank_code
					,".pb_query_gcode_dtl_name("I0009", "{$wpdb->pbclient}.bank_code")." bank_name

					,{$wpdb->pbclient}.account_num account_num
					,{$wpdb->pbclient}.account_holder account_holder
					
				,{$wpdb->pbclient}.reg_date reg_date
				,DATE_FORMAT({$wpdb->pbclient}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

				,{$wpdb->pbclient}.mod_date mod_date
				,DATE_FORMAT({$wpdb->pbclient}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

				
		     FROM {$wpdb->pbclient}
			
			LEFT OUTER JOIN {$wpdb->pbcinema}
			ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbclient}.cinema_id

		      ";

	$query_ .= apply_filters('pb_client_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['client_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['client_id'], "{$wpdb->pbclient}.client_id")." ";
	}

	if(isset($conditions_['client_ctg'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['client_ctg'], "{$wpdb->pbclient}.client_ctg")." ";
	}
	if(isset($conditions_['client_status'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['client_status'], "{$wpdb->pbclient}.client_status")." ";
	}

	if(isset($conditions_['cinema_id']) && strlen($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbclient}.cinema_id")." ";
	}
	if(isset($conditions_['exclude_independent']) && $conditions_['exclude_independent'] === true){
		$query_ .= " AND ({$wpdb->pbcinema}.cinema_type is NULL OR {$wpdb->pbcinema}.cinema_type != '".PB_CINEMA_CTG_IND."') ";
	}
	if(isset($conditions_['for_common']) && $conditions_['for_common'] === true){
		$query_ .= " AND {$wpdb->pbclient}.cinema_id = '-1' ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_client_list_keyword', array(
			"{$wpdb->pbclient}.client_name",
			"{$wpdb->pbclient}.manager_name",
			"{$wpdb->pbclient}.biz_no",
			"{$wpdb->pbclient}.manager_phone1",
			"{$wpdb->pbclient}.manager_phone2",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_client_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_client_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	return apply_filters('pb_client_list', $wpdb->get_results($query_));	
}

//거래처 단인내역 가져오기
function pb_client($client_id_){
	if(!strlen($client_id_)) return null;
	$result_ = pb_client_list(array('client_id' => $client_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_client_fields($data_){
	return pb_format_mapping(apply_filters("pb_client_fields_map",array(
	
		'client_name' => '%s',
		'client_ctg' => '%s',
		'client_status' => '%s',
		'cinema_id' => '%d',

		'manager_name' => '%s',
		'manager_phone1' => '%s',
		'manager_phone2' => '%s',
		'manager_email' => '%s',

		'biz_no' => '%s',
		'biz_boss_name' => '%s',
		'biz_addr' => '%s',
		'biz_ctg' => '%s',
		'biz_event' => '%s',

		'bank_code' => '%s',
		'account_num' => '%s',
		'account_holder' => '%s',

	)), $data_);
}

//거래처 추가
function pb_client_add($client_data_){
	$insert_data_ = _pb_parse_client_fields($client_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbclient}", $insert_value_, $insert_format_);

	if(!$result_) return $result_;

	$client_id_ = $wpdb->insert_id;

	do_action('pb_client_added', $client_id_);

	return $client_id_;
}

//거래처 수정
function pb_client_update($client_id_, $client_data_){
	$client_data_ = _pb_parse_client_fields($client_data_);

	$update_value_ = $client_data_['data'];
	$update_format_ = $client_data_['format'];

	$update_value_['mod_date'] = current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbclient}", $update_value_, array("client_id" => $client_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_client_updated', $client_id_);

	return $result_;
}

//거래처 삭제
function pb_client_remove($client_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbclient}", array("client_id" => $client_id_), array("%d"));

	if(!$result_) return $result_;

	do_action('pb_client_removed', $client_id_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/client/class.PB-client-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/client/class.PB-client-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/client/class.PB-client-adminpage.php');

?>