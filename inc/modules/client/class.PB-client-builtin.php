<?php

add_action('init', function(){

	global $wpdb;

	$wpdb->pbclient = "{$wpdb->base_prefix}client";
	// $wpdb->pbcinema_area = "{$wpdb->base_prefix}movie_open";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}client` (
		`client_id` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		
		`client_name` varchar(100)  DEFAULT NULL COMMENT '거래처명',
		`client_ctg` varchar(5)  DEFAULT NULL COMMENT '거래처구분',
		`client_status` varchar(5)  DEFAULT NULL COMMENT '등록상태',
		`cinema_id` BIGINT(20)  DEFAULT NULL COMMENT '사용상영관',

		`manager_name` varchar(50)  DEFAULT NULL COMMENT '담당자명',
		`manager_phone1` varchar(50)  DEFAULT NULL COMMENT '담당자전화번호',
		`manager_phone2` varchar(50)  DEFAULT NULL COMMENT '담당자휴대폰',
		`manager_email` varchar(50)  DEFAULT NULL COMMENT '담당자이메일',
		`biz_no` varchar(20)  DEFAULT NULL COMMENT '사업자등록번호',
		`biz_boss_name` varchar(50)  DEFAULT NULL COMMENT '대표자명',
		`biz_addr` varchar(200)  DEFAULT NULL COMMENT '사업장주소',
		`biz_ctg` varchar(200)  DEFAULT NULL COMMENT '업태',
		`biz_event` varchar(200)  DEFAULT NULL COMMENT '종목',
		`bank_code` varchar(10)  DEFAULT NULL COMMENT '은행코드',
		`account_num` varchar(50)  DEFAULT NULL COMMENT '계좌번호',
		`account_holder` varchar(50)  DEFAULT NULL COMMENT '예금주',

		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`client_id`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='거래처';";


	return $args_;
},10, 3);

define('PB_CLIENT_CTG_INNER_TRANS', '00001');
define('PB_CLIENT_CTG_INNER_STORE', '00003');
define('PB_CLIENT_CTG_OUTER_PUBL', '00005');
define('PB_CLIENT_CTG_INNER_EQUIP', '00007');

add_filter('pb_initial_gcode_list', function($gcode_list_){
	
	$gcode_list_['CL001'] = array(
		'name' => '거래처구분',
		'data' => array(
			PB_CLIENT_CTG_INNER_TRANS => '내부거래명세',
			PB_CLIENT_CTG_INNER_STORE => '매점구매',
			PB_CLIENT_CTG_OUTER_PUBL => '배급사',
			PB_CLIENT_CTG_INNER_EQUIP => '장비관련',
		),
	);
	$gcode_list_['CL003'] = array(
		'name' => '거래처상태',
		'data' => array(
			'00001' => '사용',
			'00003' => '미사용',
		),
	);
	
	return $gcode_list_;
});

?>