<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();
	$current_cinema_data_ = pb_cinema($blog_id_);

	if($blog_id_ !== PB_CINEMA_HEAD_OFFICE_ID && $current_cinema_data_->cinema_type !== PB_CINEMA_CTG_IND){
		return $results_;
	}


	$temp_cinema_list_ = pb_cinema_list(array(
		'status' => PB_CINEMA_STATUS_NORMAL,
		'include_head_office' => true,
	));

	if($current_cinema_data_->cinema_type === PB_CINEMA_CTG_IND){
		// $cinema_id_ = $current_cinema_data_->cinema_id;
		$cinema_list_ = array();
		$cinema_list_[$current_cinema_data_->cinema_id] = $current_cinema_data_->cinema_name;
	}else{
		$cinema_list_ = array("-1" => "공통");

		foreach($temp_cinema_list_ as $row_data_){
			$cinema_list_[$row_data_->cinema_id] = $row_data_->cinema_name;
		}	
	}

	$results_['client-manage'] = array(
		'title' => '거래처관리' ,
		'template-data' => array(



			'data' => array(
				'total-count' => function(){
					$blog_id_ = get_current_blog_id();
					$current_cinema_data_ = pb_cinema($blog_id_);
					$cinema_id_ = null;

					$exclude_ind_ = true;

					if($current_cinema_data_->cinema_type === PB_CINEMA_CTG_IND){
						$cinema_id_ = $current_cinema_data_->cinema_id;
						$exclude_ind_ = false;
					}

					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";						
					return pb_client_list(array(
						'keyword' => $keyword_,
						'cinema_id' => $cinema_id_,
						'exclude_independent' => $exclude_ind_,
						'just_count' => true,
					));
				},
				'list' => function($offset_, $limit_){
					$blog_id_ = get_current_blog_id();
					$current_cinema_data_ = pb_cinema($blog_id_);
					$cinema_id_ = null;

					$exclude_ind_ = true;

					if($current_cinema_data_->cinema_type === PB_CINEMA_CTG_IND){
						$cinema_id_ = $current_cinema_data_->cinema_id;
						$exclude_ind_ = false;
					}

					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					
					return pb_client_list(array(
						'keyword' => $keyword_,
						'cinema_id' => $cinema_id_,
						'exclude_independent' => $exclude_ind_,
						'limit' => array($offset_, $limit_),
					));
				},
				'single' => function($action_data_){
					return pb_client($action_data_);
				},
			),

			'options' => array(
				'list-options' => array(
					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 거래처가 없습니다.',
						'add' => '거래처등록',
					),
				),
				'edit-options' => array(
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => '거래처추가확인',
						'add-confirm-message' => '해당 거래처를 추가하시겠습니까?',
						'add-success-title' => '추가완료',
						'add-success-message' => '거래처가 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => '거래처 추가 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => '거래처정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => '거래처 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '거래처삭제확인',
						'remove-confirm-message' => '해당 거래처를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => '거래처정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => '거래처 삭제 중 에러가 발생하였습니다.',
					),
				),
				'id-format' => 'pb-client-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){
					$result_ = pb_client_update($target_data_['client_id'], $target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '거래처 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['client_id'];
				},
				'add' => function($target_data_){
					$result_ = pb_client_add($target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '거래처 추가 중 에러가 발생했습니다.');
					}


					return $result_;
				},
				'remove' => function($target_data_){
					pb_client_remove($target_data_['client_id']);
					return true;
				},
			),
			'templates' => array(
				'list' => array(
					'client_name' => array(
						'column-label' => '거래처명',
						'th-class' => "col-3",
						'td-class' => "col-3",
						'td-render-func' => function($item_, $column_name_){
							?>

							<a href="<?=pb_adminpage_url("client-manage/edit/".$item_->client_id)?>"><?=$item_->client_name?></a>

							<?php
						},
					),
					'client_ctg_name' => array(
						'column-label' => '거래처구분',
						'th-class' => "col-2 text-center hidden-xs",
						'td-class' => "col-2 text-center hidden-xs",
						/*'td-render-func' => function($item_, $column_name_){
							?>

							<a href="#"><?=$item_->client_name?></a>

							<?php
						},*/
					),
					'client_status_name' => array(
						'column-label' => '등록상태',
						'th-class' => "col-2 text-center hidden-xs",
						'td-class' => "col-2 text-center hidden-xs",
						/*'td-render-func' => function($item_, $column_name_){
							?>

							<a href="#"><?=$item_->client_name?></a>

							<?php
						},*/
					),
					'manager_name' => array(
						'column-label' => '담당자명',
						'th-class' => "col-2 text-center hidden-xs",
						'td-class' => "col-2 text-center hidden-xs",
						/*'td-render-func' => function($item_, $column_name_){
							?>

							<a href="#"><?=$item_->client_name?></a>

							<?php
						},*/
					),
					'manager_phone' => array(
						'column-label' => '담당자연락처',
						'th-class' => "col-3 text-center",
						'td-class' => "col-3 text-center",
						'td-render-func' => function($item_, $column_name_){
							?>

							<?=$item_->manager_phone1?><br/>
							<?=$item_->manager_phone2?>

							<?php
						},
					),
				),
				'edit' => array(
					array(
						'type' => 'hidden',
						'column-name' => 'client_id',
					),
					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '거래처명',
								'column-name' => 'client_name',
								'placeholder' => '거래처명 입력',
								'validates' => array(
									'required' => "거래처명을 입력하세요",
								),
								'type' => 'text',
							),
							array(
								'column' => 'col-xs-6 col-sm-6',
								'column-label' => '거래처구분',
								'column-name' => 'client_ctg',
								'placeholder' => '-거래처구분-',
								'validates' => array(
									'required' => "거래처구분를 선택하세요",
								),
								'type' => 'select-gcode',
								'code-id' => 'CL001'
							),
						),
					),
					array(
						'type' => 'row',
						'columns' => array(
					
							array(
								'column' => 'col-xs-6 col-sm-6',
								'column-label' => '거래처상태',
								'column-name' => 'client_status',
								'placeholder' => '-거래처상태-',
								'validates' => array(
									'required' => "거래처상태를 선택하세요",
								),
								'type' => 'select-gcode',
								'code-id' => 'CL003'
							),
							array(
								'column' => 'col-xs-6 col-sm-6',
								'column-label' => '사용영화관',
								'column-name' => 'cinema_id',
								'placeholder' => '-영화관선택-',
								'validates' => array(
									'required' => "영화관을 선택하세요",
								),
								'type' => 'select',
								'options' => $cinema_list_,
							)
						)
					),
					array(
						'type' => 'division',
					),
					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '담당자명',
								'column-name' => 'manager_name',
								'placeholder' => '담당자명 입력',
								'validates' => array(
									// 'required' => "담당자명을 입력하세요",
								),
								'type' => 'text',
							),
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '담당자 이메일',
								'column-name' => 'manager_email',
								'placeholder' => '이메일 입력',
								'validates' => array(
									// 'required' => "이메일을 입력하세요",
									'email' => "이메일을 입력하세요",
								),
								'type' => 'email',
							),
						),
					),
					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '담당자전화번호',
								'column-name' => 'manager_phone1',
								'placeholder' => '연락처 입력',
								'validates' => array(
									// 'required' => "연락처를 입력하세요",
								),
								'type' => 'text',
							),
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '담당자휴대폰',
								'column-name' => 'manager_phone2',
								'placeholder' => '연락처 입력',
								'validates' => array(
									// 'required' => "담당자휴대폰을 입력하세요",
								),
								'type' => 'text',
							),
						),
					),
					array(
						'type' => 'division'
					),
					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-4',
								'column-label' => '사업자등록번호',
								'column-name' => 'biz_no',
								'placeholder' => '사업자등록번호 입력',
								'validates' => array(
									// 'required' => "사업자등록번호을 입력하세요",
								),
								'type' => 'text',
							),
							array(
								'column' => 'col-xs-12 col-sm-8',
								'column-label' => '대표자명',
								'column-name' => 'biz_boss_name',
								'placeholder' => '대표자명 입력',
								'validates' => array(
									// 'required' => "대표자명을 입력하세요",
								),
								'type' => 'text',
							),
						),
					),

					array(
						'column-label' => '사업지주소',
						'column-name' => 'biz_addr',
						'placeholder' => '사업지주소 입력',
						'form-group' => true,
						'type' => 'text',
					),

					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '업태',
								'column-name' => 'biz_ctg',
								'placeholder' => '업태 입력',
								'validates' => array(
									// 'required' => "업태를 입력하세요",
								),
								'type' => 'text',
							),
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '종목',
								'column-name' => 'biz_event',
								'placeholder' => '종목 입력',
								'validates' => array(
									// 'required' => "업종을 입력하세요",
								),
								'type' => 'text',
							),
						),
					),

					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-4',
								'column-label' => '은행',
								'column-name' => 'bank_code',
								'placeholder' => '-은행선택-',
								'validates' => array(
									// 'required' => "연락처를 입력하세요",
								),
								'type' => 'select-gcode',
								'code-id' => 'I0009',
							),
							array(
								'column' => 'col-xs-12 col-sm-8',
								'column-label' => '계좌번호',
								'column-name' => 'account_num',
								'placeholder' => '계좌번호 입력',
								'validates' => array(
									// 'required' => "담당자휴대폰을 입력하세요",
								),
								'type' => 'text',
							),
						),
					),
					array(
						'column-label' => '예금주',
						'column-name' => 'account_holder',
						'placeholder' => '계좌번호 입력',
						'validates' => array(
							// 'required' => "담당자휴대폰을 입력하세요",
						),
						'type' => 'text',
					),
					
				)
			),

		),
		'lvl' => 1,
		'sort_num' => 9,
	);

	return $results_;
});

?>