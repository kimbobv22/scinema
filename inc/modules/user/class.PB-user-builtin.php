<?php



add_action('pb_db_table_installed', function(){
	
	global $wpdb;

	$row = $wpdb->get_results("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
	WHERE table_name = '{$wpdb->users}' AND column_name = 'full_name' and table_schema = '".DB_NAME."'"  );

	if(empty($row)){

		$wpdb->query("ALTER TABLE {$wpdb->users} ADD full_name VARCHAR(50) DEFAULT NULL COMMENT '이름' ");

		$wpdb->query("ALTER TABLE {$wpdb->users} ADD gender VARCHAR(5) DEFAULT NULL COMMENT '성별' ");
		$wpdb->query("ALTER TABLE {$wpdb->users} ADD birth_yyyymmdd VARCHAR(8) DEFAULT NULL COMMENT '생년월일' ");

		$wpdb->query("ALTER TABLE {$wpdb->users} ADD phone1_1 VARCHAR(5) DEFAULT NULL COMMENT '연락처1' ");
		$wpdb->query("ALTER TABLE {$wpdb->users} ADD phone1_2 VARCHAR(7) DEFAULT NULL COMMENT '연락처1' ");
		$wpdb->query("ALTER TABLE {$wpdb->users} ADD phone1_3 VARCHAR(7) DEFAULT NULL COMMENT '연락처1' ");

		$wpdb->query("ALTER TABLE {$wpdb->users} ADD findpass_vkey VARCHAR(20) DEFAULT NULL COMMENT '암호찾기 검증키' ");
		$wpdb->query("ALTER TABLE {$wpdb->users} ADD findpass_vkey_date datetime DEFAULT NULL COMMENT '암호찾기 검증키 발급일자' ");

		$wpdb->query("ALTER TABLE {$wpdb->users} ADD mailing_recv_yn VARCHAR(1) DEFAULT 'N' COMMENT '메일링여부' ");
		$wpdb->query("ALTER TABLE {$wpdb->users} ADD sms_recv_yn VARCHAR(1) DEFAULT 'N' COMMENT 'SMS수신여부' ");

		$wpdb->query("ALTER TABLE {$wpdb->users} ADD authcode VARCHAR(50) DEFAULT NULL COMMENT '본인인증코드' ");
	}	
});

//로그인URL 필터
add_filter('login_url', function($login_url_, $redirect_){
	$login_url_ = pb_user_login_url($redirect_);
    return $login_url_;
}, 10, 2 );

add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_USER_LOGIN_SLUG : 
		case PB_USER_SIGNUP_SLUG : 
		case PB_USER_MYPAGE_SLUG : 
		case PB_USER_RESETPASS_SLUG : 
		case PB_USER_LOGOUT_SLUG : 

			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_user_page_action%', '([^&]+)');
	add_rewrite_tag('%_pb_user_mypage_action%', '([^&]+)');
	
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_USER_LOGIN_SLUG.'/?$','index.php?_pb_user_page_action='.PB_USER_LOGIN_SLUG,'top');
	add_rewrite_rule('^'.PB_USER_SIGNUP_SLUG.'/?$','index.php?_pb_user_page_action='.PB_USER_SIGNUP_SLUG,'top');
		
	add_rewrite_rule('^'.PB_USER_MYPAGE_SLUG.'/(.+)/?$','index.php?_pb_user_page_action='.PB_USER_MYPAGE_SLUG.'&_pb_user_mypage_action=$matches[1]','top');
	add_rewrite_rule('^'.PB_USER_MYPAGE_SLUG.'/?$','index.php?_pb_user_page_action='.PB_USER_MYPAGE_SLUG,'top');

	add_rewrite_rule('^'.PB_USER_RESETPASS_SLUG.'/?$','index.php?_pb_user_page_action='.PB_USER_RESETPASS_SLUG,'top');
	add_rewrite_rule('^'.PB_USER_LOGOUT_SLUG.'/?$','index.php?_pb_user_page_action='.PB_USER_LOGOUT_SLUG,'top');
});

add_action('pb_template',function(){
	$user_page_action_ = get_query_var("_pb_user_page_action");

	switch($user_page_action_){
		case PB_USER_LOGIN_SLUG : 
		case PB_USER_SIGNUP_SLUG :
			if(is_user_logged_in()){
				wp_redirect(home_url());
				return;
			}
			// header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

			wp_enqueue_style("page-user-".$user_page_action_, (pb_library_url() . 'css/pages/user/'.$user_page_action_.'.css'));
			wp_enqueue_script("page-user-".$user_page_action_, (pb_library_url() . 'js/pages/user/'.$user_page_action_.'.js'), array("pb-all-main"));

			break;
		case PB_USER_LOGOUT_SLUG : 
			wp_logout();
			$redirect_url_ = isset($_GET['redirect_url']) ? $_GET['redirect_url'] : home_url();
			wp_redirect($redirect_url_);
			return;
		case PB_USER_MYPAGE_SLUG : 
			if(!is_user_logged_in()){
				wp_redirect(pb_user_login_url(pb_user_mypage_url(get_query_var("_pb_user_mypage_action"))));
				return;
			}

			global $pb_user_mypage_action,$pb_user_mypage_menu_item;

			$menu_list_ = pb_user_mypage_menu_list();
			$pb_user_mypage_action = get_query_var("_pb_user_mypage_action");

			if(!strlen($pb_user_mypage_action)){
				$tmp_ = array_keys($menu_list_);
				$pb_user_mypage_action = $tmp_[0];
			}

			$pb_user_mypage_menu_item = isset($menu_list_[$pb_user_mypage_action]) ? $menu_list_[$pb_user_mypage_action] : null;

			wp_enqueue_style("page-user-mypage", (pb_library_url() . 'css/pages/user/mypage.css'));
			wp_enqueue_script("page-user-mypage", (pb_library_url() . 'js/pages/user/mypage.js'), array("pb-all-main"));

			add_filter('pb_post_with_sidemenu', function(){
				return true;
			});
			add_filter('pb_post_with_breadcrumb', function(){
				return true;
			});

			add_filter('pb_breadcrumb', function(){
				return PB_THEME_LIB_DIR_PATH.'modules/user/views/breadcrumb.php';
			});
			add_filter('pb_sidemenu', function(){
				return PB_THEME_LIB_DIR_PATH.'modules/user/views/sidemenu.php';
			});

			do_action('pb_template_user_mypage');

			if(strlen($pb_user_mypage_action)){
				do_action('pb_template_user_mypage_'.$pb_user_mypage_action);
			}
			
			break;
		case PB_USER_RESETPASS_SLUG : 
			if(is_user_logged_in()){
				wp_redirect(home_url());
				return;
			}

			$email_ = isset($_GET['email']) ? $_GET['email'] : null;
			$vkey_ = isset($_GET['vkey']) ? $_GET['vkey'] : null;

			if(!strlen($email_) || !strlen($vkey_)){
				pb_404();
				return;
			}

			$user_data_ = pb_user_by_user_email($email_);
			$result_ = pb_user_check_findpass_validation_key($user_data_->ID, $vkey_);

			if(is_wp_error($result_)){
				pb_404();
				return;	
			}

			global $user_resetpass_user, $user_resetpass_vkey;
			$user_resetpass_user = $user_data_;
			$user_resetpass_vkey = $vkey_;

			wp_enqueue_style("page-user-resetpass", (pb_library_url() . 'css/pages/user/resetpass.css'));
			wp_enqueue_script("page-user-resetpass", (pb_library_url() . 'js/pages/user/resetpass.js'), array("pb-all-main"));
		break;
		default : break;
	}
});

add_filter('pb_content_template', function($content_template_){
	$user_page_action_ = get_query_var("_pb_user_page_action");

	switch($user_page_action_){
		
		case PB_USER_MYPAGE_SLUG : 

			global $pb_user_mypage_action,$pb_user_mypage_menu_item;
			return $pb_user_mypage_menu_item['content-templete'];

		case PB_USER_LOGIN_SLUG : 
		case PB_USER_SIGNUP_SLUG : 
		case PB_USER_RESETPASS_SLUG : 
			return "user/".$user_page_action_;
		default : return $content_template_;

	}
});

?>