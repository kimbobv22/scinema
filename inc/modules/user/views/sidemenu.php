<?php
	
	global $pb_user_mypage_action,$pb_user_mypage_menu_item;
	$mypage_list_ = pb_user_mypage_menu_list();

	?>
		<h3 class="sidemenu-title">마이페이지</h3>
		
	<ul class="sidemenu-list">

		<!-- <li class="<?= !strlen($pb_user_mypage_action) ? "active" : ""?>">
			<a href="<?=pb_user_mypage_url()?>">
				마이페이지
				<i class="icon fa fa-angle-right" aria-hidden="true"></i>
			</a>
		</li> -->

	<?php

		foreach($mypage_list_ as $menu_id_ => $row_data_){

			if(isset($row_data_['url'])){
				$target_url_ = $row_data_['url'];
			}else{
				$target_url_ = pb_user_mypage_url($menu_id_);
			}
					

			?>
			<li class="<?=$menu_id_ === $pb_user_mypage_action ? "active" : ""?>">
				<a href="<?=$target_url_?>" data-mypage-action="<?=$menu_id_?>">
					<?=$row_data_['title']?>
					<i class="icon fa fa-angle-right" aria-hidden="true"></i>
				</a>
			</li>
		<?php }	

?> </ul> <?php

?>