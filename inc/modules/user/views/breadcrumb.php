<?php
	
	global $pb_user_mypage_action,$pb_user_mypage_menu_item;
	$menu_list_ = pb_user_mypage_menu_list();

	if(!strlen($pb_user_mypage_action)){
		?>	<li class="dropdown">
				<a href="<?=pb_user_mypage_url()?>" data-toggle="dropdown">마이페이지 <i class="icon fa fa-angle-down" aria-hidden="true"></i></a>
				<ul class="dropdown-menu">
					<?php foreach($menu_list_ as $key_ => $menu_item_){

						if(isset($menu_item_['url'])){
							$target_url_ = $menu_item_['url'];
						}else{
							$target_url_ = pb_user_mypage_url($key_);
						}

					?>
						<li><a href="<?=$target_url_?>" data-mypage-action="<?=$key_?>"><?=$menu_item_['title']?></a></li>
					<?php } ?>
				</ul>

			</li>
			
		<?php		
	}else{
		?> <li class=""><a href="<?=pb_user_mypage_url()?>">마이페이지</a></li> 
			<li class="dropdown">
				<a href="#" data-toggle="dropdown"><?=$pb_user_mypage_menu_item['title'] ?> <i class="icon fa fa-angle-down" aria-hidden="true"></i></a>
				<ul class="dropdown-menu">
					<?php foreach($menu_list_ as $key_ => $menu_item_){
						if(isset($menu_item_['url'])){
							$target_url_ = $menu_item_['url'];
						}else{
							$target_url_ = pb_user_mypage_url($key_);
						}
					?>
						<li><a href="<?=$target_url_?>" data-mypage-action="<?=$key_?>"><?=$menu_item_['title']?></a></li>
					<?php } ?>
				</ul>

			</li>
			
		<?php		

	}

		

?>