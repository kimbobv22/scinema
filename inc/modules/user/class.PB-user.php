<?php

/** 사용자 리스트 가져오기
 *
 * @param array 조건절
 * @return array 사용자 리스트
 */
function pb_user_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
					 {$wpdb->users}.ID ID
					,{$wpdb->users}.user_login user_login
					,{$wpdb->users}.user_pass user_pass
					,{$wpdb->users}.user_email user_email
					,{$wpdb->users}.user_registered user_registered
					,{$wpdb->users}.user_activation_key user_activation_key
					,{$wpdb->users}.user_status user_status

					,{$wpdb->users}.full_name full_name
					,{$wpdb->users}.gender gender
					,".pb_query_gcode_dtl_name('Z0001', "{$wpdb->users}.gender")." gender_name

					,{$wpdb->users}.birth_yyyymmdd birth_yyyymmdd
					,{$wpdb->users}.phone1_1 phone1_1
					,{$wpdb->users}.phone1_2 phone1_2
					,{$wpdb->users}.phone1_3 phone1_3
					,CONCAT({$wpdb->users}.phone1_1, '-',{$wpdb->users}.phone1_2,'-',{$wpdb->users}.phone1_3) phone1
					,{$wpdb->users}.findpass_vkey findpass_vkey
					,{$wpdb->users}.findpass_vkey_date findpass_vkey_date
					
					,{$wpdb->users}.mailing_recv_yn mailing_recv_yn
					,{$wpdb->users}.sms_recv_yn sms_recv_yn

					,{$wpdb->users}.authcode authcode
				
		     FROM {$wpdb->users}
		      ";

	$query_ .= apply_filters('pb_user_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID']) && strlen($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->users}.ID")." ";
	}

	if(isset($conditions_['user_login']) && strlen($conditions_['user_login'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['user_login'], "{$wpdb->users}.user_login")." ";
	}
	if(isset($conditions_['user_email']) && strlen($conditions_['user_email'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['user_email'], "{$wpdb->users}.user_email")." ";
	}

	if(isset($conditions_['mailing_recv_yn']) && strlen($conditions_['mailing_recv_yn'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['mailing_recv_yn'], "{$wpdb->users}.mailing_recv_yn")." ";
	}
	if(isset($conditions_['authcode']) && strlen($conditions_['authcode'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['authcode'], "{$wpdb->users}.authcode")." ";
	}
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_user_list_keyword', array(
			"{$wpdb->users}.user_login",
			"{$wpdb->users}.user_email",
			"{$wpdb->users}.full_name",
			"{$wpdb->users}.phone1_3",
			
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_user_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_user_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY user_registered DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	return apply_filters('pb_user_list', $wpdb->get_results($query_));	
}

/** 사용자 단일내역 가져오기
 *
 * @param string 사용자ID
 * @return array 사용자 단일정보
 */
function pb_user($user_id_){
	if(!strlen($user_id_)) return null;
	$result_ = pb_user_list(array('ID' => $user_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}
function pb_user_by_user_login($user_login_){
	if(!strlen($user_login_)) return null;
	$result_ = pb_user_list(array('user_login' => $user_login_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}
function pb_user_by_user_email($user_email_){
	if(!strlen($user_email_)) return null;
	$result_ = pb_user_list(array('user_email' => $user_email_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function pb_user_by_both($both_){
	if(!strlen($both_)) return null;
	$result_ = pb_user_by_user_email($both_);

	if(!empty($result_)) return $result_;

	return pb_user_by_user_login($both_);
}

// 디트릭스시 사용하는 개인로그인키값으로 자동로그인
function pb_user_by_authcode($userKey_)
{
	# 개인 로그인 키 
	if(!strlen($userKey_)) return null;
	$result_ = pb_user_list(array('authcode' => $userKey_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_user_fields($data_){
	return pb_format_mapping(apply_filters("pb_user_fields_map",array(

		'user_registered' => '%s',
		'user_activation_key' => '%s',
		'user_status' => '%s',

		'full_name' => '%s',
		'gender' => '%s',
		'birth_yyyymmdd' => '%s',
		'phone1_1' => '%s',
		'phone1_2' => '%s',
		'phone1_3' => '%s',
		'findpass_vkey' => '%s',
		'findpass_vkey_date' => '%s',
		
		'mailing_recv_yn' => '%s',
		'sms_recv_yn' => '%s',
		'authcode' => '%s',

	)), $data_);
}

/** 사용자 추가
 *
 * @param array 사용자 정보
 * @return object 성공시 - 추가된 사용자ID, 실패 시 - WP_Error
 */
function pb_user_add($user_data_){
	$user_id_ = wp_insert_user(array(
		'user_login'  =>  $user_data_['user_login'],
		'user_email'  =>  $user_data_['user_email'],
	    'user_pass'   =>  $user_data_['user_pass'],
	));

	if(is_wp_error($user_id_)){
		return $user_id_;
	}

	$insert_data_ = _pb_parse_user_fields($user_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->users}", $insert_value_, array("ID" => $user_id_), $insert_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_user_added', $user_id_);

	return $user_id_;
}

/** 사용자 수정
 *
 * @param int 사용자ID
 * @param array 사용자 정보
 * @return object 수정성공 시 - TREU, 실패 시 - WP_Error
 */
function pb_user_update($user_id_, $update_data_){
	$update_data_ = _pb_parse_user_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->users}", $update_value_, array("ID" => $user_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_user_updated', $user_id_);

	return $result_;
}

/** 사용자 삭제
 *
 * @param int 사용자ID
 * @return object 삭제성공 시 - true, 실패 시 - false
 */
function pb_user_remove($user_id_){
	global $wpdb;
	$result_ = wp_delete_user($user_id_);
	if(!$result_) return false;
	do_action('pb_user_removed', $user_id_);
	return $result_;
}

define('PB_USER_EMAIL_AUTHCODE_KEY', "_PB_USER_EMAIL_AUTHCODE_KEY_");

/** 사용자 이메일 인증처리
*/
function pb_user_send_authcode_for_email($email_){

	$authcode_ = pb_random_string(5, PB_RANDOM_STRING_NUMUPPER);

	ob_start();
?>

<p>아래의 인증코드를 입력란에 입력하여 주세요.</p>
<p style="font-size:20px">
	<strong><?=$authcode_?></strong>
</p>

<?php
	$content_html_ = ob_get_clean();

	$result_ = pb_mail_send($email_, "[".get_bloginfo('name')."] 이메일인증요청", array(
		'content' => $content_html_,
	));

	if(is_wp_error($result_)){
		return $result_;
	}

	pb_session_put(PB_USER_EMAIL_AUTHCODE_KEY, array(
		'email' => strtolower($email_),
		'authcode' => $authcode_,
	));
	
	return $authcode_;
}
function pb_user_check_authcode_for_email($email_, $authcode_){
	$authcode_data_ = pb_session_get(PB_USER_EMAIL_AUTHCODE_KEY);
	if(!isset($authcode_data_) || !isset($authcode_data_['email'])) return false;
	return ($authcode_data_['email'] === strtolower($email_) && $authcode_data_['authcode'] === strtoupper($authcode_));
}

//로그인처리
function pb_user_login($email_or_login_, $pass_, $remember_ = true){
	$user_ = pb_user_by_user_email($email_or_login_);
	if(!isset($user_) || empty($user_)){
		$user_ = pb_user_by_user_login($email_or_login_);
	}

	if(!isset($user_) || empty($user_)){
		return new WP_Error('notexists', '해당 아이디와 이메일로 가입한 이력이 없습니다.');
	}

	if(!wp_check_password($pass_, $user_->user_pass, $user_->ID)){
		return new WP_Error('notvalid', '암호가 정확하지 않습니다.');
	}

	wp_set_auth_cookie($user_->ID, $remember_);
	return true;
}


define("PB_USER_FINDPASS_VKEY_NOTVALID", -1);
define("PB_USER_FINDPASS_VKEY_EXPIRED", -2);
define("PB_USER_FINDPASS_VKEY_NOTFOUND", -3);

//암호변경용 키 메일 발송
function pb_user_send_email_for_password($user_email_){
	$user_data_ = pb_user_by_user_email($user_email_);

	if(!isset($user_data_) || empty($user_data_)){
		return new WP_Error(PB_USER_FINDPASS_VKEY_NOTFOUND, '해당 이메일로 가입이력이 존재하지 않습니다.');
	}

	$user_id_ = $user_data_->ID;
	$validation_key_ = pb_user_gen_findpass_validation_key($user_id_);
	$settings_ = pb_settings();

	$validation_url_ = pb_user_resetpass_url($user_email_, $validation_key_);

	ob_start();
?>

<p>하단 링크를 클릭하여 새로운 비밀번호를 설정하세요.</p>
<br/><br/>
<p>
	<a href="<?=$validation_url_?>">새로운 비밀번호 설정</a>
</p>

<?php
	$mail_content_ = ob_get_clean();
	$mail_title_ = "[".get_bloginfo('name')."] 비밀번호 재설정 안내";

	return pb_mail_send($user_email_, $mail_title_, array(
		'content' => $mail_content_,
	));
}

//비밀번호찾기 검증키 생성
function pb_user_gen_findpass_validation_key($user_id_){
	$validation_key_ = pb_random_string(20);

	pb_user_update($user_id_, array(
		"findpass_vkey" => $validation_key_,
		"findpass_vkey_date" =>current_time('mysql')
	));

	return $validation_key_;
}

//비밀번호찾기 검증키 검증
function pb_user_check_findpass_validation_key($user_id_, $validation_key_, $expired_ = 30){
	$user_ = pb_user($user_id_);

	if(!isset($user_) || empty($user_)){
		return new WP_Error(PB_USER_FINDPASS_VKEY_NOTFOUND, '해당 이메일로 가입이력이 존재하지 않습니다.');
	}

	$stored_validation_key_ = $user_->findpass_vkey;

	if($validation_key_ !== $stored_validation_key_){
		return new WP_Error(PB_USER_FINDPASS_VKEY_NOTVALID, '비밀번호인증키가 잘못되었습니다.');
	}

	global $wpdb;

	$query_ = "select datediff(NOW(), date({$wpdb->users}.findpass_vkey_date)) CNT
			from {$wpdb->users} WHERE ID = {$user_id_} ";

	$exprie_day_count_ = $wpdb->get_var($query_);

	if($exprie_day_count_ > $expired_){
		return new WP_Error(PB_USER_FINDPASS_VKEY_EXPIRED, '비밀번호인증키가 만료되었습니다.');
	}

	return true;
}
//비밀번호찾기 검증키 삭제
function pb_user_remove_findpass_validation_key($user_id_){
	pb_user_update($user_id_, array(
		"findpass_vkey" => null,
		"findpass_vkey_date" => null
	));
}

//자동로그인 사용키 생성
function pb_user_gen_autologin_key($user_id_){
	$autologin_key_ = date("Ymd").time().pb_random_string(3, PB_RANDOM_STRING_NUM);

	pb_user_update($user_id_, array(
		"authcode" => $autologin_key_
	));

	return $autologin_key_;
}

//자동로그인 사용키 삭제
function pb_user_remove_autologin_key($user_id_){
	pb_user_update($user_id_, array(
		"authcode" => null
	));
}


//마이페이지 메뉴내역
function pb_user_mypage_menu_list(){
	$result_ =  apply_filters('pb_user_mypage_menu_list', array(
		'change-myinfo' => array(
			'title' => '내정보변경',
			'content-templete' => 'user/'.PB_USER_MYPAGE_SLUG.'/change-myinfo',
			'sort_num' => 10,
		),
		'logout' => array(
			'title' => '로그아웃',
			'url' => pb_user_logout_url(),
			'sort_num' => 9999,
		),
	));

	uasort($result_, function($a_, $b_){

		if(!isset($a_['sort_num'])) $a_['sort_num'] = 10;
		if(!isset($b_['sort_num'])) $b_['sort_num'] = 10;

		return ($a_['sort_num'] < $b_['sort_num'] ? -1 : 1);
	});

	return $result_;
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/user/class.PB-user-settings.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/user/class.PB-user-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/user/class.PB-user-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/user/class.PB-user-adminpage.php');

?>