<?php

add_filter('pb_adminpage_list', function($results_){

	$results_['user-manage'] = array(
		'title' => '사용자관리' ,
		'template-data' => array(

			'data' => array(
				'total-count' => function(){
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";						
					return pb_user_list(array(
						'keyword' => $keyword_,
						'just_count' => true,
					));
				},
				'list' => function($offset_, $limit_){
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					
					return pb_user_list(array(
						'keyword' => $keyword_,
						'limit' => array($offset_, $limit_),
					));
				},
				'single' => function($action_data_){
					return pb_user($action_data_);
				},
			),

			'options' => array(
				'list-options' => array(
					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 사용자가 없습니다.',
					),
				),
				'edit-options' => array(
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => '거래처추가확인',
						'add-confirm-message' => '해당 거래처를 추가하시겠습니까?',
						'add-success-title' => '추가완료',
						'add-success-message' => '거래처가 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => '거래처 추가 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => '거래처정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => '거래처 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '거래처삭제확인',
						'remove-confirm-message' => '해당 거래처를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => '거래처정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => '거래처 삭제 중 에러가 발생하였습니다.',
					),
				),
				'id-format' => 'pb-client-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){
					$result_ = pb_user_update($target_data_['client_id'], $target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '거래처 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['client_id'];
				},
				'add' => function($target_data_){
					$result_ = pb_user_add($target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '거래처 추가 중 에러가 발생했습니다.');
					}


					return $result_;
				},
				'remove' => function($target_data_){
					pb_user_remove($target_data_['client_id']);
					return true;
				},
			),
			'templates' => array(
				'list' => array(
					'user_login' => array(
						'column-label' => '아이디',
						'th-class' => "col-3",
						'td-class' => "col-3"
					),
					'full_name' => array(
						'column-label' => '사용자명',
						'th-class' => "col-3",
						'td-class' => "col-3",
					),
					'user_email' => array(
						'column-label' => '이메일',
						'th-class' => "col-2 text-center hidden-xs",
						'td-class' => "col-2 text-center hidden-xs",
					),
					'user_status' => array(
						'column-label' => '상태',
						'th-class' => "col-2 text-center hidden-xs",
						'td-class' => "col-2 text-center hidden-xs",
					),
					
				),
				'edit' => array(

					
					
				)
			),

		),
		'lvl' => 2,
		'sort_num' => 9,
	);

	return $results_;
});

?>