<?php

define('PB_USER_LOGIN_SLUG','login');
define('PB_USER_SIGNUP_SLUG','signup');
define('PB_USER_MYPAGE_SLUG','mypage');
define('PB_USER_RESETPASS_SLUG','resetpass');
define('PB_USER_LOGOUT_SLUG','logout');

function pb_user_login_url($redirect_url_ = null){
	$login_url_ = home_url(PB_USER_LOGIN_SLUG);

	if(strlen($redirect_url_)){
		$login_url_ = pb_make_url($login_url_, array(
			'redirect_url' => $redirect_url_,
		));
	}

	return $login_url_;
}

function pb_user_signup_url($redirect_url_ = null){
	$target_url_ = home_url(PB_USER_SIGNUP_SLUG);

	if(strlen($redirect_url_)){
		$target_url_ = pb_make_url($target_url_, array(
			'redirect_url' => $redirect_url_,
		));
	}

	return $target_url_;
}

function pb_user_mypage_url($action_ = null){
	$target_url_ = home_url(PB_USER_MYPAGE_SLUG);

	if(strlen($action_)){
		$target_url_ = pb_append_url($target_url_, $action_);
	}

	return $target_url_;
}

function pb_user_resetpass_url($email_, $vkey_){
	$target_url_ = pb_make_url(home_url(PB_USER_RESETPASS_SLUG), array(
		'email' => $email_,
		'vkey' => $vkey_,
	));
	return $target_url_;
}

function pb_user_logout_url($redirect_url_ = null){
	$target_url_ = home_url(PB_USER_LOGOUT_SLUG);

	if(strlen($redirect_url_)){
		$target_url_ = pb_make_url($target_url_, array(
			'redirect_url' => $redirect_url_,
		));
	}

	return $target_url_;
}

add_filter("pb_default_settings", function($settings_){
	return array_merge($settings_,array(
		"user_need_auth_myself" => "N",
		"user_need_auth_email" => "Y",
	));
});

function pb_user_need_auth_myself(){
	return pb_settings('user_need_auth_myself') === "Y";
}
function pb_user_need_auth_email(){
	// return pb_settings('user_need_auth_email') === "Y";

	return false;
}


?>