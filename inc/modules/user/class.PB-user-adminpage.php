<?php

add_filter('pb_adminpage_list', function($results_){

	$results_['user-manage'] = array(
		'title' => '사용자관리' ,
		'template-data' => array(

			'data' => array(
				'total-count' => function(){
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";						
					return pb_user_list(array(
						'keyword' => $keyword_,
						'without_admin' => true,
						'just_count' => true,
					));
				},
				'list' => function($offset_, $limit_){
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					
					return pb_user_list(array(
						'keyword' => $keyword_,
						'without_admin' => true,
						'limit' => array($offset_, $limit_),
					));
				},
				'single' => function($action_data_){
					return pb_user($action_data_);
				},
			),

			'options' => array(
				'not-allow-add' => true,
				'not-allow-remove' => true,
				'list-options' => array(
					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 사용자가 없습니다.',
					),
					'custom-search-frame' => function($options_, $table_options_){

						$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;

						?>
<div class="search-frame minus-margin-top">
	
	<div class="row">
		
			<div class="col-xs-12 col-sm-8">
				<label>&nbsp; &nbsp;&nbsp;</label><br/>
				<a href="#" onclick="javascript:pb_adminpage_download_excel('<?=pb_adminpage_url('user-manage')?>','#pb-adminpage-templated-list-table-form');" class="btn btn-sm btn-default" id="pb-user-excel">엑셀다운로드</a>
				<div class="form-margin-xs visible-xs"></div>
			</div>
	
			<div class="col-xs-8 col-sm-3">
				<label>검색어</label>
				<input type="text" class="form-control input-sm" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
			</div>
			<div class="col-xs-4 col-sm-1">
				<label>&nbsp; &nbsp;&nbsp;</label><br/>
				<!-- <div class="form-margin-xs visible-xs"></div> -->
				<button class="btn btn-default btn-sm btn-block" type="submit">검색</button>
			</div>
		
	</div>
	
</div>
<hr>
						<?php
					},
				),
				'edit-options' => array(
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => '거래처추가확인',
						'add-confirm-message' => '해당 거래처를 추가하시겠습니까?',
						'add-success-title' => '추가완료',
						'add-success-message' => '거래처가 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => '거래처 추가 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => '거래처정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => '거래처 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '거래처삭제확인',
						'remove-confirm-message' => '해당 거래처를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => '거래처정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => '거래처 삭제 중 에러가 발생하였습니다.',
					),
					'custom-bottom-button' => function($data_, $is_new_){

						
						?>
						
						<a href="javascript:pb_user_manage_send_reset_password(<?=$data_['ID']?>);" class="btn btn-default">비밀번호재설정</a>
						<button type="submit" class="btn btn-primary">변경사항 저장</button>

						<?php
					}
				),
				'id-format' => 'pb-client-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){

					$target_data_['mailing_recv_yn'] = isset($target_data_['mailing_recv_yn']) ? $target_data_['mailing_recv_yn'] : null;
					$target_data_['sms_recv_yn'] = isset($target_data_['mailing_recv_yn']) ? $target_data_['sms_recv_yn'] : null;

					$result_ = pb_user_update($target_data_['ID'], $target_data_);

					if(is_wp_error($result_)){
						return new WP_Error('수정실패', '사용자 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['ID'];
				},
			),
			'templates' => array(
				'list' => array(
					'seq' => array(
						'column-label' => 'No',
						'th-class' => "col-seq text-center hidden-xs",
						'td-class' => "col-seq text-center hidden-xs",
						'td-render-func' => function($item_, $column_name_){
							global $_temp_row_index;
							if(empty($_temp_row_index)){
								$_temp_row_index = 1;
							}

							echo $_temp_row_index;


							++$_temp_row_index;
						},
					),
					
					'full_name' => array(
						'column-label' => '사용자명',
						'th-class' => "col-3",
						'td-class' => "col-3",
					),
					'user_login' => array(
						'column-label' => '아이디',
						'th-class' => "col-3",
						'td-class' => "col-3"
					),
					'user_email' => array(
						'column-label' => '이메일',
						'th-class' => "col-3 text-center hidden-xs",
						'td-class' => "col-3 text-center hidden-xs",
					),
					'phone1' => array(
						'column-label' => '연락처',
						'th-class' => "col-3 text-center",
						'td-class' => "col-3 text-center",
					),
					'button_area' => array(
						'column-label' => '',
						'th-class' => "col-2 text-center",
						'td-class' => "col-2 text-center",
						'td-render-func' => function($item_, $column_name_){ ?>
							<a href="<?=pb_adminpage_url("user-manage/edit/".$item_->ID)?>" class="btn btn-default btn-sm">상세</a>
						<?php }
					),
					
				),
				'edit' => array(
					array(
						'column-name' => 'ID',
						'type' => 'hidden',
					),
					array(
						'type' => 'row',
						'columns' => array(
						

							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => 'ID',
								'column-name' => 'user_login',
								'type' => 'static',
							),
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '이메일',
								'column-name' => 'user_email',
								'type' => 'static',
							),
							
							
						),
						
					),
					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '이름',
								'column-name' => 'full_name',
								'placeholder' => '이름 입력',
								'validates' => array(
									'required' => "이름을 입력하세요",
								),
								'type' => 'text',
							),
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '성별',
								'column-name' => 'gender',
								'inline' => true,
								'validates' => array(
									'required' => "성별을 선택하세요",
								),
								'type' => 'radio',
								'options' => array(
									'00001' => '남자',
									'00003' => '여자'
								)
							),
							
							
						),
						
					),

						
					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-6',
								'type' => 'custom',
								'view' => function($attr_, $options_, $data_){
									?>
							

						<label>휴대전화</label>		
						<div class="form-group" >
							
							<div class="col-sm-4 col-xs-4 col-sm-phone">
								<select class="form-control" name="phone1_1" data-error="앞자리를 선택하세요" required id="pb-user-manage-form-phone1_1">
									<option value="">-앞자리-</option>
									<?= pb_gcode_make_options('Z0003', $data_['phone1_1']) ?>
								</select>
							</div>
							<div class="col-sm-4 col-xs-4 col-sm-phone">
								<input type="number" name="phone1_2" class="form-control" maxlength="4" data-error="중간자리를 입력하세요" required id="pb-user-manage-form-phone1_2" value="<?=$data_['phone1_2']?>">
							</div>
							<div class="col-sm-4 col-xs-4">
								<input type="number" name="phone1_3" class="form-control" maxlength="4" data-error="마지막자리를 입력하세요" required id="pb-user-manage-form-phone1_3" value="<?=$data_['phone1_3']?>">
							</div>
							<div class="help-block with-errors"></div>
							<div class="clearfix"></div>

						</div>

						<div class="form-margin"></div>

									<?php
								}
								
							),
							
						),
						
					),
					array(
						'type' => 'row',
						'columns' => array(
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => '이메일수신동의',
								'column-name' => 'mailing_recv_yn',
								'inline' => true,
								'type' => 'checkbox',
								'options' => array(
									'Y' => '수신동의',
								)
							),
							array(
								'column' => 'col-xs-12 col-sm-6',
								'column-label' => 'SMS수신동의',
								'column-name' => 'sms_recv_yn',
								'inline' => true,
								'type' => 'checkbox',
								'options' => array(
									'Y' => '수신동의',
								)
							),
							
						),
						
					),
				/*	array(
						'column-label' => '사용자상태',
						'column-name' => 'user_status_name',
						'type' => 'static',
					),*/
			
					
				)
			),

		),
		'lvl' => 2,
		'sort_num' => 9,
	);

	return $results_;
});

pb_add_ajax('adminpage-user-manage-send-reset-password', function(){
	if(!pb_is_admin()){
		echo json_encode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$user_id_ = $_POST['user_id'];

	if(!strlen($user_id_)){
		echo json_encode(array(
			'success' => false,
			'invalid_request' => true,
		));
		die();
	}

	$target_user_ = pb_user($user_id_);
	$user_email_ = $target_user_->user_email;

	pb_user_send_email_for_password($user_email_);

	echo json_encode(array(
		'success' => true,
	));
	die();

});

?>