<?php


//이메일인증코드발송
pb_add_ajax('user-send-authcode-for-email', function(){

	$email_ = isset($_POST['email']) ? $_POST['email'] : null;

	if(!strlen($email_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청',
			'error_message' => '필수항목이 비어있습니다',
		));
		die();
	}

	$authcode_ = pb_user_send_authcode_for_email($email_);

	if(is_wp_error($authcode_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '인증메일발송실패',
			'error_message' => '인증메일 발송 중, 에러가 발생했습니다.',
		));
		die();
	}

	echo json_encode(array(
		'success' => true,
	));
	die();
});

//이메일인증코드검증
pb_add_ajax('user-check-authcode-for-email', function(){

	$email_ = isset($_POST['email']) ? $_POST['email'] : null;
	$authcode_ = isset($_POST['authcode']) ? $_POST['authcode'] : null;

	if(!strlen($email_) || !strlen($authcode_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청',
			'error_message' => '필수항목이 비어있습니다',
		));
		die();
	}

	$result_ = pb_user_check_authcode_for_email($email_, $authcode_);

	if(!$result_){
		echo json_encode(array(
			'success' => false,
			'error_title' => '인증실패',
			'error_message' => '인증코드가 잘못되었거나 정확하지 않습니다.',
		));
		die();
	}

	echo json_encode(array(
		'success' => true,
	));
	die();
});


//이메일존재여부확인
pb_add_ajax('user-check-exists-email', function(){
	$key_name_ = isset($_REQUEST['key']) ? $_REQUEST['key'] : "user_email";
	$email_ = $_REQUEST[$key_name_];
	$reverse_ = (isset($_REQUEST['reverse']) ? $_REQUEST['reverse'] === "Y" : false);

	$user_data_ = pb_user_by_user_email($email_);
	$check_ = (isset($user_data_) && !empty($user_data_));

	if($reverse_) $check_ = !$check_;

	if($check_){
		echo 1;
		die();
	}else{
		http_response_code(404);
		die();
	}
});

//아아디존재여부확인
pb_add_ajax('user-check-exists-user-login', function(){
	$key_name_ = isset($_REQUEST['key']) ? $_REQUEST['key'] : "user_login";
	$email_ = $_REQUEST[$key_name_];
	$reverse_ = (isset($_REQUEST['reverse']) ? $_REQUEST['reverse'] === "Y" : false);

	$user_data_ = pb_user_by_user_login($email_);
	$check_ = (isset($user_data_) && !empty($user_data_));

	if($reverse_) $check_ = !$check_;

	if($check_){
		echo 1;
		die();
	}else{
		http_response_code(404);
		die();
	}
});

//사용자존재여부확인
pb_add_ajax('user-check-exists', function(){
	$key_name_ = isset($_REQUEST['key']) ? $_REQUEST['key'] : "login";
	$login_ = $_REQUEST[$key_name_];
	$reverse_ = (isset($_REQUEST['reverse']) ? $_REQUEST['reverse'] === "Y" : false);

	$user_data_ = pb_user_by_user_login($login_);
	$check_ = (isset($user_data_) && !empty($user_data_));

	if(!$check_){
		$user_data_ = pb_user_by_user_email($login_);
		$check_ = (isset($user_data_) && !empty($user_data_));	
	}
	

	if($reverse_) $check_ = !$check_;

	if($check_){
		echo 1;
		die();
	}else{
		http_response_code(404);
		die();
	}
});

define('PB_USER_SIGNUP_COMPLETED', '_PB_USER_SIGNUP_COMPLETED_');

//회원가입처리
pb_add_ajax('user-signup', function(){
	$signup_data_ = isset($_POST['signup_data']) ? $_POST['signup_data'] : null;

	if(empty($signup_data_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '회원가입실패',
			'error_message' => '잘못된 접근입니다.'
		));
		die();
	}

	$signup_data_ = apply_filters('pb_decrypt', $signup_data_, array('user_pass'));
	$need_auth_email_ = pb_user_need_auth_email();

	if($need_auth_email_ && !pb_user_check_authcode_for_email($signup_data_['user_email'], $signup_data_['authcode_email'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => '회원가입실패',
			'error_message' => '이메일 인증 데이타변조가 발생했습니다.',
		));
		die();
	}

	$result_ = pb_user_add($signup_data_);

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '회원가입실패',
			'error_message' => $result_->get_error_message(),
		));
		die();
	}

	pb_session_put(PB_USER_SIGNUP_COMPLETED, true);
	echo json_encode(array(
		'success' => true,
	));
	die();
});

//로그인처리
pb_add_ajax('user-login', function(){
	$login_data_ = isset($_POST['login_data']) ? $_POST['login_data'] : null;

	if(empty($login_data_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '회원가입실패',
			'error_message' => '잘못된 접근입니다.'
		));
		die();
	}

	$login_data_ = apply_filters('pb_decrypt', $login_data_, array('user_pass'));

	$result_ = pb_user_login($login_data_['login_or_email'], $login_data_['user_pass'], ($login_data_['remember'] === "Y"));

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '로그인실패',
			'error_message' => $result_->get_error_message(),
		));
		die();
	}
	echo json_encode(array(
		'success' => true,
	));
	die();
});

//비밀번호 재설정 링크 발송
pb_add_ajax('user-findpass' , function(){
	$user_email_ = isset($_POST['user_email']) ? $_POST['user_email'] : null;	

	if(!strlen($user_email_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청',
			'error_message' => '필수값이 빠져있습니다.'
		));
		die();
	}

	$result_ = pb_user_send_email_for_password($user_email_);

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '이메일발송실패',
			'error_message' => $result_->get_error_message(),
		));
		die();
	}

	echo json_encode(array(
		'success' => true,
	));
	die();	
});

//비밀번호 재설정
pb_add_ajax('user-resetpass' , function(){
	$request_data_ = isset($_POST['request_data']) ? $_POST['request_data'] : null;	
	$request_data_ = apply_filters("pb_decrypt", $request_data_, array("user_pass"));

	if(empty($request_data_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청',
			'error_message' => '필수항목이 빠져있습니다.'
		));
		die();
	}

	$user_id_ = $request_data_['ID'];
	$vkey_ = $request_data_['vkey'];

	$result_ = pb_user_check_findpass_validation_key($user_id_, $vkey_);

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '인증실패',
			'error_message' => $result_->get_error_message(),
		));
		die();	
	}

	wp_set_password($request_data_['user_pass'], $user_id_);
	pb_user_remove_findpass_validation_key($user_id_);

	echo json_encode(array(
		'success' => true,
	));
	die();	
});

//내정보 변경
pb_add_ajax('user-change-myinfo', function(){
	if(!is_user_logged_in()){
		echo json_encode(array(
			'success' => false,
			'error_title' => '변경실패',
			'error_message' => '잘못된 접근입니다.',
		));
		die();
	}
	$update_data_ = isset($_POST['update_data']) ? $_POST['update_data'] : null;	
	$update_data_ = apply_filters("pb_decrypt", $update_data_, array("user_pass"));

	$user_data_ = pb_user(get_current_user_id());

	if(!wp_check_password($update_data_['user_pass'], $user_data_->user_pass, $user_data_->ID)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '변경실패',
			'error_message' => '암호가 정확하지 않습니다.',
		));
		die();
	}	

	unset($update_data_['user_pass']);
	if(isset($update_data_['user_login'])) unset($update_data_['user_login']);
	if(isset($update_data_['user_email'])) unset($update_data_['user_email']);
	
	$result_ = pb_user_update($user_data_->ID, $update_data_);

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '변경실패',
			'error_message' => '내정보변경 중 에러가 발생했습니다. '.$result_->get_error_message(),
		));
		die();
	}

	echo json_encode(array(
		'success' => true,
	));
	die();
});

//비밀번호 변경
pb_add_ajax('user-change-password', function(){
	if(!is_user_logged_in()){
		echo json_encode(array(
			'success' => false,
			'error_title' => '변경실패',
			'error_message' => '잘못된 접근입니다.',
		));
		die();
	}
	$update_data_ = isset($_POST['update_data']) ? $_POST['update_data'] : null;	
	$update_data_ = apply_filters("pb_decrypt", $update_data_, array("current_pass", "new_pass"));

	$user_data_ = pb_user(get_current_user_id());
	
	if(!wp_check_password($update_data_['current_pass'], $user_data_->user_pass, $user_data_->ID)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '변경실패',
			'error_message' => '암호가 정확하지 않습니다.',
		));
		die();
	}

	wp_set_password($update_data_['new_pass'], $user_data_->ID);
	echo json_encode(array(
		'success' => true,
	));
	die();
});

//비밀번호 변경
pb_add_ajax('user-leave', function(){
	if(!is_user_logged_in()){
		echo json_encode(array(
			'success' => false,
			'error_title' => '탈퇴실패',
			'error_message' => '잘못된 접근입니다.',
		));
		die();
	}
	$request_data_ = isset($_POST['request_data']) ? $_POST['request_data'] : null;	
	$request_data_ = apply_filters("pb_decrypt", $request_data_, array("user_pass"));

	$user_data_ = pb_user(get_current_user_id());

	if(pb_is_admin()){
		echo json_encode(array(
			'success' => false,
			'error_title' => '탈퇴실패',
			'error_message' => '관리자는 탈퇴할 수 없습니다.',
		));
		die();
	}
	
	if(!wp_check_password($request_data_['user_pass'], $user_data_->user_pass, $user_data_->ID)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '탈퇴실패',
			'error_message' => '암호가 정확하지 않습니다.',
		));
		die();
	}

	wp_logout();
	
	require_once( ABSPATH . 'wp-admin/includes/admin.php' );
    require_once( ABSPATH . 'wp-admin/includes/user.php' );
	
	$result_ = wpmu_delete_user($user_data_->ID);

	echo json_encode(array(
		'success' => $result_,
	));
	die();
});

add_action('init', function(){

});

?>