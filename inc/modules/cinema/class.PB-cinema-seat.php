<?php

function pb_cinema_seat_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
					 {$wpdb->pbcinema_seat}.seat_id seat_id
					,{$wpdb->pbcinema_seat}.seat_name seat_name
					,{$wpdb->pbcinema_seat}.seat_desc seat_desc
					,{$wpdb->pbcinema_seat}.seat_data seat_data

					,{$wpdb->pbcinema_seat}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_id cinema_name

					,{$wpdb->pbcinema_seat}.replace_image_yn replace_image_yn
					,{$wpdb->pbcinema_seat}.replace_image_url replace_image_url

					,{$wpdb->pbcinema_seat}.status status
					,".pb_query_gcode_dtl_name("CS001", "{$wpdb->pbcinema_seat}.status")." status_name

					,{$wpdb->pbcinema_seat}.sort_num sort_num
					
					,{$wpdb->pbcinema_seat}.reg_date reg_date
					,DATE_FORMAT({$wpdb->pbcinema_seat}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

					,{$wpdb->pbcinema_seat}.mod_date mod_date
					,DATE_FORMAT({$wpdb->pbcinema_seat}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

				
		     FROM {$wpdb->pbcinema_seat}

		     LEFT OUTER JOIN {$wpdb->pbcinema}
		     ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbcinema_seat}.cinema_id

		      ";

	$query_ .= apply_filters('pb_cinema_seat_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['seat_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['seat_id'], "{$wpdb->pbcinema_seat}.seat_id")." ";
	}
	if(isset($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbcinema_seat}.cinema_id")." ";
	}

	if(isset($conditions_['status'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['status'], "{$wpdb->pbcinema_seat}.status")." ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_cinema_seat_list_keyword', array(
			"{$wpdb->pbcinema_seat}.seat_name",
			"{$wpdb->pbcinema}.cinema_name",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_cinema_seat_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_cinema_seat_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY sort_num asc';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_cinema_seat_list', $wpdb->get_results($query_));
	
	return $results_;
}

add_filter('pb_cinema_seat_list', function($results_){
	foreach($results_ as &$row_data_){
		$row_data_->seat_data = json_decode($row_data_->seat_data, true);
	}

	return $results_;
});

function pb_cinema_seat($seat_id_){
	if(!strlen($seat_id_)) return null;

	$result_ = pb_cinema_seat_list(array('seat_id' => $seat_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_cinema_seat_fields($data_){
	return pb_format_mapping(apply_filters("pb_cinema_seat_fields_map",array(
	
		'seat_name' => '%s',
		'seat_desc' => '%s',
		'seat_data' => '%s',
		'cinema_id' => '%d',
		'status' => '%s',
		'sort_num' => '%d',

		'replace_image_yn' => '%s',
		'replace_image_url' => '%s',
		

	)), $data_);
}

function pb_cinema_seat_add($seat_data_){
	$insert_data_ = _pb_parse_cinema_seat_fields($seat_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbcinema_seat}", $insert_value_, $insert_format_);

	if(!$result_) return new WP_Error("INSERT_FAILED", "좌석배치도를 추가할 수 없습니다.");

	$seat_id_ = $wpdb->insert_id;

	do_action('pb_cinema_seat_added', $seat_id_);

	return $seat_id_;
}

function pb_cinema_seat_update($seat_id_, $seat_data_){
	$insert_data_ = _pb_parse_cinema_seat_fields($seat_data_);

	$update_value_ = $insert_data_['data'];
	$update_format_ = $insert_data_['format'];

	$update_value_['mod_date'] = current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbcinema_seat}", $update_value_, array("seat_id" => $seat_id_), $update_format_, array("%d"));

	if(!$result_) return new WP_Error("UPDATE_FAILED", "좌석배치도를 수정할 수 없습니다.");

	do_action('pb_cinema_seat_updated', $seat_id_);

	return $seat_id_;
}

function pb_cinema_seat_remove($seat_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbcinema_seat}", array("seat_id" => $seat_id_), array("%d"));

	if(!$result_) return $result_;

	do_action('pb_cinema_seat_removed', $seat_id_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema-seat-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema-seat-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema-seat-adminpage.php');

?>