<?php

add_filter('pb_adminpage_list', function($results_){

	if(pb_cinema_current_is_head_office()){
		$results_['cinema-seat-manage'] = array(
			'title' => '좌석배치도관리' ,
			'content-template' => "cinema-seat-manage/list",
			'subcontent-templates' => array(
				"dtl" => "cinema-seat-manage/dtl",
			),
			'lvl' => 2,
			'sort_num' => 12,
		);	
	}else{
		$results_['cinema-seat-manage'] = array(
			'title' => '좌석배치도관리' ,
			'content-template' => "cinema-seat-manage/dtl",
			'lvl' => 1,
			'sort_num' => 12,
		);
	}

	return $results_;
});


?>