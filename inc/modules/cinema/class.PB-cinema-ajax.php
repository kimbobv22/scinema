<?php

//도메인 존재여부 체크
pb_add_ajax('cinema-check-domain', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$email_ = isset($_REQUEST['domain']) ? $_REQUEST['domain'] : null;
	
	$cinema_data_ = pb_cinema_by_domain($email_);
	$check_ = !(isset($cinema_data_) && !empty($cinema_data_));

	if($check_){
		echo 1;
		die();
	}else{
		http_response_code(404);
		die();
	}
});

//상영관 업데이트
pb_add_ajax('cinema-update', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$cinema_data_ = isset($_POST['cinema_data']) ? $_POST['cinema_data'] : null;
	
	if(empty($_POST['cinema_data'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}

		$cinema_id_ = null;

	if(!strlen($cinema_data_['cinema_id'])){ // new

		$admin_data_ = pb_user_by_both($cinema_data_['admin_login']);

		if(empty($admin_data_) && strlen($cinema_data_['admin_pass'])){
			$user_id_ = wp_insert_user(array(
				'user_login'  =>  $cinema_data_['admin_login'],
				'user_email'  =>  $cinema_data_['admin_login']."@scinema.org",
			    'user_pass'   =>  $cinema_data_['admin_pass'],
			));
			$admin_id_ = $user_id_;
		}else{
			$admin_id_ = $admin_data_->ID;

			if(strlen($cinema_data_['admin_pass'])){
				wp_set_password($cinema_data_['admin_pass'], $admin_id_);	
			}
			
		}

		$cinema_data_['admin_id'] = $admin_id_;
		$cinema_data_['replace_location_html_yn'] = isset($cinema_data_['replace_location_html_yn']) ? $cinema_data_['replace_location_html_yn'] : "N";
		$cinema_id_ = pb_cinema_add($cinema_data_);

		if(is_wp_error($cinema_id_)){
			echo json_encode(array(
				'success' => false,
				'error_title' => '상영관추가실패',
				'error_message' => $cinema_id_->get_error_message(),
			));
			die();			
		}

		update_blog_option( $cinema_id_, "description", $cinema_data_['search_desc']);
		update_blog_option( $cinema_id_, "blog_public", $cinema_data_['search_desc']);

		echo json_encode(array(
			'success' => true,
			'cinema_id' => $cinema_id_,
			'success_title' => '상영관추가완료',
			'success_message' => '상영관추가가 완료되었습니다.',
			'redirect_url' => pb_adminpage_url("cinema-manage/edit/".$cinema_id_),
		));
		die();
	}else{

		// add_user_to_blog($cinema_id_, $admin_id_, PB_ADMINLVL_ADMIN);

		$admin_data_ = pb_user_by_both($cinema_data_['admin_login']);

		if(empty($admin_data_) && strlen($cinema_data_['admin_pass'])){
			$user_id_ = wp_insert_user(array(
				'user_login'  =>  $cinema_data_['admin_login'],
				'user_email'  =>  $cinema_data_['admin_login']."@scinema.org",
			    'user_pass'   =>  $cinema_data_['admin_pass'],
			));
			$new_admin_id_ = $user_id_;
		}else{
			$new_admin_id_ = $admin_data_->ID;

			if(strlen($cinema_data_['admin_pass'])){
				wp_set_password($cinema_data_['admin_pass'], $new_admin_id_);	

			}
		}

		$before_data_ = pb_cinema($cinema_data_['cinema_id']);

		if((string)$before_data_->cinema_id !== $new_admin_id_){
			add_user_to_blog($cinema_data_['cinema_id'], $new_admin_id_, PB_ADMINLVL_ADMIN);
			$cinema_data_['admin_id'] = $new_admin_id_;
		}

		$cinema_data_['replace_location_html_yn'] = isset($cinema_data_['replace_location_html_yn']) ? $cinema_data_['replace_location_html_yn'] : "N";

		pb_cinema_update($cinema_data_['cinema_id'], $cinema_data_);

		update_blog_option($cinema_data_['cinema_id'], "blogdescription", $cinema_data_['search_desc']);

		echo json_encode(array(
			'success' => true,
			'cinema_id' => $cinema_id_,
			'success_title' => '상영관수정완료',
			'success_message' => '상영관수정 완료되었습니다.',
			'redirect_url' => pb_adminpage_url("cinema-manage/edit/".$cinema_data_['cinema_id']),
		));
		die();
	}

	
});
/*
//상영관 삭제
pb_add_ajax('cinema-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$cinema_id_ = isset($_POST['cinema_id']) ? $_POST['cinema_id'] : null;
	
	if(!strlen($cinema_id_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}

	$before_data_ = pb_cinema($cinema_id_);

	if($before_data_->ticket_ref_type !== "custom001"){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '관리자페이지에서 삭제가 불가능한 영화관입니다.'
		));
		die();		
	}	

	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbcinema}", array("cinema_id" => $cinema_id_), array("%d"));	

	echo json_encode(array(
		'success' => true,
		'success_title' => '삭제성공' ,
		'success_message' => '영화관삭제가 정상적으로 처리되었습니다.',
		'redirect_url' => pb_adminpage_url("cinema-manage"),
	));
	die();		
	
});*/

?>