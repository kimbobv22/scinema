<?php

function pb_admin_board_import($data_, $delete_before_ = false){
	global $wpdb;

	if($delete_before_){
		$wpdb->query("DELETE FROM {$wpdb->pbboard_comments}");
		$wpdb->query("DELETE FROM {$wpdb->pbboard_posts}");
		$wpdb->query("DELETE FROM {$wpdb->pbboard}");
	}


	


}

function pb_admin_board_export(){
	$results_ = array();
	$results_['boards'] = pb_boards();
	$results_['posts'] = pb_board_posts();
	$results_['comments'] = pb_board_comments();

	return $results_;


	/*header('Content-type: text/xml');
	header('Content-Disposition: attachment; filename="pbboard-exported.xml"');



	die();*/

}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/board/class.PB-admin-board-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/board/class.PB-admin-board-list.php');

foreach(glob(PB_THEME_DIR_PATH . 'pbboard-skins/**/lib-admin.php') as $filename_){
    include_once $filename_;
}

?>