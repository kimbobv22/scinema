<?php

add_action('init', function(){
	global $wpdb;

	$wpdb->pbcinema_seat = "{$wpdb->base_prefix}cinema_seat";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}cinema_seat` (
		`seat_id` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`seat_name` varchar(200)  DEFAULT NULL COMMENT '좌석명',
		`seat_desc` varchar(500)  DEFAULT NULL COMMENT '좌석설명',
		`seat_data` longtext  DEFAULT NULL COMMENT '좌석JSON',

		`replace_image_yn` varchar(1)  DEFAULT 'N' COMMENT '이미지대체여부',
		`replace_image_url` varchar(500)  DEFAULT null COMMENT '대체이미지',

		`status` varchar(5)  DEFAULT NULL COMMENT '상태',
		`cinema_id` bigint(20)  NOT NULL COMMENT '상영관ID',
		
		`sort_num` int(3)  DEFAULT NULL COMMENT '순서',

		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`seat_id`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='영화관 - 좌석';";

	return $args_;
},10, 3);	

add_filter('pb_initial_gcode_list', function($gcode_list_){

	$gcode_list_['CS001'] = array(
		'name' => '좌석상태',
		'data' => array(
			'00001' => '정상등록',
			'00003' => '숨김',
		),
	);

	$gcode_list_['CS003'] = array(
		'name' => '좌석관리분류',
		'data' => array(
			'00001' => '일반',
			'00003' => '라벨',
			'00005' => '여백',
			'00007' => '장애인',
			'00009' => '커플석',
		),
	);
	
	return $gcode_list_;
});

add_filter('pb_cinema_list_fields', function($query_){

	global $wpdb;

	$query_ .= " ,(select count(*) 
    			FROM {$wpdb->pbcinema_seat}
    			WHERE {$wpdb->pbcinema_seat}.cinema_id = {$wpdb->pbcinema}.cinema_id) cinema_seat_cnt ";
	return $query_;
});

?>