<?php

/************
일반페이지 빌트인
/*************/

//에디터 메타박스 빌트인
add_action('add_meta_boxes', function($post_type_, $post_){
	if($post_type_ !== "page") return;

	wp_enqueue_script("page-admin-page-board-edit", (pb_library_url() . 'js/pages/page-admin-page-board-edit.js') ,array("pb-all-admin"));

	add_meta_box(
		'pb-board-installed-pbboard',
		'게시판 설치하기',
		'_pb_board_meta_box_page_installed_pbboard',
		null,'side','core'
	);

}, 1, 2);

function _pb_board_meta_box_page_installed_pbboard($post_){
	wp_nonce_field('_pb_board_meta_box_for_page', 'pb_board_meta_box_for_page');

	$installed_pbboard_ = $post_->installed_pbboard;

	$board_list_ = pb_board_list();
?>

	<div>
		<label><code>게시판카테고리</code></label>
		<p>
			<select class="larget-text" name="installed_pbboard">
				<option value="">-게시판선택-</option>
			<?php foreach($board_list_ as $row_data_){ ?>
			
				<option value="<?=$row_data_->board_id?>" <?=selected($row_data_->board_id, $installed_pbboard_)?> ><?=$row_data_->board_name?></option>
			<?php }?>

			</select>
		</p>
	</div>

	<div data-custom-type-field></div>

<?php 
}

add_action('wp_ajax_pb_admin_board_load_admin_page_metabox_fields', function(){
	if(!pb_is_admin()){
		echo json_encode(array(
			'success' => false,
			'error_code' => 'deny',
			'error_title' => '에러발생',
			'error_message' => '잘못된 접근입니다.'
		));
		die();
	}

	$post_id_ = isset($_POST['post_id']) ? $_POST['post_id'] : null;
	$board_id_ = $_POST['board_id'];


	$post_ = null;
	if(strlen($post_id_)){
		$post_ = get_post($post_id_);
	}

	$board_data_ = pb_board($board_id_);
	
	ob_start();

	do_action('pb_board_admin_page_metabox_fields_'.$board_data_->type, $post_, $board_data_);

	$field_html_ = ob_get_clean();

	echo json_encode(array(
		'success' => true,
		'field_html' => $field_html_,
	));
	die();

});


add_action('edit_post', function($post_id_, $post_){
	if($post_->post_type !== "page") return;
	if(!isset($_POST['pb_board_meta_box_for_page'])){
		return;
	}

	$installed_pbboard_ = isset($_POST['installed_pbboard']) ? $_POST['installed_pbboard'] : null;
	update_post_meta($post_id_, 'installed_pbboard', $installed_pbboard_);

	//todo
	
},10,2);


?>