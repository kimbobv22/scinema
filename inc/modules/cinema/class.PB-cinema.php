<?php

define('PB_CINEMA_HEAD_OFFICE_ID', 1); //본사ID

function pb_cinema_current_is_head_office(){
	return (get_current_blog_id() === PB_CINEMA_HEAD_OFFICE_ID);
}
function pb_current_cinema_id(){
	return get_current_blog_id();
}
function pb_current_cinema(){
	global $pb_current_cinema;

	if(!isset($pb_current_cinema)){
		$pb_current_cinema = pb_cinema(pb_current_cinema_id());
	}
	return $pb_current_cinema;
}

/** 영화관 리스트 가져오기
 *
 * @param array 조건절
 * @return array 영화관 리스트
 */
function pb_cinema_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
					 {$wpdb->pbcinema}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_name cinema_name
					
					,{$wpdb->pbcinema}.cinema_type cinema_type
					,".pb_query_gcode_dtl_name("C0001", "{$wpdb->pbcinema}.cinema_type")." cinema_type_name
					
					,{$wpdb->pbcinema}.cinema_status cinema_status
					,".pb_query_gcode_dtl_name("C0003", "{$wpdb->pbcinema}.cinema_status")." cinema_status_name

					,{$wpdb->pbcinema}.cinema_desc cinema_desc
					,{$wpdb->pbcinema}.seattable seattable

					,{$wpdb->pbcinema}.addr addr
					,{$wpdb->pbcinema}.addr_dtl addr_dtl
					,{$wpdb->pbcinema}.addr_postcode addr_postcode
					,{$wpdb->pbcinema}.addr_lat addr_lat
					,{$wpdb->pbcinema}.addr_lng addr_lng

					,{$wpdb->pbcinema}.area_id area_id
					,".pb_query_gcode_dtl_name("C0005", "{$wpdb->pbcinema}.area_id")." area_name

					,{$wpdb->pbcinema}.fee_info fee_info
					,{$wpdb->pbcinema}.biz_no biz_no
					,{$wpdb->pbcinema}.phone1 phone1
					,{$wpdb->pbcinema}.phone2 phone2
					,{$wpdb->pbcinema}.faxnum faxnum

					,{$wpdb->pbcinema}.replace_location_html_yn replace_location_html_yn
					,{$wpdb->pbcinema}.replace_location_html replace_location_html

					,{$wpdb->pbcinema}.naver_synckey naver_synckey
					
					,{$wpdb->pbcinema}.ticket_ref_type ticket_ref_type
					,{$wpdb->pbcinema}.xticket_ref_code xticket_ref_code
					,{$wpdb->pbcinema}.logo_image_url logo_image_url

					,{$wpdb->blogs}.domain domain
					,{$wpdb->blogs}.path path

					,{$wpdb->pbcinema}.admin_id admin_id
					,admin_info.user_login admin_login 

					,'N' head_office_yn
		
				,{$wpdb->pbcinema}.reg_date reg_date
				,DATE_FORMAT({$wpdb->pbcinema}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

				,{$wpdb->pbcinema}.mod_date mod_date
				,DATE_FORMAT({$wpdb->pbcinema}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi
				
				 ".apply_filters('pb_cinema_list_fields', "", $conditions_)." 
				
		     FROM {$wpdb->pbcinema}

		     LEFT OUTER JOIN {$wpdb->blogs}
		     ON   {$wpdb->blogs}.blog_id = {$wpdb->pbcinema}.cinema_id

		     LEFT OUTER JOIN {$wpdb->users} admin_info
		     ON   admin_info.ID = {$wpdb->pbcinema}.admin_id

		      ";

	$query_ .= apply_filters('pb_cinema_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbcinema}.cinema_id")." ";
	}
	if(isset($conditions_['area_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['area_id'], "{$wpdb->pbcinema}.area_id")." ";
	}
	if(isset($conditions_['domain'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['domain'], "{$wpdb->blogs}.domain")." ";
	}

	if(isset($conditions_['cinema_status'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_status'], "{$wpdb->pbcinema}.cinema_status")." ";
	}
	if(isset($conditions_['cinema_type'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_type'], "{$wpdb->pbcinema}.cinema_type")." ";
	}

	if(isset($conditions_['ticket_ref_type'])){ //상영작품연동이 없을 경우를 위한
		$query_ .= " AND ".pb_query_in_fields($conditions_['ticket_ref_type'], "{$wpdb->pbcinema}.ticket_ref_type")." ";
	}
	
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_cinema_list_keyword', array(
			"{$wpdb->pbcinema}.cinema_name",
			"{$wpdb->blogs}.domain",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_cinema_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_cinema_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_cinema_list', $wpdb->get_results($query_));

	if(isset($conditions_['include_head_office']) && $conditions_['include_head_office']){

		array_unshift($results_, (object)array(
			'cinema_id' => PB_CINEMA_HEAD_OFFICE_ID,
			'cinema_name' => "본사",
			'cinema_type' => PB_CINEMA_CTG_NOR,
			'cinema_status' => PB_CINEMA_STATUS_NORMAL,
			'ticket_ref_type' => 'none',
			'head_office_yn' => 'Y',

		));
	}
	
	return $results_;
}

function pb_cinema_make_options($conditions_ = array(), $default_ = null){
	$cinema_list_ = pb_cinema_list($conditions_);

	foreach($cinema_list_ as $row_data_){
		?> <option value="<?=$row_data_->cinema_id?>" <?=selected($row_data_->cinema_id, $default_)?> ><?=$row_data_->cinema_name?></option><?php
	}
}


/** 영화관 단일내역 가져오기
 *
 * @param string 영화관ID
 * @return array 영화관 단일정보
 */
function pb_cinema($cinema_id_){
	if(!strlen($cinema_id_)) return null;

	if($cinema_id_ == PB_CINEMA_HEAD_OFFICE_ID){
		return (object)array(
			'cinema_id' => PB_CINEMA_HEAD_OFFICE_ID,
			'cinema_name' => "본사",
			'cinema_type' => PB_CINEMA_CTG_NOR,
			'cinema_status' => PB_CINEMA_STATUS_NORMAL,
			'ticket_ref_type' => 'none',
			'head_office_yn' => 'Y',

		);
	}

	$result_ = pb_cinema_list(array('cinema_id' => $cinema_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function pb_cinema_by_domain($domain_){
	if(!strlen($domain_)) return null;
	$result_ = pb_cinema_list(array('domain' => $domain_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_cinema_fields($data_){
	return pb_format_mapping(apply_filters("pb_cinema_fields_map",array(
	
		'cinema_name' => '%s',
		'admin_id' => '%d',
		'cinema_type' => '%s',
		'cinema_status' => '%s',
		'cinema_desc' => '%s',
		'seattable' => '%s',

		'addr' => '%s',
		'addr_dtl' => '%s',
		'addr_postcode' => '%s',
		'addr_lat' => '%s',
		'addr_lng' => '%s',
		'area_id' => '%s',
		'fee_info' => '%s',
		'biz_no' => '%s',
		'phone1' => '%s',
		'phone2' => '%s',
		'faxnum' => '%s',
		'ticket_ref_type' => '%s',
		'xticket_ref_code' => '%s',
		'logo_image_url' => '%s',

		'naver_synckey' => '%s',

		'replace_location_html_yn' => '%s',
		'replace_location_html' => '%s',

	)), $data_);
}

/** 영화관 추가
 *
 * @param array 영화관 정보
 * @return string 추가된 영화관ID
 */
function pb_cinema_add($cinema_data_){
	$domain_ = $cinema_data_['domain'];
	
	$insert_data_ = _pb_parse_cinema_fields($cinema_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	
	$admin_id_ = $insert_value_['admin_id'];

	if(is_subdomain_install()){
		$newdomain_ = $domain_ . '.' . preg_replace( '|^www\.|', '', get_network()->domain );
		$path_      = get_network()->path;
	} else {
		$newdomain_ = get_network()->domain;
		$path_      = get_network()->path . $domain_ . '/';
	}

	$cinema_id_ = wpmu_create_blog($newdomain_, $path_, $insert_value_['cinema_name'], get_current_user_id(), array("from_pb_cinema" => "Y"));

	if(is_wp_error($cinema_id_)){
		return $cinema_id_;
	}

	switch_to_blog($cinema_id_);
	
    switch_theme('scinema-branch');
    restore_current_blog();

	add_user_to_blog($cinema_id_, $admin_id_, PB_ADMINLVL_ADMIN);

	$insert_value_['cinema_id'] = $cinema_id_;
	$insert_format_[] = '%d';

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbcinema}", $insert_value_, $insert_format_);

	if(!$result_) return new WP_Error("INSERT_FAILED", "영화관을 추가할 수 없습니다.");



	do_action('pb_cinema_added', $cinema_id_);

	return $cinema_id_;
}

/** 영화관 수정
 *
 * @param string 영화관ID
 * @param array 영화관 정보
 * @return object 수정성공 시 - TREU, 실패 시 - WP_Error
 */
function pb_cinema_update($cinema_id_, $cinema_data_){
	$cinema_data_ = _pb_parse_cinema_fields($cinema_data_);

	$cinema_value_ = $cinema_data_['data'];
	$cinema_format_ = $cinema_data_['format'];

	$cinema_value_['mod_date'] = current_time('mysql');
	$cinema_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbcinema}", $cinema_value_, array("cinema_id" => $cinema_id_), $cinema_format_, array("%s"));
	if(!$result_) return $result_;

	do_action('pb_cinema_updated', $cinema_id_);

	return $result_;
}

/** 영화관 삭제
 *
 * @param string 영화관ID
 * @return object 삭제성공 시 - 반환값없음, 실패 시 - false
 */
function pb_cinema_remove($cinema_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbcinema}", array("cinema_id" => $cinema_id_), array("%s"));

	if(!$result_) return $result_;

	wpmu_delete_blog($cinema_id_, true);
	do_action('pb_cinema_removed', $cinema_id_);
}

function pb_cinema_site_url($obj_){
	if(gettype($obj_) !== "object"){
		$obj_ = pb_cinema($obj_);
	}

	return "//".$obj_->domain.$obj_->path;
}

function pb_cinema_wp_admin_url($obj_){
	if(gettype($obj_) !== "object"){
		$obj_ = pb_cinema($obj_);
	}

	return "//".pb_append_url($obj_->domain.$obj_->path,"wp-admin");
}

//관리가능한 티켓연동코드 리턴
function pb_cinema_manageable_ticket_ref_types(){
	$temp_list_ = pb_gcode_dtl_list('C0007', array("only_use" => true));

	$results_ = array();

	foreach($temp_list_ as $row_data_){
		$results_[] = $row_data_->code_did;
	}
	return apply_filters('pb_cinema_manageable_ticket_ref_types', $results_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema-adminpage.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema-seat.php');

?>