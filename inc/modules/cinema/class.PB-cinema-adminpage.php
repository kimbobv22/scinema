<?php

define('PB_ADMINPAGE_CINEMA_MANAGE_ACTION_EDIT', 'edit');
define('PB_ADMINPAGE_CINEMA_MANAGE_ACTION_ADD', 'add');

add_filter('pb_adminpage_list', function($results_){

	$results_['cinema-manage'] = array(
		'title' => '상영관관리' ,
		'content-template' => "cinema-manage/list",
		'subcontent-templates' => array(
			PB_ADMINPAGE_CINEMA_MANAGE_ACTION_EDIT => "cinema-manage/edit",
			PB_ADMINPAGE_CINEMA_MANAGE_ACTION_ADD => "cinema-manage/edit",
		),
		'lvl' => 2,
		'sort_num' => 10,
	);

	return $results_;
});

?>