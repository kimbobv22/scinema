<?php

pb_add_ajax('cinema-seat-update', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$cinema_seat_data_ = isset($_POST['cinema_seat_data']) ? $_POST['cinema_seat_data'] : null;
	$cinema_seat_data_['seat_data'] = json_encode($cinema_seat_data_['seat_data']);
	
	if(empty($cinema_seat_data_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}

	if(!strlen($cinema_seat_data_['seat_id'])){ // new

		$cinema_seat_data_['replace_image_yn'] = isset($cinema_seat_data_['replace_image_yn']) ? $cinema_seat_data_['replace_image_yn'] : "N";
		$seat_id_ = pb_cinema_seat_add($cinema_seat_data_);
		
		if(is_wp_error($seat_id_)){
			echo json_encode(array(
				'success' => false,
				'error_title' => '좌석배치도추가실패',
				'error_message' => $seat_id_->get_error_message(),
			));
			die();			
		}

		$redirect_url_ = null;

		if(pb_cinema_current_is_head_office()){
			$redirect_url_ = pb_make_url(pb_adminpage_url("cinema-seat-manage/dtl/".$cinema_seat_data_['cinema_id']), array(
				'seat_id' => $seat_id_,
			));
		}else{
			$redirect_url_ = pb_make_url(pb_adminpage_url("cinema-seat-manage"), array(
				'seat_id' => $seat_id_,
			));
		}

		echo json_encode(array(
			'success' => true,
			'seat_id' => $seat_id_,
			'success_title' => '좌석배치도추가완료',
			'success_message' => '좌석배치도추가가 완료되었습니다.',
			'redirect_url' => $redirect_url_,
		));
		die();

	}else{

		$cinema_seat_data_['replace_image_yn'] = isset($cinema_seat_data_['replace_image_yn']) ? $cinema_seat_data_['replace_image_yn'] : "N";

		pb_cinema_seat_update($cinema_seat_data_['seat_id'], $cinema_seat_data_);

		$redirect_url_ = null;

		if(pb_cinema_current_is_head_office()){
			$redirect_url_ = pb_make_url(pb_adminpage_url("cinema-seat-manage/dtl/".$cinema_seat_data_['cinema_id']), array(
				'seat_id' => $cinema_seat_data_['seat_id'],
			));
		}else{
			$redirect_url_ = pb_make_url(pb_adminpage_url("cinema-seat-manage"), array(
				'seat_id' => $cinema_seat_data_['seat_id'],
			));
		}

		echo json_encode(array(
			'success' => true,
			'seat_id' => $cinema_seat_data_['seat_id'],
			'success_title' => '좌석배치도수정완료',
			'success_message' => '좌석배치도 수정이 완료되었습니다.',
			'redirect_url' => $redirect_url_,
		));
		die();
	}
});

pb_add_ajax('cinema-seat-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$seat_id_ = isset($_POST['seat_id']) ? $_POST['seat_id'] : null;

	if(!strlen($seat_id_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}

	$seat_data_ = pb_cinema_seat($seat_id_);
	$cinema_id_ = $seat_data_->cinema_id;

	pb_cinema_seat_remove($seat_id_);

	$redirect_url_ = null;

	if(pb_cinema_current_is_head_office()){
		$redirect_url_ = pb_adminpage_url("cinema-seat-manage/dtl/".$cinema_id_);
	}else{
		$redirect_url_ = pb_adminpage_url("cinema-seat-manage");
	}

	echo json_encode(array(
		'success' => true,
		'success_title' => '삭제성공',
		'success_message' => '좌석배치도 삭제에 성공하였습니다.',
		'redirect_url' => $redirect_url_,
	));
	die();

});

?>