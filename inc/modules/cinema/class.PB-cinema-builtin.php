<?php

add_action('init', function(){

	global $wpdb;

	$wpdb->pbcinema = "{$wpdb->base_prefix}cinema";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}cinema` (
		`cinema_id` BIGINT(20)  NOT NULL COMMENT 'ID',
		`cinema_name` varchar(70)  DEFAULT NULL COMMENT '지점명',
		`cinema_type` varchar(5)  DEFAULT NULL COMMENT '지점형식',
		`cinema_status` varchar(5)  DEFAULT NULL COMMENT '등록상태',
		`cinema_desc` longtext  DEFAULT NULL COMMENT '지점설명',
		`seattable` longtext  DEFAULT NULL COMMENT '좌석배치도(임시)',

		`addr` varchar(200)  DEFAULT NULL COMMENT '주소',
		`addr_dtl` varchar(200)  DEFAULT NULL COMMENT '주소상세',
		`addr_postcode` varchar(10)  DEFAULT NULL COMMENT '우편번호',
		`addr_lat` varchar(50)  DEFAULT NULL COMMENT '좌표-위도',
		`addr_lng` varchar(45)  DEFAULT NULL COMMENT '좌표-경도',
		`area_id` varchar(5) DEFAULT NULL COMMENT '지역ID',
		`fee_info` varchar(500)  DEFAULT NULL COMMENT '요금정보',
		`biz_no` varchar(20)  DEFAULT NULL COMMENT '사업자번호',
		`phone1` varchar(50)  DEFAULT NULL COMMENT '연락처1',
		`phone2` varchar(50)  DEFAULT NULL COMMENT '연락처2',
		`faxnum` varchar(50)  DEFAULT NULL COMMENT '팩스번호',

		`replace_location_html_yn` varchar(1)  DEFAULT 'N' COMMENT '오시는길HTML대체',
		`replace_location_html` longtext  DEFAULT NULL COMMENT '오시는길HTML',
		
		`ticket_ref_type` varchar(10)  DEFAULT NULL COMMENT '티켓연동타입',
		`xticket_ref_code` varchar(50)  DEFAULT NULL COMMENT 'X티켓연동코드',
		`logo_image_url` varchar(500)  DEFAULT NULL COMMENT '로고이미지URL',
	
		`sort_num` int(5)  DEFAULT NULL COMMENT '정렬순서',
		`admin_id` bigint(15)  DEFAULT NULL COMMENT '관리자ID',

		`naver_synckey` varchar(100) DEFAULT NULL COMMENT '네이버META싱크키',

		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`cinema_id`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='영화관';";

	return $args_;
},10, 3);


define('PB_CINEMA_CTG_NOR', '00001'); //일반운영
define('PB_CINEMA_CTG_IND', '00003'); //독립운영
define('PB_CINEMA_CTG_PND', '00005'); //부분독립운영
define('PB_CINEMA_CTG_PNDM', '00007'); //부분독립운영(앱만 노출)

define('PB_CINEMA_STATUS_NORMAL', '00001'); //정상드록
define('PB_CINEMA_STATUS_CLOSED', '00003'); //폐업
define('PB_CINEMA_STATUS_HIDE', '00005'); //숨김


add_filter('pb_initial_gcode_list', function($gcode_list_){

	$gcode_list_['C0001'] = array(
		'name' => '상영관구분',
		'data' => array(
			PB_CINEMA_CTG_NOR => '일반',			
			PB_CINEMA_CTG_IND => '독립운영',
			PB_CINEMA_CTG_PND => '부분독립운영',
			PB_CINEMA_CTG_PNDM => '부분독립운영(앱만 사용)'
		),
	);
	
	$gcode_list_['C0003'] = array(
		'name' => '상영관상태',
		'data' => array(
			PB_CINEMA_STATUS_NORMAL => '정상등록',
			PB_CINEMA_STATUS_HIDE => '숨김',
			PB_CINEMA_STATUS_CLOSED => '폐업',
		),
	);

	$gcode_list_['C0005'] = array(
		'name' => '상영관지역',
		'data' => array(
			// '0091' => '서울',
			'1001' => '인천',
			'1011' => '경기',
			'1021' => '강원',
			'1031' => '충북',
			'1041' => '충남',
			'1051' => '전북',
			'1061' => '전남',
			'1071' => '경북',
			'1081' => '경남',
		),
	);

	$gcode_list_['C0007'] = array(
		'name' => '티켓연동방식',
		'data' => array(
			'xticket' => 'X티켓',
			'interpark' => '인터파크',
			'custom001' => '영동시네마',
		),
	);
	
	return $gcode_list_;
});

//워드프레스 빌트인으로 멀티사이트 추가 시 임시데이타 삽입
add_action('wpmu_new_blog', function($blog_id, $user_id, $domain, $path, $site_id, $meta){
	if(isset($meta['from_pb_cinema']) && $meta['from_pb_cinema'] === "Y") return;

	$check_data_ = pb_cinema($blog_id);
	if(!empty($check_data_)) return;

	switch_to_blog($blog_id);
	$cinema_name_ = get_bloginfo('name');
	restore_current_blog();
	
	pb_cinema_add(array(
		'cinema_id' => $blog_id,
		'cinema_name' => $cinema_name_,
		'admin_id' => $user_id,
	));	

},10, 6);

//워드프레스 빌트인으로 멀티사이트 삭제 시 기존데이타 삭제
add_action('deleted_blog', function($blog_id_){
	pb_cinema_remove($blog_id_);
});


add_filter('pb_user_list_where', function($query_, $conditions_){

	global $wpdb;

	if(isset($conditions_['without_admin']) && $conditions_['without_admin'] === true){
		$query_ .= " AND {$wpdb->users}.ID NOT IN (
			SELECT {$wpdb->pbcinema}.admin_id
			FROM   {$wpdb->pbcinema}
		) ";
	}

	return $query_;
},10,2);

?>