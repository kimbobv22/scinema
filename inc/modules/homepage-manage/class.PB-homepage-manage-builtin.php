<?php


add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();
	$current_cinema_data_ = pb_cinema($blog_id_);

	if($blog_id_ !== PB_CINEMA_HEAD_OFFICE_ID){
		return $results_;
	}

	$results_['homepage-manage'] = array(
		'title' => '환경설정' ,
		'content-template' => "homepage-manage",
		'lvl' => 2,
		'sort_num' => 0,
	);

	return $results_;
});

?>