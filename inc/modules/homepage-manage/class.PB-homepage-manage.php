<?php

function pb_homepage_manage_map(){
	$result_ = json_decode(get_option("pb_homepage_manage_map"), true);

	if(empty($result_)){
		$result_ = array(

			'android_min_version' => null,
			'android_max_version' => null,
			'ios_min_version' => null,
			'ios_max_version' => null,


			'address' => null,
			'contact1' => null,
			'email' => null,
			'bizinfo' => null,
			'copyrights' => null,

		);
	}

	return $result_;
}
function pb_homepage_manage_map_update($map_){
	update_option("pb_homepage_manage_map", json_encode($map_));
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/homepage-manage/class.PB-homepage-manage-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/homepage-manage/class.PB-homepage-manage-ajax.php');

?>