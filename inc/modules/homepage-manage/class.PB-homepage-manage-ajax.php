<?php

pb_add_ajax('adminpage-homepage-manage-update', function(){
	if(!pb_is_admin()){
		echo json_encode(array(
			'success' => true,
			'deny' => true,
		));
		die();
	}

	$update_data_ = isset($_POST['update_data']) ? $_POST['update_data'] : null;
	
	if(isset($update_data_['homepage_intro_text'])){
		update_option("description", $update_data_['homepage_intro_text']);
		update_option("blogdescription", $update_data_['homepage_intro_text']);

		unset($update_data_['homepage_intro_text']);
	}

	pb_homepage_manage_map_update($update_data_);

	echo json_encode(array(
		'success' => true,
	));
	die();
});

?>