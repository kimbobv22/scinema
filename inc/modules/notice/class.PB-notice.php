<?php

function pb_notice_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
				 {$wpdb->pbnotice}.ID ID
				,{$wpdb->pbnotice}.cinema_id cinema_id
				,{$wpdb->pbcinema}.cinema_name cinema_name

				,{$wpdb->pbnotice}.notice_title notice_title
				,{$wpdb->pbnotice}.notice_html notice_html

				,{$wpdb->pbnotice}.attachments attachments
				
				,{$wpdb->pbnotice}.reg_date reg_date
				,DATE_FORMAT({$wpdb->pbnotice}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi
				,DATE_FORMAT({$wpdb->pbnotice}.reg_date, '%Y.%m.%d') reg_date_ymd

				,{$wpdb->pbnotice}.mod_date mod_date
				,DATE_FORMAT({$wpdb->pbnotice}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi
				,DATE_FORMAT({$wpdb->pbnotice}.mod_date, '%Y.%m.%d') mod_date_ymd

				".apply_filters("pb_notice_list_fields", "", $conditions_)."

				
		     FROM {$wpdb->pbnotice}

		     LEFT OUTER JOIN {$wpdb->pbcinema}
		     ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbnotice}.cinema_id

		      ";

	$query_ .= apply_filters('pb_notice_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbnotice}.ID")." ";
	}

	if(isset($conditions_['cinema_id'])){
		if(isset($conditions_['with_common']) && $conditions_['with_common'] === true){
			$query_ .= " AND ( 
				".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbnotice}.cinema_id")." OR 
				{$wpdb->pbnotice}.cinema_id = '-1'
				 ) ";
		}else{
			$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbnotice}.cinema_id")." ";
		}
	}
	if(isset($conditions_['for_common']) && $conditions_['for_common'] === true){
		$query_ .= " AND {$wpdb->pbnotice}.cinema_id = '-1' ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_notice_list_keyword', array(
			"{$wpdb->pbnotice}.notice_title",
			"{$wpdb->pbcinema}.cinema_name",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_notice_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_notice_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}
	
	return apply_filters('pb_notice_list', $wpdb->get_results($query_));	
}

//개봉작품 단인내역 가져오기
function pb_notice_data($notice_id_){
	if(!strlen($notice_id_)) return null;
	$result_ = pb_notice_list(array("ID" => $notice_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_notice_data_fields($data_){
	return pb_format_mapping(apply_filters("pb_notice_data_fields_map",array(
	
		'cinema_id' => '%d',
		'notice_title' => '%s',
		'notice_html' => '%s',
		'attachments' => '%s',

	)), $data_);
}

function pb_notice_data_add($notice_data_){
	$insert_data_ = _pb_parse_notice_data_fields($notice_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbnotice}", $insert_value_, $insert_format_);

	if(!$result_) return $result_;

	$notice_id_ = $wpdb->insert_id;

	do_action('pb_notice_added', $notice_id_);

	return $notice_id_;
}

function pb_notice_data_update($notice_id_, $notice_data_){
	$notice_data_ = _pb_parse_notice_data_fields($notice_data_);

	$update_value_ = $notice_data_['data'];
	$update_format_ = $notice_data_['format'];

	$update_value_['mod_date'] = current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbnotice}", $update_value_, array("ID" => $notice_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_notice_updated', $notice_id_);

	return $result_;
}

function pb_notice_data_remove($notice_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbnotice}", array("ID" => $notice_id_), array("%d"));

	if(!$result_) return $result_;

	do_action('pb_notice_removed', $notice_id_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/notice/class.PB-notice-adminpage.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/notice/class.PB-notice-builtin.php');

?>