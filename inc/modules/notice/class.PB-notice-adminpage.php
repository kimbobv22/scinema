<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();

	$current_cinema_data_ = pb_cinema($blog_id_);
	$results_['notice-manage'] = array(
		'title' => '공지사항관리' ,
		'template-data' => array(

			'data' => array(
				'total-count' => function(){
					$is_root_ = pb_cinema_current_is_head_office();
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

					return pb_notice_list(array(
						'keyword' => $keyword_,
						'cinema_id' => ($is_root_ ? null : get_current_blog_id()),
						'just_count' => true,
					));
				},
				'list' => function($offset_, $limit_){
					$is_root_ = pb_cinema_current_is_head_office();
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

					return pb_notice_list(array(
						'keyword' => $keyword_,
						'cinema_id' => ($is_root_ ? null : get_current_blog_id()),
						'limit' => array($offset_, $limit_),
						'orderby' => ' ORDER BY reg_date DESC '
					));
				},
				'single' => function($action_data_){
					$pb_notice_data_ = pb_notice_data($action_data_);
					$pb_notice_data_->notice_title = htmlentities(stripcslashes($pb_notice_data_->notice_title));
					return $pb_notice_data_;
				},
				'empty' => function(){
					$is_root_ = pb_cinema_current_is_head_office();
					return array(
						'cinema_id' => ($is_root_ ? -1 : get_current_blog_id()),
						'status' => '00001',
					);
				},

			),

			'options' => array(
				'list-options' => array(
					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 공지사항이 없습니다.',
						'add' => '공지사항등록',
					),
				),
				'edit-options' => array(
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => '공지사항추가확인',
						'add-confirm-message' => '해당 공지사항를 추가하시겠습니까?',
						'add-success-title' => '추가완료',
						'add-success-message' => '공지사항이 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => '공지사항 추가 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => '공지사항정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => '공지사항 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '공지사항삭제확인',
						'remove-confirm-message' => '해당 공지사항를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => '공지사항정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => '공지사항 삭제 중 에러가 발생하였습니다.',
					),
				),
				'id-format' => 'pb-movie-screen-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){
					$result_ = pb_notice_data_update($target_data_['ID'], $target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '공지사항 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['ID'];
				},
				'add' => function($target_data_){
					$result_ = pb_notice_data_add($target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '공지사항 추가 중 에러가 발생했습니다.');
					}


					return $result_;
				},
				'remove' => function($target_data_){
					pb_notice_data_remove($target_data_['ID']);
					return true;
				},
			),
			'templates' => array(
				'list' => function(){

					$is_root_ = pb_cinema_current_is_head_office();

					return array(
						'cinema_name' => array(
							'column-label' => '영화관',
							'th-class' => "col-3 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
							'td-class' => "col-3 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
							'td-render-func' => function($item_, $column_name_){
								
								if($item_->cinema_id == PB_CINEMA_HEAD_OFFICE_ID){ ?>
									본사
								<?php }else if(strlen($item_->cinema_id === "-1")){ ?>
									공통
								<?php }else{ ?>
									<?=$item_->cinema_name?>
								<?php }
							},
						),
						'movie_name' => array(
							'column-label' => '공지사항명',
							'th-class' => "col-7",
							'td-class' => "col-7",
							'td-render-func' => function($item_, $column_name_){
								?>

								<a href="<?=pb_adminpage_url("notice-manage/edit/".$item_->ID)?>"><?=htmlentities(stripcslashes($item_->notice_title))?></a>

								<?php
							},
						),
						'publisher_name' => array(
							'column-label' => '작성일자',
							'th-class' => "col-2 text-center",
							'td-class' => "col-2 text-center",
							
						),
						
					);
				},
				'edit' => function(){

					$is_root_ = pb_cinema_current_is_head_office();

					$cinema_select_col_ = null;

					if($is_root_){

						$tmp_cinema_list_ = pb_cinema_list(array("include_head_office" => true));
						$cinema_list_ = array("-1" => "공통");

						foreach($tmp_cinema_list_ as $row_data_){
							$cinema_list_[$row_data_->cinema_id] = $row_data_->cinema_name;
						}

						$cinema_select_col_ = array(
							'column' => 'col-xs-12 col-sm-5',
							'column-label' => '영화관',
							'column-name' => 'cinema_id',
							'placeholder' => '-영화관 선택-',
							'validates' => array(
								'required' => "영화관을 선택하세요",
							),
							'type' => 'select',
							'options' => $cinema_list_,
						);
					}else{
						$cinema_select_col_ = array(
							'type' => 'hidden',
							'column-name' => 'cinema_id',
						);
					}

					return array(
						array(
							'type' => 'hidden',
							'column-name' => 'ID',
						),
					
					
						array(
							'type' => 'row',
							'columns' => array(
								$cinema_select_col_,
								array(
									'column' => 'col-xs-12 '.($is_root_ ? "col-sm-7" : "col-sm-12"),
									'column-label' => '공지제목',
									'column-name' => 'notice_title',
									'placeholder' => '제목 입력',
									'validates' => array(
										'required' => "제목을 입력하세요",
									),
									'type' => 'text',
								),
							),
						),
						
						array(
							'type' => 'division',
						),
						array(
							'column-label' => '공지내용',
							'column-name' => 'notice_html',
							'placeholder' => '내용 입력',
							'validates' => array(
								'required' => "공지사항상태를 선택하세요",
							),
							'type' => 'wp_editor',
						),
						array(
							'type' => 'division',
						),
						array(
							'column-label' => '첨부파일',
							'column-name' => 'attachments',
							'type' => 'filepicker',
							'maxlength' => 5,
							'data-type' => 'json',
						),
					);
				}
			),

		),
		'lvl' => 1,
		'sort_num' => 21,
	);

	return $results_;
});


?>