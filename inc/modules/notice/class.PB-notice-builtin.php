<?php


add_action('init', function(){

	global $wpdb;

	$wpdb->pbnotice = "{$wpdb->base_prefix}notice";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}notice` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`cinema_id` BIGINT(20)  DEFAULT NULL COMMENT '사용상영관',
		
		`notice_title` varchar(200)  DEFAULT NULL COMMENT '공지제목',
		`notice_html` longtext COMMENT '공지내용',

		`attachments` longtext COMMENT '첨부파일',
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='공지';";

	return $args_;
},10, 3);

/*
add_action('wp_enqueue_scripts', function(){
	wp_enqueue_script("jquery-ui-core");
	wp_enqueue_script("jquery-ui-draggable");

	wp_enqueue_script("pb-banner", (pb_library_url() . 'js/pb-banner.js'), array("pb-all-main", 'jquery-ui-draggable'));
});*/

?>