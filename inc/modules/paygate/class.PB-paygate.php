<?php

//결제방식 정의
define("PB_PAYMENT_METHOD_CARD", "card");
define("PB_PAYMENT_METHOD_VBANK", "vbank");
define("PB_PAYMENT_METHOD_BANK", "trans");
define("PB_PAYMENT_METHOD_PHONE", "phone");
define("PB_PAYMENT_METHOD_TEST", "test");

//아임포트 결제상태
define("PB_PAYMENT_IAMPORT_STATUS_PAID", "paid");
define("PB_PAYMENT_IAMPORT_STATUS_READY", "ready");
define("PB_PAYMENT_IAMPORT_STATUS_CANCELLED", "cancelled");
define("PB_PAYMENT_IAMPORT_STATUS_FAILED", "failed");

//페이게이트 아임포트 인스턴스
function pb_paygate_iamport(){

	global $pb_paygate_instance;

	if(isset($pb_paygate_instance)){
		return $pb_paygate_instance;
	}

	require_once(PB_THEME_LIB_DIR_PATH . 'modules/paygate/lib/iamport.php');

	$settings_ = PB_settings::get_settings();

	$pb_paygate_instance = new Iamport(
		$settings_['paygate_iamport_api_key'],
		$settings_['paygate_iamport_api_secret']
	);

	return $pb_paygate_instance;
}

//페이게이트에서 주문정보가져오기
function pb_paygate_load_iamport_order($order_id_){
	$iamport_ = pb_paygate_iamport();
	$result_ = $iamport_->findByImpUID($order_id_);

	if($result_->success){
		return $result_->data;
	}else{
		return new WP_Error($result_->error['code'], $result_->error['message']);
	}
}

//페이게이트에서 주문정보가져오기(PG주문번호로)
function pb_paygate_load_iamport_order_by_order_id($order_id_){
	$iamport_ = pb_paygate_iamport();
	$result_ = $iamport_->findByMerchantUID($order_id_);

	if($result_->success){
		return $result_->data;
	}else{
		return new WP_Error($result_->error['code'], $result_->error['message']);
	}
}

//주문취소요청
function pb_paygate_cancel_iamport_order($cancel_data_){
	$iamport_ = pb_paygate_iamport();
	$result_ = $iamport_->cancel($cancel_data_);
	if($result_->success){
		$pg_data_ = $result_->data->getResponse();
		do_action('pb_paygate_cancelled', $pg_data_->imp_uid);
		return $pg_data_;
	}else{
		return new WP_Error($result_->error['code'], $result_->error['message']);
	}
}

//결제성공여부체크(상태값)
function pb_paygate_is_success_status($status_){
	return ($status_ === PB_PAYMENT_IAMPORT_STATUS_PAID || $status_ === PB_PAYMENT_IAMPORT_STATUS_READY);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/paygate/class.PB-paygate-settings.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/paygate/class.PB-paygate-gcode.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/paygate/class.PB-paygate-builtin.php');

?>