<?php

add_filter("pbadmin_settings_form", function($view_list_, $settings_){
	ob_start();

?>
<h3>아임포트(PG) 설정</h3>
<table class="form-table">
	<tr>
		<th>스크립트URL</th>
		<td><input type="text" name="paygate_iamport_script_url" value="<?= $settings_["paygate_iamport_script_url"]; ?>" class="large-text"></td>
	</tr>
	<tr>
		<th>가맹점식별코드</th>
		<td><input type="text" name="paygate_iamport_user_code" value="<?= $settings_["paygate_iamport_user_code"]; ?>" class="large-text"></td>
	</tr>
	<tr>
		<th>API KEY</th>
		<td><input type="text" name="paygate_iamport_api_key" value="<?= $settings_["paygate_iamport_api_key"]; ?>" class="large-text"></td>
	</tr>
	<tr>
		<th>API SECRET</th>
		<td><input type="text" name="paygate_iamport_api_secret" value="<?= $settings_["paygate_iamport_api_secret"]; ?>" class="large-text"></td>
	</tr>
	<tr>
		<th>에스크로</th>
		<td>
			<label><input name="paygate_iamport_escrow" value="Y" type="radio" <?=checked("Y", $settings_["paygate_iamport_escrow"])?> > <span>사용</span></label>
			<label><input name="paygate_iamport_escrow" value="N" type="radio" <?=checked("N", $settings_["paygate_iamport_escrow"])?> > <span>미사용</span></label>
		</td>
	</tr>
	
	<tr>
		<th>PG사</th>
		<td>
			<select name="paygate_iamport_pg_company" id="paygate_iamport_pg_company" class="large-text">
			<?php
				global $pb_gcode;

			?>


				<?= 

					pb_gcode_make_options('I0001', $settings_['paygate_iamport_pg_company']);
					//$pb_gcode->options('I0001', $settings_['paygate_iamport_pg_company'])

				?>
			</select>
		</td>
	</tr>
	<tr>
		<th>결제통보 페이지</th>
		<td>
			<select name="paygate_iamport_notification_page_id" id="paygate_iamport_notification_page_id" class="large-text">
				<?= pb_make_select_with_pages(array(
					"default" => $settings_["paygate_iamport_notification_page_id"],
				)); ?>
			</select>
		</td>
	</tr>
	<tr>
		<th>결제결과 페이지</th>
		<td>
			<select name="paygate_iamport_payment_redirect_page_id" id="paygate_iamport_payment_redirect_page_id" class="large-text">
				<?= pb_make_select_with_pages(array(
					"default" => $settings_["paygate_iamport_payment_redirect_page_id"],
				)); ?>
			</select>
		</td>
	</tr>
	
	
</table>

<?php

	$view_ = ob_get_clean();
	$view_list_[] = array(
		"id" => "paygate-settings",
		"title" => "아임포트(PG) 관련",
		"view" => $view_,
	);

	return $view_list_;

}, 10, 2);

?>