<?php

//결제통보
add_action("template_redirect", function(){
	$settings_ = PB_settings::get_settings();

	$iamport_notification_page_id_ = (string)$settings_["paygate_iamport_notification_page_id"];
	$current_page_id_ = (string)get_the_ID();

	if(!strlen($iamport_notification_page_id_) || !strlen($current_page_id_)){
		return;
	}
	if($current_page_id_ !== $iamport_notification_page_id_){
		return;
	}

	$imp_uid_ = null;
	$order_id_ = null;

	if(pb_is_devmode()){
		$imp_uid_ = isset($_REQUEST["imp_uid"]) ? $_REQUEST["imp_uid"] : null;
		$order_id_ = isset($_REQUEST["merchant_uid"]) ? $_REQUEST["merchant_uid"] : null;	
	}else{
		$imp_uid_ = isset($_POST["imp_uid"]) ? $_POST["imp_uid"] : null;
		$order_id_ = isset($_POST["merchant_uid"]) ? $_POST["merchant_uid"] : null;	
	}

	if(!strlen($imp_uid_) || !strlen($order_id_)){
		wp_die("잘못된 접근입니다.");
		return;
	}

	do_action("pb_paygate_payment_result_notification", $imp_uid_, $order_id_);
	exit();
});

//PG 스크립트 삽입
add_action('pb_library_initialize', function(){
	$settings_ = PB_settings::get_settings();
	
	wp_register_script("paygate", $settings_["paygate_iamport_script_url"], array("jquery"));
	wp_register_script("pb-paygate", (pb_library_url() . 'js/pb-paygate.js'), array("pb-all-main", 'paygate'));
	add_filter("pb_script_common_var",function($var_){

		$settings_ = PB_settings::get_settings();

		return array_merge($var_, array(
			'paygate_pg_company' => $settings_['paygate_iamport_pg_company'],
			'paygate_user_code' => $settings_['paygate_iamport_user_code'],
			'paygate_escrow' => $settings_['paygate_iamport_escrow'],
			'paygate_m_redirect_url' => get_permalink($settings_['paygate_iamport_payment_redirect_page_id']),

			// 'api_key' => $settings_['paygate_iamport_api_key'],
			// 'api_secret' => $settings_['paygate_iamport_api_secret'],
		));
	});

	
	PB_library::register('pb-paygate', array(
		'script' => array('paygate','pb-paygate'),
	));
});

function pb_paygate_load_library(){
	pb_library_load('pb-paygate');
	return 'pb-paygate';
}

?>