<?php

add_filter('pb_initial_gcode_list', function($list_){

	$list_['I0001'] = array(
		'name' => 'PG사',
		'data' => array(
			'danal_tpay' => '다날페이',
			'kakao' => '카카오페이',
			'inicis' => '이니시스',
			'html5_inicis' => '이니시스(웹표준결제)',
			'nice' => '나이스',
			'jtnet' => 'jtnet',
			'uplus' => 'LG유플러스',
		)
	);


	$list_['I0003'] = array(
		'name' => '결제방법',
		'name_en' => 'Pay Method',
		'data' => array(
			PB_PAYMENT_METHOD_CARD 	=> "신용카드",
			PB_PAYMENT_METHOD_BANK 	=> "실시간이체",
			PB_PAYMENT_METHOD_VBANK => "무통장",
			PB_PAYMENT_METHOD_PHONE => "핸드폰",
		),
	);

	$list_['I0005'] = array(
		'name' => '카드사',
		'name_en' => 'Card Corp',
		'data' => array(
			"01" => array(
				'name' => "하나(외한)",
				'name_en' => "하나(외한)",
			),
			"03" => array(
				'name' => "롯데",
				'name_en' => "롯데",
			),
			"04" => array(
				'name' => "현대",
				'name_en' => "현대",
			),
			"06" => array(
				'name' => "국민",
				'name_en' => "국민",
			),
			"11" => array(
				'name' => "BC",
				'name_en' => "BC",
			),
			"12" => array(
				'name' => "삼성",
				'name_en' => "삼성",
			),
			"14" => array(
				'name' => "신한",
				'name_en' => "신한",
			),
			"15" => array(
				'name' => "한미",
				'name_en' => "한미",
			),
			"16" => array(
				'name' => "NH",
				'name_en' => "NH",
			),
			"17" => array(
				'name' => "하나SK",
				'name_en' => "하나SK",
			),
			"21" => array(
				'name' => "해외비자",
				'name_en' => "해외비자",
			),
			"22" => array(
				'name' => "해외마스터",
				'name_en' => "해외마스터",
			),
			"23" => array(
				'name' => "JCB",
				'name_en' => "JCB",
			),
			"24" => array(
				'name' => "해외아멕스",
				'name_en' => "해외아멕스",
			),
			"25" => array(
				'name' => "해외다이너스",
				'name_en' => "해외다이너스",
			),
		),
	);

	$list_['I0007'] = array(
		'name' => '카드발급사',
		'name_en' => 'Card Publisher',
		'data' => array(
			"02" => array(
				'name' => "한국산업은행",
				'name_en' => "한국산업은행",
			),
			"03" => array(
				'name' => "기업은행",
				'name_en' => "기업은행",
			),
			"04" => array(
				'name' => "국민은행(주택은행)",
				'name_en' => "국민은행(주택은행)",
			),
			"05" => array(
				'name' => "외환은행",
				'name_en' => "외환은행",
			),
			"07" => array(
				'name' => "수협중앙회",
				'name_en' => "수협중앙회",
			),
			"11" => array(
				'name' => "농협중앙회",
				'name_en' => "농협중앙회",
			),
			"12" => array(
				'name' => "단위농협",
				'name_en' => "단위농협",
			),
			"16" => array(
				'name' => "축협중앙회",
				'name_en' => "축협중앙회",
			),
			"20" => array(
				'name' => "우리은행",
				'name_en' => "우리은행",
			),
			"21" => array(
				'name' => "신한은행(조흥은행)",
				'name_en' => "신한은행(조흥은행)",
			),
			"23" => array(
				'name' => "제일은행",
				'name_en' => "제일은행",
			),
			"25" => array(
				'name' => "하나은행(서울은행)",
				'name_en' => "하나은행(서울은행)",
			),
			"26" => array(
				'name' => "신한은행",
				'name_en' => "신한은행",
			),
			"27" => array(
				'name' => "한국씨티은행(한미은행)",
				'name_en' => "한국씨티은행(한미은행)",
			),
			"31" => array(
				'name' => "대구은행",
				'name_en' => "대구은행",
			),
			"32" => array(
				'name' => "부산은행",
				'name_en' => "부산은행",
			),
			"34" => array(
				'name' => "광주은행",
				'name_en' => "광주은행",
			),
			"35" => array(
				'name' => "제주은행",
				'name_en' => "제주은행",
			),
			"37" => array(
				'name' => "전북은행",
				'name_en' => "전북은행",
			),
			"38" => array(
				'name' => "강원은행",
				'name_en' => "강원은행",
			),
			"39" => array(
				'name' => "경남은행",
				'name_en' => "경남은행",
			),
			"00041" => array(
				'name' => "비씨카드",
				'name_en' => "비씨카드",
			),
			"53" => array(
				'name' => "씨티은행",
				'name_en' => "씨티은행",
			),
			"54" => array(
				'name' => "홍콩상하이은행",
				'name_en' => "홍콩상하이은행",
			),
			"71" => array(
				'name' => "우체국",
				'name_en' => "우체국",
			),
			"81" => array(
				'name' => "하나은행",
				'name_en' => "하나은행",
			),
			"83" => array(
				'name' => "평화은행",
				'name_en' => "평화은행",
			),
			"87" => array(
				'name' => "신세계",
				'name_en' => "신세계",
			),
			"88" => array(
				'name' => "신한은행(조흥 통합)",
				'name_en' => "신한은행(조흥 통합)",
			),
		),
	);

	$list_['I0009'] = array(
		'name' => '은행(증권사)',
		'name_en' => 'Bank',
		'data' => array(
			"03" => array(
				'name' => "기업은행",
				'name_en' => "기업은행",
			),
			"04" => array(
				'name' => "국민은행",
				'name_en' => "국민은행",
			),
			"05" => array(
				'name' => "외환은행",
				'name_en' => "외환은행",
			),
			"07" => array(
				'name' => "수협중앙회",
				'name_en' => "수협중앙회",
			),
			"11" => array(
				'name' => "농협중앙회",
				'name_en' => "농협중앙회",
			),
			"20" => array(
				'name' => "우리은행",
				'name_en' => "우리은행",
			),
			"23" => array(
				'name' => "SC제일은행",
				'name_en' => "SC제일은행",
			),
			"31" => array(
				'name' => "대구은행",
				'name_en' => "대구은행",
			),
			"32" => array(
				'name' => "부산은행",
				'name_en' => "부산은행",
			),
			"34" => array(
				'name' => "광주은행",
				'name_en' => "광주은행",
			),
			"37" => array(
				'name' => "전북은행",
				'name_en' => "전북은행",
			),
			"39" => array(
				'name' => "경남은행",
				'name_en' => "경남은행",
			),
			"53" => array(
				'name' => "한국씨티은행",
				'name_en' => "한국씨티은행",
			),
			"71" => array(
				'name' => "우체국",
				'name_en' => "우체국",
			),
			"81" => array(
				'name' => "하나은행",
				'name_en' => "하나은행",
			),
			"88" => array(
				'name' => "통합신한은행(신한,조흥은행)",
				'name_en' => "통합신한은행(신한,조흥은행)",
			),
			"D1" => array(
				'name' => "유안타증권(구 동양증권)",
				'name_en' => "유안타증권(구 동양증권)",
			),
			"D2" => array(
				'name' => "현대증권",
				'name_en' => "현대증권",
			),
			"D3" => array(
				'name' => "미래에셋증권",
				'name_en' => "미래에셋증권",
			),
			"D4" => array(
				'name' => "한국투자증권",
				'name_en' => "한국투자증권",
			),
			"D5" => array(
				'name' => "우리투자증권",
				'name_en' => "우리투자증권",
			),
			"D6" => array(
				'name' => "하이투자증권",
				'name_en' => "하이투자증권",
			),
			"D7" => array(
				'name' => "HMC투자증권",
				'name_en' => "HMC투자증권",
			),
			"D8" => array(
				'name' => "SK증권",
				'name_en' => "SK증권",
			),
			"D9" => array(
				'name' => "대신증권",
				'name_en' => "대신증권",
			),
			"DA" => array(
				'name' => "하나대투증권",
				'name_en' => "하나대투증권",
			),
			"DB" => array(
				'name' => "굿모닝신한증권",
				'name_en' => "굿모닝신한증권",
			),
			"DC" => array(
				'name' => "동부증권",
				'name_en' => "동부증권",
			),
			"DD" => array(
				'name' => "유진투자증권",
				'name_en' => "유진투자증권",
			),
			"DE" => array(
				'name' => "메리츠증권",
				'name_en' => "메리츠증권",
			),
			"DF" => array(
				'name' => "신영증권",
				'name_en' => "신영증권",
			),
			"27" => array(
				'name' => "한국씨티은행(한미은행)",
				'name_en' => "한국씨티은행(한미은행)",
			),

		),
	);

	return $list_;
});

//결제방식명
function pb_paygate_pay_method_name($pay_method_){
	$pay_method_name_ = pb_gcode_dtl_name('I0003', $pay_method_);
	if(!strlen($pay_method_name_)) return __('기타', PBDOMAIN);
	return $pay_method_name_;
}

//카드사 코드명
function pb_paygate_card_code_name($card_code_){
	$card_code_name_ = pb_gcode_dtl_name('I0005', $card_code_);
	if(!strlen($card_code_name_)) return __('기타', PBDOMAIN);
	return $card_code_name_;
}

//카드발급사(은행) 코드명
function pb_paygate_card_bank_code_name($bank_code_){
	$card_bank_code_name_ = pb_gcode_dtl_name('I0007', $bank_code_);
	if(!strlen($card_bank_code_name_)) return __('기타', PBDOMAIN);
	return $card_bank_code_name_;
}

//은행(증권사) 코드명
function pb_paygate_bank_code_name($bank_code_){
	$bank_code_name_ = pb_gcode_dtl_name('I0009', $bank_code_);
	if(!strlen($bank_code_name_)) return __('기타', PBDOMAIN);
	return $bank_code_name_;
}

//핸드폰 앞자리
function pb_paygate_mobile_first_num_list(){
	return pb_gcode_dtl_list('I0011');
}

?>