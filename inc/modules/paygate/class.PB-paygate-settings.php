<?php

add_filter("pb_default_settings", function($settings_){
	return array_merge($settings_,array(
		"paygate_iamport_script_url" => "https://service.iamport.kr/js/iamport.payment-1.1.5.js",
		"paygate_iamport_pg_company" => "",
		"paygate_iamport_user_code" => "",
		"paygate_iamport_api_key" => "",
		"paygate_iamport_api_secret" => "",
		"paygate_iamport_escrow" => "N",
		
		"paygate_iamport_notification_page_id" => "",
		"paygate_iamport_payment_redirect_page_id" => "",
	));
});

function pb_paygate_payment_redirect_page_id(){
	return pb_settings('paygate_iamport_payment_redirect_page_id');
}
function pb_paygate_payment_redirect_url($order_id_){
	return pb_make_url(get_permalink(pb_settings('paygate_iamport_payment_redirect_page_id')), array(
		'order_id' => $order_id_
	));
}

?>