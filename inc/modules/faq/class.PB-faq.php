<?php

function pb_faq_mst_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
					 {$wpdb->pbcinema}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_name cinema_name
					
					,{$wpdb->pbfaq}.faq_cnt faq_cnt
				
		     FROM {$wpdb->pbcinema}

		     LEFT OUTER JOIN (
				SELECT  {$wpdb->pbfaq}.cinema_id cinema_id
						,COUNT(*) faq_cnt
				FROM {$wpdb->pbfaq}
				GROUP BY {$wpdb->pbfaq}.cinema_id
		     ) {$wpdb->pbfaq}
		     ON   {$wpdb->pbfaq}.cinema_id = {$wpdb->pbcinema}.cinema_id

		      ";

	$query_ .= apply_filters('pb_faq_mst_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_faq_mst_list_keyword', array(
			"{$wpdb->pbcinema}.cinema_name",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_faq_mst_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_faq_mst_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_faq_mst_list', $wpdb->get_results($query_));

	array_unshift($results_, (object)array(
		'cinema_id' => PB_CINEMA_HEAD_OFFICE_ID,
		'cinema_name' => "본사",
		'faq_cnt' => pb_faq_list(array("cinema_id" => PB_CINEMA_HEAD_OFFICE_ID, 'just_count' => true)),
	));
	
	return $results_;
}


function pb_faq_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
					 {$wpdb->pbfaq}.ID ID
					,{$wpdb->pbfaq}.cinema_id cinema_id
					,{$wpdb->pbfaq}.faq_title faq_title
					,{$wpdb->pbfaq}.faq_desc faq_desc

					,{$wpdb->pbfaq}.sort_num sort_num

					,{$wpdb->pbfaq}.reg_date reg_date
					,DATE_FORMAT({$wpdb->pbfaq}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

					,{$wpdb->pbfaq}.mod_date mod_date
					,DATE_FORMAT({$wpdb->pbfaq}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

				
		     FROM {$wpdb->pbfaq}

		      ";

	$query_ .= apply_filters('pb_faq_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbfaq}.ID")." ";
	}
	if(isset($conditions_['cinema_id'])){ //정상적으로 상영관ID가 들어온 경우
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbfaq}.cinema_id")." ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_faq_list_keyword', array(
			"{$wpdb->pbfaq}.faq_title",
			
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_faq_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_faq_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY sort_num ASC, reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	return apply_filters('pb_faq_list', $wpdb->get_results($query_));	
}

function pb_faq($faq_id_){
	if(!strlen($faq_id_)) return null;
	$result_ = pb_faq_list(array('ID' => $faq_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_faq_fields($data_){
	return pb_format_mapping(apply_filters("pb_faq_fields_map",array(

		// 'ID' => '%s',
		'cinema_id' => '%d',
		'faq_title' => '%s',
		'faq_desc' => '%s',

		'sort_num' => '%d',
		

	)), $data_);
}

function pb_faq_add($data_){
	
	$insert_data_ = _pb_parse_faq_fields($data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbfaq}", $insert_value_, $insert_format_);
	if(!$result_) return $result_;

	$faq_id_ = $wpdb->insert_id;

	do_action('pb_faq_added', $faq_id_);

	return $faq_id_;
}

function pb_faq_update($faq_id_, $update_data_){
	$update_data_ = _pb_parse_faq_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];

	$update_value_['mod_date']= current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbfaq}", $update_value_, array("ID" => $faq_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_faq_updated', $faq_id_);

	return $result_;
}

function pb_faq_remove($faq_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbfaq}", array("ID" => $faq_id_), array("%d"));

	if(!$result_) return $result_;
	do_action('pb_faq_removed', $faq_id_);
}

function pb_faq_allowed($obj_ = null){
	if(gettype($obj_) !== "object"){
		$obj_ = pb_faq($obj_);
	}

	return apply_filters('pb_faq_allowed', true, $obj_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/faq/class.PB-faq-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/faq/class.PB-faq-adminpage.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/faq/class.PB-faq-ajax.php');

?>