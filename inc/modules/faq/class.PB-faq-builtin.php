<?php


add_action('init', function(){

	global $wpdb;

	$wpdb->pbfaq = "{$wpdb->base_prefix}faq";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}faq` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`cinema_id` BIGINT(20)  NOT NULL COMMENT '지점 ID',
		
		`faq_title` varchar(200)  DEFAULT NULL COMMENT '제목',
		`faq_desc` longtext  DEFAULT NULL COMMENT '내용',
		
		`sort_num` bigint(10)  DEFAULT NULL COMMENT '순서',
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='자주묻는질문';";

	return $args_;
},10, 3);

?>