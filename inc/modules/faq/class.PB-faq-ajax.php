<?php

pb_add_ajax('faq-update', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$faq_data_ = $_POST['faq_data'];

	if(strlen($faq_data_['faq_id'])){ //update

		pb_faq_update($faq_data_['faq_id'], $faq_data_);

		echo json_encode(array(
			'success' => true,
			'success_title' => '수정성공',
			'success_message' => '배너수정에 성공했습니다.',
		));
		die();

	}else{ //add

		$result_ = pb_faq_add($faq_data_);

		if(is_wp_error($result_)){
			echo json_encode(array(
				'success' => false,
				'error_title' => '에러발생',
				'error_message' => '배너추가 중, 에러가 발생했습니다.',
			));
			die();
		}

		echo json_encode(array(
			'success' => true,
			'success_title' => '추가성공',
			'success_message' => '배너추가에 성공했습니다.',
		));
		die();
	}
});

pb_add_ajax('faq-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$faq_id_ = $_POST['faq_id'];

	pb_faq_remove($faq_id_);

	echo json_encode(array(
		'success' => true,
	));
	die();
});



?>