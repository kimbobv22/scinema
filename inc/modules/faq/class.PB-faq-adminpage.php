<?php
add_filter('pb_adminpage_list', function($results_){

	if(pb_cinema_current_is_head_office()){
		$results_['faq-manage'] = array(
			'title' => '자주묻는질문관리' ,
			'content-template' => "faq-manage/list",
			'subcontent-templates' => array(
				"edit" => "faq-manage/edit",
			),
			'lvl' => 2,
			'sort_num' => 50,
		);	
	}else{
		$results_['faq-manage'] = array(
			'title' => '자주묻는질문관리' ,
			'content-template' => "faq-manage/edit",
			'lvl' => 1,
			'sort_num' => 50,
		);
	}

	

	return $results_;
});


?>