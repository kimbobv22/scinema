<?php

define('APP_CRYPT_PRIVATE_KEY', "-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQIBLZgmPxrInUCAggA
MBQGCCqGSIb3DQMHBAiHXQasyIvklwSCBMgDCyE+c7bfhhib9eqvypYRndqkBhb/
hsDDp40tebMBGHH/TPWK4L6PCURZGKFrQHDLDCfTeWhfiviPAD7GnZr+rUDMCwvi
WJOBUeT+u1rHewn2xE4g+ENpPdo74MzdLDLhNwaA9AZqAk0i0P5e+eEIwTfSdO2A
qrzCa6fcSuW0nIwaKBhwx0ZP8UkWS2xzS5CNYN49cJV5sdraa5qnYCG4rTCVGpSh
PuQbaIL3Lz+xkSnHK9pPF8s77eT5ysqAk4Jde6HmPwAYseWzXt8exH8AMkUXEbgJ
7iorTebrN7l/GqETlLN30gUlaiOhglrdO6vu5tGVeAMyzKDt3gu8oFwQnegyqVQL
i/7d8yQgZ5OAr91dPfsETBBfHLPz3ziI5Ls5SWt6PemIjdXbtX9lWKYyy/kzvcAb
TLM0xS71xnFYRTIZ7FSMFoUEQQuxiumlgrKX0tc2l6GCQ2INV/9G6I9V1wlGyjoa
dMq/+XoC4b2VWg8sb46BS14tu4u+B0nJEgkaqvKI9+B6Owv83+pvXj8R1lmQoRgZ
RS0IeXg2ndCBbGiETVrWUPGxSdtQS7s/il22Kehypd0qGHio533wmjWkN97zWPuW
TxsVzOdqO6i7vsCFdCrlZX4eYE8R7e1XQOLr5QdCTWgPQdZSjpF0x3P6u1PNn6Uq
wkRdtt8SbXI0TQLK+F+GgntoitPRcDx0KuDI5aL262FJHz4n/aMAiOJBwZzpxaE3
KwBq2SI3i/7Z//9KQMyXzec+k3WnUJRMhFtyHFj+HJKcqzMjvrER2GlrAxJBgsoM
YIluI4/4W8r1ZUneaEJ28rXaqqbRVk27cPRzG3e0Bq6ByKXb5N+9LSRt64LrnuLT
VxmIYxHXTtGRpuBSaKa3GTaL6inJxyj6zxOEycJYfzG29L7ktZmKiMOF6eSdPNJA
cevtXP9yjrWq1RjzFeAvOK9baTidzYRpWbCz6MUB4h3c877vro35gw/ADe7LeeoL
fLxS3Mdb5VSS22sGy/oChpeWzzYC+vh7MpAXV9G/cA7GFkxOQlWp63tSLma6iMne
FYPLaiFNosygfhp8K2dr0BKun8inFAQkX1hyA9OXnht9urTtazpDqBx5KZnCko/g
d7N6tW4gvO7yDndMshpQ+P2D5kXUafFh+4EwyBwrOgollWC6U8Rq4pGRoCSszysk
0et321FiXR6LcuFGmD9U2A695zEFw/7ebamkYu6Bb68nVQimLxxnH+Q70Bzx7Nw1
IgAOXtHnetTOjG3ZZpDXAtUTAB4ScIjMUUEb9o1hG9fF8i7LM0leZ2UOeUyCGnw4
ZZn2f4e3RM6tJh4oyHg5wCW9j6d6jrjQwDen06WXFAPOLOZwwO4C4hvh8sQDE889
DgNt3Q4xiM9pZhbv5W1Vik6Ik/4FhQJV7RbZ6ak8zwLbcv7ERlELyuufF8QcZZg7
SahA5PzlXMbIf4zNaD86V8T2DeLXOp2bUq5vLawxEZBwoh1rfngfSbfxZ5H4vjyI
Eu9FmOitWaoyEYQcfyv5BevER7JHiEEElCyCSqJPDMIspfv6s0GkiBcwIe1PXPRQ
fuC+6ji8gj/EMT70N3T4LQSQQwLDsW1F+lsV+NKKnfjRNW+5pusxD9+kSqMKl+MY
24o=
-----END ENCRYPTED PRIVATE KEY-----");

define('APP_CRYPT_OPEN_KEY', "t5+qRf33aibDevE/5r74MIvn1a4yOZ3yHQVsB5/gtXfq9tKFg8r3jhbDut+p4f/zQ3BWf8BHN0G2zwqPM+O3s9e9NhEFiskkQ5v/ua+mcS4RfyCwDG84zDPTTarLwvFpw3uHP6MF6wuhvdW5LR9VFe1LFMMPA7nFEg/ZZDX38pNJSndBAeqR6eAEGKBfmDYRdROjrcziOXN4k3mD4S0s+ab/h9Q4NAILfz8DUE9ef5ysYp4RWEaV91CmNN3A/82jqLcVUbBlfYeupvoT+HvsSAYxkjUIf2lNEIvGbahMasaeH0snRUGJPCXBXbNk/eKvaho2ujyZZw+Uiz5iD7+J+Q==|010001");

pb_add_ajax('app-encrypt-get-open-key', function(){
	header("Content-Type:text/plain; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");
	// header("Access-Control-Allow-Credentials: true");

	echo APP_CRYPT_OPEN_KEY;
	die();
});

pb_add_ajax('app-load-cjform-data', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

	$target_data_ = apply_filters('pb_decrypt', $target_data_, array('customer_name'));

	$shared_crypt_keys_ = pb_crypt_shared_keys();

	$open_key_ = $shared_crypt_keys_['public_key_info']['n']."|".$shared_crypt_keys_['public_key_info']['e'];

	echo json_encode(array(
		'success' => true,
		'open_key' => $open_key_,
	));
	die();	
});



//영화관정보
pb_add_ajax('app-load-cinema-list', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

	$cinema_list_ = pb_cinema_list(array(
		'cinema_status' => '00001',
		'cinema_type' => array(PB_CINEMA_CTG_NOR, PB_CINEMA_CTG_PNDM),
		'orderby' => ' order by area_id asc, cinema_name asc '
	));
	
	$temp_area_list_ = pb_gcode_dtl_list('C0005');
	$area_list_ = array();

	foreach($temp_area_list_ as $area_data_){
		$area_list_[$area_data_->code_did] = array();
	}

	foreach($cinema_list_ as $cinema_data_){
		$area_list_[$cinema_data_->area_id][] = $cinema_data_;
	}

	foreach($area_list_ as &$area_data_){
		uasort($results_, function($a_val_,$b_val_){
			$a_str_key_ = $a_val_['hall'].$a_val_['time'];
			$b_str_key_ = $b_val_['hall'].$b_val_['time'];
			
			return $b_str_key_ < $a_str_key_;
		});
	}

	echo json_encode(array(
		'cinema_list' => $cinema_list_,
		'cinema_tree' => $area_list_,
		'area_code' => $temp_area_list_,
	));
	die();
});

//영화정보
pb_add_ajax('app-load-movie-info', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

	// $target_url_ = "http://www.scinema.org/json/screen_info.asp";

	$branch_cinema_id_ = isset($_GET['cinema_id']) ? $_GET['cinema_id'] : null;
	$branch_on_screening_ = isset($_GET['branch_on_screening']) ? $_GET['branch_on_screening'] : "N";

	$screening_list_ = pb_movie_open_list(array(
		// 'on_screening' => true,
		'status' => '00001',
		// 'on_screening_at_branch' => true,
		'for_app_screening' => true,
		// '_for_app_branch_cinema_id' => $branch_cinema_id_,
		// '_for_app_branch_on_screening' => ($branch_on_screening_ === "Y"),
		'cinema_id' => PB_CINEMA_HEAD_OFFICE_ID,
		// 'limit' => array($search_param_['offset'], $search_param_['limit']),
	));



	echo json_encode($screening_list_);
	die();
	
});

//영화상영정보
pb_add_ajax('app-load-movie-screen-info', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

	// $target_url_ = "http://www.scinema.org/json/screen_info.asp";

	$branch_cinema_id_ = isset($_GET['cinema_id']) ? $_GET['cinema_id'] : null;
	$target_date_ = isset($_GET['target_date']) ? $_GET['target_date'] : null;


	if(strlen($target_date_)){
		$screening_list_ = pb_movie_screen_list(array(
			'at_screen_date' => $target_date_,
			'status' => '00001',
			'cinema_id' => $branch_cinema_id_,
		));	
	}else{
		$screening_list_ = pb_movie_screen_list(array(
			'on_screening' => true,
			'status' => '00001',
			'cinema_id' => $branch_cinema_id_,
		));
	}

	

/*	$scheduled_list_ = pb_movie_screen_list(array(
		'cinema_id' => $branch_cinema_id_,
		'on_scheduled' => true,
		'keyword' => $keyword_,
		'status' => '00001',
	));
	echo json_encode(array_merge($screening_list_, $scheduled_list_));
*/

	echo json_encode($screening_list_);
	die();
});

//영화상영정보
pb_add_ajax('app-load-movie-screen-info-test', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

	// $target_url_ = "http://www.scinema.org/json/screen_info.asp";

	$branch_cinema_id_ = isset($_GET['cinema_id']) ? $_GET['cinema_id'] : null;
	$target_date_ = isset($_GET['target_date']) ? $_GET['target_date'] : null;

	if(strlen($target_date_)){
		$screening_list_ = pb_movie_screen_list(array(
			'at_screen_date' => $target_date_,
			'status' => '00001',
			'cinema_id' => $branch_cinema_id_,
		));	
	}else{
		$screening_list_ = pb_movie_screen_list(array(
			'on_screening' => true,
			'status' => '00001',
			'cinema_id' => $branch_cinema_id_,
		));
	}

	

/*	$scheduled_list_ = pb_movie_screen_list(array(
		'cinema_id' => $branch_cinema_id_,
		'on_scheduled' => true,
		'keyword' => $keyword_,
		'status' => '00001',
	));
	echo json_encode(array_merge($screening_list_, $scheduled_list_));
*/

	echo json_encode($screening_list_);
	die();
});

//영화상영정보
pb_add_ajax('app-load-movie-schedule-info', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

	// $target_url_ = "http://www.scinema.org/json/screen_info.asp";

	$cinema_id_ = (isset($_GET['cinema_id']) && strlen($_GET['cinema_id'])) ? $_GET['cinema_id'] : null;
	$screen_id_ = (isset($_GET['screen_id']) && strlen($_GET['screen_id'])) ? $_GET['screen_id'] : null;
	$target_date_ = (isset($_GET['target_date']) && strlen($_GET['target_date'])) ? $_GET['target_date'] : null;

	$screening_list_ = pb_movie_screen_list(array(
		'on_screening' => true,
		'cinema_id' => $cinema_id_,
		'ID' => $screen_id_,
	));

	$screen_data_ = $screening_list_[0];

	$screen_data_->timetable = pb_movie_screen_xticket_timetable($screen_data_->cinema_xticket_ref_code, $target_date_, $screen_data_->xticket_ref_code);
	
	echo json_encode($screen_data_);
	die();

});

//CJ폼데이타
pb_add_ajax('app-load-cjform-data', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");

	// $target_url_ = "http://www.scinema.org/json/screen_info.asp";

	$cinema_id_ = (isset($_GET['cinema_id']) && strlen($_GET['cinema_id'])) ? $_GET['cinema_id'] : null;
	$screen_id_ = (isset($_GET['screen_id']) && strlen($_GET['screen_id'])) ? $_GET['screen_id'] : null;
	$target_date_ = (isset($_GET['target_date']) && strlen($_GET['target_date'])) ? $_GET['target_date'] : null;

	$screening_list_ = pb_movie_screen_list(array(
		'on_screening' => true,
		'cinema_id' => $cinema_id_,
		'ID' => $screen_id_,
	));

	$screen_data_ = $screening_list_[0];

	$screen_data_->timetable = pb_movie_screen_xticket_timetable($screen_data_->cinema_xticket_ref_code, $target_date_, $screen_data_->xticket_ref_code);
	
	echo json_encode($screen_data_);
	die();
});

//로그인처리
pb_add_ajax('app-user-login', function(){
	header("Content-Type:application/json; charset=UTF-8");
	header("Access-Control-Allow-Origin: *");
	// header("Access-Control-Allow-Credentials: true");
	// session_start();

	$login_data_ = isset($_POST['login_data']) ? $_POST['login_data'] : null;

	if(empty($login_data_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '로그인실패',
			'error_message' => '잘못된 접근입니다.'
		));
		die();
	}

	$user_pass_ = pb_crypt_decrypt($login_data_['user_pass'], APP_CRYPT_PRIVATE_KEY);

	$result_ = pb_user_login($login_data_['login_or_email'], $user_pass_, ($login_data_['remember'] === "Y"));


	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '로그인실패',
			'error_message' => $result_->get_error_message(),
		));
		die();
	}

	$user_data_ = pb_user_by_both($login_data_['login_or_email']);

	echo json_encode(array(
		'success' => true,
		'user_data' => array(
			'ID' => $user_data_->ID,
			'user_login' => $user_data_->user_login,
			'full_name' => $user_data_->full_name,
			'phone1_1' => $user_data_->phone1_1,
			'phone1_2' => $user_data_->phone1_2,
			'phone1_3' => $user_data_->phone1_3,
		)
	));
	die();
});

pb_add_ajax('app-user-findpass', function(){
	$user_email_ = isset($_POST['user_email']) ? $_POST['user_email'] : null;	

	if(!strlen($user_email_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청',
			'error_message' => '필수값이 빠져있습니다.'
		));
		die();
	}

	$result_ = pb_user_send_email_for_password($user_email_);

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '이메일발송실패',
			'error_message' => $result_->get_error_message(),
		));
		die();	
	}

	echo json_encode(array(
		'success' => true,
	));
	die();	
});


?>