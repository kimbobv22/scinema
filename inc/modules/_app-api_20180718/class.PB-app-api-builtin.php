<?php

add_filter('pb_movie_open_list_where', function($query_, $conditions_){

	if(isset($conditions_['_for_app_branch_cinema_id']) && strlen($conditions_['_for_app_branch_cinema_id'])){

		$query_ .= " AND {$wpdb->pbmovie_open}.ID IN (
			SELECT {$wpdb->pbmovie_screen}.open_id
			FROM {$wpdb->pbmovie_screen}
			WHERE {$wpdb->pbmovie_screen}.cinema_id = '".$conditions_['_for_app_branch_cinema_id']."'
			AND   {$wpdb->pbmovie_screen}.status = '00001'
		 ) ";

		 if(isset($conditions_['_for_app_branch_on_screening']) && $conditions_['_for_app_branch_on_screening']){

			$query_ .= " AND {$wpdb->pbmovie_open}.ID IN (
				SELECT  {$wpdb->pbmovie_screen}.open_id open_id
				FROM {$wpdb->pbmovie_screen}
				WHERE (NOW() >= {$wpdb->pbmovie_screen}.screen_srt_date
					 AND (CURDATE() <= {$wpdb->pbmovie_screen}.screen_end_date OR {$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = ''))
				AND  ({$wpdb->pbmovie_screen}.xticket_ref_code is not null and {$wpdb->pbmovie_screen}.xticket_ref_code != '')
				AND  {$wpdb->pbmovie_screen}.cinema_id = '".$conditions_['_for_app_branch_cinema_id']."'
			 ) ";
		}
	}

	return $query_;
},10,2);

?>