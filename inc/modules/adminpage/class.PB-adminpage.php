<?php 

define('PB_ADMINPAGE_SLUG', 'admin');

define('PB_ADMINLVL_ADMIN', 'administrator');

function pb_adminpage_list(){
	$menu_list_ = apply_filters('pb_adminpage_list', array());

	uasort($menu_list_, function($a_, $b_){
		if(!isset($a_['sort_num'])) $a_['sort_num'] = 10;
		if(!isset($b_['sort_num'])) $b_['sort_num'] = 10;
		return ($a_['sort_num'] < $b_['sort_num'] ? -1 : 1);
	});

	return $menu_list_;
}

function pb_adminpage_data($action_){
	$adminpage_list_ = pb_adminpage_list();

	if(isset($adminpage_list_[$action_])) return $adminpage_list_[$action_];
	return null;
}

function pb_adminpage_blog_lvl(){
	if((string)get_current_blog_id() === "1") return 2; //본사 
	else return 1; //지점
}

function pb_adminpage_can_access($action_){
	$adminpage_data_ = pb_adminpage_data($action_);

	if(isset($adminpage_data_['lvl'])){
		return $adminpage_data_['lvl'] <= pb_adminpage_blog_lvl();
	}
	return true;
}

function pb_adminpage_url($action_ = null){
	$admin_url_ = home_url(PB_ADMINPAGE_SLUG);

	if(strlen($action_)){
		$admin_url_= pb_append_url($admin_url_, $action_);
	}

	return $admin_url_;
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/adminpage/class.PB-adminpage-settings.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/adminpage/class.PB-adminpage-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/adminpage/class.PB-adminpage-templates.php');

?>