<?php

add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_ADMINPAGE_SLUG : 

			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_adminpage%', '([^&]+)');
	add_rewrite_tag('%_pb_adminpage_menu_id%', '([^&]+)');
	add_rewrite_tag('%_pb_adminpage_action%', '([^&]+)');
	add_rewrite_tag('%_pb_adminpage_action_data%', '([^&]+)');
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_ADMINPAGE_SLUG.'/?$','index.php?_pb_adminpage=Y','top');
	add_rewrite_rule('^'.PB_ADMINPAGE_SLUG.'/(.+)/(.+)/(.+)/?$','index.php?_pb_adminpage=Y&_pb_adminpage_menu_id=$matches[1]&_pb_adminpage_action=$matches[2]&_pb_adminpage_action_data=$matches[3]','top');
	add_rewrite_rule('^'.PB_ADMINPAGE_SLUG.'/(.+)/(.+)/?$','index.php?_pb_adminpage=Y&_pb_adminpage_menu_id=$matches[1]&_pb_adminpage_action=$matches[2]','top');
	add_rewrite_rule('^'.PB_ADMINPAGE_SLUG.'/(.+)/?$','index.php?_pb_adminpage=Y&_pb_adminpage_menu_id=$matches[1]','top');
});

add_action('pb_template',function(){
	$admin_page_yn_ = get_query_var("_pb_adminpage");
	if($admin_page_yn_ !== "Y") return;

	if(!is_user_logged_in()){
		wp_redirect(pb_user_login_url(pb_adminpage_url()));
		die();
	}

	if(!pb_is_admin()){
		wp_die("잘못된 접근입니다.");
	}


	$admin_page_menu_id_ = get_query_var("_pb_adminpage_menu_id");
	$admin_page_action_ = get_query_var("_pb_adminpage_action");
	$admin_page_action_data_ = get_query_var("_pb_adminpage_action_data");

	$admin_page_list_ = pb_adminpage_list();
	$admin_page_item_ = isset($admin_page_list_[$admin_page_menu_id_]) ? $admin_page_list_[$admin_page_menu_id_] : null;

	if(!isset($admin_page_item_)){

		if(strlen($admin_page_menu_id_)){
			pb_404();
			return;
		}

		$admin_page_menu_id_ = 'home';
		$admin_page_item_ = array(
			'title' => '관리자홈',
			'content-template' => "home",
			'sort_num' => 0,
		);
	}else if(!pb_adminpage_can_access($admin_page_menu_id_)){
		wp_die("접근권한이 없습니다.");
		return;
	}

	global $pb_adminpage_menu_item, $pb_adminpage_menu_id,
		$pb_adminpage_action, $pb_adminpage_action_data;
	$pb_adminpage_menu_item = $admin_page_item_;
	$pb_adminpage_menu_id = $admin_page_menu_id_;
	$pb_adminpage_action = $admin_page_action_;
	$pb_adminpage_action_data = $admin_page_action_data_;

	if(strlen($admin_page_action_) && !apply_filters('pb_adminpage_has_action', false)){
		if(!isset($admin_page_item_['subcontent-templates'])
			|| !isset($admin_page_item_['subcontent-templates'][$admin_page_action_])){
			wp_die("잘못된 접근입니다");
			return;
		}
	}

	do_action('pb_adminpage_template', $pb_adminpage_action, $pb_adminpage_action_data);

	if(strlen($admin_page_menu_id_)){
		do_action('pb_adminpage_template_'.$admin_page_menu_id_, $pb_adminpage_action, $pb_adminpage_action_data);
	}
});

add_filter('pb_content_template', function($content_template_){
	$admin_page_yn_ = get_query_var("_pb_adminpage");
	if($admin_page_yn_ !== "Y") return $content_template_;

	global $pb_adminpage_menu_item, $pb_adminpage_action, $pb_adminpage_action_data;

	$sub_content_template_ = null;
	if(strlen($pb_adminpage_action) && isset($pb_adminpage_menu_item['subcontent-templates'])){
		$sub_content_template_ = $pb_adminpage_menu_item['subcontent-templates'][$pb_adminpage_action];
	}

	if(!isset($pb_adminpage_menu_item['content-template'])) return $content_template_;

	if(isset($pb_adminpage_menu_item['use-full-path']) && $pb_adminpage_menu_item['use-full-path']){
		return (strlen($sub_content_template_) ? $sub_content_template_ : $pb_adminpage_menu_item['content-template']);
	}

	return "adminpage/".(strlen($sub_content_template_) ? $sub_content_template_ : $pb_adminpage_menu_item['content-template']);
});
add_filter('pb_content_template_use_fullpath', function($result_){
	$admin_page_yn_ = get_query_var("_pb_adminpage");
	if($admin_page_yn_ !== "Y") return $result_;

	global $pb_adminpage_menu_item;

	if(isset($pb_adminpage_menu_item['use-full-path'])) return $pb_adminpage_menu_item['use-full-path'] === true;

	return false;
});

add_action('pb_adminpage_template', function(){
	add_filter('pb_post_with_sidemenu', function(){
		return true;
	});
	add_filter('pb_post_with_breadcrumb', function(){
		return true;
	});

	add_filter('pb_breadcrumb', function(){
		return PB_THEME_LIB_DIR_PATH.'modules/adminpage/views/breadcrumb.php';
	});
	add_filter('pb_sidemenu', function(){
		return PB_THEME_LIB_DIR_PATH.'modules/adminpage/views/sidemenu.php';
	});
});

add_action('init', function(){

	global $wp_roles;
	$roles_ = $wp_roles->roles;

	remove_role('editor');
	remove_role('author');
	remove_role('contributor');
	
	$roles_[PB_ADMINLVL_ADMIN]['name'] = '관리자';
	$roles_['subscriber']['name'] = '회원';
	
	$wp_roles->roles = $roles_;
});

?>