<?php

function pb_adminpage_templates($template_id_ = null){
	$result_ =  apply_filters('pb_adminpage_templates', array(
		'row' => '_pb_adminpage_template_row',
		'hidden' => '_pb_adminpage_template_hidden',
		
		'text' => '_pb_adminpage_template_text',
		'email' => '_pb_adminpage_template_text',
		'number' => '_pb_adminpage_template_text',
		'search' => '_pb_adminpage_template_text',
		'radio' => '_pb_adminpage_template_radio',
		'checkbox' => '_pb_adminpage_template_radio',

		'division' => '_pb_adminpage_template_division',

		'textarea' => '_pb_adminpage_template_textarea',
		'wp_editor' => '_pb_adminpage_template_wp_editor',
		'select' => '_pb_adminpage_template_select',
		'ajaxselect' => '_pb_adminpage_template_ajaxselect',
		'select-gcode' => '_pb_adminpage_template_select_gcode',

		'datepicker' => '_pb_adminpage_template_datepicker',
		'imagepicker' => '_pb_adminpage_template_imagepicker',
		'filepicker' => '_pb_adminpage_template_filepicker',

		'custom' => '_pb_adminpage_template_custom',
		'static' => '_pb_adminpage_template_static',
		'header' => '_pb_adminpage_template_header',
	));

	if(strlen($template_id_)) return $result_[$template_id_];

	return $result_;
}

function pb_adminpage_import_templated_value($data_, $column_name_){
	if(!isset($data_)) return null;

	switch(gettype($data_)){

		case "object" : 

			$temp_ = ((array)$data_);
			if(isset($temp_[$column_name_])){
				return $temp_[$column_name_];
			}

			break;

		case "array" : 
			if(isset($data_[$column_name_])){
				return $data_[$column_name_];
			}

			break;

		default : return apply_filter('pb_adminpage_import_templated_value', null, $data_, $column_name_);
	}

	return null;
}

function pb_adminpage_template_render_validates($validates_){

	foreach($validates_ as $validate_name_ => $validate_data_){

		switch($validate_name_){
			case 'required' : 
				echo " required data-required-error='".$validate_data_."' ";
			break;

			default : 

				$validate_value_ = null;
				$error_message_ = null;

				if(gettype($validate_data_) !== "string"){
					$validate_value_ = $validate_data_['value'];
					$error_message_ = $validate_data_['error-message'];
				}else{
					$validate_value_ = "Y";
					$error_message_ = $validate_data_;
				}

				echo " data-".$validate_name_."='".$validate_value_."' data-".$validate_name_."-error='".$error_message_."' ";


			break;
		}

	}
}

function pb_adminpage_templte_render_help_block($help_block_){
	if(empty($help_block_)) return;

	if(gettype($help_block_) !== "array"){
		$help_block_ = array($help_block_);
	}

	foreach($help_block_ as $help_text_){ ?>
	<p class="help-block"><small><?=$help_text_?></small></p>
	<?php }
}

function _pb_adminpage_template_row($attrs_ = array(), $options_ = array(), $data_ = null){

	?>

	<div class="row">

		<?php foreach($attrs_['columns'] as $template_){

			$column_ = isset($template_['column']) ? $template_['column'] : "col-xs-12";
			$target_draw_func_ = pb_adminpage_templates($template_['type']);

			?> <div class="<?=$column_?>"> <?php
				call_user_func_array($target_draw_func_, array($template_, $options_, $data_));
			?> </div> <?php

		?>
	
		<?php } ?>
		
	</div>

	<?php

}

function _pb_adminpage_template_hidden($attrs_ = array(), $options_ = array(), $data_ = null){

	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];

	?>

	<input type="hidden" name="<?=$attrs_['column-name']?>" value="<?=$default_value_?>" id="<?=$column_id_?>" >

	<?php
}

function _pb_adminpage_template_text($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$validates_ = isset($attrs_['validates']) ? $attrs_['validates'] : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$valid_feedback_ = isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];
	

	?>
	

<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">
	
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>

	<label for="<?=$column_id_?>"><?=$column_label_?></label>
	<input type="<?=$attrs_['type']?>" name="<?=$attrs_['column-name']?>" value="<?=$default_value_?>" placeholder="<?=$placeholder_?>"
		class="form-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>" >

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>

<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>

<?php 


}


function _pb_adminpage_template_radio($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$validates_ = isset($attrs_['validates']) ? $attrs_['validates'] : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$valid_feedback_ = isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

	$inline_el_ = isset($attrs_['inline']) ? $attrs_['inline'] === true : false;
	$radio_type_ = $attrs_['type'];
	$options_ = $attrs_['options'];
	

	?>
	

<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">

	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>
	
	<label><?=$column_label_?></label>
	
	<div class="adminpage-radio-wrap">
	<?php foreach($options_ as $opt_value_ => $opt_name_){ ?>
	
		<?php if($inline_el_){ ?>
			<label class="<?=$radio_type_?>-inline">
				<input type="<?=$radio_type_?>" name="<?=$attrs_['column-name']?>" value="<?=$opt_value_?>" class="<?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>-$opt_value_" <?=checked($default_value_, $opt_value_)?> > <?=$opt_name_?>
			</label>
		<?php }else{ ?>
		
			<div class="<?=$radio_type_?>">
				<label>
					<input type="<?=$radio_type_?>" name="<?=$attrs_['column-name']?>" value="<?=$opt_value_?>" class="<?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>-$opt_value_" <?=checked($default_value_, $opt_value_)?> > <?=$opt_name_?>
				</label>
			</div>
		<?php } ?>

	<?php } ?>
	</div>

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>

<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>

<?php 


}


function _pb_adminpage_template_custom($attrs_ = array(), $options_ = array(), $data_ = null){
	call_user_func_array($attrs_['view'], array($attrs_, $options_,$data_));
}


function _pb_adminpage_template_textarea($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$validates_ = isset($attrs_['validates']) ? $attrs_['validates'] : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$valid_feedback_ = isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

	?>
	

<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">

	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>
	
	<label for="<?=$column_id_?>"><?=$column_label_?></label>
	<textarea name="<?=$attrs_['column-name']?>" placeholder="<?=$placeholder_?>"
		class="form-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>" ><?=$default_value_?></textarea>

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>

	<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>

<div class="clearfix"></div>
</div>
<?php 


}

// $mceInit = apply_filters( 'tiny_mce_before_init', $mceInit, $editor_id );


function _pb_adminpage_template_wp_editor_filter($init_array_, $id_){
	// echo "test1";
	return $init_array_;
}
function _pb_adminpage_template_wp_editor($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$required_ = isset($attrs_['required']) && $attrs_['required'];
	
	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];


	?>
	
<?php if($required_){ ?>
<script type="text/javascript">
if(!window._pb_adminpage_template_wp_editor_requireds){
	window._pb_adminpage_template_wp_editor_requireds = {};
}
_pb_adminpage_template_wp_editor_requireds = $.extend(_pb_adminpage_template_wp_editor_requireds, {
	"<?=$column_id_?>" : {
		error_title : "<?=$attrs_['required-error-title']?>",
		error_message : "<?=$attrs_['required-error-message']?>",
	}
});
</script>
<?php } ?>
<div class="form-group wp-editor-form-group" >
	
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>

	<label for="<?=$column_id_?>"><?=$column_label_?></label>
	<div class="image-upload-button-wrap">
		<a href="#" data-wp-editor-image-upload-btn="<?=$column_id_?>" class="btn btn-sm btn-default"> <i class="fa fa-plus"></i>이미지삽입</a>
	</div>
	<?php 

		add_filter('tiny_mce_before_init', '_pb_adminpage_template_wp_editor_filter', 10, 2);
		wp_editor(stripslashes($default_value_),$column_id_, array(
			'media_buttons' => false,
			'quicktags' => false,
			'wpautop' => false,
			'editor_class' => "pb-adminpage-templates-wp-editor",
			'textarea_name' => $attrs_['column-name'],
		));
		remove_filter('tiny_mce_before_init', '_pb_adminpage_template_wp_editor_filter');

	?>

<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>

<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>
<?php 


}


function _pb_adminpage_template_select($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$validates_ = isset($attrs_['validates']) ? $attrs_['validates'] : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$form_group_ = isset($attrs_['form-group']) && $attrs_['form-group'];
	$valid_feedback_ = $form_group_ && isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

	$options_ = isset($attrs_['options']) ? $attrs_['options'] : array();

	if(is_callable($options_)){
		$options_ = call_user_func_array($options_, array($data_));
	}

	?>
	
<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">
	
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>

	<label for="<?=$column_id_?>"><?=$column_label_?></label>
	<select name="<?=$attrs_['column-name']?>"
		class="form-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>" >
		
		<?php if(strlen($placeholder_)){ ?>
			<option value=""><?=$placeholder_?></option>
		<?php } ?>

		<?php foreach($options_ as $value_ => $name_){ ?>
			<option value="<?=$value_?>" <?=selected($default_value_, $value_)?> ><?=$name_?></option>
		<?php } ?>

	</select>

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>
<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>

	
<?php 
}

function _pb_adminpage_template_ajaxselect($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$validates_ = isset($attrs_['validates']) ? $attrs_['validates'] : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$form_group_ = isset($attrs_['form-group']) && $attrs_['form-group'];
	$valid_feedback_ = $form_group_ && isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

	$default_data_ = $attrs_['default-data'];
	$converted_default_data_ = null;

	if(strlen($default_value_)){
		$default_data_ = (array)call_user_func_array($default_data_, array($default_value_));

		$converted_default_data_ = json_encode($default_data_);
		$converted_default_data_ = htmlentities(($converted_default_data_));
	}


	$render_template_ = call_user_func($attrs_['render-template']);

	if(strlen($render_template_)){
		$render_template_ = htmlentities(stripslashes($render_template_));
	}

	$additional_params_ = $attrs_['additional_params'];
	$additional_params_ = implode("|", $additional_params_);

	?>
	
<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">
		
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>

	<label for="<?=$column_id_?>"><?=$column_label_?></label>
	<select name="<?=$attrs_['column-name']?>" data-label-field="<?=$attrs_['select-label']?>" data-value-field="<?=$attrs_['select-value']?>"
		 data-default-data="<?=$converted_default_data_?>" data-render-template="<?=$render_template_?>" data-adminpage-ajaxselect
		class="form-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>" placeholder="<?=$placeholder_?>" data-additional-params="<?=$additional_params_?>">
			
		<?php if(strlen($converted_default_data_)){ ?>
		<option value="<?=$default_data_[$attrs_['select-value']]?>" ><?=$default_data_[$attrs_['select-label']]?></option>
		<?php } ?>

	</select>

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>
<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>

	
<?php 
}

function _pb_adminpage_template_imagepicker($attrs_ = array(), $options_ = array(), $data_ = null){
	$attrs_ = array_merge(array(
		'maxwidth' => null,
		'maxlength' => 1,
		'upload-label' => "이미지업로드",
		'data-type' => "string", // or json
	), $attrs_);

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$required_ = isset($attrs_['required']) && strlen($attrs_['required']) ? $attrs_['required'] : null;
	$validates_ = isset($required_) ? array($required_) : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$valid_feedback_ = isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

	if(!strlen($default_value_)){
		$default_value_ = "";
	}else{
		if($attrs_['data-type'] === "string"){
			$attrs_['maxlength'] = 1;
			$default_value_ = htmlentities(json_encode(array(array(
				'name' => $default_value_,
				'url' => $default_value_,
				'thumbnail_url' => $default_value_,
				'type' => "image/*",
			))));
		}else{
			$default_value_ = htmlentities(stripslashes($default_value_));
		}
	}
	
	
	?>
	
<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">
	
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>

	<label><?=$column_label_?></label>
	<input type="text" name="<?=$attrs_['column-name']?>" value="<?=$default_value_?>"
		class="hidden-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>-input" tabindex="-1" data-adminpage-template-imagepicker-input>
	<input type="hidden" name="_<?=$attrs_['column-name']?>-hidden" id="<?=$column_id_?>" data-adminpage-template-imagepicker="#<?=$column_id_?>-input" value="<?=$default_value_?>" 
	 data-maxlength="<?=$attrs_['maxlength']?>" data-upload-label="<?=$attrs_['upload-label']?>" data-type="<?=$attrs_['data-type']?>" data-maxwidth="<?=$attrs_['maxwidth']?>">

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>
<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>

<?php 


}
function _pb_adminpage_template_filepicker($attrs_ = array(), $options_ = array(), $data_ = null){
	$attrs_ = array_merge(array(
		'maxlength' => 1,
		'upload-label' => "파일업로드",
		'data-type' => "string", // or json
	), $attrs_);

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$required_ = isset($attrs_['required']) && strlen($attrs_['required']) ? $attrs_['required'] : null;
	$validates_ = isset($required_) ? array($required_) : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$valid_feedback_ = isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

	if(!strlen($default_value_)){
		$default_value_ = "[]";
	}else{
		if($attrs_['data-type'] === "string"){
			$attrs_['maxlength'] = 1;
			$default_value_ = htmlentities(json_encode(array(array(
				'name' => $default_value_,
				'url' => $default_value_,
				'thumbnail_url' => $default_value_,
				'type' => "plain/*",
			))));
		}else{
			$default_value_ = htmlentities(stripslashes($default_value_));
		}
	}
	
	
	?>
	
<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">
	
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>

	<label><?=$column_label_?></label>
	<input type="text" name="<?=$attrs_['column-name']?>" value="<?=$default_value_?>"
		class="hidden-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>-input" tabindex="-1" data-adminpage-template-filepicker-input>
	<input type="hidden" name="_<?=$attrs_['column-name']?>-hidden" id="<?=$column_id_?>" data-adminpage-template-filepicker="#<?=$column_id_?>-input" value="<?=$default_value_?>" 
	 data-maxlength="<?=$attrs_['maxlength']?>" data-upload-label="<?=$attrs_['upload-label']?>" data-type="<?=$attrs_['data-type']?>">

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>
<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>

<?php 


}

function _pb_adminpage_template_datepicker($attrs_ = array(), $options_ = array(), $data_ = null){
	$attrs_ = array_merge(array(
		'format' => 'YYYY-MM-DD',
		'locale' => (substr(get_locale(), 0, 2) | 'ko'),
	), $attrs_);

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$validates_ = isset($attrs_['validates']) ? $attrs_['validates'] : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	
	$valid_feedback_ = isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

	?>
	

<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">
	
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>
	<label for="<?=$column_id_?>"><?=$column_label_?></label>

	<div class='input-group date' data-adminpage-datepicker data-format="<?=$attrs_['format']?>" data-locale="<?=$attrs_['locale']?>">
		<input type='text' name="<?=$attrs_['column-name']?>" value="<?=$default_value_?>" placeholder="<?=$placeholder_?>"
		class="form-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>" />
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>
<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
<div class="clearfix"></div>
</div>

<?php 
}

function _pb_adminpage_template_select_gcode($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	$placeholder_ = isset($attrs_['placeholder']) ? $attrs_['placeholder'] : null;
	
	$classes_ = isset($attrs_['classes']) ? $attrs_['classes'] : array();
	$classes_ = implode(" ", $classes_);

	$validates_ = isset($attrs_['validates']) ? $attrs_['validates'] : array();
	$has_validates_ = count($validates_) > 0;

	ob_start();
	pb_adminpage_template_render_validates($validates_);
	$validates_ = ob_get_clean();

	$column_id_ = $options_['id-format']."-".$attrs_['column-name'];
	$valid_feedback_ = isset($attrs_['valid-feedback']) && $attrs_['valid-feedback'];

?>

<div class="form-group <?=$valid_feedback_ ? "has-feedback" : "" ?>">
		
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>
	<label for="<?=$column_id_?>"><?=$column_label_?></label>
	<select name="<?=$attrs_['column-name']?>"
		class="form-control <?=$classes_?>" <?=$validates_?> id="<?=$column_id_?>" >
		
		<?php if(strlen($placeholder_)){ ?>
			<option value=""><?=$placeholder_?></option>
		<?php } ?>

		<?= pb_gcode_make_options($attrs_['code-id'], $default_value_) ?>

	</select>

<?php if($has_validates_){ ?>
	<div class="help-block with-errors"></div>
<?php } ?>
<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>
</div>
	
	<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
	<div class="clearfix"></div>
<?php 


}

function _pb_adminpage_template_division($attrs_ = array(), $options_ = array(), $data_ = null){
	?>
	
	<div class="form-div-border"></div>
	<div class="form-margin"></div>

	<?php 
}

function _pb_adminpage_template_static($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['column-label']) ? $attrs_['column-label'] : null;
	$default_value_ = pb_adminpage_import_templated_value($data_, $attrs_['column-name']);
	
	?>

<div class="form-group">
	
	<?php if(isset($attrs_['before'])){ call_user_func($attrs_['before']); } ?>

	<label><?=$column_label_?></label>
	<p class="form-control-static"><?=$default_value_?></p>

<?php
	$help_block_ = isset($attrs_['help-block']) ? $attrs_['help-block'] : null;
	pb_adminpage_templte_render_help_block($help_block_);
?>

<?php if(isset($attrs_['after'])){ call_user_func($attrs_['after']); } ?>
	<div class="clearfix"></div>
</div>

<?php 


}

function _pb_adminpage_template_header($attrs_ = array(), $options_ = array(), $data_ = null){

	$column_label_ = isset($attrs_['text']) ? $attrs_['text'] : null;
	
?>

<div>
	<h4><?=$column_label_?></h4>
	<div class="clearfix"></div>
</div>

<?php 


}




add_filter('pb_adminpage_has_action', function($result_){
	global $pb_adminpage_menu_item, $pb_adminpage_action, $pb_adminpage_action_data;

	if(isset($pb_adminpage_menu_item['template-data'])){
		return true;
	}

	return $result_;
});

add_filter('pb_content_template', function($content_template_){
	$admin_page_yn_ = get_query_var("_pb_adminpage");
	if($admin_page_yn_ !== "Y") return $content_template_;

	global $pb_adminpage_menu_item, $pb_adminpage_action, $pb_adminpage_action_data;

	if(isset($pb_adminpage_menu_item['template-data'])){
		global $pb_adminpage_current_template_data;

		$pb_adminpage_current_template_data = $pb_adminpage_menu_item['template-data'];

		switch($pb_adminpage_action){
			case "add" :
				return PB_THEME_LIB_DIR_PATH.'modules/adminpage/views/templated-edit';
				break;
			case "edit" :
				return PB_THEME_LIB_DIR_PATH.'modules/adminpage/views/templated-edit';
				break;
			default :
				return PB_THEME_LIB_DIR_PATH.'modules/adminpage/views/templated-list';
			break;	
		}
		
	}

	return $content_template_;

});

add_filter('pb_content_template_use_fullpath', function($bool_){
	$admin_page_yn_ = get_query_var("_pb_adminpage");
	if($admin_page_yn_ !== "Y") return $bool_;
	global $pb_adminpage_menu_item;
	if(isset($pb_adminpage_menu_item['template-data'])){
		return true;
	}

	return $bool_;
});

add_action('pb_adminpage_template', function($action_, $action_data_){
	global $pb_adminpage_menu_item, $pb_adminpage_action, $pb_adminpage_action_data;
	if(!isset($pb_adminpage_menu_item['template-data'])) return;

	switch($pb_adminpage_action){
		case "add" :

			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");

			wp_enqueue_style("page-adminpage-templated-edit", (pb_library_url() . 'css/adminpage/templated-edit.css'));
			wp_enqueue_script("page-adminpage-templated-edit", (pb_library_url() . 'js/adminpage/templated-edit.js'), array('pb-all-main'));
			
			break;
		case "edit" :

			if(!strlen($pb_adminpage_action_data)){
				wp_die("잘못된 접근입니다.");
			}

			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");

			wp_enqueue_style("page-adminpage-templated-edit", (pb_library_url() . 'css/adminpage/templated-edit.css'));
			wp_enqueue_script("page-adminpage-templated-edit", (pb_library_url() . 'js/adminpage/templated-edit.js'), array('pb-all-main'));
			break;
		default :
			wp_enqueue_style("page-adminpage-cinema-manage-list", (pb_library_url() . 'css/adminpage/templated-list.css'));
			// wp_enqueue_script("page-adminpage-cinema-manage-list", (pb_library_url() . 'js/pages/adminpage/cinema-manage/edit.js'), array('pb-all-main'));
			break;	
	}

},10, 2);

function _pb_adminpage_templated_find_column_data_rec($template_edit_map_, $column_name_){

	foreach($template_edit_map_ as $row_data_){
		if($row_data_['type'] === "row"){
			$result_ = _pb_adminpage_templated_find_column_data_rec($row_data_['columns'], $column_name_);
			if(isset($result_)) return $result_;
		}

		if($row_data_['column-name'] === $column_name_) return $row_data_;
	}

	return null;
};

pb_add_ajax('adminpage-templated-selectize-load', function(){

	$adminpage_menu_id_ = $_POST['menu_id'];
	$column_name_ = $_POST['column_name'];
	$query_ = $_POST['query'];

	$admin_page_list_ = pb_adminpage_list();
	$admin_page_item_ = isset($admin_page_list_[$adminpage_menu_id_]) ? $admin_page_list_[$adminpage_menu_id_] : null;


	if(empty($admin_page_item_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 접근",
			'error_message' => "잘못된 접근입니다.(admin menu item not found)",
		));
		die();
	}

	$template_data_ = isset($admin_page_item_['template-data']) ? $admin_page_item_['template-data'] : null;

	if(empty($admin_page_item_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 요청",
			'error_message' => "잘못된 요청입니다.(template data is null)",
		));
		die();
	}

	$template_edit_map_ = call_user_func($template_data_['templates']['edit']);

	$column_data_ = _pb_adminpage_templated_find_column_data_rec($template_edit_map_, $column_name_);

	if(empty($column_data_) || !isset($column_data_['ajax'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 요청",
			'error_message' => "잘못된 요청입니다.(column data or ajax data is null)",
		));
		die();
	}

	$result_ = call_user_func_array($column_data_['ajax'], array($query_));

	echo json_encode(array(
		'success' => true,
		'options' => $result_,
	));
	die();

});

pb_add_ajax('adminpage-templated-edit', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$adminpage_menu_id_ = $_POST['menu_id'];

	$admin_page_list_ = pb_adminpage_list();
	$admin_page_item_ = isset($admin_page_list_[$adminpage_menu_id_]) ? $admin_page_list_[$adminpage_menu_id_] : null;

	if(empty($admin_page_item_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 접근",
			'error_message' => "잘못된 접근입니다.(admin menu item not found)",
		));
		die();
	}

	$template_data_ = isset($admin_page_item_['template-data']) ? $admin_page_item_['template-data'] : null;

	if(empty($admin_page_item_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 요청",
			'error_message' => "잘못된 요청입니다.(template data is null)",
		));
		die();
	}

	$target_data_ = isset($_POST['target_data']) ? $_POST['target_data'] : null;

	if(empty($target_data_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 요청",
			'error_message' => "빈 요청입니다.(data is null)",
		));
		die();
	}

	$is_new_ = $_POST['add_new'] === "Y";

	$target_func_ = $is_new_ ? $template_data_['backend']['add'] : $template_data_['backend']['edit'];

	$result_ = call_user_func_array($target_func_, array($target_data_));

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => $result_->get_error_code(),
			'error_message' => $result_->get_error_message(),
		));
		die();
	}

	echo json_encode(array(
		'success' => true,
		'redirect_url' => pb_adminpage_url($adminpage_menu_id_."/edit/".$result_),
	));
	die();
});

pb_add_ajax('adminpage-templated-remove', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	
	$adminpage_menu_id_ = $_POST['menu_id'];

	$admin_page_list_ = pb_adminpage_list();
	$admin_page_item_ = isset($admin_page_list_[$adminpage_menu_id_]) ? $admin_page_list_[$adminpage_menu_id_] : null;

	if(empty($admin_page_item_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 접근",
			'error_message' => "잘못된 접근입니다.(admin menu item not found)",
		));
		die();
	}

	$template_data_ = isset($admin_page_item_['template-data']) ? $admin_page_item_['template-data'] : null;

	if(empty($admin_page_item_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 요청",
			'error_message' => "잘못된 요청입니다.(template data is null)",
		));
		die();
	}

	$target_data_ = isset($_POST['target_data']) ? $_POST['target_data'] : null;

	if(empty($target_data_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => "잘못된 요청",
			'error_message' => "빈 요청입니다.(data is null)",
		));
		die();
	}

	$result_ = call_user_func_array($template_data_['backend']['remove'], array($target_data_));

	if(is_wp_error($result_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => $result_->get_error_code(),
			'error_message' => $result_->get_error_message(),
		));
		die();
	}

	echo json_encode(array(
		'success' => true,
		'redirect_url' => pb_adminpage_url($adminpage_menu_id_),
	));
	die();

});




?>