<?php

global $pb_adminpage_menu_item, $pb_adminpage_menu_id,
		$pb_adminpage_action, $pb_adminpage_action_data, $pb_adminpage_current_template_data;

$pb_adminpage_edit_obj = array();
$is_new_ = !strlen($pb_adminpage_action_data);

if(!$is_new_){

	
	$pb_adminpage_edit_obj = call_user_func_array($pb_adminpage_current_template_data['data']['single'], array($pb_adminpage_action_data));
	
}

$empty_obj_ = isset($pb_adminpage_current_template_data['data']['empty']) ? call_user_func($pb_adminpage_current_template_data['data']['empty']) : array();

$pb_adminpage_edit_obj = array_merge($empty_obj_, (array)$pb_adminpage_edit_obj);

$edit_options_ = $pb_adminpage_current_template_data['options']['edit-options'];
$not_allow_remove_ = isset($edit_options_['not-allow-remove']) && $edit_options_['not-allow-remove'] === true;

$redirect_to_list_ = isset($edit_options_['add-redirect-to-list']) && ($edit_options_['add-redirect-to-list']);
$edit_redirect_to_list_ = isset($edit_options_['edit-redirect-to-list']) && ($edit_options_['edit-redirect-to-list']);

$edit_templates_ = $pb_adminpage_current_template_data['templates']['edit'];
if(is_callable($edit_templates_)){
	$edit_templates_ = call_user_func($edit_templates_);
}

$templates_map_list_ = pb_adminpage_templates();

$edit_label_ = array_merge(array(
	'edit' => '수정하기',
	'add' => '등록하기',
	'remove' => '삭제하기',
	'add-confirm-title' => '추가확인',
	'add-confirm-message' => '해당 항목을 추가하시겠습니까?',
	'add-success-title' => '추가완료',
	'add-success-message' => '항목이 정상적으로 등록되었습니다.',
	'add-error-title' => '에러발생',
	'add-error-message' => '항목 추가 중 에러가 발생하였습니다.',
	'edit-success-title' => '수정성공',
	'edit-success-message' => '항목정보가 정상적으로 수정되었습니다.',
	'edit-error-title' => '에러발생',
	'edit-error-message' => '항목 수정 중 에러가 발생하였습니다.',
	'remove-confirm-title' => '삭제확인',
	'remove-confirm-message' => '해당 항목을 삭제하시겠습니까?',
	'remove-success-title' => '삭제성공',
	'remove-success-message' => '항목이 삭제되었습니다.',
	'remove-error-title' => '에러발생',
	'remove-error-message' => '항목 삭제 중 에러가 발생하였습니다.',

),$edit_options_['label']);

?>
<script type="text/javascript">
var _pb_templated_edit_label_var = <?=json_encode($edit_label_)?>;
</script>
<div class="text-right">

<?php

	// print_r(pb_adminpage_url($pb_adminpage_menu_id)."/");


	$list_url_ = pb_adminpage_url($pb_adminpage_menu_id)."/";


	if(strpos($_SERVER['HTTP_REFERER'], $list_url_."add") !== false || strpos($_SERVER['HTTP_REFERER'], $list_url_."edit") !== false){
		$temp_ = pb_session_get("_PB_ADMINPAGE_LIST_URL");
		if(strlen($temp_)){
			$list_url_ = $temp_;
		}

	}else if(strpos($_SERVER['HTTP_REFERER'], $list_url_) >=0){
		$list_url_ = $_SERVER['HTTP_REFERER'];

		pb_session_put("_PB_ADMINPAGE_LIST_URL", $list_url_);
	}



 ?>
	<a href="<?=$list_url_?>" class="btn btn-default btn-sm">목록으로</a>
</div>
<div class="form-margin-xxs"></div>
<div class="adminpage-templated-edit-frame"><form method="POST" id="pb-templated-edit-edit-form" data-add-new='<?=$is_new_ ? "Y" : "N" ?>' data-adminpage-menu-id="<?=$pb_adminpage_menu_id?>" class="pb-adminpage-template-edit-form" data-redirect-to-list="<?=$redirect_to_list_ ? "Y" : "N"?>" data-edit-redirect-to-list="<?=$edit_redirect_to_list_ ? "Y" : "N"?>" data-list-url="<?=pb_adminpage_url($pb_adminpage_menu_id)."/"?>">
	
	<div class="panel panel-default">
		<div class="panel-body">
		

		<?php foreach($edit_templates_ as $row_data_){

			if(!isset($templates_map_list_[$row_data_['type']])){
				echo "template not found";
				continue;
			}

			$templates_map_ = $templates_map_list_[$row_data_['type']];

			call_user_func_array($templates_map_, array($row_data_, $pb_adminpage_current_template_data['options'], $pb_adminpage_edit_obj));
		?>
		<?php } ?>

		</div>
	</div>
	<div class="button-area text-right">
		<?php if(isset($edit_options_['custom-bottom-button'])){ 
			call_user_func_array($edit_options_['custom-bottom-button'], array($pb_adminpage_edit_obj, $is_new_));
		}else{ ?>
			
			<?php if($is_new_){ ?>
				<button type="submit" class="btn btn-primary"><?=$edit_label_['add']?></button>
			<?php }else{ ?>

			<?php if(!$not_allow_remove_){ ?>
				<button type="button" class="btn btn-black" data-remove-btn><?=$edit_label_['remove']?></button>
			<?php } ?>
				<button type="submit" class="btn btn-primary"><?=$edit_label_['edit']?></button>
			<?php } ?>
		<?php } ?>
		
	</div>

	

</form></div>