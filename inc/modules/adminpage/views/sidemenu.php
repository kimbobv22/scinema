<?php
	
	global $pb_adminpage_menu_item, $pb_adminpage_menu_id;
	$adminpage_list_ = pb_adminpage_list();

	?>
		<h3 class="sidemenu-title">관리자메뉴</h3>
		
	<ul class="sidemenu-list">

		<li class="<?= $pb_adminpage_menu_id === "home" ? "active" : ""?>">
			<a href="<?=pb_adminpage_url()?>">
				관리자홈
				<i class="icon fa fa-angle-right" aria-hidden="true"></i>
			</a>
		</li>

	<?php

		foreach($adminpage_list_ as $menu_id_ => $row_data_){

			if(!pb_adminpage_can_access($menu_id_)) continue;


			?>
			<li class="<?=$menu_id_ === $pb_adminpage_menu_id ? "active" : ""?>">

				<?php

					if(isset($row_data_['url'])){
						$target_url_ = $row_data_['url'];
					}else{
						$target_url_ = pb_adminpage_url($menu_id_);
					}
					

				?>
				<a href="<?=$target_url_?>" data-adminpage-action="<?=$menu_id_?>">
					<?=$row_data_['title']?>
					<i class="icon fa fa-angle-right" aria-hidden="true"></i>
				</a>
			</li>
		<?php }	

?> 
	<li>
		<a href="<?=pb_user_logout_url()?>">로그아웃
			<i class="icon fa fa-angle-right" aria-hidden="true"></i>
		</a>
	</li>

</ul> <?php

?>