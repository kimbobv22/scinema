<?php
	
class PB_adminpage_templated_list_table extends PBListTable{

	function prepare(){

		global $pb_adminpage_current_template_data, $pb_adminpage_menu_id;

		$options_ = isset($pb_adminpage_current_template_data['options']) ? $pb_adminpage_current_template_data['options'] : array();
		$table_options_ = isset($options_['list-options']) ? $options_['list-options'] : array();
		$table_options_ = array_merge(array(
			"hide-pagenav" => false,
			'pagenav-number' => true,
			"pagenav-count" => 5,
			
			'hide-header' => false,
			'class' => 'table table-hover tinytable',
			'per-page' => 15,
		), $table_options_);

		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$offset_ = $this->offset($page_index_, $table_options_['per-page']);

		$template_data_ = $pb_adminpage_current_template_data['data'];
		
		$data_list_ = call_user_func_array($template_data_['list'], array($offset_, $table_options_['per-page']));

		return array(
			"page_index" => $page_index_,
			"per_page" => $table_options_['per-page'],
			
			'total_count' => call_user_func_array($template_data_['total-count'], array()),
			
			"hide_pagenav" => $table_options_['hide-pagenav'],
			'pagenav_number' => $table_options_['pagenav-number'],
			"pagenav_count" => $table_options_['pagenav-count'],
			'hide_header' => $table_options_['hide-header'],

			'items' => $data_list_,

			'html_default_class' => $table_options_['class'],
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function columns(){

		global $pb_adminpage_current_template_data;

		$list_columns_ = $pb_adminpage_current_template_data['templates']['list'];

		if(is_callable($list_columns_)){
			$list_columns_ = call_user_func($list_columns_);
		}

		$result_ = array();

		foreach($list_columns_ as $column_name_ => $column_data_){
			if(gettype($column_data_) === "string"){
				$result_[$column_name_] = $column_data_;
			}else{
				$result_[$column_name_] = $column_data_['column-label'];
			}
		}

		return $result_;
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		global $pb_adminpage_current_template_data;
		$list_columns_ = $pb_adminpage_current_template_data['templates']['list'];

		if(is_callable($list_columns_)){
			$list_columns_ = call_user_func($list_columns_);
		}

		$row_template_ = isset($list_columns_[$column_name_]) ? $list_columns_[$column_name_] : null;
		$column_value_ = pb_adminpage_import_templated_value($item_, $column_name_);

		if(gettype($row_template_) === "string"){
			return $column_value_;
		}

		if(isset($row_template_['td-render-func'])){
			ob_start();
			call_user_func_array($row_template_['td-render-func'], array($item_, $column_name_));
			return ob_get_clean();
		}

		return $column_value_;
	}

	function norowdata(){
		global $pb_adminpage_current_template_data;
		$options_ = isset($pb_adminpage_current_template_data['options']) ? $pb_adminpage_current_template_data['options'] : array();
		$table_options_ = isset($options_['list-options']) ? $options_['list-options'] : array();

		if(isset($table_options_['label']) && isset($table_options_['label']['notfound'])) return $table_options_['label']['notfound'];
	
		return "검색된 항목이 없습니다.";	
	}

	function column_header_classes($column_name_){
		global $pb_adminpage_current_template_data;
		$list_columns_ = $pb_adminpage_current_template_data['templates']['list'];
		
		if(is_callable($list_columns_)){
			$list_columns_ = call_user_func($list_columns_);
		}

		$row_template_ = isset($list_columns_[$column_name_]) ? $list_columns_[$column_name_] : null;

		if(gettype($row_template_) === "string") return "";

		return $row_template_['th-class'];
	}
	function column_body_classes($column_name_, $item_){
		global $pb_adminpage_current_template_data;
		$list_columns_ = $pb_adminpage_current_template_data['templates']['list'];

		if(is_callable($list_columns_)){
			$list_columns_ = call_user_func($list_columns_);
		}

		$row_template_ = isset($list_columns_[$column_name_]) ? $list_columns_[$column_name_] : null;
		if(gettype($row_template_) === "string") return "";

		return $row_template_['td-class'];
	}
	
}


add_filter('pb-listtable-before-pagenav-pb-adminpage-templated-list-table', function(){
	global $pb_adminpage_current_template_data, $pb_adminpage_menu_id;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;

	$options_ = isset($pb_adminpage_current_template_data['options']) ? $pb_adminpage_current_template_data['options'] : array();
	$table_options_ = isset($options_['list-options']) ? $options_['list-options'] : array();

	if(isset($table_options_['label']))

	$add_btn_label_ = isset($table_options_['label']) && isset($table_options_['label']['add']) ? $table_options_['label']['add'] : "등록하기" ;


?>
<div class="form-margin"></div>


<?php });

	
	global $pb_adminpage_current_template_data, $pb_adminpage_menu_id;
?>
<div class="adminpage-templated-list-frame">
<?php

	
	global $pb_adminpage_current_template_data, $pb_adminpage_menu_id;
	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;

	$options_ = isset($pb_adminpage_current_template_data['options']) ? $pb_adminpage_current_template_data['options'] : array();
	$table_options_ = isset($options_['list-options']) ? $options_['list-options'] : array();

	if(isset($table_options_['label']))
		$add_btn_label_ = isset($table_options_['label']) && isset($table_options_['label']['add']) ? $table_options_['label']['add'] : "등록하기" ;

?>

	
	<form id="pb-adminpage-templated-list-table-form" method="get" action="<?=pb_adminpage_url($pb_adminpage_menu_id)?>">
		<!-- <input type="hidden" name="page_index	" value="0"> -->

<?php if(isset($table_options_['custom-search-frame'])){
	call_user_func_array($table_options_['custom-search-frame'], array($options_, $table_options_));
}else{ ?>

<div class="search-frame">
	<div class="row">
		<div class="col-xs-3 col-sm-8">
			<a href="<?=pb_make_url(pb_adminpage_url($pb_adminpage_menu_id.'/add'))?>" class="btn btn-default btn-sm"><?=$add_btn_label_?></a>
		</div>
		<div class="col-xs-9 col-sm-4">
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit">검색</button>
				</span>
			</div>
		</div>
	</div>
</div>

	
<?php }

	global $pb_adminpage_current_template_data, $pb_adminpage_menu_id;
?>

	<div class="form-margin"></div>
		
		<?php
			$list_table_ = new PB_adminpage_templated_list_table("pb-adminpage-templated-list-table", "pb-adminpage-templated-list-table");
			echo $list_table_->html();
		?>
	</form>

</div>