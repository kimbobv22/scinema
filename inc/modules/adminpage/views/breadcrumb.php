<?php
	
	global $pb_adminpage_menu_item, $pb_adminpage_menu_id;

	$adminpage_list_ = pb_adminpage_list();

	if($pb_adminpage_menu_id !== "home"){ ?>


	<li class=""><a href="<?=pb_adminpage_url()?>">관리자메뉴</a></li>
	<li class="dropdown">
		<a href="#" data-toggle="dropdown"><?=$pb_adminpage_menu_item['title']?> <i class="icon fa fa-angle-down" aria-hidden="true"></i></a>
		<ul class="dropdown-menu">
			<?php foreach($adminpage_list_ as $menu_id_ => $menu_data_){
				if(!pb_adminpage_can_access($menu_id_)) continue;
			?>
				<li><a href="<?=pb_adminpage_url($menu_id_)?>"><?=$menu_data_['title']?></a></li>
			<?php } ?>
		</ul>

	</li>

	<?php }else{ ?>
		<li class="dropdown">
			<a href="#" data-toggle="dropdown">관리자메뉴 <i class="icon fa fa-angle-down" aria-hidden="true"></i></a>
			<ul class="dropdown-menu">
				<?php foreach($adminpage_list_ as $menu_id_ => $menu_data_){
					if(!pb_adminpage_can_access($menu_id_)) continue;
				?>
					<li><a href="<?=pb_adminpage_url($menu_id_)?>"><?=$menu_data_['title']?></a></li>
				<?php } ?>
			</ul>

		</li>
	<?php }


?>