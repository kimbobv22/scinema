<?php
add_action('init', function(){
	global $wpdb;
	$wpdb->pbbanner_main = "{$wpdb->base_prefix}banner_main";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){
	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}banner_main` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',	

		`banner_title` varchar(200)  DEFAULT NULL COMMENT '배너명',
		`banner_html` longtext  DEFAULT NULL COMMENT '배너HTML',
        `banner_kind` varchar(20)  DEFAULT NULL COMMENT '배너종류',

        `banner_left_color` varchar(20)  DEFAULT NULL COMMENT '배너왼쪽컬러',
        `banner_right_color` varchar(20)  DEFAULT NULL COMMENT '배너오른쪽컬러',

		`image_url` varchar(500)  DEFAULT NULL COMMENT '배너 이미지',
		`link_url` varchar(500)  DEFAULT NULL COMMENT '배너 링크',

        `link_target_yn` varchar(1)  DEFAULT NULL COMMENT '배너 링크 새창유무',
        `hide_yn` varchar(1)  DEFAULT 'Y' COMMENT '숨기기',

	    `srt_date` datetime DEFAULT NULL COMMENT '게시시작일자',
        `end_date` datetime DEFAULT NULL COMMENT '게시종료일자',

		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,	
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='메인배너';";

	return $args_;
},10, 3);

?>