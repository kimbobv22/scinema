<?php
add_filter('pb_adminpage_list', function($results_){
	if(pb_cinema_current_is_head_office()){
		$results_['banner-main-manage'] = array(
			'title' => '메인배너관리' ,
			'content-template' => "banner-main-manage/list",
			'subcontent-templates' => array(
				"edit" => "banner-main-manage/edit",
				"add" => "banner-main-manage/edit",
			),
			'lvl' => 2,
			'sort_num' =>54,
		);	
    }
    return $results_;
});
?>