<?php

// 조회
function pb_banner_main_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
					{$wpdb->pbbanner_main}.ID ID
					,{$wpdb->pbbanner_main}.banner_title banner_title
					,{$wpdb->pbbanner_main}.banner_html banner_html
                    ,{$wpdb->pbbanner_main}.banner_kind banner_kind
                    ,{$wpdb->pbbanner_main}.banner_left_color banner_left_color
                    ,{$wpdb->pbbanner_main}.banner_right_color banner_right_color
                    ,{$wpdb->pbbanner_main}.image_url image_url
                    ,{$wpdb->pbbanner_main}.link_url link_url
                    ,{$wpdb->pbbanner_main}.link_target_yn link_target_yn
                    ,{$wpdb->pbbanner_main}.hide_yn hide_yn
                    ,{$wpdb->pbbanner_main}.srt_date srt_date
                    ,{$wpdb->pbbanner_main}.end_date end_date
					,{$wpdb->pbbanner_main}.reg_date reg_date
                    ,DATE_FORMAT({$wpdb->pbbanner_main}.reg_date, '%Y.%m.%d') reg_date_ymd
					,{$wpdb->pbbanner_main}.mod_date mod_date
                    ,DATE_FORMAT({$wpdb->pbbanner_main}.mod_date, '%Y.%m.%d') mod_date_ymd
				FROM {$wpdb->pbbanner_main}
            ";

    $query_ .= apply_filters('pb_banner_main_list_join', "", $conditions_);
    
	$query_ .= ' WHERE 1 ';

    //아이디값으로 조회
	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbbanner_main}.ID")." ";
    }

    //현재 표시 기간인지 체크 
    if(isset($conditions_['in_date']) && $conditions_['in_date'] === true){
		$query_ .= " AND NOW() BETWEEN {$wpdb->pbbanner_main}.srt_date AND {$wpdb->pbbanner_main}.end_date ";	
	}

	// 메인,이벤트 배너 종류로 조회
	if(isset($conditions_['banner_kind'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['banner_kind'], "{$wpdb->pbbanner_main}.banner_kind")." ";
	}
	
	// 숨김여부 체크
	if(isset($conditions_['hide_yn'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['hide_yn'], "{$wpdb->pbbanner_main}.hide_yn")." ";
	}

    if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_banner_main_list_keyword', array(
			"{$wpdb->pbbanner_main}.banner_title",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_banner_main_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_banner_main_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date desc';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_banner_main_list', $wpdb->get_results($query_));

    return $results_;
}

function pb_banner_main($banner_id_){
	if(!strlen($banner_id_)) return null;
	$result_ = pb_banner_main_list(array('ID' => $banner_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_banner_main_fields($data_){
	return pb_format_mapping(apply_filters("pb_banner_main_fields_map",array(

        'banner_title' => '%s',
        'banner_html' => '%s',
        'banner_kind' => '%s',
        'banner_left_color' => '%s',
        'banner_right_color' => '%s',
        'image_url' => '%s',
        'link_url' => '%s',
        'link_target_yn' => '%s',
        'hide_yn' => '%s',
        'srt_date' => '%s',
		'end_date' => '%s',

	)), $data_);
}

function pb_banner_main_add($data_){
	
	$insert_data_ = _pb_parse_banner_main_fields($data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbbanner_main}", $insert_value_, $insert_format_);
	if(!$result_) return $result_;

	$banner_id_ = $wpdb->insert_id;

	do_action('pb_banner_main_added', $banner_id_);

	return $banner_id_;
}

function pb_banner_main_update($banner_id_, $update_data_){
	$update_data_ = _pb_parse_banner_main_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];

	$update_value_['mod_date']= current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbbanner_main}", $update_value_, array("ID" => $banner_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_banner_main_updated', $banner_id_);

	return $result_;
}

function pb_banner_main_remove($banner_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbbanner_main}", array("ID" => $banner_id_), array("%d"));

	if(!$result_) return $result_;
	do_action('pb_banner_main_removed', $banner_id_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner-main/class.PB-banner-main-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner-main/class.PB-banner-main-adminpage.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner-main/class.PB-banner-main-ajax.php');
?>