<?php

//메인배너 업데이트
pb_add_ajax('banner-main-update', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	
	$banner_data_ = isset($_POST['banner_data']) ? $_POST['banner_data'] : null;
	$banner_data_['hide_yn'] = isset($banner_data_['hide_yn']) ? $banner_data_['hide_yn'] : "N";
	$banner_data_['link_target_yn'] = isset($banner_data_['link_target_yn']) ? $banner_data_['link_target_yn'] : "N";
	
	if(empty($_POST['banner_data'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}

	$banner_main_id_ = null;
	
	if(!strlen($banner_data_['ID'])){ // new
		$banner_main_id_ = pb_banner_main_add($banner_data_);

		echo json_encode(array(
			'success' => true,
			'banner_main_id' => $banner_main_id_,
			'success_title' => '배너추가완료',
			'success_message' => '배너추가가 완료되었습니다.',
			'redirect_url' => pb_adminpage_url('banner-main-manage/edit/'.$banner_main_id_),
		));
		die();
	}else{	
        //수정
		pb_banner_main_update($banner_data_['ID'], $banner_data_);

		echo json_encode(array(
			'success' => true,
			'banner_main_id' => $banner_data_['ID'],
			'success_title' => '팝업수정완료',
			'success_message' => '팝업수정이 완료되었습니다.',
			'redirect_url' => pb_adminpage_url('banner-main-manage/edit/'.$banner_data_['ID']),
		));
		die();
	}	
});

pb_add_ajax('banner-main-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$banner_main_id_ = isset($_POST['banner_main_id']) ? $_POST['banner_main_id'] : null;

	if(!strlen($banner_main_id_)){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된요청',
			'error_message' => '필수값이 누락되었습니다.',
		));
		die();	
	}

	pb_banner_main_remove($banner_main_id_);

	echo json_encode(array(
		'success' => true,
		'redirect_url' => pb_adminpage_url('banner-main-manage'),
	));
	die();	
});

pb_add_ajax('banner-main-switch-hide-yn', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$banner_main_id_ = isset($_POST['banner_main_id']) ? $_POST['banner_main_id'] : null;
	$hide_yn_ = isset($_POST['hide_yn']) ? $_POST['hide_yn'] : "N";

	pb_banner_main_update($banner_main_id_, array("hide_yn" => $hide_yn_));

	echo json_encode(array(
		'success' => true,
	));
	die();	

});
?>