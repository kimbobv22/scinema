<?php

add_filter('pb_adminpage_list', function($results_){

	$results_['sms-reg-manage'] = array(
		'title' => 'SMS회원관리' ,
		'template-data' => array(

			'data' => array(
				'total-count' => function(){
					//$is_root_ = pb_cinema_current_is_head_office();
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					$gender_ = isset($_GET["gender"]) ? $_GET["gender"] : null;
					$birth_year_srt_ = isset($_GET["birth_year_srt"]) ? $_GET["birth_year_srt"] : null;
					$birth_year_end_ = isset($_GET["birth_year_end"]) ? $_GET["birth_year_end"] : date("Y");
					$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
					$phone_num_ = isset($_GET["phone_num"]) ? $_GET["phone_num"] : null;
					
					if(!pb_cinema_current_is_head_office()){
						$cinema_id_ = pb_current_cinema_id();
					}

					$total_count_ = pb_sms_reg_list(array(
						'keyword' => $keyword_,
						'gender' => $gender_,
						'birth_year_range' => array($birth_year_srt_, $birth_year_end_),
						'cinema_id' => $cinema_id_,
						'phone1_3' => $phone_num_,
						'just_count' => true,
					));

					return $total_count_;
				},
				'list' => function($offset_, $limit_){
					
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					$gender_ = isset($_GET["gender"]) ? $_GET["gender"] : null;
					$birth_year_srt_ = isset($_GET["birth_year_srt"]) ? $_GET["birth_year_srt"] : null;
					$birth_year_end_ = isset($_GET["birth_year_end"]) ? $_GET["birth_year_end"] : date("Y");
					$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
					$phone_num_ = isset($_GET["phone_num"]) ? $_GET["phone_num"] : null;

					if(!pb_cinema_current_is_head_office()){
						$cinema_id_ = pb_current_cinema_id();
					}

					$items_ = pb_sms_reg_list(array(
						'keyword' => $keyword_,
						'gender' => $gender_,
						'birth_year_range' => array($birth_year_srt_, $birth_year_end_),
						'cinema_id' => $cinema_id_,
						'phone1_3' => $phone_num_,
						'limit' => array($offset_, $limit_),
					));

					return $items_;
				},
				'single' => function($action_data_){
					$data_ = pb_sms_reg_data($action_data_);
					return $data_;
				},
				'empty' => function(){

					if(!pb_cinema_current_is_head_office()){
						$cinema_id_ = pb_current_cinema_id();
					}else{
						$cinema_id_ = null;
					}

					return array(
						'cinema_id' => $cinema_id_,
						'status' => '00001',
						'xticket_ref_code'=> null,
						'_xticket_ref_code' => null,
						'display_home_yn' => 'Y',
					);
				},

			),

			'options' => array(
				'list-options' => array(
					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 등록정보가 없습니다.',
						//'add' => '상영작품등록',
					),
					'custom-search-frame' => function($options_, $table_options_){

						$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
						$gender_ = isset($_GET["gender"]) ? $_GET["gender"] : null;
						$birth_year_srt_ = isset($_GET["birth_year_srt"]) ? $_GET["birth_year_srt"] : null;
						$birth_year_end_ = isset($_GET["birth_year_end"]) ? $_GET["birth_year_end"] : date("Y");
						$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
						$phone_num_ = isset($_GET["phone_num"]) ? $_GET["phone_num"] : null;

						if(!pb_cinema_current_is_head_office()){
							$cinema_id_ = pb_current_cinema_id();
						}

						$tmp_cinema_list_ = pb_cinema_list(array("orderby" => "order by cinema_name asc"));
						
						global $pb_adminpage_menu_id;
						?>

<div class="search-frame minus-margin-top">
	
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<label>&nbsp; &nbsp;&nbsp;</label><br/>
			<a href="<?=pb_adminpage_url($pb_adminpage_menu_id.'/add')?>" class="btn btn-default btn-sm">SMS회원추가</a>
			<label>&nbsp; &nbsp;&nbsp;</label>
			<a href="#" onclick="javascript:pb_adminpage_download_excel('<?=pb_adminpage_url('sms-reg-manage')?>','#pb-adminpage-templated-list-table-form');" class="btn btn-sm btn-default" id="pb-user-excel">엑셀다운로드</a>
			<div class="form-margin-xs visible-xs"></div>
		</div>
		<div class="col-xs-5 col-sm-3">
			<label>성별</label>
			<select class="form-control input-sm" name="gender">
				<option value="">전체</option>
				<option value="00001" <?php selected('00001', $gender_) ?> >남자</option>
				<option value="00003" <?php selected('00003', $gender_) ?> >여자</option>
			</select>
		</div>
		<div class="col-xs-7 col-sm-4">
			<label>출생년도</label>
			<div class='input-group date input-group-sm' id='screen-open-end-date'>
                <input type='text' class="form-control" name="birth_year_srt" value="<?=$birth_year_srt_?>" />
                <span class="input-group-addon">
                    ~
                </span>
                <input type='text' class="form-control" name="birth_year_end" value="<?=$birth_year_end_?>" />
            </div>
		</div>
		
	</div>
	<div class="form-margin-xs"></div>
	<div class="row">
		
		<?php if(pb_cinema_current_is_head_office()){ ?>
		<div class="col-xs-6 col-sm-3">
		
			<select class="form-control input-sm" name="cinema_id" id="pb-sms-reg-search-form-cinema-id">
				<option value="">-영화관 선택-</option>

				<?php foreach($tmp_cinema_list_ as $row_data_){ ?>
				<option value="<?=$row_data_->cinema_id?>" <?=selected($row_data_->cinema_id, $cinema_id_)?> ><?=$row_data_->cinema_name?></option>
				<?php } ?>

			</select>
			<div class="clearfix"></div>
			<?php ?>
		</div>
		<?php }else{ ?>
			
			<div class="col-xs-6 col-sm-3">
				<input type="hidden" name="cinema_id" value="<?=pb_current_cinema_id()?>">
			</div>
		<?php } ?>
		<div class="col-xs-8 col-sm-7">
			<div class="form-margin-xs visible-xs"></div>
			<input type="text" class="form-control input-sm" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
		</div>
		<div class="col-xs-4 col-sm-2">
			<div class="form-margin-xs visible-xs"></div>
			<button class="btn btn-default btn-sm btn-block" type="submit">검색</button>
		</div>
		
	</div>
	
</div>

<hr>

						<?php
					}
				),
				'edit-options' => array(
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => 'SMS회원등록확인',
						'add-confirm-message' => 'SMS회원을 등록하시겠습니까?',
						'add-success-title' => '등록완료',
						'add-success-message' => 'SMS회원이 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => 'SMS회원 등록 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => 'SMS회원정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => 'SMS회원정보 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '삭제확인',
						'remove-confirm-message' => '해당 SMS회원정보를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => 'SMS회원정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => 'SMS회원정보 삭제 중 에러가 발생하였습니다.',
					),
				),
				'id-format' => 'pb-sms-reg-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){
					$result_ = pb_sms_reg_update($target_data_['reg_id'], $target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', 'SMS회원 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['reg_id'];
				},
				'add' => function($target_data_){
					$result_ = pb_sms_reg_add($target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', 'SMS회원 추가 중 에러가 발생했습니다.');
					}


					return $result_;
				},
				'remove' => function($target_data_){
					pb_sms_reg_remove($target_data_['reg_id']);
					return true;
				},
			),
			'templates' => array(
				'list' => function(){

					$is_root_ = pb_cinema_current_is_head_office();

					return array(
						'seq' => array(
							'column-label' => '',
							'th-class' => "col-seq text-center ",
							'td-class' => "col-seq text-center ",
							'td-render-func' => function($item_, $column_name_){
								global $_temp_movie_screen_index;
								if(empty($_temp_movie_screen_index)){
									$_temp_movie_screen_index = 1;
								}

								echo $_temp_movie_screen_index;


								++$_temp_movie_screen_index;
							},
						),
						'cinema_name' => array(
							'column-label' => '영화관',
							'th-class' => "col-2 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
							'td-class' => "col-2 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
						),
						'customer_name' => array(
							'column-label' => '이름',
							'th-class' => "col-2 text-center",
							'td-class' => "col-2 text-center",
						),
						'phone_num' => array(
							'column-label' => '휴대전화번호',
							'th-class' => "col-2 text-center ",
							'td-class' => "col-2 text-center ",
							'td-render-func' => function($item_, $column_name_){
								?>

									<?=$item_->phone1_1?>-<?=$item_->phone1_2?>-<?=$item_->phone1_3?>

								<?php
							},
						),
						'gender_name' => array(
							'column-label' => '성별',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
							
						),
						'birth_year' => array(
							'column-label' => '출생년도',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
							
						),
						'reg_date_ymd' => array(
							'column-label' => '등록일',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
							
						),

						'button_area' => array(
							'column-label' => '',
							'th-class' => "col-2 text-center ",
							'td-class' => "col-2 text-center",
							'td-render-func' => function($item_, $column_name_){

								global $pb_adminpage_menu_id;
								?>
								<a href="<?=pb_adminpage_url($pb_adminpage_menu_id."/edit/".$item_->reg_id)?>" class="btn btn-sm btn-default">수정</a>
								<a href="<?=pb_adminpage_url("sms-reg-manage/edit/".$item_->reg_id)?>" class="btn btn-sm btn-default">수정11</a>
								<a href="javascript:_pb_sms_reg_manage_delete(<?=$item_->reg_id?>);" class="btn btn-sm btn-black">삭제</a>
								<?php
								
							},
						),
						
						
					);
				},
				'edit' => function(){
					
					$is_root_ = pb_cinema_current_is_head_office();

					$cinema_select_col_ = null;

					if($is_root_){

						$tmp_cinema_list_ = pb_cinema_list(array("orderby" => "order by cinema_name asc"));
						$cinema_list_ = array();

						foreach($tmp_cinema_list_ as $row_data_){
							$cinema_list_[$row_data_->cinema_id] = $row_data_->cinema_name;
						}

						$cinema_select_col_ = array(
							'column' => 'col-xs-12 col-sm-5',
							'column-label' => '상영영화관',
							'column-name' => 'cinema_id',
							'placeholder' => '-영화관 선택-',
							'validates' => array(
								'required' => "영화관을 선택하세요",
							),
							'type' => 'select',
							'options' => $cinema_list_,
						);
					}else{
						$cinema_select_col_ = array(
							'type' => 'hidden',
							'column-name' => 'cinema_id',
						);
					}

					return array(
						array(
							'type' => 'hidden',
							'column-name' => 'reg_id',
						),
						array(
							'type' => 'row',
							'columns' => array(
								$cinema_select_col_,
								array(
									'column' => 'col-xs-12 '.($is_root_ ? "col-sm-7" : "col-sm-12"),
									'column-label' => '이름',
									'column-name' => 'customer_name',
									'placeholder' => '이름 입력',
									'validates' => array(
										'required' => "이름을 입력하세요",
									),
									'type' => 'text',
								),
							),
						),
						
						array(
							'type' => 'division',
						),
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '성별',
									'column-name' => 'gender',
									'validates' => array(
										'required' => "성별을 입력하세요",
									),
									'type' => 'radio',
									'options' => array(
										'00001' => '남자',
										'00003' => '여자',
									)
								),
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '출생년도',
									'column-name' => 'birth_year',
									'placeholder' => '4자리로 입력',
									'validates' => array(
										'required' => "출생년도를 입력하세요",
									),
									'type' => 'text',
								),
							),

						),
						array(
							'type' => 'division',
						),
						array(
							'column-label' => '휴대전화',
							
							'type' => 'custom',
							'view' => function($attrs_,$options_,$data_){
								?>

								<label class="" >휴대전화번호</label>
									<div class="form-group" >
										<div class="col-sm-4 col-xs-4 col-sm-phone">
											<select class="form-control" name="phone1_1" data-error="앞자리를 선택하세요" required id="pb-sms-reg-form-phone1_1">
												<option value="">-앞자리-</option>
												<?= pb_gcode_make_options('Z0003', $data_['phone1_1']) ?>
											</select>
										</div>
										<div class="col-sm-4 col-xs-4 col-sm-phone">
											<input type="number" name="phone1_2" class="form-control" maxlength="4" data-error="중간자리를 입력하세요" required id="pb-sms-reg-form-phone1_2" value="<?=$data_['phone1_2']?>">
										</div>
										<div class="col-sm-4 col-xs-4">
											<input type="number" name="phone1_3" class="form-control" maxlength="4" data-error="마지막자리를 입력하세요" required id="pb-sms-reg-form-phone1_2" value="<?=$data_['phone1_3']?>">

										</div>
										<div class="col-xs-12">
											<div class="help-block with-errors"></div>
											<div class="clearfix"></div>
										</div>
										<div class="clearfix"></div>
									</div>

								<?php 


							}
						),

						
					);
				},
			),

		),
		'lvl' => 1,
		'sort_num' => 20,
	);

	return $results_;
});


?>