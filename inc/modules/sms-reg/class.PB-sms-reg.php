<?php


function pb_sms_reg_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
					
					 {$wpdb->pbsms_reg}.reg_id reg_id
					,{$wpdb->pbsms_reg}.customer_name customer_name
					,{$wpdb->pbsms_reg}.phone1_1 phone1_1
					,{$wpdb->pbsms_reg}.phone1_2 phone1_2
					,{$wpdb->pbsms_reg}.phone1_3 phone1_3

					,{$wpdb->pbsms_reg}.gender gender
					,".pb_query_gcode_dtl_name("Z0001", "{$wpdb->pbsms_reg}.gender")." gender_name

					,{$wpdb->pbsms_reg}.birth_year birth_year

					,{$wpdb->pbsms_reg}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_name cinema_name

					,{$wpdb->pbsms_reg}.user_id user_id
					
					,{$wpdb->pbsms_reg}.reg_date reg_date
					,DATE_FORMAT({$wpdb->pbsms_reg}.reg_date, '%Y.%m.%d') reg_date_ymd
				
		     FROM {$wpdb->pbsms_reg}
			
			LEFT OUTER JOIN {$wpdb->users}
			ON   {$wpdb->users}.ID = {$wpdb->pbsms_reg}.user_id

			LEFT OUTER JOIN {$wpdb->pbcinema}
			ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbsms_reg}.cinema_id

		      ";

	$query_ .= apply_filters('pb_sms_reg_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['reg_id']) && strlen($conditions_['reg_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['reg_id'], "{$wpdb->pbsms_reg}.reg_id")." ";
	}

	if(isset($conditions_['cinema_id']) && strlen($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbsms_reg}.cinema_id")." ";
	}
	if(isset($conditions_['status']) && strlen($conditions_['status'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['status'], "{$wpdb->pbsms_reg}.status")." ";
	}

	if(isset($conditions_['user_id']) && strlen($conditions_['user_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['user_id'], "{$wpdb->pbsms_reg}.user_id")." ";
	}

	if(isset($conditions_['user_login']) && strlen($conditions_['user_login'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['user_login'], "{$wpdb->users}.user_login")." ";
	}
	if(isset($conditions_['user_email']) && strlen($conditions_['user_email'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['user_email'], "{$wpdb->users}.user_email")." ";
	}


	if(isset($conditions_['phone1_1']) && strlen($conditions_['phone1_1'])){
		$query_ .= " AND {$wpdb->pbsms_reg}.phone1_1 = '".$conditions_['phone1_1']."' ";
	}
	if(isset($conditions_['phone1_2']) && strlen($conditions_['phone1_2'])){
		$query_ .= " AND {$wpdb->pbsms_reg}.phone1_2 = '".$conditions_['phone1_2']."' ";
	}
	if(isset($conditions_['phone1_3']) && strlen($conditions_['phone1_3'])){
		$query_ .= " AND {$wpdb->pbsms_reg}.phone1_3 = '".$conditions_['phone1_3']."' ";
	}

	if(isset($conditions_['gender']) && strlen($conditions_['gender'])){
		$query_ .= " AND {$wpdb->pbsms_reg}.gender = '".$conditions_['gender']."' ";
	}
	if(isset($conditions_['birth_year']) && strlen($conditions_['birth_year'])){
		$query_ .= " AND {$wpdb->pbsms_reg}.birth_year = '".$conditions_['birth_year']."' ";
	}
	if(isset($conditions_['birth_year_range'])){

		

		if(isset($conditions_['birth_year_range'][0]) && strlen($conditions_['birth_year_range'][0])){
			$srt_year_ = $conditions_['birth_year_range'][0];
			$query_ .= " AND {$wpdb->pbsms_reg}.birth_year >= ".$srt_year_." ";
		}

		if(isset($conditions_['birth_year_range'][1]) && strlen($conditions_['birth_year_range'][1])){
			$end_year_ = $conditions_['birth_year_range'][1];
			$query_ .= " AND {$wpdb->pbsms_reg}.birth_year <= ".$end_year_." ";
		}
	
		
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_sms_reg_list_keyword', array(
			"{$wpdb->users}.user_login",
			"{$wpdb->users}.user_email",
			"{$wpdb->pbsms_reg}.customer_name",
			"{$wpdb->pbsms_reg}.phone1_3",
			
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_sms_reg_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_sms_reg_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	return apply_filters('pb_sms_reg_list', $wpdb->get_results($query_));	
}

function pb_sms_reg_data($reg_id_){
	if(!strlen($reg_id_)) return null;
	$result_ = pb_sms_reg_list(array('reg_id' => $reg_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_sms_reg_fields($data_){
	return pb_format_mapping(apply_filters("pb_sms_reg_fields_map",array(

		'customer_name' => '%s',
		'phone1_1' => '%s',
		'phone1_2' => '%s',
		'phone1_3' => '%s',
		'cinema_id' => '%d',
		'user_id' => '%d',
		// 'status' => '%d',

		'birth_year' => '%s',
		'gender' => '%s',

	)), $data_);
}

function pb_sms_reg_add($reg_data_){
	
	$insert_data_ = _pb_parse_sms_reg_fields($reg_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time("mysql");
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbsms_reg}", $insert_value_, $insert_format_);
	if(!$result_) return $result_;

	$reg_id_ = $wpdb->insert_id;

	do_action('pb_sms_reg_added', $reg_id_);

	return $reg_id_;
}

function pb_sms_reg_update($reg_id_, $update_data_){
	$update_data_ = _pb_parse_sms_reg_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];


	$update_value_['mod_date'] = current_time("mysql");
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbsms_reg}", $update_value_, array("reg_id" => $reg_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_user_updated', $reg_id_);

	return $result_;
}

function pb_sms_reg_remove($reg_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbsms_reg}", array("reg_id" => $reg_id_), array("%d"));
	if(!$result_) return false;

	do_action('pb_sms_reg_removed', $reg_id_);
	
	return $result_;
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/sms-reg/class.PB-sms-reg-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/sms-reg/class.PB-sms-reg-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/sms-reg/class.PB-sms-reg-adminpage.php');

?>