<?php

pb_add_ajax('adminpage-sms-reg-delete', function(){
	if(!pb_is_admin()){
		echo json_encode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}


	$reg_id_ = $_POST['reg_id'];

	pb_sms_reg_remove($reg_id_);

	echo json_encode(array(
		'success' => true,
	));
	die();
});

?>