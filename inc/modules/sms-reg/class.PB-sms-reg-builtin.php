<?php

add_action('init', function(){
	global $wpdb;

	$wpdb->pbsms_reg = "{$wpdb->base_prefix}sms_reg";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}sms_reg` (
		`reg_id` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`cinema_id` BIGINT(20)  NOT NULL COMMENT '영화관ID',

		`customer_name` VARCHAR(50)  DEFAULT NULL COMMENT '고객명',
		
		`phone1_1` VARCHAR(10)  DEFAULT NULL COMMENT '휴대전화',
		`phone1_2` VARCHAR(10)  DEFAULT NULL COMMENT '',
		`phone1_3` VARCHAR(10)  DEFAULT NULL COMMENT '',

		`gender` VARCHAR(5)  DEFAULT NULL COMMENT '성별(Z0001)',
		`birth_year` VARCHAR(4)  DEFAULT NULL COMMENT '생년',
		-- `status` VARCHAR(5)  DEFAULT NULL COMMENT '등록상태',

		`user_id` BIGINT(20)  DEFAULT NULL COMMENT '사용자ID',
				
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`reg_id`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='문자수신등록';";


	return $args_;
},10, 3);


add_filter('pb_initial_gcode_list', function($gcode_list_){	
	return $gcode_list_;
});

?>