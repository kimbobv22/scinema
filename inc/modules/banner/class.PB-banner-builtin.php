<?php


add_action('init', function(){

	global $wpdb;

	$wpdb->pbbanner = "{$wpdb->base_prefix}banner";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}banner` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`cinema_id` BIGINT(20)  DEFAULT NULL COMMENT '사용상영관',
		
		`banner_title` varchar(200)  DEFAULT NULL COMMENT '배너명',
		`banner_image_url` varchar(500)  DEFAULT NULL COMMENT '배너 이미지',
		`link_url` varchar(500)  DEFAULT NULL COMMENT '배너 링크',
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='배너';";

	return $args_;
},10, 3);

?>