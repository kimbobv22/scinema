<?php

pb_add_ajax('banner-update', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$banner_data_ = $_POST['banner_data'];

	if(strlen($banner_data_['banner_id'])){ //update

		pb_banner_update($banner_data_['banner_id'], $banner_data_);

		echo json_encode(array(
			'success' => true,
			'success_title' => '수정성공',
			'success_message' => '배너수정에 성공했습니다.',
		));
		die();

	}else{ //add

		$result_ = pb_banner_add($banner_data_);

		if(is_wp_error($result_)){
			echo json_encode(array(
				'success' => false,
				'error_title' => '에러발생',
				'error_message' => '배너추가 중, 에러가 발생했습니다.',
			));
			die();
		}

		echo json_encode(array(
			'success' => true,
			'success_title' => '추가성공',
			'success_message' => '배너추가에 성공했습니다.',
		));
		die();
	}
});

pb_add_ajax('banner-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$banner_id_ = $_POST['banner_id'];

	pb_banner_remove($banner_id_);

	echo json_encode(array(
		'success' => true,
	));
	die();
});

pb_add_ajax('banner-bulk-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$banner_ids_ = $_POST['banner_ids'];

	foreach($banner_ids_ as $banner_id_){
		pb_banner_remove($banner_id_);	
	}

	echo json_encode(array(
		'success' => true,
	));
	die();
});

pb_add_ajax('adminpage-banner-bulk-upload', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	 $cinema_id_ = isset($_POST['cinema_id']) ? $_POST['cinema_id'] : null;
	 $banner_image_urls_ = isset($_POST['banner_image_urls']) ? $_POST['banner_image_urls'] : null;
	 $cinema_types_ = isset($_POST['cinema_types']) ? $_POST['cinema_types'] : null;

	if(gettype($cinema_types_) !== "array"){
		if(!strlen($cinema_types_)){
			$cinema_types_ = array();
		}else{
			$cinema_types_ = array($cinema_types_);
		}
	}

	 $ignore_data_ = array();

	if($cinema_id_ === "common"){

		$cinema_list_ = pb_cinema_list();

		foreach($cinema_list_ as $cinema_data_){

			if(array_search($cinema_data_->cinema_type, $cinema_types_) === false) continue;

			foreach($banner_image_urls_ as $banner_data_){

				switch_to_blog($cinema_data_->cinema_id);

				pb_banner_add(array(
					'cinema_id' => $cinema_data_->cinema_id,
					'banner_title' => "",
					'banner_image_url' => $banner_data_['url'],
					'link_url' => home_url("movie-timetable"),
				));

				switch_to_blog($cinema_data_->cinema_id);
				
			}
		}

	}else{
		// $cinema_data_ = pb_cinema($cinema_id_);
		foreach($banner_image_urls_ as $banner_data_){
			switch_to_blog($cinema_id_);

			pb_banner_add(array(
				'cinema_id' => $cinema_id_,
				'banner_title' => "",
				'banner_image_url' => $banner_data_['url'],
				'link_url' => home_url("movie-timetable"),
			));

			restore_current_blog();
		}
	}

	echo json_encode(array(
		'success' => true,
		'ignore_data' => $ignore_data_,
	));
	die();
});

//20190711 nhn 배너일괄등록 신규 기능 추가
pb_add_ajax('adminpage-banner-bulk-upload-new', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	 $cinema_id_arr_ = isset($_POST['cinema_id_arr']) ? $_POST['cinema_id_arr'] : null;
	 $banner_image_urls_ = isset($_POST['banner_image_urls']) ? $_POST['banner_image_urls'] : null;
	 $cinema_links_ = isset($_POST['cinema_links']) ? $_POST['cinema_links'] : null;

	 if(gettype($cinema_id_arr_) !== "array"){
		if(!strlen($cinema_id_arr_)){
			$cinema_id_arr_ = array();
		}else{
			$cinema_id_arr_ = array($cinema_id_arr_);
		}
	}
		$ignore_data_ = array();

		if(empty($cinema_links_)){ 
			$cinema_links_ = "movie-timetable";
		}
		$order_idx = time();

		foreach($cinema_id_arr_ as $cinema_id_){
			switch_to_blog($cinema_id_);
			$cinema_links_data_ = home_url($cinema_links_,'https');
			//$cinema_links_data_ = home_url($cinema_links_);
			foreach($banner_image_urls_ as $banner_data_){
				pb_banner_add(array(
					'cinema_id' => $cinema_id_,
					'banner_title' => $order_idx,
					'banner_image_url' => $banner_data_['url'],
					'link_url' => $cinema_links_data_,
				));		
			}
		}
	echo json_encode(array(
		'success' => true,
		'ignore_data' => $ignore_data_,
	));
	die();
});

//20190717 nhn - 배너 일괄 삭제 시 선택사용에 영화관 리스트 조회 기능
pb_add_ajax('adminpage-banner-bulk-cinema-list', function(){
	$banner_title_ = isset($_POST['banner_title']) ? $_POST['banner_title'] : null;
	$banner_image_url_ = isset($_POST['banner_image_url']) ? $_POST['banner_image_url'] : null;

	$list_data_ = pb_banner_list(array(
					'title' => $banner_title_,
					'image_url' => $banner_image_url_,
					'orderby' => 'ORDER BY cinema_name asc'
	));

	echo json_encode(array(
		'success' => true,
		'banner_cinema_list' => $list_data_,
	));
});

//20190718 nhn - 배너 일괄 삭제 (이미지,타이틀로 해당 배너 일괄 삭제)
pb_add_ajax('banner-bulk-all-delete', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	$banner_title_ = isset($_POST['banner_title']) ? $_POST['banner_title'] : null;
	$banner_image_url_ = isset($_POST['banner_image_url']) ? $_POST['banner_image_url'] : null;

	$a = pb_banner_remove_all(array(
		'banner_title' => $banner_title_,
		'banner_image_url' => $banner_image_url_,
	));		

	echo json_encode(array(
		'success' => true,
		'result' => $a,
	));
	die();
});
?>