<?php

//영화관별 배너 카운트
function pb_banner_mst_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				{$wpdb->pbcinema}.cinema_id cinema_id
				,{$wpdb->pbcinema}.cinema_name cinema_name
				,{$wpdb->pbbanner}.banner_cnt banner_cnt
				FROM {$wpdb->pbcinema}
					LEFT OUTER JOIN (
						SELECT  {$wpdb->pbbanner}.cinema_id cinema_id
								,COUNT(*) banner_cnt
						FROM {$wpdb->pbbanner}
						GROUP BY {$wpdb->pbbanner}.cinema_id
		    		 ) {$wpdb->pbbanner}
		    	 ON   {$wpdb->pbbanner}.cinema_id = {$wpdb->pbcinema}.cinema_id";

	$query_ .= apply_filters('pb_banner_mst_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_banner_mst_list_keyword', array(
			"{$wpdb->pbcinema}.cinema_name",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_banner_mst_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_banner_mst_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_banner_mst_list', $wpdb->get_results($query_));

	array_unshift($results_, (object)array(
		'cinema_id' => PB_CINEMA_HEAD_OFFICE_ID,
		'cinema_name' => "본사",
		'banner_cnt' => pb_banner_list(array("cinema_id" => PB_CINEMA_HEAD_OFFICE_ID, 'just_count' => true)),
	));
	
	return $results_;
}

function pb_banner_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
					{$wpdb->pbbanner}.ID ID
					,{$wpdb->pbbanner}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_name cinema_name
					,{$wpdb->pbbanner}.banner_title banner_title
					,{$wpdb->pbbanner}.banner_image_url banner_image_url
					,{$wpdb->pbbanner}.link_url link_url
					,{$wpdb->pbbanner}.reg_date reg_date
					,DATE_FORMAT({$wpdb->pbbanner}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi
					,{$wpdb->pbbanner}.mod_date mod_date
					,DATE_FORMAT({$wpdb->pbbanner}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi
				FROM {$wpdb->pbbanner}
					LEFT OUTER JOIN {$wpdb->pbcinema} 
					ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbbanner}.cinema_id";

	$query_ .= apply_filters('pb_banner_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbbanner}.ID")." ";
	}

	if(isset($conditions_['cinema_id'])){
		if(isset($conditions_['with_common']) && $conditions_['with_common'] === true){
			$query_ .= " AND (".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbbanner}.cinema_id")." OR {$wpdb->pbbanner}.cinema_id = '-1') ";
		}else{
			$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbbanner}.cinema_id")." ";	
		}
	}
	// 선택삭제 시 정보체크 위해 컬럼 추가 (배너명)
	if(isset($conditions_['title'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['title'], "{$wpdb->pbbanner}.banner_title")." ";
	}
	// 선택삭제 시 정보체크 위해 컬럼 추가 (배너이미지주소)
	if(isset($conditions_['image_url'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['image_url'], "{$wpdb->pbbanner}.banner_image_url")." ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_banner_list_keyword', array(
			"{$wpdb->pbbanner}.banner_title",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_banner_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_banner_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_banner_list', $wpdb->get_results($query_));

	if(isset($conditions_['shuffle']) && $conditions_['shuffle'] === true){
		shuffle($results_);
	}

	return $results_;
}

// 배너 일괄삭제 - 이미지,넘버별 그룹바
function pb_banner_grp_list($conditions_ = array())
{
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				{$wpdb->pbbanner}.banner_title banner_title
				,{$wpdb->pbbanner}.banner_image_url banner_image_url
				,count({$wpdb->pbbanner}.cinema_id) cinema_cnt 
				FROM {$wpdb->pbbanner} 
				Group by {$wpdb->pbbanner}.banner_title,{$wpdb->pbbanner}.banner_image_url
				having cinema_cnt > 1";

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_banner_grp_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY banner_title DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_banner_grp_list', $wpdb->get_results($query_));
	
	return $results_;
}


function pb_banner($banner_id_){
	if(!strlen($banner_id_)) return null;
	$result_ = pb_banner_list(array('ID' => $banner_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_banner_fields($data_){
	return pb_format_mapping(apply_filters("pb_banner_fields_map",array(

		'cinema_id' => '%d',
		'banner_title' => '%s',
		'banner_image_url' => '%s',
		'link_url' => '%s',
	
	)), $data_);
}

function pb_banner_add($data_){
	
	$insert_data_ = _pb_parse_banner_fields($data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbbanner}", $insert_value_, $insert_format_);
	if(!$result_) return $result_;

	$banner_id_ = $wpdb->insert_id;

	do_action('pb_banner_added', $banner_id_);

	return $banner_id_;
}

function pb_banner_update($banner_id_, $update_data_){
	$update_data_ = _pb_parse_banner_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];

	$update_value_['mod_date']= current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbbanner}", $update_value_, array("ID" => $banner_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_banner_updated', $banner_id_);

	return $result_;
}

function pb_banner_remove($banner_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbbanner}", array("ID" => $banner_id_), array("%d"));

	if(!$result_) return $result_;
	do_action('pb_banner_removed', $banner_id_);
}

function pb_banner_remove_all($data_){
	$remove_data_ = _pb_parse_banner_fields($data_);

	$remove_value_ = $remove_data_['data'];
	$remove_format_ = $remove_data_['format'];

	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbbanner}", $remove_value_, $remove_format_);
	if(!$result_) return $result_;

	do_action('pb_banner_all_removed', $data_);

	return $result_;
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner/class.PB-banner-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner/class.PB-banner-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner/class.PB-banner-adminpage.php');
?>