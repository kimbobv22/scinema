<?php
add_filter('pb_adminpage_list', function($results_){

	if(pb_cinema_current_is_head_office()){
		$results_['banner-manage'] = array(
			'title' => '배너관리' ,
			'content-template' => "banner-manage/list",
			'subcontent-templates' => array(
				"edit" => "banner-manage/edit",
			),
			'lvl' => 2,
			'sort_num' => 50,
		);	
		// $results_['banner-bulk-write'] = array(
		// 	'title' => '배너일괄등록' ,
		// 	'content-template' => "banner-bulk-write",
		// 	'lvl' => 2,
		// 	'sort_num' => 51,
		// );
		$results_['banner-bulk-write-new'] = array(
			'title' => '배너일괄등록_New',
			'content-template' => "banner-bulk-write-new",
			'lvl' => 2,
			'sort_num' => 51,
		);
		$results_['banner-bulk-remove'] = array(
			'title' => '배너일괄삭제',
			'content-template' => "banner-bulk-remove",
			'lvl' => 2,
			'sort_num' => 52,
		);
	}else{
		$results_['banner-manage'] = array(
			'title' => '배너관리' ,
			'content-template' => "banner-manage/edit",
			'lvl' => 1,
			'sort_num' => 50,
		);
	}

	return $results_;
});
?>