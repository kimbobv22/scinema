<?php

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}gcode` (
		`CODE_ID` varchar(5) NOT NULL COMMENT '코드ID',
		`CODE_NM` varchar(50) DEFAULT NULL COMMENT '코드명',
		`CODE_DESC` varchar(100) DEFAULT NULL COMMENT '코드설명',
		`USE_YN` varchar(1) DEFAULT NULL COMMENT '사용여부',

		`col1` varchar(100) DEFAULT NULL COMMENT 'col1',
		`col2` varchar(100) DEFAULT NULL COMMENT 'col2',
		`col3` varchar(100) DEFAULT NULL COMMENT 'col3',
		`col4` varchar(100) DEFAULT NULL COMMENT 'col4',

		`REG_DATE` datetime DEFAULT null COMMENT '등록일자',
		`MOD_DATE` datetime DEFAULT null COMMENT '수정일자',
		PRIMARY KEY (`CODE_ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='공통코드';";

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}gcode_dtl` (
		`CODE_ID` varchar(5) NOT NULL COMMENT '코드ID',
		`CODE_DID` varchar(10) NOT NULL COMMENT '코드상세ID',
		`CODE_DNM` varchar(50) DEFAULT NULL COMMENT '코드상세명',
		`CODE_DDESC` varchar(100) DEFAULT NULL COMMENT '코드상세설명',

		`col1` varchar(100) DEFAULT NULL COMMENT 'col1',
		`col2` varchar(100) DEFAULT NULL COMMENT 'col2',
		`col3` varchar(100) DEFAULT NULL COMMENT 'col3',
		`col4` varchar(100) DEFAULT NULL COMMENT 'col4',

		`USE_YN` varchar(1) DEFAULT NULL COMMENT '사용여부',
		`SORT_CHAR` varchar(5) DEFAULT NULL COMMENT '정렬구분자',
		`REG_DATE` datetime DEFAULT NULL COMMENT '등록일자',
		`MOD_DATE` datetime DEFAULT null COMMENT '수정일자',
		PRIMARY KEY (`CODE_ID`,`CODE_DID`),
		CONSTRAINT `{$table_prefix_}gcode_dtl_fk1` FOREIGN KEY (`CODE_ID`) REFERENCES `{$table_prefix_}gcode` (`CODE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB {$table_charset_} COMMENT='공통코드 - 상세';";

	return $args_;
},10, 3);
add_action("pb_db_table_installed", "pb_gcode_install");

?>