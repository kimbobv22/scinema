<?php

function pb_gcode($code_id_){
	global $wpdb;
	$query_ = "SELECT code_id code_id,
					code_nm code_nm,
					code_desc code_desc,
					use_yn use_yn,
					reg_date reg_date,
					mod_date mod_date
	FROM {$wpdb->base_prefix}gcode
	WHERE 1=1
	AND   CODE_ID = %s";

	return $wpdb->get_row($wpdb->prepare($query_,$code_id_));
}

function pb_gcode_name($code_id_){
	$gcode_ = pb_gcode($code_id_);
	if(!isset($gcode_)) return $gcode_;
	return $gcode_->code_nm;
}

function pb_gcode_dtl_list($code_id_, $conditions_ = array()){
	global $wpdb;

	$conditions_ = array_merge(array(
		'only_use' => true,
		'code_did' => null,
	),$conditions_);
	
	$query_ = "SELECT code_did code_did,
					code_dnm code_dnm,
					code_ddesc code_ddesc,

					col1 col1,
					col2 col2,
					col3 col3,
					col4 col4,

					use_yn use_yn,
					sort_char sort_char,
					reg_date reg_date,
					mod_date mod_date,

					DATE_FORMAT(reg_date, '%Y%m%d%H%i%S') reg_date_ymdhis,
					DATE_FORMAT(mod_date, '%Y%m%d%H%i%S') mod_date_ymdhis
	FROM {$wpdb->base_prefix}gcode_dtl
	WHERE 1=1 ";

	if(strlen($conditions_['only_use']) && $conditions_['only_use'] === true){
		$query_ .= " AND {$wpdb->base_prefix}gcode_dtl.USE_YN = 'Y' ";
	}
	if(strlen($conditions_['code_did']) && $conditions_['code_did'] === true){
		$query_ .= " AND {$wpdb->base_prefix}gcode_dtl.code_did = '".$conditions_['code_did']."' ";
	}
	
	$query_ .= " AND   code_id = '".$code_id_."' ";
	$query_ .= " ORDER BY sort_char ASC";

	return $wpdb->get_results($query_);
}

function pb_gcode_dtl($code_id_, $code_did_){
	$gcode_ = pb_gcode_dtl_list($code_id_, array("code_did" => $code_did_));
	if(!isset($gcode_) || empty($gcode_)) return null;	
	return $gcode_[0];
}

function pb_gcode_dtl_name($code_id_, $code_did_){
	$gcode_dtl_ = pb_gcode_dtl($code_id_,$code_did_);
	if(!isset($gcode_dtl_)) return null;
	return $gcode_dtl_->code_dnm;
}

function pb_gcode_make_options($code_id_, $default_ = null, $conditions_ = array()){

	$dtl_list_ = pb_gcode_dtl_list($code_id_, $conditions_);

	ob_start();

	foreach($dtl_list_ as $row_index_ => $row_data_){ ?>
		<option value="<?=$row_data_->code_did?>" <?=($default_ === $row_data_->code_did ? "selected" : "")?>
			data-col1="<?= $row_data_->col1 ?>" data-col2="<?= $row_data_->col2 ?>"
			 data-col3="<?= $row_data_->col3 ?>" data-col4="<?= $row_data_->col4 ?>"

		 ><?=$row_data_->code_dnm?></option>
	<?php }

	return ob_get_clean();
}
function pb_query_gcode_dtl_name($code_id_, $column_){
	global $wpdb;
	return "(SELECT {$wpdb->base_prefix}gcode_dtl.CODE_DNM FROM {$wpdb->base_prefix}gcode_dtl WHERE {$wpdb->base_prefix}gcode_dtl.CODE_ID = '$code_id_' AND {$wpdb->base_prefix}gcode_dtl.CODE_DID = {$column_})";
}

function pb_gcode_install(){
	$gcode_list_ = apply_filters("pb_initial_gcode_list", array());

	global $wpdb;

	foreach($gcode_list_ as $code_id_ => $data_){

		$check_exists_gcode_ = pb_gcode($code_id_);
		if(isset($check_exists_gcode_) && !empty($check_exists_gcode_)) continue; //if gcode exists, pass though it

		$gcode_dtl_list_ = $data_['data'];

		$wpdb->insert("{$wpdb->base_prefix}gcode", array(
			'code_id' => $code_id_,
			'code_nm' => $data_['name'],
			'use_yn' => ((isset($data_['use_yn']) ? $data_['use_yn'] : true) ? "Y" : "N"),
			'reg_date' => current_time("mysql"),
		));

		$sort_char_ = 0;
		foreach($gcode_dtl_list_ as $code_did_ => $dtl_data_){

			++$sort_char_;

			$code_dnm_ = null;
			$duse_yn_ = "Y";

			$col1_ = null;
			$col2_ = null;
			$col3_ = null;
			$col4_ = null;

			if(gettype($dtl_data_) === "string"){
				$code_dnm_ = $dtl_data_;
			}else{
				$code_dnm_ = $dtl_data_['name'];
				$col1_ = isset($dtl_data_['col1']) ? $dtl_data_['col1'] : null;
				$col2_ = isset($dtl_data_['col2']) ? $dtl_data_['col2'] : null;
				$col3_ = isset($dtl_data_['col3']) ? $dtl_data_['col3'] : null;
				$col4_ = isset($dtl_data_['col4']) ? $dtl_data_['col4'] : null;

				$duse_yn_ = ((isset($dtl_data_['use_yn']) ? $dtl_data_['use_yn'] : true) ? "Y" : "N");
			}

			$wpdb->insert("{$wpdb->base_prefix}gcode_dtl", array(
				'code_id' => $code_id_,
				'code_did' => $code_did_,
				'code_dnm' => $code_dnm_,
				'use_yn' => $duse_yn_,
				'col1' => $col1_,
				'col2' => $col2_,
				'col3' => $col3_,
				'col4' => $col4_,
				'sort_char' => $sort_char_,
				'reg_date' => current_time("mysql"),
			));
		}

	}
}

define('PB_SEX_MEN', '00001');
define('PB_SEX_WOMEN', '00003');
define('PB_SEX_ETC', '00009');

add_filter('pb_initial_gcode_list', function($gcode_list_){

	$gcode_list_['Z0001'] = array(
		'name' => '성별',
		'data' => array(
			'00001' => '남자',
			'00003' => '여자',
		),
	);
	
	$gcode_list_['Z0003'] = array(
		'name' => '휴대폰앞자리',
		'data' => array(
			'010' => '010',
			'011' => '011',
			'016' => '016',
			'017' => '017',
			'019' => '019',
		),
	);

	return $gcode_list_;
});

include_once(PB_THEME_LIB_DIR_PATH . 'modules/gcode/class.PB-gcode-builtin.php');

?>