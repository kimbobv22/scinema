<?php

if(!class_exists('WP_List_Table')){
	require_once(ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}

class PBWPTable extends WP_List_Table{

	static function _remove_wp_http_referer(){
		if(!empty($_GET['_wp_http_referer'])){
			wp_redirect(remove_query_arg(array("_wp_http_referer","_wpnonce"), stripslashes($_SERVER["REQUEST_URI"])));
			exit();	
		}
	}

	function paged(){
		$paged_ = isset($_GET["paged"]) ? $_GET["paged"] : 1;
		if(!is_numeric($paged_) || $paged_ <= 0)
			$paged_ = 1;

		return $paged_;
	}
	function total_page_count($per_page_, $total_count_){
		return ceil($total_count_ / $per_page_);
	}
	function offset($paged_, $per_page_){
		return ($paged_ - 1) * $per_page_;
	}
	function _set_items($items_){
		$this->items = $items_;
	}
}

add_action("init", array("PBWPTable", "_remove_wp_http_referer"));

?>