<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();

	$current_cinema_data_ = pb_cinema($blog_id_);
	$results_['press-manage'] = array(
		'title' => '언론보도관리' ,
		'template-data' => array(

			'data' => array(
				'total-count' => function(){
					$is_root_ = pb_cinema_current_is_head_office();
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

					return pb_press_list(array(
						'keyword' => $keyword_,
						'cinema_id' => ($is_root_ ? null : get_current_blog_id()),
						'just_count' => true,
					));
				},
				'list' => function($offset_, $limit_){
					$is_root_ = pb_cinema_current_is_head_office();
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

					return pb_press_list(array(
						'keyword' => $keyword_,
						'cinema_id' => ($is_root_ ? null : get_current_blog_id()),
						'limit' => array($offset_, $limit_),
						'orderby' => ' ORDER BY reg_date DESC '
					));
				},
				'single' => function($action_data_){
					$pb_press_data_ = pb_press_data($action_data_);
					$pb_press_data_->press_title = htmlentities(stripcslashes($pb_press_data_->press_title));
					return $pb_press_data_;
				},
				'empty' => function(){
					$is_root_ = pb_cinema_current_is_head_office();
					return array(
						'cinema_id' => ($is_root_ ? -1 : get_current_blog_id()),
						'status' => '00001',
					);
				},

			),

			'options' => array(
				'list-options' => array(
					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 언론보도가 없습니다.',
						'add' => '언론보도등록',
					),
				),
				'edit-options' => array(
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => '언론보도추가확인',
						'add-confirm-message' => '해당 언론보도를 추가하시겠습니까?',
						'add-success-title' => '추가완료',
						'add-success-message' => '언론보도가 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => '언론보도 추가 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => '언론보도정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => '언론보도 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '언론보도삭제확인',
						'remove-confirm-message' => '해당 언론보도를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => '언론보도정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => '언론보도 삭제 중 에러가 발생하였습니다.',
					),
				),
				'id-format' => 'pb-movie-screen-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){
					$result_ = pb_press_data_update($target_data_['ID'], $target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '언론보도 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['ID'];
				},
				'add' => function($target_data_){
					$result_ = pb_press_data_add($target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '언론보도 추가 중 에러가 발생했습니다.');
					}


					return $result_;
				},
				'remove' => function($target_data_){
					pb_press_data_remove($target_data_['ID']);
					return true;
				},
			),
			'templates' => array(
				'list' => function(){

					$is_root_ = pb_cinema_current_is_head_office();

					return array(
						'cinema_name' => array(
							'column-label' => '영화관',
							'th-class' => "col-3 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
							'td-class' => "col-3 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
							'td-render-func' => function($item_, $column_name_){
								
								if($item_->cinema_id == PB_CINEMA_HEAD_OFFICE_ID){ ?>
									본사
								<?php }else if(strlen($item_->cinema_id === "-1")){ ?>
									공통
								<?php }else{ ?>
									<?=$item_->cinema_name?>
								<?php }
							},
						),
						'movie_name' => array(
							'column-label' => '언론보도명',
							'th-class' => "col-7",
							'td-class' => "col-7",
							'td-render-func' => function($item_, $column_name_){
								?>

								<a href="<?=pb_adminpage_url("press-manage/edit/".$item_->ID)?>"><?=htmlentities(stripcslashes($item_->press_title))?></a>

								<?php
							},
						),
						'publisher_name' => array(
							'column-label' => '작성일자',
							'th-class' => "col-2 text-center",
							'td-class' => "col-2 text-center",
							
						),
						
					);
				},
				'edit' => function(){

					$is_root_ = pb_cinema_current_is_head_office();

					$cinema_select_col_ = null;

					if($is_root_){

						$tmp_cinema_list_ = pb_cinema_list(array("include_head_office" => true));
						$cinema_list_ = array("-1" => "공통");

						foreach($tmp_cinema_list_ as $row_data_){
							$cinema_list_[$row_data_->cinema_id] = $row_data_->cinema_name;
						}

						$cinema_select_col_ = array(
							'column' => 'col-xs-12 col-sm-5',
							'column-label' => '영화관',
							'column-name' => 'cinema_id',
							'placeholder' => '-영화관 선택-',
							'validates' => array(
								'required' => "영화관을 선택하세요",
							),
							'type' => 'select',
							'options' => $cinema_list_,
						);
					}else{
						$cinema_select_col_ = array(
							'type' => 'hidden',
							'column-name' => 'cinema_id',
						);
					}

					return array(
						array(
							'type' => 'hidden',
							'column-name' => 'ID',
						),
					
					
						array(
							'type' => 'row',
							'columns' => array(
								$cinema_select_col_,
								array(
									'column' => 'col-xs-12 '.($is_root_ ? "col-sm-7" : "col-sm-12"),
									'column-label' => '보도제목',
									'column-name' => 'press_title',
									'placeholder' => '제목 입력',
									'validates' => array(
										'required' => "제목을 입력하세요",
									),
									'type' => 'text',
								),
							),
						),
						array(
							'type' => 'division',
						),

						array(
							'column-label' => '대표이미지',
							'column-name' => 'feature_image_url',
							'type' => 'imagepicker',
							'maxlength' => 1,
							'data-type' => 'string',
						),
						
						array(
							'type' => 'division',
						),
						array(
							'column-label' => '보도내용',
							'column-name' => 'press_html',
							'placeholder' => '내용 입력',
							'validates' => array(
								'required' => "언론보도상태를 선택하세요",
							),
							'type' => 'wp_editor',
						),
						array(
							'type' => 'division',
						),
						array(
							'column-label' => '첨부파일',
							'column-name' => 'attachments',
							'type' => 'filepicker',
							'maxlength' => 5,
							'data-type' => 'json',
						),
					);
				}
			),

		),
		'lvl' => 2,
		'sort_num' => 22,
	);

	return $results_;
});


?>