<?php


add_action('init', function(){

	global $wpdb;

	$wpdb->pbpress = "{$wpdb->base_prefix}press";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}press` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`cinema_id` BIGINT(20)  DEFAULT NULL COMMENT '사용상영관',
		
		`press_title` varchar(200)  DEFAULT NULL COMMENT '보도제목',
		`press_html` longtext COMMENT '보도내용',

		`attachments` longtext COMMENT '첨부파일',
		`feature_image_url` varchar(500) COMMENT '대표이미지',
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='언론보도';";

	return $args_;
},10, 3);

?>