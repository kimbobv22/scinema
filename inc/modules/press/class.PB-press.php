<?php

//내역 가져오기
function pb_press_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
				 {$wpdb->pbpress}.ID ID
				,{$wpdb->pbpress}.cinema_id cinema_id
				,{$wpdb->pbcinema}.cinema_name cinema_name

				,{$wpdb->pbpress}.press_title press_title
				,{$wpdb->pbpress}.press_html press_html
				
				,{$wpdb->pbpress}.feature_image_url feature_image_url
				,{$wpdb->pbpress}.attachments attachments
				
				,{$wpdb->pbpress}.reg_date reg_date
				,DATE_FORMAT({$wpdb->pbpress}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi
				,DATE_FORMAT({$wpdb->pbpress}.reg_date, '%Y.%m.%d') reg_date_ymd

				,{$wpdb->pbpress}.mod_date mod_date
				,DATE_FORMAT({$wpdb->pbpress}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi
				,DATE_FORMAT({$wpdb->pbpress}.mod_date, '%Y.%m.%d') mod_date_ymd

				".apply_filters("pb_press_list_fields", "", $conditions_)."

				
		     FROM {$wpdb->pbpress}

		     LEFT OUTER JOIN {$wpdb->pbcinema}
		     ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbpress}.cinema_id

		      ";

	$query_ .= apply_filters('pb_press_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbpress}.ID")." ";
	}

	if(isset($conditions_['cinema_id'])){
		if(isset($conditions_['with_common']) && $conditions_['with_common'] === true){
			$query_ .= " AND ( 
				".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbpress}.cinema_id")." OR 
				{$wpdb->pbpress}.cinema_id = '-1'
				 ) ";
		}else{
			$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbpress}.cinema_id")." ";
		}
	}
	if(isset($conditions_['for_common']) && $conditions_['for_common'] === true){
		$query_ .= " AND {$wpdb->pbpress}.cinema_id = '-1' ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_press_list_keyword', array(
			"{$wpdb->pbpress}.press_title",
			"{$wpdb->pbcinema}.cinema_name",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_press_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_press_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}
	
	return apply_filters('pb_press_list', $wpdb->get_results($query_));	
}

// 단인내역 가져오기
function pb_press_data($press_id_){
	if(!strlen($press_id_)) return null;
	$result_ = pb_press_list(array("ID" => $press_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_press_data_fields($data_){
	return pb_format_mapping(apply_filters("pb_press_data_fields_map",array(
	
		'cinema_id' => '%d',
		'press_title' => '%s',
		'press_html' => '%s',
		'attachments' => '%s',
		'feature_image_url' => '%s',

	)), $data_);
}

// 추가
function pb_press_data_add($press_data_){
	$insert_data_ = _pb_parse_press_data_fields($press_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbpress}", $insert_value_, $insert_format_);

	if(!$result_) return $result_;

	$press_id_ = $wpdb->insert_id;

	do_action('pb_press_added', $press_id_);

	return $press_id_;
}

//수정
function pb_press_data_update($press_id_, $press_data_){
	$press_data_ = _pb_parse_press_data_fields($press_data_);

	$update_value_ = $press_data_['data'];
	$update_format_ = $press_data_['format'];

	$update_value_['mod_date'] = current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbpress}", $update_value_, array("ID" => $press_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_press_updated', $press_id_);

	return $result_;
}

// 삭제
function pb_press_data_remove($press_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbpress}", array("ID" => $press_id_), array("%d"));

	if(!$result_) return $result_;

	do_action('pb_press_removed', $press_id_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/press/class.PB-press-adminpage.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/press/class.PB-press-builtin.php');

?>