<?php

function _pb_add_rewrite_rule(){
	do_action("pb_add_rewrite_rule");
	flush_rewrite_rules();
}
add_action('pb_activated', '_pb_add_rewrite_rule');
add_action('pb_after_update_settings', '_pb_add_rewrite_rule');
add_action('post_updated', '_pb_add_rewrite_rule');
add_action('wp_update_nav_menu', '_pb_add_rewrite_rule');

?>