<?php

add_filter("pbadmin_settings_form",function($view_list_, $settings_){

		ob_start();
?>
<h3>SMS설정</h3>
<table class="form-table">
	<tr>
		<th>API KEY</th>
		<td>
			<input type="text" name="sms_api_key" value="<?=$settings_["sms_api_key"]?>" class="large-text">
		</td>
	</tr>
	<tr>
		<th>API SECRET</th>
		<td>
			<input type="text" name="sms_api_secret" value="<?=$settings_["sms_api_secret"]?>" class="large-text">
		</td>
	</tr>
	<tr>
		<th>발신번호</th>
		<td>
			<input type="text" name="sms_send_number" value="<?=$settings_["sms_send_number"]?>" class="large-text">
		</td>
	</tr>
</table>
<p class="hint">*SMS API는 <a href="http://www.coolsms.co.kr/" target="_blank">CoolSMS</a>을 활용합니다. <a href="http://www.coolsms.co.kr/recharge" target="_blank">충전안내</a></p>

<?php
	$view_ = ob_get_clean();
	$view_list_[] = array(
		"id" => "sms-settings",
		"title" => "SMS관련",
		"view" => $view_,
	);

	return $view_list_;
}, 10, 2);

?>