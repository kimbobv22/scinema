<?php

function pb_crypt_hash($password_){
	return password_hash($password_, PASSWORD_DEFAULT); 
}
function pb_crypt_verify($password_, $hash_){
	return password_verify($password_, $hash_);
}

// RSA 개인키, 공개키 조합 생성
function pb_crypt_generate_keys($bits_ = 2048, $digest_algorithm_ = 'sha256', $password_ = null){
	if(!strlen($password_)){
		$pb_settings_ = PB_settings::get_settings();
		$password_ = $pb_settings_["crypt_password"];
	}
	
	$res_ = openssl_pkey_new(array(
			'digest_alg' => $digest_algorithm_,
			'private_key_bits' => $bits_,
			'private_key_type' => OPENSSL_KEYTYPE_RSA,
	));
	
	openssl_pkey_export($res_, $private_key_, $password_);
	
	$public_key_info_ = openssl_pkey_get_details($res_);

	return array(
			'private_key' => $private_key_,
			'public_key_info' => array(
				'key' => $public_key_info_['key'],
				'n' => base64_encode($public_key_info_["rsa"]["n"]),
				'e' => bin2hex($public_key_info_["rsa"]["e"]),
			),
	);
}

define("PB_CRYPT_SHARED_KEYS", "_pb_crypt_shared_keys_");

function pb_crypt_shared_keys(){
	$shared_key_ = pb_session_get(PB_CRYPT_SHARED_KEYS);
	if(!isset($shared_key_)){
		pb_session_put(PB_CRYPT_SHARED_KEYS, pb_crypt_generate_keys());
		$shared_key_ = pb_session_get(PB_CRYPT_SHARED_KEYS);
	}

	return $shared_key_;
}
	
// RSA 공개키를 사용하여 문자열을 암호화
function pb_crypt_encrypt($plaintext_, $public_key_ = null){
		if(!strlen($public_key_)){
			$keys_ = pb_crypt_shared_keys();
			$public_key_ = $keys_['public_key_info']['key'];
		}
		
		$pubkey_decoded_ = @openssl_pkey_get_public($public_key_);
		if($pubkey_decoded_ === false) return false;
		
		$ciphertext_ = false;
		$status_ = @openssl_public_encrypt($plaintext_, $ciphertext_, $pubkey_decoded_);
		if(!$status_ || $ciphertext_ === false) return false;
		
		return base64_encode($ciphertext_);
}

// RSA 개인키를 사용하여 문자열을 복호화
function pb_crypt_decrypt($ciphertext_, $private_key_ = null, $password_ = null){
	if(!strlen($private_key_)){
			$keys_ = pb_crypt_shared_keys();
			$private_key_ = $keys_['private_key'];
		}

	if(!strlen($password_)){
		$pb_settings_ = PB_settings::get_settings();
		$password_ = $pb_settings_["crypt_password"];
	}

	$ciphertext_ = @base64_decode($ciphertext_, true);
	if($ciphertext_ === false) return false;

	$privkey_decoded_ = @openssl_pkey_get_private($private_key_, $password_);
	if($privkey_decoded_ === false) return false;

	$plaintext_ = false;
	$status_ = @openssl_private_decrypt($ciphertext_, $plaintext_, $privkey_decoded_);

	if(!$status_ || $plaintext_ === false) return false;
	
	return $plaintext_;
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/lib/bcrypt/bcrypt.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-crypt-settings.php');

//라이브러리 등록
add_action("pb_library_initialize",function(){
	add_filter("pb_script_common_var", function($var_){
		$shared_crypt_keys_ = pb_crypt_shared_keys();

		return array_merge($var_, array(
			'crypt-public-key-n' => $shared_crypt_keys_['public_key_info']['n'],
			'crypt-public-key-e' => $shared_crypt_keys_['public_key_info']['e'],
		));
	});

});

//서버 복호화 필터추가
add_action("init", function(){
	$settings_ = PB_settings::get_settings();
	add_filter("pb_decrypt",function($params_, $add_){
		if(!isset($add_) || count($add_) <= 0) return $params_;

		foreach($add_ as $param_name_){
			$params_[$param_name_] = pb_crypt_decrypt($params_[$param_name_]);
		}

		return $params_;
	},10,2);
});

?>