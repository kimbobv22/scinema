<?php

define("PB_SLUG_ADMIN_SETTINGS","pb-admin-settings");

class PBAdmin_settings extends PBAction{
	
	static function _initialize(){
		wp_enqueue_media();
		pb_library_load("admin");
		add_action("admin_footer", array("PBAdmin_settings", "add_script"));
	}
	static function _draw_view(){

		

		$button_html_ = "<div class='pb-text-right'>";
		$button_html_ .= "<a class='button button-primary' href='javascript:_pb_save_settings();'>변경사항 저장</a>";
		$button_html_ .= "</div>";

		$settings_ = PB_settings::get_settings(true);

		$temp_settings_views_ = apply_filters('pbadmin_settings_form', array(), $settings_);

		$sub_settings_views_ = array();
		foreach($temp_settings_views_ as $settings_view_){
			if(isset($settings_view_['parent']) && strlen($settings_view_['parent'])){
				$view_html_ = isset($sub_settings_views_[$settings_view_['parent']]) ? $sub_settings_views_[$settings_view_['parent']] : "";
				$view_html_ .= $settings_view_['view'];
				$sub_settings_views_[$settings_view_['parent']] = $view_html_;
			}
		}


		$settings_views_ = array();
		foreach($temp_settings_views_ as $settings_view_){
			if(!isset($settings_view_['parent']) || !strlen($settings_view_['parent'])){
				$settings_view_['view'] .= isset($sub_settings_views_[$settings_view_['id']]) ? $sub_settings_views_[$settings_view_['id']] : "";
				$settings_views_[] = $settings_view_;
			}
		}


	?>

<h2><?=get_bloginfo('name')?>설정</h2>

<div class="wrap pb-limit-content form-wrap"><form id="pb-settings-form">
	<?= $button_html_; ?>
	<div id="pb-settings-tab" class='tab-container'>
		<ul class='etabs'>
			<li class='tab'><a href="#common-settings">기본설정</a></li>
			<?php foreach($settings_views_ as $settings_view_){ ?>
				<li class='tab'><a href="#<?=$settings_view_["id"]?>"><?=$settings_view_["title"]?></a></li>
			<?php } ?>
		</ul>
		<div class='panel-container'>
			<div id="common-settings">



<?php do_action("pb_admin_settings_basic_form", $settings_);?>

<a href="javascript:pb_admin_settingss_create_page();" class="button button-primary">필수 페이지 생성</a>
<a href="javascript:pb_admin_settingss_reinstall_db();" class="button button-primary">DB재설치</a>


			</div>
			<?php foreach($settings_views_ as $settings_view_){ ?>
			<div id="<?=$settings_view_["id"]?>">
				<?=$settings_view_["view"]?>
			</div>
			<?php } ?>
		</div>
	</div>

	<?= $button_html_; ?>
</form></div>
<style type="text/css">
#pb-settings-form .etabs { margin: 0; padding: 0; }
#pb-settings-form #pb-settings-tab{
	margin-top: 20px;
}
#pb-settings-form .tab { display: inline-block; zoom:1; *display:inline; background: #eee; border: solid 1px #A5A5A5; -moz-border-radius: 4px ; -webkit-border-radius: 4px ; }
#pb-settings-form .tab a { font-size: 14px; line-height: 2em; display: block; padding: 0 10px; outline: none; box-shadow: none !important;}
#pb-settings-form .tab a:hover { box-shadow: none !important;}
#pb-settings-form .tab.active { background: #fff; position: relative; border-color: #888; }
#pb-settings-form .tab a.active { font-weight: bold; }
#pb-settings-form .tab-container .panel-container { border: solid #888 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }
#pb-settings-form .panel-container { margin-bottom: 10px; }
</style>
<script type="text/javascript">
jQuery('#pb-settings-tab').easytabs({
	animate : false,
	updateHash : false
});
</script>

	<?php
	}

	static function add_script(){
	?>
	<script type="text/javascript">
	function _pb_save_settings(){
		$ = jQuery;

		var settings_form_ = $("#pb-settings-form");
		var settings_data_ = PB.apply_data_filters('pb_settings_update', settings_form_.serialize_object());

		PB.post({
			action : "pb_admin_save_settings",
			settings : settings_data_
		},function(result_, response_text_, cause_){
			if(!result_){
				PB.alert({
					title : "에러발생",
					content : cause_,
					button1 : "확인"
				});
				return;
			}

			PB.alert({
				title : "저장완료",
				content : '변경사항을 저장하였습니다.',
				button1 : "확인"
			},function(){
				document.location = document.location;
			});
		}, true, {
			url : PB.admin_ajaxurl()
		});
	}
	function pb_admin_settingss_create_page(){
		PB.alert({
			title : "페이지생성",
			content : "필수 패이지를 생성합니다. 진행하시겠습니까?",
			button1 : "생성하기",
			button2 : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			_pb_admin_settingss_create_page();
		});
	}

	function _pb_admin_settingss_create_page(){
		PB.post({
			action : "pb_admin_create_required_page"
		},function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				PB.alert({
					title : "에러발생",
					content : response_json_,
					button1 : "확인"
				});
				return;
			}

			PB.alert({
				title : "생성완료",
				content : '필수 페이지를 생성하였습니다.',
				button1 : "확인"
			},function(){
				document.location = document.location;
			});

		}, true, {
			url : PB.admin_ajaxurl()
		});
	}

	function pb_admin_settingss_reinstall_db(){
		PB.alert({
			title : "DB재설치확인",
			content : "DB를 재설치합니다. 진행하시겠습니까?",
			button1 : "재설치하기",
			button2 : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			_pb_admin_settingss_reinstall_db();
		});
	}

	function _pb_admin_settingss_reinstall_db(){
		PB.post({
			action : "pb_admin_reinstall_db"
		},function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				PB.alert({
					title : "에러발생",
					content : response_json_,
					button1 : "확인"
				});
				return;
			}

			PB.alert({
				title : "재설치완료",
				content : 'DB 재설치를 완료하였습니다.',
				button1 : "확인"
			},function(){
				document.location = document.location;
			});

		}, true);
	}

	</script>
	<?php
	}

	//세팅 저장하기
	static function put_settings($settings_){
		do_action("pb_before_update_settings",PB_settings::get_settings());
		
		update_site_option(PB_KEY_SETTINGS, array_merge(PB_settings::_get_default_settings(),apply_filters("pb_update_settings", $settings_)));

		global $pb_settings;
		$pb_settings = null;

		do_action("pb_after_update_settings",PB_settings::get_settings());
		return PB_settings::get_settings();
	}

	//AJAX세팅 저장하기
	static function _ajax_save_settings(){
		if(!pb_is_admin()){
			echo "false";
			die();
		}

		$settings_ = $_POST["settings"];

		self::put_settings($settings_);
		echo json_encode(array("success" => true));
		die();
	}

	//AJAX필수 페이지 생성
	static function _ajax_create_required_page(){
		if(!pb_is_admin()){
			echo "false";
			die();
		}

		$settings_ = PB_settings::get_settings();

		$pages_ = apply_filters("pb_create_required_page", array());
		foreach($pages_ as $page_){
			$post_data_ = array_merge(array(
				"slug" => null,
				"title" => null,
				"content" => "",
				"status" => "publish",
				"type" => "page",
				"setting_key" => null,
				"template" => null,
			),$page_);

			$before_post_ = get_posts(array(
				"name" => $post_data_["slug"],
				"post_type" => "page",
				"numberposts" => 1
			));

			if($before_post_){
				wp_delete_post($before_post_[0]->ID, true);
			}

			$page_id_ = wp_insert_post(array(
				"post_name" => $post_data_["slug"],
				"post_title" => $post_data_["title"],
				"post_content" => $post_data_["content"],
				"post_status" => $post_data_["status"],
				"post_type" => $post_data_["type"],
			));

			if(!is_wp_error($page_id_)){

				if(strlen($post_data_["setting_key"])){
					$settings_[$post_data_["setting_key"]] = $page_id_;	
				}

				if(strlen($post_data_["template"])){
					update_post_meta($page_id_, '_wp_page_template', $post_data_["template"]);
				}
				
			}
		}

		self::put_settings($settings_);

		echo json_encode(array(
			"success" => true,
		));
		die();
	}

	static function _ajax_reinstall_db(){
		if(!pb_is_admin()){
			echo "false";
			die();
		}

		_pb_install_db_table();

		echo json_encode(array(
			"success" => true,
		));
		die();
	}
}

PB::add_plugin_menu(array(
	"page" => PB_SLUG_ADMIN_SETTINGS,
	"title" => get_bloginfo('name')."설정",
	"class" => "PBAdmin_settings",
	"param" => array(),
	"eparam" => array(),
	'icon' => PB_THEME_DIR_URL."img/adminsymbol-settings.png",
	"permission" => "manage_options",
));

add_action("wp_ajax_pb_admin_save_settings", array("PBAdmin_settings", "_ajax_save_settings"));
add_action("wp_ajax_pb_admin_create_required_page", array("PBAdmin_settings", "_ajax_create_required_page"));
add_action("wp_ajax_pb_admin_reinstall_db", array("PBAdmin_settings", "_ajax_reinstall_db"));


add_filter('pb_create_required_page', function($results_){
	$header_menu_list_ = pb_header_menu_list();
	$temp_list_ = array();
	foreach($header_menu_list_ as $key => $data_){
		$temp_list_[] = array(
			"slug" => $data_['slug'],
			"title" => $data_['title'],
			"status" => "publish",
			"type" => "page",
			"setting_key" => $key_,
			"template" => $data_['template'],
		);
	}

	return array_merge($results_, $temp_list_);
});


// include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-admin-settings-agreement.php');

?>