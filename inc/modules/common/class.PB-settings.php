<?php

define("PB_KEY_SETTINGS", "pb-settings");

//설정
class PB_settings{

	static function _get_default_settings(){
		return apply_filters("pb_default_settings", array(
			
		));
	}

	//설정 가져오기
	static function get_settings($reload_ = false){
		global $pb_settings;

		if(isset($pb_settings) && count($pb_settings) > 0 && $reload_ !== true){
			return $pb_settings;
		}

		$pb_settings = get_site_option(PB_KEY_SETTINGS,array());
		if(gettype($pb_settings) !== "array")
			$pb_settings = array();

		return array_merge(self::_get_default_settings(),$pb_settings);
	}

	//전면페이지 가져오기
	static function front_page_id(){
		return get_option('page_on_front');
	}
}

function pb_settings($key_ = null){

	$settings_ = PB_settings::get_settings();;

	if(strlen($key_)){
		return (isset($settings_[$key_]) ? $settings_[$key_] : null);
	}

	return $settings_;
}

function pb_mainmenu_list(){

	global $pb_cached_mainmenu_list;

	if(isset($pb_cached_mainmenu_list)){
		return $pb_cached_mainmenu_list;
	}

	$locations_ = get_nav_menu_locations();

	// print_r(wp_get_nav_menus());
	// die();

	if(!isset($locations_[PB_MAINMENU]) || empty($locations_[PB_MAINMENU])){
		$registered_menu_list_ = wp_get_nav_menus();

		foreach($registered_menu_list_ as $menu_maybe_){
			if($menu_items_ = wp_get_nav_menu_items($menu_maybe_->term_id, array('update_post_term_cache' => false))){
				return wp_get_nav_menu_items($menu_maybe_->term_id, array());
				break;
			}
		}
	}

	if(!isset($locations_[PB_MAINMENU])) return array();

	$pb_cached_mainmenu_list = wp_get_nav_menu_items($locations_[PB_MAINMENU], array('update_post_term_cache' => false));

	// _wp_menu_item_classes_by_context($pb_cached_mainmenu_list);

	return $pb_cached_mainmenu_list;
}

define('PB_SIDEMENU_TYPE_SUB', '00001');
define('PB_SIDEMENU_TYPE_MAINMENU', '00003');


function pb_current_sidemenu_item(){
	$menu_list_ = pb_mainmenu_list();

	global $wp_query, $wp_rewrite;
	$current_page_id_ = (int) $wp_query->queried_object_id;
	$current_menu_id_ = (int)apply_filters("pb_current_sidemenu_id", -1);

	$find_row_ = null;

	foreach($menu_list_ as $row_data_){
		if($current_page_id_ <= 0 && $current_menu_id_ == $row_data_->ID){
			return $row_data_;		
		}
		if($current_page_id_ > 0 && $row_data_->object_id == $current_page_id_){
			$find_row_ = $row_data_;
			break;
		}

	}

	if(isset($find_row_)){

		while(true){
			
			$find_child_ = false;
			foreach($menu_list_ as $row_data_){
				if($row_data_->menu_item_parent == $find_row_->ID && $row_data_->object_id == $find_row_->object_id){
					$find_row_ = $row_data_;
					$find_child_ = true;
					break;
				}
			}

			if(!$find_child_) break;
		}
		return $find_row_;
	}

	return null;
}
function pb_current_sidemenu_id(){
	$current_menu_item_ = pb_current_sidemenu_item();

	if(isset($current_menu_item_)) return $current_menu_item_->ID;
	return null;
}

// pb_current_menu_object_id();

function pb_sidemenu_data($id_, $find_root_ = true){
	$menu_list_ = pb_mainmenu_list();

	$current_menu_item_ = null;

	foreach($menu_list_ as $menu_item_){
		if($menu_item_->ID == $id_){
			$current_menu_item_ = $menu_item_;
		}
	}

	$sidemenu_ = array();
	$has_sidemenu_ = false;

	if(!empty($current_menu_item_)){
		$has_sidemenu_ = $current_menu_item_->menu_item_parent > 0;

		foreach($menu_list_ as $menu_item_){
			if($menu_item_->menu_item_parent == $current_menu_item_->ID){
				$has_sidemenu_ = true;
				break;
			}
		}
	}

	if($has_sidemenu_){

		$root_data_ = null;
		$sidemenu_path_ = array($current_menu_item_);

		if($find_root_){
			$temp_root_id_ = ($current_menu_item_->menu_item_parent > 0 ? $current_menu_item_->menu_item_parent  : $current_menu_item_->ID);
			while($temp_root_id_ > 0){
				foreach($menu_list_ as $menu_item_){
					if($menu_item_->ID == $temp_root_id_){
						$root_data_ = $menu_item_;
						$sidemenu_path_[] = $root_data_;
						$temp_root_id_ = $menu_item_->menu_item_parent;
						break;
					}
				}
			}
		}else{

			$temp_root_id_ = $current_menu_item_->menu_item_parent;
			foreach($menu_list_ as $menu_item_){

				if($menu_item_->ID == $temp_root_id_){
					$root_data_ = $menu_item_;
					break;
				}
			}
		}

		$result_ = array(
			'title' => $root_data_->title,
			'type' => PB_SIDEMENU_TYPE_SUB,
			'sidemenu_path' => array_reverse($sidemenu_path_),
		);

		$sidemenu_ = array();
		$mainmenu_length_ = count($menu_list_);


		for($lvl1_index_= 0;$lvl1_index_ < $mainmenu_length_ ; ++ $lvl1_index_){
			$lvl1_item_ = $menu_list_[$lvl1_index_];
			
			if($lvl1_item_->menu_item_parent != $root_data_->ID) continue;

			$root_actived_ = (isset($lvl1_item_->ID) && $lvl1_item_->ID == $id_);

			$sidemenu_item_ = array(
				'menu_id' => $lvl1_item_->ID,
				'title' => $lvl1_item_->title,
				'url' => $lvl1_item_->url,
			);


			$children_ = array();

			for($lvl2_index_= 0;$lvl2_index_ < $mainmenu_length_ ; ++ $lvl2_index_){
				$lvl2_item_ = $menu_list_[$lvl2_index_];
				if($lvl2_item_->menu_item_parent != $lvl1_item_->ID) continue;
				$actived_ = (isset($lvl2_item_->ID) && $lvl2_item_->ID == $id_);

				if($actived_) $root_actived_ = true;

				$children_[] = array(
					'menu_id' => $lvl2_item_->ID,
					'title' => $lvl2_item_->title,
					'url' => $lvl2_item_->url,
					'active' => $actived_,
					'children' => array(),
				);				
			}

			$sidemenu_item_['active'] = $root_actived_;
			$sidemenu_item_['children'] = $children_;
			$sidemenu_[] = $sidemenu_item_;
		}

		$result_['menu_list'] = $sidemenu_;


	}else{

		$result_ = array(
			'title' => '메인메뉴',
			'type' => PB_SIDEMENU_TYPE_MAINMENU,
		);
		$sidemenu_ = array();
		foreach($menu_list_ as $menu_item_){

			if($menu_item_->menu_item_parent > 0) continue;

			$actived_ = (isset($menu_item_->ID) && $menu_item_->ID == $id_);

			$sidemenu_[] = array(
				'menu_id' => $menu_item_->ID,
				'title' => $menu_item_->title,
				'url' => $menu_item_->url,
				'active' => $actived_,
				'children' => array()
			);
		}

		$result_['menu_list'] = $sidemenu_;
	}

	return $result_;
}

header("Access-Control-Allow-Origin: *");


function pb_admin_permission(){
	return "manage_options";
}
function pb_is_admin($user_id_ = null){
	if(!is_user_logged_in()) return false;

	if(!strlen($user_id_)){
		$user_id_ = get_current_user_id();

	}
	return apply_filters('pb_is_admin', user_can($user_id_, pb_admin_permission()), $user_id_);
}

?>