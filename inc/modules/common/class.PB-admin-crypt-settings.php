<?php

class PBAdmin_crypt_settings{
	static function _add_settings_form($view_list_, $settings_){
		ob_start();
	?>	
	<h3>암호화설정</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="crypt_password">암/복 호화 암호</label></th>
				<td>
					<input type="text" name="crypt_password" class="large-text" placeholder="암호 입력" value="<?= $settings_["crypt_password"]; ?>">
				</td>
			</tr>
		</tbody>
	</table>
	<?php

		$view_ = ob_get_clean();

		$view_list_[] = array(
			"id" => "crypt-settings",
			"title" => "암호화관련",
			"view" => $view_,
		);
		return $view_list_;
	}	
}
add_filter("pbadmin_settings_form",array("PBAdmin_crypt_settings","_add_settings_form"), 10, 2);

?>