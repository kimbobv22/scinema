<?php

function pb_add_ajax($action_, $class_, $add_nopriv_ = false){
	add_action("pb_ajax_".$action_, $class_);
}

function pb_get_screen_option($key_){
	$user_id_ = get_current_user_id();
	$screen_ = get_current_screen();
	$option_ = $screen_->get_option($key_, "option");

	$value_ = get_user_meta($user_id_, $option_, true);
	if(!strlen($value_)){
		$value_ = $screen_->get_option($key_, "default");		
	}

	return $value_;
}

function pb_nl2br($value_){
	return str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br/>", $value_);
}

function pb_nvl($map_, $key_, $nvl_ = null){
	if(isset($map_[$key_]) && strlen($map_[$key_]))
		return $map_[$key_];
	else $nvl_;
}

define('PB_RANDOM_STRING_ALL', "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
define('PB_RANDOM_STRING_NUMLOWER', "0123456789abcdefghijklmnopqrstuvwxyz");
define('PB_RANDOM_STRING_NUMUPPER', "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
define('PB_RANDOM_STRING_NUM', "0123456789");
define('PB_RANDOM_STRING_ALPHABET', "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
define('PB_RANDOM_STRING_LOWER', "abcdefghijklmnopqrstuvwxyz");
define('PB_RANDOM_STRING_UPPER', "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

function pb_random_string($length_ = 20, $characters_ = PB_RANDOM_STRING_ALL){
	$characters_length_ = mb_strlen($characters_, 'UTF-8');
	$random_string_ = "";
	for($i_ = 0; $i_ < $length_; $i_++){
		$random_string_ .= mb_substr($characters_, rand(0, $characters_length_ - 1), 1, 'UTF-8');
	}
	return $random_string_;
}

function pb_append_url($base_url_, $path_){

	$check_path_ = strpos($path_, "/");
	if($check_path_ !== false && $check_path_ == 0){
		$path_ = substr($path_, 1);
	}

	if((strrpos($base_url_, "/") + 1) >= mb_strlen($base_url_)){
		$base_url_ .= $path_;
	}else{
		$base_url_ .= "/".$path_;
	}
	
	return $base_url_;
}
function pb_make_url($base_url_, $params_ = array()){
	$concat_char_ = "?";

	foreach($params_ as $key_ => $value_){
		if(strpos($base_url_, "?") > 0)
			$concat_char_ = "&";
		else $concat_char_ = "?";

		$base_url_ .= $concat_char_.$key_."=".urlencode($value_);			
	}

	return $base_url_;
}

function pb_is_https($url_ = null){
	if(!strlen($url_)){
		$url_ = pb_current_url();
	}

	return (strpos($url_, "https") === 0);
}
function pb_make_https($url_ = null){
	if(!strlen($url_)){
		$url_ = pb_current_url();
	}

	if(pb_is_https($url_)){
		return $url_;
	}

	return preg_replace('|^http://|', 'https://', $url_);
}

function pb_current_url(){
	$page_url_ = "http";
	if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] === "on")
		$page_url_ .= "s";

	$page_url_ .= "://";
	$page_url_ .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	
	return $page_url_;
}

function pb_pretty_number($num_, $max_ = -1){
	$over_bool_ = false;
	if(isset($max_) && $max_ > 0 && $max_ < $num_){
		$num_ = $max_;
		$over_bool_ = true;
	}
	
	$append_char_ = "";
	$num_ = (float)$num_;

	if($num_ >= 1000000){
		$num_ = round($num_ / 1000000.0,1);
		$append_char_ = "M";
	}else if($num_ >= 1000){
		$num_ = round($num_ / 1000.0,1);
		$append_char_ = "K";
	}

	if($num_ > 10){
		$num_ = number_format($num_);
	}

	return $num_.$append_char_.($over_bool_ ? "+" : "");
}

function pb_url2tag($str_){
	$rex_protocol = '(https?://)?';
	$rex_domain   = '((?:[-a-zA-Z0-9]{1,63}\.)+[-a-zA-Z0-9]{2,63}|(?:[0-9]{1,3}\.){3}[0-9]{1,3})';
	$rex_port	 = '(:[0-9]{1,5})?';
	$rex_path	 = '(/[!$-/0-9:;=@_\':;!a-zA-Z\x7f-\xff]*?)?';
	$rex_query	= '(\?[!$-/0-9:;=@_\':;!a-zA-Z\x7f-\xff]+?)?';
	$rex_fragment = '(#[!$-/0-9:;=@_\':;!a-zA-Z\x7f-\xff]+?)?';

	return preg_replace_callback("&\\b$rex_protocol$rex_domain$rex_port$rex_path$rex_query$rex_fragment(?=[?.!,;:\"]?(\s|$))&",
		function($match_){
			$complete_url_ = $match_[1] ? $match_[0] : "http://{$match_[0]}";
			return '<a href="' . $complete_url_ . '" target="_blank">'.$complete_url_.'</a>';
		}, htmlspecialchars($str_));
}

function pb_client_ip(){
	$ipaddress_ = false;
	
	if(isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress_ = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress_ = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress_ = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress_ = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress_ = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress_ = $_SERVER['REMOTE_ADDR'];

	if($ipaddress_ === "::1"){
		return "127.0.0.1";
	}

	return $ipaddress_;
}

function pb_client_countrycode(){

	$ip_ = pb_client_ip();

	$country_ = exec("whois $ip_  | grep -i country");
	
	$country_ = str_replace("country:", "", "$country_");
	$country_ = str_replace("Country:", "", "$country_");
	$country_ = str_replace("Country :", "", "$country_");
	$country_ = str_replace("country :", "", "$country_");
	$country_ = str_replace("network:country-code:", "", "$country_");
	$country_ = str_replace("network:Country-Code:", "", "$country_");
	$country_ = str_replace("Network:Country-Code:", "", "$country_");
	$country_ = str_replace("network:organization-", "", "$country_");
	$country_ = str_replace("network:organization-usa", "us", "$country_");
	$country_ = str_replace("network:country-code;i:us", "us", "$country_");
	$country_ = str_replace("eu#countryisreallysomewhereinafricanregion", "af", "$country_");
	$country_ = str_replace("", "", "$country_");
	$country_ = str_replace("countryunderunadministration", "", "$country_");
	$country_ = str_replace(" ", "", "$country_");

	return $country_;
}

function pb_map_string($string_, $map_){
	$result_ = $string_;
	foreach($map_ as $key_ => $value_){
		$result_ = str_replace("{".$key_."}",$value_,$result_);
	}

	return $result_;
}

function pb_colorhex_to_rgb($hex_){
	$hex_ = preg_replace("/[^0-9A-Fa-f]/", '', $hex_); // Gets a proper hex string

	$color_val_ = hexdec($hex_);
	$result_['r'] = 0xFF & ($color_val_ >> 0x10);
	$result_['g'] = 0xFF & ($color_val_ >> 0x8);
	$result_['b'] = 0xFF & $color_val_;

	return $result_;
}
function pb_colorhex_is_bright($hex_){
	return (pb_colorhex_contrast($hex_) > 130);
}

function pb_colorhex_contrast($hex_){
	$rgb_ = pb_colorhex_to_rgb($hex_);
	return sqrt(
		$rgb_['r'] * $rgb_['r'] * .241 +
		$rgb_['g'] * $rgb_['g'] * .691 +
		$rgb_['b'] * $rgb_['b'] * .068
	);
}

function pb_excel_output($file_name_, $data_, $columns_ = array(), $echo_ = true){
	header("Content-type: application/vnd.ms-excel" );
	header("Content-type: application/vnd.ms-excel; charset=upb-8");
	header("Content-Disposition: attachment; filename = ".$file_name_);
	header("Content-Description: Autism@Work Generated Data"); 

	if(count($columns_) <= 0){
		$sample_row_data_ = $data_[0];
		$columns_ = array();
		foreach($sample_row_data_ as $column_name_ => $column_value_){
			$columns_[] = $column_name_;
		}
	}
	
	ob_start();
?>
	<meta content="application/vnd.ms-excel; charset=UTF-8" name="Content-type">
	<table border=1>
		<?php
			if(count($columns_) > 0){
				?> <tr> <?php
				foreach($columns_ as $column_title_ => $column_name_){
					?> <td><?=$column_title_?></td> <?php
				}
				?> </tr> <?php
			}

			foreach($data_ as $row_data_){

				$row_data_ = (array)$row_data_;
			?> <tr> <?php
				foreach($columns_ as $column_title_ => $column_name_){
					?> <td><?=$row_data_[$column_name_]?></td> <?php
				}
			?> </tr> <?php }
		?>

	</table>

<?php 
	
	$excel_html_ = ob_get_clean();
	if($echo_){
		echo $excel_html_;
	}else $excel_html_;
}

function pb_is_ajax(){
	return ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX );
}

function pb_object_to_html($object_){
	return htmlentities(json_encode($object_, true));
}

function pb_upload_path(){
	$upload_dir_ = wp_upload_dir();
	$upload_path_ = $upload_dir_["path"];
	$upload_url_ = $upload_dir_["url"];

	if(!file_exists($upload_path_)){
		mkdir($upload_path_, 0777, true);
	}

	return array(
		"url" => $upload_url_."/",
		"path" => $upload_path_."/",
	);
}

//업로드파일경로 정보 가져오기
function pb_upload_filepath($filename_ = null, $extension_ = null){
	$upload_dir_ = pb_upload_path();
	$upload_path_ = $upload_dir_["path"];
	$upload_url_ = $upload_dir_["url"];

	$extension_ = (strlen($extension_) ? $extension_ : pathinfo($filename_, PATHINFO_EXTENSION));
	$filename_ = pathinfo($filename_, PATHINFO_FILENAME);
	$filename_ = (strlen($filename_) ? $filename_ : pb_random_string());

	$new_filename_ = wp_unique_filename($upload_path_, $filename_.(strlen($extension_) ? ".".$extension_ : ""));

	return array(
		'filename' => $filename_,
		"url" => $upload_url_.$new_filename_,
		"path" => $upload_path_.$new_filename_,
	);
}

//URL에서 사진 긁어오기
function pb_craw_image($url_, $extension_ = null){
	

	$upload_filepath_ = pb_upload_filepath(null, (strlen($extension_) ? $extension_ : pathinfo($url_, PATHINFO_EXTENSION)));

	// $fp_ = fopen($upload_filepath_["path"], "wb");
	// curl_setopt($ch_, CURLOPT_FILE, $fp_);
	// curl_setopt($ch_, CURLOPT_HEADER, 0);
	// curl_exec($ch_);
	// curl_close($ch_);
	// fclose($fp_);

	// $ch_ = curl_init($url_);
	// curl_setopt($ch_, CURLOPT_HEADER, 0);
	// curl_setopt($ch_, CURLOPT_RETURNTRANSFER, 1);
	// curl_setopt($ch_, CURLOPT_BINARYTRANSFER,1);
	// curl_setopt($ch_, CURLOPT_TIMEOUT, 1000);
 //	curl_setopt($ch_, CURLOPT_USERAGENT, 'Mozilla/5.0');
	// $raw_ = curl_exec($ch_);
	// curl_close($ch_);

	$raw_ = file_get_contents($url_);

	$fp_ = fopen($upload_filepath_["path"], "w");
	fwrite($fp_, $raw_);
	fclose($fp_);

	return $upload_filepath_;
}

function pb_fetch_url($url_){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url_);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	$result = curl_exec($ch);
	curl_close($ch); 
	return $result;
}

function pb_make_thumbnail($image_path_, $thumbnail_width = 150, $thumbnail_height = true){

	$upload_filepath_ = pb_upload_filepath(null, pathinfo($image_path_, PATHINFO_EXTENSION));

	$arr_image_details = getimagesize($image_path_); // pass id to thumb name
	$original_width = $arr_image_details[0];
	$original_height = $arr_image_details[1];

	$thumbnail_height = ($thumbnail_height === true ? (($thumbnail_width / $original_width) * $original_height) : $thumbnail_height);

	$new_width = $thumbnail_width;
	$new_height = (($original_height * $new_width / $original_width));

	if($thumbnail_height > $new_height){
		$new_height = $thumbnail_height;
		$new_width = (($new_height / $original_height) * $original_width);
	}

	if($arr_image_details[2] == IMAGETYPE_GIF){
		$imgt = "ImageGIF";
		$imgcreatefrom = "ImageCreateFromGIF";
	}
	if($arr_image_details[2] == IMAGETYPE_JPEG){
		$imgt = "ImageJPEG";
		$imgcreatefrom = "ImageCreateFromJPEG";
	}
	if($arr_image_details[2] == IMAGETYPE_PNG){
		$imgt = "ImagePNG";
		$imgcreatefrom = "ImageCreateFromPNG";
	}
	if($imgt) {
		$old_image = $imgcreatefrom($image_path_);
		$new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
		
		imagecopyresampled($new_image, $old_image, -($new_width / 2) + ($thumbnail_width / 2), -($new_height / 2) + ($thumbnail_height / 2), 0, 0, $new_width , $new_height , $original_width, $original_height);
		$imgt($new_image, $upload_filepath_["path"]);
	}

	return $upload_filepath_;
}

function pb_upload_file($file_){
	if(!function_exists( 'wp_handle_upload')){
		require_once(ABSPATH . 'wp-admin/includes/file.php');
	}

	$upload_overrides_ = array( 'test_form' => false );
	$movefile_ = wp_handle_upload($file_, $upload_overrides_ );

	if($movefile_ && !isset($movefile_['error'])){
		return $movefile_;
	}else{
		return false;
	}
}

function pb_query_latlng_distance($olat_, $olng_, $tlat_, $tlng_){
	return "(6371 * acos (
		cos ( radians($olat_) )
	* cos( radians( $tlat_ ) )
	* cos( radians( $tlng_ ) - radians($olng_) )
	+ sin ( radians($olat_) )
	* sin( radians( $tlat_ ) )
    ))";
}

function pb_generate_unique_id($length_ = 10){
	$capital_letters_ = range("A", "Z");
	$lowcase_letters_ = range("a", "z");
	$numbers_ = range(0, 9);

	$all_ = array_merge($capital_letters_, $lowcase_letters_, $numbers_);
	$count_ = count($all_);
	$id_	= "";

	for($index_ = 0; $index_ < $length_; ++$index_){
		$key_ = rand(0, $count_ - 1);
		$id_ .= $all_[$key_];
	}

	return $id_;
}

global $_pb_yoil;
$_pb_yoil = array("일","월","화","수","목","금","토");

function pb_yoil($yyyymmdd_){
	global $_pb_yoil;
	return $_pb_yoil[date('w', strtotime($yyyymmdd_))];
}

define("PB_QUERY_NULL","{{PB_NULL}}");
define("PB_QUERY_NOW","{{PB_NOW}}");
function PB_QUERY_FUNC($func_){
	return "{{PB_FUNC:".$func_."/PB_FUNC}}";
}

function _pb_filter_query_null_mapping($query_){
	$query_ = str_replace(("'".PB_QUERY_NULL."'"), "NULL", $query_);
	$query_ = str_replace(("'".PB_QUERY_NOW."'"), "NOW()", $query_);
	$query_ = str_replace(("'{{PB_FUNC:"), "", $query_);
	$query_ = str_replace(("/PB_FUNC}}'"), "", $query_);

	return $query_;
}
add_filter("query", "_pb_filter_query_null_mapping");

function pb_query_insert($table_name_, $data_, $data_format_ = null, $replace_ = false){
	global $wpdb;

	$values_ = array();
	$query_data_column_ = "";
	$query_data_format_ = "";
	$current_index_ = 0;
	foreach($data_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_data_column_ .= ",";
			$query_data_format_ .= ",";
		}

		$query_data_column_ .= $column_name_;
		$query_data_format_ .= isset($data_format_[$current_index_]) ? $data_format_[$current_index_] : "%s";
		$values_[] = $column_value_;

		++$current_index_;
	}

	$query_ = ($replace_ ? "REPLACE" : "INSERT")." INTO {$table_name_} ($query_data_column_) VALUES ($query_data_format_)";

	return $wpdb->prepare($query_, $values_);
}

function pb_query_update($table_name_, $data_, $where_, $data_format_ = null, $where_format_ = null){
	global $wpdb;

	$values_ = array();
	$query_data_column_ = "";
	$query_where_column_ = "";

	$current_index_ = 0;
	foreach($data_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_data_column_ .= ",";
		}

		$query_data_column_ .= $column_name_." = ".(isset($data_format_[$current_index_]) ? $data_format_[$current_index_] : "%s");
		$values_[] = $column_value_;

		++$current_index_;
	}

	$current_index_ = 0;
	foreach($where_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_where_column_ .= " AND ";
		}

		$query_where_column_ .= $column_name_." = ".(isset($where_format_[$current_index_]) ? $where_format_[$current_index_] : "%s");
		$values_[] = $column_value_;

		++$current_index_;
	}

	return $wpdb->prepare("UPDATE {$table_name_} SET $query_data_column_ WHERE $query_where_column_", $values_);
}

function pb_query_delete($table_name_, $where_, $where_format_ = null){
	global $wpdb;

	$values_ = array();
	$query_where_column_ = "";

	$current_index_ = 0;
	foreach($where_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_where_column_ .= " AND ";
		}

		$query_where_column_ .= $column_name_." = ".(isset($where_format_[$current_index_]) ? $where_format_[$current_index_] : "%s");
		$values_[] = $column_value_;

		++$current_index_;
	}

	return $wpdb->prepare("DELETE FROM {$table_name_} WHERE $query_where_column_", $values_);
}

function pb_query_in_fields($data_, $column_name_, $empty_replace_ = "1", $is_str_ = true, $trim_ = true){
	$query_ = "";
	$cover_char_ = ($is_str_ ? "'" : "");

	if(gettype($data_) !== "array"){

		if($trim_){
			$data_ = trim($data_);
		}

		if(strlen($data_) > 0){
			$data_ = array($data_);	
		}else $data_ = array();
	}

	if(count($data_) == 1){
		return " {$column_name_} = {$cover_char_}".$data_[0]."{$cover_char_} ";
	}

	for($index_=1;$index_ < count($data_); ++$index_){
		$query_ .= ", {$cover_char_}".$data_[$index_]."{$cover_char_} ";
	}

	if(count($data_) > 0){
		return " {$column_name_} IN ( {$cover_char_}".$data_[0]."{$cover_char_} ".$query_.") ";
	}else return " {$empty_replace_} ";
}
function pb_query_min_max_fields($data_, $column_name_, $empty_replace_ = "1"){
	$query_ = "";
	if(gettype($data_) === "array"){

		$min_ = isset($data_[0]) ? intval($data_[0]) : 0 ;
		$max_ = isset($data_[1]) ? intval($data_[1]) : 0 ;

		if($min_ > 0){
			$query_ .= "$column_name_ >= ".$min_;

			if($max_ > 0){
				$query_ = "(".$query_." AND $column_name_ <= ".$max_.")";
			}
		}else{
			if($max_ > 0){
				$query_ .= "$column_name_ <= ".$max_;
			}
		}
		
	}else if(strlen($data_)){
		$query_ .= "$column_name_ = ".$data_;
	}else{
		$query_ .= $empty_replace_;
	}

	return $query_;
}

function pb_query_keyword_search($fields_, $keyword_){
	$query_where_keyword_ = "";
	$keyword_first_ = true;
	foreach($fields_ as $field_){
		if(!$keyword_first_){
			$query_where_keyword_ .= " OR ";
		}

		$query_where_keyword_ .= " {$field_} LIKE '%".esc_sql($keyword_)."%'  ";
		$keyword_first_ = false;
	}

	return " ({$query_where_keyword_}) ";
}

function pb_time_unix2date($unixtime_, $format_ = "Y-m-d H:i:s"){
	return gmdate($format_, $unixtime_);
}

//관리자 - 수정페이지여부
function pb_is_edit_page(){
	global $pagenow;
	if(!is_admin()) return false;
	return in_array($pagenow, array('post.php'));
}
function pb_is_new_edit_page(){
	global $pagenow;
	if(!is_admin()) return false;
	return in_array($pagenow, array('post-new.php'));
}

function pb_json_decode($json, $assoc = TRUE){
	$json = str_replace(array("\n","\r"),"\\n",$json);
	$json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
	$json = preg_replace('/(,)\s*}$/','}',$json);
	return json_decode($json,$assoc);
}

function pb_admin_ajax_url($action_ = null){
	if(strlen($action_)){
		return pb_make_url(admin_url("admin-ajax.php"), array('action' => $action_));
	}else return admin_url("admin-ajax.php");
}

function pb_format_mapping($map_, $data_){
	$converted_data_ = array();
	$converted_format_ = array();
	foreach($data_ as $key_ => $value_){
		if(isset($map_[$key_])){
			$converted_data_[$key_] = $value_;
			$converted_format_[] = $map_[$key_];
		}
	}
	
	return array('data' => $converted_data_, 'format' => $converted_format_);
}

function pb_iff($a_, $b_, $result_ = ""){
	return ($a_ === $b_ ? $result_ : "");
}
function pb_checked_in($a_, $array_){
	return in_array($a_, $array_) ? "checked" : "";
}
function pb_selected_in($a_, $array_){
	return in_array($a_, $array_) ? "selected" : "";
}

function pb_active($a_, $b_){
	return pb_iff($a_, $b_, "active");
}

function pb_number_options($srt_, $end_, $unit_obj_ = 1, $default_ = null, $func_ = null){
	$results_ = array();

	$func_ = isset($func_) ? $func_ : function($value_){ return number_format($value_); };

	if(gettype($unit_obj_) === "array"){
		$break_points_ = $unit_obj_;
		// $results_[] = '<option value="'.$srt_.'" '.selected($srt_, $default_).' >'.number_format($srt_).'</option>';

		$last_break_point_ = 0;

		foreach($break_points_ as $break_point_ => $break_unit_){
			for($index_ = $srt_; $index_ < $break_point_; $index_ += $break_unit_){
				$results_[] = '<option value="'.$index_.'" '.selected($index_, $default_).' >'.$func_($index_).'</option>';
			}

			$srt_ = $break_point_;
			$last_break_point_ = $break_point_;
		}

		$results_[] = '<option value="'.$last_break_point_.'" '.selected($last_break_point_, $default_).' >'.$func_($last_break_point_).'</option>';	
	}else{
		for($index_=$srt_; $index_< $end_; $index_ += $unit_obj_){
			$results_[] = '<option value="'.$index_.'" '.selected($index_, $default_).' >'.$func_($index_).'</option>';
		}	

		$results_[] = '<option value="'.$end_.'" '.selected($end_, $default_).' >'.$func_($end_).'</option>';	
	}

	// $results_ = ($reverse_ ? array_reverse($results_) : $results_);

	foreach($results_ as $result_){
		echo $result_;
	}
}

function pb_currency_symbol($currency_code_, $locale_ = 'en_US'){
	$formatter_ = new \NumberFormatter($locale_ . '@currency=' . $currency_code_, \NumberFormatter::CURRENCY);
	return $formatter_->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
}
function pb_number_only_digit($num_){
	$digit_ = strlen(intval($num_));
	$clean_num_ = pow(10, ($digit_-1));
	return intval($num_ / $clean_num_) * $clean_num_;
}

function pb_404(){
	global $wp_query;
	status_header(404);
	$wp_query->set_404();
	do_action('pb_template_404');
	// wp_redirect();
}

function pb_ymd($source_ = null){
	if(!empty($source_)){
		return date('Ymd', $source_);	
	}else{
		return date('Ymd');
	}
}
function pb_ymdhis($source_ = null){
	if(!empty($source_)){
		return date('YmdHis', $source_);	
	}else{
		return date('YmdHis');
	}
	
}

function pb_convert_url_to_path($url_){
	$path_ = parse_url($url_, PHP_URL_PATH);

//To get the dir, use: dirname($path)

	return $_SERVER['DOCUMENT_ROOT'] . $path_;
}

function pb_convert_to_array($data_){
	if(gettype($data_) !== "array"){

		if($trim_){
			$data_ = trim($data_);
		}

		if(strlen($data_) > 0){
			$data_ = array($data_);	
		}else $data_ = array();
	}

	return $data_;
}

function pb_is_image_vertical($image_url_){
	$arr_image_details = getimagesize($image_url_); // pass id to thumb name

	$width_ = $arr_image_details[0];
	$height_ = $arr_image_details[1];

	return $width_ >= $height_;
}

function pb_check_user_role($user_id_, $role_){
	$user_ = get_user_by('ID', $user_id_);
	if(empty($user_)) return false;
	return in_array($role_, (array)$user_->roles);
}

function pb_exchange_rate_realtime($currency_a_, $currency_b_){

	$exchange_url_ = "http://api.fixer.io/latest?base=".strtoupper($currency_a_)."&symbols=".strtoupper($currency_b_);
	$ch_ = curl_init();
	curl_setopt($ch_, CURLOPT_URL, $exchange_url_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch_, CURLOPT_CONNECTTIMEOUT, 1000);
	$result_ = curl_exec($ch_);
	curl_close($ch_);

	$result_ = json_decode($result_, true);

	return array(
		'base' => $result_['base'],
		'date' => $result_['date'],
		'rate' => $result_['rates'][strtoupper($currency_b_)],
	);

	return $result_;
}

function pb_make_select_with_pages($args_ = array()){
	$page_list_ = $pages = get_pages($args_);

	$html_ = "";
	$html_ .= '<option value="">-페이지선택-</option>';
	foreach($page_list_ as $row_index_ => $page_){
		$html_ .= '<option value="'.$page_->ID.'" '.((isset($args_["default"]) && ((string)$args_["default"]) === (string)$page_->ID) ? "selected" : "").'>'.'('.$page_->ID.') '.$page_->post_title.'</option>';
	}

	return $html_;
}

function pb_draw_admin_badge($count_){
	return "<span class='update-plugins count-1'><span class='update-count'>$count_</span></span>";
}

function pb_resize_image($file, $w, $h, $crop=FALSE){
	list($width, $height) = getimagesize($file);
	$r = $width / $height;
	if ($crop) {
		if ($width > $height) {
			$width = ceil($width-($width*abs($r-$w/$h)));
		} else {
			$height = ceil($height-($height*abs($r-$w/$h)));
		}
		$newwidth = $w;
		$newheight = $h;
	} else {
		if ($w/$h > $r) {
			$newwidth = $h*$r;
			$newheight = $h;
		} else {
			$newheight = $w/$r;
			$newwidth = $w;
		}
	}
	$src = imagecreatefromjpeg($file);
	$dst = imagecreatetruecolor($newwidth, $newheight);
	imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

	return $dst;
}

function pb_colorcode_to_rgba($color_code_){
    if($color_code_[0] == '#')
      $color_code_ = substr($color_code_, 1);

    if (strlen($color_code_) == 3)
    {
      $color_code_ = $color_code_[0] . $color_code_[0] . $color_code_[1] . $color_code_[1] . $color_code_[2] . $color_code_[2];
    }

    $r = hexdec($color_code_[0] . $color_code_[1]);
    $g = hexdec($color_code_[2] . $color_code_[3]);
    $b = hexdec($color_code_[4] . $color_code_[5]);

    return $b + ($g << 0x8) + ($r << 0x10);
}

function pb_rgb_to_hsl($RGB){
    $r = 0xFF & ($RGB >> 0x10);
    $g = 0xFF & ($RGB >> 0x8);
    $b = 0xFF & $RGB;

    $r = ((float)$r) / 255.0;
    $g = ((float)$g) / 255.0;
    $b = ((float)$b) / 255.0;

    $maxC = max($r, $g, $b);
    $minC = min($r, $g, $b);

    $l = ($maxC + $minC) / 2.0;

    if($maxC == $minC)
    {
      $s = 0;
      $h = 0;
    }
    else
    {
      if($l < .5)
      {
        $s = ($maxC - $minC) / ($maxC + $minC);
      }
      else
      {
        $s = ($maxC - $minC) / (2.0 - $maxC - $minC);
      }
      if($r == $maxC)
        $h = ($g - $b) / ($maxC - $minC);
      if($g == $maxC)
        $h = 2.0 + ($b - $r) / ($maxC - $minC);
      if($b == $maxC)
        $h = 4.0 + ($r - $g) / ($maxC - $minC);

      $h = $h / 6.0; 
    }

    $h = (int)round(255.0 * $h);
    $s = (int)round(255.0 * $s);
    $l = (int)round(255.0 * $l);

    return (object) Array('hue' => $h, 'saturation' => $s, 'lightness' => $l);
}

function pb_is_colorcode_light($color_code_){
	$rgb_ = pb_colorcode_to_rgba($color_code_);
	$hsl_ = pb_rgb_to_hsl($rgb_);
	return $hsl_->lightness > 200;
}

?>