<?php

add_filter("pbadmin_settings_form", function($view_list_, $settings_){

		ob_start();
	?>
	<h3>지도관련</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="google_map_api_key">네이버맵 API KEY</label></th>
				<td><input type="text" name="naver_map_api_key" class="large-text" placeholder="API KEY" value="<?= $settings_["naver_map_api_key"]; ?>"></td>
			</tr>
			
		</tbody>
	</table>
	<?php

		$view_ = ob_get_clean();
		$view_list_[] = array(
			"id" => "map-settings",
			"title" => "지도관련",
			"view" => $view_,
		);
		return $view_list_;
	}, 10, 2);

?>