<?php

class PBAdmin_crontab_settings{
	static function _add_settings_form($view_list_, $settings_){
		$exists_cron_ = PB_crontab::available();

		$crontab_cycle_ = (int)(isset($settings_["common_cron_cycle"]) ? $settings_["common_cron_cycle"] : 30);

		if($crontab_cycle_ == 0){
			$crontab_cycle_ = 30;
		}

		ob_start();
	?>	
	<h3>스케쥴러설정</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="common_use_cron">스케쥴러사용</label></th>
				<td><p>
					<label for="common_use_cron"><input type="checkbox" name="common_use_cron" id="common_use_cron" <?= ($settings_["common_use_cron"] === true ? "checked" : ""); ?> <?= ($exists_cron_ === null ? "readonly" : ""); ?> >스케쥴러 사용</label>
					</p>
					<p class="hint">*Linux cron 환경에서만 가능합니다.</p>
					<p class="hint">*스케쥴러는 <?=$crontab_cycle_?>분마다 한번 호출됩니다.</p>
				</td>
			</tr>

		<?php if($exists_cron_){ ?>
			<!-- <tr>
				<th><label for="common_cron_cycle">주기설정</label></th>
				<td>
					<input type="number" name="common_cron_cycle" id="common_cron_cycle" class="large-text" placeholder="분단위로 입력" value="<?=$settings_["common_cron_cycle"]?>">
					<p class="hint">*분 단위로 입력하세요.</p>
				</td>
			</tr> -->

			<input type="hidden" name="common_cron_cycle" id="common_cron_cycle" value="<?=$crontab_cycle_?>">
			
			<tr>
				<th>
					<label for="common_cron_perform_allow_access">접근허용</label>
				</th>
				<td>
					<select name="common_cron_perform_allow_access">
						<option value="admin" <?= ($settings_["common_cron_perform_allow_access"] === "admin" ? "selected" : ""); ?> >관리자만</option>
						<option value="all" <?= ($settings_["common_cron_perform_allow_access"] === "all" ? "selected" : ""); ?> >전체</option>
					</select>
					<p class="hint">*위 항목을 체크할 경우 관리자만 스케쥴러 수행 페이지에 접근할 수 있습니다.</p>
				</td>
			</tr>
			<tr>
				<th>
					<label for="common_cron_perform_ip_white_list">접근IP White List</label>
				</th>
				<td>
					<input type="text" name="common_cron_perform_ip_white_list" class="large-text" placeholder="접근IP" value="<?= $settings_["common_cron_perform_ip_white_list"]; ?>">
					<p class="hint">*위 항목을 체크할 경우 White List에 등록된 IP만 스케쥴러 수행 페이지에 접근할 수 있습니다.</p>
					<p class="hint">*다수의 접근IP를 설정할 경우 ',' 쉼표로 구분하여 입력하시길 바랍니다.</p>
				</td>
			</tr>
			<tr>
				<th>
					<label for="common_cron_perform_ip_white_list">cron 명령어 상태</label>
				</th>
				<td>
					<?php

					$cron_cycle_ = $settings_["common_cron_cycle"];
					if(PB_crontab::job_exists("*/".$cron_cycle_." * * * * curl ".pb_crontab_url())){
						echo "<b style='color:blue;'>스케쥴러 명령어가 정상등록되었습니다.</b>";
					}else{
						echo "<b>스케쥴러 명령어가 등록되어 있지 않습니다.</b>";
					}
					?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<?php

		$view_ = ob_get_clean();

		$view_list_[] = array(
			"id" => "crontab-settings",
			"title" => "스케쥴러관련",
			"view" => $view_,
		);

		return $view_list_;
	}

	static function _filter_settings($settings_){
		$settings_["common_use_cron"] = ((isset($settings_["common_use_cron"]) && $settings_["common_use_cron"] === "on") ? true : false);
		
		return $settings_;
	}

	static function _before_update_settings($settings_){
		if(PB_crontab::available() === null){
			return;
		}
		
		$use_cron_ = $settings_["common_use_cron"];
		
		if($use_cron_){
			$cron_cycle_ = $settings_["common_cron_cycle"];
			PB_crontab::remove_job("*/".$cron_cycle_." * * * * curl ".pb_crontab_url());
		}

	}
	static function _after_update_settings($settings_){
		if(PB_crontab::available() === null){
			return;
		}

		$use_cron_ = $settings_["common_use_cron"];

		if($use_cron_){
			$cron_cycle_ = $settings_["common_cron_cycle"];
			PB_crontab::add_job("*/".$cron_cycle_." * * * * curl ".pb_crontab_url());
		}
	}
}

add_filter("pbadmin_settings_form",array("PBAdmin_crontab_settings","_add_settings_form"), 10, 2);
add_filter("pb_update_settings", array("PBAdmin_crontab_settings", "_filter_settings"));
add_action("pb_before_update_settings", array("PBAdmin_crontab_settings", "_before_update_settings"));
add_action("pb_after_update_settings", array("PBAdmin_crontab_settings", "_after_update_settings"));

?>