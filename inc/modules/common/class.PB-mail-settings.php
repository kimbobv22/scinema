<?php

class PB_mail_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"email_sender" => "",
			"email_smtp_host" => "",
			"email_smtp_port" => "",
			"email_smtp_auth" => "Y",
			"email_smtp_user" => "",
			"email_smtp_pass" => "",
			"email_smtp_secure" => "",

			"email_receipt" => "",
			"email_template" => "",
		),$settings_);
	}
}

add_filter("pb_default_settings", array("PB_mail_settings", "_add_default_settings"));

add_action('phpmailer_init', function($phpmailer){
	$settings_ = pb_settings();

	if(!strlen($settings_['email_smtp_host'])) return;

	$phpmailer->IsSMTP();
	$phpmailer->Host = $settings_['email_smtp_host'];
	$phpmailer->Port = $settings_['email_smtp_port'];
	
	if($settings_['email_smtp_auth'] === "Y"){
		$phpmailer->SMTPAuth = true;
		$phpmailer->Username = $settings_['email_smtp_user'];
		$phpmailer->Password = $settings_['email_smtp_pass'];
	}

	if(strlen($settings_['email_smtp_secure'])){
		$phpmailer->SMTPSecure = $settings_['email_smtp_secure'];
	}

	$phpmailer->From = $settings_['email_sender'];
	$phpmailer->FromName = get_bloginfo('name');
	$phpmailer->SetFrom($phpmailer->From, $phpmailer->FromName);
	$phpmailer->SMTPAutoTLS = false;
});

?>