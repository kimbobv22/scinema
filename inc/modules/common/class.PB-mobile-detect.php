<?php

include_once(PB_THEME_LIB_DIR_PATH."modules/common/lib/mobile-detect/Mobile_Detect.php");

function pb_is_mobile(){
	$detect = new Mobile_Detect;
	return $detect->isMobile();
}
function pb_is_ios(){
	$detect = new Mobile_Detect;
	return $detect->isiOS();
}

function pb_is_tablet(){
	$detect = new Mobile_Detect;
	return $detect->isTablet();
}

?>