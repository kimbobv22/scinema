<?php

define('PBFILEUPLOADSLUG', 'file_upload');

add_filter('wp_unique_post_slug', function($slug_){
	if($slug_ === PBFILEUPLOADSLUG || $slug_ === PBFILEUPLOADSLUG) return $slug_."1";
	return $slug_;
});

add_action("init" ,function(){
	add_rewrite_tag('%pbfileupload%', '([^&]+)');
});

function _pb_fileupload_rewrite_rule(){
	add_rewrite_rule('^'.PBFILEUPLOADSLUG.'/?$','index.php?pbfileupload=Y','top');
}
add_action('pb_add_rewrite_rule', '_pb_fileupload_rewrite_rule');

define('PBFILEUPLOAD_TEMP_DIR', ABSPATH."pbtempupload/");
define('PBFILEUPLOAD_TEMP_URL', home_url()."/pbtempupload/");

add_action('template_redirect', function(){
	if(get_query_var('pbfileupload') !== "Y") return;


	@header('Vary: Accept');
	@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
	if(isset($_SERVER['HTTP_ACCEPT']) &&
		(strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)){
		header('Content-type: application/json');
	}else{
		header('Content-type: text/plain');
	}

	send_nosniff_header();
	nocache_headers();

	require(PB_THEME_LIB_DIR_PATH . 'modules/common/lib/UploadHandler.php');

	$tmp_ = (isset($_GET['temp']) && $_GET['temp'] === "Y");
	$image_max_width_ = isset($_GET['max_width']) ? $_GET['max_width']: null;

	$upload_url_ = null;
	$upload_path_ = null;



	date_default_timezone_set('Asia/Seoul');
	$yyymmddhh_ = new DateTime();
	$yyymmddhh_ = date_format($yyymmddhh_,"YmdH");

	if((isset($_GET['temp']) && $_GET['temp'] === "Y")){
		$upload_path_ = PBFILEUPLOAD_TEMP_DIR.$yyymmddhh_."/";
		$upload_url_ = PBFILEUPLOAD_TEMP_URL.$yyymmddhh_."/";
	}else{
		$upload_dir_ = wp_upload_dir();
		$upload_path_ = $upload_dir_["path"];
		$upload_url_ = $upload_dir_["url"];
	}

	$upload_handler_ = new UploadHandler(array(
		'upload_dir' => $upload_path_,
		'upload_url' => $upload_url_,
		'max_width' => $image_max_width_,
	));


	$action_ = isset($_GET['action']) ? $_GET['action'] : null;

	if(strlen($action_)){
		do_action('pb_fileuploaded_'.$action_, $upload_handler_->get_response());		
	}

	die();
});

function pb_fileupload_url(){
	//return home_url(PBFILEUPLOADSLUG,'https');
	return home_url(PBFILEUPLOADSLUG);
}

function pb_fileupload_temp_to_real($path_){
	$upload_filepath_ = pb_upload_filepath(null, pathinfo($path_, PATHINFO_EXTENSION));
	rename($path_, $upload_filepath_["path"]);
	return $upload_filepath_;
}

add_action('pb_crontab_task', function(){ //임시파일 삭제

	date_default_timezone_set('Asia/Seoul');
	$yyymmddhh_ = new DateTime();
	$yyymmddhh_ = date_format($yyymmddhh_,"YmdH");

	$temp_dir_ = scandir(PBFILEUPLOAD_TEMP_DIR);
	foreach($temp_dir_ as $dir_){
		$a_ = (int)$dir_;
		$b_ = (int)$yyymmddhh_;

		if($b_ - $a_ > 2){
			rmdir(PBFILEUPLOAD_TEMP_DIR.$dir_);
		}
	}
});

add_filter('pb_script_common_var', function($var_){
	$var_['fileupload_url'] = pb_fileupload_url();
	return $var_;
});

?>