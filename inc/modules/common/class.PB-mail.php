<?php

function pb_mail_template($data_ = array()){
	$settings_ = PB_settings::get_settings();
	$email_template_ = $settings_["email_template"];
	$email_template_ = pb_map_string($email_template_, $data_);

	return stripslashes($email_template_);
}
function pb_mail_send($to_, $subject_, $data_ = array(), $headers_ = array('Content-Type: text/html; charset=UTF-8'), $attachments_ = null){
	return wp_mail($to_, $subject_, pb_mail_template($data_), $headers_, $attachments_);
}
function pb_mail_send_to_admin($subject_, $data_ = array(), $headers_ = array('Content-Type: text/html; charset=UTF-8'), $attachments_ = null){
	return wp_mail(pb_settings("email_receipt"), $subject_, $data_, $headers_, $attachments_);	
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-mail-settings.php');

?>