<?php

class PB_crontab{

	static function available(){
		$return_ = shell_exec("which crontab");
		return (empty($return_) ? false : true);
	}

	static private function _string_to_array($jobs_ = ""){
		$array_ = explode("\r\n", trim($jobs_));
		foreach($array_ as $key_ => $item_){

			if($item_ === ""){
				unset($array_[$key_]);
			}
		}

		return $array_;
	}

	static private function _array_to_string($jobs_ = array()){
		return implode("\r\n", $jobs_);
	}

	static function jobs(){
		$output_ = shell_exec("crontab -l");
		return self::_string_to_array($output_);
	}

	static function save_jobs($jobs_ = array()){
		$output_ = shell_exec(('echo "'.self::_array_to_string($jobs_).'" | crontab -'));
		return $output_;
	}

	static function job_exists($job_ = ""){
		$jobs_ = self::jobs();
		if(in_array($job_, $jobs_)){
			return true;
		}else{
			return false;
		}
	}

	static function add_job($job_ = ""){
		if(self::job_exists($job_)){
			return false;
		}

		$jobs_ = self::jobs();
		array_push($jobs_, $job_);
		return self::save_jobs($jobs_);
	}

	static function remove_job($job_ = ""){
		if(!self::job_exists($job_)){
			return false;
		}

		$jobs_ = self::jobs();
		unset($jobs_[array_search($job_, $jobs_)]);
		return self::save_jobs($jobs_);
	}

	static function remove_all_jobs(){
		return self::save_jobs(array());
	}

}


add_action('template_redirect', function(){
	if(get_query_var('pbusecrontab') !== "Y") return;

	@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
	@header( 'X-Robots-Tag: noindex' );
	// @header('Access-Control-Allow-Origin: *');  

	send_nosniff_header();
	nocache_headers();

	$settings_ = PB_settings::get_settings();
	
	if($settings_["common_cron_perform_allow_access"] === "admin" && !pb_is_admin()){
		wp_die("잘못된 접근입니다.");
	}

	$white_list_ = $settings_["common_cron_perform_ip_white_list"];
	$client_ip_ = pb_client_ip();

	if($settings_["common_cron_perform_allow_access"] === "all"
		&& strlen(trim($white_list_)) > 0 && $_SERVER['SERVER_ADDR'] !== $client_ip_){
		if(strpos($white_list_, ",") === false){
			$white_list_ = array(trim($white_list_));	
		}else{
			$white_list_ = explode (",", $white_list_);
		}

		$white_list_[] = '127.0.0.1';
		$white_list_[] = '::1';
		
		$can_access_ = false;
		foreach($white_list_ as $row_index_ => $ip_){
			if(strpos($client_ip_, $ip_) !== false){
				$can_access_ = true;
				break;
			}
		}

		if(!$can_access_){
			wp_die("잘못된 접근입니다.");
		}
	}

	date_default_timezone_set('Asia/Seoul');
	$hh_mm_ = date("H_i",time());
	$hh_mm_ = substr($hh_mm_,0,-1)."0";

	do_action("pb_crontab_task");
	do_action("pb_crontab_task_".$hh_mm_);
	die();
});

define('PBCRONTABSLUG', '_crontab_task');

add_filter('wp_unique_post_slug', function($slug_){
	if($slug_ === PBCRONTABSLUG || $slug_ === PBCRONTABSLUG) return $slug_."1";
	return $slug_;
});

add_action("init" ,function(){
	add_rewrite_tag('%pbusecrontab%', '([^&]+)');
});

function _pb_crontab_rewrite_rule(){
	add_rewrite_rule('^'.PBCRONTABSLUG.'/?$','index.php?pbusecrontab=Y','top');
}
add_action('pb_add_rewrite_rule', '_pb_crontab_rewrite_rule');

function pb_crontab_url(){
	return home_url(PBCRONTABSLUG);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-crontab-settings.php');

?>