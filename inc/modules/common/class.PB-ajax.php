<?php

define('PBAJAXSLUG', 'ajax');

add_filter('wp_unique_post_slug', function($slug_){
	if($slug_ === PBAJAXSLUG || $slug_ === PBAJAXSLUG) return $slug_."1";
	return $slug_;
});

add_action("init" ,function(){
	add_rewrite_tag('%pbuseajax%', '([^&]+)');
});

function _pb_ajax_rewrite_rule(){
	add_rewrite_rule('^'.PBAJAXSLUG.'/?$','index.php?pbuseajax=Y','top');
}
add_action('pb_add_rewrite_rule', '_pb_ajax_rewrite_rule');

//AJAX 처리
add_action('template_redirect', function(){
	if(get_query_var('pbuseajax') !== "Y") return;

	@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
	@header( 'X-Robots-Tag: noindex' );
	// @header('Access-Control-Allow-Origin: *');  

	send_nosniff_header();
	nocache_headers();

	if(isset($_REQUEST['action'])){
		do_action('pb_ajax_' . $_REQUEST['action']);
	}

	die();
});

function pb_ajax_url($action_ = null, $params_ = array()){
	if(!strlen($action_)){
		return home_url(PBAJAXSLUG);
	}
	$params_['action'] = $action_;
	return pb_make_url(home_url(PBAJAXSLUG), $params_);
}

add_filter('pb_script_common_var', function($var_){
	$ajax_url_ = pb_ajax_url();

	if(is_admin()){
		$ajax_url_ = admin_url("admin-ajax.php");
	}
	$var_['admin_ajaxurl'] = pb_admin_ajax_url();
	$var_['ajaxurl'] = $ajax_url_;
	
	return $var_;
});

?>