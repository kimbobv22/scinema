<?php

add_filter("pbadmin_settings_form", function($view_list_, $settings_){

		ob_start();
	?>
	<h3>메일관련사항</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="email_sender">발신메일</label></th>
				<td><input type="text" name="email_sender" class="large-text" placeholder="발신메일주소" value="<?= $settings_["email_sender"]; ?>"></td>
			</tr>
	
			<tr>
				<th><label for="email_smtp_host">SMTP 호스트</label></th>
				<td><input type="text" name="email_smtp_host" class="large-text" placeholder="URL 입력" value="<?= $settings_["email_smtp_host"]; ?>"></td>
			</tr>
			<tr>
				<th><label for="email_smtp_port">SMTP 포트</label></th>
				<td><input type="text" name="email_smtp_port" class="large-text" placeholder="포트 입력" value="<?= $settings_["email_smtp_port"]; ?>"></td>
			</tr>
			<tr>
				<th><label for="email_smtp_auth">SMTP 인증사용</label></th>
				<td>
					<label><input type="radio" name="email_smtp_auth" value="Y" <?= checked($settings_["email_smtp_auth"], "Y") ?>> 사용</label><br>
					<label><input type="radio" name="email_smtp_auth" value="N" <?= checked($settings_["email_smtp_auth"], "N") ?>> 미사용</label>
				</td>
			</tr>
			<tr data-email-smtp-auth-group>
				<th><label for="email_smtp_user">SMTP ID</label></th>
				<td><input type="text" name="email_smtp_user" class="large-text" placeholder="ID 입력" value="<?= $settings_["email_smtp_user"]; ?>"></td>
			</tr>
			<tr data-email-smtp-auth-group>
				<th><label for="email_smtp_pass">SMTP PASS</label></th>
				<td><input type="password" name="email_smtp_pass" class="large-text" placeholder="PASS 입력" value="<?= $settings_["email_smtp_pass"]; ?>"></td>
			</tr>
			<tr>
				<th><label for="email_smtp_secure">SMTP 보안</label></th>
				<td>
					<?php

						$secure_list_ = array('' => '사용하지 않음', 'ssl' => 'SSL', 'tls' => 'TLS');

					?>
					<select name="email_smtp_secure">
						<?php foreach($secure_list_ as $key_ => $title_){ ?>
						<option value="<?=$key_?>" <?=selected($key_, $settings_["email_smtp_secure"])?>><?=$title_?></option>
						<?php } ?>
					</select>
				</td>
			</tr>

			<tr>
				<th><label for="email_sender">수신메일</label></th>
				<td><input type="text" name="email_receipt" class="large-text" placeholder="수신메일주소" value="<?= $settings_["email_receipt"]; ?>"></td>
			</tr>
			
			<tr>
				<th colspan="2"><label for="email_template">메일서식</label></th>
			</tr>
			<tr>
				<td colspan="2">
					<?php wp_editor(stripslashes($settings_["email_template"]), "pb-settings-email-template" , array(
						"textarea_name" => "email_template",
						"tinymce" => false,
					)); ?>
				</td>
			</tr>

		</tbody>
	</table>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		var stmp_auth_checkbox_ = $(":input[name='email_smtp_auth']");

		var stmp_auth_check_func_ = function(){
			var toggle_ = $(":input[name='email_smtp_auth']:checked").val() === "Y";
			$("[data-email-smtp-auth-group]").toggle(toggle_);
		};

		stmp_auth_checkbox_.click(stmp_auth_check_func_);
		stmp_auth_check_func_();
	});
	</script>
	<?php

		$view_ = ob_get_clean();

		$view_list_[] = array(
			"id" => "mail-settings",
			"title" => "메일관련",
			"view" => $view_,
		);
		return $view_list_;
	}, 10, 2);


?>