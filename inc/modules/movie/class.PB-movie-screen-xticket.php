<?php

function pb_movie_screen_xticket_timetable($xcinema_id_, $target_date_, $xmovie_id_, $include_not_available_ = false){

	if(strpos($xmovie_id_, "|") > 0){ //예매가 인상에 따른 영화코드가 두개일경우 임시 분기
		$xmovie_id_ = explode("|", $xmovie_id_);

		$results_ = array();

		foreach($xmovie_id_ as $temp_id_){
			$temp_array_ = pb_movie_screen_xticket_timetable($xcinema_id_, $target_date_, $temp_id_, $include_not_available_);
			$results_ = array_merge($results_, $temp_array_);

		}

		uasort($results_, function($a_val_,$b_val_){
			$a_str_key_ = $a_val_['hall'].$a_val_['time'];
			$b_str_key_ = $b_val_['hall'].$b_val_['time'];
			
			return $b_str_key_ < $a_str_key_;
		});

		return $results_;
	}


	$api_url_ = "http://www.xticket.co.kr/Ticketing/SCScheduleToXml.aspx?p_PlanCompanycd=".$xcinema_id_."&p_PlayDate=".$target_date_;

	$ch_ = curl_init();
	curl_setopt($ch_, CURLOPT_URL, $api_url_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch_, CURLOPT_CONNECTTIMEOUT, 1000);
	$result_ = curl_exec($ch_);
	curl_close($ch_);

	$t_result_ = simplexml_load_string($result_);


	if($t_result_ === false) return false;

	$t_result_ = json_decode(json_encode($t_result_), true);

	if(!isset($t_result_['Movie'])) return array();

	$results_ = array();

	if(isset($t_result_['Movie']['@attributes'])){
		$t_result_['Movie'] = array($t_result_['Movie']);
	}

	foreach($t_result_['Movie'] as $movie_data_){
		$attributes_ = $movie_data_['@attributes'];

		if($xmovie_id_ !== $attributes_['movie_id']){

			continue;
		}


		$t_schedules_ = $movie_data_['Schedule'];

		if(isset($t_schedules_['id'])){
			$t_schedules_ = array($t_schedules_);
		}

		foreach($t_schedules_ as $t_schedule_){

			if(!$include_not_available_ && (!isset($t_schedule_['status']) || $t_schedule_['status'] === "N")) continue;

			$results_[$t_schedule_['id']] = array(
				'id' => $t_schedule_['id'],
				'hall' => $t_schedule_['hall'],
				'time' => $t_schedule_['time'],
				'seat' => $t_schedule_['seat'],
				'available_yn' => $t_schedule_['status'],
			);
		}
	}


	uasort($results_, function($a_val_,$b_val_){
		$a_str_key_ = $a_val_['hall'].$a_val_['time'];
		$b_str_key_ = $b_val_['hall'].$b_val_['time'];
		
		return $b_str_key_ < $a_str_key_;
	});


	return $results_;

}

function pb_movie_xticket_movie_list($xcinema_id_){
	$api_url_ = "http://www.xticket.co.kr/Ticketing/SCPerformanceToXml.aspx?p_PlanCompanycd=".$xcinema_id_;

	$ch_ = curl_init();
	curl_setopt($ch_, CURLOPT_URL, $api_url_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch_, CURLOPT_CONNECTTIMEOUT, 1000);
	$result_ = curl_exec($ch_);
	curl_close($ch_);

	$t_result_ = simplexml_load_string($result_);


	if($t_result_ === false) return false;

	$t_result_ = json_decode(json_encode($t_result_), true);

	if(!isset($t_result_['Movie'])) return array();

	$movie_list_ = $t_result_['Movie'];

	if(isset($movie_list_['id'])){
		$movie_list_ = array($movie_list_);
	}

	return $movie_list_;
}

function pb_movie_xticket_movie_make_options($xcinema_id_, $default_ = null){
	$movie_list_ = pb_movie_xticket_movie_list($xcinema_id_);

	ob_start();

	foreach($movie_list_ as $row_data_){
		?> 
			<option value="<?=$row_data_['id']?>" <?=selected($row_data_['id'], $default_)?> ><?=$row_data_['name']?></option>
		<?php
	}
	return ob_get_clean();
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-xticket-view.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-xticket-ajax.php');

?>