<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();
	$current_cinema_data_ = pb_cinema($blog_id_);
	$cinema_type_ = $current_cinema_data_->ticket_ref_type;

	if ($cinema_type_ === "dtryx") {
		return $results_;
	}

	$results_['movie-screen-status'] = array(
		'title' => '상영작현황' ,
		'content-template' => "movie-screen-status/list",
		'lvl' => 1,
		'sort_num' => 14,
	);

	return $results_;
});

?>