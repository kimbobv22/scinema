<?php

add_action('init', function(){

	global $wpdb;

	$wpdb->pbmovie_open = "{$wpdb->base_prefix}movie_open";
	// $wpdb->pbcinema_area = "{$wpdb->base_prefix}movie_open";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}movie_open` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`cinema_id` BIGINT(20)  DEFAULT NULL COMMENT '영화관ID',

		`kofic_code` varchar(50)  DEFAULT NULL COMMENT '영진위코드',
		`movie_name` varchar(100)  DEFAULT NULL COMMENT '영화제목',
		`open_date` date  DEFAULT NULL COMMENT '개봉에정일자',
		
		`level` varchar(5)  DEFAULT NULL COMMENT '등급 - MO001',

		`genre` varchar(100)  DEFAULT NULL COMMENT '장르',
		`3d_yn` varchar(2)  DEFAULT NULL COMMENT '3D여부',
		`director` varchar(50)  DEFAULT NULL COMMENT '감독',
		`main_actors` varchar(200)  DEFAULT NULL COMMENT '주연',
		`running_time` bigint(5)  DEFAULT NULL COMMENT '러닝타임',
		`country` varchar(50)  DEFAULT NULL COMMENT '제작국가',
		`image_url` longtext  DEFAULT NULL COMMENT '이미지',	
		`story` longtext  DEFAULT NULL COMMENT '줄거리',	

		`status` varchar(5)  DEFAULT NULL COMMENT '등록상태 - MO003',	

		`publisher_id` BIGINT(20)  DEFAULT NULL COMMENT '배급사ID',	
		`publisher_charge` BIGINT(20)  DEFAULT NULL COMMENT '배급사부금',	
		`cinema_charge` BIGINT(20)  DEFAULT NULL COMMENT '극장부금',	
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='영화개봉정보';";

	return $args_;
},10, 3);

add_filter('pb_initial_gcode_list', function($gcode_list_){

	$gcode_list_['MO001'] = array(
		'name' => '등급',
		'data' => array(
			'00001' => '12세 이상 관람가',
			'00003' => '15세 이상 관람가',
			'00005' => '18세 이상 관람가',
			'00007' => '전체 관람가',
		),
	);

	$gcode_list_['MO003'] = array(
		'name' => '개봉영화상태',
		'data' => array(
			'00001' => '정상등록',
			'00009' => '숨김',
		),
	);

	return $gcode_list_;
});

?>