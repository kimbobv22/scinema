<?php

function pb_movie_draw_xticketing_popup($schedule_id_, $cinema_id_){

?>

<div class="pb-xticketing-modal modal" id="pb-xticketing-modal"><form method="post" action="<?=pb_movie_xticketing_url($schedule_id_)?>" id="pb-xticketing-modal-form">
	<input type="hidden" name="cinema_id" value="<?=$cinema_id_?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">예매정보입력</h4>
			</div>
			<div class="modal-body">
				<h4>개인정보활용동의<br/>
					<small>작은영화관 예매서비스 제공을 위해 필요한 최소한의 개인정보이므로 입력(수집)에 동의하지 않을 경우 서비스를 이용하실 수 없습니다.</small></h4>
				<table class="table table-bordered privacy-info-table">
					<tbody>
						<tr class="target-purpose">
							<th>수집목적</th>
							<td>이용자 식별 및 본인 여부 확인</td>
						</tr>
						<tr class="target-purpose">
							<th>제3자제공</th>
							<td>CJ올리브네트웍스</td>
						</tr>
						
						<tr class="target-entry">
							<th>수집항목</th>
							<td>이름, 연락처</td>
						</tr>
					</tbody>
				</table>
				<div class="form-group"><div class="checkbox with-top text-right">
					<label><input type="checkbox" name="agreement02" required data-error="개인정보수집 동의가 필요합니다"> 개인정보수집에 동의합니다.</label>
					<div class="clearfix"></div>
				</div></div>
			
				<div class="clearfix"></div>

				<hr>

				<h4>개인정보입력<br/>
					<small>예매내역 확인 및 취소 그리고 티켓 발권에 지장없도록 잘 확인하시어 정확히 입력해주시기 바랍니다.</small></h4>

				<div class="form-group" >
					<label >이름</label>	
					<input type="text" class="form-control" id="pb-xticket-popup-form-full_name" placeholder="이름 입력" name="full_name" required data-error="이름을 입력하세요">
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>
					
				</div>

				
<!-- 
				<div class="form-group" >
					<label class="" for="pb-xticket-popup-form-birth_yyyymmdd">생년월일</label>
						<div class='input-group date' id='pb-xticket-popup-form-birth_yyyymmdd-wrap'>
							<input type="text" class="form-control" id="pb-xticket-popup-form-birth_yyyymmdd" placeholder="생년월일 입력" name="birth_yyyymmdd" required data-error="생년월일을 입력하세요">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
				</div> -->

				<div class="form-group" >
					<label class="" >휴대전화</label>
					<div class="row">
						<div class="col-sm-4 col-xs-4 col-sm-phone">
							<select class="form-control" name="phone1_1" data-error="앞자리를 선택하세요" required id="pb-xticket-popup-form-phone1_1">
								<option value="">-앞자리-</option>
								<?= pb_gcode_make_options('Z0003') ?>
							</select>
						</div>
						<div class="col-sm-4 col-xs-4 col-sm-phone">
							<input type="number" name="phone1_2" class="form-control" maxlength="4" data-error="중간자리를 입력하세요" required id="pb-xticket-popup-form-phone1_2">
						</div>
						<div class="col-sm-4 col-xs-4">
							<input type="number" name="phone1_3" class="form-control" maxlength="4" data-error="마지막자리를 입력하세요" required id="pb-xticket-popup-form-phone1_2">
						</div>
					</div>
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>

				</div>



				<div class="clearfix"></div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
				<button type="submit" class="btn btn-primary" >예매하기</button>
			</div>
		</div>
	</div>
</form></div>

<?php

}

function pb_movie_draw_xticketing_check_popup($cinema_id_ = null){

?>

<div class="pb-xticketing-modal modal" id="pb-xticketing-check-modal"><form method="post" action="<?=pb_movie_xticketing_check_url()?>" id="pb-xticketing-check-modal-form">

	<?php if(strlen($cinema_id_)){ ?>
		<input type="hidden" name="cinema_id" value="<?=$cinema_id_?>">
	<?php } ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">예매정보입력</h4>
			</div>
			<div class="modal-body">
				<h4>개인정보활용동의<br/>
					<small>작은영화관 예매서비스 제공을 위해 필요한 최소한의 개인정보이므로 입력(수집)에 동의하지 않을 경우 서비스를 이용하실 수 없습니다.</small></h4>
				<table class="table table-bordered privacy-info-table">
					<tbody>
						<tr class="target-purpose">
							<th>수집목적</th>
							<td>이용자 식별 및 본인 여부 확인</td>
						</tr>
						<tr class="target-purpose">
							<th>제3자제공</th>
							<td>CJ올리브네트웍스</td>
						</tr>
						
						<tr class="target-entry">
							<th>수집항목</th>
							<td>이름, 연락처</td>
						</tr>
					</tbody>
				</table>
				<div class="form-group"><div class="checkbox with-top text-right">
					<label><input type="checkbox" name="agreement02" required data-error="개인정보수집 동의가 필요합니다"> 개인정보수집에 동의합니다.</label>
					<div class="clearfix"></div>
				</div></div>
			
				<div class="clearfix"></div>

				<hr>

				<h4>개인정보입력<br/>
					<small>예매내역 확인 및 취소 그리고 티켓 발권에 지장없도록 잘 확인하시어 정확히 입력해주시기 바랍니다.</small></h4>

				<div class="form-group" >
					<label >이름</label>	
					<input type="text" class="form-control" id="pb-xticket-popup-form-full_name" placeholder="이름 입력" name="full_name" required data-error="이름을 입력하세요">
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>
					
				</div>

				
<!-- 
				<div class="form-group" >
					<label class="" for="pb-xticket-popup-form-birth_yyyymmdd">생년월일</label>
						<div class='input-group date' id='pb-xticket-popup-form-birth_yyyymmdd-wrap'>
							<input type="text" class="form-control" id="pb-xticket-popup-form-birth_yyyymmdd" placeholder="생년월일 입력" name="birth_yyyymmdd" required data-error="생년월일을 입력하세요">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						
						<div class="help-block with-errors"></div>
						<div class="clearfix"></div>
				</div> -->

				<div class="form-group" >
					<label class="" >휴대전화</label>
					<div class="row">
						<div class="col-sm-4 col-xs-4 col-sm-phone">
							<select class="form-control" name="phone1_1" data-error="앞자리를 선택하세요" required id="pb-xticket-popup-form-phone1_1">
								<option value="">-앞자리-</option>
								<?= pb_gcode_make_options('Z0003') ?>
							</select>
						</div>
						<div class="col-sm-4 col-xs-4 col-sm-phone">
							<input type="number" name="phone1_2" class="form-control" maxlength="4" data-error="중간자리를 입력하세요" required id="pb-xticket-popup-form-phone1_2">
						</div>
						<div class="col-sm-4 col-xs-4">
							<input type="number" name="phone1_3" class="form-control" maxlength="4" data-error="마지막자리를 입력하세요" required id="pb-xticket-popup-form-phone1_2">
						</div>
					</div>
					<div class="help-block with-errors"></div>
					<div class="clearfix"></div>

				</div>



				<div class="clearfix"></div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
				<button type="submit" class="btn btn-primary" >예매확인</button>
			</div>
		</div>
	</div>
</form></div>

<?php

}



?>