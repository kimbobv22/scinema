<?php

add_filter("pbadmin_settings_form", function($view_list_, $settings_){
	ob_start();

?>
<h3>OPENAPI</h3>
<table class="form-table">
	<tr>
		<th>APIKEY</th>
		<td>
			<input type="text" name="moive_openapi_key" value="<?= $settings_["moive_openapi_key"]; ?>" class="large-text">
			<p class="desc">*APIKEY는 <a href="http://open.koreafilm.or.kr/" target="_blank">한국영상자료원</a>에서 발급받을 수 있습니다.</p>
		</td>
	</tr>
	
	
</table>

<?php

	$view_ = ob_get_clean();
	$view_list_[] = array(
		"id" => "movie-settings",
		"title" => "영화 관련",
		"view" => $view_,
	);

	return $view_list_;

}, 10, 2);

?>