<?php

pb_add_ajax('movie-screen-load-timetable', function(){

	$target_date_ = isset($_REQUEST["target_date"]) ? $_REQUEST["target_date"] : null;
	$area_id_ = isset($_REQUEST["area_id"]) ? $_REQUEST["area_id"] : null;	
	$screen_id_ = isset($_REQUEST["screen_id"]) ? $_REQUEST["screen_id"] : null;
	$open_id_ = isset($_REQUEST["open_id"]) ? $_REQUEST["open_id"] : null;

	if(strlen($screen_id_)){
		$movie_screen_data_ = pb_movie_screen_data($screen_id_);
		$timetable_results_ = pb_movie_screen_timetable($movie_screen_data_->cinema_id, $target_date_, $movie_screen_data_->ID);

		foreach($timetable_results_ as &$t_row_data_){
			$t_row_data_['cinema_id'] = $movie_screen_data_->cinema_id;
			$t_row_data_['cinema_name'] = $movie_screen_data_->cinema_name;
			$t_row_data_['cinema_logo_image_url'] = $movie_screen_data_->cinema_logo_image_url;
		}


	}else if(strlen($open_id_)){

		if($area_id_ === "all"){
			$area_id_ = null;
		}

		global $wpdb;

		$t_cinema_list_ = pb_cinema_list(array(
			'area_id' => $area_id_,
			'cinema_type' => PB_CINEMA_CTG_NOR,
			'orderby' => ' order by area_id asc, cinema_name asc '
		));

		$cinema_list_ = array();

		foreach($t_cinema_list_ as $cinema_){
			$cinema_list_[] = $cinema_->cinema_id;
		}

		if(count($cinema_list_) <= 0){
			echo json_encode(array(
				'success' => true,
				'timetable' => array(),
			));
			die();
		}

		$movie_screen_list_ = pb_movie_screen_list(array(
			'open_id' => $open_id_,
			'at_screen_date' => $target_date_,
			'cinema_id' => $cinema_list_,
		));


		$timetable_results_ = array();

		foreach($movie_screen_list_ as $screen_data_){

			$t_result_ = pb_movie_screen_timetable($screen_data_->cinema_id, $target_date_, $screen_data_->ID);

			foreach($t_result_ as &$t_row_data_){
				$t_row_data_['cinema_id'] = $screen_data_->cinema_id;
				$t_row_data_['cinema_name'] = $screen_data_->cinema_name;
				$t_row_data_['cinema_logo_image_url'] = $screen_data_->cinema_logo_image_url;
			}

			$timetable_results_ = array_merge($timetable_results_, $t_result_);
		}

	}else{
		if(!strlen($screen_id_)){
			echo json_encode(array(
				'success' => false,
				'error_title' => '필수항목누락',
				'error_message' => '영화ID가 누락되었습니다.',
			));
			die();
		}
	}

	echo json_encode(array(
		'success' => true,
		'timetable' => $timetable_results_,
	));
	die();
});

pb_add_ajax('adminpage-movie-screen-bulk-upload-load-subdata', function(){
	if(!pb_is_admin()){
		echo json_encode(array(
			'success' => false,
			'deny' => true
		));
		die();
	}

	$cinema_id_ = $_POST['cinema_id'];

	$cinema_data_ = pb_cinema($cinema_id_);


	if($cinema_data_->cinema_type !== PB_CINEMA_CTG_IND){
		$movie_open_list_ = pb_movie_open_list(array(
			'cinema_id' => PB_CINEMA_HEAD_OFFICE_ID,
			'orderby' => ' ORDER BY movie_name ASC ',
		));	
	}else{
		$movie_open_list_ = pb_movie_open_list(array(
			'cinema_id' => $cinema_id_,
			'orderby' => ' ORDER BY movie_name ASC ',
		));
	}

	foreach($movie_open_list_ as &$row_data_){
		$checkdata_ = pb_movie_screen_list(array(
			"open_id" => $row_data_->ID,
			"cinema_id" => $cinema_id_,
			'on_screening' => true,
		));

		if(count($checkdata_) > 0){
			$row_data_->disabled = "Y";
		}
	}

	

	
	$xticket_ref_list_ = pb_movie_xticket_movie_list($cinema_data_->xticket_ref_code);

	echo json_encode(array(
		'success' => true,
		'open_list' => $movie_open_list_,
		'xticket_ref_list' => $xticket_ref_list_,
	));
	die();

});

pb_add_ajax('adminpage-movie-screen-bulk-upload', function(){
	if(!pb_is_admin()){
		echo json_encode(array(
			'success' => false,
			'deny' => true
		));
		die();
	}


	$target_data_ = $_POST['target_data'];
	$cinema_id_ = $_POST['cinema_id'];

	foreach($target_data_ as $row_data_){
		if($row_data_['screen_srt_date'] === ""){
			$row_data_['screen_srt_date'] = null;
		}
		if($row_data_['screen_end_date'] === ""){
			$row_data_['screen_end_date'] = null;
		}

		$row_data_['status'] = "00001";
		$row_data_['cinema_id'] = $cinema_id_;

		$row_data_['screen_end_date_fg'] = strlen($row_data_['screen_end_date']) ? "Y" : null;
		$result_ = pb_movie_screen_data_add($row_data_);
	}

	echo json_encode(array(
		'success' => true,
	));
	die();
});

?>