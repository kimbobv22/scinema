<?php

add_action('init', function(){

	global $wpdb;

	$wpdb->pbmovie_screen = "{$wpdb->base_prefix}movie_screen";
	// $wpdb->pbcinema_area = "{$wpdb->base_prefix}movie_open";
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}movie_screen` (
		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`open_id` BIGINT(20)  NOT NULL COMMENT '개봉영화ID',
		`cinema_id` BIGINT(20)  NOT NULL COMMENT '영화관ID',

		`screen_srt_date` DATETIME  DEFAULT NULL COMMENT '상영시작일자',
		`screen_end_date` DATETIME  DEFAULT NULL COMMENT '상영종료예정일자',
		`screen_end_date_fg` varchar(1) DEFAULT NULL COMMENT '상영종료수정불가FG',
		`screen_close_date` DATETIME  DEFAULT NULL COMMENT '상영종료일자',

		`ticket_charge` BIGINT(10)  DEFAULT NULL COMMENT '예매가',
		`publisher_charge` BIGINT(10)  DEFAULT NULL COMMENT '배급사부금',
		`cinema_charge` BIGINT(10)  DEFAULT NULL COMMENT '극장부금',

		`status` varchar(5)  DEFAULT NULL COMMENT '등록상태',
		`xticket_ref_code` varchar(20)  DEFAULT NULL COMMENT 'X티켓연동코드',
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='영화개봉정보';";

	return $args_;
},10, 3);

add_filter('pb_initial_gcode_list', function($gcode_list_){

	$gcode_list_['MS001'] = array(
		'name' => '등록상태',
		'data' => array(
			'00001' => '예매가능',
			// '00003' => '예매불가',
			'00009' => '숨김',
		),
	);

	return $gcode_list_;
});

//개봉영화 상영작품 수 표기
add_filter('pb_movie_open_list_fields', function($query_, $conditions_){
	$query_ .= " 
		,IFNULL(movie_screen_info.screen_count, 0) screen_count ";

	return $query_;
},10,2);
//개봉영화 상영작품 수 표기
add_filter('pb_movie_open_list_where', function($query_, $conditions_){

	global $wpdb;

	if(isset($conditions_['on_screening_at_branch']) && $conditions_['on_screening_at_branch']){
		$query_ .= " AND IFNULL(movie_screen_info.screen_count, 0) > 0 ";
	}

	if(isset($conditions_['for_app_screening']) && $conditions_['for_app_screening']){
		$query_ .= " AND (IFNULL(movie_screen_info.screen_count, 0) > 0
			OR (
				DATEDIFF({$wpdb->pbmovie_open}.open_date, NOW()) <= 10
				AND DATEDIFF({$wpdb->pbmovie_open}.open_date, NOW()) >= 0
			)

		) ";
	}

	return $query_;
},10,2);

add_filter('pb_movie_open_list_join', function($query_, $conditions_){

	global $wpdb;

	$query_ .= " 
			 LEFT OUTER JOIN (
				SELECT  {$wpdb->pbmovie_screen}.open_id open_id
						,count(*) screen_count
				FROM {$wpdb->pbmovie_screen}
				WHERE (CURDATE() >= {$wpdb->pbmovie_screen}.screen_srt_date
					 AND {$wpdb->pbmovie_screen}.status = '00001'
					 AND (CURDATE() <= {$wpdb->pbmovie_screen}.screen_end_date OR {$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = ''))
				GROUP BY {$wpdb->pbmovie_screen}.open_id
			 ) movie_screen_info
		     ON   movie_screen_info.open_id = {$wpdb->pbmovie_open}.ID ";
	return $query_;

},10,2);

?>