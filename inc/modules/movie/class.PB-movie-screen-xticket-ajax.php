<?php

pb_add_ajax('movie-load-xticketing-popup', function(){
	$schedule_id_ = isset($_POST["schedule_id"]) ? $_POST["schedule_id"] : null;
	$cinema_id_ = isset($_POST["cinema_id"]) ? $_POST["cinema_id"] : null;

	ob_start();
	
	pb_movie_draw_xticketing_popup($schedule_id_, $cinema_id_);

	$popup_html_ = ob_get_clean();

	echo json_encode(array(
		'success' => true,
		'popup_html' => $popup_html_,
	));
	die();
});

pb_add_ajax('movie-load-xticketing-check-popup', function(){
	// $schedule_id_ = isset($_POST["schedule_id"]) ? $_POST["schedule_id"] : null;
	$cinema_id_ = isset($_POST["cinema_id"]) ? $_POST["cinema_id"] : null;

	ob_start();
	
	pb_movie_draw_xticketing_check_popup($cinema_id_);

	$popup_html_ = ob_get_clean();

	echo json_encode(array(
		'success' => true,
		'popup_html' => $popup_html_,
	));
	die();
});

pb_add_ajax('movie-screen-xticket-movie-list', function(){
	$cinema_id_ = $_POST['cinema_id'];

	if(strlen($cinema_id_)){
		$cinema_data_ = pb_cinema($cinema_id_);
		$movie_list_ = pb_movie_xticket_movie_list($cinema_data_->xticket_ref_code);
	}else{
		$movie_list_ = array();
	}

	echo json_encode(array(
		'success' => true,
		'movie_list' => $movie_list_,
	));
	die();
});

?>