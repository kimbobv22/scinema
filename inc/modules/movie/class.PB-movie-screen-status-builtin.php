<?php

add_filter('pb_movie_screen_list_where', function($query_, $conditions_){

	global $wpdb;

	if(isset($conditions_['for_movie_screen_status_ended'])){
		$query_ .= " AND  {$wpdb->pbmovie_screen}.screen_end_date >= '".$conditions_['for_movie_screen_status_ended'][0]."' ";
		$query_ .= " AND  {$wpdb->pbmovie_screen}.screen_end_date <= '".$conditions_['for_movie_screen_status_ended'][1]."' ";
	}
	if(isset($conditions_['for_movie_screen_status_started'])){
		$query_ .= " AND  {$wpdb->pbmovie_screen}.screen_srt_date >= '".$conditions_['for_movie_screen_status_started'][0]."' ";
		$query_ .= " AND  {$wpdb->pbmovie_screen}.screen_srt_date <= '".$conditions_['for_movie_screen_status_started'][1]."' ";
	}
	if(isset($conditions_['for_movie_screen_status_screening'])){
		$query_ .= " AND  {$wpdb->pbmovie_screen}.screen_srt_date <= '".$conditions_['for_movie_screen_status_screening'][0]."' ";
		$query_ .= " AND  ({$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = '' OR {$wpdb->pbmovie_screen}.screen_end_date >= '".$conditions_['for_movie_screen_status_screening'][1]."') ";

		$query_ .= " AND  !({$wpdb->pbmovie_screen}.screen_srt_date >= '".$conditions_['for_movie_screen_status_screening'][0]."' 
						 AND  {$wpdb->pbmovie_screen}.screen_srt_date <= '".$conditions_['for_movie_screen_status_screening'][1]."') ";
	}

	return $query_;
},10, 2);

?>