<?php



function pb_movie_boxoffice_openapi($yyyymmdd_){
	$url_ = "http://www.kobis.or.kr/kobisopenapi/webservice/rest/boxoffice/searchDailyBoxOfficeList.json";

	$params_ = "?key=".pb_settings('moive_openapi_key');
	$params_ .= "&targetDt=".urlencode($yyyymmdd_);

	$ch_ = curl_init();

	curl_setopt($ch_, CURLOPT_URL, $url_ . $params_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch_, CURLOPT_HEADER, FALSE);
	curl_setopt($ch_, CURLOPT_CUSTOMREQUEST, 'GET');
	$response_ = curl_exec($ch_);
	curl_close($ch_);

	$tmp_results_ =  json_decode($response_, true);

	print_r($tmp_results_);
	die();
	/*$tmp_results_ =  $tmp_results_['movieListResult']['movieList'];

	$results_ = array();

	foreach($tmp_results_ as $row_data_){
		$results_[] = array(
			'kofic_code' => $row_data_["movieCd"],
			'movie_name' => $row_data_["movieNm"],
			'open_date' => $row_data_["openDt"],
			'genre' => $row_data_["genreAlt"],
			'country' => $row_data_["repNationNm"],
		);
	}

	return $results_;*/
}


function pb_movie_find_openapi($query_, $offset_ = 0, $limit_ = 20){
	$url_ = "http://www.kobis.or.kr/kobisopenapi/webservice/rest/movie/searchMovieList.json";

	$params_ = "?key=".pb_settings('moive_openapi_key');
	$params_ .= "&movieNm=".urlencode($query_);

	$ch_ = curl_init();

	curl_setopt($ch_, CURLOPT_URL, $url_ . $params_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch_, CURLOPT_HEADER, FALSE);
	curl_setopt($ch_, CURLOPT_CUSTOMREQUEST, 'GET');
	$response_ = curl_exec($ch_);
	curl_close($ch_);

	$tmp_results_ =  json_decode($response_, true);
	$tmp_results_ =  $tmp_results_['movieListResult']['movieList'];

	$results_ = array();

	foreach($tmp_results_ as $row_data_){
		$results_[] = array(
			'kofic_code' => $row_data_["movieCd"],
			'movie_name' => $row_data_["movieNm"],
			'open_date' => $row_data_["openDt"],
			'genre' => $row_data_["genreAlt"],
			'country' => $row_data_["repNationNm"],
			// 'company_name' => $row_data_["companyNm"],
		);
	}

	return $results_;
}

function pb_movie_load_openapi($kofic_code_){
	$url_ = "http://www.kobis.or.kr/kobisopenapi/webservice/rest/movie/searchMovieInfo.json";


	$params_ = "?key=".pb_settings('moive_openapi_key');
	$params_ .= "&movieCd=".urlencode($kofic_code_);

	$ch_ = curl_init();

	curl_setopt($ch_, CURLOPT_URL, $url_ . $params_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch_, CURLOPT_HEADER, FALSE);
	curl_setopt($ch_, CURLOPT_CUSTOMREQUEST, 'GET');
	$response_ = curl_exec($ch_);
	curl_close($ch_);

	$tmp_results_ =  json_decode($response_, true);
	$tmp_results_ =  $tmp_results_['movieInfoResult']['movieInfo'];

	$country_array_ = $tmp_results_["nations"];
	$country_str_ = "";

	foreach($country_array_ as $data_){
		if(strlen($country_str_)) $country_str_ .= ",";
		$country_str_ .= $data_['nationNm'];
	}

	$genre_array_ = $tmp_results_["genres"];
	$genre_str_ = "";

	foreach($genre_array_ as $data_){
		if(strlen($genre_str_)) $genre_str_ .= ",";
		$genre_str_ .= $data_['genreNm'];
	}

	$main_actors_array_ = $tmp_results_["actors"];
	$main_actors_str_ = "";

	for($index_= 0; $index_< count($main_actors_array_) && $index_ < 3; ++$index_){
		$data_ = $main_actors_array_[$index_];

		if(strlen($main_actors_str_)) $main_actors_str_ .= ",";
		$main_actors_str_ .= $data_['peopleNm'];
	}


	$director_array_ = $tmp_results_["directors"];
	$director_str_ = "";

	foreach($director_array_ as $data_){
		if(strlen($director_str_)) $director_str_ .= ",";
		$director_str_ .= $data_['peopleNm'];
	}

	$level_str_ = $tmp_results_['audits'][0]['watchGradeNm'];
	$level_ = null;

	switch($level_str_){
		case "12세이상관람가" : 
			$level_ = '00001';
		break;
		case "15세이상관람가" : 
			$level_ = '00003';
		break;
		case "18세이상관람가" : 
			$level_ = '00005';
		break;
		default :
			$level_ = '00007';
		break;

	}

	$results_ = array(
		'kofic_code' => $tmp_results_["movieCd"],
		'movie_name' => $tmp_results_["movieNm"],
		'open_date' => $tmp_results_["openDt"],
		'running_time' => $tmp_results_["showTm"],
		'genre' => $genre_str_,
		'country' => $country_str_,
		'main_actors' => $main_actors_str_,
		'director' => $director_str_,
		'level' => $level_,
	);


	return $results_;
}

function pb_movie_draw_level_badge($level_code_, $size_ = "normal"){

	$class_name_ = "all";

	switch($level_code_){
		case '00001' :
			$class_name_ = "12";
			break;
		case '00003' :
			$class_name_ = "15";
			break;
		case '00005' :
			$class_name_ = "19";
			break;
		case '00007' :
		default  : break;
	}


	?>
	<span class="movie-rate-badge movie-rate-badge-<?=$class_name_?> <?=$size_?>"></span>
	<?php
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-settings.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-open.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen.php');
// include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-xticket.php'); //xticket 임시바인딩

?>