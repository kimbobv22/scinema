<?php
/*
 작성일자 : 2019.03.12
 작성자 : 나하나
 설명 : 현재상영작,상영예정작 정보 가져오기 (디트릭스 api call)
*/

//영화 리스트 (현재상영작)
function pb_dtryx_movie_now_list($dcinema_id_,$imgsize_cd_,$kind_){
	$api_url_ = "https://api.dtryx.com:30443/dtryx/cms/thirdparty/movie/third-party-type2-movie-now";

	$params_ = "?BrandCd=scinema";
	$params_ .= "&CinemaCd=".$dcinema_id_;
	$params_ .= "&ImgSize=".$imgsize_cd_;
	$params_ .= "&ChannelCd=homepage";
	$params_ .= "&WorkGuID=AF3747C8-628C-487E-BC1A-C00AC1A92C6D";
	$params_ .= "&EngVerYn=N";

	$ch_ = curl_init();
	curl_setopt($ch_, CURLOPT_URL, $api_url_ . $params_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch_, CURLOPT_HEADER, FALSE);
	curl_setopt($ch_, CURLOPT_CUSTOMREQUEST, 'GET');
	$response_ = curl_exec($ch_);
	curl_close($ch_);

	//echo $response_;
	$tmp_results_ =  json_decode($response_, true);
	if(!isset($tmp_results_['RecordTotalCount'])) return array();
	else $tmp_results_ =  $tmp_results_['Recordset'];

	if($kind_ === 'count') $result_ = $tmp_results_['RecordTotalCount'];
	else {
		$results_ = array();
		foreach($tmp_results_ as $row_data_){
		//echo $row_data_["Url"];
			$results_[] = array(
				'ID' => $row_data_["MovieCd"],
				'movie_name' => $row_data_["MovieNm"],
				'level' => dtryx_level_convert($row_data_["Rating"]),
				'image_url' => $row_data_["Url"],
				'open_date' => $row_data_["ReleaseDT"],
			);
		}
	}
	return $results_;
}

// 영화리스트(상영예정작)
function pb_dtryx_movie_plan_list($dcinema_id_,$imgsize_cd_,$kind_){
	$api_url_ = "https://api.dtryx.com:30443/dtryx/cms/thirdparty/movie/third-party-type2-movie-plan";

	$params_ = "?BrandCd=scinema";
	$params_ .= "&CinemaCd=".$dcinema_id_;
	$params_ .= "&ImgSize=".$imgsize_cd_;
	$params_ .= "&ChannelCd=homepage";
	$params_ .= "&WorkGuID=AF3747C8-628C-487E-BC1A-C00AC1A92C6D";
	$params_ .= "&EngVerYn=N";

	$ch_ = curl_init();
	curl_setopt($ch_, CURLOPT_URL, $api_url_ . $params_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch_, CURLOPT_HEADER, FALSE);
	curl_setopt($ch_, CURLOPT_CUSTOMREQUEST, 'GET');
	$response_ = curl_exec($ch_);
	curl_close($ch_);

	//echo $response_;
	$tmp_results_ =  json_decode($response_, true);
	if(!isset($tmp_results_['RecordTotalCount'])) return array();
	else $tmp_results_ =  $tmp_results_['Recordset'];

	if($kind_ === 'count') $result_ = $tmp_results_['RecordTotalCount'];
	else {
		$results_ = array();
		foreach($tmp_results_ as $row_data_){
		//echo $row_data_["Url"];
			$results_[] = array(
				'ID' => $row_data_["MovieCd"],
				'movie_name' => $row_data_["MovieNm"],
				'level' => dtryx_level_convert($row_data_["Rating"]),
				'image_url' => $row_data_["Url"],
				'open_date' => $row_data_["ReleaseDT"],
			);
		}
	}
	return $results_;
}

// 몇세이상 관람작 관련 기존 css 적용으로 인해 기존과 동일하게 코드 맞춰줌
function dtryx_level_convert($level_str_) {
	switch($level_str_){
		case "12" : 
			$level_ = '00001';
		break;
		case "15" : 
			$level_ = '00003';
		break;
		case "18" : 
			$level_ = '00005';
		break;
		default :
			$level_ = '00007';
		break;
	}
	return $level_;
}

// 단일 영화정보 가져오기
function pb_dtryx_movie_detail($dcinema_id_,$movie_id_){
	$api_url_ = "https://api.dtryx.com:30443/dtryx/cms/thirdparty/movie/third-party-type2-movie-detail";

	$params_ = "?BrandCd=scinema";
	$params_ .= "&CinemaCd=".$dcinema_id_;
	$params_ .= "&MovieCd=".$movie_id_;
	$params_ .= "&ImgSize=small";
	$params_ .= "&ChannelCd=homepage";
	$params_ .= "&WorkGuID=AF3747C8-628C-487E-BC1A-C00AC1A92C6D";
	$params_ .= "&EngVerYn=N";

	$ch_ = curl_init();
	curl_setopt($ch_, CURLOPT_URL, $api_url_ . $params_);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch_, CURLOPT_HEADER, FALSE);
	curl_setopt($ch_, CURLOPT_CUSTOMREQUEST, 'GET');
	$response_ = curl_exec($ch_);
	curl_close($ch_);

	//echo $response_;

	$tmp_results_ =  json_decode($response_, true);
	if(!isset($tmp_results_['RecordTotalCount'])) return array();
	else $tmp_results_ =  $tmp_results_['Recordset'];

		foreach($tmp_results_ as $row_data_){
			//echo $row_data_["MovieCd"];
			$results_ = array(
			'ID' => $row_data_["MovieCd"],
			'movie_name' => $row_data_["MovieNm"],
			'level' => dtryx_level_convert($row_data_["Rating"]),
			'image_url' => $row_data_["Url"],
			'open_date' => $row_data_["ReleaseDT"],
			'director' => $row_data_['DirectorNm'],
			'main_actors' => $row_data_['ActorNm'],
			'genre' => $row_data_['GenreNm'],
			'running_time' => $row_data_['RunningTime'],
			'story' => $row_data_['SynopsisNm'],
			);
		}
	return $results_;
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-dtryx-view.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-dtryx-ajax.php');
?>