<?php

function pb_movie_draw_screen_timetable($options_ = array()){

	$options_ = array_merge(array(
		'id' => "pb-movie-screen-timetable",
		'target_date' => null,
		'open_id' => null,
		'screen_id' => null,
		'area_id' => null,
	), $options_);

	$cinema_area_list_ = pb_gcode_dtl_list("C0005");
?>

<div class="movie-screen-timetable" id="<?=$options_['id']?>" 
	<?php if(strlen($options_["open_id"])){ ?>
	 data-default-open-id="<?=$options_["open_id"]?>"
	<?php }else if(strlen($options_["screen_id"])){ ?>
	 data-default-screen-id="<?=$options_["screen_id"]?>"
	<?php } ?>

	<?php if(strlen($options_["area_id"])){ ?>
	 data-default-area-id="<?=$options_["area_id"]?>"
	<?php } ?>

	<?php if(strlen($options_["target_date"])){ ?>
	 data-default-target-date="<?=$options_["target_date"]?>"
	<?php } ?>
	>
	<div class="movie-box-fix">
		<div class="days-row">
			<div class="movie-days-slider ">
				<div class="swiper-wrapper"></div>
				<div class="slider-button-prev"><i class="fa fa-chevron-left"></i></div>
				<div class="slider-button-next"><i class="fa fa-chevron-right"></i></div>
			</div>
		</div>
	</div>
	<div class="cinema-area-row <?=strlen($options_['screen_id']) ? "hidden" : ""?>">
		<div class="cinema-area-list">
			<a href="#" data-area-id="all" class="area-item <?=!strlen($options_['area_id']) ? "active" : ""?>" >전체</a>
			<?php foreach($cinema_area_list_ as $row_data_){ ?>
				<a href="#" data-area-id="<?=$row_data_->code_did?>" class="area-item <?=$row_data_->code_did === $options_['area_id'] ? "active" : ""?>" ><?=$row_data_->code_dnm?></a>
		<?php } ?>
		</div>
	</div>

	<div class="timetable-list-row">
		<div class="timetable-list"></div>

	</div>
	
</div>

<?php
}


?>