<?php
/*
 작성일자 : 2019.03.12
 작성자 : 나하나
 설명 : 디트릭스 데이터를 홈화면 리스트 그리기
*/

// 홈화면 영화정보 그리기 
function pb_dtryx_movie_draw_light($arr_data_){
    foreach($arr_data_ as $row_data_){
?>
        <div class="swiper-slide" >
            <div class="pb-movie-item light">
				<a href="<?=pb_movie_view_url($row_data_["ID"])?>" class="poster-image">
                    <img src="<?=$row_data_["image_url"]?>" class="image">
                </a>
              	<div class="movie-info">
                    <span class="movie-name">
                    <?php pb_movie_draw_level_badge($row_data_["level"]); ?>
                    <?=$row_data_["movie_name"]?></span>
                </div>
            </div>
        </div>
<?php
    }
}
function pb_dtryx_movie_draw_dark($arr_data_){
    foreach($arr_data_ as $row_data_){
?>
    <div class="swiper-slide" >
        <div class="pb-movie-item dark">
            <a href="<?=pb_movie_view_url($row_data_["ID"])?>" class="poster-image">
                <img src="<?=$row_data_["image_url"]?>" class="image">
            </a>
            <div class="movie-info">
                <span class="movie-name">
                <?php pb_movie_draw_level_badge($row_data_["level"]); ?>
                <?=$row_data_["movie_name"]?></span>
            </div>
        </div>
    </div>
<?php
    }                     
}

// 상영시간표 
function pb_dtryx_draw_timetable(){
?>
<!--예매리스트 출력부분//-->
  <div class="movie-cal">
    <div class="hd">
      <div class="day-cnt">
        <div class="swiper-wrapper" id='DTRYX-SCINEMA-PLAYDATE'>
				</div>
				<div id='DTRYX-SCINAMA-NABI'>
        	<a href="#none" class="prev"><span>이전</span></a>
					<a href="#none" class="next"><span>다음</span></a> 
				</div>
      </div>
    </div>

    <div class="loc-tab" id='DTRYX-SCINEMA-REGION'>
    </div>

    <div class="con" id='DTRYX-SCINEMA-TIMETABLE'>
    </div>
    <p class="cmt">*예매/취소 가능시간은 상영시작 20분전 까지</p>
  </div>
<!--디트릭스 예매 Layer 출력부분//-->
  <div class="bPopup pop-if" id="scinemaPopIf">
  	<div class="content"></div>
    <button class="b-closer bt-closeM" onClick="callBpopClose2('#scinemaPopIf');" style="border: 0 none;background: none;cursor: pointer;"></button>
  </div>
<!--비회원 예매정보 받는 form 부분//-->
  <div class="bPopup" id="popReg">
		<h3 class="tit">예매정보 입력</h3>
		<div class="reg-con">
			<div class="rowc">
				<h4>개인정보활용동의</h4>
				<div class="desc01">
					작은영화관 예매서비스 제공 위해 필요한 최소한의 개인정보이므로 입력(수집)에 동의하지 않을 경우 서비스를 이용하실 수 없습니다.
				</div>
				<table>
				<tbody>
				<tr>
					<th>수집목적</th>
					<td>이용자 식별 및 본인 여부 확인</td>
				</tr>
				<tr>
					<th>제3자제공</th>
					<td>디트릭스</td>
				</tr>
				<tr>
					<th>수집항목</th>
					<td>이름, 연락처, 생년월일</td>
				</tr>
				</tbody>
				</table>
				<div class="scrollbx">
				개인정보의 수집목적 및 항목<br><br>
				① 개인정보 수집 목적: 비회원 예매 및 예매확인<br>
				② 수집항목: 이름, 휴대폰번호, 생년월일, 비밀번호<br><br>
				개인정보의 보유 및 이용기간<br><br>
				개인정보는 영화 예매 완료 후 동의일로부터 90일까지 위 이용 목적으로 이용 및 보관 됩니다.<br>다만, 상법 등 관련법령의 규정에 의하여 거래 관련 관리 의무 관계의 확인 등을 이유로 일정기간 보유하여야 할 필요가 있을 경우 아래와 같이 보유합니다.<br><br>
				<b>대금결제 및 재화 등의 공급에 관한 기록 : 5년</b><br><br>
				※ 비회원 예매서비스 제공을 위한 최소한의 개인정보이며 거부할 수 있습니다. 다만, 수집에 동의하지 않을 경우 서비스 이용이 제한됩니다.<br>
				</div>
				<div class="chk">
					<label><input type="checkbox" name="AgreeChk"> 개인정보수집에 동의합니다.</label>
				</div>
			</div>
			<div class="rowc">
				<h4>개인정보입력</h4>
				<div class="desc01">
					예매내역 확인 및 취소 그리고 티켓 발권에 지장없도록 잘 확인하시어 정확히 입력해주시기 바랍니다.
				</div>
				<div class="reg-wrap">
					<div class="reg-row">
						<div class="stit">이름</div>
						<div class="scon">
							<input type="text" class="input" name="MemberNm" placeholder="이름">
						</div>
					</div>
					<div class="reg-row">
						<div class="stit">휴대전화</div>
						<div class="scon hp">
							<select class="select" name="MobileNo01">
								<option value="">앞자리</option>
								<option value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="019">019</option>
							</select>
							<input type="text" class="input" name="MobileNo02" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
							<input type="text" class="input" name="MobileNo03" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
					</div>
          <div class="reg-row">
						<div class="stit">생년월일</div>
						<div class="scon">
							<input type="text" class="input" maxlength="6" name="BirthdayDT" placeholder="생년월일 6자리" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
					</div>
          <div class="reg-row">
						<div class="stit">비밀번호</div>
						<div class="scon">
							<input type="password" class="input" maxlength="4" name="CertiNo" placeholder="비밀번호 4자리" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
					</div>
				</div>
				<div class="btn-gup">
					<a href="#" class="cancel b-close">취소하기</a>
					<a href="#" class="ok" onClick="DTRYXNonMemberBooking()">예매하기</a>
				</div>
      </div>
		</div>
		<a href="#none" class="bt-close b-close"><i></i><span>닫기</span></a>
    </div>
<?php
}

//예매확인
function pb_dtryx_drew_reservation()
{
	$current_cinema_ = pb_current_cinema();
	$cinema_id_ = $current_cinema_->xticket_ref_code;
		if(is_user_logged_in()){
			$user_data_ = pb_user(get_current_user_id());
			$s_user_id_ = $user_data_->user_login;
			$s_user_name_ = $user_data_->full_name;
			$s_phone_num_ = $user_data_->phone1_1.$user_data_->phone1_2.$user_data_->phone1_3;
			$s_user_birthday_ = $user_data_->birth_yyyymmdd;
			$s_user_birthday_ = substr($s_user_birthday_,2,8);
			$s_user_idx_ = $user_data_->ID;
		}
		else {
			# code...
			$s_user_id_ = "";
			$s_user_name_ = "";
			$s_phone_num_ = "";
			$s_user_birthday_ = "";
			$s_user_idx_ = "";
		}
?>
<div id="dtryx-check-popup">
	<!--디트릭스 예매 Layer 출력부분//-->
  <div class="bPopup pop-if">
   	<div class="content"></div>
    <button class="b-closer bt-closeM" onClick="callBpopClose2('.pop-if');" style="border: 0 none;background: none;cursor: pointer;"></button>
  </div>
<!--비회원 예매정보 받는 form 부분//-->
  <div class="bPopup" id="popRegR">
		<h3 class="tit">예매정보 입력</h3>
		<div class="reg-con">
			<div class="rowc">
				<h4>개인정보활용동의</h4>
				<div class="desc01">
					작은영화관 예매서비스 제공 위해 필요한 최소한의 개인정보이므로 입력(수집)에 동의하지 않을 경우 서비스를 이용하실 수 없습니다.
				</div>
				<table>
				<tbody>
				<tr>
					<th>수집목적</th>
					<td>이용자 식별 및 본인 여부 확인</td>
				</tr>
				<tr>
					<th>제3자제공</th>
					<td>디트릭스</td>
				</tr>
				<tr>
					<th>수집항목</th>
					<td>이름, 연락처, 생년월일</td>
				</tr>
				</tbody>
				</table>
				<div class="scrollbx">
				개인정보의 수집목적 및 항목<br><br>
				① 개인정보 수집 목적: 비회원 예매 및 예매확인<br>
				② 수집항목: 이름, 휴대폰번호, 생년월일, 비밀번호<br><br>
				개인정보의 보유 및 이용기간<br><br>
				개인정보는 영화 예매 완료 후 동의일로부터 90일까지 위 이용 목적으로 이용 및 보관 됩니다.<br>다만, 상법 등 관련법령의 규정에 의하여 거래 관련 관리 의무 관계의 확인 등을 이유로 일정기간 보유하여야 할 필요가 있을 경우 아래와 같이 보유합니다.<br><br>
				<b>대금결제 및 재화 등의 공급에 관한 기록 : 5년</b><br><br>
				※ 비회원 예매서비스 제공을 위한 최소한의 개인정보이며 거부할 수 있습니다. 다만, 수집에 동의하지 않을 경우 서비스 이용이 제한됩니다.<br>
				</div>
				<div class="chk">
					<label><input type="checkbox" name="AgreeChk"> 개인정보수집에 동의합니다.</label>
				</div>
			</div>
			<div class="rowc">
				<h4>개인정보입력</h4>
				<div class="desc01">
					예매내역 확인 및 취소 그리고 티켓 발권에 지장없도록 잘 확인하시어 정확히 입력해주시기 바랍니다.
				</div>
				<div class="reg-wrap">
					<div class="reg-row">
						<div class="stit">이름</div>
						<div class="scon">
							<input type="text" class="input" name="MemberNm" placeholder="이름">
						</div>
					</div>
					<div class="reg-row">
						<div class="stit">휴대전화</div>
						<div class="scon hp">
							<select class="select" name="MobileNo01">
								<option value="">앞자리</option>
								<option value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="019">019</option>
							</select>
							<input type="text" class="input" name="MobileNo02" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
							<input type="text" class="input" name="MobileNo03" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
					</div>
          <div class="reg-row">
						<div class="stit">생년월일</div>
						<div class="scon">
							<input type="text" class="input" maxlength="6" name="BirthdayDT" placeholder="생년월일 6자리" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
					</div>
          <div class="reg-row">
						<div class="stit">비밀번호</div>
						<div class="scon">
							<input type="password" class="input" maxlength="4" name="CertiNo" placeholder="비밀번호 4자리" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
					</div>
				</div>
				<div class="btn-gup">
					<a href="#" class="cancel b-close">취소하기</a>
					<a href="#" class="ok" onClick="DTRYXNonMemberReservation()">예매확인</a>
				</div>
      </div>
		</div>
		<a href="#none" class="bt-close b-close"><i></i><span>닫기</span></a>
    </div>
<script>
/*예매내역 세팅부분*/
  DTRYX.Common.CinemaCd 	= '<?=$cinema_id_?>';
  DTRYX.Common.MemberNm     = '<?=$s_user_name_?>';       /*회원이름*/
  DTRYX.Common.MobileNoFull = '<?=$s_phone_num_?>';       /*회원연락처*/
  DTRYX.Common.BirthdayDT   = '<?=$s_user_birthday_?>';   /*회원생년월일*/
  DTRYX.Common.MemberGuID   = '<?=$s_user_id_?>';         /*회원고유아이디*/
  DTRYX.Common.PublicID     = '<?=$s_user_idx_?>';		    /*IPIN/CI값 없을 경우 공백*/
</script>
</div>
<?php	
}
?>