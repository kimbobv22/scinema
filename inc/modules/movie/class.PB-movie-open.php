<?php


//개봉작품내역 가져오기
function pb_movie_open_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
				 {$wpdb->pbmovie_open}.ID ID
				,{$wpdb->pbmovie_open}.movie_name movie_name
				,{$wpdb->pbmovie_open}.kofic_code kofic_code
				
				,{$wpdb->pbmovie_open}.cinema_id cinema_id
				,{$wpdb->pbcinema}.cinema_name cinema_name
				
				,{$wpdb->pbmovie_open}.open_date open_date
				,DATE_FORMAT({$wpdb->pbmovie_open}.open_date, '%Y.%m.%d') open_date_ymd
				,DATE_FORMAT({$wpdb->pbmovie_open}.open_date, '%Y.%m.%d %H:%i') open_date_ymdhi
				
				,{$wpdb->pbmovie_open}.level level
				,".pb_query_gcode_dtl_name("MO001", "{$wpdb->pbmovie_open}.level")." level_name
				
				,{$wpdb->pbmovie_open}.genre genre
				,{$wpdb->pbmovie_open}.3d_yn 3d_yn
				,{$wpdb->pbmovie_open}.director director
				,{$wpdb->pbmovie_open}.main_actors main_actors
				,{$wpdb->pbmovie_open}.running_time running_time
				,{$wpdb->pbmovie_open}.country country
				,{$wpdb->pbmovie_open}.image_url image_url
				,{$wpdb->pbmovie_open}.story story
				
				,{$wpdb->pbmovie_open}.status status
				,".pb_query_gcode_dtl_name("MO003", "{$wpdb->pbmovie_open}.status")." status_name

				,IF({$wpdb->pbmovie_open}.open_date <= NOW(), 'Y', 'N') on_screening_yn
				,IF({$wpdb->pbmovie_open}.open_date > NOW(), 'Y', 'N') on_scheduled_yn

				,{$wpdb->pbmovie_open}.publisher_id publisher_id
				,{$wpdb->pbclient}.client_name publisher_name
				,{$wpdb->pbmovie_open}.publisher_charge publisher_charge
				,{$wpdb->pbmovie_open}.cinema_charge cinema_charge

				,{$wpdb->pbmovie_open}.reg_date reg_date
				,DATE_FORMAT({$wpdb->pbmovie_open}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

				,{$wpdb->pbmovie_open}.mod_date mod_date
				,DATE_FORMAT({$wpdb->pbmovie_open}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

				".apply_filters("pb_movie_open_list_fields", "", $conditions_)."

				
		     FROM {$wpdb->pbmovie_open}

		     LEFT OUTER JOIN {$wpdb->pbclient}
		     ON   {$wpdb->pbclient}.client_id = {$wpdb->pbmovie_open}.publisher_id

		     LEFT OUTER JOIN {$wpdb->pbcinema}
		     ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbmovie_open}.cinema_id

		      ";

	$query_ .= apply_filters('pb_movie_open_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbmovie_open}.ID")." ";
	}

	if(isset($conditions_['status'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['status'], "{$wpdb->pbmovie_open}.status")." ";
	}
	if(isset($conditions_['on_screening'])){
		$query_ .= " AND {$wpdb->pbmovie_open}.open_date <= NOW() ";
	}
	if(isset($conditions_['on_scheduled'])){
		$query_ .= " AND {$wpdb->pbmovie_open}.open_date > NOW() ";
	}

	if(isset($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbmovie_open}.cinema_id")." ";
	}

	if(isset($conditions_['kofic_code'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['kofic_code'], "{$wpdb->pbmovie_open}.kofic_code")." ";
	}

	if(isset($conditions_['publisher_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['publisher_id'], "{$wpdb->pbmovie_open}.publisher_id")." ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_movie_open_list_keyword', array(
			"{$wpdb->pbmovie_open}.movie_name",
			"{$wpdb->pbcinema}.cinema_name",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}

	if(isset($conditions_['movie_name']) && strlen($conditions_['movie_name'])){
		$query_ .= " AND {$wpdb->pbmovie_open}.movie_name like '%".$conditions_['movie_name']."%' ";
	}

	if(isset($conditions_['open_srt_date']) && strlen($conditions_['open_srt_date'])){
		$query_ .= " AND {$wpdb->pbmovie_open}.open_date >= '".$conditions_['open_srt_date']."' ";
	}
	if(isset($conditions_['open_end_date']) && strlen($conditions_['open_end_date'])){
		$query_ .= " AND {$wpdb->pbmovie_open}.open_date <= '".$conditions_['open_end_date']."' ";
	}
	
	$query_ .= apply_filters('pb_movie_open_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_movie_open_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}
	
	return apply_filters('pb_movie_open_list', $wpdb->get_results($query_), $conditions_);	
}

//개봉작품 단인내역 가져오기
function pb_movie_open_data($movie_open_id_){
	if(!strlen($movie_open_id_)) return null;
	$result_ = pb_movie_open_list(array("ID" => $movie_open_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_movie_open_data_fields($data_){
	return pb_format_mapping(apply_filters("pb_movie_open_data_fields_map",array(
	
		'movie_name' => '%s',
		'kofic_code' => '%s',
		'open_date' => '%s',
		'level' => '%s',
		'genre' => '%s',
		'3d_yn' => '%s',
		'director' => '%s',
		'main_actors' => '%s',
		'running_time' => '%d',
		'country' => '%s',
		'image_url' => '%s',
		'story' => '%s',
		'status' => '%s',
		'publisher_id' => '%s',
		'publisher_charge' => '%s',
		'cinema_charge' => '%s',
		'cinema_id' => '%d',

	)), $data_);
}

//개봉작품 추가
function pb_movie_open_data_add($movie_open_data_){
	$insert_data_ = _pb_parse_movie_open_data_fields($movie_open_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbmovie_open}", $insert_value_, $insert_format_);

	if(!$result_) return $result_;

	$movie_open_id_ = $wpdb->insert_id;

	do_action('pb_movie_open_added', $movie_open_id_);

	return $movie_open_id_;
}

//개메 수정
function pb_movie_open_data_update($movie_open_id_, $movie_open_data_){
	$movie_open_data_ = _pb_parse_movie_open_data_fields($movie_open_data_);

	$update_value_ = $movie_open_data_['data'];
	$update_format_ = $movie_open_data_['format'];

	$update_value_['mod_date'] = current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbmovie_open}", $update_value_, array("ID" => $movie_open_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_movie_open_updated', $movie_open_id_);

	return $result_;
}

//개봉작품 삭제
function pb_movie_open_data_remove($movie_open_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbmovie_open}", array("ID" => $movie_open_id_), array("%d"));

	if(!$result_) return $result_;

	do_action('pb_movie_open_removed', $movie_open_id_);
}


include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-open-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-open-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-open-adminpage.php');


?>