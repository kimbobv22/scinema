<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();

	$current_cinema_data_ = pb_cinema($blog_id_);

	if($blog_id_ !== PB_CINEMA_HEAD_OFFICE_ID && $current_cinema_data_->cinema_type !== PB_CINEMA_CTG_IND){
		return $results_;
	}

	$results_['movie-open-manage'] = array(
		'title' => '개봉기준작품관리' ,
		'template-data' => array(

			'data' => array(
				'total-count' => function(){

					$blog_id_ = get_current_blog_id();
					$current_cinema_data_ = pb_cinema($blog_id_);
					$cinema_id_ = null;

					if($current_cinema_data_->cinema_type === PB_CINEMA_CTG_IND){
						$cinema_id_ = $current_cinema_data_->cinema_id;
					}else{
						$cinema_id_ = PB_CINEMA_HEAD_OFFICE_ID;
					}

					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					$srt_date_ = isset($_GET["srt_date"]) ? $_GET["srt_date"] : date("Y-m-d", strtotime("-60 days"));
					$end_date_ = isset($_GET["end_date"]) ? $_GET["end_date"] : date("Y-m-d", strtotime("+60 days"));
					return pb_movie_open_list(array(
						'cinema_id' => $cinema_id_,
						'keyword' => $keyword_,
						'open_srt_date' => $srt_date_,
						'open_end_date' => $end_date_,
						'just_count' => true,
					));
				},
				'list' => function($offset_, $limit_){

					$blog_id_ = get_current_blog_id();
					$current_cinema_data_ = pb_cinema($blog_id_);
					$cinema_id_ = null;

					if($current_cinema_data_->cinema_type === PB_CINEMA_CTG_IND){
						$cinema_id_ = $current_cinema_data_->cinema_id;
					}else{
						$cinema_id_ = PB_CINEMA_HEAD_OFFICE_ID;
					}

					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					$srt_date_ = isset($_GET["srt_date"]) ? $_GET["srt_date"] : date("Y-m-d", strtotime("-60 days"));
					$end_date_ = isset($_GET["end_date"]) ? $_GET["end_date"] : date("Y-m-d", strtotime("+60 days"));
					return pb_movie_open_list(array(
						'cinema_id' => $cinema_id_,
						'keyword' => $keyword_,
						'open_srt_date' => $srt_date_,
						'open_end_date' => $end_date_,
						'limit' => array($offset_, $limit_),
						'orderby' => ' ORDER BY open_date DESC '
					));
				},
				'single' => function($action_data_){
					return pb_movie_open_data($action_data_);
				},
				'empty' => function(){
					return array(
						'cinema_id' => get_current_blog_id(),
						'status' => '00001',
					);
				},
				
			),

			'options' => array(
				'list-options' => array(

					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 개봉작품이 없습니다.',
						// 'add' => '개봉작품등록',
					),
					'custom-search-frame' => function($options_, $table_options_){

						$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
						$srt_date_ = isset($_GET["srt_date"]) ? $_GET["srt_date"] : date("Y-m-d", strtotime("-60 days"));
						$end_date_ = isset($_GET["end_date"]) ? $_GET["end_date"] : date("Y-m-d", strtotime("+60 days"));

						global $pb_adminpage_menu_id;
						?>

<div class="search-frame minus-margin-top">
	
	<div class="row">
		
			<div class="col-xs-12 col-sm-2">
				<label>&nbsp; &nbsp;&nbsp;</label><br/>
				<a href="<?=pb_adminpage_url($pb_adminpage_menu_id.'/add')?>" class="btn btn-default btn-sm">개봉작품등록</a>
				<div class="form-margin-xs visible-xs"></div>
			</div>
			<div class="col-xs-6 col-sm-2">
				<label>개봉시작일자</label>
				<div class='input-group date input-group-sm' id='movie-open-srt-date'>
	                <input type='text' class="form-control" name="srt_date" value="<?=$srt_date_?>" />
	                <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                </span>
	            </div>

			</div>
			<div class="col-xs-6 col-sm-2">
				<label>개봉종료일자</label>
				<div class='input-group date input-group-sm' id='movie-open-end-date'>
	                <input type='text' class="form-control" name="end_date" value="<?=$end_date_?>" />
	                <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                </span>
	            </div>
	            <div class="form-margin-xs visible-xs"></div>
			</div>
	
			<div class="col-xs-8 col-sm-3">
				<label>검색어</label>
				<input type="text" class="form-control input-sm" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
			</div>
			<div class="col-xs-4 col-sm-1">
				<label>&nbsp; &nbsp;&nbsp;</label><br/>
				<!-- <div class="form-margin-xs visible-xs"></div> -->
				<button class="btn btn-default btn-sm btn-block" type="submit">검색</button>
			</div>
			<div class="col-xs-4 col-sm-2">
				<label>&nbsp; &nbsp;&nbsp;</label><br/>
				<!-- <div class="form-margin-xs visible-xs"></div> -->
				<a href="javascript:pb_movie_open_resize_poster_images();">포스터일괄리사이즈</a>
			</div>
		
	</div>
	
</div>
<hr>

						<?php
					}
				),
				'edit-options' => array(
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => '개봉작품추가확인',
						'add-confirm-message' => '해당 개봉작품를 추가하시겠습니까?',
						'add-success-title' => '추가완료',
						'add-success-message' => '개봉작품가 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => '개봉작품 추가 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => '개봉작품정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => '개봉작품 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '개봉작품삭제확인',
						'remove-confirm-message' => '해당 개봉작품를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => '개봉작품정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => '개봉작품 삭제 중 에러가 발생하였습니다.',
					),
				),
				'id-format' => 'pb-movie-open-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){
					$result_ = pb_movie_open_data_update($target_data_['ID'], $target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '개봉작품 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['ID'];
				},
				'add' => function($target_data_){
					$result_ = pb_movie_open_data_add($target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '개봉작품 추가 중 에러가 발생했습니다.');
					}


					return $result_;
				},
				'remove' => function($target_data_){
					pb_movie_open_data_remove($target_data_['ID']);
					return true;
				},
			),
			'templates' => array(
				'list' => function(){
					return array(
						'seq' => array(
							'column-label' => '',
							'th-class' => "col-seq text-center ",
							'td-class' => "col-seq text-center ",
							'td-render-func' => function($item_, $column_name_){
								global $_temp_movie_open_index;
								if(empty($_temp_movie_open_index)){
									$_temp_movie_open_index = 1;
								}

								echo $_temp_movie_open_index;


								++$_temp_movie_open_index;
							},
						),
						'open_date_ymd' => array(
							'column-label' => '개봉일자',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
						),
						'movie_name' => array(
							'column-label' => '개봉작품명',
							'th-class' => "col-3",
							'td-class' => "col-3",
							'td-render-func' => function($item_, $column_name_){
								?>

								<a href="<?=pb_adminpage_url("movie-open-manage/edit/".$item_->ID)?>"><?=$item_->movie_name?></a>

								<?php
							},
						),
						'level_name' => array(
							'column-label' => '등급',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
						),
						'3d_yn' => array(
							'column-label' => '3D',
							'th-class' => "col-1 text-center hidden-xs hidden-sm",
							'td-class' => "col-1 text-center hidden-xs hidden-sm",
						),
						'status_name' => array(
							'column-label' => '등록상태',
							'th-class' => "col-1 text-center",
							'td-class' => "col-1 text-center",
							
						),
						'publisher_name' => array(
							'column-label' => '배급사',
							'th-class' => "col-3 text-center hidden-xs",
							'td-class' => "col-3 text-center hidden-xs",
							
						),
						
					);
				},
				'edit' => function(){

					global $pb_adminpage_action_data;


					$publisher_list_ = array();

					$blog_id_ = get_current_blog_id();
					$current_cinema_data_ = pb_cinema($blog_id_);
					$cinema_id_ = null;

					if($current_cinema_data_->cinema_type === PB_CINEMA_CTG_IND){
						$temp_publisher_list_ = pb_client_list(array(
							'client_status' => '00001',
							'client_ctg' => PB_CLIENT_CTG_OUTER_PUBL,
							'cinema_id' => $current_cinema_data_->cinema_id,
						));
						foreach($temp_publisher_list_ as $row_data_){
							$publisher_list_[$row_data_->client_id] = $row_data_->client_name;
						}
					}else{
						$temp_publisher_list_ = pb_client_list(array(
							'client_status' => '00001',
							'client_ctg' => PB_CLIENT_CTG_OUTER_PUBL,
							'cinema_id' => $current_cinema_data_->cinema_id,
							// 'orderby' => 'order by client_name asc',
						));

						$temp_publisher_list2_ = pb_client_list(array(
							'client_status' => '00001',
							'client_ctg' => PB_CLIENT_CTG_OUTER_PUBL,
							'for_common' => true,

						));

						foreach($temp_publisher_list2_ as $row_data_){
							$publisher_list_[$row_data_->client_id] = $row_data_->client_name;
						}
						foreach($temp_publisher_list_ as $row_data_){
							$publisher_list_[$row_data_->client_id] = $row_data_->client_name;
						}
					}

						

					uasort($publisher_list_, function($a_, $b_){
						return $a_ > $b_;
					});


					return array(
						array(
							'type' => 'hidden',
							'column-name' => 'ID',
						),
						array(
							'type' => 'hidden',
							'column-name' => 'cinema_id',
						),
						
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-4',
									'type' => 'custom',
									'view' => '_pb_movie_open_mange_edit_section_kofic_code'
								),
								array(
									'column' => 'col-xs-12 col-sm-8',
									'column-label' => '개봉작품명',
									'column-name' => 'movie_name',
									'placeholder' => '작품명을 입력하세요',
									'validates' => array(
										'required' => "개봉작품구분를 선택하세요",
									),
									'type' => 'text',
								),
							),
						),
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '개봉작품상태',
									'column-name' => 'status',
									'placeholder' => '-개봉작품상태-',
									'validates' => array(
										'required' => "개봉작품상태를 선택하세요",
									),
									'type' => 'select-gcode',
									'code-id' => 'MO003'
								),
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '개봉예정일자',
									'column-name' => 'open_date',
									'placeholder' => '개봉예정일자 선택',
									'validates' => array(
										'required' => "개봉예정일자를 입력하세요",
									),
									'type' => 'datepicker',
								)
							),
						),
						array(
							'type' => 'division',
						),
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '등급',
									'column-name' => 'level',
									'placeholder' => '-등급 선택-',
									'validates' => array(
										'required' => "등급을 선택하세요",
									),
									'type' => 'select-gcode',
									'code-id' => 'MO001'
								),
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '장르',
									'column-name' => 'genre',
									'placeholder' => '장르 입력',
									'validates' => array(
										'required' => "장르를 입력하세요",
									),
									'type' => 'text',
								)
							),
						),
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '감독',
									'column-name' => 'director',
									'placeholder' => '감독 입력',
									'validates' => array(
										'required' => "감독을 입력하세요",
									),
									'type' => 'text',
								),
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '2D/3D',
									'column-name' => '3d_yn',
									'type' => 'select',
									'options' => array(
										'N' => '2D',
										'Y' => '3D',
									),
								),
							),
						),
						
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '러닝타임',
									'column-name' => 'running_time',
									'placeholder' => '러닝타임 입력',
									'validates' => array(
										'required' => "러닝타임을 입력하세요",
									),
									'type' => 'text',
									'help-block' => '숫자(분)로 입력하세요',
								),
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '제작국가',
									'column-name' => 'country',
									'placeholder' => '국가 입력',
									'validates' => array(
										'required' => "국가를 입력하세요",
									),
									'type' => 'text',
								)
							),
						),
						array(
							'column-label' => '주연',
							'column-name' => 'main_actors',
							'placeholder' => '주연 입력',
							'validates' => array(
								'required' => "주연을 입력하세요",
							),
							'help-block' => '쉽표로 구분하여 입력하세요',
							'type' => 'text',
						),
						array(
							'column-label' => '줄거리',
							'column-name' => 'story',
							'placeholder' => '줄거리 입력',
							'required' => true,
							'required-error-title' => "확인필요",
							'required-error-message' => "줄거리를 입력하세요",
							'type' => 'wp_editor',
						),

						array(
							'column-label' => '포스터',
							'column-name' => 'image_url',
							// 'required' => "포스터를 선택하세요",
							'type' => 'imagepicker',
							'maxlength' => 1,
							'maxwidth' => 768,
							'data-type' => "string",
						),

						array(
							'type' => 'division',
						),
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-8 col-sm-9',
									'column-label' => '배급사',
									'column-name' => 'publisher_id',
									'placeholder' => '-배급사 선택-',
									'validates' => array(
										'required' => "배급사를 선택하세요",
									),
									'type' => 'select',
									'options' => $publisher_list_,
								),
								array(
									'column' => 'col-xs-4 col-sm-3',
									'type' => 'custom',
									'view' => '_pb_movie_open_mange_edit_section_publisher_add_btn'
								)
							),
						),

							
						/*
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '배급사부금',
									'column-name' => 'publisher_charge',
									'placeholder' => '배급사부금 입력',
									'validates' => array(
										'required' => "배급사부금을 입력하세요",
									),
									'type' => 'number',
								),
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '극장부금',
									'column-name' => 'cinema_charge',
									'placeholder' => '극장부금 입력',
									'validates' => array(
										'required' => "극장부금을 입력하세요",
									),
									'type' => 'number',
								)
							),
						),
						*/
					);
				}
			),

		),
		'lvl' => 1,
		'sort_num' => 12,
	);

	return $results_;
});

function _pb_movie_open_mange_edit_section_kofic_code($attrs_ = array(), $options_ = array(), $data_ = null){
	?>

	<!-- <a href="javascript:_pb_movie_open_mange_edit_load_openapi();" class="btn btn-sm btn-default">KOFIC정보가져오기</a> -->

	<div class="form-group">
		<label for="pb-movie-open-edit-form-kofix-code">KOFIC 영화코드</label>
		<div class="input-group">
			<input type="text" class="form-control" placeholder="코드 입력" name="kofic_code" value="<?=isset($data_['kofic_code']) ? $data_['kofic_code'] : ""?>">
			<span class="input-group-btn">
			<button class="btn btn-default" type="button" onclick="pb_movie_open_manage_edit_find_kofic_code();">검색하기</button>
		  </span>
		</div>
		<div class="help-block with-errors"></div>
		<div class="clearfix"></div>
	</div>

	<?php
}

function _pb_movie_open_mange_edit_section_publisher_add_btn($attrs_ = array(), $options_ = array(), $data_ = null){
	?>

	<div class="form-group">
		<label>&nbsp;</label>
		<a href="javascript:_pb_movie_open_mange_open_publisher_add_popup();" class="btn btn-block btn-default">배급사등록</a>
		<div class="clearfix"></div>
	</div>

	<?php	
}

pb_add_ajax('movie-open-add-publisher', function(){
	if(!pb_is_admin()){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된접근',
			'error_message' => '접근권한이 없습니다.'
		));
		die();
	}

	$publisher_data_ = $_POST['publisher_data'];

	$result_ = pb_client_add($publisher_data_);
	if(!$result_){
		echo json_encode(array(
			'success' => false,
			'error_title' => '등록실패',
			'error_message' => '배급사 등록 중, 에러가 발생했습니다.'
		));
		die();	
	}

	$client_data_ = pb_client($result_);

	echo json_encode(array(
		'success' => true,
		'client_id' => $client_data_->client_id,
		'client_name' => $client_data_->client_name,
	));
	die();	
})

?>