<?php


//개봉작품내역 가져오기
function pb_movie_screen_list($conditions_ = array()){
	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 
				
				 {$wpdb->pbmovie_screen}.ID ID
				,{$wpdb->pbmovie_screen}.open_id open_id
				,{$wpdb->pbmovie_screen}.cinema_id cinema_id
				,{$wpdb->pbcinema}.cinema_name cinema_name
				,{$wpdb->pbcinema}.xticket_ref_code cinema_xticket_ref_code
				,{$wpdb->pbcinema}.logo_image_url cinema_logo_image_url

				,{$wpdb->pbmovie_screen}.screen_srt_date screen_srt_date
				,DATE_FORMAT({$wpdb->pbmovie_screen}.screen_srt_date, '%Y.%m.%d') screen_srt_date_ymd
				,DATE_FORMAT({$wpdb->pbmovie_screen}.screen_srt_date, '%Y.%m.%d %H:%i') screen_srt_date_ymdhi

				,{$wpdb->pbmovie_screen}.screen_end_date screen_end_date
				,DATE_FORMAT({$wpdb->pbmovie_screen}.screen_end_date, '%Y.%m.%d') screen_end_date_ymd
				,DATE_FORMAT({$wpdb->pbmovie_screen}.screen_end_date, '%Y.%m.%d %H:%i') screen_end_date_ymdhi

				,IF({$wpdb->pbmovie_screen}.screen_end_date_fg = 'Y', 'Y', 'N') screen_end_date_fg

				,{$wpdb->pbmovie_screen}.screen_close_date screen_close_date
				,DATE_FORMAT({$wpdb->pbmovie_screen}.screen_close_date, '%Y.%m.%d') screen_close_date_ymd
				,DATE_FORMAT({$wpdb->pbmovie_screen}.screen_close_date, '%Y.%m.%d %H:%i') screen_close_date_ymdhi
				
				,{$wpdb->pbmovie_screen}.ticket_charge ticket_charge
				,{$wpdb->pbmovie_screen}.publisher_charge publisher_charge
				,{$wpdb->pbmovie_screen}.cinema_charge cinema_charge

				,{$wpdb->pbmovie_screen}.xticket_ref_code xticket_ref_code

				
				,{$wpdb->pbmovie_screen}.status status
				,".pb_query_gcode_dtl_name("MS001", "{$wpdb->pbmovie_screen}.status")." status_name

				,IF((NOW() >= {$wpdb->pbmovie_screen}.screen_srt_date
					 AND (CURDATE() <= {$wpdb->pbmovie_screen}.screen_end_date OR {$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = '')),'Y','N') on_screening_yn
				,IF(NOW() < {$wpdb->pbmovie_screen}.screen_srt_date, 'Y', 'N') on_scheduled_yn

				/* 개봉정보 START */
				,{$wpdb->pbmovie_open}.movie_name movie_name
				,{$wpdb->pbmovie_open}.kofic_code kofic_code
				,{$wpdb->pbmovie_open}.open_date open_date
				,DATE_FORMAT({$wpdb->pbmovie_open}.open_date, '%Y.%m.%d') open_date_ymd
				,DATE_FORMAT({$wpdb->pbmovie_open}.open_date, '%Y.%m.%d %H:%i') open_date_ymdhi

				,{$wpdb->pbmovie_open}.level level
				,".pb_query_gcode_dtl_name("MO001", "{$wpdb->pbmovie_open}.level")." level_name
				
				,{$wpdb->pbmovie_open}.genre genre
				,{$wpdb->pbmovie_open}.3d_yn 3d_yn
				,{$wpdb->pbmovie_open}.director director
				,{$wpdb->pbmovie_open}.main_actors main_actors
				,{$wpdb->pbmovie_open}.running_time running_time
				,{$wpdb->pbmovie_open}.country country
				,{$wpdb->pbmovie_open}.image_url image_url
				,{$wpdb->pbmovie_open}.story story
				
				,{$wpdb->pbmovie_open}.status open_status
				,".pb_query_gcode_dtl_name("MO003", "{$wpdb->pbmovie_open}.status")." open_status_name

				,{$wpdb->pbmovie_open}.publisher_id publisher_id
				,{$wpdb->pbclient}.client_name publisher_name
				/* 개봉정보 END */


				,{$wpdb->pbmovie_screen}.reg_date reg_date
				,DATE_FORMAT({$wpdb->pbmovie_screen}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

				,{$wpdb->pbmovie_screen}.mod_date mod_date
				,DATE_FORMAT({$wpdb->pbmovie_screen}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

				
		     FROM {$wpdb->pbmovie_screen}

		     LEFT OUTER JOIN {$wpdb->pbmovie_open}
		     ON   {$wpdb->pbmovie_open}.ID = {$wpdb->pbmovie_screen}.open_id
			
			 LEFT OUTER JOIN {$wpdb->pbclient}
		     ON   {$wpdb->pbclient}.client_id = {$wpdb->pbmovie_open}.publisher_id

		     LEFT OUTER JOIN {$wpdb->pbcinema}
		     ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbmovie_screen}.cinema_id


		      ";

	$query_ .= apply_filters('pb_movie_screen_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbmovie_screen}.ID")." ";
	}
	if(isset($conditions_['open_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['open_id'], "{$wpdb->pbmovie_screen}.open_id")." ";
	}
	if(isset($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbmovie_screen}.cinema_id")." ";
	}

	if(isset($conditions_['status'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['status'], "{$wpdb->pbmovie_screen}.status")." ";
	}

	if(isset($conditions_['on_screening']) && $conditions_['on_screening'] === true){
		$query_ .= " AND CURDATE() >= {$wpdb->pbmovie_screen}.screen_srt_date
					 AND ({$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = '' OR CURDATE() <= {$wpdb->pbmovie_screen}.screen_end_date) ";
	}

	if(isset($conditions_['at_screen_date']) && strlen($conditions_['at_screen_date'])){
		$query_ .= " AND '".$conditions_['at_screen_date']."' >= {$wpdb->pbmovie_screen}.screen_srt_date
					 AND ({$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = '' OR '".$conditions_['at_screen_date']."' <= {$wpdb->pbmovie_screen}.screen_end_date) ";
	}

	if(isset($conditions_['on_scheduled']) && $conditions_['on_scheduled'] === true){
		$query_ .= " AND CURDATE() < {$wpdb->pbmovie_screen}.screen_srt_date  ";
	}
	if(isset($conditions_['on_scheduled_and_screening']) && $conditions_['on_scheduled_and_screening'] === true){
		$query_ .= " AND (CURDATE() < {$wpdb->pbmovie_screen}.screen_srt_date  OR 
							(CURDATE() >= {$wpdb->pbmovie_screen}.screen_srt_date
					 			AND ({$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = '' OR CURDATE() <= {$wpdb->pbmovie_screen}.screen_end_date))
						)";
	}

	if(isset($conditions_['only_screen_ended_date']) && $conditions_['only_screen_ended_date'] === "Y"){
		$query_ .= " AND CURDATE() > {$wpdb->pbmovie_screen}.screen_end_date  ";	
	}
	if(isset($conditions_['on_closed']) && $conditions_['on_closed'] === true){
		$query_ .= " AND CURDATE() >= {$wpdb->pbmovie_screen}.screen_close_date ";
	}

	if(isset($conditions_['open_status'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['open_status'], "{$wpdb->pbmovie_open}.status")." ";
	}

	if(isset($conditions_['only_display_home']) && $conditions_['only_display_home'] === true){
		$query_ .= " AND {$wpdb->pbmovie_screen}.display_home_yn = 'Y' ";
	}
	if(isset($conditions_['xticket_ref_code'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['xticket_ref_code'], "{$wpdb->pbmovie_screen}.xticket_ref_code")." ";
	}
	
	if(isset($conditions_['kofic_code'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['kofic_code'], "{$wpdb->pbmovie_open}.kofic_code")." ";
	}

	if(isset($conditions_['publisher_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['publisher_id'], "{$wpdb->pbmovie_open}.publisher_id")." ";
	}
	
	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_movie_screen_list_keyword', array(
			"{$wpdb->pbmovie_open}.movie_name",
			"{$wpdb->pbcinema}.cinema_name",
			
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}

	if(isset($conditions_['screen_srt_date']) && strlen($conditions_['screen_srt_date'])){
		$query_ .= " AND {$wpdb->pbmovie_screen}.screen_srt_date >= '".$conditions_['screen_srt_date']."' ";
	}
	if(isset($conditions_['screen_end_date']) && strlen($conditions_['screen_end_date'])){
		$query_ .= " AND {$wpdb->pbmovie_screen}.screen_end_date <= '".$conditions_['screen_end_date']."' ";
	}

	if(isset($conditions_['timetable_end_date']) && strlen($conditions_['timetable_end_date'])){
		$query_ .= " AND ({$wpdb->pbmovie_screen}.screen_end_date >= '".$conditions_['timetable_end_date']."' OR {$wpdb->pbmovie_screen}.screen_end_date IS NULL) ";
	}

	if(isset($conditions_['isc_screen_date'])){
		$query_ .= " AND ({$wpdb->pbmovie_screen}.screen_srt_date between '".$conditions_['isc_screen_date'][0]."' AND '".$conditions_['isc_screen_date'][1]."' OR {$wpdb->pbmovie_screen}.screen_end_date between '".$conditions_['isc_screen_date'][0]."' AND '".$conditions_['isc_screen_date'][1]."')  ";
	}
	if(isset($conditions_['isc_screen_end_date']) && strlen($conditions_['isc_screen_end_date'])){
		$query_ .= " AND {$wpdb->pbmovie_screen}.screen_end_date <= '".$conditions_['screen_end_date']."' ";
	}
	
	$query_ .= apply_filters('pb_movie_screen_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_movie_screen_list_orderby", "", $conditions_);

	if(!strlen($query_orderby_)){
		$query_orderby_ = isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['orderby_for_admin']) && $conditions_['orderby_for_admin'] === true){

		$query_orderby_ = " 

			ORDER BY {$wpdb->pbmovie_screen}.screen_srt_date DESC, (CASE 
					WHEN NOW() < {$wpdb->pbmovie_screen}.screen_srt_date THEN '00001' 
					WHEN (NOW() >= {$wpdb->pbmovie_screen}.screen_srt_date
					 AND (CURDATE() <= {$wpdb->pbmovie_screen}.screen_end_date OR {$wpdb->pbmovie_screen}.screen_end_date IS NULL OR {$wpdb->pbmovie_screen}.screen_end_date = '')) THEN '00003' 
					ELSE '00005'
				END) ASC, movie_name asc
		 ";

	}

	$query_ .= $query_orderby_;

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;
		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	return apply_filters('pb_movie_screen_list', $wpdb->get_results($query_), $conditions_);	
}

//개봉작품 단인내역 가져오기
function pb_movie_screen_data($movie_screen_id_){
	if(!strlen($movie_screen_id_)) return null;
	$result_ = pb_movie_screen_list(array("ID" => $movie_screen_id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_movie_screen_data_fields($data_){
	return pb_format_mapping(apply_filters("pb_movie_screen_data_fields_map",array(
	
		'open_id' => '%d',
		'cinema_id' => '%d',
		'screen_srt_date' => '%s',
		'screen_end_date' => '%s',
		'screen_close_date' => '%s',
		'ticket_charge' => '%d',
		'publisher_charge' => '%d',
		'cinema_charge' => '%d',
		'screen_end_date_fg' => '%s',
		'xticket_ref_code' => '%s',
		'status' => '%s',

	)), $data_);
}

//개봉작품 추가
function pb_movie_screen_data_add($movie_screen_data_){
	$insert_data_ = _pb_parse_movie_screen_data_fields($movie_screen_data_);

	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbmovie_screen}", $insert_value_, $insert_format_);

	if(!$result_) return $result_;

	$movie_screen_id_ = $wpdb->insert_id;

	do_action('pb_movie_screen_added', $movie_screen_id_);

	return $movie_screen_id_;
}

//개봉작품 수정
function pb_movie_screen_data_update($movie_screen_id_, $movie_screen_data_){
	$movie_screen_data_ = _pb_parse_movie_screen_data_fields($movie_screen_data_);

	$update_value_ = $movie_screen_data_['data'];
	$update_format_ = $movie_screen_data_['format'];

	$update_value_['mod_date'] = current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbmovie_screen}", $update_value_, array("ID" => $movie_screen_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_movie_screen_updated', $movie_screen_id_);

	return $result_;
}

//개봉작품 삭제
function pb_movie_screen_data_remove($movie_screen_id_){
	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbmovie_screen}", array("ID" => $movie_screen_id_), array("%d"));

	if(!$result_) return $result_;

	do_action('pb_movie_screen_removed', $movie_screen_id_);
}


function pb_movie_ref_movie_list($cinema_id_){
	return apply_filters('pb_movie_ref_movie_list', array(), $cinema_id_);
}

function pb_movie_screen_timetable($cinema_id_, $target_date_, $screen_id_, $include_not_available_ = false){
	return apply_filters('pb_movie_screen_timetable', array(), $cinema_id_, $target_date_, $screen_id_, $include_not_available_);
}

include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-xticket.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-view.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-adminpage.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-status.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie-screen-dtryx.php')
?>