<?php
/*
 작성일자 : 2019.04.23
 작성자 : 나하나
 설명 : 디트릭스 예매확인 팝업창 띄우기 위해 ajax 설정
*/
pb_add_ajax('movie-dtryx-reservation-popup', function(){
	$current_cinema_ = pb_current_cinema();
	$cinema_type_ = $current_cinema_->ticket_ref_type;

	if($cinema_type_ !== 'dtryx') {
		$popup_html_ = "";
	}
	else {
		ob_start();
		pb_dtryx_drew_reservation();
		$popup_html_ = ob_get_clean();
	}

	echo json_encode(array(
		'success' => true,
		'cinema_type' => $cinema_type_,
		'popup_html' => $popup_html_,
	));
	die();
});

?>