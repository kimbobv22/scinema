<?php


pb_add_ajax('movie-open-manage-find-openapi', function(){
	$query_ = $_POST['query'];

	$result_ = pb_movie_find_openapi($query_);

	echo json_encode(array(
		'success' => true,
		'movie_list' => $result_,
	));
	die();
});
pb_add_ajax('movie-open-manage-load-openapi', function(){


	$kofic_code_ = $_POST['kofic_code'];

	$result_ = pb_movie_load_openapi($kofic_code_);


	echo json_encode(array(
		'success' => true,
		'movie_info' => $result_
	));
	die();

});


pb_add_ajax('movie-open-manage-resize-poster-image', function(){
	$movie_open_list_ = pb_movie_open_list();

	$resize_count_ = 0;

	foreach($movie_open_list_ as $row_data_){
		$ch_ = curl_init($row_data_->image_url);
		curl_setopt($ch_, CURLOPT_HEADER, 0);
		curl_setopt($ch_, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch_, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($ch_, CURLOPT_TIMEOUT, 1000);
	 	curl_setopt($ch_, CURLOPT_USERAGENT, 'Mozilla/5.0');
		$raw_ = curl_exec($ch_);
		curl_close($ch_);

		$upload_filepath_ = pb_upload_filepath(null, pathinfo($row_data_->image_url, PATHINFO_EXTENSION));

		$fp_ = fopen($upload_filepath_["path"], "w");
		fwrite($fp_, $raw_);
		fclose($fp_);



		list($image_width_, $image_height_) = @getimagesize($upload_filepath_["path"]);

		

		if($image_width_ > 768){

			include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/lib/ImageResize.php');

			++$resize_count_;

			$image = new ImageResize($upload_filepath_["path"]);
			$image->resizeToWidth(768);
			$image->save($upload_filepath_["path"]);

			pb_movie_open_data_update($row_data_->ID, array(
				'image_url' => $upload_filepath_["url"],
			));

		}else{
			unlink($upload_filepath_["path"]);
		}
	}


	echo json_encode(array(
		'success' => true,
		'resize_count' => $resize_count_,
	));
	die();

});


?>