<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();

	$current_cinema_data_ = pb_cinema($blog_id_);

	$cinema_type_ = $current_cinema_data_->ticket_ref_type;

	if ($cinema_type_ === "dtryx") {
		return $results_;
	}

	// $is_root_ = pb_cinema_current_is_head_office();

	$results_['movie-screen-manage'] = array(
		'title' => '상영작품관리' ,
		'template-data' => array(

			'data' => array(
				'total-count' => function(){
					//$is_root_ = pb_cinema_current_is_head_office();
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					$srt_date_ = isset($_GET["srt_date"]) ? $_GET["srt_date"] : date("Y-m-d", strtotime("-60 days"));
					$end_date_ = isset($_GET["end_date"]) ? $_GET["end_date"] : date("Y-m-d", strtotime("+60 days"));
					$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
					$status_ = isset($_GET["status"]) ? $_GET["status"] : null;

					if(!pb_cinema_current_is_head_office()){
						$cinema_id_ = pb_current_cinema_id();
					}


					$query_cond_ = array(
						'keyword' => $keyword_,
						'isc_screen_srt_date' => $srt_date_,
						'isc_screen_end_date' => $end_date_,
						'status' => $status_,
						'cinema_id' => $cinema_id_,
						'just_count' => true,
						'orderby_for_admin' => true,
					);

					if($status_ === "expected"){
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'on_scheduled' => true,
							'cinema_id' => $cinema_id_,
							'just_count' => true,
							'orderby_for_admin' => true,
						);
					}else if($status_ === "ended"){
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'only_screen_ended_date' => "Y",
							'cinema_id' => $cinema_id_,
							'just_count' => true,
							'orderby_for_admin' => true,
						);
					}else if($status_ === "00001"){
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'on_screening' => true,
							'status' => $status_,
							'cinema_id' => $cinema_id_,
							'just_count' => true,
							'orderby_for_admin' => true,
						);
					}else{
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'status' => $status_,
							'cinema_id' => $cinema_id_,
							'just_count' => true,
							'orderby_for_admin' => true,
						);
					}

					$total_count_ = pb_movie_screen_list($query_cond_);

					return $total_count_;
				},
				'list' => function($offset_, $limit_){
					//$is_root_ = pb_cinema_current_is_head_office();
					$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
					$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
					$srt_date_ = isset($_GET["srt_date"]) ? $_GET["srt_date"] : date("Y-m-d", strtotime("-60 days"));
					$end_date_ = isset($_GET["end_date"]) ? $_GET["end_date"] : date("Y-m-d", strtotime("+60 days"));
					$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;

					if(!pb_cinema_current_is_head_office()){
						$cinema_id_ = pb_current_cinema_id();
					}

					$status_ = isset($_GET["status"]) ? $_GET["status"] : null;

					$query_cond_ = array(
						'keyword' => $keyword_,
						'isc_screen_srt_date' => $srt_date_,
						'isc_screen_end_date' => $end_date_,
						'status' => $status_,
						'cinema_id' => $cinema_id_,
						'limit' => array($offset_, $limit_),
						'orderby_for_admin' => true,
					);

					if($status_ === "expected"){
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'on_scheduled' => true,
							'cinema_id' => $cinema_id_,
							'limit' => array($offset_, $limit_),
							'orderby_for_admin' => true,
						);
					}else if($status_ === "ended"){
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'only_screen_ended_date' => "Y",
							'cinema_id' => $cinema_id_,
							'limit' => array($offset_, $limit_),
							'orderby_for_admin' => true,
						);
					}else if($status_ === "00001"){
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'on_screening' => true,
							'status' => $status_,
							'cinema_id' => $cinema_id_,
							'limit' => array($offset_, $limit_),
							'orderby_for_admin' => true,
						);
					}else{
						$query_cond_ = array(
							'keyword' => $keyword_,
							'isc_screen_date' => array($srt_date_, $end_date_),
							'status' => $status_,
							'cinema_id' => $cinema_id_,
							'limit' => array($offset_, $limit_),
							'orderby_for_admin' => true,
						);
					}

					$items_ = pb_movie_screen_list($query_cond_);

					return $items_;
				},
				'single' => function($action_data_){
					$data_ = pb_movie_screen_data($action_data_);

					$data_->_xticket_ref_code = $data_->xticket_ref_code;

					return $data_;
				},
				'empty' => function(){

					if(!pb_cinema_current_is_head_office()){
						$cinema_id_ = pb_current_cinema_id();
					}else{
						$cinema_id_ = null;
					}

					return array(
						'cinema_id' => $cinema_id_,
						'status' => '00001',
						'open_id' => null,
						'movie_name' => null,
						'xticket_ref_code'=> null,
						'_xticket_ref_code' => null,
						'display_home_yn' => 'Y',
					);
				},

			),

			'options' => array(
				'list-options' => array(
					'per-page' => 15,
					'label' => array(
						'notfound' => '검색된 상영작품이 없습니다.',
						//'add' => '상영작품등록',
					),
					'custom-search-frame' => function($options_, $table_options_){

						$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
						$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
						$srt_date_ = isset($_GET["srt_date"]) ? $_GET["srt_date"] : date("Y-m-d", strtotime("-60 days"));
						$end_date_ = isset($_GET["end_date"]) ? $_GET["end_date"] : date("Y-m-d", strtotime("+60 days"));
						$status_ = isset($_GET["status"]) ? $_GET["status"] : null;

						if(!pb_cinema_current_is_head_office()){
							$cinema_id_ = pb_current_cinema_id();
						}

						$tmp_cinema_list_ = pb_cinema_list(array(
							"ticket_ref_type" => pb_cinema_manageable_ticket_ref_types(),
							"orderby" => "order by cinema_name asc"
						));
						
						global $pb_adminpage_menu_id;
						?>

<div class="search-frame minus-margin-top">
	
	<div class="row">
		<div class="col-xs-12 col-sm-2">
			<label>&nbsp; &nbsp;&nbsp;</label><br/>
			<a href="<?=pb_adminpage_url($pb_adminpage_menu_id.'/add')?>" class="btn btn-default btn-sm">상영작품등록</a>
			<div class="form-margin-xs visible-xs"></div>
		</div>
		<div class="col-xs-6 col-sm-3">
			<label>상영시작일자</label>
			<div class='input-group date input-group-sm' id='screen-open-srt-date'>
                <input type='text' class="form-control" name="srt_date" value="<?=$srt_date_?>" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
		</div>
		<div class="col-xs-6 col-sm-3">
			<label>상영종료일자</label>
			<div class='input-group date input-group-sm' id='screen-open-end-date'>
                <input type='text' class="form-control" name="end_date" value="<?=$end_date_?>" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
		</div>
	</div>
	<div class="form-margin-xs"></div>
	<div class="row">
		
		<?php if(pb_cinema_current_is_head_office()){ ?>
		<div class="col-xs-6 col-sm-3">
		
			<select class="form-control input-sm" name="cinema_id" id="pb-movie-screen-search-form-cinema-id">
				<option value="">-영화관 선택-</option>

				<?php foreach($tmp_cinema_list_ as $row_data_){ ?>
				<option value="<?=$row_data_->cinema_id?>" <?=selected($row_data_->cinema_id, $cinema_id_)?> ><?=$row_data_->cinema_name?></option>
				<?php } ?>

			</select>
			<div class="clearfix"></div>
			<?php ?>
		</div>
		<?php }else{ ?>
			<input type="hidden" name="cinema_id" value="<?=pb_current_cinema_id()?>">
		<?php } ?>
		<div class="col-xs-6 col-sm-3">
			<select class="form-control input-sm" name="status" id="pb-movie-screen-search-form-status">
				<option value="">-전체상태-</option>
				<option value="expected" <?=selected($status_, "expected")?> >상영예정</option>
				<option value="ended" <?=selected($status_, "ended")?> >상영종료</option>
				<?= pb_gcode_make_options('MS001', $status_)?>
			</select>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-8 col-sm-4">
			<div class="form-margin-xs visible-xs"></div>
			<input type="text" class="form-control input-sm" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
		</div>
		<div class="col-xs-4 col-sm-2">
			<div class="form-margin-xs visible-xs"></div>
			<button class="btn btn-default btn-sm btn-block" type="submit">검색</button>
		</div>
		
	</div>
	
</div>

<hr>

						<?php
					}
				),
				'edit-options' => array(
					'add-redirect-to-list' => true,
					'edit-redirect-to-list' => true,
					'label' => array(
						'edit' => '수정하기',
						'add' => '등록하기',
						'remove' => '삭제하기',
						'add-confirm-title' => '상영작품추가확인',
						'add-confirm-message' => '해당 상영작품를 추가하시겠습니까?',
						'add-success-title' => '추가완료',
						'add-success-message' => '상영작품가 정상적으로 등록되었습니다.',
						'add-error-title' => '에러발생',
						'add-error-message' => '상영작품 추가 중 에러가 발생하였습니다.',
						'edit-success-title' => '수정성공',
						'edit-success-message' => '상영작품정보가 정상적으로 수정되었습니다.',
						'edit-error-title' => '에러발생',
						'edit-error-message' => '상영작품 수정 중 에러가 발생하였습니다.',
						'remove-confirm-title' => '상영작품삭제확인',
						'remove-confirm-message' => '해당 상영작품를 삭제하시겠습니까?',
						'remove-success-title' => '삭제성공',
						'remove-success-message' => '상영작품정보가 삭제되었습니다.',
						'remove-error-title' => '에러발생',
						'remove-error-message' => '상영작품 삭제 중 에러가 발생하였습니다.',
					),
					'not-allow-remove' => !pb_cinema_current_is_head_office(),
				),
				'id-format' => 'pb-movie-screen-edit-form',
			),
			'backend' => array(
				'edit' => function($target_data_){
					//$target_data_['screen_close_date'] = strlen($target_data_['screen_close_date']) ? $target_data_['screen_close_date'] : null;

					if($target_data_['screen_srt_date'] === ""){
						$target_data_['screen_srt_date'] = null;
					}
					if($target_data_['screen_end_date'] === ""){
						$target_data_['screen_end_date'] = null;
					}
					$target_data_['screen_end_date_fg'] = isset($target_data_['screen_end_date_fg']) ? $target_data_['screen_end_date_fg'] : null;

					$target_data_['status'] = isset($target_data_['status']) ? $target_data_['status'] : "00001";					

					$result_ = pb_movie_screen_data_update($target_data_['ID'], $target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '상영작품 수정 중 에러가 발생했습니다.');
					}


					return $target_data_['ID'];
				},
				'add' => function($target_data_){

					$cinema_id_ = $target_data_['cinema_id'];
					$open_id_ = $target_data_['open_id'];

					$checkdata_ = pb_movie_screen_list(array(
						"open_id" => $open_id_,
						"cinema_id" => $cinema_id_,
						'on_screening' => true,
					));

					if(count($checkdata_) > 0){
						return new WP_Error('확인필요', '현재 동일한 작품이 상영중 입니다.');
					}

					if($target_data_['screen_srt_date'] === ""){
						$target_data_['screen_srt_date'] = null;
					}
					if($target_data_['screen_end_date'] === ""){
						$target_data_['screen_end_date'] = null;
					}

					$target_data_['status'] = isset($target_data_['status']) ? $target_data_['status'] : "00001";

					$target_data_['screen_end_date_fg'] = isset($target_data_['screen_end_date_fg']) ? $target_data_['screen_end_date_fg'] : null;
					$result_ = pb_movie_screen_data_add($target_data_);

					if(is_wp_error($result_) || !$result_){
						return new WP_Error('수정실패', '상영작품 추가 중 에러가 발생했습니다.');
					}


					return $result_;
				},
				'remove' => function($target_data_){
					pb_movie_screen_data_remove($target_data_['ID']);
					return true;
				},
			),
			'templates' => array(
				'list' => function(){

					$is_root_ = pb_cinema_current_is_head_office();

					return array(
						'seq' => array(
							'column-label' => '',
							'th-class' => "col-seq text-center ",
							'td-class' => "col-seq text-center ",
							'td-render-func' => function($item_, $column_name_){
								global $_temp_movie_screen_index;
								if(empty($_temp_movie_screen_index)){
									$_temp_movie_screen_index = 1;
								}

								echo $_temp_movie_screen_index;


								++$_temp_movie_screen_index;
							},
						),
						'cinema_name' => array(
							'column-label' => '영화관',
							'th-class' => "col-2 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
							'td-class' => "col-2 text-center hidden-xs ".($is_root_ ? "" : "hidden"),
						),
						'movie_name' => array(
							'column-label' => '상영작품명',
							'th-class' => "col-3",
							'td-class' => "col-3",
							'td-render-func' => function($item_, $column_name_){
								$is_root_ = pb_cinema_current_is_head_office();
								?>


								
								<?php if($is_root_){ ?>
								<div class="cinema-name visible-xs">[<?=$item_->cinema_name?>]</div>
								<?php } ?>
								<a href="<?=pb_adminpage_url("movie-screen-manage/edit/".$item_->ID)?>"><?=strlen($item_->movie_name) ? $item_->movie_name: "(개봉영화정보없음)"?></a>

								<?php
							},
						),
						'publisher_name' => array(
							'column-label' => '배급사',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
							
						),
						'screen_date' => array(
							'column-label' => '상영일자',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
							'td-render-func' => function($item_, $column_name_){
								?>

									<?=$item_->screen_srt_date_ymd?> ~ <?=$item_->screen_end_date_ymd?>

								<?php
							},
						),
						
						'status_name' => array(
							'column-label' => '상태',
							'th-class' => "col-2",
							'td-class' => "col-2",
							'td-render-func' => function($item_, $column_name_){

								?>
									<?php if($item_->status === "00009"){ ?>
										<div class="status-name status-<?=$item_->status?>"><?=$item_->status_name?></div>
									<?php }else if($item_->on_screening_yn === "N" && $item_->on_scheduled_yn === "N"){ ?>
										<div class="status-name status-ended">상영종료</div>
									<?php }else if($item_->on_screening_yn === "N" && $item_->on_scheduled_yn === "Y"){ ?>
										<div class="status-name status-scheduled">상영예정</div>
									<?php }else{ ?>
										<div class="status-name status-<?=$item_->status?>"><?=$item_->status_name?></div>
									<?php } ?>
								<?php
							},
						),

						'xticket_reg_yn' => array(
							'column-label' => 'XTicket등록',
							'th-class' => "col-2 text-center hidden-xs",
							'td-class' => "col-2 text-center hidden-xs",
							'td-render-func' => function($item_, $column_name_){

								echo (strlen($item_->xticket_ref_code) ? "<strong class='xticket-reg-Y'>Y</strong>" : "<strong class='xticket-reg-N'>N</strong>");
							},
						),
						
						
					);
				},
				'edit' => function(){

					$is_root_ = pb_cinema_current_is_head_office();

					$cinema_select_col_ = null;

					if($is_root_){

						$tmp_cinema_list_ = pb_cinema_list(array("orderby" => "order by cinema_name asc"));
						$cinema_list_ = array();

						foreach($tmp_cinema_list_ as $row_data_){
							$cinema_list_[$row_data_->cinema_id] = $row_data_->cinema_name;
						}

						$cinema_select_col_ = array(
							'column' => 'col-xs-12 col-sm-5',
							'column-label' => '상영영화관',
							'column-name' => 'cinema_id',
							'placeholder' => '-영화관 선택-',
							'validates' => array(
								'required' => "영화관을 선택하세요",
							),
							'type' => 'select',
							'options' => $cinema_list_,
							'after' => function(){
								?>
								<small class="help-block">*영화관을 선택 후, 개봉영화선택이 가능합니다.</small>

								<?php
							}
						);
					}else{
						$cinema_select_col_ = array(
							'type' => 'hidden',
							'column-name' => 'cinema_id',
						);
					}

					return array(
						array(
							'type' => 'hidden',
							'column-name' => 'ID',
						),
					
					
						array(
							'type' => 'row',
							'columns' => array(
								$cinema_select_col_,
								array(
									'column' => 'col-xs-12 '.($is_root_ ? "col-sm-7" : "col-sm-12"),
									'column-label' => '개봉영화선택',
									'column-name' => 'open_id',
									'placeholder' => '개봉영화 검색..',
									'validates' => array(
										'required' => "개봉영화를 선택하세요",
									),
									'type' => 'custom',
									'view' => function($attrs_ = array(), $options_ = array(), $data_ = null){

										if(strlen($data_['open_id'])){
											$open_data_ = pb_movie_open_data($data_['open_id']);
										}else{
											$open_data_ = null;
										}


										?>

										<input type="hidden" name="default_open_data" value="<?=htmlentities(json_encode($open_data_))?>">

										<div class="form-group">
											<input type="hidden" name="open_id" value="<?=$data_['open_id']?>">
											<label for="pb-movie-open-edit-form-kofix-code">개봉영화선택</label>
											<div class="input-group">
												<input type="text" class="form-control readonly" placeholder="검색하여 선택" name="open_id_name" value="<?=isset($data_['movie_name']) ? $data_['movie_name'] : ""?>" required data-error="개봉영화를 선택하세요">
												<span class="input-group-btn">
													<button class="btn btn-default" type="button" onclick="pb_movie_screen_manage_edit_find_open_id();">검색하기</button>
												  </span>
											</div>

											<div class="with-errors help-block"></div>
											<div class="clearfix"></div>
											
										</div>

										<div class="pb-movie-open-item-wrap hidden" id="pb-movie-open-item-preview">
											<div class="pb-movie-open-item">
												<div class="col poster-image-col">
													<div class="poster-image light">
														<img src="{$image_url}" class="image" data-image="image_url">
													</div>
												</div>
												<div class="col info-col">
													<div class="movie-name" data-column="movie_name">{$movie_name}</div>
													<div class="subinfo-list">
														<div class="subinfo-item" data-column="publisher_name">{$publisher_name}</div>
														<div class="subinfo-item" data-column="genre">{$genre}</div>
														<div class="subinfo-item" data-column="level_name">{$level_name}</div>
														<div class="subinfo-item"><span data-column="open_date_ymd">{$open_date_ymd}</span> 개봉</div>
													</div>
												</div>
											</div>
										</div>

										<?php
									}

								),
							),
						),
						
						array(
							'type' => 'division',
						),
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-5',
									'column-label' => '상영작품상태',
									'column-name' => 'status',
									'type' => 'checkbox',
									'options' => array(
										'00009' => '영화숨기기'
									),
									'after' => function(){
										?>
											<small class="help-block">*상영종료일자가 지나면 '예매가능'이어도 예매가 불가능합니다.</small>
										<?php
									}
								),

								
							),
						),
						array(
							'type' => 'division',
						),
						/*
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-4',
									'column-label' => '예매가',
									'column-name' => 'ticket_charge',
									'placeholder' => '예매가 입력',
									'validates' => array(
										'required' => "예매가를 입력하세요",
									),
									'type' => 'number',
								),
								array(
									'column' => 'col-xs-12 col-sm-4',
									'column-label' => '배급사부금',
									'column-name' => 'publisher_charge',
									'placeholder' => '배급사부금 입력',
									'validates' => array(
										'required' => "배급사부금을 입력하세요",
									),
									'type' => 'number',
								),
								array(
									'column' => 'col-xs-12 col-sm-4',
									'column-label' => '극장부금',
									'column-name' => 'cinema_charge',
									'placeholder' => '극장부금 입력',
									'validates' => array(
										'required' => "극장부금을 입력하세요",
									),
									'type' => 'number',
								)
							),
						),
						array(
							'type' => 'division',
						),
						*/
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-4',
									'column-label' => '상영시작일자',
									'column-name' => 'screen_srt_date',
									'placeholder' => '상영시작일자 입력',
									'validates' => array(
										'required' => "상영일자를 선택하세요",
									),
									'type' => 'datepicker',
								),
								array(
									'column' => 'col-xs-12 col-sm-4',
									'column-label' => '상영종료일자',
									'column-name' => 'screen_end_date',
									'placeholder' => '상영종료일자 입력',
									'type' => 'datepicker',
									'after' => function(){
										?>
										<small class="help-block"><span class="text-danger">* 상영종료일자 선택 시 수정이 불가능 합니다.</span></small>

										<?php
									}
								),
								array(
									'column' => 'col-xs-12 col-sm-4 '. ($is_root_ ? "" : "hidden"),
									'column-label' => '상영종료수정가능여부',
									'column-name' => 'screen_end_date_fg',
									'type' => 'checkbox',
									'options' => array(
										'Y' => '수정불가'
									),
									'after' => function(){
										?>

										<input type="hidden" name="is_head_office" value="<?=pb_cinema_current_is_head_office() ? "Y" : "N" ?>">

										<?php
									}
								),
								
								/*
								array(
									'column' => 'col-xs-12 col-sm-4',
									'column-label' => '종영일자',
									'column-name' => 'screen_close_date',
									'placeholder' => '종영일자 입력',
									'validates' => array(
										// 'required' => "종영일자를 선택하세요",
									),
									'type' => 'datepicker',
								),
								*/
							),
						),
						array(
							'type' => 'division',
						),
						array(
							'type' => 'row',
							'columns' => array(
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '연동코드',
									'column-name' => 'xticket_ref_code',
									'placeholder' => '연동코드입력',
									'validates' => array(
										// 'required' => "연동코드를 선택하세요",
									),
									'type' => 'text',
									'after' => function(){
										if(pb_cinema_current_is_head_office()){
											?> <small class="help-block">*영화관 선택 후 선택이 가능합니다.</small><?php
										}
									}
								),
								array(
									'column' => 'col-xs-12 col-sm-6',
									'column-label' => '연동코드검색',
									'column-name' => '_xticket_ref_code',
									'placeholder' => '연동코드검색..',
									'validates' => array(
										// 'required' => "연동코드를 선택하세요",
									),
									'type' => 'select',
									'options' => function($movie_screen_data_){


										$cinema_id_ = $movie_screen_data_['cinema_id'];

										$xticket_movie_list_ = pb_movie_ref_movie_list($cinema_id_);
										return $xticket_movie_list_;
									},
									'after' => function(){
										?> <small class="help-block">*리스트에서 선택하면 자동으로 입력됩니다.</small><?php
									}
								),
								
							),
						),

						
					);
				}
			),

		),
		'lvl' => 1,
		'sort_num' => 13,
	);

	return $results_;
});




add_filter('pb_adminpage_list', function($results_){
	// $results_['movie-screen-bulk-write'] = array(
	// 	'title' => '상영작품일괄등록' ,
	// 	'content-template' => "movie-screen-bulk-write",
	// 	'lvl' => 2,
	// 	'sort_num' => 14,
	// );
	
	 return $results_;
});



class PB_find_movie_open_id_table extends PBListGrid{

	function prepare(){
		$page_index_ = isset($_POST["page_index"]) ? (int)$_POST["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		$keyword_ = isset($_POST["keyword"]) ? $_POST["keyword"] : "";
		$cinema_id_ = isset($_POST['cinema_id']) ? $_POST['cinema_id'] : null;

		$movie_open_list_ = array();
		$movie_open_count_ = 0;


		if(strlen($cinema_id_)){
			$cinema_data_ = pb_cinema($cinema_id_);
			$cinema_type_ = $cinema_data_->cinema_type;

			$movie_open_list_ = pb_movie_open_list(array(
				'keyword' => $keyword_,
				'limit' => array($offset_, $per_page_),
				'cinema_id' => ($cinema_type_ === PB_CINEMA_CTG_IND ? $cinema_id_ : PB_CINEMA_HEAD_OFFICE_ID),
				'orderby' => 'ORDER BY reg_date DESC',
			));	
			$movie_open_count_ = pb_movie_open_list(array(
				'keyword' => $keyword_,
				'cinema_id' => ($cinema_type_ === PB_CINEMA_CTG_IND ? $cinema_id_ : PB_CINEMA_HEAD_OFFICE_ID),
				'just_count' => true,
			));
		}

	
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => $movie_open_count_,
			
			"hide_pagenav" => true,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $movie_open_list_,
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function render_item($open_data_){

		?>

		<a class="pb-movie-open-item" href="javascript:_pb_movie_screen_mange_edit_select_open_id(<?=$open_data_->ID?>);">
			<input type="hidden" data-movie-open-data-id="<?=$open_data_->ID?>" value="<?=htmlentities(json_encode($open_data_))?>">
			<div class="col poster-image-col">
				<div class="poster-image light">
					<img src="<?=$open_data_->image_url?>" class="image">
				</div>
			</div>
			<div class="col info-col">
				<div class="movie-name">
					<?php pb_movie_draw_level_badge($open_data_->level); ?>
					 <?=$open_data_->movie_name?>
					 
				</div>
				<div class="subinfo-list">
					<div class="subinfo-item"><?=$open_data_->publisher_name?></div>
					<div class="subinfo-item"><?=$open_data_->genre?></div>
					<div class="subinfo-item"><?=$open_data_->level_name?></div>
					<div class="subinfo-item"><?=$open_data_->open_date_ymd?> 개봉</div>
				</div>
			</div>
		</a>

			<div class="clearfix"></div>
		
		<?php
	}

	function noitemdata(){
		return "검색된 영화가 없습니다.";	
	}
	
}



?>