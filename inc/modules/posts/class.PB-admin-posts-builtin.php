<?php

//에디터 메타박스 빌트인
add_action('add_meta_boxes', function($post_type_, $post_){
	if($post_type_ !== "page") return;

	wp_enqueue_script("page-admin-page", (pb_library_url() . 'js/pages/page-admin-page-edit.js') ,array("pb-all-admin"));

	add_meta_box(
		'pb-post-meta-display',
		'화면표시',
		'_pb_post_meta_box_display',
		null,'side','core'
	);

}, 1, 2);


function _pb_post_meta_box_display($post_){
	wp_nonce_field('_pb_post_meta_box_for_page', 'pb_post_meta_box_for_page');

	$header_fixed_ = get_post_meta($post_->ID, "pb_post_header_fixed", true);
	$header_fixed_ = strlen($header_fixed_) ? $header_fixed_ : "N";
	$post_with_sidemenu_ = get_post_meta($post_->ID, "pb_post_with_sidemenu", true);
	$revslider_id_ = get_post_meta($post_->ID, "pb_post_revslider", true);

	$header_fixed_ = strlen($header_fixed_) ? $header_fixed_ : "N";
	$post_with_sidemenu_ = strlen($post_with_sidemenu_) ? $post_with_sidemenu_ : "Y";

?>
<div>
	<label><input type="checkbox" name="pb_post_header_fixed" value="Y" <?=checked($header_fixed_, "Y")?> ><code>불투명헤더</code></label>
</div>
<br/>
<div>
	<label><input type="checkbox" name="pb_post_with_sidemenu" value="Y" <?=checked($post_with_sidemenu_, "Y")?> ><code>사이드메뉴표시</code></label>
</div>
<br>
<div>
<label><code>상단 레볼루션 슬라이더 ID</code></label>
	<input type="text" name="pb_post_revslider" class="large-text" placeholder="ID 입력" value="<?=$revslider_id_?>">
</div>

<?php

}

add_action('edit_post', function($post_id_, $post_){
	if($post_->post_type !== "page") return;
	if(!isset($_POST['pb_post_meta_box_for_page'])){
		return;
	}

	$header_fixed_ = isset($_POST['pb_post_header_fixed']) ? $_POST['pb_post_header_fixed'] : "N";
	$post_with_sidemenu_ = isset($_POST['pb_post_with_sidemenu']) ? $_POST['pb_post_with_sidemenu'] : "N";
	$revslider_id_ = isset($_POST['pb_post_revslider']) ? $_POST['pb_post_revslider'] : "";
	
	update_post_meta($post_id_, 'pb_post_header_fixed', $header_fixed_);
	update_post_meta($post_id_, 'pb_post_with_sidemenu', $post_with_sidemenu_);
	update_post_meta($post_id_, 'pb_post_revslider', $revslider_id_);
},10,2);

?>