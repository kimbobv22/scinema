<?php

function pb_push_android_send($ids_, $push_data_){

	// $api_key_ = pb_settings("pb_push_gcm_key");
	$api_key_ = "AAAAPiKFS5U:APA91bGOZEuxrqaRb1yXq0Bdi0v4YRcMs9pIUz6SflAUTw8g3CehNezCInXJMJiJp8Zzeex0qpbpo64ISid7ztsehcrro0iv_fq1EuoMTCejO7pGTb4bmncduzjmvX4J4KYhWblaCiTY5L5jJ4XgMeVaJmuq8igNfQ"; //for test

	$message_data_ = array(
		'title'		=> $push_data_['msg_title'],
		// 'subtitle'	=> $push_data_['subtitle'],
		// 'tickerText'	=> $push_data_['short_message'],
		'message' => $push_data_['msg_short_txt'],
		'msg_id' 	=> $push_data_['ID'],
		'push_type' => $push_data_['push_type'],
	);


	if(isset($push_data_['feature_image_url_str']) && strlen($push_data_['feature_image_url_str'])){
		$message_data_['image'] = $push_data_['feature_image_url_str'];
	}

	/*if(isset($push_data_['msg_short_txt']) && strlen($push_data_['msg_short_txt'])){
		$message_data_['subtitle'] = $push_data_['subtitle'];
	}*/

	if(isset($push_data_['msg_short_txt']) && strlen($push_data_['msg_short_txt'])){
		$message_data_['tickerText'] = $push_data_['msg_short_txt'];
	}

	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_HTTPHEADER, array(
		'Authorization: key=' .$api_key_,
		'Content-Type: application/json',
	));
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	// curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode(array(
		'registration_ids' 	=> $ids_,
		'data' => $message_data_,
	)));
	$result_message_ = curl_exec($ch);
	$result_code_ = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	$result_json_ = json_decode($result_message_, true);
	$success_count_ = 0;
	$fail_count_ = 0;

	if(isset($result_json_)){
		$success_count_ = $result_json_['success'];
		$fail_count_ = $result_json_['failure'];
		$result_message_ = $result_json_;
	}

	return array(
		'code' => $result_code_,
		'success_count' => $success_count_,
		'fail_count' => $fail_count_,
		'message' => $result_message_,
	);


}

add_filter('pb_push_send', function($result_, $push_data_){
	if($push_data_['dvc_type'] !== "-1" && $push_data_['dvc_type'] !== PB_PUSH_DEVICE_TYPE_AND){
		return $result_;
	}

	$conditions_ = array('serialize_uid' => true);

	if($push_data_['cinema_id'] === "-1"){
		// $conditions_['only_for_all_cinema'] = true;
	}else{
		$conditions_['cinema_id'] = $push_data_['cinema_id'];
	}

	$conditions_['dvc_type'] = PB_PUSH_DEVICE_TYPE_AND;

	$_push_dvc_uids_ = pb_push_dvc_list($conditions_);
	$push_dvc_count_ = pb_push_dvc_list(array_merge($conditions_, array("just_count" => true)));

	$_push_dvc_uids_ = array_chunk($_push_dvc_uids_, 900);

	$result_['total']['android'] = $push_dvc_count_;

	foreach($_push_dvc_uids_ as $push_dvc_uids_){
		$sent_result_ = pb_push_android_send($push_dvc_uids_, $push_data_);

		if($sent_result_['code'] === 200){
			$result_['success']['android'] += $sent_result_['success_count'];
			$result_['fail']['android'] += $sent_result_['fail_count'];
		}else{
			$result_['fail']['android'] += $push_dvc_count_;
		}	
	}

	return $result_;

}, 10, 2);


?>