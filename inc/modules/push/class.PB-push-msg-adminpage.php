<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();
	$current_cinema_data_ = pb_cinema($blog_id_);
	$cinema_type_ = $current_cinema_data_->ticket_ref_type;

	// 190611 앱 메뉴 없앴다가 일단 다시 살림 --> 190709 앱 메뉴 막기 
	if ($cinema_type_ === "dtryx") {
	 	return $results_;
	}
	
	if($blog_id_ !== PB_CINEMA_HEAD_OFFICE_ID && $current_cinema_data_->cinema_type !== PB_CINEMA_CTG_NOR && $current_cinema_data_->cinema_type !== PB_CINEMA_CTG_PNDM) {
		return $results_;
	}

	// $results_['push-msg-notice-manage'] = array(
	// 	'title' => '앱공지사항관리' ,
	// 	'content-template' => "push-msg-manage/notice/list",
	// 	'subcontent-templates' => array(
	// 		'detail' => "push-msg-manage/notice/detail",
	// 		'add' => "push-msg-manage/notice/add",
	// 	),
	// 	'lvl' => 1,
	// 	'sort_num' => 57,
	// );

	// $results_['push-msg-new-movie-manage'] = array(
	// 	'title' => '앱신작알림관리' ,
	// 	'content-template' => "push-msg-manage/new-movie/list",
	// 	'subcontent-templates' => array(
	// 		'detail' => "push-msg-manage/new-movie/detail",
	// 		'add' => "push-msg-manage/new-movie/add",
	// 	),
	// 	'lvl' => 1,
	// 	'sort_num' => 58,
	// );

	return $results_;

});


class PB_push_msg_new_movie_screen_id_table extends PBListGrid{

	function prepare(){
		$page_index_ = isset($_POST["page_index"]) ? (int)$_POST["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		$keyword_ = isset($_POST["keyword"]) ? $_POST["keyword"] : "";
		$cinema_id_ = isset($_POST['cinema_id']) ? $_POST['cinema_id'] : null;

		$movie_screen_list_ = array();
		$movie_screen_count_ = 0;


		if(strlen($cinema_id_)){
			$cinema_data_ = pb_cinema($cinema_id_);
			$cinema_type_ = $cinema_data_->cinema_type;

			$movie_screen_list_ = pb_movie_screen_list(array(
				'keyword' => $keyword_,
				'limit' => array($offset_, $per_page_),
				'cinema_id' => $cinema_id_,
				'orderby' => 'ORDER BY reg_date DESC',
			));	
			$movie_screen_count_ = pb_movie_screen_list(array(
				'keyword' => $keyword_,
				'cinema_id' => $cinema_id_,
				'just_count' => true,
			));
		}

	
		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => $movie_screen_count_,
			
			"hide_pagenav" => true,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $movie_screen_list_,
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function render_item($screen_data_){

		?>

		<a class="pb-movie-screen-item" href="javascript:_pb_push_msg_new_movie_select_screen_id(<?=$screen_data_->ID?>);">
			<input type="hidden" data-movie-screen-data-id="<?=$screen_data_->ID?>" value="<?=htmlentities(json_encode($screen_data_))?>">
			<div class="col poster-image-col">
				<div class="poster-image light">
					<img src="<?=$screen_data_->image_url?>" class="image">
				</div>
			</div>
			<div class="col info-col">
				<div class="movie-name">
					<?php pb_movie_draw_level_badge($screen_data_->level); ?>
					 <?=$screen_data_->movie_name?>
					 
				</div>
				<div class="subinfo-list">
					<div class="subinfo-item"><?=$screen_data_->publisher_name?></div>
					<div class="subinfo-item"><?=$screen_data_->genre?></div>
					<div class="subinfo-item"><?=$screen_data_->level_name?></div>
					<div class="subinfo-item"><?=$screen_data_->open_date_ymd?> 개봉</div>
				</div>
			</div>
		</a>

			<div class="clearfix"></div>
		
		<?php
	}

	function noitemdata(){
		return "검색된 영화가 없습니다.";	
	}
	
}


?>