<?php

/** 푸시 전송이력 리스트 가져오기
 *
 * @param array 조건절
 * @return array 푸시 전송이력 리스트
 */
function pb_push_msg_list($conditions_ = array()){

	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 

					 {$wpdb->pbpush_msg}.ID ID
					
					,{$wpdb->pbpush_msg}.push_type push_type
					,".pb_query_gcode_dtl_name("DV003", "{$wpdb->pbpush_msg}.push_type")." push_type_name

					,{$wpdb->pbpush_msg}.dvc_type dvc_type
					,CASE 
						WHEN {$wpdb->pbpush_msg}.dvc_type = '-1' THEN '전체'
						ELSE (".pb_query_gcode_dtl_name("DV001", "{$wpdb->pbpush_msg}.dvc_type").")
					END dvc_type_name

					,{$wpdb->pbpush_msg}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_name cinema_name
						
					,{$wpdb->pbpush_msg}.movie_screen_id movie_screen_id
					,{$wpdb->pbmovie_open}.ID movie_open_id
					,{$wpdb->pbmovie_open}.movie_name movie_screen_title
					,{$wpdb->pbmovie_open}.image_url movie_screen_image_url
					,{$wpdb->pbmovie_open}.level movie_screen_level
					
					,{$wpdb->pbmovie_open}.publisher_id movie_screen_publisher_id
					,{$wpdb->pbclient}.client_name movie_screen_publisher_name
					
					,{$wpdb->pbmovie_open}.genre movie_screen_genre
					,{$wpdb->pbmovie_open}.level movie_screen_level
					,".pb_query_gcode_dtl_name("MO001", "{$wpdb->pbmovie_open}.level")." movie_screen_level_name
					,DATE_FORMAT({$wpdb->pbmovie_open}.open_date, '%Y.%m.%d %H:%i') movie_screen_open_date_ymdhi

					,{$wpdb->pbpush_msg}.feature_image_url feature_image_url
					,{$wpdb->pbpush_msg}.msg_short_txt msg_short_txt
					,{$wpdb->pbpush_msg}.msg_title msg_title

					,{$wpdb->pbpush_msg}.msg_html msg_html

					,{$wpdb->pbpush_msg}.total_ios total_ios
					,{$wpdb->pbpush_msg}.total_and total_and

					,{$wpdb->pbpush_msg}.success_ios success_ios
					,{$wpdb->pbpush_msg}.success_and success_and

					,{$wpdb->pbpush_msg}.fail_ios fail_ios
					,{$wpdb->pbpush_msg}.fail_and fail_and

					,{$wpdb->pbpush_msg}.reg_date reg_date
					,DATE_FORMAT({$wpdb->pbpush_msg}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

					,{$wpdb->pbpush_msg}.mod_date mod_date
					,DATE_FORMAT({$wpdb->pbpush_msg}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

					".apply_filters('pb_push_msg_list_fields', "", $conditions_)." 

				FROM {$wpdb->pbpush_msg}
				LEFT OUTER JOIN {$wpdb->pbcinema}
		     	ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbpush_msg}.cinema_id
		     	
		     	LEFT OUTER JOIN {$wpdb->pbmovie_screen}
		     	ON   {$wpdb->pbmovie_screen}.ID = {$wpdb->pbpush_msg}.movie_screen_id 

		     	LEFT OUTER JOIN {$wpdb->pbmovie_open}
		     	ON   {$wpdb->pbmovie_open}.ID = {$wpdb->pbmovie_screen}.open_id 

		     	LEFT OUTER JOIN {$wpdb->pbclient}
		     	ON   {$wpdb->pbclient}.client_id = {$wpdb->pbmovie_open}.publisher_id

			 ";

	$query_ .= apply_filters('pb_push_msg_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbpush_msg}.ID")." ";
	}

	if(isset($conditions_['only_for_all_cinema']) && $conditions_['only_for_all_cinema'] === true){
		if(isset($conditions_['cinema_id'])){
			$query_ .= " AND ({$wpdb->pbpush_msg}.cinema_id = -1
			 OR ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbpush_msg}.cinema_id").") ";
		}else{
			$query_ .= " AND {$wpdb->pbpush_msg}.cinema_id = -1 ";
		}
	}else if(isset($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbpush_msg}.cinema_id")." ";
	}

	if(isset($conditions_['movie_screen_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['movie_screen_id'], "{$wpdb->pbpush_msg}.movie_screen_id")." ";
	}

	if(isset($conditions_['dvc_type'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['dvc_type'], "{$wpdb->pbpush_msg}.dvc_type")." ";
	}

	if(isset($conditions_['push_type'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['push_type'], "{$wpdb->pbpush_msg}.push_type")." ";
	}

	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_cinema_list_keyword', array(
			"{$wpdb->pbpush_msg}.msg_title",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_push_msg_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_push_msg_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}


	$results_ = apply_filters('pb_push_msg_list', $wpdb->get_results($query_));


	return $results_;

}

add_filter('pb_push_msg_list', function($result_){

	foreach($result_ as &$row_data_){
		$row_data_->feature_image_url_str = json_decode(stripslashes($row_data_->feature_image_url), true);
		// $row_data_['feature_image_url_str'] = $row_data_['feature_image_url_str'][0]['']

		if(isset($row_data_->feature_image_url_str) && count($row_data_->feature_image_url_str) > 0){
			$row_data_->feature_image_url_str = $row_data_->feature_image_url_str[0];
			$row_data_->feature_image_url_str = $row_data_->feature_image_url_str['url'];
		}else{
			$row_data_->feature_image_url_str = null;
		}
	}

	return $result_;
});


/** 푸시 전송이력 단일내역 가져오기 (ID)
 *
 * @param string 푸시 전송이력 ID
 * @return array 푸시 전송이력 단일정보
 */
function pb_push_msg($id_){
	if(!strlen($id_)) return null;

	$result_ = pb_push_msg_list(array('ID' => $id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}



function _pb_parse_push_msg_fields($data_){
	return pb_format_mapping(apply_filters("pb_push_msg_fields_map",array(
	
		'push_type' => '%s',
		'dvc_type' => '%s',

		'cinema_id' => '%d',

		'msg_title' => '%s',

		'movie_screen_id' => '%d',

		'feature_image_url' => '%s',
		'msg_short_txt' => '%s',
		'msg_html' => '%s',

		'total_ios' => '%d',
		'total_and' => '%d',
		'success_ios' => '%d',
		'success_and' => '%d',
		'fail_ios' => '%d',
		'fail_and' => '%d',

		'reg_date' => '%s',
		'mod_date' => '%s',
		

	)), $data_);
}

/** 메세지 추가
 *
 * @param array 메세지 정보
 * @return string 추가된 메세지 ID
 */
 function pb_push_msg_add($push_msg_data_){

 	$insert_data_ = _pb_parse_push_msg_fields($push_msg_data_);

 	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbpush_msg}", $insert_value_, $insert_format_);
	if(!$result_) return $result_;

	$push_msg_id_ = $wpdb->insert_id;

	do_action('pb_push_msg_added', $push_msg_id_);

	return $push_msg_id_;

 }

/** 메세지 수정
 *
 * @param array 메세지 정보
 * @return string 추가된 메세지 ID
 */
function pb_push_msg_update($push_msg_id_, $update_data_){

	$update_data_ = _pb_parse_push_msg_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];

	$update_value_['mod_date']= current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbpush_msg}", $update_value_, array("ID" => $push_msg_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_push_msg_updated', $push_msg_id_);

	return $result_;

}

/** 메세지 삭제
 *
 * @param array 메세지 정보
 * @return string 추가된 메세지 ID
 */
function pb_push_msg_delete($push_msg_id_){

	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbpush_msg}", array("ID" => $push_msg_id_), array("%d"));

	if(!$result_) return $result_;
	do_action('pb_push_msg_removed', $push_msg_id_);
 
}

/** 메세지 전송
 *
 * @param array 메세지 정보
 * @return string 추가된 메세지 ID
 */
function pb_push_send_message($push_msg_data_ = array()){

	$result_id_ = pb_push_msg_add($push_msg_data_);

	$push_msg_data_ = (array)pb_push_msg($result_id_);

	$result_ = apply_filters('pb_push_send', array(
		'total' => array(
			'ios' => 0,
			'android' => 0,
		),
		'success' => array(
			'ios' => 0,
			'android' => 0,
		),
		'fail' => array(
			'ios' => 0,
			'android' => 0,
		)
	),$push_msg_data_);

	$push_msg_data_['total_ios'] = $result_['total']['ios'];
	$push_msg_data_['total_and'] = $result_['total']['android'];

	$push_msg_data_['success_ios'] = $result_['success']['ios'];
	$push_msg_data_['success_and'] = $result_['success']['android'];

	$push_msg_data_['fail_ios'] = $result_['fail']['ios'];
	$push_msg_data_['fail_and'] = $result_['fail']['android'];


	pb_push_msg_update($result_id_, $push_msg_data_);

	return $result_id_;
}

/** 메세지 전송
 *
 * @param array 메세지 정보
 * @return string 추가된 메세지 ID
 */
function pb_push_resend_message($push_id_){

	$push_msg_data_ = (array)pb_push_msg($push_id_);


	$result_ = apply_filters('pb_push_send', array(
		'total' => array(
			'ios' => 0,
			'android' => 0,
		),
		'success' => array(
			'ios' => 0,
			'android' => 0,
		),
		'fail' => array(
			'ios' => 0,
			'android' => 0,
		)
	),$push_msg_data_);

	$push_msg_data_['total_ios'] = $result_['total']['ios'];
	$push_msg_data_['total_and'] = $result_['total']['android'];

	$push_msg_data_['success_ios'] = $result_['success']['ios'];
	$push_msg_data_['success_and'] = $result_['success']['android'];

	$push_msg_data_['fail_ios'] = $result_['fail']['ios'];
	$push_msg_data_['fail_and'] = $result_['fail']['android'];

	return pb_push_msg_update($push_id_, $push_msg_data_);
}

/** 메세지 전송 이력 삭제
 *
 * @param array 메세지 정보
 * @return string 추가된 메세지 ID
 */
function pb_push_remove_message($push_id_){

	return pb_push_msg_delete($push_id_);

}


include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push-msg-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push-msg-adminpage.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push-msg-gcode.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push-msg-ajax.php');

?>