<?php

add_filter('pb_adminpage_list', function($results_){

	$blog_id_ = get_current_blog_id();
	$current_cinema_data_ = pb_cinema($blog_id_);

	if($blog_id_ !== PB_CINEMA_HEAD_OFFICE_ID && $current_cinema_data_->cinema_type !== PB_CINEMA_CTG_NOR && $current_cinema_data_->cinema_type !== PB_CINEMA_CTG_PNDM){
		return $results_;
	}

	// $results_['push-dvc-manage'] = array(
	// 	'title' => '앱등록기기조회' ,
	// 	'content-template' => "push-dvc-manage/list",
	// 	'lvl' => 2,
	// 	'sort_num' => 55,
	// );

	return $results_;

});

?>