<?php

add_action('init', function(){

	global $wpdb;

	$wpdb->pbpush_dvc = "{$wpdb->base_prefix}push_dvc";
	
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}push_dvc` (

		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`UID` varchar(300)  NOT NULL COMMENT '디바이스 기기번호',

		`cinema_id` BIGINT(20)  DEFAULT NULL COMMENT '상영관 ID',
		
		`dvc_type` varchar(10)  DEFAULT NULL COMMENT '디바이스타입',
		`dvc_model` varchar(50) DEFAULT NULL COMMENT '기기모델',
		`dvc_version` varchar(50) DEFAULT NULL COMMENT '기기버젼',
	
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='푸시디바이스';";
	
	return $args_;
},10, 3);





?>