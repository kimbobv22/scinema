<?php

define('PB_PUSH_DEVICE_TYPE_IOS','00001');
define('PB_PUSH_DEVICE_TYPE_AND','00003');
define('PB_PUSH_DEVICE_TYPE_ETC','90009');

/** 디바이스 리스트 가져오기
 *
 * @param array 조건절
 * @return array 디바이스 리스트
 */
function pb_push_dvc_list($conditions_ = array()){

	global $wpdb;

	$just_count_ = (isset($conditions_['just_count']) && $conditions_['just_count'] === true);

	$query_ = '';

	$query_ .= "SELECT 

					 {$wpdb->pbpush_dvc}.ID ID

					,{$wpdb->pbpush_dvc}.UID UID

					,{$wpdb->pbpush_dvc}.cinema_id cinema_id
					,{$wpdb->pbcinema}.cinema_name cinema_name

					,{$wpdb->pbpush_dvc}.dvc_type dvc_type
					,".pb_query_gcode_dtl_name("DV001", "{$wpdb->pbpush_dvc}.dvc_type")." dvc_type_name

					,{$wpdb->pbpush_dvc}.dvc_model dvc_model
					,{$wpdb->pbpush_dvc}.dvc_version dvc_version

					,{$wpdb->pbpush_dvc}.reg_date reg_date
					,DATE_FORMAT({$wpdb->pbpush_dvc}.reg_date, '%Y.%m.%d %H:%i') reg_date_ymdhi

					,{$wpdb->pbpush_dvc}.mod_date mod_date
					,DATE_FORMAT({$wpdb->pbpush_dvc}.mod_date, '%Y.%m.%d %H:%i') mod_date_ymdhi

					".apply_filters('pb_push_dvc_list_fields', "", $conditions_)." 

				FROM {$wpdb->pbpush_dvc}
				LEFT OUTER JOIN {$wpdb->pbcinema}
		     	ON   {$wpdb->pbcinema}.cinema_id = {$wpdb->pbpush_dvc}.cinema_id

			 ";

	$query_ .= apply_filters('pb_push_dvc_list_join', "", $conditions_);

	$query_ .= ' WHERE 1 ';

	if(isset($conditions_['ID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['ID'], "{$wpdb->pbpush_dvc}.ID")." ";
	}

	if(isset($conditions_['UID'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['UID'], "{$wpdb->pbpush_dvc}.UID")." ";
	}

	if(isset($conditions_['only_for_all_cinema']) && $conditions_['only_for_all_cinema'] === true){
		if(isset($conditions_['cinema_id'])){
			$query_ .= " AND ({$wpdb->pbpush_dvc}.cinema_id is null
			 OR ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbpush_dvc}.cinema_id").") ";
		}else{
			$query_ .= " AND {$wpdb->pbpush_dvc}.cinema_id is null ";
		}
	}else if(isset($conditions_['cinema_id'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['cinema_id'], "{$wpdb->pbpush_dvc}.cinema_id")." ";
	}

	if(isset($conditions_['dvc_type'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['dvc_type'], "{$wpdb->pbpush_dvc}.dvc_type")." ";
	}

	if(isset($conditions_['dvc_model'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['dvc_model'], "{$wpdb->pbpush_dvc}.dvc_model")." ";
	}

	if(isset($conditions_['dvc_version'])){
		$query_ .= " AND ".pb_query_in_fields($conditions_['dvc_version'], "{$wpdb->pbpush_dvc}.dvc_version")." ";
	}

	if(isset($conditions_['keyword']) && strlen($conditions_['keyword'])){
		$keyword_fields_ = apply_filters('pb_cinema_list_keyword', array(
			"{$wpdb->pbpush_dvc}.UID",
			"{$wpdb->pbpush_dvc}.cinema_id",
			"{$wpdb->pbpush_dvc}.dvc_model",
			"{$wpdb->pbpush_dvc}.dvc_version",
		));

		$query_ .= " AND ".pb_query_keyword_search($keyword_fields_, $conditions_['keyword'])." ";
	}
	
	$query_ .= apply_filters('pb_push_dvc_list_where', "", $conditions_);

	if($just_count_){
		return $wpdb->get_var("SELECT COUNT(*) FROM ({$query_}) TMP ");
	}

	$query_orderby_ = apply_filters("pb_push_dvc_list_orderby", "", $conditions_);

	if(strlen($query_orderby_)){
		$query_ .= $query_orderby_;
	}else{
		$query_ .= isset($conditions_['orderby']) ? esc_sql($conditions_['orderby']) : ' ORDER BY reg_date DESC';	
	}

	if(isset($conditions_['limit'])){
		$offset_ = isset($conditions_['limit'][0]) ? $conditions_['limit'][0] : 0;
		$length_ = isset($conditions_['limit'][1]) ? $conditions_['limit'][1] : 15;

		$query_ .= " LIMIT ".$offset_.", ".$length_;
	}

	$results_ = apply_filters('pb_push_dvc_list', $wpdb->get_results($query_));

	if(isset($conditions_['serialize_uid']) && $conditions_['serialize_uid'] == true){
		$uids_ = array();

		foreach($results_ as $row_data_){
			$uids_[] = $row_data_->UID;
		}

		return $uids_;
	}



	return $results_;

}


/** 디바이스 단일내역 가져오기 (ID)
 *
 * @param string 디바이스 ID
 * @return array 디바이스 단일정보
 */
function pb_push_dvc($id_){
	if(!strlen($id_)) return null;

	$result_ = pb_push_dvc_list(array('ID' => $id_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

/** 디바이스 단일내역 가져오기 (UID)
 *
 * @param string 디바이스 UID
 * @return array 디바이스 단일정보
 */
function pb_push_dvc_by_uid($uid_){

	if(!strlen($uid_)) return null;

	$result_ = pb_push_dvc_list(array('UID' => $uid_));

	if(count($result_) > 0){
		return $result_[0];
	}

	return null;
}

function _pb_parse_push_dvc_fields($data_){
	return pb_format_mapping(apply_filters("pb_push_dvc_fields_map",array(
	
		'UID' => '%s',
		'cinema_id' => '%d',

		'dvc_type' => '%s',
		'dvc_model' => '%s',
		'dvc_version' => '%s',
		

	)), $data_);
}

/** 디바이스 추가
 *
 * @param array 디바이스 정보
 * @return string 추가된 디바이스 UID
 */
 function pb_push_dvc_add($push_dvc_data_){

 	$insert_data_ = _pb_parse_push_dvc_fields($push_dvc_data_);

 	$insert_value_ = $insert_data_['data'];
	$insert_format_ = $insert_data_['format'];

	$insert_value_['reg_date'] = current_time('mysql');
	$insert_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->insert("{$wpdb->pbpush_dvc}", $insert_value_, $insert_format_);
	if(!$result_) return $result_;

	$push_dvc_id_ = $wpdb->insert_id;

	do_action('pb_push_dvc_added', $push_dvc_id_);

	return $push_dvc_id_;

 }

/** 디바이스 수정
 *
 * @param array 디바이스 정보
 * @return string 추가된 디바이스 ID
 */
function pb_push_dvc_update($push_dvc_id_, $update_data_){

	$update_data_ = _pb_parse_push_dvc_fields($update_data_);

	$update_value_ = $update_data_['data'];
	$update_format_ = $update_data_['format'];

	$update_value_['mod_date']= current_time('mysql');
	$update_format_[] = '%s';

	global $wpdb;

	$result_ = $wpdb->update("{$wpdb->pbpush_dvc}", $update_value_, array("ID" => $push_dvc_id_), $update_format_, array("%d"));
	if(!$result_) return $result_;

	do_action('pb_push_dvc_updated', $push_dvc_id_);

	return $result_;

}

/** 디바이스 삭제
 *
 * @param array 디바이스 정보
 * @return string 추가된 디바이스 ID
 */
function pb_push_dvc_delete($push_dvc_id_){

	global $wpdb;

	$result_ = $wpdb->delete("{$wpdb->pbpush_dvc}", array("ID" => $push_dvc_id_), array("%d"));

	if(!$result_) return $result_;
	do_action('pb_push_dvc_removed', $push_dvc_id_);
 
}
include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push-dvc-builtin.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push-dvc-adminpage.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push-dvc-gcode.php');


?>