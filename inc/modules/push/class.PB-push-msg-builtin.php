<?php

add_action('init', function(){

	global $wpdb;

	$wpdb->pbpush_msg = "{$wpdb->base_prefix}push_msg";
	
});

add_filter('pb_install_db_table', function($args_, $table_prefix_, $table_charset_){

	global $wpdb;

	$args_[] = "CREATE TABLE IF NOT EXISTS `{$table_prefix_}push_msg` (

		`ID` BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',

		`push_type` varchar(10)  DEFAULT NULL COMMENT '알림 종류',
		
		`dvc_type` varchar(10)  DEFAULT NULL COMMENT '디바이스타입',
		`cinema_id` BIGINT(20) COMMENT '영화관 ID',

		`msg_title` varchar(100) COMMENT '알림 제목',

		`movie_screen_id` BIGINT(20) COMMENT '상영영화 ID',

		`feature_image_url` varchar(500) DEFAULT null COMMENT '메인이미지',
		`msg_short_txt` varchar(500)  DEFAULT NULL COMMENT '알림 한줄 설명',
		`msg_html` longtext  DEFAULT NULL COMMENT '알림 내용',

		`total_ios` BIGINT(10) DEFAULT 0 COMMENT '총 횟수 (ios)',
		`total_and` BIGINT(10) DEFAULT 0 COMMENT '총 횟수 (android)',

		`success_ios` BIGINT(10) DEFAULT 0 COMMENT '성공 횟수 (ios)',
		`success_and` BIGINT(10) DEFAULT 0 COMMENT '성공 횟수 (android)',

		`fail_ios` BIGINT(10) DEFAULT 0 COMMENT '실패 횟수 (ios)',
		`fail_and` BIGINT(10) DEFAULT 0 COMMENT '실패 횟수 (android)',
		
		`reg_date` datetime DEFAULT NULL,
		`mod_date` datetime DEFAULT NULL,
		
		PRIMARY KEY (`ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='푸시메세지';";

	return $args_;
},10, 3);





?>