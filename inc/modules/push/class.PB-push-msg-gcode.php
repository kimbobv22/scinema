<?php

add_filter('pb_initial_gcode_list', function($list_){

	$list_['DV003'] = array(
		'name' => '알림종류',
		'data' => array(
			'00001' => '공지사항',
			'00003' => '영화정보',
		)
	);

	return $list_;
});

?>