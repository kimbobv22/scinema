<?php

add_filter('pb_initial_gcode_list', function($list_){

	$list_['DV001'] = array(
		'name' => '디바이스구분',
		'data' => array(
			PB_PUSH_DEVICE_TYPE_IOS => 'IOS',
			PB_PUSH_DEVICE_TYPE_AND => '안드로이드',
			PB_PUSH_DEVICE_TYPE_ETC => '기타',
		)
	);

	return $list_;
});

?>