<?php


//푸시전송
pb_add_ajax('push-send-message', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	
	$push_data_ = isset($_POST['form_data']) ? $_POST['form_data'] : null;
	
	if(empty($_POST['form_data'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}

	$msg_id_ = pb_push_send_message($push_data_);

	echo json_encode(array(
		'success' => true,
		'msg_id' => $msg_id_,
		'success_title' => '푸시전송완료',
		'success_message' => '푸시 전송이 완료되었습니다.',
	));
	die();
});

//푸시재전송
pb_add_ajax('push-resend-message', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}
	
	$msg_id_ = isset($_POST['msg_id']) ? $_POST['msg_id'] : null;
	
	if(!strlen($_POST['msg_id'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}
	
	pb_push_resend_message($msg_id_);

	echo json_encode(array(
		'success' => true,
		'success_title' => '푸시재전송완료',
		'success_message' => '푸시 재전송이 완료되었습니다.',
		'msg_id' => $msg_id_,
	));
	die();
});

//내역삭제
pb_add_ajax('push-remove-message', function(){
	if(!pb_is_admin()){
		echo json_decode(array(
			'success' => false,
			'deny' => true,
		));
		die();
	}

	$msg_id_ = isset($_POST['msg_id']) ? $_POST['msg_id'] : null;
	
	if(empty($_POST['msg_id'])){
		echo json_encode(array(
			'success' => false,
			'error_title' => '잘못된 요청' ,
			'error_message' => '필수변수가 누락되었습니다.'
		));
		die();
	}
	
	pb_push_remove_message($msg_id_);

	echo json_encode(array(
		'success' => true,
		'success_title' => '삭제완료',
		'success_message' => '정상적으로 처리되었습니다.',
	));
	die();
});

?>