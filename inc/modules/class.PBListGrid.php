<?php

define("PB_LISTGRID_MAPS", "_pb_listgrid_maps");

$listgrid_maps_ = pb_session_get(PB_LISTGRID_MAPS);
if(!isset($listgrid_maps_)){
	pb_session_put(PB_LISTGRID_MAPS, array());
}

class PBListGrid{

	private $global_id = "";

	private $html_class = "";
	private $html_id = "";

	private $ajax = false;

	private $current_index = -1;

	function global_id(){
		return $this->global_id;
	}

	function current_index(){
		return $this->current_index;
	}
	function set_ajax($bool_){
		$this->ajax = $bool_;
	}
	function is_ajax(){
		return $this->ajax;
	}

	function __construct($id_ = "",$class_ = ""){
		$this->html_id = $id_;
		$this->html_class = $class_;

		$listgrid_maps_ = pb_session_get(PB_LISTGRID_MAPS);
		
		$global_id_ = pb_random_string(20);
		$this->global_id = $global_id_;

		$listgrid_maps_[$global_id_] = serialize($this);
		pb_session_put(PB_LISTGRID_MAPS, $listgrid_maps_);
	}

	function offset($page_index_, $per_page_){
		return ($page_index_) * $per_page_;
	}

	function render_item($item_){
		_e("must override PBListGrid::rander_item()");
	}

	function item_classes($items_){
		return "";
	}

	function noitemdata(){
		return __("Data not found");
	}

	function first_row_display(){
		return $this->noitemdata();
	}

	function prepare(){
		_e("must override PBListGrid::prepare()");
	}

	function items($args_){
		_e("must override PBListGrid::items()");
	}

	function rander_grid($args_, $items_){
		$is_first_ = isset($args_["first"]) ? $args_["first"] : false;
		$is_noitemdata_ = (count($items_) == 0);

		$html_ = '';
		
		$this->current_index = 0;
		foreach($items_ as $row_index_ => $row_data_){
			
			$html_ .= '<div class="pb-list-grid-item '.$this->item_classes($row_data_).'" >';

			ob_start();

			$this->render_item($row_data_);

			$html_ .= ob_get_clean();

			$html_ .='</div>';

			$this->current_index = $this->current_index + 1;
		}

		if($is_first_){
			$html_ .= '<div class="no-rowdata first" >'.$this->first_row_display().'</div>';
		}else if($is_noitemdata_){
			$html_ .= '<div class="no-rowdata" >'.$this->noitemdata().'</div>';
		}

		return $html_;
	}
	function rander_pagenav($args_){
		$per_page_ = $args_["per_page"];
		$total_count_ = $args_["total_count"];
		$total_page_count_ = ceil($total_count_ / $per_page_);
		$page_index_ = $args_["page_index"];
		$pagenav_count_ = $args_["pagenav_count"];
		$is_pagenav_number_ = $args_['pagenav_number'];
		
		$pagenav_offset_ = floor(($page_index_) / $pagenav_count_);
		$html_ = '';

		if($is_pagenav_number_){

			$pagenav_start_index_ = ($pagenav_offset_ * $pagenav_count_);
			$pagenav_end_offset_ = ($pagenav_count_*($pagenav_offset_+1));

			if($total_page_count_ <= $pagenav_end_offset_){
				$pagenav_end_offset_= $total_page_count_;
			}

			if($pagenav_start_index_ > 0){
				$html_ .= '<a href="javascript:void(0);" class="pagenav-left pagenav-btn" data-page-index="'.($pagenav_start_index_-1).'"><i class="fa fa-angle-left"></i></a>';
			}else{
				$html_ .= '<span class="pagenav-left pagenav-btn"></span>';
			}

			for($pagenav_index_ = $pagenav_start_index_; $pagenav_index_ < $pagenav_end_offset_; ++$pagenav_index_){
				$html_ .= '<a href="javascript:void(0);" data-page-index="'.($pagenav_index_).'" class="'.($pagenav_index_ == $page_index_ ? "active" : "").' page-numbers">'.($pagenav_index_ + 1).'</a>';
			}

			if($total_page_count_ > $pagenav_end_offset_){
				$html_ .= '<a href="javascript:void(0);" class="pagenav-right pagenav-btn" data-page-index="'.($pagenav_end_offset_).'"><i class="fa fa-angle-right"></i></a>';
			}else{
				$html_ .= '<span class="pagenav-left pagenav-btn"></span>';
			}
		}else{
			if(($page_index_-1) >= 0){
				$html_ .= '<a href="javascript:void(0);" class="pagenav-left pagenav-btn" data-page-index="'.($page_index_-1).'"><i class="fa fa-angle-left"></i></a>';
			}else{
				$html_ .= '<span class="pagenav-left pagenav-btn"></span>';
			}

			
			if($total_count_ > 0){
				$html_ .= '<span class="page-monitor">'.($page_index_+1).'/'.$total_page_count_.'</span>';	
			}	
			
			if($total_page_count_ > ($page_index_+1)){
				$html_ .= '<a href="javascript:void(0);" class="pagenav-right pagenav-btn" data-page-index="'.($page_index_+1).'"><i class="fa fa-angle-right"></i></a>';
			}else{
				$html_ .= '<span class="pagenav-left pagenav-btn"></span>';
			}

		}

		$html_ .= '<div class="clearfix"></div>';

		return $html_;
	}

	function html(){
		$args_ = $this->prepare();
		
		$page_index_ = $args_["page_index"];
		$per_page_ = $args_["per_page"];
		$total_count_ = $args_["total_count"];

		$hide_pagenav_ = (isset($args_["hide_pagenav"]) ? $args_["hide_pagenav"] : false);
		$html_default_class_ = (isset($args_['html_default_class'])) ? $args_['html_default_class'] : "pb-listgrid" ;

		$html_ = '';
		$html_ .= '<input type="hidden" name="page_index" value="'.$page_index_.'">';
		$html_ .= '<div class="pb-list-grid '.$html_default_class_.' '.$this->html_class.'" '.(strlen($this->html_id) ? 'id="'.$this->html_id.'"' : '').' data-pb-listgrid-id="'.$this->global_id.'" ';

		$html_ .= '>';

		
		if(!$this->is_ajax()){
			$html_ .= $this->rander_grid($args_, $this->items($args_));
		}else $html_ .= $this->rander_grid(array('first' => true), array());
		
		$html_ .= '</div>';

		ob_start();
		apply_filters('pb-listgrid-before-pagenav-'.$this->html_id, '');
		$html_ .= ob_get_clean();

		$html_ .= '<div class="pb-list-pagenav" '.(strlen($this->html_id) ? 'id="'.$this->html_id.'-pagenav"' : '').'  data-pb-listgrid-pagenav-id="'.$this->global_id.'">';
		if(!$hide_pagenav_ && !$this->is_ajax()){
			$html_ .= $this->rander_pagenav($args_);
		}
		$html_ .= '</div>';

		ob_start();
		apply_filters('pb-listgrid-after-pagenav-'.$this->html_id, '');
		$html_ .= ob_get_clean();

		
		$html_ .= '<script type="text/javascript">';
		$html_ .= '_pb_list_grid_initialize("'.$this->global_id.'"'.($this->is_ajax() ? ",true" : "").');';
		$html_ .= '</script>';

		return $html_;
	}
}

pb_add_ajax("pb-listgrid-load-html", function(){
	$global_id_ = $_REQUEST["global_id"];
	$listgrid_maps_ = pb_session_get(PB_LISTGRID_MAPS);
	$listgrid_ = unserialize($listgrid_maps_[$global_id_]);
	
	$args_ = $listgrid_->prepare();
	$body_html_ = $listgrid_->rander_grid($args_, $listgrid_->items($args_));
	$pagenav_html_ = $listgrid_->rander_pagenav($args_);	

	echo json_encode(array(
		"success" => true,
		"body_html" => $body_html_,
		"pagenav_html" => $pagenav_html_,
		"orgdata" => $args_,
	));
	exit();
});

//라이브러리 등록
add_action("pb_library_initialize", function(){
	wp_register_script("pb-listgrid", (pb_library_url() . 'js/pb.listgrid/pb.listgrid.js'),array("jquery","pb-all"));

	PB_library::register("pb-listgrid",array(
		"script" => array(
			"pb-listgrid",
		),
	));
});

?>