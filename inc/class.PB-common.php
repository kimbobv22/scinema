<?php



/***
Core Modules
**/
include_once(PB_THEME_LIB_DIR_PATH . 'class.PB-library.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-session.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-rewriterule.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-utils.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-ajax.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-fileupload.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-mobile-detect.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-crontab.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-crypt.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-settings.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-map.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-mail.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/common/class.PB-sms.php');


/***
Extension Modules
**/
include_once(PB_THEME_LIB_DIR_PATH . 'modules/class.PBListTable.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/class.PBListGrid.php');

/***
Common
**/
include_once(PB_THEME_LIB_DIR_PATH . 'modules/gcode/class.PB-gcode.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/paygate/class.PB-paygate.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/posts/class.PB-posts.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/user/class.PB-user.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/adminpage/class.PB-adminpage.php');

include_once(PB_THEME_LIB_DIR_PATH . 'modules/cinema/class.PB-cinema.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/client/class.PB-client.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/movie/class.PB-movie.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/popup/class.PB-popup.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner/class.PB-banner.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/faq/class.PB-faq.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/notice/class.PB-notice.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/press/class.PB-press.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/sms-reg/class.PB-sms-reg.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/app-api/class.PB-app-api.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/push/class.PB-push.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/homepage-manage/class.PB-homepage-manage.php');
include_once(PB_THEME_LIB_DIR_PATH . 'modules/banner-main/class.PB-banner-main.php');

/***
Page
**/
foreach(glob(PB_THEME_LIB_DIR_PATH . 'pages/*.php') as $filename_){
    include_once $filename_;
}

?>