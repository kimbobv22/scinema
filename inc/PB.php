<?php

define("PBDOMAIN", 'PB');
define("PB_THEME_LIB_DIR_PATH",PB_THEME_DIR_PATH.'inc/');
define("PB_THEME_LIB_DIR_URL",PB_THEME_DIR_URL.'inc/');

if(!defined("WP_ALLOW_MULTISITE")){
	define('WP_ALLOW_MULTISITE', true);
}

class PB{

	static function _event_deactivated(){
		do_action('pb_deactivated');
		delete_option("pb_deactivated");
	}

	static function _event_activated(){
		do_action('pb_activated');
		add_option("pb_activated",true);
	}

	static function _fire_action_for_template(){
		$post_type_ = get_post_type();

		if(is_404()){
			$post_type_ = 'page';
			do_action('pb_template_404');
		}

		$post_type_ = get_post_type();
		
		do_action("pb_template");

		if(isset($post_type_) && strlen($post_type_)){
			$template_name_ = get_page_template_slug();
			$template_name_ = substr($template_name_, strrpos($template_name_, '/')+1);
			$template_name_ = substr($template_name_, 0, strrpos($template_name_, '.'));

			$post_format_ = get_post_format();

			if(!strlen($template_name_)){
				$template_name_ = 'standard';
			}

			do_action("pb_template_{$post_type_}");

			if(!is_404()){
				if($post_type_ === "page" || !strlen($post_format_)){
					do_action("pb_template_{$post_type_}_{$template_name_}");
				}else if(strlen($post_format_)){
					do_action("pb_template_{$post_type_}_{$post_format_}");
				}

				if(is_home() || is_front_page()){
					do_action("pb_template_front_page");
				}
			}
		}
	}

	static function get_current_class_data(){
		global $_pb_class_map;
		if(!isset($_pb_class_map))
			return null;

		$page_ = isset($_REQUEST["page"]) ? $_REQUEST["page"] : "";
		$current_class_data_ = null;
		foreach($_pb_class_map as $row_index_ => $class_data_){
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$equals_parameters_ = $class_data_["equals_parameters"];
			if(!current_user_can($class_data_["permission"])) continue;
			if(strlen($page_) && $page_ !== $class_data_["slug"]) continue;

			$checked_ = true;
			foreach($parameters_ as $row_index_ => $param_key_){
				if(!isset($_REQUEST[$param_key_]) || !strlen($_REQUEST[$param_key_])){
					$checked_ = false;
					break;
				}
			}

			if($checked_) foreach($equals_parameters_ as $key_ => $value_){
				if(!isset($_REQUEST[$key_]) || $_REQUEST[$key_] !== $value_){
					$checked_ = false;
					break;
				}
			}

			if($checked_){
				$before_param_count_ = count($current_class_data_["parameters"]) + count($current_class_data_["equals_parameters"]);
				$param_count_ = count($parameters_) + count($equals_parameters_);

				if($param_count_ >= $before_param_count_){
					$current_class_data_ = $class_data_;
				}
			}
		}

		return isset($current_class_data_) ? $current_class_data_ : null;
	}

	static function _get_root_class_data($page_){
		global $_pb_class_map;
		if(!isset($_pb_class_map))
			return null;

		foreach($_pb_class_map as $row_index_ => $class_data_){
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$equals_parameters_ = $class_data_["equals_parameters"];
			if(!current_user_can($class_data_["permission"])) continue;
			if(strlen($page_) && $page_ !== $class_data_["slug"]) continue;

			if(count($parameters_) == 0 && count($equals_parameters_) == 0) return $class_data_;
		}
		return null;
	}

	static function add_plugin_menu($attr_){
		$slug_ = isset($attr_["page"]) ? $attr_["page"] : "";
		$title_ = isset($attr_["title"]) ? $attr_["title"] : "";
		$class_ = isset($attr_["class"]) ? $attr_["class"] : "";

		if(!strlen($slug_) || !strlen($title_) || !strlen($class_)){
			wp_die("잘못된 접근");
			return;
		}

		$sub_title_ = isset($attr_["sub_title"]) ? $attr_["sub_title"] : "";
		$parameters_ = isset($attr_["param"]) ? $attr_["param"] : array();
		$equals_parameters_ = isset($attr_["eparam"]) ? $attr_["eparam"] : array();
		$permission_ = isset($attr_["permission"]) ? $attr_["permission"] : "read";
		$permission_class_ = isset($attr_["permission_class"]) ? $attr_["permission_class"] : null;
		$parent_ = isset($attr_["parent"]) ? $attr_["parent"] : null;
		$icon_ = isset($attr_["icon"]) ? $attr_["icon"] : "dashicons-admin-generic";
		$position_ = isset($attr_["position"]) ? $attr_["position"] : null;
		$screen_options_ = isset($attr_["screen_options"]) ? $attr_["screen_options"] : array();

		foreach($screen_options_ as $key_ => $row_data_){
			$screen_options_[$key_] = array_merge(array("option" => strtolower($class_."_".$key_)),$row_data_);
		}

		global $_pb_class_map;
		if(!isset($_pb_class_map)){
			$_pb_class_map = array();

			$defaults_map_ = apply_filters("pb_menu_defaults_map",array());
			foreach($defaults_map_ as $row_index_ => $default_){
				self::add_plugin_menu($default_);
			}
		}
			
		$menu_item_ = array(
			"slug" => $slug_,
			"title" => $title_,
			"sub_title" => $sub_title_,
			"class" => $class_,
			"parameters" => $parameters_,
			"equals_parameters" => $equals_parameters_,
			"permission" => $permission_,
			"permission_class" => $permission_class_,
			"parent" => $parent_,
			"icon" => $icon_,
			"position" => $position_,
			"screen_options" => $screen_options_,
		);

		array_push($_pb_class_map, $menu_item_);

		if(count($parameters_) == 0 && count($equals_parameters_) == 0){
			add_filter("set-screen-option",array("PB", "_set_screen_option"), 10, 3);
		}

		return $menu_item_;
	}

	static function _add_plugin_menu(){
		global $_pb_class_map;
		if(!isset($_pb_class_map))
			return;

		//add parent first
		foreach($_pb_class_map as $row_index_ => $class_data_){
			if(count($class_data_["parameters"]) > 0 || count($class_data_["equals_parameters"]) > 0) continue; //pass root class only

			$slug_ = $class_data_["slug"];
			$title_ = $class_data_["title"];
			$sub_title_ = $class_data_["sub_title"];
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$permission_ = $class_data_["permission"];
			$permission_class_ = $class_data_["permission_class"];
			$parent_ = $class_data_["parent"];
			$icon_ = $class_data_["icon"];
			$position_ = $class_data_['position'];
			$hook_ = null;

			if(!apply_filters("before_add_plugin_menu", true, $class_data_)){
				continue;
			}

			if(!pb_is_admin() && strlen($permission_class_) && call_user_func_array($permission_class_, array($slug_)) !== false){
				continue;
			}

			if(!strlen($parent_)){
				$hook_ = add_menu_page($title_, $title_, $permission_, $slug_, array("PB", "handle_plugin_menu"), $icon_, $position_);
				$sub_title_ = strlen($sub_title_) ? $sub_title_ : $title_;
				add_submenu_page($slug_, $sub_title_, $sub_title_, $permission_, $slug_);
				add_action("load-$hook_",array("PB", "_initialize"));
				$_pb_class_map[$row_index_]["hook"] = $hook_;
			}
		}

		//add submenu
		foreach($_pb_class_map as $row_index_ => $class_data_){
			if(count($class_data_["parameters"]) > 0 || count($class_data_["equals_parameters"]) > 0) continue; //pass root class only

			if(!apply_filters("before_add_plugin_menu", true, $class_data_)){
				continue;
			}

			$slug_ = $class_data_["slug"];
			$title_ = $class_data_["title"];
			$sub_title_ = $class_data_["sub_title"];
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$permission_ = $class_data_["permission"];
			$permission_class_ = $class_data_["permission_class"];
			$parent_ = $class_data_["parent"];
			$icon_ = $class_data_["icon"];
			$hook_ = null;

			if(!pb_is_admin() && strlen($permission_class_) && call_user_func_array($permission_class_, array($slug_)) !== false){
				continue;
			}

			if(strlen($parent_)){
				$hook_ = add_submenu_page($parent_, $title_, $title_, $permission_, $slug_, array("PB", "handle_plugin_menu"));
				add_action("load-$hook_",array("PB", "_initialize"));
				$_pb_class_map[$row_index_]["hook"] = $hook_;
			}
		}

		//set hook to children
		foreach($_pb_class_map as $row_index_ => $class_data_){
			if(count($class_data_["parameters"]) && count($class_data_["equals_parameters"]) == 0) continue; //pass child class only

			if(!apply_filters("before_add_plugin_menu", true, $class_data_)){
				continue;
			}

			$slug_ = $class_data_["slug"];
			$root_class_data_ = self::_get_root_class_data($slug_);
			$hook_ = $root_class_data_["hook"];

			$_pb_class_map[$row_index_]["hook"] = $hook_;
		}
	}

	static function _initialize(){
		$current_class_data_ = self::get_current_class_data();
		if(isset($current_class_data_)){
			$screen_options_ = $current_class_data_["screen_options"];
			foreach($screen_options_ as $key_ => $row_data_){
				add_screen_option($key_, $row_data_);
			}
			call_user_func(array($current_class_data_["class"], "_initialize"));
		}
	}
	static function _set_screen_option($status_, $option_, $value_){
		$current_class_data_ = self::get_current_class_data();
		if(isset($current_class_data_)){
			$screen_options_ = $current_class_data_["screen_options"];
			foreach($screen_options_ as $key_ => $row_data_){
				if($row_data_["option"] === $option_)
					return $value_;
			}
		}

		return $status_;
	}

	static function handle_plugin_menu(){
		$current_class_data_ = self::get_current_class_data();
		if(isset($current_class_data_)){
			call_user_func(array($current_class_data_["class"], "_draw_view"));
		}
	}
}

add_action('admin_menu', array('PB','_add_plugin_menu'));
add_action('template_redirect', array('PB','_fire_action_for_template'));
add_action('after_switch_theme', array('PB','_event_activated'));

function pb_is_devmode(){
	return (defined("PBDEV") && PBDEV === true);
}

include_once(PB_THEME_LIB_DIR_PATH.'PB-install.php');
include_once(PB_THEME_LIB_DIR_PATH.'class.PB-common.php');

if(is_admin()){
	include_once(PB_THEME_LIB_DIR_PATH.'class.PB-admin.php');
}

//xmlrpc 취약점 보안
add_filter('xmlrpc_methods', function($methods_){
	unset($methods_['pingback.ping']);
	return $methods_;
});

?>