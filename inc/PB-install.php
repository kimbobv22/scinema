<?php

add_action('pb_activated', function(){
	_pb_install_db_table();
});

function _pb_install_db_table(){
	global $wpdb;

	include_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$table_prefix_ = $wpdb->base_prefix;
	$table_charset_ = $wpdb->get_charset_collate();

	$install_querys_ = apply_filters('pb_install_db_table', array(), $table_prefix_, $table_charset_);

	foreach($install_querys_ as $query_){
		dbDelta($query_);
	}

	do_action("pb_db_table_installed");
}

add_action('after_setup_theme', function(){
	show_admin_bar(false);
});

require_once PB_THEME_DIR_PATH . 'class-tgm-plugin-activation.php';

function _pb_register_tgmpa(){
	$plugins_ = array(
		array(
			'name'		  => 'Slider Revolution',
			'slug'		  => 'revslider',
			'source'			=> PB_THEME_DIR_PATH . 'plugins/revslider.zip',
			'required'		  => true,
			'version'		   => '5.4',
			'force_activation'	  => false,
			'force_deactivation'	=> false,
			'external_url'	  => '',
		),
		array(
			'name'		  => 'Visual Composer',
			'slug'		  => 'js_composer',
			'source'			=> PB_THEME_DIR_PATH . 'plugins/js_composer.zip',
			'required'		  => true,
			'version'		   => '5.2.1',
			'force_activation'	  => false,
			'force_deactivation'	=> false,
			'external_url'	  => '',
		),
		array(
			'name'		  => 'TINYMCE Table',
			'slug'		  => 'mce-table-buttons',
			'source'			=> PB_THEME_DIR_PATH . 'plugins/mce-table-buttons.zip',
			'required'		  => true,
			'version'		   => '3.2',
			'force_activation'	  => false,
			'force_deactivation'	=> false,
			'external_url'	  => '',
		),
		
	);
  
	$config_ = array(
		'id'           => PBDOMAIN,
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
	);
	
	tgmpa($plugins_, $config_);
}

add_action('tgmpa_register', '_pb_register_tgmpa');

add_action( 'vc_before_init', function(){
	vc_set_as_theme();
});

define('PB_MAINMENU', 'pbmainmenu');

add_action('init',function(){
	register_nav_menu(PB_MAINMENU,__('메인메뉴'));
});
add_action('widgets_init', function(){
	
	register_sidebar( array(
		'name' => '서브메뉴위젯',
		'id' => 'pb-submenu-widget',
		'before_widget' => '<div id="%1$s" class="submenu-widget-item  %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
});



?>