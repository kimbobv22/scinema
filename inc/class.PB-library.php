<?php

global $pb_lib_map;
$pb_lib_map = array();

define('PB_LIBRARY_R_ALL', '000');
define('PB_LIBRARY_R_JS', '001');
define('PB_LIBRARY_R_CSS', '003');

class PB_library{
			
	static function _initialize(){
		wp_register_style("pb-main", (pb_library_url() . 'css/pb-main.css'),array(),'190612A');

		wp_register_style("pb-admin", (pb_library_url() . 'css/pb-admin.css'));
		wp_register_style("jquery-ui", (pb_library_url() . 'js/jquery.ui/jquery-ui.css'));
		
		if(!is_admin() && !in_array( $GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))){
			wp_deregister_script('jquery');
			wp_register_script('jquery', (pb_library_url() . 'js/jquery.js'));
		}
		
		wp_register_script("pb-all-main", (pb_library_url() . 'comp-lib/all-main.js') ,array("jquery"),'190502A');	


		wp_register_script("pb-all-admin", (pb_library_url() . 'comp-lib/all-admin.js') ,array("jquery"));
		wp_register_script("jquery-ui", (pb_library_url() . 'js/jquery.ui/jquery-ui.js'),array("jquery"));

		wp_register_style("jquery-multi-select", (pb_library_url() . 'js/modules/jquery.multi-select/jquery.multi-select.css'));
		wp_register_script("jquery-multi-select", (pb_library_url() . 'js/modules/jquery.multi-select/jquery.multi-select.js'),array("jquery"));


		
		wp_register_script("pb-main-dev", (pb_library_url() . 'js/pb-dev.js'), array("jquery","pb-all-main"));
		wp_register_script("pb-admin-dev", (pb_library_url() . 'js/pb-dev.js'), array("jquery","pb-all-admin"));

	do_action("pb_library_initialize");

		$pbvar_ = apply_filters("pb_script_common_var", array(
			"blogname" => get_bloginfo('name'),
			"homeurl" => home_url(),
			"is_user_logged_in" => is_user_logged_in() ? "Y" : "N",
			"is_admin" => is_admin(),
		));

		wp_localize_script("pb-all-main", "PBVAR", $pbvar_);
		wp_localize_script("pb-all-admin", "PBVAR", $pbvar_);

		$main_libraries_ = array(
			"script" => array(
				"jquery",
				"pb-all-main",
			),
			"style" => array(
				"pb-main",
			),
		);

		$admin_libraries_ = array(
			"script" => array(
				"jquery",
				"pb-all-admin",
			),
			"style" => array(
				"pb-admin",
			),
		);


		if(pb_is_devmode()){
			$main_libraries_["script"][] = "pb-main-dev";
			$admin_libraries_["script"][] = "pb-admin-dev";
		}

		self::register("main",$main_libraries_);
		self::register("admin",$admin_libraries_);

		self::register("jquery-multi-select",array(
			'script' => array(
				'jquery',
				'jquery-multi-select',
			),
			'style' => array(
				'jquery-multi-select',
			),
		));


		self::register("jquery-ui-with-plugin",array(
			"script" => array(
				"jquery",
				"jquery-ui",
			),
			"style" => array(
				"jquery-ui",
			),
		));

	}

	static function register($key_, $data_){
		global $pb_lib_map;
		$pb_lib_map[$key_] = $data_;
		return $pb_lib_map[$key_];
	}
	static function deregister($key_){
		global $pb_lib_map;
		unset($pb_lib_map[$key_]);
	}

	static function get($key_){
		global $pb_lib_map;
		if(!isset($pb_lib_map[$key_])) return false;
		return $pb_lib_map[$key_];
	}

	static function merge($key_, $data_){
		global $pb_lib_map;
		if(!isset($pb_lib_map[$key_])) return false;
		$pb_lib_map[$key_] = array_merge($pb_lib_map[$key_], $data_);
		return $pb_lib_map[$key_];
	}

	static function load($key_, $return_option_ = PB_LIBRARY_R_JS){
		global $pb_lib_map;
		$data_ = $pb_lib_map[$key_];
		if(!isset($data_)) return;

		$script_ = isset($data_["script"]) ? $data_["script"] : array();
		$style_ = isset($data_["style"]) ? $data_["style"] : array();
		$library_ = isset($data_["library"]) ? $data_["library"] : array();
		$result_ = array();
		foreach($library_ as $lib_name_){
			$result_ = array_merge($result_, self::load($lib_name_, $return_option_));
		}
		foreach($script_ as $script_name_){
			if(!wp_script_is($script_name_)){
				wp_enqueue_script($script_name_);
				do_action("pb_load_script_".$script_name_);	
			}
		}
		foreach($style_ as $style_name_){
			if(!wp_style_is($style_name_)){
				wp_enqueue_style($style_name_);
				do_action("pb_load_style_".$style_name_);
			}
		}

		switch($return_option_){
			case PB_LIBRARY_R_JS : return array_merge($result_, $script_); break;
			case PB_LIBRARY_R_CSS : return array_merge($result_, $style_); break;
			case PB_LIBRARY_R_ALL : return array_merge($result_, array(
				'style' => $style_,
				'script' => $script_,
			)); break;
		}

	}
}



function pb_library_load($key_, $return_option_ = PB_LIBRARY_R_JS){
	return PB_library::load($key_, $return_option_);
}

function pb_library_url(){
	return PB_THEME_DIR_URL."lib/".(pb_is_devmode() ? "dev/" : "dist/");
}

add_action("init", array("PB_library","_initialize"));

?>