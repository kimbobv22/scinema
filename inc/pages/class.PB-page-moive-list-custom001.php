<?php


add_filter('pb_content_template', function($content_template_){
	$current_cinema_ = pb_current_cinema();

	if($current_cinema_->ticket_ref_type !== "custom001"){
		return $content_template_;
	}

	$pb_movie_list_action = get_query_var("_pb_movie_list_action");

	switch($pb_movie_list_action){
		case PB_MOVIE_VIEW_SLUG : 
			return "movie/view-custom001";
		default : return $content_template_;

	}
},999);

add_action('pb_template',function(){
	if(get_query_var("_pb_movie_timetable_action") !== PB_MOVIE_TIMETABLE_SLUG){
		return;
	}

	$current_cinema_ = pb_current_cinema();

	if($current_cinema_->ticket_ref_type === "custom001"){
		wp_redirect("http://ticket.yd21.go.kr/rsvc/rsv_mv.html?b_id=rainbow");
		die();
	}

	
});


?>