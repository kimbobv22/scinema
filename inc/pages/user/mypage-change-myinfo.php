<?php

add_action('pb_template_user_mypage_change-myinfo', function(){
	wp_enqueue_style("page-user-mypage-change-myinfo", (pb_library_url() . 'css/pages/user/mypage-change-myinfo.css'));
	wp_enqueue_script("page-user-mypage-change-myinfo", (pb_library_url() . 'js/pages/user/mypage-change-myinfo.js'), array("pb-all-main"));
});

?>