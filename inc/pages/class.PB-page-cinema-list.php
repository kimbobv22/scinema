<?php

if(!pb_cinema_current_is_head_office()) return;

define('PB_CINEMA_LIST_SLUG', 'cinema-list');

function pb_cinema_list_url(){
	return home_url(PB_CINEMA_LIST_SLUG);
}
add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_CINEMA_LIST_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_cinema_list_action%', '([^&]+)');
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_CINEMA_LIST_SLUG.'/?$','index.php?_pb_cinema_list_action='.PB_CINEMA_LIST_SLUG,'top');
});


add_action('pb_template',function(){
	
	global $pb_cinema_list_action;
	$pb_cinema_list_action = get_query_var("_pb_cinema_list_action");

	switch($pb_cinema_list_action){
		case PB_CINEMA_LIST_SLUG : 

			add_filter('pb_post_with_sidemenu', function($result_){
				return true;
			});
			add_filter('pb_post_with_breadcrumb', function($result_){
				return true;
			});

			add_filter("pb_current_sidemenu_id", function($menu_id_){
				$mainmenu_list_ = pb_mainmenu_list();

				global $pb_cinema_list_action;

				$last_item_ = null;

				foreach($mainmenu_list_ as $menu_row_){
					if($menu_row_->type === $pb_cinema_list_action){

						if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
							$last_item_ = $menu_row_;
							break;
						}
						$last_item_ = $menu_row_;
					}
				}

				if(isset($last_item_)) return $last_item_->ID;

				return $menu_id_;
			});

			wp_enqueue_style("page-cinema-list", (pb_library_url() . 'css/pages/cinema/list.css'));
			wp_enqueue_script("page-cinema-list", (pb_library_url() . 'js/pages/cinema/list.js'), array("pb-all-main"));
	
		break;
		default : break;
	}
});

add_filter('pb_content_template', function($content_template_){

	if(!pb_cinema_current_is_head_office()) return $content_template_;

	global $pb_cinema_list_action;

	switch($pb_cinema_list_action){
		
		case PB_CINEMA_LIST_SLUG : 
			return "cinema/list";
		default : return $content_template_;

	}
});


add_filter('pb_common_navmenu_list', function($results_){
	$results_[] = (object) array(
		'ID' => 1,
		'db_id' => 0,
		'menu_item_parent' => 0,
		'object_id' => PB_CINEMA_LIST_SLUG,
		'post_parent' => 0,
		'type' => PB_CINEMA_LIST_SLUG,
		'object' => PB_CINEMA_LIST_SLUG,
		'type_label' => '영화관찾기',
		'title' => '영화관찾기',
		'url' => home_url(PB_CINEMA_LIST_SLUG),
		'target' => '',
		'attr_title' => '',
		'description' => '',
		'classes' => array(),
		'xfn' => '',
	);

	return $results_;
});


add_filter('wp_get_nav_menu_items', function($items_, $menu_, $args_){
	foreach($items_ as &$row_data_){
		if($row_data_->type === PB_CINEMA_LIST_SLUG){
			$row_data_->url = home_url(PB_CINEMA_LIST_SLUG);
		}
	}
	return $items_;
},10,3);

?>