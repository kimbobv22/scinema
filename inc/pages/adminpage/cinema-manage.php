<?php

add_action('pb_adminpage_template_cinema-manage',function($action_, $action_data_){


	switch($action_){
		case PB_ADMINPAGE_CINEMA_MANAGE_ACTION_EDIT : 

		if(!strlen($action_data_)){
			wp_die("잘못된 접근입니다.");
			return;
		}

		global $pb_cinema;

		$pb_cinema = pb_cinema($action_data_);

		if(empty($pb_cinema)){
			wp_die("잘못된 접근입니다.");
			return;
		}

		wp_enqueue_script("jquery-ui-core");
		wp_enqueue_script("jquery-ui-sortable");
		
		// 20190409 버전 관리를 위해 관련 해당 스트립트와 css에 현재시간 설정
		wp_enqueue_style("page-adminpage-cinema-manage-edit", (pb_library_url() . 'css/pages/adminpage/cinema-manage/edit.css'));
		wp_enqueue_script("daum-map", '//dapi.kakao.com/v2/maps/sdk.js?appkey=31a6156665633547295bc8c68dd3cbd4&libraries=services', array("pb-all-main"),'190409.01');
		wp_enqueue_script("page-adminpage-cinema-manage-edit", (pb_library_url() . 'js/pages/adminpage/cinema-manage/edit.js'), array('pb-all-main'),'190409.02');

		break;
		case PB_ADMINPAGE_CINEMA_MANAGE_ACTION_ADD : 

			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");
			
			// 20190409 버전 관리를 위해 관련 해당 스트립트와 css에 현재시간 설정
			wp_enqueue_style("page-adminpage-cinema-manage-edit", (pb_library_url() . 'css/pages/adminpage/cinema-manage/edit.css'));
			wp_enqueue_script("daum-map", '//dapi.kakao.com/v2/maps/sdk.js?appkey=31a6156665633547295bc8c68dd3cbd4&libraries=services', array("pb-all-main"),'190409.01');
			wp_enqueue_script("page-adminpage-cinema-manage-edit", (pb_library_url() . 'js/pages/adminpage/cinema-manage/edit.js'), array('pb-all-main'),'190409.02');

		break;

		default:

			wp_enqueue_style("page-adminpage-cinema-manage-list", (pb_library_url() . 'css/pages/adminpage/cinema-manage/list.css'));
			wp_enqueue_script("page-adminpage-cinema-manage-list", (pb_library_url() . 'js/pages/adminpage/cinema-manage/list.js'), array("pb-all-main"));

		break;
	}

	/*global $pb_cinema_mange_action;

	wp_enqueue_style("page-adminpage-cinema-manage-list", (pb_library_url() . 'css/pages/adminpage/cinema-manage/list.css'));
	wp_enqueue_script("page-adminpage-cinema-manage-list", (pb_library_url() . 'js/pages/adminpage/cinema-manage/list.js'), array("pb-all-main"));*/
},10, 2);



?>