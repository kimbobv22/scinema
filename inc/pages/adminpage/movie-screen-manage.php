<?php

add_action('pb_adminpage_template_movie-screen-manage',function($action_, $action_data_){

	global $pb_movie_screen_manage_open_id_table;
		$pb_movie_screen_manage_open_id_table = new PB_find_movie_open_id_table("pb-movie-open-id-table", "pb-movie-open-id-table");

	add_action('wp_footer', "_pb_adminpage_template_movie_screen_manage_draw_movie_open_id_popup");

	switch($action_){
		case "add" : 
		case "edit" : 

			wp_enqueue_style("page-adminpage-movie-screen-manage-edit", (pb_library_url() . 'css/pages/adminpage/movie-screen-manage/edit.css'));
			wp_enqueue_script("page-adminpage-movie-screen-manage-edit", (pb_library_url() . 'js/pages/adminpage/movie-screen-manage/edit.js'), array('pb-all-main'));

		break;

		default:

			wp_enqueue_style("page-adminpage-movie-screen-manage-list", (pb_library_url() . 'css/pages/adminpage/movie-screen-manage/list.css'));
			wp_enqueue_script("page-adminpage-movie-screen-manage-list", (pb_library_url() . 'js/pages/adminpage/movie-screen-manage/list.js'), array("pb-all-main"));

		break;
	}

	
},10, 2);


function _pb_adminpage_template_movie_screen_manage_draw_movie_open_id_popup(){
	?>



<div class="pb-kofic-find-open-id-modal modal" id="pb-find-movie-open-id-modal"><form method="get" id="pb-find-movie-open-id-modal-form">
	<input type="hidden" name="cinema_id" value="">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">개봉영화 검색</h4>
			</div>
			<div class="modal-body">
				<div class="search-condition">
					<div class="input-group input-group-lg">
						<input type="search" class="form-control" placeholder="검색어 입력" name="keyword">
						<span class="input-group-btn">
						<button class="btn btn-default" type="submit">검색</button>
					  </span>
					</div>

				

				</div>	

				<?php
					global $pb_movie_screen_manage_open_id_table;
					$pb_movie_screen_manage_open_id_table->set_ajax(true);
					echo $pb_movie_screen_manage_open_id_table->html();
				?>
				

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</form></div>

	<?php
}

?>