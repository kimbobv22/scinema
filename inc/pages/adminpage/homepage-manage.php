<?php

add_action('pb_adminpage_template_homepage-manage',function($action_, $action_data_){
	wp_enqueue_script("jquery-ui-core");
	wp_enqueue_script("jquery-ui-sortable");

	//wp_enqueue_style("page-adminpage-homepage-manage", (pb_library_url() . 'css/pages/adminpage/homepage-manage.css'));
	wp_enqueue_script("page-adminpage-homepage-manage", (pb_library_url() . 'js/pages/adminpage/homepage-manage.js'), array('pb-all-main', 'jquery-ui-sortable'));
},10, 2);


?>