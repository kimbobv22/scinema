<?php

add_action('pb_adminpage_template_cinema-seat-manage',function($action_, $action_data_){
	
	if(pb_cinema_current_is_head_office()){
		switch($action_){
			case 'dtl' : 



			if(!strlen($action_data_)){
				wp_die("잘못된 접근입니다.");
				return;
			}

			global $pb_cinema, $pb_cinema_seat;

			$pb_cinema = pb_cinema($action_data_);

			if(empty($pb_cinema)){
				wp_die("잘못된 접근입니다.");
				return;
			}



			$seat_id_ = isset($_GET["seat_id"]) ? $_GET["seat_id"] : null;

			if(strlen($seat_id_)){
				$pb_cinema_seat = pb_cinema_seat($seat_id_);

				if(empty($pb_cinema_seat)){
					wp_die("잘못된 접근입니다.");
					return;
				}

				wp_enqueue_script("jquery-ui-core");
				wp_enqueue_script("jquery-ui-sortable");

				wp_enqueue_style("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'css/pages/adminpage/cinema-seat-manage/dtl-edit.css'));
				wp_enqueue_script("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'js/pages/adminpage/cinema-seat-manage/dtl-edit.js'), array('pb-all-main', 'jquery-ui-sortable'));


			}else if(isset($_GET['new']) && $_GET['new'] === "Y"){
				$pb_cinema_seat = (object)array(
					'seat_id' => null,
					'seat_name' => null,
					'seat_desc' => null,
					'seat_data' => null,
					'sort_num' => null,
					'status' => '00001',
					'replace_image_yn' => 'N',
					'replace_image_url' => null,
				);

				wp_enqueue_script("jquery-ui-core");
				wp_enqueue_script("jquery-ui-sortable");

				wp_enqueue_style("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'css/pages/adminpage/cinema-seat-manage/dtl-edit.css'));
				wp_enqueue_script("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'js/pages/adminpage/cinema-seat-manage/dtl-edit.js'), array('pb-all-main', 'jquery-ui-sortable'));

			}else{
				wp_enqueue_style("page-adminpage-cinema-seat-manage-dtl-list", (pb_library_url() . 'css/pages/adminpage/cinema-seat-manage/dtl-list.css'));
				wp_enqueue_script("page-adminpage-cinema-seat-manage-dtl-list", (pb_library_url() . 'js/pages/adminpage/cinema-seat-manage/dtl-list.js'), array('pb-all-main', 'jquery-ui-sortable'));
			}

			
			break;
			
			default:

				wp_enqueue_style("page-adminpage-cinema-seat-manage-list", (pb_library_url() . 'css/pages/adminpage/cinema-seat-manage/list.css'));
				wp_enqueue_script("page-adminpage-cinema-seat-manage-list", (pb_library_url() . 'js/pages/adminpage/cinema-seat-manage/list.js'), array("pb-all-main"));

			break;
		}	
	}else{
		

		global $pb_cinema, $pb_cinema_seat;

		$pb_cinema = pb_cinema(pb_current_cinema_id());

		$seat_id_ = isset($_GET["seat_id"]) ? $_GET["seat_id"] : null;

		if(strlen($seat_id_)){
			$pb_cinema_seat = pb_cinema_seat($seat_id_);

			if(empty($pb_cinema_seat)){
				wp_die("잘못된 접근입니다.");
				return;
			}

			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");

			wp_enqueue_style("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'css/pages/adminpage/cinema-seat-manage/dtl-edit.css'));
			wp_enqueue_script("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'js/pages/adminpage/cinema-seat-manage/dtl-edit.js'), array('pb-all-main', 'jquery-ui-sortable'));


		}else if(isset($_GET['new']) && $_GET['new'] === "Y"){
			$pb_cinema_seat = (object)array(
				'seat_id' => null,
				'seat_name' => null,
				'seat_desc' => null,
				'seat_data' => null,
				'sort_num' => null,
				'status' => '00001',
				'replace_image_yn' => 'N',
				'replace_image_url' => null,
			);

			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");

			wp_enqueue_style("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'css/pages/adminpage/cinema-seat-manage/dtl-edit.css'));
			wp_enqueue_script("page-adminpage-cinema-seat-manage-dtl-edit", (pb_library_url() . 'js/pages/adminpage/cinema-seat-manage/dtl-edit.js'), array('pb-all-main', 'jquery-ui-sortable'));

		}else{
			wp_enqueue_style("page-adminpage-cinema-seat-manage-dtl-list", (pb_library_url() . 'css/pages/adminpage/cinema-seat-manage/dtl-list.css'));
			wp_enqueue_script("page-adminpage-cinema-seat-manage-dtl-list", (pb_library_url() . 'js/pages/adminpage/cinema-seat-manage/dtl-list.js'), array('pb-all-main', 'jquery-ui-sortable'));
		}

			
	}

	
},10, 2);



?>