<?php

add_action('pb_adminpage_template_banner-manage',function($action_, $action_data_){

	if(pb_cinema_current_is_head_office()){
		switch($action_){
			case 'edit' : 

			if(!strlen($action_data_)){
				wp_die("잘못된 접근입니다.");
				return;
			}

			global $pb_cinema;

			$pb_cinema = pb_cinema($action_data_);

			if(empty($pb_cinema)){
				wp_die("잘못된 접근입니다.");
				return;
			}

			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");

			wp_enqueue_style("page-adminpage-banner-manage-edit", (pb_library_url() . 'css/pages/adminpage/banner-manage/edit.css'));
			wp_enqueue_script("page-adminpage-banner-manage-edit", (pb_library_url() . 'js/pages/adminpage/banner-manage/edit.js'), array('pb-all-main', 'jquery-ui-sortable'));

			break;
			
			default:

				wp_enqueue_style("page-adminpage-banner-manage-list", (pb_library_url() . 'css/pages/adminpage/banner-manage/list.css'));
				wp_enqueue_script("page-adminpage-banner-manage-list", (pb_library_url() . 'js/pages/adminpage/banner-manage/list.js'), array("pb-all-main"));

			break;
		}	
	}else{
		global $pb_cinema;

		$pb_cinema = pb_cinema(pb_current_cinema_id());

		if(empty($pb_cinema)){
			wp_die("잘못된 접근입니다.");
			return;
		}

		wp_enqueue_script("jquery-ui-core");
		wp_enqueue_script("jquery-ui-sortable");

		wp_enqueue_style("page-adminpage-banner-manage-edit", (pb_library_url() . 'css/pages/adminpage/banner-manage/edit.css'));
		wp_enqueue_script("page-adminpage-banner-manage-edit", (pb_library_url() . 'js/pages/adminpage/banner-manage/edit.js'), array('pb-all-main', 'jquery-ui-sortable'));
	}

	
},10, 2);



?>