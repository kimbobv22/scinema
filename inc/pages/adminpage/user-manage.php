<?php

add_action('pb_adminpage_template_user-manage',function($action_, $action_data_){

	if(isset($_GET['excel']) && $_GET['excel'] === "Y"){

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		$user_list_ = pb_user_list(array(
			'keyword' => $keyword_,
			'without_admin' => true,
		));
		
		pb_excel_output("userinfo_list.xls", ((array)$user_list_), array(
			'사용자 아이디' => 'user_login',
			'사용자 이름' => 'full_name',
			'성별' => 'gender_name',
			'이메일' => 'user_email',
			'연락처' => 'phone1',
			'생년월일' => 'birth_yyyymmdd',
			'이메일 수신 동의 여부' => 'mailing_recv_yn',
			'SMS 수신 동의 여부' => 'sms_recv_yn',
		));

		die();
	}

	switch($action_){
		case 'edit' :
			wp_enqueue_script("page-adminpage-user-manage-edit", (pb_library_url() . 'js/pages/adminpage/user-manage/edit.js'));

		break;
		default : break;
	}

},10, 2);



?>