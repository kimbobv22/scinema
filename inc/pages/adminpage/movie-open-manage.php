<?php

add_action('pb_adminpage_template_movie-open-manage',function($action_, $action_data_){

	add_action('wp_footer', "_pb_adminpage_template_movie_open_manage_draw_kofic_popup");
	add_action('wp_footer', "_pb_adminpage_template_movie_open_manage_draw_publisher_popup");

	switch($action_){
		case "edit" : 

		wp_enqueue_style("page-adminpage-movie-open-manage-edit", (pb_library_url() . 'css/pages/adminpage/movie-open-manage/edit.css'));
		wp_enqueue_script("page-adminpage-movie-open-manage-edit", (pb_library_url() . 'js/pages/adminpage/movie-open-manage/edit.js'), array('pb-all-main'));



		break;
		case "add" : 

			wp_enqueue_style("page-adminpage-movie-open-manage-edit", (pb_library_url() . 'css/pages/adminpage/movie-open-manage/edit.css'));
			wp_enqueue_script("page-adminpage-movie-open-manage-edit", (pb_library_url() . 'js/pages/adminpage/movie-open-manage/edit.js'), array('pb-all-main'));

		break;

		default:

			wp_enqueue_style("page-adminpage-movie-open-manage-list", (pb_library_url() . 'css/pages/adminpage/movie-open-manage/list.css'));
			wp_enqueue_script("page-adminpage-movie-open-manage-list", (pb_library_url() . 'js/pages/adminpage/movie-open-manage/list.js'), array("pb-all-main"));

		break;
	}

	
},10, 2);


function _pb_adminpage_template_movie_open_manage_draw_kofic_popup(){
	?>



<div class="pb-kofic-find-db-modal modal" id="pb-kofic-find-db-modal"><form method="get" id="pb-kofic-find-db-modal-form">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">KOFIC DB검색</h4>
			</div>
			<div class="modal-body">
				<div class="search-condition">
					<div class="input-group input-group-lg">
						<input type="search" class="form-control" placeholder="검색어 입력" name="query" required>
						<span class="input-group-btn">
						<button class="btn btn-default" type="submit">검색</button>
					  </span>
					</div>

				</div>	
				<div class="find-results-list">
					<div class="no-results">검색된 결과가 없습니다.</div>
				</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</form></div>

	<?php
}

function _pb_adminpage_template_movie_open_manage_draw_publisher_popup(){

	$blog_id_ = get_current_blog_id();
	$current_cinema_data_ = pb_cinema($blog_id_);
	?>

<div class="pb-publisher-reg-modal modal" id="pb-publisher-reg-modal"><form method="get" id="pb-publisher-reg-form">
	
	<input type="hidden" name="client_status" value="00001">
	<input type="hidden" name="client_ctg" value="00005">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">배급사등록</h4>
			</div>
			<div class="modal-body">
				
				<div class="form-group">
					<label for="pb-publisher-reg-form-client_name">배급사명</label>
					<input type="text" class="form-control" name="client_name" id="pb-publisher-reg-form-client_name" placeholder="배급사명 입력" required data-error="배급사를 입력하세요">
				</div>
				<?php if($blog_id_ !== PB_CINEMA_HEAD_OFFICE_ID){ ?>
					<input type="hidden" name="cinema_id" value="<?=$blog_id_?>">
				<?php }else{

					$temp_cinema_list_ = pb_cinema_list(array(
						'status' => PB_CINEMA_STATUS_NORMAL,
						'include_head_office' => true,
					));
					$cinema_list_ = array("-1" => "공통");

					foreach($temp_cinema_list_ as $row_data_){
						$cinema_list_[$row_data_->cinema_id] = $row_data_->cinema_name;
					}	

				?>
					<div class="form-group">
						<label for="pb-publisher-reg-form-cinema_id">사용영화관</label>
						<select class="form-control" name="cinema_id" required data-error="영화관을 선택하세요">
							<option value="">-영화관선택-</option>
							<?php foreach($cinema_list_ as $key_ => $label_){ ?>
								<option value="<?=$key_?>"><?=$label_?></option>
							<?php } ?>
						</select>
					</div>
				<?php }?>
				

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
				<button type="submit" class="btn btn-default">등록하기</button>
			</div>
		</div>
	</div>
</form></div>

	<?php
}


?>