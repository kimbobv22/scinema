<?php

add_action('pb_adminpage_template_popup-manage',function($action_, $action_data_){


	switch($action_){
		case 'edit' : 

		if(!strlen($action_data_)){
			wp_die("잘못된 접근입니다.");
			return;
		}

		global $pb_popup;

		$pb_popup = pb_popup($action_data_);

		if(empty($pb_popup)){
			wp_die("잘못된 접근입니다.");
			return;
		}

		wp_enqueue_script("jquery-ui-core");
		wp_enqueue_script("jquery-ui-sortable");

		wp_enqueue_style("page-adminpage-popup-manage-edit", (pb_library_url() . 'css/pages/adminpage/popup-manage/edit.css'));
		wp_enqueue_script("page-adminpage-popup-manage-edit", (pb_library_url() . 'js/pages/adminpage/popup-manage/edit.js'), array('pb-all-main', 'jquery-ui-sortable'));

		break;
		case 'add' : 


			wp_enqueue_style("page-adminpage-popup-manage-edit", (pb_library_url() . 'css/pages/adminpage/popup-manage/edit.css'));
			wp_enqueue_script("page-adminpage-popup-manage-edit", (pb_library_url() . 'js/pages/adminpage/popup-manage/edit.js'), array('pb-all-main', 'jquery-ui-sortable'));

		break;

		default:

			wp_enqueue_style("page-adminpage-popup-manage-list", (pb_library_url() . 'css/pages/adminpage/popup-manage/list.css'));
			wp_enqueue_script("page-adminpage-popup-manage-list", (pb_library_url() . 'js/pages/adminpage/popup-manage/list.js'), array("pb-all-main"));

		break;
	}
},10, 2);



?>