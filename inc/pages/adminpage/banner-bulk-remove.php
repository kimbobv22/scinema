<?php
/*
 작성일자 : 2019.07.16
 작성자 : 나하나
 설명 : 관리자 배너 일괄삭제 - 페이지의 css, js 추가 기능
*/
	
add_action('pb_adminpage_template_banner-bulk-remove',function($action_, $action_data_){
	wp_enqueue_script("jquery-ui-core");
	wp_enqueue_script("jquery-ui-sortable");
	wp_enqueue_style("page-adminpage-banner-bulk-", (pb_library_url() . 'css/pages/adminpage/banner-manage/bulk-remove.css'),array(),'170718B');
	 wp_enqueue_script("page-adminpage-banner-bulk-remove", (pb_library_url() . 'js/pages/adminpage/banner-bulk-remove.js'), array("pb-all-main"),'170718A');
},10, 2);
?>