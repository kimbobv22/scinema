<?php
/*
 작성일자 : 2019.04.29
 작성자 : 나하나
 설명 : 관리자 배너 일괄등록 - 페이지의 css, js 추가 기능
*/
	
add_action('pb_adminpage_template_banner-bulk-write-new',function($action_, $action_data_){
	wp_enqueue_script("jquery-ui-core");
	wp_enqueue_script("jquery-ui-sortable");
	wp_enqueue_style("page-adminpage-banner-bulk-", (pb_library_url() . 'css/pages/adminpage/banner-bulk-write.css'),array(),'170911A');
	wp_enqueue_script("page-adminpage-banner-bulk-write", (pb_library_url() . 'js/pages/adminpage/banner-bulk-write-new.js'), array("pb-all-main"),'170911A');
},10, 2);
?>