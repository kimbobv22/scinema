<?php

add_action('pb_adminpage_template_sms-reg-manage',function($action_, $action_data_){

	if(isset($_GET['excel']) && $_GET['excel'] === "Y"){

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		$gender_ = isset($_GET["gender"]) ? $_GET["gender"] : null;
		$birth_year_srt_ = isset($_GET["birth_year_srt"]) ? $_GET["birth_year_srt"] : null;
		$birth_year_end_ = isset($_GET["birth_year_end"]) ? $_GET["birth_year_end"] : date("Y");
		$cinema_id_ = isset($_GET["cinema_id"]) ? $_GET["cinema_id"] : null;
		$phone_num_ = isset($_GET["phone_num"]) ? $_GET["phone_num"] : null;

		$user_list_ = pb_sms_reg_list(array(
			'keyword' => $keyword_,
			'gender' => $gender_,
			'birth_year_range' => array($birth_year_srt_, $birth_year_end_),
			'cinema_id' => $cinema_id_,
			'phone1_3' => $phone_num_,
		));

		foreach ($user_list_ as $rowdata_) {
			$rowdata_->phone_num = $rowdata_->phone1_1 . "-" . $rowdata_->phone1_2 . "-" . $rowdata_->phone1_3;
		}

		pb_excel_output("SMS_user_list.xls", ((array)$user_list_), array(
			'영화관' => 'cinema_name',
			'이름' => 'customer_name',
			'휴대전화번호' => 'phone_num',
			'성별' => 'gender_name',
			'출생년도' => 'birth_year',
			'등록일' => 'reg_date_ymd',
		));

		die();
	}

	switch($action_){
		case "add" : 
		case "edit" : 

			// wp_enqueue_style("page-adminpage-sms-reg-manage-edit", (pb_library_url() . 'css/pages/adminpage/sms-reg-manage/edit.css'));
			wp_enqueue_script("page-adminpage-sms-reg-manage-edit", (pb_library_url() . 'js/pages/adminpage/sms-reg-manage/edit.js'), array('pb-all-main'));

		break;

		default:

			// wp_enqueue_style("page-adminpage-sms-reg-manage-list", (pb_library_url() . 'css/pages/adminpage/sms-reg-manage/list.css'));
			wp_enqueue_script("page-adminpage-sms-reg-manage-list", (pb_library_url() . 'js/pages/adminpage/sms-reg-manage/list.js'), array("pb-all-main"));

		break;
	}

	
},10, 2);

?>