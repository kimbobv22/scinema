<?php
/*
 작성일자 : 2019.08.20
 작성자 : 나하나
 설명 : 관리자 메인배너 기능
*/
	
add_action('pb_adminpage_template_banner-main-manage',function($action_, $action_data_){
	switch($action_){
		case 'edit' : 
			if(!strlen($action_data_)){
				wp_die("잘못된 접근입니다.");
				return;
			}
			global $pb_banner_main;
			$pb_banner_main = pb_banner_main($action_data_);
			if(empty($pb_banner_main)){
				wp_die("잘못된 접근입니다.");
				return;
			}
			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");
			wp_enqueue_script("page-adminpage-banner-main-manage-edit", (pb_library_url() . 'js/pages/adminpage/banner-main-manage/edit.js'), array('pb-all-main'));
			wp_enqueue_style("page-adminpage-banner-main-manage-edit", (pb_library_url() . 'css/pages/adminpage/banner-main-manage/edit.css'));
		break;
		case 'add' : 
			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");
			wp_enqueue_script("page-adminpage-banner-main-manage-edit", (pb_library_url() . 'js/pages/adminpage/banner-main-manage/edit.js'), array('pb-all-main'));
			wp_enqueue_style("page-adminpage-banner-main-manage-edit", (pb_library_url() . 'css/pages/adminpage/banner-main-manage/edit.css'));
		break;
		default:
			wp_enqueue_script("jquery-ui-core");
			wp_enqueue_script("jquery-ui-sortable");
			wp_enqueue_script("page-adminpage-banner-main-manage-list", (pb_library_url() . 'js/pages/adminpage/banner-main-manage/list.js'), array('pb-all-main'),'20190823A');
		break;
	}
},10, 2);
?>