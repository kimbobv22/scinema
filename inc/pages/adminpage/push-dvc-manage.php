<?php

add_action('pb_adminpage_template_push-dvc-manage',function($action_, $action_data_){

	$file_name_ = null;

	$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;

	if(isset($_GET['excel']) && $_GET['excel'] === "Y"){

		$push_dvc_list_ = pb_push_dvc_list(array(
			'keyword' => $keyword_,
		));				

		$file_name_ = "push_dvc_list.xls";

		pb_excel_output($file_name_, (array)$push_dvc_list_, array(
			'UID' => 'UID',
			'영화관' => 'cinema_name',
			'디바이스구분' => 'dvc_type_text',
			'기기모델' => 'dvc_model',
			'기기버젼' => 'dvc_version',

			'등록일자' => 'reg_date_ymdhi',
			

		));
		die();
	}

	wp_enqueue_script("page-adminpage-push-dvc-manage-list", (pb_library_url() . 'js/pages/adminpage/push-dvc-manage/list.js'), array("pb-all-main"));
	

},10, 2);



?>