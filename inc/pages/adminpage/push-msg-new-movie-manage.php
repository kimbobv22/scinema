<?php

add_action('pb_adminpage_template_push-msg-new-movie-manage',function($action_, $action_data_){
	
	switch($action_){
		case 'detail' : 

		if(!strlen($action_data_)){
			wp_die("잘못된 접근입니다.");
			return;
		}

		global $pb_push_msg;

		$pb_push_msg = pb_push_msg($action_data_);

		if(empty($pb_push_msg)){
			wp_die("잘못된 접근입니다.");
			return;
		}

		wp_enqueue_script("jquery-ui-core");
		wp_enqueue_script("jquery-ui-sortable");

		wp_enqueue_style("page-adminpage-push-msg-new-movie-list", (pb_library_url() . 'css/pages/adminpage/push-msg-manage/new-movie/list.css'));
		wp_enqueue_script("page-adminpage-push-msg-new-movie-list", (pb_library_url() . 'js/pages/adminpage/push-msg-manage/new-movie/list.js'), array("pb-all-main"));
		wp_enqueue_script("page-adminpage-push-msg-new-movie-edit", (pb_library_url() . 'js/pages/adminpage/push-msg-manage/new-movie/detail.js'), array('pb-all-main', 'jquery-ui-sortable', "page-adminpage-push-msg-new-movie-list"));

		break;
		case 'add' : 

			wp_enqueue_style("page-adminpage-push-msg-new-movie-list", (pb_library_url() . 'css/pages/adminpage/push-msg-manage/new-movie/list.css'));


			//wp_enqueue_style("page-adminpage-push-msg-manage-add", (pb_library_url() . 'css/pages/adminpage/push-msg-manage/add.css'));
			wp_enqueue_script("page-adminpage-push-msg-new-movie-add", (pb_library_url() . 'js/pages/adminpage/push-msg-manage/new-movie/add.js'), array('pb-all-main', 'jquery-ui-sortable'));

		break;

		default:

			wp_enqueue_style("page-adminpage-push-msg-new-movie-list", (pb_library_url() . 'css/pages/adminpage/push-msg-manage/new-movie/list.css'));
			wp_enqueue_script("page-adminpage-push-msg-new-movie-list", (pb_library_url() . 'js/pages/adminpage/push-msg-manage/new-movie/list.js'), array("pb-all-main"));

		break;
	}

},10, 2);



?>