<?php

define('PB_MOVIE_XTICKETING_SLUG', 'xticketing');
define('PB_MOVIE_XTICKETING_CHECK_SLUG', 'xticketing-check');

add_filter('pb_script_common_var', function($data_){
	$data_['xticketing_url'] = home_url(PB_MOVIE_XTICKETING_SLUG);
	$data_['xticketing_check_url'] = home_url(PB_MOVIE_XTICKETING_CHECK_SLUG);

	return $data_;
});

function pb_movie_xticketing_url($schedule_id_){
	return pb_append_url(home_url(PB_MOVIE_XTICKETING_SLUG), $schedule_id_);
}
function pb_movie_xticketing_check_url(){
	return home_url(PB_MOVIE_XTICKETING_CHECK_SLUG);
}

add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_MOVIE_XTICKETING_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_movie_xticketing%', '([^&]+)');
	add_rewrite_tag('%_pb_movie_xticketing_schedule_id_id%', '([^&]+)');
	
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_MOVIE_XTICKETING_SLUG.'/(.+)/?$','index.php?_pb_movie_xticketing='.PB_MOVIE_XTICKETING_SLUG.'&_pb_movie_xticketing_schedule_id_id=$matches[1]','top');
	add_rewrite_rule('^'.PB_MOVIE_XTICKETING_CHECK_SLUG.'/?$','index.php?_pb_movie_xticketing='.PB_MOVIE_XTICKETING_CHECK_SLUG,'top');
});


add_action('template_redirect',function(){
	$xticket_aciton_ = get_query_var("_pb_movie_xticketing");
	if($xticket_aciton_ !== PB_MOVIE_XTICKETING_SLUG){
		return;
	}

	$schedule_id_ = get_query_var("_pb_movie_xticketing_schedule_id_id");
	header("Content-Type: text/html; charset=UTF-8");

	$cinema_id_ = isset($_REQUEST["cinema_id"]) ? $_REQUEST["cinema_id"] : null;
	$list_yn_ = "N";
	
	if(strlen($cinema_id_)){
		$cinema_data_ = pb_cinema($cinema_id_);
		$cinema_id_ = $cinema_data_->xticket_ref_code; 
	}

	include_once (PB_THEME_LIB_DIR_PATH . 'pages/xticket/lib.php');

	$spcode_ = "SP0019";
	$is_mobile_ = pb_is_mobile();
	$api_url_ = null;
	$store_code_ = ($is_mobile_ ? "06": "01");
	$encrypt_key_ = "CJ".$spcode_;

	$for_app_ = isset($_REQUEST['_app']) ? $_REQUEST['_app'] : "N";


	// if(pb_is_devmode()){
	// 	// $api_url_ = ($is_mobile_ ? "http://210.122.98.243/Ticketing/MRIALoader.aspx" : "http://210.122.98.243/Ticketing/RIALoader.aspx");
	// 	$api_url_ = ($is_mobile_ ? "https://www.xticket.co.kr/Ticketing/MRIALoader.aspx" : "https://www.xticket.co.kr/Ticketing/RIALoader.aspx");
	// }else{
	// 	$api_url_ = ($is_mobile_ ? "https://www.xticket.co.kr/Ticketing/MRIALoader.aspx" : "https://www.xticket.co.kr/Ticketing/RIALoader.aspx");
	// }

	$api_url_ = ($is_mobile_ ? "https://www.xticket.co.kr/Ticketing/MRIALoader.aspx" : "https://www.xticket.co.kr/Ticketing/RIALoader.aspx");

	// header("Content-Type: text/html; charset=UTF-8");
?>

<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
	<title>작은영화관</title>
</head>
<body onload="_pb_xticket_submit();">
<form id="pb-xticket-submit-form" action="<?=$api_url_?>" method="POST" accept-charset="UTF-8">
	<input type="hidden" name="companyCd" value="<?=$spcode_?>">
	<input type="hidden" name="scheCd" value="<?=$schedule_id_?>">
	<input type="hidden" name="performanceId" value="<?=$schedule_id_?>">
	
	<?php if($cinema_id_){ ?>
		<input type="hidden" name="planCompanyCd" value="<?=$cinema_id_?>">
	<?php } ?>


	<input type="hidden" name="storeCd" value="<?=$store_code_?>">
	<input type="hidden" name="authKey" value="<?=encryption($encrypt_key_, "cjsystems")?>">
	<input type="hidden" name="encFlag" value="N">
	<input type="hidden" name="listYn" value="N"> 
	<input type="hidden" name="riaType" value="G">

	<?php if($for_app_ === "Y"){ ?>
	<input type="hidden" name="_for_app" value="Y">
	<?php } ?>
		
	<?php if(is_user_logged_in()){

		$user_data_ = pb_user(get_current_user_id());

		$s_user_id_ = $user_data_->user_login;
		$s_user_name_ = $user_data_->full_name;
		$s_pin_code_ = $user_data_->phone1_1.$user_data_->phone1_2.$user_data_->phone1_3;
		$s_phone_num_ = $user_data_->phone1_1.$user_data_->phone1_2.$user_data_->phone1_3;

		$s_user_id_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_user_id_)));
		$s_user_name_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_user_name_)));
		$s_pin_code_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_pin_code_)));
		$s_phone_num_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_phone_num_)));

		$s_user_id_ = str_replace("\n",'',$s_user_id_);
		$s_user_name_ = str_replace("\n",'',$s_user_name_);
		$s_pin_code_ = str_replace("\n",'',$s_pin_code_);
		$s_phone_num_ = str_replace("\n",'',$s_phone_num_);

	}else{
		$name_ = $_REQUEST["full_name"];
		$phone1_1_ = $_REQUEST["phone1_1"];
		$phone1_2_ = $_REQUEST["phone1_2"];
		$phone1_3_ = $_REQUEST["phone1_3"];

		$name_ = trim($name_);
		$phone1_1_ = trim($phone1_1_);
		$phone1_2_ = trim($phone1_2_);
		$phone1_3_ = trim($phone1_3_);

		$s_user_id_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $name_));
		$s_user_name_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $name_));
		$s_pin_code_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $phone1_1_.$phone1_2_.$phone1_3_));
		$s_phone_num_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $phone1_1_.$phone1_2_.$phone1_3_));

		$s_user_id_ = str_replace("\n",'',$s_user_id_);
		$s_user_name_ = str_replace("\n",'',$s_user_name_);
		$s_pin_code_ = str_replace("\n",'',$s_pin_code_);
		$s_phone_num_ = str_replace("\n",'',$s_phone_num_);

	} ?>




	<input type="hidden" name="userId" value="<?=$s_user_id_?>">
	<input type="hidden" name="userName" value="<?=$s_user_name_?>">
	<input type="hidden" name="pinCode" value="<?=$s_pin_code_?>">

	<input type="hidden" name="phoneNo" value="<?=$s_phone_num_?>">
	<input type="hidden" name="juminYN" value="N">
	
</form>
<script type="text/javascript">
function _pb_xticket_submit(){
	document.getElementById("pb-xticket-submit-form").submit();
}
</script>
</body>
</html>

<?php

			

	die();	
});


add_action('template_redirect',function(){
	$xticket_aciton_ = get_query_var("_pb_movie_xticketing");
	if($xticket_aciton_ !== PB_MOVIE_XTICKETING_CHECK_SLUG){
		return;
	}
	header('Content-Type: text/html; charset=utf-8');

	$cinema_id_ = isset($_REQUEST["cinema_id"]) ? $_REQUEST["cinema_id"] : null;
	
	if(strlen($cinema_id_)){
		$cinema_data_ = pb_cinema($cinema_id_);

		if(isset($cinema_data_)){
			$cinema_id_ = $cinema_data_->xticket_ref_code; 	
		}else{
			$cinema_id_ = null;
		}
	}

	include_once (PB_THEME_LIB_DIR_PATH . 'pages/xticket/lib.php');

	$spcode_ = "SP0019";
	$is_mobile_ = pb_is_mobile();
	$api_url_ = null;
	$store_code_ = ($is_mobile_ ? "06": "01");
	$encrypt_key_ = "CJ".$spcode_;

	$for_app_ = isset($_REQUEST['_app']) ? $_REQUEST['_app'] : "N";


	/*if(pb_is_devmode()){
		$api_url_ = ($is_mobile_ ? "http://210.122.98.243/Ticketing/MRIALoader.aspx" : "http://210.122.98.243/Ticketing/RIALoader.aspx");
	}else{
		$api_url_ = ($is_mobile_ ? "https://www.xticket.co.kr/Ticketing/MRIALoader.aspx" : "https://www.xticket.co.kr/Ticketing/RIALoader.aspx");
	}*/

	$api_url_ = ($is_mobile_ ? "https://www.xticket.co.kr/Ticketing/MRIALoader.aspx" : "https://www.xticket.co.kr/Ticketing/RIALoader.aspx");

	// header("Content-Type: text/html; charset=UTF-8");
?>

<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
	<title>작은영화관</title>
</head>
<body onload="_pb_xticket_submit();">
<form id="pb-xticket-submit-form" action="<?=$api_url_?>" method="POST" accept-charset="UTF-8">
	<input type="hidden" name="companyCd" value="<?=$spcode_?>">
	<?php if($cinema_id_){ ?>
		<input type="hidden" name="planCompanyCd" value="<?=$cinema_id_?>">
	<?php } ?>


	<input type="hidden" name="storeCd" value="<?=$store_code_?>">
	<input type="hidden" name="authKey" value="<?=encryption($encrypt_key_, "cjsystems")?>">
	<input type="hidden" name="encFlag" value="N">
	<input type="hidden" name="listYn" value="Y"> 
	<input type="hidden" name="riaType" value="G"> 

	<?php if($for_app_ === "Y"){ ?>
	<input type="hidden" name="_for_app" value="Y">
	<?php } ?>
		
	<?php if(is_user_logged_in()){

		$user_data_ = pb_user(get_current_user_id());

		$s_user_id_ = $user_data_->user_login;
		$s_user_name_ = $user_data_->full_name;
		$s_pin_code_ = $user_data_->phone1_1.$user_data_->phone1_2.$user_data_->phone1_3;
		$s_phone_num_ = $user_data_->phone1_1.$user_data_->phone1_2.$user_data_->phone1_3;

		$s_user_id_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_user_id_)));
		$s_user_name_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_user_name_)));
		$s_pin_code_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_pin_code_)));
		$s_phone_num_ = encryption($encrypt_key_, trim(iconv("utf-8", "euc-kr", $s_phone_num_)));

		$s_user_id_ = str_replace("\n",'',$s_user_id_);
		$s_user_name_ = str_replace("\n",'',$s_user_name_);
		$s_pin_code_ = str_replace("\n",'',$s_pin_code_);
		$s_phone_num_ = str_replace("\n",'',$s_phone_num_);

	}else{
		$name_ = $_REQUEST["full_name"];
		$phone1_1_ = $_REQUEST["phone1_1"];
		$phone1_2_ = $_REQUEST["phone1_2"];
		$phone1_3_ = $_REQUEST["phone1_3"];

		$name_ = trim($name_);
		$phone1_1_ = trim($phone1_1_);
		$phone1_2_ = trim($phone1_2_);
		$phone1_3_ = trim($phone1_3_);

		$s_user_id_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $name_));
		$s_user_name_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $name_));
		$s_pin_code_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $phone1_1_.$phone1_2_.$phone1_3_));
		$s_phone_num_ = encryption($encrypt_key_, iconv("utf-8", "euc-kr", $phone1_1_.$phone1_2_.$phone1_3_));

		$s_user_id_ = str_replace("\n",'',$s_user_id_);
		$s_user_name_ = str_replace("\n",'',$s_user_name_);
		$s_pin_code_ = str_replace("\n",'',$s_pin_code_);
		$s_phone_num_ = str_replace("\n",'',$s_phone_num_);

	} ?>

	<input type="hidden" name="userId" value="<?=$s_user_id_?>">
	<input type="hidden" name="userName" value="<?=$s_user_name_?>">
	<input type="hidden" name="pinCode" value="<?=$s_pin_code_?>">

	<input type="hidden" name="phoneNo" value="<?=$s_phone_num_?>">
	<input type="hidden" name="juminYN" value="N">


	
</form>
<script type="text/javascript">
function _pb_xticket_submit(){
	document.getElementById("pb-xticket-submit-form").submit();
}
</script>
</body>
</html>
<?php

			

	die();	
});
?>