<?php
/*=====================================================
 * 전문 암호화.
 * 암호화 알고리즘은 DES, MD5, BASE64를 이용합니다.
 * arg : key - DES 암호화 KEY
 *       input - 암호화 할 전문
 * return : Base64[md5[입력 데이터]] + Base64[des[입력 데이터]]
 *======================================================*/
function hexToStr($hex)
{
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2)
    {
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}

function encryption($key, $input)
{

	$data_len=strlen($input);
	$data_len=str_pad($data_len,4,"0",STR_PAD_LEFT);

    $input=$data_len.$input; 
    
	$md5_data = md5($input);
	$md5_data = hexToStr($md5_data);

	$md5_data_len = strlen($md5_data);
	$md5_data = base64_encode( $md5_data );

    $td = mcrypt_module_open('des', '', 'ecb', '');
    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, $key, $iv);
    $encrypted_data = mcrypt_generic($td, $input);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);

	$encrypted_data = base64_encode( $encrypted_data );
	$buffer = $md5_data . $encrypted_data;
	
	return $buffer;
}

function decryption($key, $input)
{
	
	$md5_data = substr($input, 0, 24);
	$buffer = substr($input, 24);
	$md5_data = base64_decode ( $md5_data);
	
	$buffer = base64_decode ( $buffer );

	$size = mcrypt_get_iv_size (MCRYPT_DES, MCRYPT_MODE_ECB); 
	$iv = mcrypt_create_iv ($size, MCRYPT_DEV_RANDOM); 
	$decrypted_data = mcrypt_ecb (MCRYPT_DES, $key, $buffer, MCRYPT_DECRYPT, $iv);
	
	$data_len = substr($decrypted_data, 0, 4);
	$decrypted_data = substr($decrypted_data, 0, $data_len + 4);
	
	$new_md5_data = md5($decrypted_data);
	$new_md5_data = hexToStr($new_md5_data);

	return $decrypted_data;
}

?> 