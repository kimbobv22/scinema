<?php

add_action('pb_template_page_home',function(){
	PB_library::load('main');

	wp_enqueue_style("page-home", (pb_library_url() . 'css/pages/page-home.css'),'','1901106A');
	wp_enqueue_script("page-home", (pb_library_url() . 'js/pages/page-home.js'), array("pb-all-main"),'191106A');

	add_filter('pb_post_with_sidemenu', function(){
		return false;
	});
	add_filter('pb_full_width', function(){
		return true;
	});
});

?>