<?php

define('PB_PRESS_SLUG', 'press');

add_filter('pb_script_common_var', function($data_){
	$data_['press_url'] = home_url(PB_PRESS_SLUG);

	return $data_;
});

function pb_press_list_url(){
	return home_url(PB_PRESS_SLUG);
}
function pb_press_view_url($press_id_){
	return pb_append_url(home_url(PB_PRESS_SLUG), $press_id_);		
}
add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_PRESS_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_press_action%', '([^&]+)');
	add_rewrite_tag('%_pb_press_id%', '([^&]+)');
	
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_PRESS_SLUG.'/(.+)/?$','index.php?_pb_press_action=view&_pb_press_id=$matches[1]','top');
	add_rewrite_rule('^'.PB_PRESS_SLUG.'/?$','index.php?_pb_press_action=list','top');
});


add_action('pb_template',function(){
	$press_action_yn_ = get_query_var("_pb_press_action");
	if(!strlen($press_action_yn_)) return;

	add_filter("pb_current_sidemenu_id", function($menu_id_){
		$mainmenu_list_ = pb_mainmenu_list();

		$last_item_ = null;

		foreach($mainmenu_list_ as $menu_row_){
			if($menu_row_->type === PB_PRESS_SLUG){

				if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
					$last_item_ = $menu_row_;
					break;
				}
				$last_item_ = $menu_row_;
			}
		}

		if(isset($last_item_)) return $last_item_->ID;

		return $menu_id_;
	});


	add_filter('pb_post_with_sidemenu', function($result_){
		return true;
	});
	add_filter('pb_post_with_breadcrumb', function($result_){
		return true;
	});
/*
	add_filter('pb_breadcrumb', function(){
		return PB_THEME_DIR_PATH.'content-templates/movie/breadcrumb.php';
	});
	add_filter('pb_sidemenu', function(){
		return PB_THEME_DIR_PATH.'content-templates/movie/sidemenu.php';
	});*/

	wp_enqueue_style("page-press-common", (pb_library_url() . 'css/pages/press/common.css'));

	switch($press_action_yn_){
		case "view" :

			global $pb_press;

			$pb_press = pb_press_data(get_query_var("_pb_press_id"));

			add_filter('pb_content_template', function($content_template_){
				return "press/view";		
			});

		break;
		default : 

			add_filter('pb_content_template', function($content_template_){
				return "press/list";		
			});

		break;
	}

});

add_filter('pb_common_navmenu_list', function($results_){
	$results_[] = (object) array(
		'ID' => 1,
		'db_id' => 0,
		'menu_item_parent' => 0,
		'object_id' => PB_PRESS_SLUG,
		'test' => PB_PRESS_SLUG,
		'post_parent' => 0,
		'type' => PB_PRESS_SLUG,
		'object' => PB_PRESS_SLUG,
		'type_label' => '언론보도',
		'title' => '언론보도',
		'url' => home_url(PB_PRESS_SLUG),
		'target' => '',
		'attr_title' => '',
		'description' => '',
		'classes' => array(),
		'xfn' => '',
	);
	return $results_;
});

add_filter('wp_get_nav_menu_items', function($items_, $menu_, $args_){
	foreach($items_ as &$row_data_){
		if($row_data_->type === PB_PRESS_SLUG){
			$row_data_->url = home_url(PB_PRESS_SLUG);
		}
	}
	return $items_;
},10,3);

?>