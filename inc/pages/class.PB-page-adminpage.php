<?php

add_action('pb_adminpage_template',function(){
	PB_library::load('main');

	add_filter('pb_body_classes', function($obj_){
		$obj_[] = 'page-adminpage';
		return $obj_;
	});

	wp_enqueue_script("page-adminpage-common", (pb_library_url() . 'js/pages/adminpage/adminpage.js'), array("pb-all-main"));

	foreach(glob(PB_THEME_LIB_DIR_PATH . 'pages/adminpage/*.php') as $filename_){
	    include_once $filename_;
	}
});



?>