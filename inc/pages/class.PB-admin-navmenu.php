<?php

add_action('admin_head-nav-menus.php',function(){
	add_meta_box( 'pb-common-list-navmenu-metabox', '작은영화관메뉴', '_pb_common_render_navmenu', 'nav-menus', 'side', 'default', null);
});


function _pb_common_render_navmenu($object, $args){
	global $nav_menu_selected_id;
	// Create an array of objects that imitate Post objects
	$my_items = apply_filters('pb_common_navmenu_list', array());
	
	$walker = new Walker_Nav_Menu_Checklist(false);
	$removed_args = array(
		'action',
		'customlink-tab',
		'edit-menu-item',
		'menu-item',
		'page-tab',
		'_wpnonce',
	); ?>
	<div id="my-plugin-div">
		<div id="tabs-panel-my-plugin-all" class="tabs-panel tabs-panel-active">
		<ul id="pb-common-list-navmenu-metabox-checklist-pop" class="categorychecklist form-no-clear" >
			<?php echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $my_items ), 0, (object) array( 'walker' => $walker ) ); ?>
		</ul>

		<p class="button-controls">
			<span class="list-controls">
				<a href="<?php
					echo esc_url(add_query_arg(
						array(
							'pb-common-all' => 'all',
							'selectall' => 1,
						),
						remove_query_arg( $removed_args )
					));
				?>#pb-common-list-navmenu-metabox" class="select-all"><?php _e( 'Select All' ); ?></a>
			</span>

			<span class="add-to-menu">
				<input type="submit"<?php wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e( 'Add to Menu' ); ?>" name="add-pb-common-list-navmenu-metabox" id="submit-pb-common-list-navmenu-metabox" />
				<span class="spinner"></span>
			</span>
		</p>
	</div>
	<?php 
}

?>