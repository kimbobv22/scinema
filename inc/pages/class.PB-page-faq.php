<?php

define('PB_FAQ_SLUG', 'faq');

add_filter('pb_script_common_var', function($data_){
	$data_['faq_url'] = home_url(PB_FAQ_SLUG);

	return $data_;
});

function pb_faq_url(){
	return home_url(PB_FAQ_SLUG);
}
add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_FAQ_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_faq_action%', '([^&]+)');
	
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_FAQ_SLUG.'/?$','index.php?_pb_faq_action=Y','top');
});


add_action('pb_template',function(){
	$faq_action_yn_ = get_query_var("_pb_faq_action");
	if(!strlen($faq_action_yn_)) return;

	add_filter("pb_current_sidemenu_id", function($menu_id_){
		$mainmenu_list_ = pb_mainmenu_list();

		$last_item_ = null;

		foreach($mainmenu_list_ as $menu_row_){
			if($menu_row_->type === PB_FAQ_SLUG){

				if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
					$last_item_ = $menu_row_;
					break;
				}
				$last_item_ = $menu_row_;
			}
		}

		if(isset($last_item_)) return $last_item_->ID;

		return $menu_id_;
	});


	add_filter('pb_post_with_sidemenu', function($result_){
		return true;
	});
	add_filter('pb_post_with_breadcrumb', function($result_){
		return true;
	});

	wp_enqueue_style("page-faq", (pb_library_url() . 'css/pages/page-faq.css'));


	add_filter('pb_content_template', function($content_template_){
		return "content-faq";		
	});
});

add_filter('pb_common_navmenu_list', function($results_){
	$results_[] = (object) array(
		'ID' => 1,
		'db_id' => 0,
		'menu_item_parent' => 0,
		'object_id' => PB_FAQ_SLUG,
		'post_parent' => 0,
		'type' => PB_FAQ_SLUG,
		'object' => PB_FAQ_SLUG,
		'type_label' => '자주묻는질문',
		'title' => '자주묻는질문',
		'url' => home_url(PB_FAQ_SLUG),
		'target' => '',
		'attr_title' => '',
		'description' => '',
		'classes' => array(),
		'xfn' => '',
	);
	return $results_;
});

add_filter('wp_get_nav_menu_items', function($items_, $menu_, $args_){
	foreach($items_ as &$row_data_){
		if($row_data_->type === PB_FAQ_SLUG){
			$row_data_->url = home_url(PB_FAQ_SLUG);
		}
	}
	return $items_;
},10,3);

?>