<?php
add_filter('nav_menu_css_class' , function($classes_, $item_){

	$current_sidemenu_id_ = pb_current_sidemenu_id();

	if($current_sidemenu_id_ == $item_->ID){
		$classes_[] = "current-menu-item";
	}


	$mainmenu_list_ = pb_mainmenu_list();

	foreach($mainmenu_list_ as $row_data_){
		if($row_data_->ID == $current_sidemenu_id_ && $row_data_->menu_item_parent == $item_->ID){
			$classes_[] = "current-menu-item";
			break;
		}
	}

	return $classes_;

}, 10 , 2);

add_action('pb_template',function(){
	PB_library::load('main');
	wp_enqueue_style("page-header", (pb_library_url() . 'css/pages/head-header.css'),'','190808A');
});
?>