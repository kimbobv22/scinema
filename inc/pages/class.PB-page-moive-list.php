<?php

define('PB_MOVIE_SCREENING_LIST_SLUG', 'screening-movies');
define('PB_MOVIE_SCHEDULED_LIST_SLUG', 'scheduled-movies');
define('PB_MOVIE_VIEW_SLUG', 'movie');

function pb_movie_screening_list_url(){
	return apply_filters('pb_movie_screening_list_url', home_url(PB_MOVIE_SCREENING_LIST_SLUG));
}
function pb_movie_scheduled_list_url(){
	return apply_filters('pb_movie_scheduled_list_url', home_url(PB_MOVIE_SCHEDULED_LIST_SLUG));
}
function pb_movie_view_url($movie_id_){
	return apply_filters('pb_movie_view_url', pb_append_url(home_url(PB_MOVIE_VIEW_SLUG), $movie_id_), $movie_id_);
}

add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_MOVIE_SCREENING_LIST_SLUG : 
		case PB_MOVIE_SCHEDULED_LIST_SLUG : 
		case PB_MOVIE_VIEW_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_movie_list_action%', '([^&]+)');
	add_rewrite_tag('%_pb_movie_view_screen_id%', '([^&]+)');
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_MOVIE_SCREENING_LIST_SLUG.'/?$','index.php?_pb_movie_list_action='.PB_MOVIE_SCREENING_LIST_SLUG,'top');
	add_rewrite_rule('^'.PB_MOVIE_SCHEDULED_LIST_SLUG.'/?$','index.php?_pb_movie_list_action='.PB_MOVIE_SCHEDULED_LIST_SLUG,'top');
	add_rewrite_rule('^'.PB_MOVIE_VIEW_SLUG.'/(.+)/?$','index.php?_pb_movie_list_action='.PB_MOVIE_VIEW_SLUG.'&_pb_movie_view_screen_id=$matches[1]','top');
});


add_action('pb_template',function(){
	global $pb_movie_list_action;
	$pb_movie_list_action = get_query_var("_pb_movie_list_action");

	switch($pb_movie_list_action){
		case PB_MOVIE_SCREENING_LIST_SLUG : 
		case PB_MOVIE_SCHEDULED_LIST_SLUG : 

			add_filter('pb_post_with_sidemenu', function($result_){
				return true;
			});
			add_filter('pb_post_with_breadcrumb', function($result_){
				return true;
			});

			add_filter("pb_current_sidemenu_id", function($menu_id_){
				$mainmenu_list_ = pb_mainmenu_list();

				global $pb_movie_list_action;

				$last_item_ = null;

				foreach($mainmenu_list_ as $menu_row_){
					if($menu_row_->type === $pb_movie_list_action){

						if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
							$last_item_ = $menu_row_;
							break;
						}
						$last_item_ = $menu_row_;
					}
				}
				if(isset($last_item_)) return $last_item_->ID;
				return $menu_id_;
			});


/*			global $post;
			$post = null;*/

		/*	add_filter('pb_breadcrumb', function(){
				return PB_THEME_DIR_PATH.'content-templates/movie/breadcrumb.php';
			});
			add_filter('pb_sidemenu', function(){
				return PB_THEME_DIR_PATH.'content-templates/movie/sidemenu.php';
			});*/


			wp_enqueue_style("page-movie-list", (pb_library_url() . 'css/pages/movie/list.css'));
			wp_enqueue_script("page-movie-list", (pb_library_url() . 'js/pages/movie/list.js'), array("pb-all-main"));
		
		break;
		case PB_MOVIE_VIEW_SLUG : 

			add_filter('pb_post_with_sidemenu', function($result_){
				return true;
			});
			add_filter('pb_post_with_breadcrumb', function($result_){
				return true;
			});

		/*	add_filter('pb_breadcrumb', function(){
				return PB_THEME_DIR_PATH.'content-templates/movie/breadcrumb.php';
			});
			add_filter('pb_sidemenu', function(){
				return PB_THEME_DIR_PATH.'content-templates/movie/sidemenu.php';
			});*/
	

			$screen_id_ = get_query_var("_pb_movie_view_screen_id");

			if(pb_cinema_current_is_head_office()){ //본사일 경우 개봉영화정보
				//20190620 nhn - 디트릭스로 시스템 교체되어 본사 데이터는 다 디트릭스 데이터로 표시
				//echo ("나는 본사 데이터");
				global $cinema_type_;
				$cinema_type_='dtryx';

				// 20190620 nhn - 기존소스 모두 주석 처리 
				// global $pb_moive_open_data;
				// $pb_moive_open_data = pb_movie_open_data($screen_id_);

				// if(!isset($pb_moive_open_data)){
				// 		pb_404();
				// 		return;
				// 	}

				// add_filter("pb_current_sidemenu_id", function($menu_id_){
				// 	$mainmenu_list_ = pb_mainmenu_list();

				// 	global $pb_moive_open_data;
				// 	$target_status_ = PB_MOVIE_SCREENING_LIST_SLUG;

				// 	if($pb_moive_open_data->on_scheduled_yn === "Y"){
				// 		$target_status_ = PB_MOVIE_SCHEDULED_LIST_SLUG;
				// 	}

				// 	$last_item_ = null;

				// 	foreach($mainmenu_list_ as $menu_row_){
				// 		if($menu_row_->type === $target_status_){

				// 			if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
				// 				$last_item_ = $menu_row_;
				// 				break;
				// 			}
				// 			$last_item_ = $menu_row_;
				// 		}
				// 	}

				// 	if(isset($last_item_)) return $last_item_->ID;

				// 	return $menu_id_;
				// });

			}else{//지점일 경우 상영영화정보
				//echo ("나 지점 데이터");
				global $pb_moive_screen_data;
				$pb_moive_screen_data = pb_movie_screen_data($screen_id_);
				
				global $cinema_type_;
				
				if($cinema_type_ !== "dtryx"){
					if(!isset($pb_moive_screen_data)){
						pb_404();
						return;
					}	
					add_filter("pb_current_sidemenu_id", function($menu_id_){
						$mainmenu_list_ = pb_mainmenu_list();

						global $pb_moive_screen_data;
						$target_status_ = PB_MOVIE_SCREENING_LIST_SLUG;

						if($pb_moive_screen_data->on_scheduled_yn === "Y"){
							$target_status_ = PB_MOVIE_SCHEDULED_LIST_SLUG;
						}

						$last_item_ = null;

						foreach($mainmenu_list_ as $menu_row_){
							if($menu_row_->type === $target_status_){

								if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
									$last_item_ = $menu_row_;
									break;
								}
								$last_item_ = $menu_row_;
							}
						}
						if(isset($last_item_)) return $last_item_->ID;
						return $menu_id_;
					});
				}
			}
			if($cinema_type_ !== "dtryx"){
				wp_enqueue_script("page-movie-timetable", (pb_library_url() . 'js/pages/movie/timetable.js'), array("pb-all-main"));
			}
			wp_enqueue_style("page-movie-view", (pb_library_url() . 'css/pages/movie/view.css'),array(),'190612A');
			wp_enqueue_script("page-movie-view", (pb_library_url() . 'js/pages/movie/view.js'), array("pb-all-main", "page-movie-timetable"));
		break;
		default : break;
	}
});

add_filter('pb_content_template', function($content_template_){
	global $pb_movie_list_action;

	switch($pb_movie_list_action){
		
		case PB_MOVIE_SCREENING_LIST_SLUG : 
		case PB_MOVIE_SCHEDULED_LIST_SLUG : 

			// if(pb_cinema_current_is_head_office()){
			// 	return "movie/headoffice-list";				
			// }

			return "movie/list";

		case PB_MOVIE_VIEW_SLUG : 
			// if(pb_cinema_current_is_head_office()){
			// 	return "movie/headoffice-view";
			// }

			return "movie/view";
			
		default : return $content_template_;

	}
});


add_filter('pb_common_navmenu_list', function($results_){
	$results_[] = (object) array(
		'ID' => 1,
		'db_id' => 0,
		'menu_item_parent' => 0,
		'object_id' => PB_MOVIE_SCREENING_LIST_SLUG,
		'post_parent' => 0,
		'type' => PB_MOVIE_SCREENING_LIST_SLUG,
		'object' => PB_MOVIE_SCREENING_LIST_SLUG,
		'type_label' => '현재상영작',
		'title' => '현재상영작',
		'url' => home_url(PB_MOVIE_SCREENING_LIST_SLUG),
		'target' => '',
		'attr_title' => '',
		'description' => '',
		'classes' => array(),
		'xfn' => '',
	);

	$results_[] = (object) array(
		'ID' => 2,
		'db_id' => 0,
		'menu_item_parent' => 0,
		'object_id' => PB_MOVIE_SCHEDULED_LIST_SLUG,
		'post_parent' => 0,
		'type' => PB_MOVIE_SCHEDULED_LIST_SLUG,
		'object' => PB_MOVIE_SCHEDULED_LIST_SLUG,
		'type_label' => '상영예정작',
		'title' => '상영예정작',
		'url' => home_url(PB_MOVIE_SCHEDULED_LIST_SLUG),
		'target' => '',
		'attr_title' => '',
		'description' => '',
		'classes' => array(),
		'xfn' => '',
	);
	return $results_;
});


add_filter('wp_get_nav_menu_items', function($items_, $menu_, $args_){
	foreach($items_ as &$row_data_){
		if($row_data_->type === PB_MOVIE_SCREENING_LIST_SLUG){
			$row_data_->url = home_url(PB_MOVIE_SCREENING_LIST_SLUG);
		}else if($row_data_->type === PB_MOVIE_SCHEDULED_LIST_SLUG){
			$row_data_->url = home_url(PB_MOVIE_SCHEDULED_LIST_SLUG);
		}
	}
	return $items_;
},10,3);

?>