<?php
add_filter('pb_content_template', function($content_template_){
	global $cinema_type_;
 	if($cinema_type_ !== "dtryx"){
		return $content_template_;
	}
	global $pb_movie_list_action;

	switch($pb_movie_list_action){	
		case PB_MOVIE_SCREENING_LIST_SLUG : 
		case PB_MOVIE_SCHEDULED_LIST_SLUG : 
			return "movie/dtryx-list";
		case PB_MOVIE_VIEW_SLUG : 
			return "movie/dtryx-view";		
		default : return $content_template_;
	}	
},999);

add_action('pb_template',function(){
 	global $cinema_type_;
	$current_cinema_ = pb_current_cinema();
	$screen_id_ = get_query_var("_pb_movie_view_screen_id"); 

	if(pb_cinema_current_is_head_office()) { 
		$cinema_id_ = 'all'; 
		$cinema_type_ = 'dtryx';
	}
	else {
		$cinema_id_ = $current_cinema_->xticket_ref_code;
		$cinema_type_ = $current_cinema_->ticket_ref_type;
	}

	if($cinema_type_ === "dtryx"){
		global $pb_movie_list_action;
		$pb_movie_list_action = get_query_var("_pb_movie_list_action");
		global $pb_movie_timetable_action;
		$pb_movie_timetable_action = get_query_var("_pb_movie_timetable_action");
		
		global $pb_moive_screen_data_;
		$pb_moive_screen_data_ = pb_dtryx_movie_detail($cinema_id_,$screen_id_);

		if($pb_movie_list_action === PB_MOVIE_VIEW_SLUG || $pb_movie_timetable_action === PB_MOVIE_TIMETABLE_SLUG){
			if($pb_movie_list_action === PB_MOVIE_VIEW_SLUG) {
				add_filter("pb_current_sidemenu_id", function($menu_id_){
					$mainmenu_list_ = pb_mainmenu_list();
					
					$target_status_ = PB_MOVIE_SCREENING_LIST_SLUG;
					global $pb_moive_screen_data_;

					if(strtotime($pb_moive_screen_data_["open_date"]) > strtotime(date("Y-m-d"))){
						$target_status_ = PB_MOVIE_SCHEDULED_LIST_SLUG;
					}
					$last_item_ = null;
					foreach($mainmenu_list_ as $menu_row_){
						if($menu_row_->type === $target_status_){
							if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
								$last_item_ = $menu_row_;
								break;
							}
							$last_item_ = $menu_row_;
						}
					}
					if(isset($last_item_)) return $last_item_->ID;
					return $menu_id_;
				},11);
			}
			else {
				add_filter('pb_content_template', function($content_template_){
					return "movie/dtryx-timetable";
				},11); 	
			}
		}		
	}
});

add_action('template_redirect',function(){
	global $cinema_type_;
	if($cinema_type_ === 'dtryx'){
		wp_enqueue_style("dtryx-modal", (pb_library_url() . 'css/pages/movie/dtryx.css'),array(),'190625A');
		wp_enqueue_script("dtryx-timetable", (pb_library_url() . 'js/pages/movie/dtryx/scinema-timetable.js'), array("pb-all-main"),'191205C');
		wp_enqueue_script("jq-bpopup", (pb_library_url() . 'js/pages/movie/dtryx/jquery.bpopup.min.js'), array("pb-all-main"));
	 }
});
?>