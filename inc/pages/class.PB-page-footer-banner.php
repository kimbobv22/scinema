<?php

add_action("pb_body_container_after", function(){

	global $pb_adminpage_menu_item;

	$banner_list_ = pb_banner_list(array(
		'cinema_id' => pb_current_cinema_id(),
		'shuffle' => true,
	));


	if(count($banner_list_) > 0 && !isset($pb_adminpage_menu_item)){ ?>

	<div class="footer-banner-frame">
		<div class="swiper-container footer-banner-slider" id="pb-footer-banner-slider">
			<div class="swiper-wrapper">
				<?php foreach($banner_list_ as $row_data_){?>
					<div class="swiper-slide">
					<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') { ?>
						<a href="<?=apply_filters('pb_footer_banner_link', preg_replace("/^http:/i", "https:", $row_data_->link_url))?>" class="banner-link-item">
							<img src="<?=preg_replace("/^http:/i", "https:",$row_data_->banner_image_url)?>" alt="<?=$row_data_->banner_title?>" class="banner-image">
						</a>
					<?php }
					else { ?>
						<a href="<?=apply_filters('pb_footer_banner_link', $row_data_->link_url)?>" class="banner-link-item">
							<img src="<?=$row_data_->banner_image_url?>" alt="<?=$row_data_->banner_title?>" class="banner-image">
						</a>
					<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		$("#pb-footer-banner-slider").swiper({
			autoplayDisableOnInteraction : false,
			autoplay : 3000,
			loop: true,
			effect : "fade",
			autoHeight : true,
		});
	});
	</script>

	<?php

	}
});

?>