<?php

define('PB_FILEDOWNLOAD_SLUG', 'filedownload');

function pb_filedownload_url($url_, $file_name_){
	return pb_make_url(network_site_url(PB_FILEDOWNLOAD_SLUG), array(
		'file_url' => $url_,
		'file_name' => urlencode($file_name_),
	));
}
add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_FILEDOWNLOAD_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_filedownload_action%', '([^&]+)');
	
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_FILEDOWNLOAD_SLUG.'/?$','index.php?_pb_filedownload_action=Y','top');
});


add_action('pb_template',function(){
	$filedownload_action_yn_ = get_query_var("_pb_filedownload_action");
	if($filedownload_action_yn_ !== "Y") return;

	$file_url_ = isset($_GET['file_url']) ? $_GET['file_url'] : null;
	$file_name_ = isset($_GET['file_name']) ? $_GET['file_name'] : null;

	if(!strlen($file_url_) || !strlen($file_name_)){
		wp_die("잘못된 접근");
	}


	$yyymmddhh_ = date("YmdH");

	$upload_dir_ = PBFILEUPLOAD_TEMP_DIR.$yyymmddhh_."/";
	$upload_path_ = $upload_dir_.pb_random_string(10).".".pathinfo($file_url_, PATHINFO_EXTENSION);

	if(!file_exists($upload_dir_)){
		mkdir($upload_dir_, 0777, true);
	}
	// file_put_contents($upload_path_, fopen($file_url_, 'r'));

	$ch_ = curl_init($file_url_);
	curl_setopt($ch_, CURLOPT_HEADER, 0);
	curl_setopt($ch_, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch_, CURLOPT_BINARYTRANSFER,1);
	curl_setopt($ch_, CURLOPT_TIMEOUT, 1000);
 	curl_setopt($ch_, CURLOPT_USERAGENT, 'Mozilla/5.0');
	$raw_ = curl_exec($ch_);
	curl_close($ch_);

	$fp_ = fopen($upload_path_, "w");
	fwrite($fp_, $raw_);
	fclose($fp_);

	$upload_path_ = $upload_path_;
	$filesize_ = filesize($upload_path_);

	if(isset($_SERVER['HTTP_USER_AGENT'])
	 && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false
	 	|| strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false)
	) $file_name_ = iconv("UTF-8","cp949//IGNORE", $file_name_);


	header("Pragma: public");
	header("Expires: 0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"$file_name_\"");
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: $filesize_");


	readfile($upload_path_);
	die();
});


?>