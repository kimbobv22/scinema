<?php

define('PB_NOTICE_SLUG', 'notice');

add_filter('pb_script_common_var', function($data_){
	$data_['notice_url'] = home_url(PB_NOTICE_SLUG);

	return $data_;
});

function pb_notice_list_url(){
	return home_url(PB_NOTICE_SLUG);
}
function pb_notice_view_url($notice_id_){
	return pb_append_url(home_url(PB_NOTICE_SLUG), $notice_id_);		
}
add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_NOTICE_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_notice_action%', '([^&]+)');
	add_rewrite_tag('%_pb_notice_id%', '([^&]+)');
	
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_NOTICE_SLUG.'/(.+)/?$','index.php?_pb_notice_action=view&_pb_notice_id=$matches[1]','top');
	add_rewrite_rule('^'.PB_NOTICE_SLUG.'/?$','index.php?_pb_notice_action=list','top');
});


add_action('pb_template',function(){
	$notice_action_yn_ = get_query_var("_pb_notice_action");
	if(!strlen($notice_action_yn_)) return;

	add_filter("pb_current_sidemenu_id", function($menu_id_){
		$mainmenu_list_ = pb_mainmenu_list();

		$last_item_ = null;

		foreach($mainmenu_list_ as $menu_row_){
			if($menu_row_->type === PB_NOTICE_SLUG){

				if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
					$last_item_ = $menu_row_;
					break;
				}
				$last_item_ = $menu_row_;
			}
		}

		if(isset($last_item_)) return $last_item_->ID;

		return $menu_id_;
	});


	add_filter('pb_post_with_sidemenu', function($result_){
		return true;
	});
	add_filter('pb_post_with_breadcrumb', function($result_){
		return true;
	});
/*
	add_filter('pb_breadcrumb', function(){
		return PB_THEME_DIR_PATH.'content-templates/movie/breadcrumb.php';
	});
	add_filter('pb_sidemenu', function(){
		return PB_THEME_DIR_PATH.'content-templates/movie/sidemenu.php';
	});*/

	wp_enqueue_style("page-notice-common", (pb_library_url() . 'css/pages/notice/common.css'));

	switch($notice_action_yn_){
		case "view" :

			global $pb_notice;

			$pb_notice = pb_notice_data(get_query_var("_pb_notice_id"));

			add_filter('pb_content_template', function($content_template_){
				return "notice/view";		
			});

		break;
		default : 

			add_filter('pb_content_template', function($content_template_){
				return "notice/list";		
			});

		break;
	}
});


add_filter('pb_common_navmenu_list', function($results_){
	$results_[] = (object) array(
		'ID' => 1,
		'db_id' => 0,
		'menu_item_parent' => 0,
		'object_id' => PB_NOTICE_SLUG,
		'post_parent' => 0,
		'type' => PB_NOTICE_SLUG,
		'object' => PB_NOTICE_SLUG,
		'type_label' => '공지사항',
		'title' => '공지사항',
		'url' => home_url(PB_NOTICE_SLUG),
		'target' => '',
		'attr_title' => '',
		'description' => '',
		'classes' => array(),
		'xfn' => '',
	);
	return $results_;
});

add_filter('wp_get_nav_menu_items', function($items_, $menu_, $args_){
	foreach($items_ as &$row_data_){
		if($row_data_->type === PB_NOTICE_SLUG){
			$row_data_->url = home_url(PB_NOTICE_SLUG);
		}
	}
	return $items_;
},10,3);

?>