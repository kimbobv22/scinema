<?php

//상영시간표 훅
add_filter('pb_movie_screen_timetable', function($result_, $cinema_id_, $target_date_, $screen_id_, $include_not_available_){
	if(!strlen($cinema_id_)) return $result_;

	$cinema_data_ = pb_cinema($cinema_id_);

	if($cinema_data_->ticket_ref_type === "xticket"){

		$screen_data_ = pb_movie_screen_data($screen_id_);

		return pb_movie_screen_xticket_timetable($cinema_data_->xticket_ref_code, $target_date_, $screen_data_->xticket_ref_code, $include_not_available_);
	}
	return $result_;
}, 10, 5);

//영화목록 훅
add_filter('pb_movie_ref_movie_list', function($result_, $cinema_id_){
	if(!strlen($cinema_id_)) return $result_;

	$cinema_data_ = pb_cinema($cinema_id_);

	if($cinema_data_->ticket_ref_type === "xticket"){
		$tmp_xticket_movie_list_ = pb_movie_xticket_movie_list($cinema_data_->xticket_ref_code);
		$xticket_movie_list_ = array();

		foreach($tmp_xticket_movie_list_ as $row_data_){
			$xticket_movie_list_[$row_data_['id']] = $row_data_['name'];
		}	
		return $xticket_movie_list_;
	}

	return $result_;
}, 10, 2);

?>