<?php

define('PB_MOVIE_INTERPARK_TIMETABLE_SLUG', 'interparktimetable');

function pb_movie_interpark_timetable_url(){
	return apply_filters('pb_movie_interpark_timetable_url', home_url(PB_MOVIE_INTERPARK_TIMETABLE_SLUG));
}

add_filter('wp_unique_post_slug', function($slug_){
	switch($slug_){
		case PB_MOVIE_INTERPARK_TIMETABLE_SLUG : 
			return $slug_."1";
		default : return $slug_;
	}
});


add_action("init" ,function(){
	add_rewrite_tag('%_pb_interpark_timetable_action%', '([^&]+)');
});

add_action('pb_add_rewrite_rule', function(){
	add_rewrite_rule('^'.PB_MOVIE_INTERPARK_TIMETABLE_SLUG.'/?$','index.php?_pb_interpark_timetable_action='.PB_MOVIE_INTERPARK_TIMETABLE_SLUG,'top');
});


add_action('pb_template',function(){
	$current_cinema_ = pb_current_cinema();
	if($current_cinema_->ticket_ref_type !== "interpark") return;

	global $pb_movie_list_action;
	$pb_movie_list_action = get_query_var("_pb_movie_list_action");

	switch($pb_movie_list_action){
		case PB_MOVIE_SCREENING_LIST_SLUG : 
		case PB_MOVIE_SCHEDULED_LIST_SLUG :
		case PB_MOVIE_VIEW_SLUG : 

			wp_redirect(pb_movie_interpark_timetable_url());
			die();
		
		break;
		default : break;
	}
});


add_action('pb_template',function(){
	global $_pb_interpark_timetable_action;
	$_pb_interpark_timetable_action = get_query_var("_pb_interpark_timetable_action");

	switch($_pb_interpark_timetable_action){
		case PB_MOVIE_INTERPARK_TIMETABLE_SLUG : 
		
			add_filter('pb_post_with_sidemenu', function($result_){
				return true;
			});
			add_filter('pb_post_with_breadcrumb', function($result_){
				return true;
			});

			add_filter("pb_current_sidemenu_id", function($menu_id_){
				$mainmenu_list_ = pb_mainmenu_list();

				global $_pb_interpark_timetable_action;

				$last_item_ = null;

				foreach($mainmenu_list_ as $menu_row_){
					if($menu_row_->type === $_pb_interpark_timetable_action){

						if(isset($last_item_) && $menu_row_->menu_item_parent == $last_item_->ID){
							$last_item_ = $menu_row_;
							break;
						}
						$last_item_ = $menu_row_;
					}
				}

				if(isset($last_item_)) return $last_item_->ID;

				return $menu_id_;
			});

			wp_enqueue_style("page-movie-list", (pb_library_url() . 'css/pages/movie/interpark.css'));
			wp_enqueue_script("page-movie-list", (pb_library_url() . 'js/pages/movie/interpark.js'), array("pb-all-main"));
		
		default : break;
	}
});

add_filter('pb_content_template', function($content_template_){
	global $_pb_interpark_timetable_action;

	switch($_pb_interpark_timetable_action){
		
		case PB_MOVIE_INTERPARK_TIMETABLE_SLUG : 

			return "movie/interpark";
		default : return $content_template_;

	}
});


add_filter('pb_common_navmenu_list', function($results_){
	$results_[] = (object) array(
		'ID' => 1,
		'db_id' => 0,
		'menu_item_parent' => 0,
		'object_id' => PB_MOVIE_INTERPARK_TIMETABLE_SLUG,
		'post_parent' => 0,
		'type' => PB_MOVIE_INTERPARK_TIMETABLE_SLUG,
		'object' => PB_MOVIE_INTERPARK_TIMETABLE_SLUG,
		'type_label' => '영화예매하기(인터파크)',
		'title' => '영화예매하기',
		'url' => home_url(PB_MOVIE_INTERPARK_TIMETABLE_SLUG),
		'target' => '',
		'attr_title' => '',
		'description' => '',
		'classes' => array(),
		'xfn' => '',
	);

	return $results_;
});


add_filter('wp_get_nav_menu_items', function($items_, $menu_, $args_){
	foreach($items_ as &$row_data_){
		if($row_data_->type === PB_MOVIE_INTERPARK_TIMETABLE_SLUG){
			$row_data_->url = home_url(PB_MOVIE_INTERPARK_TIMETABLE_SLUG);
		}
	}
	return $items_;
},10,3);


add_filter('pb_movie_screening_list_url', function($result_){
	$current_cinema_ = pb_current_cinema();

	if($current_cinema_->ticket_ref_type === "interpark"){
		return pb_movie_interpark_timetable_url();
	}

	return $result_;
});

add_filter('pb_movie_scheduled_list_url', function($result_){
	$current_cinema_ = pb_current_cinema();

	if($current_cinema_->ticket_ref_type === "interpark"){
		return pb_movie_interpark_timetable_url();
	}
	return $result_;
});

add_filter('pb_movie_view_url', function($result_, $movie_id_){
	$current_cinema_ = pb_current_cinema();

	if($current_cinema_->ticket_ref_type === "interpark"){
		return pb_movie_interpark_timetable_url();
	}
	return $result_;
},10,2);

add_filter('wp_footer', function(){
	$current_cinema_ = pb_current_cinema();
	if($current_cinema_->ticket_ref_type !== "interpark") return;

	?>

<script type="text/javascript">
function pb_interpark_check_ticketing() {
  var surl					= "http://Movie.interpark.com/MoviePartner/TOneStop/Bridge.asp?Set=FPL&PCode=<?=$current_cinema_->xticket_ref_code?>";
  var sname					= "index";
  var popupOptions	= "left=120,top=100,width=920,height=650,toolbar=no,status=no,scrollbars=no,resizable=yes,menubar=no";
  window.open(surl, sname, popupOptions);
}
jQuery(document).ready(function(){
	PB.movie.open_xticket_check_popup = pb_interpark_check_ticketing;
});
</script>

	<?php
});

?>