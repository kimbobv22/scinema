<!DOCTYPE html>
<html>
<head>

	<title><?=get_bloginfo('name')?></title>
	<meta charset="<?=bloginfo('charset')?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="apple-touch-icon" sizes="57x57" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?=PB_THEME_DIR_URL?>img/icon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?=PB_THEME_DIR_URL?>img/icon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?=PB_THEME_DIR_URL?>img/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?=PB_THEME_DIR_URL?>img/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?=PB_THEME_DIR_URL?>img/icon/favicon-16x16.png">

	<link rel="manifest" href="<?=PB_THEME_DIR_URL?>img/icon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?=PB_THEME_DIR_URL?>img/icon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-site-verification" content="s2Ioe8bzYUrc6NmTSkWtM9PE6U3y_LCTRqUD3zkcyc4" />
	<meta name="naver-site-verification" content="d04231c755768000e87c4731deecdf1db7ed3216"/>

	<?php wp_head(); ?>

<?php 
	
	$page_title_ = null;
	$page_description_ = null;
	$page_url_ = null;

	if(is_front_page() || is_404()){
		$page_title_ = get_bloginfo('name');
		$page_description_ = get_bloginfo('description');
		$page_url_ = get_the_permalink();

	}else{
		$page_title_ = get_the_title();
		$page_description_ = apply_filters('get_the_excerpt',get_the_excerpt());
		$page_url_ = get_the_permalink();
	}

	$post_id_ = get_the_ID();

	$body_classes_ = apply_filters('pb_body_classes', array("headoffice"));
	
	$header_white_fixed_ = get_post_meta($post_id_, "pb_post_header_fixed", true);
	$header_white_fixed_ = strlen($header_white_fixed_) ? $header_white_fixed_  === "Y" : true;
	
	$with_sidemenu_ = get_post_meta($post_id_, "pb_post_with_sidemenu", true) === "Y";
	$with_sidemenu_ = apply_filters('pb_post_with_sidemenu', $with_sidemenu_);

	$with_breadcrumb_ = get_post_meta($post_id_, "pb_post_with_breadcrumb", true) === "Y";
	$with_breadcrumb_ = apply_filters('pb_post_with_breadcrumb', $with_breadcrumb_);

	$body_with_padding_ = apply_filters("pb_post_body_with_padding", false);
	$body_full_width_ = apply_filters("pb_full_width", false);

	if($header_white_fixed_) $body_classes_[] = 'header-white-fixed';
	if($body_with_padding_) $body_classes_[] = 'body-with-padding';
	if($body_full_width_) $body_classes_[] = 'full-width';
	if($with_sidemenu_) $body_classes_[] = 'with-sidemenu';
	if($with_breadcrumb_) $body_classes_[] = 'with-breadcrumb';
		

?>

	<meta property="og:title" content="<?=$page_title_?>" />
	<meta property="og:description" content="<?=$page_description_?>" />
	<meta name="description" content="<?=$page_description_?>" />
	<meta property="og:url" content="<?=$page_url_?>" />

</head>
<body <?php body_class($body_classes_); ?>>
<div class="pb-responsive-var">
	<div class="xs"></div><div class="sm"></div><div class="md"></div><div class="lg"></div>
</div>
<nav class="navbar navbar-transparent navbar-fixed-top header-navbar <?=$header_white_fixed_ ? "white-fixed" : ""?>" id="pb-header">
	<div class="headoffice-top-menu-frame"><div class="container">
		<ul class="top-menu-list">
			<?php if(is_user_logged_in()){ ?>
				<li class="menu-item"><a href="<?=pb_user_mypage_url()?>">마이페이지</a></li>
				<li class="menu-item"><a href="<?=pb_user_logout_url()?>">로그아웃</a></li>
			<?php }else{ ?>
				<li class="menu-item"><a href="<?=pb_user_login_url()?>">로그인</a></li>
				<li class="menu-item"><a href="<?=pb_user_signup_url()?>">회원가입</a></li>
			<?php } ?>
			<?php if(pb_is_admin()){ ?>
				<li class="menu-item"><a href="<?=pb_adminpage_url()?>">관리자메뉴</a></li>
			<?php } ?>
		</ul>
	</div></div>
	<div class="container">	
	  <div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#pb-mainnavbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">MENU</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		 </button>
		 <a class="navbar-img-logo" href="<?=home_url()?>">
		 	<img src="<?=PB_THEME_DIR_URL?>img/header-logo-black.png" class="black">
		 	<img src="<?=PB_THEME_DIR_URL?>img/header-logo-white.png" class="white">
		 </a>
	  </div>
	   	<div id="pb-mainnavbar" class="collapse navbar-collapse navbar-right">
		   <div class="mobile_menu visible-xs">
	  		<aside class="sidemenu">
		  	<div class="m_header">
			  <div class="clearfix">
				<?php if(is_user_logged_in()){ 
					$user_data_ = pb_user(get_current_user_id());
					$s_user_name_ = $user_data_->full_name;	
				?>
					<span><?=$s_user_name_?>님
						<span class="small">반갑습니다.</span>
						<a href="<?=pb_user_logout_url()?>" class="mainLogout">로그아웃</a>
					</span>
				<?php }else{ ?>
					<a href="<?=pb_user_login_url()?>" class="mainNav">로그인</a>
					<a href="<?=pb_user_signup_url()?>" class="mainNav">회원가입</a>
				<?php } ?>
			  </div> 
			  	<div class="clearfix top-nav">
				  <a href="<?=pb_user_mypage_url()?>" class="float_left">마이페이지</a>
				  <a href="<?=pb_cinema_list_url()?>" class="float_left">영화관찾기</a>
				  <?php if(pb_is_admin()){ ?>
					<a href="<?=pb_adminpage_url()?>" class="float_left">관리자메뉴</a>
				  <?php }else { ?>
				  <a href="<?=pb_notice_list_url()?>" class="float_left">공지사항</a>
				  <?php } ?>
				</div>
			</div> 
			</aside>
	   	</div>
     	<?php
		 	wp_nav_menu(array(
		 		'theme_location' => PB_MAINMENU,
		 		'menu_class' => 'nav navbar-nav menu',
		 		'menu_id' => 'pb-header-mainmenu-item',
		 		'container' => 'ul',
		 		'depth' => 2,
		 		'container_class' => 'nav navbar-nav menu',
		 		'container_id' => 'pb-header-mainmenu-item',

		 	));
		 ?>
	  </div><!--/.nav-collapse -->
	</div>
 </nav>

<?php if($with_sidemenu_ || $with_breadcrumb_){ ?>
	
	<div class="breadcrumb-wrap"><div class="breadcrumb">
		<?php
			$custom_breadcrumb_ = apply_filters('pb_breadcrumb', null);

			if(strlen($custom_breadcrumb_)){
				include_once($custom_breadcrumb_);
			}else{
				get_template_part('content-templates/content-breadcrumb');
			}

		 ?>
	</div></div>

<?php } ?>
<div class="body-container-wrap <?=$body_full_width_ ? "body-container-wrap-full-width" : ""?>">

<?php if($with_sidemenu_){ ?>
	<div class="sidemenu-wrap">
		
		<div class="sidemenu-item sidemenu-list-wrap">
			<?php

			$custom_sidemenu_ = apply_filters('pb_sidemenu', null);

			if(strlen($custom_sidemenu_)){
				include_once($custom_sidemenu_);
			}else{
				get_template_part('content-templates/content-sidemenu');
			}

			?>
		</div>
		
		
	</div>
<?php } ?>

<div class="<?=($body_full_width_ || $with_sidemenu_)? "container-fluid" : "container"?> body-container">