<?php

define("PB_THEME_DIR_URL",get_template_directory_uri().'/');
define("PB_THEME_DIR_PATH",get_template_directory().'/');

require_once PB_THEME_DIR_PATH.'inc/PB.php';

define('PB_THEME_LIB_READY', true);
do_action('pb_theme_ready');

?>