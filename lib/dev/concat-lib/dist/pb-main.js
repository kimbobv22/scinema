(function($){
	
	var PB = window.PB = {
		_admin_ajaxurl : PBVAR["admin_ajaxurl"],
		_ajaxurl : PBVAR["ajaxurl"],
		admin_ajaxurl : function(params_){
			if(params_ === undefined){
				return this._admin_ajaxurl;
			}

			return this.make_url(this._admin_ajaxurl, params_);
		},
		ajaxurl : function(params_){
			if(params_ === undefined){
				return this._ajaxurl;
			}

			return this.make_url(this._ajaxurl, params_);
		},
		nl2br : function(str_){
			return str_.replace(/\n/g, "<br />");  
		},
		make_currency : function(value_){
			$result_ = String(Math.abs(value_)).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');

			if(value_ < 0){
				$result_ = "-"+$result_;
			}

			return $result_;
		},make_k_number_format : function(value_){
			  return (value_ > 999 ? (value_/1000).toFixed(1) + 'k' : value_);
		},
		random_string : function(length_, possible_){
			var text_ = "";
			var possible_ = (possible_ !== undefined ? possible_ : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

			for(var i=0; i < length_; i++)
				text_ += possible_.charAt(Math.floor(Math.random() * possible_.length));

			return text_;
		},
		lpad : function(str_, char_, length_){
			if(! str_|| !char_|| str_.length >= length_) {
				return str_;
			}

			var max_ = (length_ - str_.length) / char_.length;

			for(var index_=0;index_<max_; ++index_){
				str_ = char_ + str_;
			}

			return str_;
		},
		rpad : function(str_, char_, length_){
			if(! str_|| !char_|| str_.length >= length_) {
				return str_;
			}

			var max_ = (length_ - str_.length) / char_.length;

			for(var index_=0;index_<max_; ++index_){
				str_ += char_;
			}

			return str_;
		},
		make_options : function(array_, value_column_, title_column_){
			var html_ = '';
			for(var index_=0;index_<array_.length; ++index_){
				html_ += '<option value="'+array_[index_][value_column_]+'">'+array_[index_][title_column_]+'</option>';
			}

			return html_;
		},
		json_equals : function(a_, b_, sort_array_){
			var sort_func_ = function(object_){
				if(sort_array_ === true && Array.isArray(object_)) {
					return object_.sort();
				}else if(typeof object_ !== "object" || object_ === null) {
					return object_;
				}

				return Object.keys(object_).sort().map(function(key_) {
					return {
						key: key_,
						value: sort_func_(object_[key_])
					};
				});
			};
			
			return JSON.stringify(sort_func_(a_)) === JSON.stringify(sort_func_(b_));
		},
		_data_filters : {},
		add_data_filter : function(key_, func_){
			if(this._data_filters[key_] === undefined || this._data_filters[key_] === null){
				this._data_filters[key_] = [];
			}
			this._data_filters[key_].push(func_);
		},
		apply_data_filters : function(key_, params_, add_){
			if(this._data_filters[key_] !== undefined && this._data_filters[key_] !== null){
				var filter_count_ = this._data_filters[key_].length;
				for(var index_=0;index_<filter_count_;++index_){
					params_ = this._data_filters[key_][index_](params_, add_);	
				}
			}
			return params_;
		},
		_data_actions : {},
		add_data_action : function(key_, func_){
			if(this._data_actions[key_] === undefined || this._data_actions[key_] === null){
				this._data_actions[key_] = [];
			}
			this._data_actions[key_].push(func_);
		},
		do_data_action : function(key_, params_){
			if(this._data_actions[key_] !== undefined && this._data_actions[key_] !== null){
				var filter_count_ = this._data_actions[key_].length;
				for(var index_=0;index_<filter_count_;++index_){
					this._data_actions[key_][index_](params_);	
				}
			}
		},
		_post : function(data_, callback_, options_){
			options_ = $.extend({
				type: "POST",
				url: this._ajaxurl,
				data : $.extend(true,{},data_),
				success : function(response_text_){
					var result_ = true;
					try{
						response_text_ = JSON.parse(response_text_);
					}catch(ex_){
						result_ = false;
					}
					callback_(result_,response_text_, response_text_);
				},error : function(xhr_, status_, thrown_){
					if(status_ === "abort") return;
					callback_(false, status_, thrown_);
				}
			},options_);
			
			return $.ajax(options_);
		},
		post : function(data_,callback_,block_, options_){
			if(block_ !== undefined && block_ === true){
				this.indicator(true, function(){
					PB._post.apply(PB, [data_, function(result_, response_text_, cause_){
						PB.indicator(false);
						callback_(result_, response_text_, cause_);
					}, options_]);
				});
			}else{
				PB._post.apply(PB,[data_, callback_]);
			}
		},
		make_url : function(url_, parameters_){
			parameters_ = $.extend({},parameters_);

			var concat_char_ = "?";
			if(url_.indexOf(concat_char_) >= 0)
				concat_char_ = "&";
			return url_+concat_char_+$.param(parameters_);
		},append_url : function(url_, append_path_){
				

			if(url_.lastIndexOf("/") != (url_.length -1)){
				url_ += "/";
			}

			return (url_ + append_path_);
		},modal : function(options_, callback_){
			console.error("please override me!");
			return;
		},confirm : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : PBVAR['OK'],
				button2 : PBVAR['cancel']
			},options_), callback_);
		},alert : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : PBVAR['OK']
			},options_), callback_);
		},forward : function(url_, params_, target_){
			var temp_form_ = document.createElement('form');

			for(var key_ in params_){
				var value_ = params_[key_];
				var param_element_ = document.createElement('input');
					param_element_.setAttribute('type', 'hidden');
					param_element_.setAttribute('name', key_);
					param_element_.setAttribute('value', value_);

				temp_form_.appendChild(param_element_);
			}

			if(target_){
				temp_form_.setAttribute('target', target_);
			}
			
			temp_form_.setAttribute('method', 'post');
			temp_form_.setAttribute('action', url_);

			document.body.appendChild(temp_form_);
			temp_form_.submit();
		},redirect : function(url_, params_){
			document.location = PB.make_url(url_, params_);
		},
		refresh : function(){
			var methods = [
				"location.reload()",
				"history.go(0)",
				"location.href = location.href",
				"location.href = location.pathname",
				"location.replace(location.pathname)",
				"location.reload(false)"
			];

			var $body = $("body");
			for(var i = 0; i < methods.length; ++i) {
				(function(cMethod) {
					eval(cMethod);
				})(methods[i]);
			}
		},url_parameter : function(name_){
			return decodeURIComponent((new RegExp('[?|&]' + name_ + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
		},url_path : function(){

			var t_result_ = location.pathname.split("/")
			var result_ = [];

			for(var index_=0;index_<t_result_.length; ++index_){
				if(t_result_[index_] !== ""){
					result_.push(t_result_[index_]);
				}
			}

			return result_;
		},
		export_youtube_id : function(url_){
			var video_id_ = url_.split('v=')[1];
			if(video_id_ === undefined){
				return null;
			}

			var ampersand_position_ = video_id_.indexOf('&');
			if(ampersand_position_ != -1) {
				video_id_ = video_id_.substring(0, ampersand_position_);
			}
			return video_id_;
		},
		week_of_month : function(date) {
			var day = date.getDate()
            day += (date.getDay() == 0 ? 0 : 7 - date.getDay());
            return Math.ceil(parseFloat(day) / 7);
		},
		geolocation : function(callback_){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(function(position_){
					var results_ = {
						"lat" : position_.coords.latitude,
						"lng" : position_.coords.longitude
					};

					callback_(results_);

				},function(){
					callback_(false, "failed");
				});
			}else{
				callback_(false, "not_supported");
			}
		},
		indicator_message : function(){
			return "<div class='pb-indicator-frame'><div class='uil-ring-css'><div></div></div></div>";
		},
		indicator : function(bool_, callback_){
			$("body").toggleClass('block-ui-in', bool_);
			if(bool_){
				$.blockUI({
					message : PB.indicator_message(),
					onBlock : callback_
				});
			}else{
				$.unblockUI({
					onUnblock : callback_
				});
			}
		},
		click_position : function(base_element_, event_, offset_){
			offset_ = $.extend({x : 0, y: 0}, offset_);

			var pageX_ = null,pageY_ = null;

			var touch_event_ = event_.originalEvent.changedTouches;

			if(touch_event_ && touch_event_.length > 0){
				pageX_ = touch_event_[0].pageX;
				pageY_ = touch_event_[0].pageY;
			}else{
				touch_event_ = event_.originalEvent.touches;

				if(touch_event_ && touch_event_.length > 0){
					pageX_ = touch_event_[0].pageX;
					pageY_ = touch_event_[0].pageY;
				}
			}

			if(!pageX_ || pageX_ === null){
				pageX_ = event_.pageX;
				pageY_ = event_.pageY;
			}

			var posX = base_element_.offset().left,posY = base_element_.offset().top;
			var x_ = (pageX_ - posX) + offset_.x;
			var y_ = (pageY_ - posY) + offset_.y;

			var width_ = base_element_.outerWidth();
			var height_ = base_element_.outerHeight();

			return {
				"x" : x_, "y" : y_,
				"xrate" : (x_ / width_), "yrate" : (y_ / height_)};
		},classic_popup : function (url_, options_) {

			options_ = $.extend({
				'width' : 500,
				'height' : 400,
				'resize' : false,
				'title' : "untitled"
			},options_);

			var strResize_ = (options_['resize'] ? 'yes' : 'no');

			var strParam = 'width=' + options_['width'] + ',height=' + options_['height'] + ',resizable=' + strResize_,
				objWindow = window.open(url_, options_['title'], strParam);

			objWindow.focus();

			return objWindow;
		},unix_to_date : function(unix_, format_){
			var moment_ = moment.unix(unix_);
			return moment_.format(format_);
		}
	}

	$.fn.smooth_inout = (function(toggle_, options_){

		if(this.length > 1){
			this.each(function(){
				$(this).smooth_inout(toggle_, options_);
			});
			return;
		}

		options_ = $.extend({
			in_class : 'in',
			out_class : 'out',
			animating_class : 'animating',
			end_class : 'end',
			duration : 300,
			callback : $.noop
		},options_);

		var inclass_ = options_.in_class;
		var outclass_ = options_.out_class;
		var animating_class_ = options_.animating_class;
		var end_class_ = options_.end_class;
		var animate_duration_ = options_.duration;
		var callback_ = options_.callback;

		if(toggle_ === undefined || toggle_ === null){
			toggle_ = !(this.hasClass(inclass_) && !this.hasClass(outclass_));
		}

		/*if(this.hasClass(animating_class_)){
			return !toggle_;
		}*/

		clearTimeout(this.data('pb_smooth_inout_timeout'));

		this.toggleClass(animating_class_, true);
		this.toggleClass(end_class_, false);

		if(toggle_){

			if(this.hasClass(inclass_)){
				this.toggleClass(outclass_, false);
			}else{
				this.toggleClass(inclass_, true);
			}

		}else{

			if(this.hasClass(inclass_)){
				this.toggleClass(outclass_, true);
			}else{
				this.toggleClass(inclass_, true);
				this.toggleClass(outclass_, true);
			}
		}

		var timeout_obj_ = setTimeout($.proxy(function(){
			this.target.toggleClass(this.animating_class, false);
			this.target.toggleClass(this.end_class, true);
			this.callback(this.toggle);
		},{
			target : this,
			toggle : toggle_,
			animating_class : animating_class_,
			end_class : end_class_,
			callback : callback_
		}), animate_duration_);

		this.data('pb_smooth_inout_timeout', timeout_obj_);

		return toggle_;
	});

	window.pb_add_filter = (function(key_, func_){
		return PB.add_data_filter(key_, func_);
	});
	window.pb_apply_filters = (function(key_, params_, add_){
		return PB.apply_data_filters(key_, params_, add_);
	});
	window.add_data_action = (function(key_, func_){
		return PB.add_data_action(key_, func_);
	});
	window.do_data_action = (function(key_, params_){
		return PB.do_data_action(key_, params_);
	});

	return PB;

})(jQuery);
(function($){

	window.PB.crypt = {
		_public_key_ : (PBVAR["crypt-public-key-n"]+"|"+PBVAR["crypt-public-key-e"]),
		//암호화하기
	    encrypt : function(text_){
	      var result_ = cryptico.encrypt(text_, this._public_key_);
	      if(result_.status === "success"){
	        return result_.cipher;
	      }else return false;
	    }
	}

	//param 필터 등록
	PB.add_data_filter("encrypt", function(params_, add_){
		if(add_ === undefined || add_ === null) return params_;
		var add_count_ = add_.length;
		for(var index_=0;index_<add_count_;++index_){
			var param_name_ = add_[index_];
			params_[param_name_] = PB.crypt.encrypt(params_[param_name_]);
		}

		return params_;
	});

	return PB.crypt;

})(jQuery);
(function($){

	var NON_CHAR_KEYCODE = window.NON_CHAR_KEYCODE = {
		 BACKSPACE : 8
		,TAB : 9
		,ENTER : 13
		,SHIFT : 16
		,CTRL : 17
		,ALT : 18
		,PAUSE : 19
		,CAPSLOCK : 20
		,ESCAPE : 27
		,PAGEUP : 33
		,PAGEDOWN : 34
		,END : 35
		,HOME : 36
		,LEFT : 37
		,UP : 38
		,RIGHT : 39
		,DOWN : 40
		,INSERT : 45
		,DELETE : 46
	}

	window.is_esc_key = (function(key_code_a_){
		return (NON_CHAR_KEYCODE.ESCAPE === key_code_a_);
	});

	window.is_enter_key = (function(key_code_a_){
		return (NON_CHAR_KEYCODE.ENTER === key_code_a_);
	});

	window.is_whitespace_key = (function(key_code_a_){
		return (NON_CHAR_KEYCODE.ENTER === key_code_a_ || NON_CHAR_KEYCODE.TAB === key_code_a_);
	});

	window.is_null = (function(target_){
		return (target_ === undefined || target_ === null || target_ ==="");
	});


	$.fn.serialize_object = (function(map_data_, key_array_convert_){
		key_array_convert_ = (key_array_convert_ === undefined ? false : key_array_convert_);
		var result_ = {};
		var pass_map_data_ = (map_data_ === undefined || map_data_ === null);
		var that_ = this;
		var array_ = this.serializeArray();
		$.each(array_, function(){
			var converted_name_ = this.name;
			var target_input_ = that_.find('[name="'+converted_name_+'"]');

			if(!pass_map_data_){
				if(map_data_[this.name] !== undefined && map_data_[this.name] !== null)
					converted_name_ = map_data_[this.name];
				else return true;
			}
				
			if(key_array_convert_ === true && converted_name_.indexOf("[]") >= 0){
				converted_name_ = converted_name_.replace("[]","");
			}

			if(result_[converted_name_] !== undefined){
				if(!result_[converted_name_].push){
					result_[converted_name_] = [result_[converted_name_]];
				}

				result_[converted_name_].push(PB.apply_data_filters('pb-serialize-object', this.value, target_input_) || '');
			}else result_[converted_name_] = (PB.apply_data_filters('pb-serialize-object', this.value, target_input_) || '');
		});

		return result_;
	});

	$.fn.update_form = (function(data_){

		var inputs_ = this.find(":input");

		$.each(inputs_, function(){
			var input_ = $(this);
			var value_ = data_[input_.attr("name")];

			if(value_ !== undefined){
				input_.val(value_);
			}
		});

	});

	$.fn.sval = (function(value_){
		if(value_ !== undefined){
			var options_ = $(this).children("option");
			options_.each(function(){
				if($(this).val() === value_) $(this).attr("selected",true);
				else $(this).removeAttr("selected");
			});
		}

		return $(this).find("option:selected").val();
	});

	$.fn.checkbox_mass_toggle = (function(){
		var checkboxes_ = this.data("checkbox-mass-toggle").split(",");

		this.data("mass-checkboxes", checkboxes_);

		this.on("change", function(){
			var checkboxes_ = $(this).data("mass-checkboxes");			
			var checked_ = $(this).is(":checked");

			for(var index_=0;index_<checkboxes_.length;++index_){
				var target_checkbox_ = $(checkboxes_[index_]);
					target_checkbox_.prop("checked", checked_);
					target_checkbox_.change();
			}
		});


		for(var index_=0;index_<checkboxes_.length;++index_){
			var target_checkbox_ = $(checkboxes_[index_]);
				target_checkbox_.data("root-checkbox", $(this));

			target_checkbox_.change(function(){
				var temp_ = $(this);
				var root_checkbox_ = temp_.data("root-checkbox");
				var tcheckboxes_ = root_checkbox_.data("mass-checkboxes");

				var result_ = true;

				for(var tindex_=0;tindex_<tcheckboxes_.length;++tindex_){

					var tmp_checkbox_ = $(tcheckboxes_[tindex_]);

					$.each(tmp_checkbox_, function(){
						if(!$(this).is(":checked")){
							result_ = false;
							return false;
						}
					});

					if(!result_) break;
				}

				root_checkbox_.prop("checked", result_);

			});
		}
	});

	$.fn.indicator = (function(bool_, options_){
		var indicator_message_ = PB.indicator_message();
		
		this.toggleClass('block-ui-in', bool_);
		if(bool_){
			this.block($.extend({
				message : indicator_message_
			},options_));
		}else{
			this.unblock(options_);
		}
	});

	$.fn.tap_event = (function(callback_){
		return this.on('mousedown', callback_);
	});

	$(document).ready(function(){
		var toggle_buttons_ = $("[data-checkbox-mass-toggle]");
		if(toggle_buttons_.length > 0){
			toggle_buttons_.each(function(){
				$(this).checkbox_mass_toggle();
			});
		}
	});
	
})(jQuery);
(function($){

	var PB_list_table = (function(target_){
		this._target = target_;
		this._target.data("pblisttable", this);
		this._global_id = this._target.data("pb-listtable-id");
		this._table_form = this._target.closest("form");
		this._page_index_el = this._table_form.find("input[name='page_index']");
		this._pagenav = $("[data-pb-listtable-pagenav-id='"+this._global_id+"']");

		this._rander_pagenav();
	});
	PB_list_table.prototype.target = (function(){
		return this._target;
	});
	PB_list_table.prototype.page_index = (function(page_index_){
		if(page_index_ !== undefined){
			this._page_index_el.val(page_index_);
			this.load();
		}

		return this._page_index_el.val();
	});
	PB_list_table.prototype.load = (function(){
		this._table_form.submit();
	});

	PB_list_table.prototype._rander_pagenav = (function(){
		var pagenav_buttons_ = this._pagenav.find("a[data-page-index]");
		var that_ = this;

		$.each(pagenav_buttons_, function(){
			var target_ = $(this);
			target_.data("pb-listtable",that_);
			target_.click(function(event_){
				event_.preventDefault();
				var pagenav_btn_ = $(this);
				var page_index_ = pagenav_btn_.data("page-index");
				var listtable_module_ = pagenav_btn_.data("pb-listtable");

				var current_page_index_ = listtable_module_.page_index();
				listtable_module_.page_index(page_index_);
			});
		});
	});

	$.fn.pblisttable = (function(){
		var target_ = this;

		if(target_.data("pblisttable") !== undefined && target_.data("pblisttable") !== null){
			return target_.data("pblisttable");
		}

		return new PB_list_table(target_);
	});

	var PB_ajax_list_table = (function(target_){
		PB_list_table.apply(this, [target_]);

		this._options = {
			'before_load' : function(){
				return true;
			}
		};

		this._table_form.data("htajaxlisttable", this);
		this._table_form.on("submit", function(){
			var listtable_module_ = $(this).data("htajaxlisttable");
			listtable_module_.load();
			return false;
		});

		var autoload_el_ = this._table_form.find(":input[name='firstload']");
		if(autoload_el_.length <= 0 || autoload_el_.val() !== "N"){
			this.load();
		}
	});

	PB_ajax_list_table.prototype = Object.create(PB_list_table.prototype);

	PB_ajax_list_table.prototype.load = (function(){
		if(!this._options['before_load'].apply(this)) return;

		var table_form_ = this._table_form;
		var table_params_ = table_form_.serialize_object();

		var that_ = this;
		var target_ = this._target;
		var thead_el_ = this._target.children("thead");
		var tbody_el_ = this._target.children("tbody");
		var pagenav_el_ = this._pagenav;

		target_.trigger("pblisttablestartload");
		
		tbody_el_.empty();
		pagenav_el_.empty();

		tbody_el_.append("<tr><td class='ajax-loading' colspan='"+thead_el_.find("tr > th").length+"'><center><i class='sicon sicon-spinner sicon-hc-spin'></i></center></td></tr>");

		PB.post($.extend(table_params_,{
			action : "pb-listtable-load-html",
			global_id : this._global_id
		}),function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				return;
			}

			tbody_el_.empty();
			pagenav_el_.empty();
			tbody_el_.append(response_json_.body_html);
			pagenav_el_.append(response_json_.pagenav_html);
			that_._rander_pagenav();
			target_.trigger("pblisttableload", [response_json_.orgdata, tbody_el_.children()]);
		});
	});

	$.fn.htajaxlisttable = (function(){
		var target_ = this;

		if(target_.data("pblisttable") !== undefined && target_.data("pblisttable") !== null){
			return target_.data("pblisttable");
		}

		return new PB_ajax_list_table(target_);
	});

})(jQuery);

function _pb_list_table_initialize(global_id_, ajax_){
	$ = jQuery;
	if(ajax_ === undefined || ajax_ === null){
		ajax_ = false;
	}

	if(ajax_ === true){
		$("[data-pb-listtable-id='"+global_id_+"']").htajaxlisttable();
	}else{
		$("[data-pb-listtable-id='"+global_id_+"']").pblisttable();
	}
	
}
(function($){

	var PB_list_grid = (function(target_){
		this._target = target_;
		this._target.data("pblistgrid", this);
		this._global_id = this._target.data("pb-listgrid-id");
		this._table_form = this._target.closest("form");
		this._page_index_el = this._table_form.find("input[name='page_index']");
		this._pagenav = $("[data-pb-listgrid-pagenav-id='"+this._global_id+"']");

		this._rander_pagenav();
	});
	PB_list_grid.prototype.target = (function(){
		return this._target;
	});
	PB_list_grid.prototype.page_index = (function(page_index_){
		if(page_index_ !== undefined){
			this._page_index_el.val(page_index_);
			this.load();
		}

		return this._page_index_el.val();
	});
	PB_list_grid.prototype.load = (function(){
		this._table_form.submit();
	});

	PB_list_grid.prototype._rander_pagenav = (function(){
		var pagenav_buttons_ = this._pagenav.find("a[data-page-index]");
		var that_ = this;

		$.each(pagenav_buttons_, function(){
			var target_ = $(this);
			target_.data("pb-listgrid",that_);
			target_.click(function(event_){
				event_.preventDefault();
				var pagenav_btn_ = $(this);
				var page_index_ = pagenav_btn_.data("page-index");
				var listgrid_module_ = pagenav_btn_.data("pb-listgrid");

				var current_page_index_ = listgrid_module_.page_index();

				if(parseInt(current_page_index_) != parseInt(page_index_)){
					listgrid_module_.page_index(page_index_);
				}
			});
		});
	});

	$.fn.pblistgrid = (function(){
		var target_ = this;

		if(target_.data("pblistgrid") !== undefined && target_.data("pblistgrid") !== null){
			return target_.data("pblistgrid");
		}

		return new PB_list_grid(target_);
	});

	var PB_ajax_list_grid = (function(target_){
		PB_list_grid.apply(this, [target_]);

		this._options = {
			'before_load' : function(){
				return true;
			}
		};

		this._table_form.data("htajaxlistgrid", this);
		this._table_form.on("submit", function(){
			var listgrid_module_ = $(this).data("htajaxlistgrid");
			listgrid_module_.load();
			return false;
		});

		var autoload_el_ = this._table_form.find(":input[name='firstload']");
		if(autoload_el_.length <= 0 || autoload_el_.val() !== "N"){
			this.load();
		}
	});

	PB_ajax_list_grid.prototype = Object.create(PB_list_grid.prototype);

	PB_ajax_list_grid.prototype.load = (function(){
		if(!this._options['before_load'].apply(this)) return;

		var table_form_ = this._table_form;
		var table_params_ = table_form_.serialize_object();

		var that_ = this;
		var target_ = this._target;
		// var tbody_el_ = this._target;
		var pagenav_el_ = this._pagenav;

		target_.trigger("pblistgridstartload");
		
		target_.empty();
		pagenav_el_.empty();

		target_.append("<div class='loading-indicator'><i class='icon pb-loading-indicator'></i></div>");

		PB.post($.extend(table_params_,{
			action : "pb-listgrid-load-html",
			global_id : this._global_id
		}),function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				return;
			}

			target_.empty();
			target_.append(response_json_.body_html);
			pagenav_el_.append(response_json_.pagenav_html);
			that_._rander_pagenav();
			target_.trigger("pblistgridload", [response_json_.orgdata, target_.children()]);
		});
	});

	$.fn.htajaxlistgrid = (function(){
		var target_ = this;

		if(target_.data("pblistgrid") !== undefined && target_.data("pblistgrid") !== null){
			return target_.data("pblistgrid");
		}

		return new PB_ajax_list_grid(target_);
	});

})(jQuery);

function _pb_list_grid_initialize(global_id_, ajax_){
	$ = jQuery;
	if(ajax_ === undefined || ajax_ === null){
		ajax_ = false;
	}

	if(ajax_ === true){
		$("[data-pb-listgrid-id='"+global_id_+"']").htajaxlistgrid();
	}else{
		$("[data-pb-listgrid-id='"+global_id_+"']").pblistgrid();
	}
	
}
(function($){

	window.PB.address = {
		

		geolocation : function(callback_){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(function(position_){
					var latlng_ = {
						"lat" : position_.coords.latitude,
						"lng" : position_.coords.longitude
					};

					PB.maps._cache_latlng(latlng_, function(success_, latlng_){
						PB.do_data_action("geolocation", {
							type : "geo",
							latlng : latlng_
						});
						callback_(success_, latlng_);
					});
				},function(){
					callback_(false);
				});
			}else{
				callback_(false);
			}
		},

		_check_maps_api : function(){
			var result_ = (daum !== undefined && daum.maps !== undefined);

			if(!result_) console.error('daum.maps API must included');

			return result_;
		},
		_check_postcode_api : function(){
			var result_ =  (daum !== undefined && daum.Postcode !== undefined);

			if(!result_) console.error('daum.Postcode API must included');

			return result_;
		},

		search_postcode : function(callback_, options_){
			callback_ = callback_ || $.noop;

			if(!this._check_postcode_api()){
				callback_(false);
				 return;
			}

			callback_ = callback_ || $.noop;

			options_ = $.extend({
				opentype : "embed",
				onclose: $.proxy(function(state_){
					if(state_ === 'FORCE_CLOSE'){
						this(false);
					}
				}, callback_),
				oncomplete : $.proxy(function(data_){

					this(true, {
						postcode : data_['zonecode'],
						address : (data_['autoJibunAddress'] || data_['jibunAddress']),
						address_road : (data_['autoRoadAddress'] || data_['roadAddress']),
						si : data_['sido'],
						gu : data_['sigungu'],
						dong : data_['bname'],
					});

				}, callback_)
			},options_);

			var daum_map_ = new daum.Postcode(options_);

			if(options_.opentype === "embed" && options_.element){
				daum_map_.embed(options_.element);
			}else daum_map_.open();			
		}, search_latlng : function(address_, callback_){
			callback_ = callback_ || $.noop;

			if(!this._check_maps_api()){
				callback_(false);
				 return;
			}

			var geocoder_ = new daum.maps.services.Geocoder();

			geocoder_.addr2coord(address_, $.proxy(function(status_, result_){
				if(status_ !== daum.maps.services.Status.OK){
					this(false);
					return;
				}

				this(true, {
					lat : result_.addr[0].lat,
					lng : result_.addr[0].lng
				});

			}, callback_));
		}
	};

})(jQuery);
(function($){
	PB.file = $.extend(PB.file, {
		_upload_url : PBVAR["fileupload_url"],
		upload_url : function(param_){

			if(!param_){
				return this._upload_url;				
			}

			return PB.make_url(this._upload_url, param_);
		}
	});

	$(document).on('dragover', function(event_){
		var dropZones = $('.btn-fileupload-dropzone'),
			timeout = window.pbfileupload_dropzone_timeout;
		if (timeout) {
			clearTimeout(timeout);
		}

		var hoveredDropZone = $(event_.target).closest(dropZones);
		dropZones.not(hoveredDropZone).removeClass('dropin');
		hoveredDropZone.addClass('dropin');
		window.pbfileupload_dropzone_timeout = setTimeout(function () {
			window.pbfileupload_dropzone_timeout = null;
			dropZones.removeClass('dropin');
		}, 100);
	});


	var pb_fileupload_btn = (function(target_, options_){
		this._target = target_;

		this._options = $.extend({
			upload_url : PB.file.upload_url(),
			autoupload : false,
			dropzone : false,
			accept : null,
			limit : null,
			
			progress : $.noop,
			start : $.noop,
			fail : $.noop,
			done : $.noop,

			label_icon : '<i class="glyphicon glyphicon-plus"></i>',
			label : '파일선택',
			label_dropzone : '파일을 드래그하여 업로드할 수 있습니다.',

			button_class : 'btn-default',
		},options_);

		this._target.wrap("<span class='btn btn-fileupload'></span>");
		this._wrap = this._target.parent();

		this._wrap.addClass(this._options['button_class']);
		this._wrap.toggleClass("btn-fileupload-dropzone", this._options['dropzone']);

	
		this._wrap.append("<span class='label-icon'></span>");
		this._wrap.append("<span class='label-text'></span>");
		this._wrap.append('<i class="loading-indicator fa fa-circle-o-notch fa-spin fa-fw"></i>');

		this._label = this._wrap.find(".label-text");
		this._label_icon = this._wrap.find(".label-icon");
		this._loading_indicator = this._wrap.find(".loading-indicator");

		this._label.html(this._options['label']);
		this._label_icon.html(this._options['label_icon']);

		if(this._options['dropzone']){
			this._wrap.append("<span class='label-dropzone'>"+this._options['label_dropzone']+"</span>");
		}

		this._target.fileupload({
			// xhrFields: {withCredentials: true},
			url: this._options['upload_url'],
			dataType : 'json',
			dropZone : target_,
			autoupload : this._options['autoupload'],
			limitMultiFileUploads : this._options['limit'],
			start : $.proxy(function(event_, data_){
				this.toggle_loading(true);
				this._options['start'].apply(this, [data_]);
			}, this), 
			fail: $.proxy(function(event_, data_){
				this._options['fail'].apply(this, [data_]);
			}, this), 
			done: $.proxy(function(event_, data_){

				var files_ = data_.result.files;
				var result_ = [];

				$.each(files_, function(){
					result_.push({
						'name' : this['name'],
						'url' : this['url'],
						'thumbnail_url' : this['thumbnailUrl'],
						'type' : this['type'],
					});
				});

				this._options['done'].apply(this, [result_]);
			}, this),
			always :  $.proxy(function(event_, data_){
				this.toggle_loading(false);
			}, this),
			progressall: $.proxy(function(event_, data_){
				var progress_ = parseInt(data_.loaded / data_.total * 100, 10);
				this._options['progress'].apply(this, [progress_]);
			}, this)

		});

		this._target.data('pb-fileupload-btn-module', this);
	});

	pb_fileupload_btn.prototype.toggle_loading = (function(bool_){
		this._wrap.toggleClass("loading", bool_);
		this._wrap.toggleClass('disabled', bool_);
		this._target.prop('disabled', bool_);
	});

	$.fn.pb_fileupload_btn = (function(options_){
		var module_ = this.data('pb-fileupload-btn-module');
		if(module_) return module_;
		return new pb_fileupload_btn(this, options_);
	});

	
})(jQuery);
(function($){

	var _pb_image_uploader_btn = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'use_thumbnail' : true,
			'callback' : $.noop,
		}, options_);


		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-image-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-image-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		this._uploader_modal.find("#pb-image-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : PB.file.upload_url(),
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				this._options['callback'].apply(this, [files_]);
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._target.click($.proxy(function(){
			$("#pb-image-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._target.data('pb-image-uploader-module', this);
	}

	$.fn.pb_image_uploader_btn = (function(options_){
		var module_ = this.data('pb-image-uploader-module');
		if(module_) return module_;
		return new _pb_image_uploader_btn(this, options_);
	});
	
	var _pb_multiple_image_uploader = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'change' : $.noop,
			'max_width' : null,
		}, options_);

		this._target.wrap("<div class='pb-multiple-image-uploader pb-image-list-frame'></div>");
		this._wrap = this._target.parent();
		this._
		
		this._add_btn = $("<a href='#' class='add-btn'>+</a>");
		this._add_btn.appendTo(this._wrap);

		this._target.data('pb-multiple-image-uploader-module', this);

		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-multiple-image-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-multiple-image-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		var upload_url_ = PB.file.upload_url();

		if(this._options['max_width']){
			upload_url_ = PB.file.upload_url({
				'max_width' : this._options['max_width']
			});
		}

		this._uploader_modal.find("#pb-multiple-image-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : upload_url_,
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				$.each(files_, function(){
					module_.add(this);
				});
			
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._add_btn.click($.proxy(function(){
			$("#pb-multiple-image-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._wrap.sortable({
			items: '.image-item'
		});

		this._wrap.on("sortupdate",$.proxy(function(){
			this._apply_to_input();
		}, this));

		var target_json_ = null;

		try{
			target_json_ = JSON.parse(this._target.val());
		}catch(e_){
			target_json_ = [];
		}

		this.apply_json(target_json_);
	};

	_pb_multiple_image_uploader.prototype.target = (function(){
		return this._target;
	});

	_pb_multiple_image_uploader.prototype.options = (function(options_){
		if(options_ !== undefined){
			this._options = $.extend(true, {
				'maxlength' : 10,
				'modal_label' : '이미지업로드'
			}, options_);
		}

		return this._options;
	});

	_pb_multiple_image_uploader.prototype.add = (function(data_){
		var preview_el_ = $("<div class='preview'></div>");
			preview_el_.css({
				'backgroundImage' : "url("+(this._options['use_thumbnail'] ? data_['thumbnail_url'] : data_['url'])+")"
			});

		var image_item_el_ = $("<div class='image-item'></div>");
			image_item_el_.append(preview_el_);
			image_item_el_.append("<a href='#' class='delete-btn'></a>");

		image_item_el_.attr("data-thumbnail-url", data_['thumbnail_url']);
		image_item_el_.attr("data-url", data_['url']);
		image_item_el_.attr("data-name", data_['name']);
		image_item_el_.attr("data-type", data_['type']);

		this._add_btn.before(image_item_el_);

		image_item_el_.find(".delete-btn").click($.proxy(function(event_){
			$(event_.currentTarget).closest(".image-item").remove();
			this._update_add_btn();
			this._apply_to_input();
			this._options['change'].apply(this);
			return false;
		}, this));

		this._update_add_btn();
		this._apply_to_input();
		this._options['change'].apply(this);	
		
	});

	_pb_multiple_image_uploader.prototype._update_add_btn = (function(){
		var toggle_ = (this._options['maxlength'] > this._wrap.find(".image-item").length);
		this._add_btn.toggle(toggle_);
	});
		
	_pb_multiple_image_uploader.prototype.apply_json = (function(json_){
		this._wrap.find(".image-item").remove();

		if($.type(json_) !== "array") return;

		var that_ = this;
		$.each(json_, function(){
			that_.add(this);
		});
		this._apply_to_input();
		this._update_add_btn();
	});
	_pb_multiple_image_uploader.prototype.to_json = (function(){
		var image_items_ = this._wrap.children(".image-item");
		var results_ = [];

		if(image_items_.length > 0){
			image_items_.each(function(){
				var image_item_ = $(this);

				results_.push({
					name : image_item_.attr("data-name"),
					thumbnail_url : image_item_.attr("data-thumbnail-url"),
					url : image_item_.attr("data-url"),
					type : image_item_.attr("data-type"),
				});
			});
		}
			

		return results_;
	});
	_pb_multiple_image_uploader.prototype._apply_to_input = (function(){
		this._target.val(JSON.stringify(this.to_json()));
	});


	$.fn.pb_multiple_image_uploader = (function(options_){
		var module_ = this.data('pb-multiple-image-uploader-module');
		if(module_) return module_;
		return new _pb_multiple_image_uploader(this, options_);
	});

	
})(jQuery);
(function($){

	var _pb_file_uploader_btn = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '파일업로드',
			'callback' : $.noop,
		}, options_);


		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-fileupload-dropzone-modal modal" id="pb-file-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-file-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*,.zip,.pdf,.hwp,.xls,.xlsx,.doc,.docx,*.ppt,*.pptx" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		this._uploader_modal.find("#pb-file-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : PB.file.upload_url(),
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(zip|hwp|pdf|doc|docx|ppt|pptx|xls|xlsx|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				this._options['callback'].apply(this, [files_]);
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._target.click($.proxy(function(){
			$("#pb-file-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._target.data('pb-file-uploader-module', this);
	}

	$.fn.pb_file_uploader_btn = (function(options_){
		var module_ = this.data('pb-file-uploader-module');
		if(module_) return module_;
		return new _pb_file_uploader_btn(this, options_);
	});
	
	var _pb_multiple_file_uploader = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '파일업로드',
			'change' : $.noop
		}, options_);

		this._target.wrap("<div class='pb-multiple-file-uploader pb-file-list-frame'></div>");
		this._wrap = this._target.parent();
		
		this._add_btn = $("<a href='#' class='add-btn btn btn-sm btn-default'><i class='fa fa-plus'></i> 파일추가</a>");
		this._add_btn.appendTo(this._wrap);

		this._target.data('pb-multiple-file-uploader-module', this);

		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-fileupload-dropzone-modal modal" id="pb-multiple-file-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-multiple-file-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="file/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		this._uploader_modal.find("#pb-multiple-file-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : PB.file.upload_url(),
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : true,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				$.each(files_, function(){
					module_.add(this);
				});
			
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._add_btn.click($.proxy(function(){
			$("#pb-multiple-file-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._wrap.sortable({
			items: '.file-item'
		});

		this._wrap.on("sortupdate",$.proxy(function(){
			this._apply_to_input();
		}, this));

		var target_json_ = null;

		try{
			target_json_ = JSON.parse(this._target.val());
		}catch(e_){
			target_json_ = [];
		}

		this.apply_json(target_json_);
	};

	_pb_multiple_file_uploader.prototype.target = (function(){
		return this._target;
	});

	_pb_multiple_file_uploader.prototype.options = (function(options_){
		if(options_ !== undefined){
			this._options = $.extend(true, {
				'maxlength' : 10,
				'modal_label' : '파일업로드'
			}, options_);
		}

		return this._options;
	});

	_pb_multiple_file_uploader.prototype.add = (function(data_){
		
		var file_item_el_ = $("<div class='file-item'></div>");
			file_item_el_.append('<i class="icon fa fa-file-o" aria-hidden="true"></i>');
			file_item_el_.append('<span>' + data_['name'] + '</span>');
			file_item_el_.append('<a href="#" class="delete-btn"><i class="fa fa-times" aria-hidden="true"></i></a>');

		file_item_el_.attr("data-url", data_['url']);
		file_item_el_.attr("data-name", data_['name']);

		this._add_btn.before(file_item_el_);

		file_item_el_.find(".delete-btn").click($.proxy(function(event_){
			$(event_.currentTarget).closest(".file-item").remove();
			this._update_add_btn();
			this._apply_to_input();
			this._options['change'].apply(this);
			return false;
		}, this));

		this._update_add_btn();
		this._apply_to_input();
		this._options['change'].apply(this);	
		
	});

	_pb_multiple_file_uploader.prototype._update_add_btn = (function(){
		var toggle_ = (this._options['maxlength'] > this._wrap.find(".file-item").length);
		this._add_btn.toggle(toggle_);
	});
		
	_pb_multiple_file_uploader.prototype.apply_json = (function(json_){
		this._wrap.find(".file-item").remove();

		if($.type(json_) !== "array") return;

		var that_ = this;
		$.each(json_, function(){
			that_.add(this);
		});
		this._apply_to_input();
		this._update_add_btn();
	});
	_pb_multiple_file_uploader.prototype.to_json = (function(){
		var file_items_ = this._wrap.children(".file-item");
		var results_ = [];

		if(file_items_.length > 0){
			file_items_.each(function(){
				var file_item_ = $(this);

				results_.push({
					name : file_item_.attr("data-name"),
					url : file_item_.attr("data-url"),
				});
			});
		}
			

		return results_;
	});
	_pb_multiple_file_uploader.prototype._apply_to_input = (function(){
		this._target.val(JSON.stringify(this.to_json()));
	});


	$.fn.pb_multiple_file_uploader = (function(options_){
		var module_ = this.data('pb-multiple-file-uploader-module');
		if(module_) return module_;
		return new _pb_multiple_file_uploader(this, options_);
	});

	
})(jQuery);
(function($){

	var _pb_slider_uploader_btn = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'callback' : $.noop,
		}, options_);


		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-slider-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-slider-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		this._uploader_modal.find("#pb-slider-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : PB.file.upload_url(),
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				this._options['callback'].apply(this, [files_]);
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._target.click($.proxy(function(){
			$("#pb-slider-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._target.data('pb-slider-uploader-module', this);
	}

	$.fn.pb_slider_uploader_btn = (function(options_){
		var module_ = this.data('pb-slider-uploader-module');
		if(module_) return module_;
		return new _pb_slider_uploader_btn(this, options_);
	});
	
	var _pb_multiple_slider_uploader = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'change' : $.noop,
			'max_width' : null,
		}, options_);

		this._target.wrap("<div class='pb-multiple-slider-uploader pb-slide-list-frame'></div>");
		this._wrap = this._target.parent();
		
		this._add_btn = $("<a href='#' class='add-btn'>+</a>");
		this._add_btn.appendTo(this._wrap);

		this._target.data('pb-multiple-slider-uploader-module', this);

		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-multiple-slider-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-multiple-slider-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		var upload_url_ = PB.file.upload_url();

		if(this._options['max_width']){
			upload_url_ = PB.file.upload_url({
				'max_width' : this._options['max_width']
			});
		}

		this._uploader_modal.find("#pb-multiple-slider-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : upload_url_,
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				$.each(files_, function(){
					module_.add(this);
				});
			
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._add_btn.click($.proxy(function(){
			$("#pb-multiple-slider-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._wrap.sortable({
			items: '.slide-item'
		});

		var target_json_ = null;

		try{
			target_json_ = JSON.parse(this._target.val());
		}catch(e_){
			target_json_ = [];
		}

		this.apply_json(target_json_);
	};

	_pb_multiple_slider_uploader.prototype.target = (function(){
		return this._target;
	});

	_pb_multiple_slider_uploader.prototype.options = (function(options_){
		if(options_ !== undefined){
			this._options = $.extend(true, {
				'maxlength' : 10,
				'modal_label' : '이미지업로드'
			}, options_);
		}

		return this._options;
	});

	_pb_multiple_slider_uploader.prototype.add = (function(data_){
		var preview_el_ = $("<div class='preview'></div>");
			preview_el_.css({
				'backgroundImage' : "url("+(data_['url'])+")"
			});

		var preview_wrap_el_ = $("<div class='preview-wrap'></div>");
			preview_wrap_el_.append(preview_el_);

		data_ = $.extend(true, {
			url : "",
			title : "",
			content : "",
			button_name : "",
			link_url : "",
		},data_);

		var image_item_el_ = $("<div class='slide-item'></div>");
			image_item_el_.append(preview_wrap_el_);
			image_item_el_.append("<a href='#' class='delete-btn'></a>");

		var slider_content_el_ = $("<div class='content'></div>");
		
		var slider_content_html_ = '<label>타이틀</label>' +
			'<input type="text" placeholder="타이틀 입력" class="form-control input-sm" data-column-name="title" value="'+data_['title']+'">' +
			'<div class="form-margin"></div>' +
			'<label>내용</label>' +
			'<textarea placeholder="내용 입력" class="form-control input-sm" data-column-name="content">'+data_['content']+'</textarea>' +
			'<div class="form-margin"></div>' +
			'<label>버튼명</label>' +
			'<input type="text" placeholder="버튼명 입력" class="form-control input-sm" data-column-name="button_name" value="'+data_['button_name']+'">' +
			'<small class="help-block">*입력하지 않으면 표시되지 않습니다.</small>' + 
			'<div class="form-margin"></div>' +
			'<label>버튼링크</label>' +
			'<input type="url" placeholder="URL 입력" class="form-control input-sm" data-column-name="link_url" value="'+data_['link_url']+'">';

		slider_content_el_.append(slider_content_html_);
		image_item_el_.append(slider_content_el_);

		image_item_el_.attr("data-url", data_['url']);

		this._add_btn.before(image_item_el_);

		image_item_el_.find(".delete-btn").click($.proxy(function(event_){
			$(event_.currentTarget).closest(".slide-item").remove();
			this._update_add_btn();
			
			this._options['change'].apply(this);
			return false;
		}, this));

		this._update_add_btn();
		
		this._options['change'].apply(this);	
		
	});

	_pb_multiple_slider_uploader.prototype._update_add_btn = (function(){
		var toggle_ = (this._options['maxlength'] > this._wrap.find(".slide-item").length);
		this._add_btn.toggle(toggle_);
	});
		
	_pb_multiple_slider_uploader.prototype.apply_json = (function(json_){
		this._wrap.find(".slide-item").remove();

		if($.type(json_) !== "array") return;

		var that_ = this;
		$.each(json_, function(){
			that_.add(this);
		});
		
		this._update_add_btn();
	});
	_pb_multiple_slider_uploader.prototype.to_json = (function(){
		var image_items_ = this._wrap.children(".slide-item");
		var results_ = [];

		if(image_items_.length > 0){
			image_items_.each(function(){
				var image_item_ = $(this);

				var data_url_ = {
					url : image_item_.attr("data-url"),
					title : image_item_.find("[data-column-name='title']").val(),
					content : image_item_.find("[data-column-name='content']").val(),
					button_name : image_item_.find("[data-column-name='button_name']").val(),
					link_url : image_item_.find("[data-column-name='link_url']").val(),
				};

				results_.push(data_url_);
			});
		}
			

		return results_;
	});
	

	$.fn.pb_multiple_slider_uploader = (function(options_){
		var module_ = this.data('pb-multiple-slider-uploader-module');
		if(module_) return module_;
		return new _pb_multiple_slider_uploader(this, options_);
	});

	
})(jQuery);
(function($){

	var _pb_client_slider_uploader_btn = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'callback' : $.noop,
			'max_width' : null,
		}, options_);


		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-client-slider-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-client-slider-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		this._uploader_modal.find("#pb-client-slider-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : PB.file.upload_url(),
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				this._options['callback'].apply(this, [files_]);
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._target.click($.proxy(function(){
			$("#pb-client-slider-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._target.data('pb-client-slider-uploader-module', this);
	}

	$.fn.pb_client_slider_uploader_btn = (function(options_){
		var module_ = this.data('pb-client-slider-uploader-module');
		if(module_) return module_;
		return new _pb_client_slider_uploader_btn(this, options_);
	});
	
	var _pb_multiple_client_slider_uploader = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'change' : $.noop
		}, options_);

		this._target.wrap("<div class='pb-multiple-client-slider-uploader pb-client-slide-list-frame'></div>");
		this._wrap = this._target.parent();
		
		this._add_btn = $("<a href='#' class='add-btn'>+</a>");
		this._add_btn.appendTo(this._wrap);

		this._target.data('pb-multiple-client-slider-uploader-module', this);

		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-multiple-client-slider-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-multiple-client-slider-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		var upload_url_ = PB.file.upload_url();

		if(this._options['max_width']){
			upload_url_ = PB.file.upload_url({
				'max_width' : this._options['max_width']
			});
		}

		this._uploader_modal.find("#pb-multiple-client-slider-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : upload_url_,
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				$.each(files_, function(){
					module_.add(this);
				});
			
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._add_btn.click($.proxy(function(){
			$("#pb-multiple-client-slider-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._wrap.sortable({
			items: '.slide-item'
		});

		var target_json_ = null;

		try{
			target_json_ = JSON.parse(this._target.val());
		}catch(e_){
			target_json_ = [];
		}

		this.apply_json(target_json_);
	};

	_pb_multiple_client_slider_uploader.prototype.target = (function(){
		return this._target;
	});

	_pb_multiple_client_slider_uploader.prototype.options = (function(options_){
		if(options_ !== undefined){
			this._options = $.extend(true, {
				'maxlength' : 10,
				'modal_label' : '이미지업로드'
			}, options_);
		}

		return this._options;
	});

	_pb_multiple_client_slider_uploader.prototype.add = (function(data_){
		var preview_el_ = $("<div class='preview'></div>");
			preview_el_.css({
				'backgroundImage' : "url("+(data_['url'])+")"
			});

		var preview_wrap_el_ = $("<div class='preview-wrap'></div>");
			preview_wrap_el_.append(preview_el_);

		data_ = $.extend(true, {
			url : "",
			link_url : "",
			open_new_window : "Y",
		},data_);

		var image_item_el_ = $("<div class='slide-item'></div>");
			image_item_el_.append(preview_wrap_el_);
			image_item_el_.append("<a href='#' class='delete-btn'></a>");

		var slider_content_el_ = $("<div class='content'></div>");
		
		var slider_content_html_ = '<input type="url" placeholder="URL 입력" class="form-control input-sm" data-column-name="link_url" value="'+data_['link_url']+'">' +
			'<label class="checkbox-inline">' +
  				'<input type="checkbox" data-column-name="open_new_window" '+(data_["open_new_window"] === "Y" ? "checked" : "")+'> 새창으로 열기' +
			'</label>';

		slider_content_el_.append(slider_content_html_);
		image_item_el_.append(slider_content_el_);

		image_item_el_.attr("data-url", data_['url']);

		this._add_btn.before(image_item_el_);

		image_item_el_.find(".delete-btn").click($.proxy(function(event_){
			$(event_.currentTarget).closest(".slide-item").remove();
			this._update_add_btn();
			
			this._options['change'].apply(this);
			return false;
		}, this));

		this._update_add_btn();
		
		this._options['change'].apply(this);	
		
	});

	_pb_multiple_client_slider_uploader.prototype._update_add_btn = (function(){
		var toggle_ = (this._options['maxlength'] > this._wrap.find(".slide-item").length);
		this._add_btn.toggle(toggle_);
	});
		
	_pb_multiple_client_slider_uploader.prototype.apply_json = (function(json_){
		this._wrap.find(".slide-item").remove();

		if($.type(json_) !== "array") return;

		var that_ = this;
		$.each(json_, function(){
			that_.add(this);
		});
		
		this._update_add_btn();
	});
	_pb_multiple_client_slider_uploader.prototype.to_json = (function(){
		var image_items_ = this._wrap.children(".slide-item");
		var results_ = [];

		if(image_items_.length > 0){
			image_items_.each(function(){
				var image_item_ = $(this);

				var data_url_ = {
					url : image_item_.attr("data-url"),
					link_url : image_item_.find("[data-column-name='link_url']").val(),
					open_new_window : (image_item_.find("[data-column-name='open_new_window']").prop("checked") ? "Y" : "N"),
				};

				results_.push(data_url_);
			});
		}
			

		return results_;
	});
	

	$.fn.pb_multiple_client_slider_uploader = (function(options_){
		var module_ = this.data('pb-multiple-client-slider-uploader-module');
		if(module_) return module_;
		return new _pb_multiple_client_slider_uploader(this, options_);
	});

	
})(jQuery);
(function($){

	PB.user = $.extend(PB.user, {
		send_authcode_for_email : function(email_, callback_){
			callback_ = callback_ || $.noop;

			PB.post({
				action : "user-send-authcode-for-email",
				email : email_,
			}, $.proxy(function(result_, response_json_){
				this((result_ && response_json_.success), response_json_);
			}, callback_), true);
		},
		check_authcode_for_email : function(email_, authcode_, callback_){
			callback_ = callback_ || $.noop;

			PB.post({
				action : "user-check-authcode-for-email",
				email : email_,
				authcode : authcode_,
			}, $.proxy(function(result_, response_json_){
				this((result_ && response_json_.success), response_json_);
			},callback_), true);
		}
	});
	
})(jQuery);
(function($){

	PB.movie = {

		open_xticket_popup : function(schedule_id_, cinema_id_){ //비회원 티케팅 팝업

			if(PBVAR['is_user_logged_in'] === "Y"){
				window.open(PB.make_url(PB.append_url(PBVAR['xticketing_url'], schedule_id_),{
					cinema_id : cinema_id_
				}), null, "width=910,height=623");
				return;
			}

			PB.post({
				action : "movie-load-xticketing-popup",
				schedule_id : schedule_id_,
				cinema_id : cinema_id_
			}, function(result_, response_json_){

				if(!result_ || response_json_.success !== true){
					PB.alert_error({
						title : "에러발생",
						content : "예매정보팝업을 불러오는 중 에러가 발생했습니다.",
					});
					return;
				}

				$("#pb-xticketing-modal").remove();
				$("body").append(response_json_.popup_html);

				var popup_ = $("#pb-xticketing-modal");
				var popup_form_ = popup_.find("#pb-xticketing-modal-form");

/*				popup_form_.find("#pb-xticket-popup-form-birth_yyyymmdd-wrap").datetimepicker({
					viewMode: 'years',
					locale: 'ko',
					allowInputToggle : true,
					format: 'YYYY-MM-DD'
				});
*/
				popup_form_.validator();

				popup_form_.submit_handler(function(){
					var window_ = window.open('about:blank','xtikcet-popup','width=910,height=623');
					popup_form_[0].target ="xtikcet-popup";
					popup_form_[0].submit();

					popup_.modal("hide");			
				});

				popup_.modal("show");

			}, true);
		},

		open_xticket_check_popup : function(cinema_id_){ //비회원 티케팅 확인 팝업
			if(PBVAR['is_user_logged_in'] === "Y"){
				window.open(PB.make_url(PBVAR['xticketing_check_url'],{
					cinema_id : cinema_id_
				}), null, "width=910,height=623");
				return;
			}


			PB.post({
				action : "movie-load-xticketing-check-popup",
				cinema_id : cinema_id_
			}, function(result_, response_json_){

				if(!result_ || response_json_.success !== true){
					PB.alert_error({
						title : "에러발생",
						content : "예매정보팝업을 불러오는 중 에러가 발생했습니다.",
					});
					return;
				}

				$("#pb-xticketing-check-modal").remove();
				$("body").append(response_json_.popup_html);

				var popup_ = $("#pb-xticketing-check-modal");
				var popup_form_ = popup_.find("#pb-xticketing-check-modal-form");

				popup_form_.validator();

				popup_form_.submit_handler(function(){
					var window_ = window.open('about:blank','xtikcet-popup','width=910,height=623');
					popup_form_[0].target ="xtikcet-popup";
					popup_form_[0].submit();

					popup_.modal("hide");			
				});

				popup_.modal("show");

			}, true);
		},
		open_dtryx_check_popup : function(cinema_id_) { // 디트릭스 예매확인 

			PB.post({
				action : "movie-dtryx-reservation-popup"
			}, function(result_, response_json_){

				if(!result_ || response_json_.success !== true){
					PB.alert_error({
						title : "에러발생",
						content : "예매정보팝업을 불러오는 중 에러가 발생했습니다.",
					});
					return;
				}  

				if(response_json_.cinema_type !== 'dtryx'){
					PB.movie.open_xticket_check_popup(cinema_id_);
				}
				else {
					$("#dtryx-check-popup").remove();
					$("#popRegR").remove();
					$("body").append(response_json_.popup_html);
					DTRYXBookingRsv();
				}
			}, true);
		}
	}

	jQuery(document).ready(function(){
		var xticket_check_btns_ = $("[data-xticket-check]");

		if(xticket_check_btns_.length > 0){
			xticket_check_btns_.each(function(){
				$(this).click(function(){
					var target_btn_ = $(this);
					//PB.movie.open_xticket_check_popup(target_btn_.attr("data-xticket-check"));
					PB.movie.open_dtryx_check_popup(target_btn_.attr("data-xticket-check"));
					return false;
				});
				
			});
		}
	});

})(jQuery);
(function($){

	window.$ = jQuery;

	PB = $.extend(PB, {
		modal : function(options_, callback_){
			options_ = $.extend({
				show : true,
				classes : "pb-common-popup",
				closebtn : true,
				escbtn : true,
				backdrop : 'static',
				callback_input : false,
				button1Classes : "btn btn-primary",
				button2Classes : "btn btn-default",
				button3Classes : "btn btn-default",
				appendTo : null
			},options_);

			var unique_id_ = this.random_string(10,"abcdefg1234567890");
			var has_title_ = (options_.title !== undefined && options_.title !== null);
			var has_icon_ = (options_.icon !== undefined && options_.icon !== null);

			options_._unique_id = unique_id_;

			var html_ = '<div class="modal fade '+(options_.classes)+' '+(has_title_ ? '' : 'notitle')+'" id="pb-common-modal-'+unique_id_+'" tabindex="-1" role="dialog" aria-labelledby="pb-common-modal-label" data-unique-id="'+unique_id_+'">' +
				'<div class="modal-dialog" role="document">' +
					'<div class="modal-content">';

			if(options_.closebtn){
				html_ +=	'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="fa fa-times"></i></button>';
			}

			if(has_title_ && !has_icon_){
				html_ += '<div class="modal-header">';
				html_ +=	'<h4 class="modal-title" id="pb-common-modal-label">'+options_.title+'</h4>';			
				html_ += '</div>';
			}
				
			html_ += '<div class="modal-body">';

			if(has_icon_){
				html_ += '<div class="icon-wrap">';
				html_ += 	'<i class="icon '+options_.icon+'"></i>';
				html_ += '</div>';

				if(has_title_){
					html_ += '<div class="title-wrap">';
					html_ +=	'<h4 class="modal-title" id="pb-common-modal-label">'+options_.title+'</h4>';
					html_ += '</div>';
				}
			}

			html_ += '<div class="content-wrap '+(has_icon_ ? "has-icon-title" : "")+'">';
			if(options_.content) html_ += options_.content;
			html_ += '</div>';

			html_ += '</div>';

			if(!options_.button1Classes.indexOf("btn-")){
				options_.button1Classes += " btn-primary ";
			}

			if(!options_.button2Classes.indexOf("btn-")){
				options_.button2Classes += " btn-default ";
			}
			
			if(options_.button1 !== undefined && options_.button1 !== null && options_.button1 !== ""){
				
				html_ +=	'<div class="modal-footer '+(has_icon_ ? "has-icon-title" : "")+'">';

				var _temp_func_make_button2 = function(){
					if(options_.button2 !== undefined && options_.button2 !== null && options_.button2 !== "")
						return '<button type="button" class="'+(has_icon_ ? "btn-block" : "")+' button2 '+options_.button2Classes+' ">'+options_.button2+'</button>';
					else return "";
				}

				var _temp_func_make_button3 = function(){
					if(options_.button3 !== undefined && options_.button3 !== null && options_.button3 !== "")
						return '<button type="button" class="'+(has_icon_ ? "btn-block" : "")+' button3 '+options_.button3Classes+' ">'+options_.button3+'</button>';
					else return "";
				}

				if(!has_icon_){
					html_ += _temp_func_make_button3();
					html_ += _temp_func_make_button2();
				}
				
				html_ += '<button type="button" class="'+(has_icon_ ? "btn-block" : "")+' button1 '+options_.button1Classes+' ">'+options_.button1+'</button>';

				if(has_icon_){
					html_ += _temp_func_make_button2();
					html_ += _temp_func_make_button3();
				}
				html_ += '</div>';
			}

			html_ += '</div>' +
				'</div>' +
			'</div>';


			var modal_ = $(html_);
			$("body").append(modal_);

			modal_.modal({
				keyboard : options_.escbtn,
				backdrop : options_.backdrop,
				show : options_.show
			});

			if(options_.show && options_.appendTo !== null){
				var appendToElement_ = $(options_.appendTo);
				if(appendToElement_.length > 0){
					$("body").removeClass("modal-open");
					$('.modal-backdrop').appendTo(appendToElement_); 
					modal_.appendTo(appendToElement_); 
					modal_.addClass('appendto-element');
					$('.modal-backdrop').addClass("appendto-element");
					$('.modal-backdrop').off('focusin.bs.modal');

					$('.modal-backdrop').one('bsTransitionEnd', function(){
						$(document).off('focusin.bs.modal');
					});
					
				}
			}
			
			// modal_.modal("show");

			var button1_ = modal_.find(".modal-footer .button1");
			var button2_ = modal_.find(".modal-footer .button2");
			var button3_ = modal_.find(".modal-footer .button3");

			button1_.data("pb-modal-options", options_);
			button1_.data("pb-modal-callback", callback_);
			button1_.click(function(){
				var options_ = $(this).data("pb-modal-options");
				var modal_ = $("#pb-common-modal-"+options_._unique_id);
				var callback_ = $(this).data("pb-modal-callback");

				var inputs_ = {};
				if(options_.callback_input){
					var input_els_ = modal_.find('.modal-body :input');

					if(input_els_.length > 0){
						input_els_.each(function(){
							inputs_[$(this).attr('name')] = $(this).val();
						});
					}
				}

				modal_.modal("hide");
				if(callback_ !== undefined) callback_(true, inputs_);
			});

			if(button2_.length > 0){
				button2_.data("pb-modal-options", options_);
				button2_.data("pb-modal-callback", callback_);
				button2_.click(function(){
					var options_ = $(this).data("pb-modal-options");
					var modal_ = $("#pb-common-modal-"+options_._unique_id);
					var callback_ = $(this).data("pb-modal-callback");

					modal_.modal("hide");
					if(callback_ !== undefined) callback_(false, 1);
				});
			}

			if(button3_.length > 0){
				button3_.data("pb-modal-options", options_);
				button3_.data("pb-modal-callback", callback_);
				button3_.click(function(){
					var options_ = $(this).data("pb-modal-options");
					var modal_ = $("#pb-common-modal-"+options_._unique_id);
					var callback_ = $(this).data("pb-modal-callback");

					modal_.modal("hide");
					if(callback_ !== undefined) callback_(false, 2);
				});
			}

			modal_.on("hidden.bs.modal", function(){
				$(this).remove();
			});

			return modal_;
		},confirm : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : "확인",
				button2 : "취소"
			},options_), callback_);
		},confirm_success : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-check text-success"
			}, options_), callback_);
		},confirm_info : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa fa-info-circle text-info"
			}, options_), callback_);
		},confirm_warning : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-exclamation text-warning"
			}, options_), callback_);
		},confirm_q : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-question text-primary"
			}, options_), callback_);
		},
		confirm_error : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-meh-o text-danger"
			}, options_), callback_);
		},alert : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : "확인"
			},options_), callback_);
		},alert_success : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-check text-success"
			}, options_), callback_);
		},alert_info : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa fa-info-circle text-info"
			}, options_), callback_);
		},alert_warning : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-exclamation text-warning"
			}, options_), callback_);
		},alert_q : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-question text-primary"
			}, options_), callback_);
		},
		alert_error : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-meh-o text-danger"
			}, options_), callback_);
		}

	});

	$.fn.scrollFocus = (function(animate_){
		animate_ = (animate_ === undefined ? 200 : 0);
		var offset_ = this.offset();
		$("body").animate({scrollTop : offset_['top']}, animate_);
	});

	$(function(){
		var responsive_var_element_ = $("body").find(".pb-responsive-var");
		var xs_element_ = responsive_var_element_.children(".xs");
		var sm_element_ = responsive_var_element_.children(".sm");
		var md_element_ = responsive_var_element_.children(".md");
		var lg_element_ = responsive_var_element_.children(".lg");

		PB.responsive_info = {
			xs_min : parseInt(xs_element_.css('minWidth')),
			xs_max : parseInt(xs_element_.css('maxWidth')),
			
			sm_min : parseInt(sm_element_.css('minWidth')),
			sm_max : parseInt(sm_element_.css('maxWidth')),

			md_min : parseInt(md_element_.css('minWidth')),
			md_max : parseInt(md_element_.css('maxWidth')),

			lg_min : parseInt(lg_element_.css('minWidth'))
		}
	});



	$(document).ready(function(){
		var window_el_ = $(window);
		var pb_header_ = $("#pb-header");

		window_el_.scroll(function(event_){
			pb_header_.toggleClass('scroll-fixed', (window_el_.scrollTop() >= 200));
		});
		AOS.init();

		pb_header_.on("show.bs.collapse", function(){
			$(this).toggleClass("navbar-toggled", true);
		}).on( "hide.bs.collapse", function(){
			$(this).toggleClass("navbar-toggled", false);
		});

		function _pb_hide_nav_submenu(sub_menu_el_){
			sub_menu_el_.toggleClass("out", true);

			sub_menu_el_.stop(true);
			sub_menu_el_.delay(500).queue(function(){
				$(this).toggle(false);
			});
		}

		var dropdown_li_list_ = pb_header_.find("#pb-header-mainmenu-item > .menu-item.menu-item-has-children");
			dropdown_li_list_.each(function(){
				var dropdown_li_ = $(this);
				dropdown_li_.mouseenter(function(event_){

					var window_width_ = window_el_.width();
					if(window_width_ < PB.responsive_info.xs_max){
						return;
					}

					var parent_li_ = $(event_.currentTarget);
					var sub_menu_el_ = parent_li_.find("ul.sub-menu");
					var other_sub_menu_ = pb_header_.find(".menu-item.menu-item-has-children ul.sub-menu");

					$.each(other_sub_menu_, function(){
						var other_sub_menu_item_ = $(this);
						if(!other_sub_menu_item_.is(sub_menu_el_)){
							_pb_hide_nav_submenu(other_sub_menu_item_);	
						}
					});
					
					sub_menu_el_.toggle(true);
					sub_menu_el_.stop(true);
					sub_menu_el_.toggleClass("dropdown-active", true);
					sub_menu_el_.toggleClass("out", false);
					
					// sub_menu_el_.css({left : 0});
					var offset_ = parent_li_.offset();
					var max_x_ = offset_.left + sub_menu_el_.outerWidth();
					var window_width_ = $(window).width() - 10;

					if(window_width_ < max_x_){
						var fix_val_ = -(max_x_ - window_width_);
						sub_menu_el_.css({left : fix_val_+"px"})
					}else{
						sub_menu_el_.css({left : "0px"})
					}

					clearTimeout(parent_li_.data("menu-dropdown-timeout"));
				});
				dropdown_li_.mouseout(function(event_){

					var window_width_ = window_el_.width();
					if(window_width_ < PB.responsive_info.xs_max){
						return;
					}

					var timeout_ = $(this).data("menu-dropdown-timeout");
					clearTimeout(timeout_);

					timeout_ = setTimeout($.proxy(function(){
						_pb_hide_nav_submenu($(this).find("ul.sub-menu"));
					}, $(this)), 700);
					$(this).data("menu-dropdown-timeout", timeout_);

				});
			});

		window_el_.resize(function(){
			var window_width_ = window_el_.width();

			if(window_width_ < PB.responsive_info.xs_max){
				var all_submenu_ = pb_header_.find(".menu-item.menu-item-has-children ul.sub-menu");
					all_submenu_.stop(true);
					all_submenu_.toggle(true);
				all_submenu_.css({left : null});
			}
		});
	});


	$(document).on('hidden.bs.modal', '.modal', function () {
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});

	$(document).on('show.bs.modal', '.modal', function () {
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() {
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});

	$.fn.form_working = (function(bool_, add_objects_){
		var targets_ = this.find("[data-form-field]");
		if(add_objects_ !== undefined){
			targets_ = $.merge(targets_, add_objects_);
		}

		this.form_msg(false);
		targets_.each(function(){
			var target_ = $(this);
			if(target_.data("loading-text") !== undefined && target_.data("loading-text") !== null){
				if(bool_){
					target_.button("loading");
				}else target_.button("reset");
			}else{
				target_.toggleClass("disabled", bool_);
				target_.prop("disabled", bool_);
			}
		});
	});

	// $.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="hidden"], [type="submit"], [type="reset"], button,)'
	$.fn.validator.Constructor.VALIDATORS = $.extend($.fn.validator.Constructor.VALIDATORS, {
		"number" : function($el_){
			if(!(/^[0-9]+$/.test($el_.val()))){
				return "error number"
			}
		},
		"maxlength" : function($el_){
			var maxlength = $el_.data('maxlength')
			return $el_.val().length > maxlength && "error maxlength";
		},
		"alphanum" : function($el_){
			if(!(/^[a-zA-Z][a-zA-Z0-9]+$/.test($el_.val()))){
				return "error alphanum"
			}
		},"password" : function($el_){
			if(!(/^[A-Za-z0-9!_#@$^*&]{8,}$/.test($el_.val()))){
				return "error password";
			}
		},
		required: function ($el) { return !!$.trim($el.val()) }
	});

	$.fn.submit_handler = (function(success_, fail_){
		this.data('form-callback-success', success_) || $.fn.noop;
		this.data('form-callback-fail', fail_) || $.fn.noop;

		this.validator().on('submit', function(event_){
			var target_ = $(this);
			var callback_success_ = target_.data('form-callback-success')  || $.noop;
			var callback_fail_ = target_.data('form-callback-fail') || $.noop;

			if(event_.isDefaultPrevented()){
				callback_fail_.apply(this, [target_]);
				return;
			}

			return callback_success_.apply(this, [target_]) || false;
		});

		
	});

	

	function getOsCode() {
		var uagent = navigator.userAgent.toLocaleLowerCase();
		if (uagent.search("android") > -1 && '0.0.0' <= '4.0.3') {
			return "android";
		} else if (uagent.search("iphone") > -1 || uagent.search("ipod") > -1 || uagent.search("ipad") > -1) {
			return "ios";
		} else {
			return "etc";
		}
	}
	var osCode = getOsCode();

	function openWithApp(osCode, scheme, customUrl, packageName, appStoreUrl) {
		var intentUrl = "intent://" + customUrl + "#Intent;scheme=" + scheme + ";package=" + packageName + ";end";
		var customScheme = scheme + "://" + customUrl;
		if (osCode == "android") {
			window.location = intentUrl;
			return true;
		} else if (osCode == "ios") {
			var clickedAt = +new Date;
			var talkInstCheckTimer = setTimeout(function() {
				if (+new Date - clickedAt < 2000) {
					window.location = appStoreUrl;
				}
			}, 1500);
			window.location = customScheme;
			return true;
		} else {
			return false;
		}
	}
		

	jQuery(document).ready(function($){
		$("[data-kakao-talk-btn]").click(function(event_) {

			var scheme_ = $(this).attr("data-kakao-talk-btn");
			var customUrl = "plusfriend/friend/"+scheme_;
			var appStoreUrl = "https://itunes.apple.com/kr/app/kakaotog-kakaotalk/id362057947";
			var result_ = openWithApp(osCode, "kakaoplus", customUrl, "com.kakao.talk", appStoreUrl);
			if(result_){
				event_.preventDefault();
				return;
			}

		});

		$(".form-control.readonly").keydown(function(event_){
			return false;
		});
	});


	return PB;

})(jQuery);