(function($){
	
	var PB = window.PB = {
		_admin_ajaxurl : PBVAR["admin_ajaxurl"],
		_ajaxurl : PBVAR["ajaxurl"],
		admin_ajaxurl : function(params_){
			if(params_ === undefined){
				return this._admin_ajaxurl;
			}

			return this.make_url(this._admin_ajaxurl, params_);
		},
		ajaxurl : function(params_){
			if(params_ === undefined){
				return this._ajaxurl;
			}

			return this.make_url(this._ajaxurl, params_);
		},
		nl2br : function(str_){
			return str_.replace(/\n/g, "<br />");  
		},
		make_currency : function(value_){
			$result_ = String(Math.abs(value_)).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');

			if(value_ < 0){
				$result_ = "-"+$result_;
			}

			return $result_;
		},make_k_number_format : function(value_){
			  return (value_ > 999 ? (value_/1000).toFixed(1) + 'k' : value_);
		},
		random_string : function(length_, possible_){
			var text_ = "";
			var possible_ = (possible_ !== undefined ? possible_ : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

			for(var i=0; i < length_; i++)
				text_ += possible_.charAt(Math.floor(Math.random() * possible_.length));

			return text_;
		},
		lpad : function(str_, char_, length_){
			if(! str_|| !char_|| str_.length >= length_) {
				return str_;
			}

			var max_ = (length_ - str_.length) / char_.length;

			for(var index_=0;index_<max_; ++index_){
				str_ = char_ + str_;
			}

			return str_;
		},
		rpad : function(str_, char_, length_){
			if(! str_|| !char_|| str_.length >= length_) {
				return str_;
			}

			var max_ = (length_ - str_.length) / char_.length;

			for(var index_=0;index_<max_; ++index_){
				str_ += char_;
			}

			return str_;
		},
		make_options : function(array_, value_column_, title_column_){
			var html_ = '';
			for(var index_=0;index_<array_.length; ++index_){
				html_ += '<option value="'+array_[index_][value_column_]+'">'+array_[index_][title_column_]+'</option>';
			}

			return html_;
		},
		json_equals : function(a_, b_, sort_array_){
			var sort_func_ = function(object_){
				if(sort_array_ === true && Array.isArray(object_)) {
					return object_.sort();
				}else if(typeof object_ !== "object" || object_ === null) {
					return object_;
				}

				return Object.keys(object_).sort().map(function(key_) {
					return {
						key: key_,
						value: sort_func_(object_[key_])
					};
				});
			};
			
			return JSON.stringify(sort_func_(a_)) === JSON.stringify(sort_func_(b_));
		},
		_data_filters : {},
		add_data_filter : function(key_, func_){
			if(this._data_filters[key_] === undefined || this._data_filters[key_] === null){
				this._data_filters[key_] = [];
			}
			this._data_filters[key_].push(func_);
		},
		apply_data_filters : function(key_, params_, add_){
			if(this._data_filters[key_] !== undefined && this._data_filters[key_] !== null){
				var filter_count_ = this._data_filters[key_].length;
				for(var index_=0;index_<filter_count_;++index_){
					params_ = this._data_filters[key_][index_](params_, add_);	
				}
			}
			return params_;
		},
		_data_actions : {},
		add_data_action : function(key_, func_){
			if(this._data_actions[key_] === undefined || this._data_actions[key_] === null){
				this._data_actions[key_] = [];
			}
			this._data_actions[key_].push(func_);
		},
		do_data_action : function(key_, params_){
			if(this._data_actions[key_] !== undefined && this._data_actions[key_] !== null){
				var filter_count_ = this._data_actions[key_].length;
				for(var index_=0;index_<filter_count_;++index_){
					this._data_actions[key_][index_](params_);	
				}
			}
		},
		_post : function(data_, callback_, options_){
			options_ = $.extend({
				type: "POST",
				url: this._ajaxurl,
				data : $.extend(true,{},data_),
				success : function(response_text_){
					var result_ = true;
					try{
						response_text_ = JSON.parse(response_text_);
					}catch(ex_){
						result_ = false;
					}
					callback_(result_,response_text_, response_text_);
				},error : function(xhr_, status_, thrown_){
					if(status_ === "abort") return;
					callback_(false, status_, thrown_);
				}
			},options_);
			
			return $.ajax(options_);
		},
		post : function(data_,callback_,block_, options_){
			if(block_ !== undefined && block_ === true){
				this.indicator(true, function(){
					PB._post.apply(PB, [data_, function(result_, response_text_, cause_){
						PB.indicator(false);
						callback_(result_, response_text_, cause_);
					}, options_]);
				});
			}else{
				PB._post.apply(PB,[data_, callback_]);
			}
		},
		make_url : function(url_, parameters_){
			parameters_ = $.extend({},parameters_);

			var concat_char_ = "?";
			if(url_.indexOf(concat_char_) >= 0)
				concat_char_ = "&";
			return url_+concat_char_+$.param(parameters_);
		},append_url : function(url_, append_path_){
				

			if(url_.lastIndexOf("/") != (url_.length -1)){
				url_ += "/";
			}

			return (url_ + append_path_);
		},modal : function(options_, callback_){
			console.error("please override me!");
			return;
		},confirm : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : PBVAR['OK'],
				button2 : PBVAR['cancel']
			},options_), callback_);
		},alert : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : PBVAR['OK']
			},options_), callback_);
		},forward : function(url_, params_, target_){
			var temp_form_ = document.createElement('form');

			for(var key_ in params_){
				var value_ = params_[key_];
				var param_element_ = document.createElement('input');
					param_element_.setAttribute('type', 'hidden');
					param_element_.setAttribute('name', key_);
					param_element_.setAttribute('value', value_);

				temp_form_.appendChild(param_element_);
			}

			if(target_){
				temp_form_.setAttribute('target', target_);
			}
			
			temp_form_.setAttribute('method', 'post');
			temp_form_.setAttribute('action', url_);

			document.body.appendChild(temp_form_);
			temp_form_.submit();
		},redirect : function(url_, params_){
			document.location = PB.make_url(url_, params_);
		},
		refresh : function(){
			var methods = [
				"location.reload()",
				"history.go(0)",
				"location.href = location.href",
				"location.href = location.pathname",
				"location.replace(location.pathname)",
				"location.reload(false)"
			];

			var $body = $("body");
			for(var i = 0; i < methods.length; ++i) {
				(function(cMethod) {
					eval(cMethod);
				})(methods[i]);
			}
		},url_parameter : function(name_){
			return decodeURIComponent((new RegExp('[?|&]' + name_ + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
		},url_path : function(){

			var t_result_ = location.pathname.split("/")
			var result_ = [];

			for(var index_=0;index_<t_result_.length; ++index_){
				if(t_result_[index_] !== ""){
					result_.push(t_result_[index_]);
				}
			}

			return result_;
		},
		export_youtube_id : function(url_){
			var video_id_ = url_.split('v=')[1];
			if(video_id_ === undefined){
				return null;
			}

			var ampersand_position_ = video_id_.indexOf('&');
			if(ampersand_position_ != -1) {
				video_id_ = video_id_.substring(0, ampersand_position_);
			}
			return video_id_;
		},
		week_of_month : function(date) {
			var day = date.getDate()
            day += (date.getDay() == 0 ? 0 : 7 - date.getDay());
            return Math.ceil(parseFloat(day) / 7);
		},
		geolocation : function(callback_){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(function(position_){
					var results_ = {
						"lat" : position_.coords.latitude,
						"lng" : position_.coords.longitude
					};

					callback_(results_);

				},function(){
					callback_(false, "failed");
				});
			}else{
				callback_(false, "not_supported");
			}
		},
		indicator_message : function(){
			return "<div class='pb-indicator-frame'><div class='uil-ring-css'><div></div></div></div>";
		},
		indicator : function(bool_, callback_){
			$("body").toggleClass('block-ui-in', bool_);
			if(bool_){
				$.blockUI({
					message : PB.indicator_message(),
					onBlock : callback_
				});
			}else{
				$.unblockUI({
					onUnblock : callback_
				});
			}
		},
		click_position : function(base_element_, event_, offset_){
			offset_ = $.extend({x : 0, y: 0}, offset_);

			var pageX_ = null,pageY_ = null;

			var touch_event_ = event_.originalEvent.changedTouches;

			if(touch_event_ && touch_event_.length > 0){
				pageX_ = touch_event_[0].pageX;
				pageY_ = touch_event_[0].pageY;
			}else{
				touch_event_ = event_.originalEvent.touches;

				if(touch_event_ && touch_event_.length > 0){
					pageX_ = touch_event_[0].pageX;
					pageY_ = touch_event_[0].pageY;
				}
			}

			if(!pageX_ || pageX_ === null){
				pageX_ = event_.pageX;
				pageY_ = event_.pageY;
			}

			var posX = base_element_.offset().left,posY = base_element_.offset().top;
			var x_ = (pageX_ - posX) + offset_.x;
			var y_ = (pageY_ - posY) + offset_.y;

			var width_ = base_element_.outerWidth();
			var height_ = base_element_.outerHeight();

			return {
				"x" : x_, "y" : y_,
				"xrate" : (x_ / width_), "yrate" : (y_ / height_)};
		},classic_popup : function (url_, options_) {

			options_ = $.extend({
				'width' : 500,
				'height' : 400,
				'resize' : false,
				'title' : "untitled"
			},options_);

			var strResize_ = (options_['resize'] ? 'yes' : 'no');

			var strParam = 'width=' + options_['width'] + ',height=' + options_['height'] + ',resizable=' + strResize_,
				objWindow = window.open(url_, options_['title'], strParam);

			objWindow.focus();

			return objWindow;
		},unix_to_date : function(unix_, format_){
			var moment_ = moment.unix(unix_);
			return moment_.format(format_);
		}
	}

	$.fn.smooth_inout = (function(toggle_, options_){

		if(this.length > 1){
			this.each(function(){
				$(this).smooth_inout(toggle_, options_);
			});
			return;
		}

		options_ = $.extend({
			in_class : 'in',
			out_class : 'out',
			animating_class : 'animating',
			end_class : 'end',
			duration : 300,
			callback : $.noop
		},options_);

		var inclass_ = options_.in_class;
		var outclass_ = options_.out_class;
		var animating_class_ = options_.animating_class;
		var end_class_ = options_.end_class;
		var animate_duration_ = options_.duration;
		var callback_ = options_.callback;

		if(toggle_ === undefined || toggle_ === null){
			toggle_ = !(this.hasClass(inclass_) && !this.hasClass(outclass_));
		}

		/*if(this.hasClass(animating_class_)){
			return !toggle_;
		}*/

		clearTimeout(this.data('pb_smooth_inout_timeout'));

		this.toggleClass(animating_class_, true);
		this.toggleClass(end_class_, false);

		if(toggle_){

			if(this.hasClass(inclass_)){
				this.toggleClass(outclass_, false);
			}else{
				this.toggleClass(inclass_, true);
			}

		}else{

			if(this.hasClass(inclass_)){
				this.toggleClass(outclass_, true);
			}else{
				this.toggleClass(inclass_, true);
				this.toggleClass(outclass_, true);
			}
		}

		var timeout_obj_ = setTimeout($.proxy(function(){
			this.target.toggleClass(this.animating_class, false);
			this.target.toggleClass(this.end_class, true);
			this.callback(this.toggle);
		},{
			target : this,
			toggle : toggle_,
			animating_class : animating_class_,
			end_class : end_class_,
			callback : callback_
		}), animate_duration_);

		this.data('pb_smooth_inout_timeout', timeout_obj_);

		return toggle_;
	});

	window.pb_add_filter = (function(key_, func_){
		return PB.add_data_filter(key_, func_);
	});
	window.pb_apply_filters = (function(key_, params_, add_){
		return PB.apply_data_filters(key_, params_, add_);
	});
	window.add_data_action = (function(key_, func_){
		return PB.add_data_action(key_, func_);
	});
	window.do_data_action = (function(key_, params_){
		return PB.do_data_action(key_, params_);
	});

	return PB;

})(jQuery);
(function($){

	window.PB.crypt = {
		_public_key_ : (PBVAR["crypt-public-key-n"]+"|"+PBVAR["crypt-public-key-e"]),
		//암호화하기
	    encrypt : function(text_){
	      var result_ = cryptico.encrypt(text_, this._public_key_);
	      if(result_.status === "success"){
	        return result_.cipher;
	      }else return false;
	    }
	}

	//param 필터 등록
	PB.add_data_filter("encrypt", function(params_, add_){
		if(add_ === undefined || add_ === null) return params_;
		var add_count_ = add_.length;
		for(var index_=0;index_<add_count_;++index_){
			var param_name_ = add_[index_];
			params_[param_name_] = PB.crypt.encrypt(params_[param_name_]);
		}

		return params_;
	});

	return PB.crypt;

})(jQuery);
(function($){

	var NON_CHAR_KEYCODE = window.NON_CHAR_KEYCODE = {
		 BACKSPACE : 8
		,TAB : 9
		,ENTER : 13
		,SHIFT : 16
		,CTRL : 17
		,ALT : 18
		,PAUSE : 19
		,CAPSLOCK : 20
		,ESCAPE : 27
		,PAGEUP : 33
		,PAGEDOWN : 34
		,END : 35
		,HOME : 36
		,LEFT : 37
		,UP : 38
		,RIGHT : 39
		,DOWN : 40
		,INSERT : 45
		,DELETE : 46
	}

	window.is_esc_key = (function(key_code_a_){
		return (NON_CHAR_KEYCODE.ESCAPE === key_code_a_);
	});

	window.is_enter_key = (function(key_code_a_){
		return (NON_CHAR_KEYCODE.ENTER === key_code_a_);
	});

	window.is_whitespace_key = (function(key_code_a_){
		return (NON_CHAR_KEYCODE.ENTER === key_code_a_ || NON_CHAR_KEYCODE.TAB === key_code_a_);
	});

	window.is_null = (function(target_){
		return (target_ === undefined || target_ === null || target_ ==="");
	});


	$.fn.serialize_object = (function(map_data_, key_array_convert_){
		key_array_convert_ = (key_array_convert_ === undefined ? false : key_array_convert_);
		var result_ = {};
		var pass_map_data_ = (map_data_ === undefined || map_data_ === null);
		var that_ = this;
		var array_ = this.serializeArray();
		$.each(array_, function(){
			var converted_name_ = this.name;
			var target_input_ = that_.find('[name="'+converted_name_+'"]');

			if(!pass_map_data_){
				if(map_data_[this.name] !== undefined && map_data_[this.name] !== null)
					converted_name_ = map_data_[this.name];
				else return true;
			}
				
			if(key_array_convert_ === true && converted_name_.indexOf("[]") >= 0){
				converted_name_ = converted_name_.replace("[]","");
			}

			if(result_[converted_name_] !== undefined){
				if(!result_[converted_name_].push){
					result_[converted_name_] = [result_[converted_name_]];
				}

				result_[converted_name_].push(PB.apply_data_filters('pb-serialize-object', this.value, target_input_) || '');
			}else result_[converted_name_] = (PB.apply_data_filters('pb-serialize-object', this.value, target_input_) || '');
		});

		return result_;
	});

	$.fn.update_form = (function(data_){

		var inputs_ = this.find(":input");

		$.each(inputs_, function(){
			var input_ = $(this);
			var value_ = data_[input_.attr("name")];

			if(value_ !== undefined){
				input_.val(value_);
			}
		});

	});

	$.fn.sval = (function(value_){
		if(value_ !== undefined){
			var options_ = $(this).children("option");
			options_.each(function(){
				if($(this).val() === value_) $(this).attr("selected",true);
				else $(this).removeAttr("selected");
			});
		}

		return $(this).find("option:selected").val();
	});

	$.fn.checkbox_mass_toggle = (function(){
		var checkboxes_ = this.data("checkbox-mass-toggle").split(",");

		this.data("mass-checkboxes", checkboxes_);

		this.on("change", function(){
			var checkboxes_ = $(this).data("mass-checkboxes");			
			var checked_ = $(this).is(":checked");

			for(var index_=0;index_<checkboxes_.length;++index_){
				var target_checkbox_ = $(checkboxes_[index_]);
					target_checkbox_.prop("checked", checked_);
					target_checkbox_.change();
			}
		});


		for(var index_=0;index_<checkboxes_.length;++index_){
			var target_checkbox_ = $(checkboxes_[index_]);
				target_checkbox_.data("root-checkbox", $(this));

			target_checkbox_.change(function(){
				var temp_ = $(this);
				var root_checkbox_ = temp_.data("root-checkbox");
				var tcheckboxes_ = root_checkbox_.data("mass-checkboxes");

				var result_ = true;

				for(var tindex_=0;tindex_<tcheckboxes_.length;++tindex_){

					var tmp_checkbox_ = $(tcheckboxes_[tindex_]);

					$.each(tmp_checkbox_, function(){
						if(!$(this).is(":checked")){
							result_ = false;
							return false;
						}
					});

					if(!result_) break;
				}

				root_checkbox_.prop("checked", result_);

			});
		}
	});

	$.fn.indicator = (function(bool_, options_){
		var indicator_message_ = PB.indicator_message();
		
		this.toggleClass('block-ui-in', bool_);
		if(bool_){
			this.block($.extend({
				message : indicator_message_
			},options_));
		}else{
			this.unblock(options_);
		}
	});

	$.fn.tap_event = (function(callback_){
		return this.on('mousedown', callback_);
	});

	$(document).ready(function(){
		var toggle_buttons_ = $("[data-checkbox-mass-toggle]");
		if(toggle_buttons_.length > 0){
			toggle_buttons_.each(function(){
				$(this).checkbox_mass_toggle();
			});
		}
	});
	
})(jQuery);
(function($){

	var PB_list_table = (function(target_){
		this._target = target_;
		this._target.data("pblisttable", this);
		this._global_id = this._target.data("pb-listtable-id");
		this._table_form = this._target.closest("form");
		this._page_index_el = this._table_form.find("input[name='page_index']");
		this._pagenav = $("[data-pb-listtable-pagenav-id='"+this._global_id+"']");

		this._rander_pagenav();
	});
	PB_list_table.prototype.target = (function(){
		return this._target;
	});
	PB_list_table.prototype.page_index = (function(page_index_){
		if(page_index_ !== undefined){
			this._page_index_el.val(page_index_);
			this.load();
		}

		return this._page_index_el.val();
	});
	PB_list_table.prototype.load = (function(){
		this._table_form.submit();
	});

	PB_list_table.prototype._rander_pagenav = (function(){
		var pagenav_buttons_ = this._pagenav.find("a[data-page-index]");
		var that_ = this;

		$.each(pagenav_buttons_, function(){
			var target_ = $(this);
			target_.data("pb-listtable",that_);
			target_.click(function(event_){
				event_.preventDefault();
				var pagenav_btn_ = $(this);
				var page_index_ = pagenav_btn_.data("page-index");
				var listtable_module_ = pagenav_btn_.data("pb-listtable");

				var current_page_index_ = listtable_module_.page_index();
				listtable_module_.page_index(page_index_);
			});
		});
	});

	$.fn.pblisttable = (function(){
		var target_ = this;

		if(target_.data("pblisttable") !== undefined && target_.data("pblisttable") !== null){
			return target_.data("pblisttable");
		}

		return new PB_list_table(target_);
	});

	var PB_ajax_list_table = (function(target_){
		PB_list_table.apply(this, [target_]);

		this._options = {
			'before_load' : function(){
				return true;
			}
		};

		this._table_form.data("htajaxlisttable", this);
		this._table_form.on("submit", function(){
			var listtable_module_ = $(this).data("htajaxlisttable");
			listtable_module_.load();
			return false;
		});

		var autoload_el_ = this._table_form.find(":input[name='firstload']");
		if(autoload_el_.length <= 0 || autoload_el_.val() !== "N"){
			this.load();
		}
	});

	PB_ajax_list_table.prototype = Object.create(PB_list_table.prototype);

	PB_ajax_list_table.prototype.load = (function(){
		if(!this._options['before_load'].apply(this)) return;

		var table_form_ = this._table_form;
		var table_params_ = table_form_.serialize_object();

		var that_ = this;
		var target_ = this._target;
		var thead_el_ = this._target.children("thead");
		var tbody_el_ = this._target.children("tbody");
		var pagenav_el_ = this._pagenav;

		target_.trigger("pblisttablestartload");
		
		tbody_el_.empty();
		pagenav_el_.empty();

		tbody_el_.append("<tr><td class='ajax-loading' colspan='"+thead_el_.find("tr > th").length+"'><center><i class='sicon sicon-spinner sicon-hc-spin'></i></center></td></tr>");

		PB.post($.extend(table_params_,{
			action : "pb-listtable-load-html",
			global_id : this._global_id
		}),function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				return;
			}

			tbody_el_.empty();
			pagenav_el_.empty();
			tbody_el_.append(response_json_.body_html);
			pagenav_el_.append(response_json_.pagenav_html);
			that_._rander_pagenav();
			target_.trigger("pblisttableload", [response_json_.orgdata, tbody_el_.children()]);
		});
	});

	$.fn.htajaxlisttable = (function(){
		var target_ = this;

		if(target_.data("pblisttable") !== undefined && target_.data("pblisttable") !== null){
			return target_.data("pblisttable");
		}

		return new PB_ajax_list_table(target_);
	});

})(jQuery);

function _pb_list_table_initialize(global_id_, ajax_){
	$ = jQuery;
	if(ajax_ === undefined || ajax_ === null){
		ajax_ = false;
	}

	if(ajax_ === true){
		$("[data-pb-listtable-id='"+global_id_+"']").htajaxlisttable();
	}else{
		$("[data-pb-listtable-id='"+global_id_+"']").pblisttable();
	}
	
}
(function($){

	window.PB.address = {
		

		geolocation : function(callback_){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(function(position_){
					var latlng_ = {
						"lat" : position_.coords.latitude,
						"lng" : position_.coords.longitude
					};

					PB.maps._cache_latlng(latlng_, function(success_, latlng_){
						PB.do_data_action("geolocation", {
							type : "geo",
							latlng : latlng_
						});
						callback_(success_, latlng_);
					});
				},function(){
					callback_(false);
				});
			}else{
				callback_(false);
			}
		},

		_check_maps_api : function(){
			var result_ = (daum !== undefined && daum.maps !== undefined);

			if(!result_) console.error('daum.maps API must included');

			return result_;
		},
		_check_postcode_api : function(){
			var result_ =  (daum !== undefined && daum.Postcode !== undefined);

			if(!result_) console.error('daum.Postcode API must included');

			return result_;
		},

		search_postcode : function(callback_, options_){
			callback_ = callback_ || $.noop;

			if(!this._check_postcode_api()){
				callback_(false);
				 return;
			}

			callback_ = callback_ || $.noop;

			options_ = $.extend({
				opentype : "embed",
				onclose: $.proxy(function(state_){
					if(state_ === 'FORCE_CLOSE'){
						this(false);
					}
				}, callback_),
				oncomplete : $.proxy(function(data_){

					this(true, {
						postcode : data_['zonecode'],
						address : (data_['autoJibunAddress'] || data_['jibunAddress']),
						address_road : (data_['autoRoadAddress'] || data_['roadAddress']),
						si : data_['sido'],
						gu : data_['sigungu'],
						dong : data_['bname'],
					});

				}, callback_)
			},options_);

			var daum_map_ = new daum.Postcode(options_);

			if(options_.opentype === "embed" && options_.element){
				daum_map_.embed(options_.element);
			}else daum_map_.open();			
		}, search_latlng : function(address_, callback_){
			callback_ = callback_ || $.noop;

			if(!this._check_maps_api()){
				callback_(false);
				 return;
			}

			var geocoder_ = new daum.maps.services.Geocoder();

			geocoder_.addr2coord(address_, $.proxy(function(status_, result_){
				if(status_ !== daum.maps.services.Status.OK){
					this(false);
					return;
				}

				this(true, {
					lat : result_.addr[0].lat,
					lng : result_.addr[0].lng
				});

			}, callback_));
		}
	};

})(jQuery);
(function($){

	window.$ = jQuery;

	PB = $.extend(PB,{
		_media_selector_popup : null,
		select_media : function(options_){
			var element_ = options_.selector;

			options_ = $.extend({
				selector : null,
				title : null,
				multiple: false
			},options_);

			if(options_.selector === null){
				if(this._media_selector_popup === null){
					this._media_selector_popup = $("<div data-pb-media-selector-popup>");
					$("body").append(this._media_selector);
				}

				options_.selector = this._media_selector_popup;
			}
				
			var target_ = $(options_.selector);
			var media_frame_ = target_.data("pb-media-frame");

			if(media_frame_){
				media_frame_.open();
				return;
			}

			media_frame_ = wp.media({
				title: options_.title,
				multiple: options_.multiple
			});

			media_frame_.on('select', function(){
				var result_ = [];
				var selection_ = this.models[0].get('selection');
				selection_.map(function(attachment_){
					result_[result_.length] = attachment_.toJSON();
				});
				options_.callback(result_)
			});

			media_frame_.open();
		},modal : function(options_, callback_){
			options_ = $.extend({
				classes : "",
				button_to_close : function(){
					return true;
				},
				callback : function(){}
			},options_);

			var unique_id_ = PB.random_string(5, "abcdef0123456789");

			var html_ = '<div id="pb-common-confirm-popup-'+unique_id_+'" class="pb-mg-popup admin with-button '+options_.classes+'">';
				html_ += '<div class="title-wrap"><h4>'+options_.title+'</h4></div>';
				html_ += '<div class="wrap">';
				html_ += '<div class="content-wrap">';
				
				if(options_.content !== undefined && options_.content !== null){
					html_ += options_.content;	
				}
				
				html_ += '</div>';
				html_ += '</div>';
				if(options_.button1 !== undefined || options_.button2 !== undefined){
					html_ += '<div class="button-area">';
					if(options_.button1 !== undefined) html_ += '<a class="button button-primary yes-btn" href="javascript:;">'+options_.button1+'</a>';
			 		if(options_.button2 !== undefined) html_ += '<a class="button button-secondary no-btn" href="javascript:;">'+options_.button2+'</a>';
		 		}
				html_ += '</div>';
				html_ += '</div>';

			$.magnificPopup.open({
				items: {
					src: $(html_),
					type: 'inline',
					preloader: false
				},
				closeBtnInside : false,
				closeOnBgClick : false,
				showCloseBtn : false,
				enableEscapeKey : false
			});

			var popup_ = $("#pb-common-confirm-popup-"+unique_id_);
				popup_.data('modal-options', options_);
				popup_.data('modal-callback', callback_);

			var yes_btn_ = popup_.find(".button-area .yes-btn");			
			var no_btn_ = popup_.find(".button-area .no-btn");			

			yes_btn_.data('modal-id', unique_id_);
			no_btn_.data('modal-id', unique_id_);

			yes_btn_.click(function(){
				var target_modal_ = $("#pb-common-confirm-popup-"+$(this).data('modal-id'));
				var modal_options_ = target_modal_.data('modal-options');
				if(modal_options_.button_to_close === undefined || modal_options_.button_to_close.apply(target_modal_, [true])){
					target_modal_.magnificPopup("close");	
				}
				if(callback_ !== undefined) callback_.apply(target_modal_, [true]);
			});
			no_btn_.click(function(){
				var target_modal_ = $("#pb-common-confirm-popup-"+$(this).data('modal-id'));
				var modal_options_ = target_modal_.data('modal-options');
				if(modal_options_.button_to_close === undefined || modal_options_.button_to_close.apply(target_modal_, [false])){
					target_modal_.magnificPopup("close");
				}
				if(callback_ !== undefined) callback_.apply(target_modal_, [false]);
			});

			return popup_;
		}
	});
	
	var _media_selector = (function(target_){
		this._selector_id = PB.random_string(5);
		this._target = $(target_);
		this._target.data("pb-media-selector", this);
		this._target.addClass("pb-media-selector pb-list-selector");

		this._max_length = 0;
		var max_length_ = parseInt(this._target.attr('data-max-length'));
		if(!isNaN(max_length_)){
			this._max_length = max_length_;
		}

		this._media_list = $("<div class='media-list item-list'></div>");
		this._target.append(this._media_list);

		if(this._media_list.sortable !== undefined){
			this._media_list.sortable({
				items : '> .media-item',
				handle : '.preview'
			});
		}

		this._select_btn = $("<button class='select-btn'><span class='icon'>+</span></button>");
		this._media_list.append(this._select_btn);
		
		this._select_btn.data("pb-media-selector", this);

		this._select_btn.click(function(event_){
			event_.preventDefault();
			var target_ = $(this);
			var media_selector_ = target_.data("pb-media-selector");
			media_selector_.select();
		});

		var default_img_elements_ = this._target.children("input");
		var default_imgs_ = [];

		if(default_img_elements_.length > 0){
			default_img_elements_.each(function(){
				default_imgs_.push($(this).val());
			});	
		}

		default_img_elements_.remove();
	
		for(var index_=0;index_<default_imgs_.length;++index_){
			this._add_url(default_imgs_[index_]);
		}

		this._check_select_btn();
	});

	_media_selector.prototype._add_url = (function(url_){
		var media_id_ = PB.random_string(10);
		var media_item_ = $("<div class='item media-item pb-img-frame noimg' data-pb-media-id='"+media_id_+"'>");
		media_item_.data("pb-media-url", url_);
		media_item_.css({
			position : "relative"
		});
		var preview_ = $('<div class="preview"></div>');
			preview_.css({
				backgroundImage : "url("+url_+")"
			});

		var delete_btn_ = $('<a class="delete-btn"><span class="icon">&times;</span></a>');

		var last_media_items_ = this._media_list.children('.media-item');

		if(last_media_items_.length > 0){
			last_media_items_.last().after(media_item_);
		}else{
			this._media_list.prepend(media_item_);
		}

		media_item_.append(preview_);
		media_item_.append(delete_btn_);

		delete_btn_.data("pb-media-selector", this);
		delete_btn_.data("pb-media-id", media_id_);
		delete_btn_.click(function(event_){
			event_.preventDefault();
			var delete_btn_ = $(this);
			var media_selector_ = delete_btn_.data("pb-media-selector");
			media_selector_._delete(delete_btn_.data("pb-media-id"));
		});

		media_item_.data("pb-media-selector", this);
		
		var input_name_ = this._target.data("pb-media-selector-input-name");

		if(input_name_ !== undefined && input_name_ !== null && input_name_ !== ""){
			media_item_.append("<input type='hidden' name='"+input_name_+"' value='"+url_+"'>");
		}
		

		this._check_select_btn();

		return media_item_;
	});
	_media_selector.prototype._delete = (function(media_id_){
		this._media_list.children("[data-pb-media-id='"+media_id_+"']").remove();
		this._check_select_btn();
	});
	_media_selector.prototype._delete_all = (function(){
		this._media_list.children("[data-pb-media-id]").remove();
		this._check_select_btn();
	});
	_media_selector.prototype.count = (function(){
		return this._media_list.children('.media-item').length;
	});
	_media_selector.prototype.select = (function(){
		var that_ = this;
		PB.select_media({
			multiple : true,
			callback : function(result_){
				for(var index_=0;index_<result_.length; ++ index_){
					if(!that_._check_select_btn()) break;

					var img_url_ = result_[index_]["url"];
					that_._add_url(img_url_);
				}
			}
		});
	});
	_media_selector.prototype.to_json = (function(){
		var media_items_ = this._media_list.children('.media-item');
		if(media_items_.length <= 0) return [];

		var results_ = [];
		media_items_.each(function(){
			results_.push($(this).data("pb-media-url"));
		});

		return results_;
	});
	_media_selector.prototype._check_select_btn = (function(){
		var bool_ = (this.count() < this._max_length || this._max_length == 0);
		this._select_btn.toggle(bool_);
		return bool_;
	});

	$.fn.pb_media_selector = (function(){
		var target_ = $(this);

		if(target_.data("pb-media-selector") === undefined || target_.data("pb-media-selector") === null){
			return new _media_selector(this);
		}else{
			return target_.data("pb-media-selector");
		}
	});


	var _attach_selector = (function(target_){
		this._selector_id = PB.random_string(5);
		this._target = $(target_);
		this._target.data("pb-attach-selector", this);
		this._target.addClass("pb-attach-selector pb-list-selector");
		this._max_length = 1;

		this._attach_list = $("<div class='attach-list item-list'></div>");
		this._target.append(this._attach_list);

		if(this._attach_list.sortable !== undefined){
			this._attach_list.sortable({
				items : '> .attach-item',
				handle : '.preview'
			});
		}

		this._select_btn = $("<button class='select-btn'><span class='icon'>+</span></button>");
		this._attach_list.append(this._select_btn);
		
		this._select_btn.data("pb-attach-selector", this);

		this._select_btn.click(function(event_){
			event_.preventDefault();
			var target_ = $(this);
			var attach_selector_ = target_.data("pb-attach-selector");
			attach_selector_.select();
		});

		var default_img_elements_ = this._target.children("input");
		var default_imgs_ = [];

		if(default_img_elements_.length > 0){
			default_img_elements_.each(function(){
				default_imgs_.push($(this).val());
			});	
		}

		default_img_elements_.remove();
	
		for(var index_=0;index_<default_imgs_.length;++index_){
			this._add_url(default_imgs_[index_]);
		}

		this._check_select_btn();
	});

	_attach_selector.prototype._add_url = (function(url_){
		var attach_id_ = PB.random_string(10);
		var attach_item_ = $("<div class='item attach-item pb-img-frame noimg' data-pb-attach-id='"+attach_id_+"'>");
		attach_item_.data("pb-attach-url", url_);
		attach_item_.css({
			position : "relative"
		});

		var delete_btn_ = $('<a class="delete-btn"><span class="icon">&times;</span></a>');

		var last_attach_items_ = this._attach_list.children('.attach-item');

		if(last_attach_items_.length > 0){
			last_attach_items_.last().after(attach_item_);
		}else{
			this._attach_list.prepend(attach_item_);
		}

		attach_item_.append(delete_btn_);

		delete_btn_.data("pb-attach-selector", this);
		delete_btn_.data("pb-attach-id", attach_id_);
		delete_btn_.click(function(event_){
			event_.preventDefault();
			var delete_btn_ = $(this);
			var attach_selector_ = delete_btn_.data("pb-attach-selector");
			attach_selector_._delete(delete_btn_.data("pb-attach-id"));
		});

		attach_item_.data("pb-attach-selector", this);
		
		var input_name_ = this._target.data("pb-attach-selector-input-name");

		if(input_name_ !== undefined && input_name_ !== null && input_name_ !== ""){
			attach_item_.append("<input type='hidden' name='"+input_name_+"' value='"+url_+"'>");
		}
		

		this._check_select_btn();

		return attach_item_;
	});
	_attach_selector.prototype._delete = (function(attach_id_){
		this._attach_list.children("[data-pb-attach-id='"+attach_id_+"']").remove();
		this._check_select_btn();
	});
	_attach_selector.prototype._delete_all = (function(){
		this._attach_list.children("[data-pb-attach-id]").remove();
		this._check_select_btn();
	});
	_attach_selector.prototype.count = (function(){
		return this._attach_list.children('.attach-item').length;
	});
	_attach_selector.prototype.select = (function(){
		var that_ = this;
		PB.select_media({
			multiple : false,
			callback : function(result_){
				for(var index_=0;index_<result_.length; ++ index_){
					if(!that_._check_select_btn()) break;

					var img_url_ = result_[index_]["url"];
					that_._add_url(img_url_);
				}
			}
		});
	});
	_attach_selector.prototype.to_url = (function(){
		var attach_items_ = this._attach_list.children('.attach-item');
		if(attach_items_.length <= 0) return null;

		var results_ = [];
		return $(attach_items_[0]).data("pb-attach-url");
		
		return results_;
	});
	_attach_selector.prototype._check_select_btn = (function(){
		var bool_ = (this.count() < this._max_length || this._max_length == 0);
		this._select_btn.toggle(bool_);
		return bool_;
	});

	$.fn.pb_attach_selector = (function(){
		var target_ = $(this);

		if(target_.data("pb-attach-selector") === undefined || target_.data("pb-attach-selector") === null){
			return new _attach_selector(this);
		}else{
			return target_.data("pb-attach-selector");
		}
	});

	$(document).ready(function(){
		var media_selectors_ = $("[data-pb-media-selector]");
		if(media_selectors_.length > 0){
			media_selectors_.each(function(){
				new _media_selector(this);
			});
		}

		var attach_selectors_ = $("[data-pb-attach-selector]");
		if(attach_selectors_.length > 0){
			attach_selectors_.each(function(){
				new _attach_selector(this);
			});
		}
	});

	PB._ajaxurl = PB._admin_ajaxurl;

	window._validateForm = window.validateForm;
	window.validateForm = function(form_){
		var bool1_ = window._validateForm(form_);
		var bool2_ = !$(form_)
			.find('.form-required')
			.filter( function() { return $( 'select:visible', this ).sval() === ''; } )
			.addClass( 'form-invalid' )
			.find( 'select:visible' )
			.change( function() { $( this ).closest( '.form-invalid' ).removeClass( 'form-invalid' ); } )
			.size();
		return (bool1_ && bool2_);
	};

	return PB;

})(jQuery);