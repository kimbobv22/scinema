/* ========================================================================
 * bootstrap-spin - v1.0
 * https://github.com/wpic/bootstrap-spin
 * ========================================================================
 * Copyright 2014 WPIC, Hamed Abdollahpour
 *
 * ========================================================================
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

(function ($) {

    $.fn.bootstrapNumber = function (options) {

        var settings = $.extend({
            upClass: 'default',
            downClass: 'default',
            upIcon: 'fa fa-plus',
            downIcon: 'fa fa-minus',
            center: true
        }, options);

        return this.each(function (e) {
            var self = $(this);

            var min = self.attr('min');
            var max = self.attr('max');

            function setText(n) {
                if ((min && n < min) || (max && n > max)) {
                    return false;
                }

                var before_val_ = self.val();
                self.focus().val(n);

                if(before_val_ != self.val()){
                    self.change();
                }
                return true;
            }

            var group = $("<div class='input-group number-input-group'></div>");

            // group.replaceWith(self);

            self.wrap(group);

            group = self.parent();

           var down = $("<button type='button'><i class='"+settings.downIcon+"'></i></button>").attr('class', 'btn btn-' + settings.downClass).click(function () {
                setText(parseInt(self.val()) - 1);
            });
            var up = $("<button type='button'><i class='"+settings.upIcon+"'></i></button>").attr('class', 'btn btn-' + settings.upClass).click(function () {
                setText(parseInt(self.val()) + 1);
            });


            self.before($("<span class='input-group-btn'></span>").append(down));
            // self.appendTo(self);
            if (self) {
                self.css('text-align', 'center');
            }
            self.after($("<span class='input-group-btn'></span>").append(up));

            // remove spins from original
            self.prop('type', 'text').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					(e.keyCode == 65 && e.ctrlKey === true) ||
					(e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }

                var c = String.fromCharCode(e.which);
                var n = parseInt(self.val() + c);

                //if ((min && n < min) || (max && n > max)) {
                //    e.preventDefault();
                //}
            });

            self.prop('type', 'text').blur(function (e) {
                var c = String.fromCharCode(e.which);
                var n = parseInt(self.val() + c);
                if ((min && n < min)) {
                    setText(min);
                }
                else if (max && n > max) {
                    setText(max);
                }
            });


            // self.wrap(group);
        });
    };
}(jQuery));
