(function($){

	window.$ = jQuery;

	PB = $.extend(PB,{
		_media_selector_popup : null,
		select_media : function(options_){
			var element_ = options_.selector;

			options_ = $.extend({
				selector : null,
				title : null,
				multiple: false
			},options_);

			if(options_.selector === null){
				if(this._media_selector_popup === null){
					this._media_selector_popup = $("<div data-pb-media-selector-popup>");
					$("body").append(this._media_selector);
				}

				options_.selector = this._media_selector_popup;
			}
				
			var target_ = $(options_.selector);
			var media_frame_ = target_.data("pb-media-frame");

			if(media_frame_){
				media_frame_.open();
				return;
			}

			media_frame_ = wp.media({
				title: options_.title,
				multiple: options_.multiple
			});

			media_frame_.on('select', function(){
				var result_ = [];
				var selection_ = this.models[0].get('selection');
				selection_.map(function(attachment_){
					result_[result_.length] = attachment_.toJSON();
				});
				options_.callback(result_)
			});

			media_frame_.open();
		},modal : function(options_, callback_){
			options_ = $.extend({
				classes : "",
				button_to_close : function(){
					return true;
				},
				callback : function(){}
			},options_);

			var unique_id_ = PB.random_string(5, "abcdef0123456789");

			var html_ = '<div id="pb-common-confirm-popup-'+unique_id_+'" class="pb-mg-popup admin with-button '+options_.classes+'">';
				html_ += '<div class="title-wrap"><h4>'+options_.title+'</h4></div>';
				html_ += '<div class="wrap">';
				html_ += '<div class="content-wrap">';
				
				if(options_.content !== undefined && options_.content !== null){
					html_ += options_.content;	
				}
				
				html_ += '</div>';
				html_ += '</div>';
				if(options_.button1 !== undefined || options_.button2 !== undefined){
					html_ += '<div class="button-area">';
					if(options_.button1 !== undefined) html_ += '<a class="button button-primary yes-btn" href="javascript:;">'+options_.button1+'</a>';
			 		if(options_.button2 !== undefined) html_ += '<a class="button button-secondary no-btn" href="javascript:;">'+options_.button2+'</a>';
		 		}
				html_ += '</div>';
				html_ += '</div>';

			$.magnificPopup.open({
				items: {
					src: $(html_),
					type: 'inline',
					preloader: false
				},
				closeBtnInside : false,
				closeOnBgClick : false,
				showCloseBtn : false,
				enableEscapeKey : false
			});

			var popup_ = $("#pb-common-confirm-popup-"+unique_id_);
				popup_.data('modal-options', options_);
				popup_.data('modal-callback', callback_);

			var yes_btn_ = popup_.find(".button-area .yes-btn");			
			var no_btn_ = popup_.find(".button-area .no-btn");			

			yes_btn_.data('modal-id', unique_id_);
			no_btn_.data('modal-id', unique_id_);

			yes_btn_.click(function(){
				var target_modal_ = $("#pb-common-confirm-popup-"+$(this).data('modal-id'));
				var modal_options_ = target_modal_.data('modal-options');
				if(modal_options_.button_to_close === undefined || modal_options_.button_to_close.apply(target_modal_, [true])){
					target_modal_.magnificPopup("close");	
				}
				if(callback_ !== undefined) callback_.apply(target_modal_, [true]);
			});
			no_btn_.click(function(){
				var target_modal_ = $("#pb-common-confirm-popup-"+$(this).data('modal-id'));
				var modal_options_ = target_modal_.data('modal-options');
				if(modal_options_.button_to_close === undefined || modal_options_.button_to_close.apply(target_modal_, [false])){
					target_modal_.magnificPopup("close");
				}
				if(callback_ !== undefined) callback_.apply(target_modal_, [false]);
			});

			return popup_;
		}
	});
	
	var _media_selector = (function(target_){
		this._selector_id = PB.random_string(5);
		this._target = $(target_);
		this._target.data("pb-media-selector", this);
		this._target.addClass("pb-media-selector pb-list-selector");

		this._max_length = 0;
		var max_length_ = parseInt(this._target.attr('data-max-length'));
		if(!isNaN(max_length_)){
			this._max_length = max_length_;
		}

		this._media_list = $("<div class='media-list item-list'></div>");
		this._target.append(this._media_list);

		if(this._media_list.sortable !== undefined){
			this._media_list.sortable({
				items : '> .media-item',
				handle : '.preview'
			});
		}

		this._select_btn = $("<button class='select-btn'><span class='icon'>+</span></button>");
		this._media_list.append(this._select_btn);
		
		this._select_btn.data("pb-media-selector", this);

		this._select_btn.click(function(event_){
			event_.preventDefault();
			var target_ = $(this);
			var media_selector_ = target_.data("pb-media-selector");
			media_selector_.select();
		});

		var default_img_elements_ = this._target.children("input");
		var default_imgs_ = [];

		if(default_img_elements_.length > 0){
			default_img_elements_.each(function(){
				default_imgs_.push($(this).val());
			});	
		}

		default_img_elements_.remove();
	
		for(var index_=0;index_<default_imgs_.length;++index_){
			this._add_url(default_imgs_[index_]);
		}

		this._check_select_btn();
	});

	_media_selector.prototype._add_url = (function(url_){
		var media_id_ = PB.random_string(10);
		var media_item_ = $("<div class='item media-item pb-img-frame noimg' data-pb-media-id='"+media_id_+"'>");
		media_item_.data("pb-media-url", url_);
		media_item_.css({
			position : "relative"
		});
		var preview_ = $('<div class="preview"></div>');
			preview_.css({
				backgroundImage : "url("+url_+")"
			});

		var delete_btn_ = $('<a class="delete-btn"><span class="icon">&times;</span></a>');

		var last_media_items_ = this._media_list.children('.media-item');

		if(last_media_items_.length > 0){
			last_media_items_.last().after(media_item_);
		}else{
			this._media_list.prepend(media_item_);
		}

		media_item_.append(preview_);
		media_item_.append(delete_btn_);

		delete_btn_.data("pb-media-selector", this);
		delete_btn_.data("pb-media-id", media_id_);
		delete_btn_.click(function(event_){
			event_.preventDefault();
			var delete_btn_ = $(this);
			var media_selector_ = delete_btn_.data("pb-media-selector");
			media_selector_._delete(delete_btn_.data("pb-media-id"));
		});

		media_item_.data("pb-media-selector", this);
		
		var input_name_ = this._target.data("pb-media-selector-input-name");

		if(input_name_ !== undefined && input_name_ !== null && input_name_ !== ""){
			media_item_.append("<input type='hidden' name='"+input_name_+"' value='"+url_+"'>");
		}
		

		this._check_select_btn();

		return media_item_;
	});
	_media_selector.prototype._delete = (function(media_id_){
		this._media_list.children("[data-pb-media-id='"+media_id_+"']").remove();
		this._check_select_btn();
	});
	_media_selector.prototype._delete_all = (function(){
		this._media_list.children("[data-pb-media-id]").remove();
		this._check_select_btn();
	});
	_media_selector.prototype.count = (function(){
		return this._media_list.children('.media-item').length;
	});
	_media_selector.prototype.select = (function(){
		var that_ = this;
		PB.select_media({
			multiple : true,
			callback : function(result_){
				for(var index_=0;index_<result_.length; ++ index_){
					if(!that_._check_select_btn()) break;

					var img_url_ = result_[index_]["url"];
					that_._add_url(img_url_);
				}
			}
		});
	});
	_media_selector.prototype.to_json = (function(){
		var media_items_ = this._media_list.children('.media-item');
		if(media_items_.length <= 0) return [];

		var results_ = [];
		media_items_.each(function(){
			results_.push($(this).data("pb-media-url"));
		});

		return results_;
	});
	_media_selector.prototype._check_select_btn = (function(){
		var bool_ = (this.count() < this._max_length || this._max_length == 0);
		this._select_btn.toggle(bool_);
		return bool_;
	});

	$.fn.pb_media_selector = (function(){
		var target_ = $(this);

		if(target_.data("pb-media-selector") === undefined || target_.data("pb-media-selector") === null){
			return new _media_selector(this);
		}else{
			return target_.data("pb-media-selector");
		}
	});


	var _attach_selector = (function(target_){
		this._selector_id = PB.random_string(5);
		this._target = $(target_);
		this._target.data("pb-attach-selector", this);
		this._target.addClass("pb-attach-selector pb-list-selector");
		this._max_length = 1;

		this._attach_list = $("<div class='attach-list item-list'></div>");
		this._target.append(this._attach_list);

		if(this._attach_list.sortable !== undefined){
			this._attach_list.sortable({
				items : '> .attach-item',
				handle : '.preview'
			});
		}

		this._select_btn = $("<button class='select-btn'><span class='icon'>+</span></button>");
		this._attach_list.append(this._select_btn);
		
		this._select_btn.data("pb-attach-selector", this);

		this._select_btn.click(function(event_){
			event_.preventDefault();
			var target_ = $(this);
			var attach_selector_ = target_.data("pb-attach-selector");
			attach_selector_.select();
		});

		var default_img_elements_ = this._target.children("input");
		var default_imgs_ = [];

		if(default_img_elements_.length > 0){
			default_img_elements_.each(function(){
				default_imgs_.push($(this).val());
			});	
		}

		default_img_elements_.remove();
	
		for(var index_=0;index_<default_imgs_.length;++index_){
			this._add_url(default_imgs_[index_]);
		}

		this._check_select_btn();
	});

	_attach_selector.prototype._add_url = (function(url_){
		var attach_id_ = PB.random_string(10);
		var attach_item_ = $("<div class='item attach-item pb-img-frame noimg' data-pb-attach-id='"+attach_id_+"'>");
		attach_item_.data("pb-attach-url", url_);
		attach_item_.css({
			position : "relative"
		});

		var delete_btn_ = $('<a class="delete-btn"><span class="icon">&times;</span></a>');

		var last_attach_items_ = this._attach_list.children('.attach-item');

		if(last_attach_items_.length > 0){
			last_attach_items_.last().after(attach_item_);
		}else{
			this._attach_list.prepend(attach_item_);
		}

		attach_item_.append(delete_btn_);

		delete_btn_.data("pb-attach-selector", this);
		delete_btn_.data("pb-attach-id", attach_id_);
		delete_btn_.click(function(event_){
			event_.preventDefault();
			var delete_btn_ = $(this);
			var attach_selector_ = delete_btn_.data("pb-attach-selector");
			attach_selector_._delete(delete_btn_.data("pb-attach-id"));
		});

		attach_item_.data("pb-attach-selector", this);
		
		var input_name_ = this._target.data("pb-attach-selector-input-name");

		if(input_name_ !== undefined && input_name_ !== null && input_name_ !== ""){
			attach_item_.append("<input type='hidden' name='"+input_name_+"' value='"+url_+"'>");
		}
		

		this._check_select_btn();

		return attach_item_;
	});
	_attach_selector.prototype._delete = (function(attach_id_){
		this._attach_list.children("[data-pb-attach-id='"+attach_id_+"']").remove();
		this._check_select_btn();
	});
	_attach_selector.prototype._delete_all = (function(){
		this._attach_list.children("[data-pb-attach-id]").remove();
		this._check_select_btn();
	});
	_attach_selector.prototype.count = (function(){
		return this._attach_list.children('.attach-item').length;
	});
	_attach_selector.prototype.select = (function(){
		var that_ = this;
		PB.select_media({
			multiple : false,
			callback : function(result_){
				for(var index_=0;index_<result_.length; ++ index_){
					if(!that_._check_select_btn()) break;

					var img_url_ = result_[index_]["url"];
					that_._add_url(img_url_);
				}
			}
		});
	});
	_attach_selector.prototype.to_url = (function(){
		var attach_items_ = this._attach_list.children('.attach-item');
		if(attach_items_.length <= 0) return null;

		var results_ = [];
		return $(attach_items_[0]).data("pb-attach-url");
		
		return results_;
	});
	_attach_selector.prototype._check_select_btn = (function(){
		var bool_ = (this.count() < this._max_length || this._max_length == 0);
		this._select_btn.toggle(bool_);
		return bool_;
	});

	$.fn.pb_attach_selector = (function(){
		var target_ = $(this);

		if(target_.data("pb-attach-selector") === undefined || target_.data("pb-attach-selector") === null){
			return new _attach_selector(this);
		}else{
			return target_.data("pb-attach-selector");
		}
	});

	$(document).ready(function(){
		var media_selectors_ = $("[data-pb-media-selector]");
		if(media_selectors_.length > 0){
			media_selectors_.each(function(){
				new _media_selector(this);
			});
		}

		var attach_selectors_ = $("[data-pb-attach-selector]");
		if(attach_selectors_.length > 0){
			attach_selectors_.each(function(){
				new _attach_selector(this);
			});
		}
	});

	PB._ajaxurl = PB._admin_ajaxurl;

	window._validateForm = window.validateForm;
	window.validateForm = function(form_){
		var bool1_ = window._validateForm(form_);
		var bool2_ = !$(form_)
			.find('.form-required')
			.filter( function() { return $( 'select:visible', this ).sval() === ''; } )
			.addClass( 'form-invalid' )
			.find( 'select:visible' )
			.change( function() { $( this ).closest( '.form-invalid' ).removeClass( 'form-invalid' ); } )
			.size();
		return (bool1_ && bool2_);
	};

	return PB;

})(jQuery);