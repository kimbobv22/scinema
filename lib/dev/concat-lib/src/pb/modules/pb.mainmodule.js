(function($){
	
	var PB = window.PB = {
		_admin_ajaxurl : PBVAR["admin_ajaxurl"],
		_ajaxurl : PBVAR["ajaxurl"],
		admin_ajaxurl : function(params_){
			if(params_ === undefined){
				return this._admin_ajaxurl;
			}

			return this.make_url(this._admin_ajaxurl, params_);
		},
		ajaxurl : function(params_){
			if(params_ === undefined){
				return this._ajaxurl;
			}

			return this.make_url(this._ajaxurl, params_);
		},
		nl2br : function(str_){
			return str_.replace(/\n/g, "<br />");  
		},
		make_currency : function(value_){
			$result_ = String(Math.abs(value_)).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');

			if(value_ < 0){
				$result_ = "-"+$result_;
			}

			return $result_;
		},make_k_number_format : function(value_){
			  return (value_ > 999 ? (value_/1000).toFixed(1) + 'k' : value_);
		},
		random_string : function(length_, possible_){
			var text_ = "";
			var possible_ = (possible_ !== undefined ? possible_ : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

			for(var i=0; i < length_; i++)
				text_ += possible_.charAt(Math.floor(Math.random() * possible_.length));

			return text_;
		},
		lpad : function(str_, char_, length_){
			if(! str_|| !char_|| str_.length >= length_) {
				return str_;
			}

			var max_ = (length_ - str_.length) / char_.length;

			for(var index_=0;index_<max_; ++index_){
				str_ = char_ + str_;
			}

			return str_;
		},
		rpad : function(str_, char_, length_){
			if(! str_|| !char_|| str_.length >= length_) {
				return str_;
			}

			var max_ = (length_ - str_.length) / char_.length;

			for(var index_=0;index_<max_; ++index_){
				str_ += char_;
			}

			return str_;
		},
		make_options : function(array_, value_column_, title_column_){
			var html_ = '';
			for(var index_=0;index_<array_.length; ++index_){
				html_ += '<option value="'+array_[index_][value_column_]+'">'+array_[index_][title_column_]+'</option>';
			}

			return html_;
		},
		json_equals : function(a_, b_, sort_array_){
			var sort_func_ = function(object_){
				if(sort_array_ === true && Array.isArray(object_)) {
					return object_.sort();
				}else if(typeof object_ !== "object" || object_ === null) {
					return object_;
				}

				return Object.keys(object_).sort().map(function(key_) {
					return {
						key: key_,
						value: sort_func_(object_[key_])
					};
				});
			};
			
			return JSON.stringify(sort_func_(a_)) === JSON.stringify(sort_func_(b_));
		},
		_data_filters : {},
		add_data_filter : function(key_, func_){
			if(this._data_filters[key_] === undefined || this._data_filters[key_] === null){
				this._data_filters[key_] = [];
			}
			this._data_filters[key_].push(func_);
		},
		apply_data_filters : function(key_, params_, add_){
			if(this._data_filters[key_] !== undefined && this._data_filters[key_] !== null){
				var filter_count_ = this._data_filters[key_].length;
				for(var index_=0;index_<filter_count_;++index_){
					params_ = this._data_filters[key_][index_](params_, add_);	
				}
			}
			return params_;
		},
		_data_actions : {},
		add_data_action : function(key_, func_){
			if(this._data_actions[key_] === undefined || this._data_actions[key_] === null){
				this._data_actions[key_] = [];
			}
			this._data_actions[key_].push(func_);
		},
		do_data_action : function(key_, params_){
			if(this._data_actions[key_] !== undefined && this._data_actions[key_] !== null){
				var filter_count_ = this._data_actions[key_].length;
				for(var index_=0;index_<filter_count_;++index_){
					this._data_actions[key_][index_](params_);	
				}
			}
		},
		_post : function(data_, callback_, options_){
			options_ = $.extend({
				type: "POST",
				url: this._ajaxurl,
				data : $.extend(true,{},data_),
				success : function(response_text_){
					var result_ = true;
					try{
						response_text_ = JSON.parse(response_text_);
					}catch(ex_){
						result_ = false;
					}
					callback_(result_,response_text_, response_text_);
				},error : function(xhr_, status_, thrown_){
					if(status_ === "abort") return;
					callback_(false, status_, thrown_);
				}
			},options_);
			
			return $.ajax(options_);
		},
		post : function(data_,callback_,block_, options_){
			if(block_ !== undefined && block_ === true){
				this.indicator(true, function(){
					PB._post.apply(PB, [data_, function(result_, response_text_, cause_){
						PB.indicator(false);
						callback_(result_, response_text_, cause_);
					}, options_]);
				});
			}else{
				PB._post.apply(PB,[data_, callback_]);
			}
		},
		make_url : function(url_, parameters_){
			parameters_ = $.extend({},parameters_);

			var concat_char_ = "?";
			if(url_.indexOf(concat_char_) >= 0)
				concat_char_ = "&";
			return url_+concat_char_+$.param(parameters_);
		},append_url : function(url_, append_path_){
				

			if(url_.lastIndexOf("/") != (url_.length -1)){
				url_ += "/";
			}

			return (url_ + append_path_);
		},modal : function(options_, callback_){
			console.error("please override me!");
			return;
		},confirm : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : PBVAR['OK'],
				button2 : PBVAR['cancel']
			},options_), callback_);
		},alert : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : PBVAR['OK']
			},options_), callback_);
		},forward : function(url_, params_, target_){
			var temp_form_ = document.createElement('form');

			for(var key_ in params_){
				var value_ = params_[key_];
				var param_element_ = document.createElement('input');
					param_element_.setAttribute('type', 'hidden');
					param_element_.setAttribute('name', key_);
					param_element_.setAttribute('value', value_);

				temp_form_.appendChild(param_element_);
			}

			if(target_){
				temp_form_.setAttribute('target', target_);
			}
			
			temp_form_.setAttribute('method', 'post');
			temp_form_.setAttribute('action', url_);

			document.body.appendChild(temp_form_);
			temp_form_.submit();
		},redirect : function(url_, params_){
			document.location = PB.make_url(url_, params_);
		},
		refresh : function(){
			var methods = [
				"location.reload()",
				"history.go(0)",
				"location.href = location.href",
				"location.href = location.pathname",
				"location.replace(location.pathname)",
				"location.reload(false)"
			];

			var $body = $("body");
			for(var i = 0; i < methods.length; ++i) {
				(function(cMethod) {
					eval(cMethod);
				})(methods[i]);
			}
		},url_parameter : function(name_){
			return decodeURIComponent((new RegExp('[?|&]' + name_ + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
		},url_path : function(){

			var t_result_ = location.pathname.split("/")
			var result_ = [];

			for(var index_=0;index_<t_result_.length; ++index_){
				if(t_result_[index_] !== ""){
					result_.push(t_result_[index_]);
				}
			}

			return result_;
		},
		export_youtube_id : function(url_){
			var video_id_ = url_.split('v=')[1];
			if(video_id_ === undefined){
				return null;
			}

			var ampersand_position_ = video_id_.indexOf('&');
			if(ampersand_position_ != -1) {
				video_id_ = video_id_.substring(0, ampersand_position_);
			}
			return video_id_;
		},
		week_of_month : function(date) {
			var day = date.getDate()
            day += (date.getDay() == 0 ? 0 : 7 - date.getDay());
            return Math.ceil(parseFloat(day) / 7);
		},
		geolocation : function(callback_){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(function(position_){
					var results_ = {
						"lat" : position_.coords.latitude,
						"lng" : position_.coords.longitude
					};

					callback_(results_);

				},function(){
					callback_(false, "failed");
				});
			}else{
				callback_(false, "not_supported");
			}
		},
		indicator_message : function(){
			return "<div class='pb-indicator-frame'><div class='uil-ring-css'><div></div></div></div>";
		},
		indicator : function(bool_, callback_){
			$("body").toggleClass('block-ui-in', bool_);
			if(bool_){
				$.blockUI({
					message : PB.indicator_message(),
					onBlock : callback_
				});
			}else{
				$.unblockUI({
					onUnblock : callback_
				});
			}
		},
		click_position : function(base_element_, event_, offset_){
			offset_ = $.extend({x : 0, y: 0}, offset_);

			var pageX_ = null,pageY_ = null;

			var touch_event_ = event_.originalEvent.changedTouches;

			if(touch_event_ && touch_event_.length > 0){
				pageX_ = touch_event_[0].pageX;
				pageY_ = touch_event_[0].pageY;
			}else{
				touch_event_ = event_.originalEvent.touches;

				if(touch_event_ && touch_event_.length > 0){
					pageX_ = touch_event_[0].pageX;
					pageY_ = touch_event_[0].pageY;
				}
			}

			if(!pageX_ || pageX_ === null){
				pageX_ = event_.pageX;
				pageY_ = event_.pageY;
			}

			var posX = base_element_.offset().left,posY = base_element_.offset().top;
			var x_ = (pageX_ - posX) + offset_.x;
			var y_ = (pageY_ - posY) + offset_.y;

			var width_ = base_element_.outerWidth();
			var height_ = base_element_.outerHeight();

			return {
				"x" : x_, "y" : y_,
				"xrate" : (x_ / width_), "yrate" : (y_ / height_)};
		},classic_popup : function (url_, options_) {

			options_ = $.extend({
				'width' : 500,
				'height' : 400,
				'resize' : false,
				'title' : "untitled"
			},options_);

			var strResize_ = (options_['resize'] ? 'yes' : 'no');

			var strParam = 'width=' + options_['width'] + ',height=' + options_['height'] + ',resizable=' + strResize_,
				objWindow = window.open(url_, options_['title'], strParam);

			objWindow.focus();

			return objWindow;
		},unix_to_date : function(unix_, format_){
			var moment_ = moment.unix(unix_);
			return moment_.format(format_);
		}
	}

	$.fn.smooth_inout = (function(toggle_, options_){

		if(this.length > 1){
			this.each(function(){
				$(this).smooth_inout(toggle_, options_);
			});
			return;
		}

		options_ = $.extend({
			in_class : 'in',
			out_class : 'out',
			animating_class : 'animating',
			end_class : 'end',
			duration : 300,
			callback : $.noop
		},options_);

		var inclass_ = options_.in_class;
		var outclass_ = options_.out_class;
		var animating_class_ = options_.animating_class;
		var end_class_ = options_.end_class;
		var animate_duration_ = options_.duration;
		var callback_ = options_.callback;

		if(toggle_ === undefined || toggle_ === null){
			toggle_ = !(this.hasClass(inclass_) && !this.hasClass(outclass_));
		}

		/*if(this.hasClass(animating_class_)){
			return !toggle_;
		}*/

		clearTimeout(this.data('pb_smooth_inout_timeout'));

		this.toggleClass(animating_class_, true);
		this.toggleClass(end_class_, false);

		if(toggle_){

			if(this.hasClass(inclass_)){
				this.toggleClass(outclass_, false);
			}else{
				this.toggleClass(inclass_, true);
			}

		}else{

			if(this.hasClass(inclass_)){
				this.toggleClass(outclass_, true);
			}else{
				this.toggleClass(inclass_, true);
				this.toggleClass(outclass_, true);
			}
		}

		var timeout_obj_ = setTimeout($.proxy(function(){
			this.target.toggleClass(this.animating_class, false);
			this.target.toggleClass(this.end_class, true);
			this.callback(this.toggle);
		},{
			target : this,
			toggle : toggle_,
			animating_class : animating_class_,
			end_class : end_class_,
			callback : callback_
		}), animate_duration_);

		this.data('pb_smooth_inout_timeout', timeout_obj_);

		return toggle_;
	});

	window.pb_add_filter = (function(key_, func_){
		return PB.add_data_filter(key_, func_);
	});
	window.pb_apply_filters = (function(key_, params_, add_){
		return PB.apply_data_filters(key_, params_, add_);
	});
	window.add_data_action = (function(key_, func_){
		return PB.add_data_action(key_, func_);
	});
	window.do_data_action = (function(key_, params_){
		return PB.do_data_action(key_, params_);
	});

	return PB;

})(jQuery);