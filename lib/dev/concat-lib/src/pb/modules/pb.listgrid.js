(function($){

	var PB_list_grid = (function(target_){
		this._target = target_;
		this._target.data("pblistgrid", this);
		this._global_id = this._target.data("pb-listgrid-id");
		this._table_form = this._target.closest("form");
		this._page_index_el = this._table_form.find("input[name='page_index']");
		this._pagenav = $("[data-pb-listgrid-pagenav-id='"+this._global_id+"']");

		this._rander_pagenav();
	});
	PB_list_grid.prototype.target = (function(){
		return this._target;
	});
	PB_list_grid.prototype.page_index = (function(page_index_){
		if(page_index_ !== undefined){
			this._page_index_el.val(page_index_);
			this.load();
		}

		return this._page_index_el.val();
	});
	PB_list_grid.prototype.load = (function(){
		this._table_form.submit();
	});

	PB_list_grid.prototype._rander_pagenav = (function(){
		var pagenav_buttons_ = this._pagenav.find("a[data-page-index]");
		var that_ = this;

		$.each(pagenav_buttons_, function(){
			var target_ = $(this);
			target_.data("pb-listgrid",that_);
			target_.click(function(event_){
				event_.preventDefault();
				var pagenav_btn_ = $(this);
				var page_index_ = pagenav_btn_.data("page-index");
				var listgrid_module_ = pagenav_btn_.data("pb-listgrid");

				var current_page_index_ = listgrid_module_.page_index();

				if(parseInt(current_page_index_) != parseInt(page_index_)){
					listgrid_module_.page_index(page_index_);
				}
			});
		});
	});

	$.fn.pblistgrid = (function(){
		var target_ = this;

		if(target_.data("pblistgrid") !== undefined && target_.data("pblistgrid") !== null){
			return target_.data("pblistgrid");
		}

		return new PB_list_grid(target_);
	});

	var PB_ajax_list_grid = (function(target_){
		PB_list_grid.apply(this, [target_]);

		this._options = {
			'before_load' : function(){
				return true;
			}
		};

		this._table_form.data("htajaxlistgrid", this);
		this._table_form.on("submit", function(){
			var listgrid_module_ = $(this).data("htajaxlistgrid");
			listgrid_module_.load();
			return false;
		});

		var autoload_el_ = this._table_form.find(":input[name='firstload']");
		if(autoload_el_.length <= 0 || autoload_el_.val() !== "N"){
			this.load();
		}
	});

	PB_ajax_list_grid.prototype = Object.create(PB_list_grid.prototype);

	PB_ajax_list_grid.prototype.load = (function(){
		if(!this._options['before_load'].apply(this)) return;

		var table_form_ = this._table_form;
		var table_params_ = table_form_.serialize_object();

		var that_ = this;
		var target_ = this._target;
		// var tbody_el_ = this._target;
		var pagenav_el_ = this._pagenav;

		target_.trigger("pblistgridstartload");
		
		target_.empty();
		pagenav_el_.empty();

		target_.append("<div class='loading-indicator'><i class='icon pb-loading-indicator'></i></div>");

		PB.post($.extend(table_params_,{
			action : "pb-listgrid-load-html",
			global_id : this._global_id
		}),function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				return;
			}

			target_.empty();
			target_.append(response_json_.body_html);
			pagenav_el_.append(response_json_.pagenav_html);
			that_._rander_pagenav();
			target_.trigger("pblistgridload", [response_json_.orgdata, target_.children()]);
		});
	});

	$.fn.htajaxlistgrid = (function(){
		var target_ = this;

		if(target_.data("pblistgrid") !== undefined && target_.data("pblistgrid") !== null){
			return target_.data("pblistgrid");
		}

		return new PB_ajax_list_grid(target_);
	});

})(jQuery);

function _pb_list_grid_initialize(global_id_, ajax_){
	$ = jQuery;
	if(ajax_ === undefined || ajax_ === null){
		ajax_ = false;
	}

	if(ajax_ === true){
		$("[data-pb-listgrid-id='"+global_id_+"']").htajaxlistgrid();
	}else{
		$("[data-pb-listgrid-id='"+global_id_+"']").pblistgrid();
	}
	
}