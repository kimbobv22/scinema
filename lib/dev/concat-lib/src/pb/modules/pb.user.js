(function($){

	PB.user = $.extend(PB.user, {
		send_authcode_for_email : function(email_, callback_){
			callback_ = callback_ || $.noop;

			PB.post({
				action : "user-send-authcode-for-email",
				email : email_,
			}, $.proxy(function(result_, response_json_){
				this((result_ && response_json_.success), response_json_);
			}, callback_), true);
		},
		check_authcode_for_email : function(email_, authcode_, callback_){
			callback_ = callback_ || $.noop;

			PB.post({
				action : "user-check-authcode-for-email",
				email : email_,
				authcode : authcode_,
			}, $.proxy(function(result_, response_json_){
				this((result_ && response_json_.success), response_json_);
			},callback_), true);
		}
	});
	
})(jQuery);