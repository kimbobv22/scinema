(function($){

	window.PB.crypt = {
		_public_key_ : (PBVAR["crypt-public-key-n"]+"|"+PBVAR["crypt-public-key-e"]),
		//암호화하기
	    encrypt : function(text_){
	      var result_ = cryptico.encrypt(text_, this._public_key_);
	      if(result_.status === "success"){
	        return result_.cipher;
	      }else return false;
	    }
	}

	//param 필터 등록
	PB.add_data_filter("encrypt", function(params_, add_){
		if(add_ === undefined || add_ === null) return params_;
		var add_count_ = add_.length;
		for(var index_=0;index_<add_count_;++index_){
			var param_name_ = add_[index_];
			params_[param_name_] = PB.crypt.encrypt(params_[param_name_]);
		}

		return params_;
	});

	return PB.crypt;

})(jQuery);