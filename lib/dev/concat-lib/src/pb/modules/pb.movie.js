(function($){

	PB.movie = {

		open_xticket_popup : function(schedule_id_, cinema_id_){ //비회원 티케팅 팝업

			if(PBVAR['is_user_logged_in'] === "Y"){
				window.open(PB.make_url(PB.append_url(PBVAR['xticketing_url'], schedule_id_),{
					cinema_id : cinema_id_
				}), null, "width=910,height=623");
				return;
			}

			PB.post({
				action : "movie-load-xticketing-popup",
				schedule_id : schedule_id_,
				cinema_id : cinema_id_
			}, function(result_, response_json_){

				if(!result_ || response_json_.success !== true){
					PB.alert_error({
						title : "에러발생",
						content : "예매정보팝업을 불러오는 중 에러가 발생했습니다.",
					});
					return;
				}

				$("#pb-xticketing-modal").remove();
				$("body").append(response_json_.popup_html);

				var popup_ = $("#pb-xticketing-modal");
				var popup_form_ = popup_.find("#pb-xticketing-modal-form");

/*				popup_form_.find("#pb-xticket-popup-form-birth_yyyymmdd-wrap").datetimepicker({
					viewMode: 'years',
					locale: 'ko',
					allowInputToggle : true,
					format: 'YYYY-MM-DD'
				});
*/
				popup_form_.validator();

				popup_form_.submit_handler(function(){
					var window_ = window.open('about:blank','xtikcet-popup','width=910,height=623');
					popup_form_[0].target ="xtikcet-popup";
					popup_form_[0].submit();

					popup_.modal("hide");			
				});

				popup_.modal("show");

			}, true);
		},

		open_xticket_check_popup : function(cinema_id_){ //비회원 티케팅 확인 팝업
			if(PBVAR['is_user_logged_in'] === "Y"){
				window.open(PB.make_url(PBVAR['xticketing_check_url'],{
					cinema_id : cinema_id_
				}), null, "width=910,height=623");
				return;
			}


			PB.post({
				action : "movie-load-xticketing-check-popup",
				cinema_id : cinema_id_
			}, function(result_, response_json_){

				if(!result_ || response_json_.success !== true){
					PB.alert_error({
						title : "에러발생",
						content : "예매정보팝업을 불러오는 중 에러가 발생했습니다.",
					});
					return;
				}

				$("#pb-xticketing-check-modal").remove();
				$("body").append(response_json_.popup_html);

				var popup_ = $("#pb-xticketing-check-modal");
				var popup_form_ = popup_.find("#pb-xticketing-check-modal-form");

				popup_form_.validator();

				popup_form_.submit_handler(function(){
					var window_ = window.open('about:blank','xtikcet-popup','width=910,height=623');
					popup_form_[0].target ="xtikcet-popup";
					popup_form_[0].submit();

					popup_.modal("hide");			
				});

				popup_.modal("show");

			}, true);
		},
		open_dtryx_check_popup : function(cinema_id_) { // 디트릭스 예매확인 

			PB.post({
				action : "movie-dtryx-reservation-popup"
			}, function(result_, response_json_){

				if(!result_ || response_json_.success !== true){
					PB.alert_error({
						title : "에러발생",
						content : "예매정보팝업을 불러오는 중 에러가 발생했습니다.",
					});
					return;
				}  

				if(response_json_.cinema_type !== 'dtryx'){
					PB.movie.open_xticket_check_popup(cinema_id_);
				}
				else {
					$("#dtryx-check-popup").remove();
					$("#popRegR").remove();
					$("body").append(response_json_.popup_html);
					DTRYXBookingRsv();
				}
			}, true);
		}
	}

	jQuery(document).ready(function(){
		var xticket_check_btns_ = $("[data-xticket-check]");

		if(xticket_check_btns_.length > 0){
			xticket_check_btns_.each(function(){
				$(this).click(function(){
					var target_btn_ = $(this);
					//PB.movie.open_xticket_check_popup(target_btn_.attr("data-xticket-check"));
					PB.movie.open_dtryx_check_popup(target_btn_.attr("data-xticket-check"));
					return false;
				});
				
			});
		}
	});

})(jQuery);