(function($){

	var _pb_slider_uploader_btn = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'callback' : $.noop,
		}, options_);


		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-slider-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-slider-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		this._uploader_modal.find("#pb-slider-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : PB.file.upload_url(),
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				this._options['callback'].apply(this, [files_]);
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._target.click($.proxy(function(){
			$("#pb-slider-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._target.data('pb-slider-uploader-module', this);
	}

	$.fn.pb_slider_uploader_btn = (function(options_){
		var module_ = this.data('pb-slider-uploader-module');
		if(module_) return module_;
		return new _pb_slider_uploader_btn(this, options_);
	});
	
	var _pb_multiple_slider_uploader = function(target_, options_){
		this._target = $(target_);
		this._options = $.extend(true, {
			'maxlength' : 10,
			'modal_label' : '이미지업로드',
			'change' : $.noop,
			'max_width' : null,
		}, options_);

		this._target.wrap("<div class='pb-multiple-slider-uploader pb-slide-list-frame'></div>");
		this._wrap = this._target.parent();
		
		this._add_btn = $("<a href='#' class='add-btn'>+</a>");
		this._add_btn.appendTo(this._wrap);

		this._target.data('pb-multiple-slider-uploader-module', this);

		this._modal_uid = PB.random_string(5);

		var modal_html_ = '<div class="pb-imageupload-dropzone-modal modal" id="pb-multiple-slider-uploader-modal-'+this._modal_uid+'">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-body">' +
						'<input id="pb-multiple-slider-uploader-modal-input-'+this._modal_uid+'" type="file" name="files[]" accept="image/*" multiple>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

		this._uploader_modal = $(modal_html_);
		this._uploader_modal.appendTo("body");

		var upload_url_ = PB.file.upload_url();

		if(this._options['max_width']){
			upload_url_ = PB.file.upload_url({
				'max_width' : this._options['max_width']
			});
		}

		this._uploader_modal.find("#pb-multiple-slider-uploader-modal-input-"+this._modal_uid).pb_fileupload_btn({
			upload_url : upload_url_,
			label : this._options['modal_label'],
			button_class : "btn-default btn-sm",
			dropzone : this._uploader_modal,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			autoupload : true,
			limit : this._options['maxlength'],
			done : $.proxy(function(files_){
				var module_ = this;
				$.each(files_, function(){
					module_.add(this);
				});
			
				this._uploader_modal.modal("hide");
			}, this)
		});

		this._add_btn.click($.proxy(function(){
			$("#pb-multiple-slider-uploader-modal-"+this._modal_uid).modal("show");
			return false;
		}, this));

		this._wrap.sortable({
			items: '.slide-item'
		});

		var target_json_ = null;

		try{
			target_json_ = JSON.parse(this._target.val());
		}catch(e_){
			target_json_ = [];
		}

		this.apply_json(target_json_);
	};

	_pb_multiple_slider_uploader.prototype.target = (function(){
		return this._target;
	});

	_pb_multiple_slider_uploader.prototype.options = (function(options_){
		if(options_ !== undefined){
			this._options = $.extend(true, {
				'maxlength' : 10,
				'modal_label' : '이미지업로드'
			}, options_);
		}

		return this._options;
	});

	_pb_multiple_slider_uploader.prototype.add = (function(data_){
		var preview_el_ = $("<div class='preview'></div>");
			preview_el_.css({
				'backgroundImage' : "url("+(data_['url'])+")"
			});

		var preview_wrap_el_ = $("<div class='preview-wrap'></div>");
			preview_wrap_el_.append(preview_el_);

		data_ = $.extend(true, {
			url : "",
			title : "",
			content : "",
			button_name : "",
			link_url : "",
		},data_);

		var image_item_el_ = $("<div class='slide-item'></div>");
			image_item_el_.append(preview_wrap_el_);
			image_item_el_.append("<a href='#' class='delete-btn'></a>");

		var slider_content_el_ = $("<div class='content'></div>");
		
		var slider_content_html_ = '<label>타이틀</label>' +
			'<input type="text" placeholder="타이틀 입력" class="form-control input-sm" data-column-name="title" value="'+data_['title']+'">' +
			'<div class="form-margin"></div>' +
			'<label>내용</label>' +
			'<textarea placeholder="내용 입력" class="form-control input-sm" data-column-name="content">'+data_['content']+'</textarea>' +
			'<div class="form-margin"></div>' +
			'<label>버튼명</label>' +
			'<input type="text" placeholder="버튼명 입력" class="form-control input-sm" data-column-name="button_name" value="'+data_['button_name']+'">' +
			'<small class="help-block">*입력하지 않으면 표시되지 않습니다.</small>' + 
			'<div class="form-margin"></div>' +
			'<label>버튼링크</label>' +
			'<input type="url" placeholder="URL 입력" class="form-control input-sm" data-column-name="link_url" value="'+data_['link_url']+'">';

		slider_content_el_.append(slider_content_html_);
		image_item_el_.append(slider_content_el_);

		image_item_el_.attr("data-url", data_['url']);

		this._add_btn.before(image_item_el_);

		image_item_el_.find(".delete-btn").click($.proxy(function(event_){
			$(event_.currentTarget).closest(".slide-item").remove();
			this._update_add_btn();
			
			this._options['change'].apply(this);
			return false;
		}, this));

		this._update_add_btn();
		
		this._options['change'].apply(this);	
		
	});

	_pb_multiple_slider_uploader.prototype._update_add_btn = (function(){
		var toggle_ = (this._options['maxlength'] > this._wrap.find(".slide-item").length);
		this._add_btn.toggle(toggle_);
	});
		
	_pb_multiple_slider_uploader.prototype.apply_json = (function(json_){
		this._wrap.find(".slide-item").remove();

		if($.type(json_) !== "array") return;

		var that_ = this;
		$.each(json_, function(){
			that_.add(this);
		});
		
		this._update_add_btn();
	});
	_pb_multiple_slider_uploader.prototype.to_json = (function(){
		var image_items_ = this._wrap.children(".slide-item");
		var results_ = [];

		if(image_items_.length > 0){
			image_items_.each(function(){
				var image_item_ = $(this);

				var data_url_ = {
					url : image_item_.attr("data-url"),
					title : image_item_.find("[data-column-name='title']").val(),
					content : image_item_.find("[data-column-name='content']").val(),
					button_name : image_item_.find("[data-column-name='button_name']").val(),
					link_url : image_item_.find("[data-column-name='link_url']").val(),
				};

				results_.push(data_url_);
			});
		}
			

		return results_;
	});
	

	$.fn.pb_multiple_slider_uploader = (function(options_){
		var module_ = this.data('pb-multiple-slider-uploader-module');
		if(module_) return module_;
		return new _pb_multiple_slider_uploader(this, options_);
	});

	
})(jQuery);