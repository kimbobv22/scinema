(function($){

	window.PB.address = {
		

		geolocation : function(callback_){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(function(position_){
					var latlng_ = {
						"lat" : position_.coords.latitude,
						"lng" : position_.coords.longitude
					};

					PB.maps._cache_latlng(latlng_, function(success_, latlng_){
						PB.do_data_action("geolocation", {
							type : "geo",
							latlng : latlng_
						});
						callback_(success_, latlng_);
					});
				},function(){
					callback_(false);
				});
			}else{
				callback_(false);
			}
		},

		_check_maps_api : function(){
			var result_ = (daum !== undefined && daum.maps !== undefined);

			if(!result_) console.error('daum.maps API must included');

			return result_;
		},
		_check_postcode_api : function(){
			var result_ =  (daum !== undefined && daum.Postcode !== undefined);

			if(!result_) console.error('daum.Postcode API must included');

			return result_;
		},

		search_postcode : function(callback_, options_){
			callback_ = callback_ || $.noop;

			if(!this._check_postcode_api()){
				callback_(false);
				 return;
			}

			callback_ = callback_ || $.noop;

			options_ = $.extend({
				opentype : "embed",
				onclose: $.proxy(function(state_){
					if(state_ === 'FORCE_CLOSE'){
						this(false);
					}
				}, callback_),
				oncomplete : $.proxy(function(data_){

					this(true, {
						postcode : data_['zonecode'],
						address : (data_['autoJibunAddress'] || data_['jibunAddress']),
						address_road : (data_['autoRoadAddress'] || data_['roadAddress']),
						si : data_['sido'],
						gu : data_['sigungu'],
						dong : data_['bname'],
					});

				}, callback_)
			},options_);

			var daum_map_ = new daum.Postcode(options_);

			if(options_.opentype === "embed" && options_.element){
				daum_map_.embed(options_.element);
			}else daum_map_.open();			
		}, search_latlng : function(address_, callback_){
			callback_ = callback_ || $.noop;

			if(!this._check_maps_api()){
				callback_(false);
				 return;
			}

			var geocoder_ = new daum.maps.services.Geocoder();

			geocoder_.addr2coord(address_, $.proxy(function(status_, result_){
				if(status_ !== daum.maps.services.Status.OK){
					this(false);
					return;
				}

				this(true, {
					lat : result_.addr[0].lat,
					lng : result_.addr[0].lng
				});

			}, callback_));
		}
	};

})(jQuery);