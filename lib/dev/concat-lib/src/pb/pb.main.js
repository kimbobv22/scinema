(function($){

	window.$ = jQuery;

	PB = $.extend(PB, {
		modal : function(options_, callback_){
			options_ = $.extend({
				show : true,
				classes : "pb-common-popup",
				closebtn : true,
				escbtn : true,
				backdrop : 'static',
				callback_input : false,
				button1Classes : "btn btn-primary",
				button2Classes : "btn btn-default",
				button3Classes : "btn btn-default",
				appendTo : null
			},options_);

			var unique_id_ = this.random_string(10,"abcdefg1234567890");
			var has_title_ = (options_.title !== undefined && options_.title !== null);
			var has_icon_ = (options_.icon !== undefined && options_.icon !== null);

			options_._unique_id = unique_id_;

			var html_ = '<div class="modal fade '+(options_.classes)+' '+(has_title_ ? '' : 'notitle')+'" id="pb-common-modal-'+unique_id_+'" tabindex="-1" role="dialog" aria-labelledby="pb-common-modal-label" data-unique-id="'+unique_id_+'">' +
				'<div class="modal-dialog" role="document">' +
					'<div class="modal-content">';

			if(options_.closebtn){
				html_ +=	'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="fa fa-times"></i></button>';
			}

			if(has_title_ && !has_icon_){
				html_ += '<div class="modal-header">';
				html_ +=	'<h4 class="modal-title" id="pb-common-modal-label">'+options_.title+'</h4>';			
				html_ += '</div>';
			}
				
			html_ += '<div class="modal-body">';

			if(has_icon_){
				html_ += '<div class="icon-wrap">';
				html_ += 	'<i class="icon '+options_.icon+'"></i>';
				html_ += '</div>';

				if(has_title_){
					html_ += '<div class="title-wrap">';
					html_ +=	'<h4 class="modal-title" id="pb-common-modal-label">'+options_.title+'</h4>';
					html_ += '</div>';
				}
			}

			html_ += '<div class="content-wrap '+(has_icon_ ? "has-icon-title" : "")+'">';
			if(options_.content) html_ += options_.content;
			html_ += '</div>';

			html_ += '</div>';

			if(!options_.button1Classes.indexOf("btn-")){
				options_.button1Classes += " btn-primary ";
			}

			if(!options_.button2Classes.indexOf("btn-")){
				options_.button2Classes += " btn-default ";
			}
			
			if(options_.button1 !== undefined && options_.button1 !== null && options_.button1 !== ""){
				
				html_ +=	'<div class="modal-footer '+(has_icon_ ? "has-icon-title" : "")+'">';

				var _temp_func_make_button2 = function(){
					if(options_.button2 !== undefined && options_.button2 !== null && options_.button2 !== "")
						return '<button type="button" class="'+(has_icon_ ? "btn-block" : "")+' button2 '+options_.button2Classes+' ">'+options_.button2+'</button>';
					else return "";
				}

				var _temp_func_make_button3 = function(){
					if(options_.button3 !== undefined && options_.button3 !== null && options_.button3 !== "")
						return '<button type="button" class="'+(has_icon_ ? "btn-block" : "")+' button3 '+options_.button3Classes+' ">'+options_.button3+'</button>';
					else return "";
				}

				if(!has_icon_){
					html_ += _temp_func_make_button3();
					html_ += _temp_func_make_button2();
				}
				
				html_ += '<button type="button" class="'+(has_icon_ ? "btn-block" : "")+' button1 '+options_.button1Classes+' ">'+options_.button1+'</button>';

				if(has_icon_){
					html_ += _temp_func_make_button2();
					html_ += _temp_func_make_button3();
				}
				html_ += '</div>';
			}

			html_ += '</div>' +
				'</div>' +
			'</div>';


			var modal_ = $(html_);
			$("body").append(modal_);

			modal_.modal({
				keyboard : options_.escbtn,
				backdrop : options_.backdrop,
				show : options_.show
			});

			if(options_.show && options_.appendTo !== null){
				var appendToElement_ = $(options_.appendTo);
				if(appendToElement_.length > 0){
					$("body").removeClass("modal-open");
					$('.modal-backdrop').appendTo(appendToElement_); 
					modal_.appendTo(appendToElement_); 
					modal_.addClass('appendto-element');
					$('.modal-backdrop').addClass("appendto-element");
					$('.modal-backdrop').off('focusin.bs.modal');

					$('.modal-backdrop').one('bsTransitionEnd', function(){
						$(document).off('focusin.bs.modal');
					});
					
				}
			}
			
			// modal_.modal("show");

			var button1_ = modal_.find(".modal-footer .button1");
			var button2_ = modal_.find(".modal-footer .button2");
			var button3_ = modal_.find(".modal-footer .button3");

			button1_.data("pb-modal-options", options_);
			button1_.data("pb-modal-callback", callback_);
			button1_.click(function(){
				var options_ = $(this).data("pb-modal-options");
				var modal_ = $("#pb-common-modal-"+options_._unique_id);
				var callback_ = $(this).data("pb-modal-callback");

				var inputs_ = {};
				if(options_.callback_input){
					var input_els_ = modal_.find('.modal-body :input');

					if(input_els_.length > 0){
						input_els_.each(function(){
							inputs_[$(this).attr('name')] = $(this).val();
						});
					}
				}

				modal_.modal("hide");
				if(callback_ !== undefined) callback_(true, inputs_);
			});

			if(button2_.length > 0){
				button2_.data("pb-modal-options", options_);
				button2_.data("pb-modal-callback", callback_);
				button2_.click(function(){
					var options_ = $(this).data("pb-modal-options");
					var modal_ = $("#pb-common-modal-"+options_._unique_id);
					var callback_ = $(this).data("pb-modal-callback");

					modal_.modal("hide");
					if(callback_ !== undefined) callback_(false, 1);
				});
			}

			if(button3_.length > 0){
				button3_.data("pb-modal-options", options_);
				button3_.data("pb-modal-callback", callback_);
				button3_.click(function(){
					var options_ = $(this).data("pb-modal-options");
					var modal_ = $("#pb-common-modal-"+options_._unique_id);
					var callback_ = $(this).data("pb-modal-callback");

					modal_.modal("hide");
					if(callback_ !== undefined) callback_(false, 2);
				});
			}

			modal_.on("hidden.bs.modal", function(){
				$(this).remove();
			});

			return modal_;
		},confirm : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : "확인",
				button2 : "취소"
			},options_), callback_);
		},confirm_success : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-check text-success"
			}, options_), callback_);
		},confirm_info : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa fa-info-circle text-info"
			}, options_), callback_);
		},confirm_warning : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-exclamation text-warning"
			}, options_), callback_);
		},confirm_q : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-question text-primary"
			}, options_), callback_);
		},
		confirm_error : function(options_, callback_){
			return this.confirm($.extend({
				icon : "fa fa-meh-o text-danger"
			}, options_), callback_);
		},alert : function(options_, callback_){
			return this.modal($.extend({
				closebtn : false,
				escbtn : false,
				button1 : "확인"
			},options_), callback_);
		},alert_success : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-check text-success"
			}, options_), callback_);
		},alert_info : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa fa-info-circle text-info"
			}, options_), callback_);
		},alert_warning : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-exclamation text-warning"
			}, options_), callback_);
		},alert_q : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-question text-primary"
			}, options_), callback_);
		},
		alert_error : function(options_, callback_){
			return this.alert($.extend({
				icon : "fa fa-meh-o text-danger"
			}, options_), callback_);
		}

	});

	$.fn.scrollFocus = (function(animate_){
		animate_ = (animate_ === undefined ? 200 : 0);
		var offset_ = this.offset();
		$("body").animate({scrollTop : offset_['top']}, animate_);
	});

	$(function(){
		var responsive_var_element_ = $("body").find(".pb-responsive-var");
		var xs_element_ = responsive_var_element_.children(".xs");
		var sm_element_ = responsive_var_element_.children(".sm");
		var md_element_ = responsive_var_element_.children(".md");
		var lg_element_ = responsive_var_element_.children(".lg");

		PB.responsive_info = {
			xs_min : parseInt(xs_element_.css('minWidth')),
			xs_max : parseInt(xs_element_.css('maxWidth')),
			
			sm_min : parseInt(sm_element_.css('minWidth')),
			sm_max : parseInt(sm_element_.css('maxWidth')),

			md_min : parseInt(md_element_.css('minWidth')),
			md_max : parseInt(md_element_.css('maxWidth')),

			lg_min : parseInt(lg_element_.css('minWidth'))
		}
	});



	$(document).ready(function(){
		var window_el_ = $(window);
		var pb_header_ = $("#pb-header");

		window_el_.scroll(function(event_){
			pb_header_.toggleClass('scroll-fixed', (window_el_.scrollTop() >= 200));
		});
		AOS.init();

		pb_header_.on("show.bs.collapse", function(){
			$(this).toggleClass("navbar-toggled", true);
		}).on( "hide.bs.collapse", function(){
			$(this).toggleClass("navbar-toggled", false);
		});

		function _pb_hide_nav_submenu(sub_menu_el_){
			sub_menu_el_.toggleClass("out", true);

			sub_menu_el_.stop(true);
			sub_menu_el_.delay(500).queue(function(){
				$(this).toggle(false);
			});
		}

		var dropdown_li_list_ = pb_header_.find("#pb-header-mainmenu-item > .menu-item.menu-item-has-children");
			dropdown_li_list_.each(function(){
				var dropdown_li_ = $(this);
				dropdown_li_.mouseenter(function(event_){

					var window_width_ = window_el_.width();
					if(window_width_ < PB.responsive_info.xs_max){
						return;
					}

					var parent_li_ = $(event_.currentTarget);
					var sub_menu_el_ = parent_li_.find("ul.sub-menu");
					var other_sub_menu_ = pb_header_.find(".menu-item.menu-item-has-children ul.sub-menu");

					$.each(other_sub_menu_, function(){
						var other_sub_menu_item_ = $(this);
						if(!other_sub_menu_item_.is(sub_menu_el_)){
							_pb_hide_nav_submenu(other_sub_menu_item_);	
						}
					});
					
					sub_menu_el_.toggle(true);
					sub_menu_el_.stop(true);
					sub_menu_el_.toggleClass("dropdown-active", true);
					sub_menu_el_.toggleClass("out", false);
					
					// sub_menu_el_.css({left : 0});
					var offset_ = parent_li_.offset();
					var max_x_ = offset_.left + sub_menu_el_.outerWidth();
					var window_width_ = $(window).width() - 10;

					if(window_width_ < max_x_){
						var fix_val_ = -(max_x_ - window_width_);
						sub_menu_el_.css({left : fix_val_+"px"})
					}else{
						sub_menu_el_.css({left : "0px"})
					}

					clearTimeout(parent_li_.data("menu-dropdown-timeout"));
				});
				dropdown_li_.mouseout(function(event_){

					var window_width_ = window_el_.width();
					if(window_width_ < PB.responsive_info.xs_max){
						return;
					}

					var timeout_ = $(this).data("menu-dropdown-timeout");
					clearTimeout(timeout_);

					timeout_ = setTimeout($.proxy(function(){
						_pb_hide_nav_submenu($(this).find("ul.sub-menu"));
					}, $(this)), 700);
					$(this).data("menu-dropdown-timeout", timeout_);

				});
			});

		window_el_.resize(function(){
			var window_width_ = window_el_.width();

			if(window_width_ < PB.responsive_info.xs_max){
				var all_submenu_ = pb_header_.find(".menu-item.menu-item-has-children ul.sub-menu");
					all_submenu_.stop(true);
					all_submenu_.toggle(true);
				all_submenu_.css({left : null});
			}
		});
	});


	$(document).on('hidden.bs.modal', '.modal', function () {
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});

	$(document).on('show.bs.modal', '.modal', function () {
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() {
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});

	$.fn.form_working = (function(bool_, add_objects_){
		var targets_ = this.find("[data-form-field]");
		if(add_objects_ !== undefined){
			targets_ = $.merge(targets_, add_objects_);
		}

		this.form_msg(false);
		targets_.each(function(){
			var target_ = $(this);
			if(target_.data("loading-text") !== undefined && target_.data("loading-text") !== null){
				if(bool_){
					target_.button("loading");
				}else target_.button("reset");
			}else{
				target_.toggleClass("disabled", bool_);
				target_.prop("disabled", bool_);
			}
		});
	});

	// $.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="hidden"], [type="submit"], [type="reset"], button,)'
	$.fn.validator.Constructor.VALIDATORS = $.extend($.fn.validator.Constructor.VALIDATORS, {
		"number" : function($el_){
			if(!(/^[0-9]+$/.test($el_.val()))){
				return "error number"
			}
		},
		"maxlength" : function($el_){
			var maxlength = $el_.data('maxlength')
			return $el_.val().length > maxlength && "error maxlength";
		},
		"alphanum" : function($el_){
			if(!(/^[a-zA-Z][a-zA-Z0-9]+$/.test($el_.val()))){
				return "error alphanum"
			}
		},"password" : function($el_){
			if(!(/^[A-Za-z0-9!_#@$^*&]{8,}$/.test($el_.val()))){
				return "error password";
			}
		},
		required: function ($el) { return !!$.trim($el.val()) }
	});

	$.fn.submit_handler = (function(success_, fail_){
		this.data('form-callback-success', success_) || $.fn.noop;
		this.data('form-callback-fail', fail_) || $.fn.noop;

		this.validator().on('submit', function(event_){
			var target_ = $(this);
			var callback_success_ = target_.data('form-callback-success')  || $.noop;
			var callback_fail_ = target_.data('form-callback-fail') || $.noop;

			if(event_.isDefaultPrevented()){
				callback_fail_.apply(this, [target_]);
				return;
			}

			return callback_success_.apply(this, [target_]) || false;
		});

		
	});

	

	function getOsCode() {
		var uagent = navigator.userAgent.toLocaleLowerCase();
		if (uagent.search("android") > -1 && '0.0.0' <= '4.0.3') {
			return "android";
		} else if (uagent.search("iphone") > -1 || uagent.search("ipod") > -1 || uagent.search("ipad") > -1) {
			return "ios";
		} else {
			return "etc";
		}
	}
	var osCode = getOsCode();

	function openWithApp(osCode, scheme, customUrl, packageName, appStoreUrl) {
		var intentUrl = "intent://" + customUrl + "#Intent;scheme=" + scheme + ";package=" + packageName + ";end";
		var customScheme = scheme + "://" + customUrl;
		if (osCode == "android") {
			window.location = intentUrl;
			return true;
		} else if (osCode == "ios") {
			var clickedAt = +new Date;
			var talkInstCheckTimer = setTimeout(function() {
				if (+new Date - clickedAt < 2000) {
					window.location = appStoreUrl;
				}
			}, 1500);
			window.location = customScheme;
			return true;
		} else {
			return false;
		}
	}
		

	jQuery(document).ready(function($){
		$("[data-kakao-talk-btn]").click(function(event_) {

			var scheme_ = $(this).attr("data-kakao-talk-btn");
			var customUrl = "plusfriend/friend/"+scheme_;
			var appStoreUrl = "https://itunes.apple.com/kr/app/kakaotog-kakaotalk/id362057947";
			var result_ = openWithApp(osCode, "kakaoplus", customUrl, "com.kakao.talk", appStoreUrl);
			if(result_){
				event_.preventDefault();
				return;
			}

		});

		$(".form-control.readonly").keydown(function(event_){
			return false;
		});
	});


	return PB;

})(jQuery);