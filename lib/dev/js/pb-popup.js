$(function($){

	PB.popup = $.extend(PB.popup,{
		layout : function(popup_data_, offset_, z_index_, ignore_cookie_){


			popup_data_ = $.extend({
				size_width : 350,
				size_height : 400,
			}, popup_data_);

			ignore_cookie_ = ignore_cookie_ === true;			

			if(!offset_ && $(".pb-popup-overlay").length > 0){

				var last_popup_ = $(".pb-popup-overlay").last();
				var offset_ = {
					left : parseInt(last_popup_.css("left")),
					top :  parseInt(last_popup_.css("top")),
				};

				offset_ = {
					left : offset_.left + last_popup_.width() + 5,
					top : 10
				}

				z_index_ = parseInt(last_popup_.css('zIndex')) + 1;

			}else{
				offset_ = $.extend({left : 10, top: 10}, offset_);	
				z_index_ = z_index_ ? z_index_ : 2002;
			}

			if(!ignore_cookie_ && popup_data_['ID']){
				var popup_ids_ = $.cookie('pb-do-not-see-popup-ids');
					popup_ids_ = popup_ids_ ? popup_ids_ : "";
					popup_ids_ = popup_ids_.replace(/\/$/, "");
					popup_ids_ = popup_ids_.split(':');

				if(popup_ids_.indexOf(popup_data_['ID']) >= 0){
					return;
				}
			}

			var popup_html_ = '<div class="pb-popup-overlay">'+
				'<a href="#" data-popup-close-btn class="close-btn"><i class="fa fa-times"></i></a>' +
				'<div class="warp">' +
					'<div class="content">' +
						popup_data_['popup_html'] +
					'</div>' +
					'<div class="bottom text-center">' +
						'<a href="#" data-popup-cookie-close-btn class="cookie-close-link">다시보지않기</a>' +
					'</div>' +
				'</div>' +
			'</div>';


			var popup_el_ = $(popup_html_);
				popup_el_.appendTo("body");
				popup_el_.css({
					left : offset_.left,
					top : offset_.top,
					width : popup_data_.size_width+"px",
					zIndex : z_index_
				});			

			popup_el_.data('popup-id', popup_data_['ID']);

			popup_el_.find("[data-popup-close-btn]").click(function(event_){
				$(event_.currentTarget).closest(".pb-popup-overlay").remove();
				return false;
			});
			popup_el_.find("[data-popup-cookie-close-btn]").click(function(event_){
				

				var target_popup_ = $(event_.currentTarget).closest('.pb-popup-overlay');
				var popup_id_ = target_popup_.data('popup-id');
				if(popup_id_){
					var popup_ids_ = $.cookie('pb-do-not-see-popup-ids');
						popup_ids_ = popup_ids_ ? popup_ids_ : "";
						popup_ids_ = popup_ids_.replace(/\/$/, "");

					popup_ids_ = popup_ids_.split(':');
					popup_ids_.push(popup_id_);

					$.cookie('pb-do-not-see-popup-ids', popup_ids_.join(":"));
				}

				$(event_.currentTarget).closest(".pb-popup-overlay").remove();
				return false;
			});

			popup_el_.draggable();
			return popup_el_;
		}
	});

});