﻿/*
    jTable localization file for 'Vietnamese' language.
    Author: Lê Hoàng Hiếu
*/
(function ($) {
    $.extend(true, $.hik.jtable.prototype.options.messages, {
        serverCommunicationError: '서버통신 중 에러가 발생했습니다.',
        loadingMessage: '불러오는 중...',
        noDataAvailable: '데이타가 없습니다.',
        addNewRecord: '+ 행추가',
        editRecord: '행수정',
        areYouSure: 'Bạn có chắc không?',
        deleteConfirmation: 'Dữ liệu này sẽ bị xóa! Bạn có chắc không?',
        save: 'Lưu',
        saving: 'Đang lưu',
        cancel: 'Hủy',
        deleteText: 'Xóa',
        deleting: 'Đang xóa',
        error: 'Lỗi',
        close: 'Đóng',
        cannotLoadOptionsFor: 'Không thể tải các tùy chọn cho trường {0}!',
        pagingInfo: 'Hiện từ {0} đến {1} của {2} bản ghi',
        canNotDeletedRecords: 'Không thể xóa {0} bản ghi của {1} bản ghi!',
        deleteProggress: 'Đã xóa được {0} của {1} bản ghi. Đang xử lý...',
        pageSizeChangeLabel: 'Số bản ghi', //New. Must be localized.
        gotoPageLabel: 'Tới trang' //New. Must be localized.
    });
})(jQuery);
