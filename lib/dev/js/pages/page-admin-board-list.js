jQuery(document).ready(function($){
		
	var board_add_popup_ = $("#pb-admin-board-add-popup");

	_pb_admin_pbboard_build_popup(board_add_popup_, function(form_data_){
		_pb_admin_pbboard_add(form_data_);
	});
});

function pb_admin_pbboard_add(){
	var target_popup_ = $("#pb-admin-board-add-popup");
	$.magnificPopup.open({
		items: {
			src: target_popup_,
			type: 'inline',
			preloader: false
		},
		callbacks : {
			open : $.proxy(function(){
				this.find(":input[name='type']").change();
			}, target_popup_)
		},
		closeBtnInside : false,
		closeOnBgClick : false,
		showCloseBtn : false,
		enableEscapeKey : false
	});
}

function _pb_admin_pbboard_add(board_data_){
	PB.post({
		action : 'pb_admin_board_add_new',
		board_data : board_data_
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : (response_json_.error_title || '에러발생'),
				content : (response_json_.error_message || '게시판 추가 중 에러가 발생했습니다.'),
			});
			return;
		}

		document.location = document.location;

	}, true);
}

function pb_admin_pbboard_edit(board_id_){

	PB.post({
		action : "pb_admin_board_load_edit_popup",
		board_id : board_id_
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : (response_json_.error_title || '에러발생'),
				content : (response_json_.error_message || '게시판정보를 불러오는 중 에러가 발생했습니다.'),
			});

			return;
		}

		var popup_el_ = $(response_json_.popup_html);
		$("#pb-admin-board-edit-popup").remove();
		popup_el_.appendTo("body");

		_pb_admin_pbboard_build_popup(popup_el_ , function(form_data_){
			_pb_admin_pbboard_update(form_data_.board_id, form_data_);
		});

		$.magnificPopup.open({
			items: {
				src: popup_el_,
				type: 'inline',
				preloader: false
			},
			callbacks : {
				open : $.proxy(function(){
					this.find(":input[name='type']").change();
				}, popup_el_)
			},
			closeBtnInside : false,
			closeOnBgClick : false,
			showCloseBtn : false,
			enableEscapeKey : false
		});
	}, true);
}

function _pb_admin_pbboard_update(board_id_, board_data_){
	PB.post({
		action : 'pb_admin_board_update',
		board_id : board_id_,
		board_data : board_data_
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : (response_json_.error_title || '에러발생'),
				content : (response_json_.error_message || '게시판 수정 중 에러가 발생했습니다.'),
			});
			return;
		}

		document.location = document.location;

	}, true);


}

function _pb_admin_pbboard_build_popup(popup_el_, submit_callback_){
	submit_callback_ = submit_callback_ || $.noop;
	var target_form_ = popup_el_.find("form");

	var use_secret_el_ = popup_el_.find(":input[name='use_secret']");

	var use_secret_el_toggle_func_ = function(tform_){
		var use_secret_ = tform_.find(":input[name='use_secret']:checked").val() === "Y";

		// tform_.find("[data-toogle-use-secret]").toggle(use_secret_);

		tform_.find(":input[name='use_only_secret']").prop("disabled", !use_secret_);

		if(!use_secret_){
			tform_.find(":input[name='use_only_secret'][value='N']").prop('checked', true);
		}
	}

	use_secret_el_.click($.proxy(function(){
		use_secret_el_toggle_func_(popup_el_);
	}, popup_el_));
	use_secret_el_toggle_func_(popup_el_);


	popup_el_.find(":input[name='type']").change($.proxy(function(){

		var board_data_ = this.serialize_object();

		PB.post({
			action : "pb_admin_board_load_admin_fields",
			board_data : board_data_,
		}, $.proxy(function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				PB.alert_error({
					title : (response_json_.error_title || '에러발생'),
					content : (response_json_.error_message || '관리자 항목을 불러오는 중 에러가 발생했습니다.'),
				});
				return;
			}

			var board_type_tr_ = this.find("[data-board-type-tr]");

			this.find("[data-board-type-field-tr]").remove();
			var field_el_ = $(response_json_.field_html);
			board_type_tr_.after(field_el_);
			field_el_.attr('data-board-type-field-tr', "installed");

		}, this), true);

	}, target_form_));

	popup_el_.find("[data-popup-close-btn]").click(function(){
		$.magnificPopup.close();
		return false;
	});

	popup_el_.on('mfpOpen', $.proxy(function(){

		this.find(":input[name='type']").change();
	},popup_el_));

	target_form_.validate({
		messages : {
			board_name : "게시판제목을 입력하세요"
		},
		submitHandler : $.proxy(function(){
			this['callback'].apply(this['form'], [this['form'].serialize_object()]);	
		}, {callback : submit_callback_, form : target_form_})
	});
}

function pb_admin_pbboard_remove(board_id_){
	PB.confirm({
		title : "게시판삭제",
		content : "게시판의 모든 글이 삭제됩니다. 게시판을 삭제하시겠습니까?",
		button1 : "삭제하기"
	}, function(confirmed_){
		if(!confirmed_) return;

		_pb_admin_pbboard_remove(board_id_);
	});
}

function _pb_admin_pbboard_remove(board_id_){
	PB.post({
		action : "pb_admin_board_remove",
		board_id : board_id_
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : (response_json_.error_title || '에러발생'),
				content : (response_json_.error_message || '게시판 삭제 중 에러가 발생했습니다.'),
			});
			return;
		}

		document.location = document.location;
	}, true);
}