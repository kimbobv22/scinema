jQuery(document).ready(function($){
	var movive_chart_type_links_ = $("#pb-movie-chart-type-menu [data-movie-slider-tab]");
	var movive_chart_slider1_ = $("#pb-home-movie-chart-slider1");
	var movive_chart_slider2_ = $("#pb-home-movie-chart-slider2");

	movive_chart_type_links_.click(function(){
		var target_link_ = $(this);

		$("#pb-movie-chart-type-menu > li").toggleClass("actived", false);
		target_link_.parent().toggleClass("actived", true);

		var slider_tab_ = $(target_link_.attr("data-movie-slider-tab"));

		movive_chart_type_links_.each(function(){
			$($(this).attr("data-movie-slider-tab")).toggleClass("hidden", true);
		});

		slider_tab_.toggleClass("hidden", false);
		slider_tab_.find(".swiper-container")[0].swiper.update(true);
		slider_tab_.find(".swiper-container")[0].swiper.stopAutoplay();
		slider_tab_.find(".swiper-container")[0].swiper.startAutoplay();

		return false;
	});

	var xs_min_ = PB.responsive_info.xs_min;
	var xs_max_ = PB.responsive_info.xs_max;
	var sm_max_ = PB.responsive_info.sm_max;

	var breakpoints_ = {};
	breakpoints_[xs_min_] = {
		slidesPerView: 1,
		spaceBetween: 20
	};
	breakpoints_[xs_max_] = {
		slidesPerView: 2,
		spaceBetween: 20
	};
	breakpoints_[sm_max_] = {
		slidesPerView: 3,
		spaceBetween: 20
	};

	var movive_chart_slider1_module_ = new Swiper(movive_chart_slider1_[0], {
		speed: 400,
		spaceBetween: 30,
		slidesPerView: 5,
		autoplayDisableOnInteraction : false,
		autoplay : 3000,
		prevButton: '.swiper-button-prev',
		nextButton: '.swiper-button-next',

		grabCursor : true,
		breakpoints : {
			480 : {
				slidesPerView: 1.5,
				spaceBetween: 10,
				centeredSlides : true,
			},
			768 : {
				slidesPerView: 3,
				spaceBetween: 15,
			},
			1199 : {
				slidesPerView: 3,
				spaceBetween: 15,
			}
		}
	}); 

	var movive_chart_slider2_module_ = new Swiper(movive_chart_slider2_[0], {
		speed: 400,
		spaceBetween: 30,
		slidesPerView: 5,
		autoplayDisableOnInteraction : false,
		autoplay : 3000,
		prevButton: '.swiper-button-prev',
		nextButton: '.swiper-button-next',
		grabCursor : true,
		breakpoints : {
			480 : {
				slidesPerView: 1.5,
				spaceBetween: 10,
				centeredSlides : true,
			},
			768 : {
				slidesPerView: 3,
				spaceBetween: 15,
			},
			1199 : {
				slidesPerView: 3,
				spaceBetween: 15,
			}
		}
	}); 

	var window_el_ = $(window);
	var partner_slider_ = $("#pb-home-partner-slider");

	breakpoints_[xs_min_] = {
		slidesPerView: 3,
		spaceBetween: 20,
		autoplay : 3000,
	};
	breakpoints_[xs_max_] = {
		slidesPerView: 4,
		spaceBetween: 20,
		autoplay : 3000,
	};
	breakpoints_[sm_max_] = {
		slidesPerView: 5,
		spaceBetween: 30,
		autoplay : 3000,
	};

	var partner_slider_module_ = new Swiper(partner_slider_[0], {
		speed: 400,
		spaceBetween: 30,
		slidesPerView: 8,
		autoplayDisableOnInteraction : false,
		autoplay : 3000,
		breakpoints : breakpoints_,
	}); 

	var home_banner_slider_module_ = new Swiper('#pb-home-main-banner-slider', {
		pagination: '.swiper-pagination',
        paginationClickable: true,
        parallax: true,
		speed: 1000,
		spaceBetween: 0,
		autoplay: 3000,		
	});

	var home_event_slider_module_ = new Swiper('#pb-home-main-event-slider', {
		pagination: '.swiper-pagination',
        paginationClickable: true,
        parallax: true,
		speed: 300,
		spaceBetween: 10,
		autoplay: 3000,		
		slidesPerView: 1,
		breakpoints : {
			480 : {
				slidesPerView: 1,
			},
			700 : {
				slidesPerView: 1,
			},
			991 : {
				slidesPerView: 2,
				spaceBetween: 15,
			}
		}
	});

});
