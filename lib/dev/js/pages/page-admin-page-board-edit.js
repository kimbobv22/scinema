jQuery(document).ready(function($){
	
	var installed_pbboard_el_ = $("#post").find(":input[name='installed_pbboard']");

	installed_pbboard_el_.change(function(){

		var selected_option_ = $(this).children(":selected");
		var board_id_ = selected_option_.val();
		var post_id_ = $("#post").find(":input[name='post_ID']").val();

		$("[data-custom-type-field]").empty();

		if(!board_id_){
			return;
		}

		console.log(post_id_, board_id_);

		PB.post({
			action : "pb_admin_board_load_admin_page_metabox_fields",
			post_id : post_id_,
			board_id : board_id_,
		}, $.proxy(function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				PB.alert_error({
					title : (response_json_.error_title || '에러발생'),
					content : (response_json_.error_message || '관리자 항목을 불러오는 중 에러가 발생했습니다.'),
				});
				return;
			}

			$("[data-custom-type-field]").append(response_json_.field_html)

		}, this), true);

	});

	installed_pbboard_el_.change();
});