jQuery(document).ready(function($){


});

function pb_user_manage_send_reset_password(user_id_){

	PB.confirm({
		'title' : "메일발송확인",
		'content' : "비밀번호 재설정을 위해 메일로 발송하시겠습니까?",
		'button1' : "재설정하기",
		'button2' : "취소"
	}, function(c_){
		if(!c_) return;

		_pb_user_manage_send_reset_password(user_id_);
	});
}

function _pb_user_manage_send_reset_password(user_id_){
	PB.post({
		action : "adminpage-user-manage-send-reset-password",
		user_id : user_id_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : "에러발생",
				content : "메일발송 중 에러가 발생했습니다."
			});
			return;
		}

		PB.alert_success({
			title : "발송완료",
			content : "해당 사용자 가입메일로 비밀번호 재설정 링크를 발송하였습니다."
		});

	}, true);
}