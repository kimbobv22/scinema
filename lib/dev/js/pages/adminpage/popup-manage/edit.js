jQuery(document).ready(function($){

	var edit_form_ = $("#pb-popup-manage-edit-form");
	var popup_html_wrap_ = edit_form_.find(".popup-html-wrap");
	var popup_html_wrap_inner_ = popup_html_wrap_.children();

	edit_form_.validator();
	edit_form_.submit_handler(pb_popup_manage_update);

	$("#pb-popup-manage-edit-form-size_width").change(function(event_){
		var target_width_ = $(event_.currentTarget).val();
			target_width_ = parseInt(target_width_);
			target_width_ = isNaN(target_width_) ? 0 : target_width_;

			if(target_width_ == 0){
				popup_html_wrap_inner_.css({
					'maxWidth' : null
				});
				return;				
			}

			popup_html_wrap_inner_.css({
				'maxWidth' : target_width_+"px"
			});
	});
	$("#pb-popup-manage-edit-form-size_width").change();

	$("#pb-popup-manage-edit-form-srt_date").datetimepicker({
		format : 'YYYY-MM-DD HH:mm'
	});

	$("#pb-popup-manage-edit-form-end_date").datetimepicker({
		format : 'YYYY-MM-DD HH:mm'
	});


	$('#pb-popup-manage-upload-image').pb_fileupload_btn({
		label : "이미지업로드",
		button_class : "btn-default btn-sm",
		dropzone : true,
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
		autoupload : true,
		done : function(files_){
			var target_editor_ = tinyMCE.activeEditor;

			$.each(files_, function(){
				target_editor_.execCommand('mceInsertRawHTML', false, '<img src="'+this['url']+'">');
			});

			$("#pb-popup-manage-imageupload-popup").modal("hide");
			
		}
	});


});

function pb_popup_manage_update(){
	$ = jQuery;
	var edit_form_ = $("#pb-popup-manage-edit-form");

	var wp_content_editor_ = edit_form_.find(":input[name='popup_html']");
	var wp_content_ = tinyMCE.activeEditor.getContent();

	if(wp_content_ === null || wp_content_.length <= 0){
		PB.alert_warning({
			title : "내용없음",
			content : "작성하신 내용이 없습니다. 내용을 입력하여 주세요."
		});
		return;
	}

	if(edit_form_.data('add-new') === "Y"){
		PB.confirm_q({
			title : "팝업추가",
			content : "팝업을 추가하시겠습니끼?",
			button1 : "추가하기"
		}, function(c_){
			if(!c_) return;
			_pb_popup_manage_edit_submit();
		});
	}else{
		_pb_popup_manage_edit_submit();
	}
}

function _pb_popup_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-popup-manage-edit-form");
	var popup_data_ = edit_form_.serialize_object();

	var wp_content_editor_ = edit_form_.find(":input[name='popup_html']");
	popup_data_['popup_html'] = tinyMCE.activeEditor.getContent();

	console.log(popup_data_);

	PB.post({
		action : "popup-update",
		popup_data : popup_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '팝업 추가,수정 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = response_json_.redirect_url;
		});

	}, true);
}

function pb_popup_manage_preview(){
	$ = jQuery;

	var edit_form_ = $("#pb-popup-manage-edit-form");	
	var popup_data_ = edit_form_.serialize_object();

	var wp_content_editor_ = edit_form_.find(":input[name='popup_html']");
	popup_data_['popup_html'] = tinyMCE.activeEditor.getContent();

	PB.popup.layout(popup_data_, null, null, true);
}


function _pb_popup_manage_delete(){
	PB.confirm_q({
		title : "팝업삭제확인",
		content : "해당 팝업을 삭제하시겠습니까?",
		button1 : '삭제하기',
		button1_classes : "btn btn-black",
	}, function(c_){
		if(!c_) return;

		_pb_popup_manage_do_delete();

	});
}
function _pb_popup_manage_do_delete(){
	$ = jQuery;
	var edit_form_ = $("#pb-popup-manage-edit-form");

	PB.post({
		action : "popup-delete",
		popup_id : edit_form_.find(":input[name='ID']").val()
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '팝업삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "팝업삭제완료",
			content : "팝업삭제가 완료되었습니다.",
		}, function(){
			document.location = response_json_.redirect_url;
		});


	},true);
}