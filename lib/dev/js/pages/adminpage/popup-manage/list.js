jQuery(document).ready(function($){
	var hiden_yns_ = $("[data-popup-hide-yn]");


	hiden_yns_.each(function(){
		$(this).click(function(){

			pb_switch_hide_yn($(this).attr("data-popup-hide-yn"), ($(this).prop("checked") ? "Y" : "N"));

		});
	});

	var bulk_delete_btn_ = $("#pb-popup-manage-bulk-delete-btn");
	var all_popup_id_checkboxes_ = $("#pb-adminpage-popup-manage-table :input[name='popup_id']");

	all_popup_id_checkboxes_.click(function(){
		var toggle_ = ($("#pb-adminpage-popup-manage-table :input[name='popup_id']:checked").length > 0);
		bulk_delete_btn_.prop("disabled", !toggle_);
	});
});

function pb_switch_hide_yn(popup_id_, yn_){
	$ = jQuery;

	var hide_yn_ = $("[data-popup-hide-yn='"+popup_id_+"']");

	hide_yn_.prop("disabled", true);

	PB.post({
		action : "popup-switch-hide-yn",
		popup_id : popup_id_,
		yn :yn_
	}, $.proxy(function(result_, response_json_){
		this.prop("disabled", false);
		

	}, hide_yn_), false);
}

function pb_popup_manage_bulk_delete(){
	PB.confirm_q({
		title : "선택삭제",
		content : "선택한 팝업을 일괄삭제하시겠습니까?",
		button1 : "삭제하기"
	}, function(c_){
		if(!c_) return;
		_pb_popup_manage_bulk_delete();
	});
}
function _pb_popup_manage_bulk_delete(){
	$ = jQuery;
	var popup_id_el_ = $("#pb-adminpage-popup-manage-table :input[name='popup_id']:checked");
	var popup_ids_ = [];

	$.each(popup_id_el_, function(){
		popup_ids_.push($(this).val());
	});

	PB.post({
		action :  "popup-bulk-delete",
		popup_ids : popup_ids_
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '팝업삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "팝업삭제완료",
			content : "팝업삭제가 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	},true);
}