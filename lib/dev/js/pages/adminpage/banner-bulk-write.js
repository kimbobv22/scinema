jQuery(document).ready(function($){
	var target_form_ = $("#pb-banner-bulk-write-form");

	var banner_image_uploaders_ = target_form_.find('[data-column-name="banner_image_url"]');

	$.each(banner_image_uploaders_, function(){
		$(this).pb_multiple_image_uploader({
			'maxlength' : 15,
			'modal_label' : "배너이미지 업로드",
			'use_thumbnail' : false,
		});
	});

	$("#pb-banner-bulk-write-form-cinema_id").change(function(){
		var toggle_ = $(this).children(":selected").val() === "common";
		$("[data-all-cinema-type]").toggle(toggle_);
	}).change();
	
	target_form_.submit_handler(function(){

		var banner_image_urls_ = $("#pb-banner-bulk-form-banner_image_url").pb_multiple_image_uploader().to_json();

		PB.confirm_q({
			title : "일괄등록확인",
			content : "입력한 내용을 일괄등록하시겠습니까?",
			button1  : "일괄등록하기"
		}, function(c_){
			if(!c_) return false;

			_pb_adminpage_banner_bulk_write(target_form_.find("#pb-banner-bulk-write-form-cinema_id").sval(), banner_image_urls_);

		});

	});
});

function _pb_adminpage_banner_bulk_write(cinema_id_, banner_image_urls_){
	$ = jQuery;

	var target_form_ = $("#pb-banner-bulk-write-form");

	var cinema_types_ = [];

	target_form_.find("[name='cinema_type']:checked").each(function(){
		cinema_types_.push($(this).val());
	})

	PB.post({
		action : "adminpage-banner-bulk-upload",
		cinema_id : cinema_id_,
		banner_image_urls :  banner_image_urls_,
		cinema_types : cinema_types_
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '일괄등록 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "등록성공",
			content : "배너 일괄등록이 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	}, true);

}