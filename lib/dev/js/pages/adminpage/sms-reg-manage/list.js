jQuery(document).ready(function($){
	
});


function _pb_sms_reg_manage_delete(reg_id_){
	PB.confirm_q({
		title : "삭제확인",
		content : "해당 SMS회원정보를 삭제하시겠습니까?",
		button1 : "삭제하기",
		button1Classes : "btn btn-black",
	}, function(c_){
		if(!c_) return ;

		_pb_sms_reg_manage_delete_phase(reg_id_);

	});
}

function _pb_sms_reg_manage_delete_phase(reg_id_){

	PB.post({
		action : "adminpage-sms-reg-delete",
		reg_id : reg_id_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : "에러발생",
				content : "SMS회원정보 삭제 중 에러가 발생했습니다."
			});
			return;
		}

		PB.alert_success({
			title : "삭제완료",
			content : "SMS회원정보를 삭제하였습니다.",
		}, function(){
			document.location = document.location;
		});

	}, true);

}