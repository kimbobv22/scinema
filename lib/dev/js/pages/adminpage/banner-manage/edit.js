jQuery(document).ready(function($){

	var popup_form_ = $("#pb-banner-manage-edit-popup-form");

	popup_form_.validator();
	popup_form_.submit_handler(_pb_banner_manage_update);

	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");

	var module_ = image_url_hidden_.pb_multiple_image_uploader({
		'maxlength' : 1,
		'modal_label' : "배너이미지 업로드",
		'use_thumbnail' : false,
	});
	

	var bulk_delete_btn_ = $("#pb-banner-manage-bulk-delete-btn");
	var all_banner_id_checkboxes_ = $("#pb-adminpage-banner-manage-table :input[name='banner_id']");

	all_banner_id_checkboxes_.click(function(){
		var toggle_ = ($("#pb-adminpage-banner-manage-table :input[name='banner_id']:checked").length > 0);
		bulk_delete_btn_.prop("disabled", !toggle_);
	});
});

function _pb_banner_manage_add(){
	$ = jQuery;
	var popup_ = $("#pb-banner-manage-edit-popup");
	var popup_form_ = $("#pb-banner-manage-edit-popup-form");
		popup_form_.toggleClass("edit-form", false);
		popup_form_.toggleClass("add-form", true);

	popup_form_.data('add-new', "Y");
	popup_form_[0].reset();

	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");
	var image_module_ = image_url_hidden_.pb_multiple_image_uploader();
		image_module_.apply_json([]);

	popup_.modal("show");
}
function _pb_banner_manage_edit(id_){
	$ = jQuery;

	$ = jQuery;
	var popup_ = $("#pb-banner-manage-edit-popup");
	var popup_form_ = $("#pb-banner-manage-edit-popup-form");
		popup_form_.toggleClass("edit-form", true);
		popup_form_.toggleClass("add-form", false);

	popup_form_.data('add-new', "N");
	popup_form_[0].reset();

	var banner_data_ = JSON.parse($("[data-banner-data-id='"+id_+"']").val());
	
	popup_form_.find("[name='banner_id']").val(banner_data_['ID']);
	popup_form_.find("[name='banner_title']").val(banner_data_['banner_title']);
	popup_form_.find("[name='link_url']").val(banner_data_['link_url']);




	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");
	var image_module_ = image_url_hidden_.pb_multiple_image_uploader();
		image_module_.apply_json([{
			'url' : banner_data_['banner_image_url'],
			'name' : banner_data_['banner_image_url'],
			'type' : "",
		}]);

	popup_.modal("show");
}

function _pb_banner_manage_update(){
	$ = jQuery;
	var popup_form_ = $("#pb-banner-manage-edit-popup-form");

	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");
	var image_module_ = image_url_hidden_.pb_multiple_image_uploader();
	var image_urls_ = image_module_.to_json();

	if(image_urls_.length <= 0){
		PB.alert_error({
			title : "배너이미지확인",
			content : "배너이미지를 선택하세요"
		});
		return;
	}

	if(popup_form_.data('add-new') === "Y"){
		PB.confirm_q({
			title : "배너추가",
			content : "배너를 추가하시겠습니끼?",
			button1 : "추가하기"
		}, function(c_){
			if(!c_) return;
			_pb_banner_manage_edit_submit();
		});
	}else{
		_pb_banner_manage_edit_submit();
	}
}

function _pb_banner_manage_edit_submit(){
	$ = jQuery;
	var popup_form_ = $("#pb-banner-manage-edit-popup-form");
	var banner_data_ = popup_form_.serialize_object();

	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");
	var image_module_ = image_url_hidden_.pb_multiple_image_uploader();
	var image_urls_ = image_module_.to_json();

	banner_data_['banner_image_url'] = image_urls_[0].url;

	PB.post({
		action : "banner-update",
		banner_data : banner_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배너 추가,수정 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = document.location;
		});

	}, true);
}


function _pb_banner_manage_delete(id_){
	PB.confirm_q({
		title : "배너삭제확인",
		content : "해당 배너를 삭제하시겠습니까?",
		button1 : '삭제하기',
		button1_classes : "btn btn-black",
	}, function(c_){
		if(!c_) return;

		_pb_banner_manage_do_delete(id_);

	});
}
function _pb_banner_manage_do_delete(id_){
	$ = jQuery;

	PB.post({
		action : "banner-delete",
		banner_id : id_
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배너삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "배너삭제완료",
			content : "배너삭제가 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	},true);
}

function pb_banner_manage_bulk_delete(){
	PB.confirm_q({
		title : "선택삭제",
		content : "선택한 배너를 일괄삭제하시겠습니까?",
		button1 : "삭제하기"
	}, function(c_){
		if(!c_) return;
		_pb_banner_manage_bulk_delete();
	});
}
function _pb_banner_manage_bulk_delete(){
	$ = jQuery;
	var banner_id_el_ = $("#pb-adminpage-banner-manage-table :input[name='banner_id']:checked");
	var banner_ids_ = [];

	$.each(banner_id_el_, function(){
		banner_ids_.push($(this).val());
	});

	PB.post({
		action :  "banner-bulk-delete",
		banner_ids : banner_ids_
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배너삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "배너삭제완료",
			content : "배너삭제가 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	},true);
}