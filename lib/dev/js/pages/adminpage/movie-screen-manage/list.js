jQuery(document).ready(function($){
	$('#screen-open-srt-date').datetimepicker({
		format : 'YYYY-MM-DD',
		locale : 'ko',
	});
	$('#screen-open-end-date').datetimepicker({
		format : 'YYYY-MM-DD',
		locale : 'ko',
	});
});