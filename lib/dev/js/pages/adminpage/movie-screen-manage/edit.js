jQuery(document).ready(function($){
	var edit_form_ = $("#pb-templated-edit-edit-form");
	var open_id_select_ = edit_form_.find("[name='open_id']");
	var open_preview_item_ = $("#pb-movie-open-item-preview");

	window._pb_update_open_movie_preview = function(){
		var option_data_ = JSON.parse(edit_form_.find("[name='default_open_data']").val());

		open_preview_item_.toggleClass("hidden",!option_data_);

		if(!option_data_) return;

		$.each(option_data_, function(key_, value_){
			open_preview_item_.find("[data-column='"+key_+"']").text(value_);
		});

		$.each(option_data_, function(key_, value_){
			open_preview_item_.find("[data-image='"+key_+"']").attr("src", value_);
		});
	}
/*
	open_id_select_.on("change",function(event_){
		_pb_update_open_movie_preview();

		var default_value_ = open_id_select_.sval();
		
		var module_ = open_id_select_[0].selectize;
		var options_ = module_.options;
		var row_data_ = null;
		var option_data_ = options_[parseInt(default_value_)];

		edit_form_.find("[name='publisher_charge']").val(option_data_.publisher_charge);
		edit_form_.find("[name='cinema_charge']").val(option_data_.cinema_charge);
		edit_form_.find("[name='ticket_charge']").val((parseInt(option_data_.cinema_charge) + parseInt(option_data_.publisher_charge)));

	});
*/
	_pb_update_open_movie_preview();


	var screen_end_date_default_val_ = edit_form_.find("[name='screen_end_date']").val();
	var screen_end_date_default_fg_val_ = edit_form_.find("[name='screen_end_date_fg']").prop("checked");
	var screen_end_date_el_ = edit_form_.find("[name='screen_end_date']");

	if(edit_form_.find("[name='is_head_office']").val() === "N"){
		screen_end_date_el_.parent().on("dp.change", function(event_){
			var new_screen_end_date_ = screen_end_date_el_.val();

			if(new_screen_end_date_ === "" || new_screen_end_date_ === screen_end_date_default_val_){
				edit_form_.find("[name='screen_end_date_fg']").prop("checked", (screen_end_date_default_fg_val_ === "Y"));

				return;
			}

			edit_form_.find("[name='screen_end_date_fg']").prop("checked",true);


		});	
	}

	edit_form_.find("[name='screen_end_date_fg']").on("click", function(){
		var toggle_ = $(this).prop("checked");
		screen_end_date_el_.prop("readonly", toggle_);
	});
	screen_end_date_el_.prop("readonly", edit_form_.find("[name='screen_end_date_fg']").prop("checked"));
		
	var cinema_id_el_ = edit_form_.find("[name='cinema_id']");
	var xticket_ref_code_el_ = edit_form_.find("[name='_xticket_ref_code']");

	if(cinema_id_el_.attr("type") !== "hidden"){
		cinema_id_el_.change(function(){
			_pb_movie_screen_load_xticket_ref_code();			
		});	


	}
	var real_xticket_ref_code_el_ = edit_form_.find("[name='xticket_ref_code']");

	xticket_ref_code_el_.change(function(){
		edit_form_.find("[name='xticket_ref_code']").val($(this).sval());
	});


	$("#pb-find-movie-open-id-modal-form").submit(function(){
		$("#pb-find-movie-open-id-modal-form").find("[name='page_index']").val(0);
	});
});

function _pb_movie_screen_load_xticket_ref_code(){
	$ = jQuery;

	var edit_form_ = $("#pb-templated-edit-edit-form");

	var cinema_id_el_ = edit_form_.find("[name='cinema_id']");
	var xticket_ref_code_el_ = edit_form_.find("[name='_xticket_ref_code']");
	var cinema_id_ = null;

	if(cinema_id_el_.attr("type") !== "hidden"){
		var cinema_id_ = cinema_id_el_.sval();
	}else{
		var cinema_id_ = cinema_id_el_.val();
	}

	PB.post({
		action : "movie-screen-xticket-movie-list",
		cinema_id : cinema_id_
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : "에러발생",
				content : "영화정보 불러오는 중 에러가 발생했습니다."
			});
			return;
		}

		var movie_list_ = response_json_.movie_list;

		for(var row_index_=0;row_index_<movie_list_.length;++row_index_){
			var movie_data_ = movie_list_[row_index_];

			xticket_ref_code_el_.append("<option value='"+movie_data_['id']+"'>"+movie_data_['name']+"</option>");

		}

	}, true);
}

function pb_movie_screen_manage_edit_find_open_id(){
	$ = jQuery;

	var edit_form_ = $("#pb-templated-edit-edit-form");

	var cinema_id_el_ = edit_form_.find("[name='cinema_id']");
	var xticket_ref_code_el_ = edit_form_.find("[name='_xticket_ref_code']");
	var cinema_id_ = null;

	if(cinema_id_el_.attr("type") !== "hidden"){
		var cinema_id_ = cinema_id_el_.sval();
	}else{
		var cinema_id_ = cinema_id_el_.val();
	}


	if(!cinema_id_ || cinema_id_ === ""){
		PB.alert_info({
			title : "영화관선택확인",
			content : "개봉영화선택 전, 영화관을 선택하세요",
		});
		return;
	}

	var open_id_modal_ = $("#pb-find-movie-open-id-modal");
	var open_id_modal_form_ = open_id_modal_.find("form");
		open_id_modal_form_[0].reset();

	open_id_modal_form_.find(":input[name='cinema_id']").val(cinema_id_);

	$("#pb-movie-open-id-table").htajaxlistgrid().page_index(0);

	open_id_modal_.modal("show");
	
}

function _pb_movie_screen_mange_edit_select_open_id(open_id_){
	$ = jQuery;

	var open_data_str_ = $("[data-movie-open-data-id='"+open_id_+"']").val();
	var open_data_ = JSON.parse(open_data_str_);

	var edit_form_ = $("#pb-templated-edit-edit-form");
		edit_form_.find("[name='default_open_data']").val($("[data-movie-open-data-id='"+open_id_+"']").val());

	edit_form_.find("[name='open_id']").val(open_id_);
	edit_form_.find("[name='open_id_name']").val(open_data_.movie_name);
	
	$("#pb-find-movie-open-id-modal").modal("hide");
	
	_pb_update_open_movie_preview();	
}
