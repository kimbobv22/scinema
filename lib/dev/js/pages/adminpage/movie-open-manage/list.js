jQuery(document).ready(function($){
	$('#movie-open-srt-date').datetimepicker({
		format : 'YYYY-MM-DD',
		locale : 'ko',
	});
	$('#movie-open-end-date').datetimepicker({
		format : 'YYYY-MM-DD',
		locale : 'ko',
	});


});

function pb_movie_open_resize_poster_images(){
	PB.confirm_q({
		title : "리사이즈확인",
		content : "포스터이미지를 일괄로 최적화합니다. 진행하시겠습니까?",
		button1 : "최적화하기"
	},function(c_){
		if(!c_) return;

		PB.post({
			action : "movie-open-manage-resize-poster-image",
		}, function(result_,response_json_){

			if(!result_ || response_json_.success !== true){
				PB.alert_error({
					title : "에러발생",
					content : "리사이즈 중 에러가 발생했습니다."
				});
				return;
			}

			var resize_count_ = response_json_.resize_count;

			PB.alert_success({
				title : "작업완료",
				content : "총 "+resize_count_+"개의 리사이즈 작업이 성공적으로 완료되었습니다."
			});

		}, true);

	});
		
}