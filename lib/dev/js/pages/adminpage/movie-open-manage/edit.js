jQuery(document).ready(function($){
	var kofic_find_modal_ = $("#pb-kofic-find-db-modal");
	var kofic_find_modal_form_ = $("#pb-kofic-find-db-modal-form");
	var kofic_find_results_list_ = kofic_find_modal_.find(".find-results-list");

	kofic_find_modal_.on("shown.bs.modal", function(){
		kofic_find_modal_.find("[name='query']").focus();
	});

	kofic_find_modal_.on("show.bs.modal", function(){
		kofic_find_results_list_.empty();
		kofic_find_results_list_.append("<div class='message no-results'>검색어를 입력하여 검색하기</div>");
		// kofic_find_results_list_.append("<div class='loading-indicator'><i class='icon pb-loading-indicator'></i></div>")
	});

	kofic_find_modal_form_.validator();
	kofic_find_modal_form_.submit_handler(_pb_movie_open_manage_edit_find_openapi);


	var publisher_add_modal_ = $("#pb-publisher-reg-modal");
	var publisher_add_modal_form_ = publisher_add_modal_.find("form");
		publisher_add_modal_form_.validator();

	publisher_add_modal_form_.submit_handler(pb_movie_open_mange_open_publisher_do_add);
});

function pb_movie_open_manage_edit_find_kofic_code(){
	$ = jQuery;
	$("#pb-kofic-find-db-modal").modal("show");
}
function _pb_movie_open_manage_edit_find_openapi(){
	$ = jQuery;
	var kofic_find_modal_ = $("#pb-kofic-find-db-modal");
	var kofic_find_modal_form_ = $("#pb-kofic-find-db-modal-form");
	var kofic_find_results_list_ = kofic_find_modal_.find(".find-results-list");

	kofic_find_results_list_.empty();

	kofic_find_results_list_.append("<div class='loading-indicator'><i class='icon pb-loading-indicator'></i></div>")

	PB.post({
		action : "movie-open-manage-find-openapi",
		query : kofic_find_modal_form_.find("[name='query']").val()
	}, function(result_, response_json_){

		kofic_find_results_list_.empty();
		var movie_list_ = response_json_.movie_list;

		$.each(movie_list_, function(){
			var row_data_ = this;

			var row_html_ = '<div class="kofic-movie-info-item">' + 
				'<a href="#" class="movie-title" data-kofic-code="'+row_data_['kofic_code']+'">'+row_data_['movie_name']+'</a>' + 
				'<div class="subinfo">' + 
					'<div class="item opendate">개봉일'+moment(row_data_['open_date'], 'YYYYMMDD').format("YYYY-MM-DD")+'</div>' +
					'<div class="item genre">'+row_data_['genre']+'</div>' + 
				'</div>' +
			'</div>';


			var row_element_ = $(row_html_);

			kofic_find_results_list_.append(row_element_);

			row_element_.find(".movie-title").click(function(){
				var kofic_code_ = $(this).attr("data-kofic-code");
				_pb_movie_open_mange_edit_load_openapi(kofic_code_);
				kofic_find_modal_.modal("hide");
				return false;
			});
				
		});

		if(movie_list_.length <= 0){
			kofic_find_results_list_.append("<div class='message no-results'>검색된 영화가 없습니다.</div>");
		}


		
	});
}
function _pb_movie_open_mange_edit_load_openapi(kofic_code_){
	$ = jQuery;
	var edit_form_ = $("#pb-templated-edit-edit-form");

	PB.post({
		action : "movie-open-manage-load-openapi",
		kofic_code : kofic_code_
	}, function(result_, response_json_){

		var movie_info_ = response_json_.movie_info;
		$.each(movie_info_ , function(key_, value_){
			edit_form_.find(":input[name='"+key_+"']").val(value_);
		});
	}, true);
}

function _pb_movie_open_mange_open_publisher_add_popup(){
	$ = jQuery;
	var publisher_add_modal_ = $("#pb-publisher-reg-modal");
	var publisher_add_modal_form_ = publisher_add_modal_.find("form");
		publisher_add_modal_form_[0].reset();

	publisher_add_modal_.modal("show");
}
function pb_movie_open_mange_open_publisher_do_add(){
	PB.confirm_q({
		title : "등록확인",
		content : "해당정보로 배급사를 등록하시겠습니까?",
		button1 : "등록하기"
	}, function(c_){
		if(!c_) return;

		_pb_movie_open_mange_open_publisher_do_add();
	});
}

function _pb_movie_open_mange_open_publisher_do_add(){
	$ = jQuery;
	var publisher_add_modal_ = $("#pb-publisher-reg-modal");
	var publisher_add_modal_form_ = publisher_add_modal_.find("form");
	var publisher_data_ = publisher_add_modal_form_.serialize_object();

	PB.post({
		'action' : "movie-open-add-publisher",
		'publisher_data' : publisher_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배급사 등록 중, 에러가 발생했습니다',
			});
		}

		var publisher_id_el_ = $("#pb-movie-open-edit-form-publisher_id");
			publisher_id_el_.append("<option value='"+response_json_.client_id+"'>"+response_json_.client_name+"</option>");

		publisher_id_el_.sval(response_json_.client_id);

		publisher_add_modal_.modal("hide");

	}, true);
}