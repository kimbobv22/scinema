jQuery(document).ready(function($){
	var popup_form_ = $("#pb-banner-remove-popup-form");
	popup_form_.submit_handler(pb_banner_manage_bulk_delete);
});

function _pb_banner_check_remove(id_){
	$ = jQuery;
	var popup_ = $("#pb-banner-remove-popup");
	var popup_form_ = $("#pb-banner-remove-popup-form");

	var banner_data_ = JSON.parse($("[data-banner-data-id='"+id_+"']").val());
	
	popup_form_.find("[name='banner_title']").val(banner_data_['banner_title']);
	popup_form_.find("[name='banner_image_url']").attr("src",(banner_data_['banner_image_url']));

	_pb_banner_remove_cinema_list(banner_data_['banner_title'],banner_data_['banner_image_url']);

	//popup_.modal("show");
}

function _pb_banner_remove_cinema_list(title,url){
	$ = jQuery;
	var popup_ = $("#pb-banner-remove-popup");

	PB.post({
		action :  "adminpage-banner-bulk-cinema-list",
		banner_title : title,
		banner_image_url : url,
	}, function(result_, response_json_){
		var cinema_list_=response_json_.banner_cinema_list;
		$("#pb-banner-remove-popup-form-cinema-list").empty();
			for(var i=0; i<cinema_list_.length; i++){
				var check_html = 
						'<div class="box-group solid">' +
							'<input type="checkbox" name="cinema_id_list" id="checkbox_solid_border_' + i + '" value="' + cinema_list_[i]['ID'] + '"/>' +
							'<label for="checkbox_solid_border_' + i + '">'+ cinema_list_[i]['cinema_name'] + '</label>'+
						'</div>';
					$("#pb-banner-remove-popup-form-cinema-list").append(check_html);
				};
		popup_.modal("show");
		return;
	});
}

// 선택삭제 알럿
function pb_banner_manage_bulk_delete(){
	PB.confirm_q({
		title : "선택삭제",
		content : "선택한 영화관에 해당배너를 삭제하시겠습니까?",
		button1 : "삭제하기"
	}, function(c_){
		if(!c_) return;
		_pb_banner_manage_bulk_delete();
	});
}
// 선택삭제 기능 
function _pb_banner_manage_bulk_delete(){
	$ = jQuery;
	var banner_id_el_ = $("#pb-banner-remove-popup-form :input[name='cinema_id_list']:checked");
	var banner_ids_ = [];

	$.each(banner_id_el_, function(){
		banner_ids_.push($(this).val());
	});

	console.log(banner_ids_);
 
	PB.post({
		action :  "banner-bulk-delete",
		banner_ids : banner_ids_
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배너삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "배너삭제완료",
			content : "배너삭제가 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	},true);
}

//일괄삭제 알럿
function pb_banner_manage_bulk_all_delete(id_){
	PB.confirm_q({
		title : "일괄삭제",
		content : "해당배너가 등록되어 있는 모든 영화관 배너를 삭제하시겠습니까?",
		button1 : "일괄삭제하기"
	}, function(c_){
		if(!c_) return;
		_pb_banner_manage_bulk_all_delete(id_);
	});
}

// 일괄삭제 기능 
function _pb_banner_manage_bulk_all_delete(id_){
	$ = jQuery;

	var banner_data_ = JSON.parse($("[data-banner-data-id='"+id_+"']").val());

	PB.post({
		action :  "banner-bulk-all-delete",
		banner_title : banner_data_['banner_title'],
		banner_image_url : banner_data_['banner_image_url'],
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배너삭제 중 에러가 발생했습니다.',
			});
			return;
		}
		PB.alert_success({
			title : "배너삭제완료",
			content : "배너삭제가 완료되었습니다.",
		}, function(){
			document.location = document.location;
		})
	},true);
}
