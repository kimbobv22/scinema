jQuery(document).ready(function($){

	var edit_form_ = $("#pb-homepage-manage-form");

	edit_form_.submit_handler(_pb_home_manage_update);

});

function _pb_home_manage_update(){
	$ = jQuery;

	var edit_form_ = $("#pb-homepage-manage-form");
	var update_data_ = edit_form_.serialize_object();

	PB.post({
		action : "adminpage-homepage-manage-update",
		update_data : update_data_,
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : "에러발생",
				content:  "설정 저장 중 에러가 발생했습니다.",
			});
			return;
		}

		PB.alert_success({
			title : "저장완료",
		}, function(){
			document.location = document.location;
		});

	}, true);

}