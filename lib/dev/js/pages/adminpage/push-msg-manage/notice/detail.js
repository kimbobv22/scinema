jQuery(document).ready(function($){

	
});

function _pb_push_msg_manage_delete(msg_id_){

	$ = jQuery;

	PB.confirm_q({
		title : "삭제확인",
		content : "해당 공지사항을 삭제하시겠습니끼?",
		button1 : "삭제하기"
	}, function(c_){

		if(!c_) return;

		PB.post({
			action : "push-remove-message",
			msg_id : msg_id_
		}, function(result_, response_json_){

			if(!result_ || response_json_.success !== true){
				PB.alert_error({
					title : response_json_.error_title || '에러발생',
					content : response_json_.error_message || '처리 도중 에러가 발생했습니다.',
				});
				return;
			}

			PB.alert_success({
				title : '삭제완료',
				content : "정상적으로 처리되었습니다.",
			}, function(){
				document.location = $("#pb-push-msg-manage-edit-form").attr("data-redirect-url");
			});

		}, true);

	});
}