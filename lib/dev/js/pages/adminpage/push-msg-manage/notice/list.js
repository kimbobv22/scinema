jQuery(document).ready(function($){

	
});

function pb_push_msg_manage_resend(msg_id_){

	$ = jQuery;

	PB.confirm_q({
		title : "재발송확인",
		content : "해당 공지사항 알림을 재발송하시겠습니끼?<br/>발송된 알림은 취소할 수 없습니다.",
		button1 : "재발송하기"
	}, function(c_){

		if(!c_) return;

		PB.post({
			action : "push-resend-message",
			msg_id : msg_id_
		}, function(result_, response_json_){

			if(!result_ || response_json_.success !== true){
				PB.alert_error({
					title : response_json_.error_title || '에러발생',
					content : response_json_.error_message || '처리 도중 에러가 발생했습니다.',
				});
				return;
			}

			PB.alert_success({
				title : '재발송완료',
				content : "정상적으로 처리되었습니다.",
			});

		}, true);

	});
}