jQuery(document).ready(function($){
	
	var edit_form_ = $("#pb-push-msg-manage-edit-form");

	var feature_image_url_el_ = $("#pb-push-msg-manage-edit-form-feature_image_url");
	var feature_image_url_ = feature_image_url_el_.attr("data-default-value");

	var module_ = feature_image_url_el_.pb_multiple_image_uploader({
		'maxlength' : 1,
		'modal_label' : "대표서이미지 업로드",

	});

	if(feature_image_url_ !== null && feature_image_url_ !== ""){
		module_.add({
			url : feature_image_url_,
			thumbnail_url : feature_image_url_,
			name : feature_image_url_,
			type : "jpg",
		});	
	}
	

	edit_form_.validator();
	edit_form_.submit_handler(pb_push_msg_manage_edit_submit);

	
});


function pb_push_msg_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-push-msg-manage-edit-form");

	PB.confirm_q({
		title : "등록확인",
		content : "해당 공지사항을 등록하시겠습니까?<br/>등록 시, 알림은 즉시 발송되며, 발송된 알림은 되돌릴 수 없습니다.",
		button1 : "등록하기"
	}, function(c_){
		if(!c_) return;
		_pb_cinema_manage_edit_submit();
	});
}

function _pb_cinema_manage_edit_submit(){
	$ = jQuery;
	var submit_form_ = $("#pb-push-msg-manage-edit-form");
	var form_data_ = submit_form_.serialize_object();

	PB.post({
		action : "push-send-message",
		form_data : form_data_
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '전송 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "등록완료",
			content : "공지사항이 정상적으로 등록되었습니다.",
		}, function(){
			document.location = PB.append_url($("#pb-push-msg-manage-edit-form").attr("data-redirect-url"), "detail/"+response_json_.msg_id);
		});

	}, true);
}