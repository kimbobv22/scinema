jQuery(document).ready(function($){
	
	var edit_form_ = $("#pb-push-msg-manage-edit-form");

	var screen_preview_item_ = $("#pb-movie-screen-item-preview");

	window._pb_update_screen_movie_preview = function(){
		var option_data_ = JSON.parse(edit_form_.find("[name='default_screen_data']").val());

		screen_preview_item_.toggleClass("hidden",!option_data_);

		if(!option_data_) return;

		$.each(option_data_, function(key_, value_){
			screen_preview_item_.find("[data-column='"+key_+"']").text(value_);
		});

		$.each(option_data_, function(key_, value_){
			screen_preview_item_.find("[data-image='"+key_+"']").attr("src", value_);
		});
	}
	

	edit_form_.validator();
	edit_form_.submit_handler(pb_push_msg_manage_edit_submit);

	_pb_update_screen_movie_preview();

});


function pb_push_msg_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-push-msg-manage-edit-form");

	PB.confirm_q({
		title : "등록확인",
		content : "해당 신작알림을 등록하시겠습니까?<br/>등록 시, 알림은 즉시 발송되며, 발송된 알림은 되돌릴 수 없습니다.",
		button1 : "등록하기"
	}, function(c_){
		if(!c_) return;
		_pb_cinema_manage_edit_submit();
	});
}

function _pb_cinema_manage_edit_submit(){
	$ = jQuery;
	var submit_form_ = $("#pb-push-msg-manage-edit-form");
	var form_data_ = submit_form_.serialize_object();

	PB.post({
		action : "push-send-message",
		form_data : form_data_
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '전송 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "등록완료",
			content : "공지사항이 정상적으로 등록되었습니다.",
		}, function(){
			document.location = PB.append_url($("#pb-push-msg-manage-edit-form").attr("data-redirect-url"), "detail/"+response_json_.msg_id);
		});

	}, true);
}


function pb_push_msg_new_movie_find_screen_id(){
	$ = jQuery;

	var edit_form_ = $("#pb-push-msg-manage-edit-form");

	var cinema_id_el_ = edit_form_.find("[name='cinema_id']");
	var cinema_id_ = null;

	if(cinema_id_el_.attr("type") !== "hidden"){
		var cinema_id_ = cinema_id_el_.sval();
	}else{
		var cinema_id_ = cinema_id_el_.val();
	}


	if(!cinema_id_ || cinema_id_ === ""){
		PB.alert_info({
			title : "영화관선택확인",
			content : "상영영화선택 전, 영화관을 선택하세요",
		});
		return;
	}

	var screen_id_modal_ = $("#pb-find-movie-screen-id-modal");
	var screen_id_modal_form_ = screen_id_modal_.find("form");
		screen_id_modal_form_[0].reset();

	screen_id_modal_form_.find(":input[name='cinema_id']").val(cinema_id_);

	$("#pb-movie-screen-id-table").htajaxlistgrid().page_index(0);

	screen_id_modal_.modal("show");
	
}

function _pb_push_msg_new_movie_select_screen_id(screen_id_){
	$ = jQuery;

	var screen_data_str_ = $("[data-movie-screen-data-id='"+screen_id_+"']").val();
	var screen_data_ = JSON.parse(screen_data_str_);

	var edit_form_ = $("#pb-push-msg-manage-edit-form");
		edit_form_.find("[name='default_screen_data']").val($("[data-movie-screen-data-id='"+screen_id_+"']").val());

	edit_form_.find("[name='movie_screen_id']").val(screen_id_);
	edit_form_.find("[name='screen_id_name']").val(screen_data_.movie_name);
	
	$("#pb-find-movie-screen-id-modal").modal("hide");
	
	_pb_update_screen_movie_preview();	
}
