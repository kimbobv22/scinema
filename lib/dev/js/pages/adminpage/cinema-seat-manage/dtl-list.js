function pb_cinema_seat_manage_delete(seat_id_){
	PB.confirm_q({
		title : "삭제확인",
		content : "해당 좌석배치도를 삭제하시겠습니까?",
		button1 : "삭제하기"
	}, function(c_){
		if(!c_) return;
		_pb_cinema_seat_manage_delete(seat_id_);
	});
}

function _pb_cinema_seat_manage_delete(seat_id_){
	$ = jQuery;
	
	PB.post({
		action : "cinema-seat-delete",
		seat_id : seat_id_	
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '좌석배치도 삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = response_json_.redirect_url;
		});
	}, true);
}