jQuery(function($){

	var _pb_cinema_seat_types = {
		'00001' : "일반",
		'00003' : "라벨",
		'00005' : "여백",
		'00007' : "장애인",
		'00009' : "커플석",

	};

	function _pb_cinema_seat_gen_seat_id(){
		return PB.random_string(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
	}

	function _pb_cinema_seat_make_seat_item(data_){

		data_ = $.extend({
			id : _pb_cinema_seat_gen_seat_id(),
			name : "",
			type : "00001",
		}, data_);
		

		var item_html_ = '<div class="seattable-col">' +

			'<div class="seat-item seat-type-'+data_['type']+'">'+
				'<input type="hidden" value="'+data_['id']+'" data-seat-item="seat_id">'+
				'<input type="text" value="'+data_['name']+'" class="seat-name" placeholder="-" data-seat-item="seat_name">'+
				'<select class="form-control form-control-sm seat-type" data-seat-item="seat_type">';

			$.each(_pb_cinema_seat_types, function(key_, name_){
				item_html_ += '<option value="'+key_+'" '+(key_ === data_['type'] ? "selected" : "")+'>'+name_+'</option>';
			});

			item_html_ += '</select>' +				

			'</div>' +


		'</div>';
		var item_el_ = $(item_html_);

		var seat_type_el_ = item_el_.find(".seat-type");

		seat_type_el_.change(function(){
			var target_el_ = $(this);
			var target_type_ = target_el_.sval();

			var target_parent_el_ = target_el_.parent();
				target_parent_el_.attr("class", "seat-item seat-type-"+target_type_);
		});

		var seat_name_el_ = item_el_.find(".seat-name");

		seat_name_el_.focus(function(){
			$(this).parent().toggleClass("active", true);
		}).blur(function(){
			$(this).parent().toggleClass("active", false);
		});

		seat_name_el_.keydown(function(event_){

			var add_row_index_ = 0;
			var add_column_index_ = 0;

			if(event_.shiftKey || event_.ctrlKey || event_.altKey) return;

			switch(event_.keyCode){
				case 37 :  // left

					if(this.selectionStart > 0) return;

					--add_column_index_;
					break;
				case 38 :  //up
					--add_row_index_;
					break;
				case 39 :  //right
					if(this.selectionStart < $(this).val().length ) return;

					++add_column_index_;
					break;

				case 40: //down
					++add_row_index_;
					break;

			}

			if(add_row_index_ != 0 || add_column_index_ != 0){
				var current_seat_item_ = $(this).parent();
				var current_seat_col_el_ = current_seat_item_.parent();
				var current_seat_row_el_ = current_seat_col_el_.parent();
				var row_items_ = current_seat_row_el_.parent().children();
				var column_count_ = current_seat_row_el_.children().length;
				
				var current_row_index_ = row_items_.index(current_seat_row_el_);
				var current_column_index_ = current_seat_row_el_.children().index(current_seat_col_el_);

				current_row_index_ += add_row_index_;
				current_column_index_ += add_column_index_;

				if(current_row_index_ < 0){
					current_row_index_ = row_items_.length - 1;
				}

				if(current_row_index_ >= row_items_.length){
					current_row_index_ = 0;
				}

				if(current_column_index_ < 0){
					current_column_index_ = column_count_ - 1;
				}

				if(current_column_index_ >= column_count_){
					current_column_index_ = 0;
				}


				var target_row_item_ = row_items_.eq(current_row_index_);
				target_row_item_.children().eq(current_column_index_).find(".seat-name").focus();
			}
		});


		return item_el_;
	}


	var _pb_cinema_seat_manager = (function(target_, options_){
		this._target = $(target_);
		this._options = $.extend({
			// 'screen_margin' : {left : 0, right: 0};
		}, options_);

		this._screen_frame = this._target.find(".screen-frame");
		this._screen_el = this._screen_frame.find(".screen");
		this._seat_item_row_list = this._target.find(".seattable-row-list");

		this.screen_margin({left : 0, right : 0});

		this._target.data("pb-cinema-seat-manager-module", this);
	});

	_pb_cinema_seat_manager.prototype.target = (function(){
		return this._target;
	});

	_pb_cinema_seat_manager.prototype.options = (function(options_){
		if(options_ !== undefined){
			this._options = $.extend(true, this._options, options_);
		}

		return this._options;
	});

	_pb_cinema_seat_manager.prototype.column_count = (function(){
		if(this._seat_item_row_list.length <= 0) return 0;
		return this._seat_item_row_list.children().eq(0).children().length;
	});
	_pb_cinema_seat_manager.prototype.row_count = (function(){
		return this._seat_item_row_list.children().length;
	});

	_pb_cinema_seat_manager.prototype.update_layout = (function(row_, column_){
		var row_count_ = this.row_count();
		var column_count_ = this.column_count();

		if(row_count_ > row_){
			this._seat_item_row_list.children().slice(row_).remove();			
		}else{
			var add_row_count_ = row_ - row_count_;

			for(var row_index_ = 0; row_index_< add_row_count_; ++row_index_){
				var row_item_ = $('<div class="seattable-row"></div>');

				for(var col_index_=0;col_index_<column_count_; ++col_index_){
					row_item_.append(_pb_cinema_seat_make_seat_item());
				}

				this._seat_item_row_list.append(row_item_);
			}
		}

		var row_items_ = this._seat_item_row_list.children();

		if(row_items_.length <= 0) return;

		var add_column_count_ = column_ - column_count_;

		row_items_.each(function(){
			var row_item_ = $(this);

			if(column_count_ > column_){
				row_item_.children().slice(column_).remove();
			}else{
				for(var col_index_=0;col_index_<add_column_count_; ++col_index_){
					row_item_.append(_pb_cinema_seat_make_seat_item());
				}
			}

		});
		this.screen_margin(this.screen_margin());
	});

	_pb_cinema_seat_manager.prototype.screen_margin = (function(margin_){
		var column_count_ = this.column_count();

		if(margin_ !== undefined){
			this._screen_el.css({
				'marginLeft' : parseInt((margin_.left / column_count_) * 100)+"%",
				'marginRight' : parseInt((margin_.right / column_count_) * 100)+"%",
			});

			this._screen_el.data("screen-margin", margin_);
			this.update_screen();
		}

		return this._screen_el.data("screen-margin");
	});	

	_pb_cinema_seat_manager.prototype.update_screen = (function(){

		var row_items_ = this._seat_item_row_list.children();

		if(row_items_.length <= 0){
			this._screen_frame.css({
				width : 0,
			});	
		}else{
			this._screen_frame.css({
				width : row_items_.eq(0).width()
			});	
		}
	
	});

	_pb_cinema_seat_manager.prototype.to_json = (function(){

		var screen_margin_ = this.screen_margin();
		var row_count_ = this.row_count();
		var column_count_ = this.column_count();

		var seattable_ = [];

		var row_items_ = this._seat_item_row_list.children();

		if(row_count_ > 0 && column_count_ > 0){

			for(var row_index_ = 0; row_index_ < row_count_; ++row_index_){
				var row_item_ = row_items_.eq(row_index_);
				var col_items_ = row_item_.children();

				var row_data_ = [];

				for(var col_index_ = 0; col_index_ < column_count_; ++col_index_){
					var col_item_ = col_items_.eq(col_index_);

					var seat_id_ = col_item_.find("[data-seat-item='seat_id']").val();
					var seat_name_ = col_item_.find("[data-seat-item='seat_name']").val();
					var seat_type_ = col_item_.find("[data-seat-item='seat_type']").sval();

					row_data_.push({
						seat_id : seat_id_,
						seat_name : seat_name_,
						seat_type : seat_type_,
					});

				}

				seattable_.push(row_data_);


			}

		}


		return {
			screen : {
				margin : screen_margin_
			},
			schema : {row : row_count_, column: column_count_},
			seattable : seattable_,
		};
	});
	_pb_cinema_seat_manager.prototype.apply_json = (function(json_){
		var screen_info_ = json_['screen'];
		var screen_margin_ = screen_info_.margin;
		var schema_ = json_.schema;
		var seattable_ = json_.seattable;

		this._seat_item_row_list.empty();

		this.update_layout(schema_.row, schema_.column);

		if(schema_.row > 0 && schema_.column > 0){

			var row_items_ = this._seat_item_row_list.children();

			for(var row_index_ = 0; row_index_ < schema_.row; ++row_index_){
				var row_data_ = (seattable_[row_index_] ? seattable_[row_index_] : null);
				var row_item_ = row_items_.eq(row_index_);

				if(!row_data_) continue;

				var col_items_ = row_item_.children();

				for(var col_index_ = 0; col_index_ < schema_.column; ++col_index_){
					var col_data_ = (row_data_[col_index_] ? row_data_[col_index_] : null);
					var col_item_ = col_items_.eq(col_index_);

					if(!col_data_) continue;

					$target_seat_id_ = col_data_['seat_id'];
					if(!$target_seat_id_ || $target_seat_id_ === ""){
						$target_seat_id_ = _pb_cinema_seat_gen_seat_id();
					}

					col_item_.find("[data-seat-item='seat_id']").val($target_seat_id_);
					col_item_.find("[data-seat-item='seat_name']").val(col_data_['seat_name']);
					col_item_.find("[data-seat-item='seat_type']").sval(col_data_['seat_type']);
					col_item_.find("[data-seat-item='seat_type']").change();
				}
			}
		}

		this.screen_margin(screen_margin_);
	});


	$.fn.pb_cinema_seat_manager = (function(options_){
		var module_ = this.data('pb-cinema-seat-manager-module');
		if(module_) return module_;
		return new _pb_cinema_seat_manager(this, options_);
	});

});


jQuery(document).ready(function($){
	var seattable_el_ = $("#pb-seattable-manager");
	var seattable_module_ = seattable_el_.pb_cinema_seat_manager();

	var edit_form_ = $("#pb-seattable-form");

		edit_form_.validator();

		edit_form_.submit_handler(pb_cinema_seat_manage_edit_submit);

	var seattable_row_el_ = edit_form_.find("[name='seattable_row']");
	var seattable_column_el_ = edit_form_.find("[name='seattable_column']");
	var seattable_screen_margin_left_el_ = edit_form_.find("[name='seattable_screen_margin_left']");
	var seattable_screen_margin_right_el_ = edit_form_.find("[name='seattable_screen_margin_right']");


	edit_form_.find("[data-seattable-schema-field]").change(function(){
		// var target_input_ = $(this);

		seattable_module_.update_layout(
			parseInt(seattable_row_el_.val()),
			parseInt(seattable_column_el_.val())
		);
	});

	edit_form_.find("[data-seattable-screen-margin-field]").change(function(){
		seattable_module_.screen_margin({
			left : parseInt(seattable_screen_margin_left_el_.val()),
			right : parseInt(seattable_screen_margin_right_el_.val()),
		});
	});

	seattable_module_.apply_json(_pb_cinema_seattable_default_data);

	var replace_image_url_frame = $("[data-replace-image-group]");
	var replace_image_url_ = $("#pb-cinema-seat-manage-form-replace_image_url");
	var replace_image_url_module_ = replace_image_url_.pb_multiple_image_uploader({
		maxlength : 1,
	});


	var replace_image_url_default_value_ = replace_image_url_.attr("data-default-value");

	if(replace_image_url_default_value_ !== null && replace_image_url_default_value_ !== ""){
		replace_image_url_module_.add({
			url : replace_image_url_default_value_,
			thumbnail_url : replace_image_url_default_value_,
			name : replace_image_url_default_value_,
			type : "jpg",
		});
	}

	var replace_image_yn_change_callback_ = function(){
		replace_image_url_frame.toggleClass("hidden", !$("#pb-cinema-seat-manage-form-replace_image_yn").prop("checked"));
	}

	$("#pb-cinema-seat-manage-form-replace_image_yn").click(function(){
		replace_image_yn_change_callback_();
		// return false;
	});

	replace_image_yn_change_callback_();
});

function pb_cinema_seat_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-seattable-form");

	if(edit_form_.find("[name='seat_id']").val() === ""){
		PB.confirm_q({
			title : "좌석배치도추가",
			content : "해당 좌석배치도를 추가하시겠습니끼?",
			button1 : "추가하기"
		}, function(c_){
			if(!c_) return;
			_pb_cinema_seat_manage_edit_submit();
		});
	}else{
		_pb_cinema_seat_manage_edit_submit();
	}
}

function _pb_cinema_seat_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-seattable-form");
	var cinema_seat_data_ = edit_form_.serialize_object();

	var seattable_el_ = $("#pb-seattable-manager");
	var seattable_module_ = seattable_el_.pb_cinema_seat_manager();

	cinema_seat_data_['seat_data'] = seattable_module_.to_json();

	var replace_image_url_el_ = edit_form_.find("#pb-cinema-seat-manage-form-replace_image_url");
	var replace_image_url_json_ = replace_image_url_el_.pb_multiple_image_uploader().to_json();

	if(replace_image_url_json_.length > 0){
		cinema_seat_data_['replace_image_url'] = replace_image_url_json_[0]['url'];	
	}else{
		cinema_seat_data_['replace_image_url'] = null;
	}

	PB.post({
		action : "cinema-seat-update",
		cinema_seat_data : cinema_seat_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '좌석배치도 추가,수정 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = response_json_.redirect_url;
		});

	}, true);
}
function pb_cinema_seat_manage_delete(){
	PB.confirm_q({
		title : "삭제확인",
		content : "해당 좌석배치도를 삭제하시겠습니까?",
		button1 : "삭제하기"
	}, function(c_){
		if(!c_) return;
		_pb_cinema_seat_manage_delete();
	});
}

function _pb_cinema_seat_manage_delete(){
	$ = jQuery;
	var edit_form_ = $("#pb-seattable-form");
	var cinema_seat_data_ = edit_form_.serialize_object();

	PB.post({
		action : "cinema-seat-delete",
		seat_id : cinema_seat_data_.seat_id
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '좌석배치도 삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = response_json_.redirect_url;
		});
	}, true);
}