jQuery(document).ready(function($){
	var target_form_ = $("#pb-movie-screen-bulk-write-form");

	target_form_.find("#pb-movie-screen-edit-form-cinema_id").change(function(){

		var target_cinema_id_ = $(this).sval();

		

		var open_id_selects_ = target_form_.find("[data-column-name='open_id']");
		var xticket_ref_code_selects = target_form_.find("[data-column-name='xticket_ref_code']");

		open_id_selects_.empty();
		open_id_selects_.append("<option value=''>-개봉작품선택-</option>");

		xticket_ref_code_selects.empty();
		xticket_ref_code_selects.append("<option value=''>-연동코드선택-</option>");

		if(target_cinema_id_ === "") return;


		PB.post({
			action : "adminpage-movie-screen-bulk-upload-load-subdata",
			cinema_id : target_cinema_id_,
		}, function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				PB.alert_error({
					title : "에러발생",
					content : "데이타를 불러오는 중 에러가 발생했습니다."
				});
				return;
			}

			$.each(response_json_.open_list, function(){

				if(this['disabled'] === "Y"){
					open_id_selects_.append("<option value='"+this['ID']+"' disabled>"+this['movie_name']+"(등록됨)</option>");
				}else{
					open_id_selects_.append("<option value='"+this['ID']+"'>"+this['movie_name']+"</option>");	
				}
				
			});

			xticket_ref_code_selects.append("<option value='0'>*선택하지 않음</option>");

			$.each(response_json_.xticket_ref_list, function(){
				xticket_ref_code_selects.append("<option value='"+this['id']+"'>"+this['name']+"</option>");
			});

		}, true);

	});
	target_form_.find("#pb-movie-screen-edit-form-cinema_id").change();

	var datepickers_ = target_form_.find("[data-datepicker]");

	$.each(datepickers_, function(){
		var datepicker_ = $(this);
		datepicker_.datetimepicker({
			locale : datepicker_.attr("data-locale"),
			format : datepicker_.attr("data-format")
		});
	});

	target_form_.submit_handler(function(){

		var form_table_ = $("#pb-movie-screen-bulk-write-form-table");
		var form_list_ = form_table_.find("tbody > tr");

		var target_data_ = [];

		form_list_.each(function(){

			var target_row_data_ = {};
			var target_row_ = $(this);
			var target_col_list_ = target_row_.find("[data-column-name]");

			if(target_row_.find("[data-column-name='open_id']").sval() === "") return true;

			$.each(target_col_list_ , function(){
				var target_input_ = $(this);

				var column_name_ = target_input_.attr("data-column-name");
				var column_value_ = target_input_.val();

				// if(column_value_ === "") return true;

				target_row_data_[column_name_] = column_value_;

			});

			target_data_.push(target_row_data_);


		});



		PB.confirm_q({
			title : "일괄등록확인",
			content : "입력한 내용을 일괄등록하시겠습니까?",
			button1  : "일괄등록하기"
		}, function(c_){
			if(!c_) return false;

			_pb_adminpage_screen_bulk_write(target_form_.find("#pb-movie-screen-edit-form-cinema_id").sval(), target_data_);

		});

	});
});

function _pb_adminpage_screen_bulk_write(cinema_id_, target_data_){

	PB.post({
		action : "adminpage-movie-screen-bulk-upload",
		cinema_id : cinema_id_,
		target_data:  target_data_
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '일괄등록 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "등록성공",
			content : "상영작품 일괄등록이 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	}, true);

}