jQuery(document).ready(function($){
	var hiden_yns_ = $("[data-banner-hide-yn]");

    hiden_yns_.each(function(){
		$(this).click(function(){
			pb_switch_hide_yn($(this).attr("data-banner-hide-yn"), ($(this).prop("checked") ? "Y" : "N"));
		});
	});
});

function pb_switch_hide_yn(banner_id_, yn_){
	$ = jQuery;

	var hide_yn_ = $("[data-banner-hide-yn='"+banner_id_+"']");

	hide_yn_.prop("disabled", true);

	PB.post({
		action : "banner-main-switch-hide-yn",
		banner_main_id : banner_id_,
		hide_yn :yn_
	}, $.proxy(function(result_, response_json_){
        this.prop("disabled", false);	
        console.log(result_);
	}, hide_yn_), false);
} 