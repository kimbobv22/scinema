jQuery(document).ready(function($){
    var edit_form_ = $("#pb-banner-main-manage-edit-form");

	edit_form_.validator();
	edit_form_.submit_handler(_pb_banner_main_manage_update);

    var image_url_hidden_ = $("#pb-banner-main-manage-edit-form-banner_image_url");
    var module_ = image_url_hidden_.pb_multiple_image_uploader({
		'maxlength' : 1,
		'modal_label' : "배너이미지 업로드",
		'use_thumbnail' : false,
	});
	if(edit_form_.data('add-new') === "N"){
		module_.apply_json([{
			'url' : image_url_hidden_.data('image-url'),
			'name' : image_url_hidden_.data('image-url'),
			'type' : "",
		}]);
	}

    var color_pickers_ = edit_form_.find(".color-picker");
	color_pickers_.each(function(){
		$(this).minicolors();
    });

    $("#pb-banner-main-manage-edit-form-srt_date").datetimepicker({
		format : 'YYYY-MM-DD HH:mm'
	});

	$("#pb-banner-main-manage-edit-form-end_date").datetimepicker({
		format : 'YYYY-MM-DD HH:mm'
	});
});

function _pb_banner_main_manage_update(){
	$ = jQuery;
	var edit_form_ = $("#pb-banner-main-manage-edit-form");

	if(edit_form_.data('add-new') === "Y"){
		PB.confirm_q({
			title : "배너추가",
			content : "메인 배너를 추가하시겠습니끼?",
			button1 : "추가하기"
		}, function(c_){
			if(!c_) return;
			_pb_banner_main_manage_edit_submit();
		});
	}else{
		_pb_banner_main_manage_edit_submit();
	} 
}

function _pb_banner_main_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-banner-main-manage-edit-form");
	var banner_data_ = edit_form_.serialize_object();

	var wp_editor_ = $("#pb-banner-manage-edit-form-banner_html");
	var wp_editor_instance_ = tinyMCE.get(wp_editor_.attr("id"));
	if(wp_editor_instance_ && !wp_editor_.is(':visible')){
		banner_data_['banner_html'] = wp_editor_instance_.getContent();
	}else{
		banner_data_['banner_html'] = wp_editor_.val();
	}

	var image_url_hidden_ = $("#pb-banner-main-manage-edit-form-banner_image_url");
	var image_module_ = image_url_hidden_.pb_multiple_image_uploader();
	var image_urls_ = image_module_.to_json();
	banner_data_['image_url'] = image_urls_[0].url;

	console.log(banner_data_);

	PB.post({
		action : "banner-main-update",
		banner_data : banner_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배너 추가,수정 중 에러가 발생했습니다.',
			});
			return;
		}
		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = response_json_.redirect_url;
		});
	}, true);
}

function _pb_banner_main_manage_delete(){
	PB.confirm_q({
		title : "배너삭제확인",
		content : "현재 메인배너를 삭제하시겠습니까?",
		button1 : '삭제하기',
		button1_classes : "btn btn-black",
	}, function(c_){
		if(!c_) return;
		_pb_banner_main_manage_do_delete();
	});
}

function _pb_banner_main_manage_do_delete(){
	$ = jQuery;
	var edit_form_ = $("#pb-banner-main-manage-edit-form");

	PB.post({
		action : "banner-main-delete",
		banner_main_id : edit_form_.find(":input[name='ID']").val()
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '배너삭제 중 에러가 발생했습니다.',
			});
			return;
		}
		PB.alert_success({
			title : "배너삭제완료",
			content : "배너가 삭제 되었습니다.",
		}, function(){
			document.location = response_json_.redirect_url;
		});
	},true);
}