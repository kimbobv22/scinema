jQuery(document).ready(function($){
	
	var edit_form_ = $("#pb-cinema-manage-edit-form");

	var logo_image_url_el_ = $("#pb-cinema-manage-edit-form-logo_image_url");
	var logo_image_url_ = logo_image_url_el_.attr("data-default-value");

	var module_ = logo_image_url_el_.pb_multiple_image_uploader({
		'maxlength' : 1,
		'modal_label' : "로고이미지 업로드",

	});

	if(logo_image_url_ !== null && logo_image_url_ !== ""){
		module_.add({
			url : logo_image_url_,
			thumbnail_url : logo_image_url_,
			name : logo_image_url_,
			type : "jpg",
		});	
	}

	

	var seattable_el_ = $("#pb-cinema-manage-edit-form-seattable");
	var seattable_data_ = seattable_el_.attr("data-default-value");

	var seattable_module_ = seattable_el_.pb_multiple_image_uploader({
		'maxlength' : 5,
		'modal_label' : "로고이미지 업로드",
	});

	edit_form_.validator();
	edit_form_.submit_handler(pb_cinema_manage_edit_submit);


	edit_form_.find("#pb-cinema-manage-edit-form-addr_postcode").focus(_pb_cinema_manage_edit_find_postcode);
	edit_form_.find("#pb-cinema-manage-edit-form-addr").focus(_pb_cinema_manage_edit_find_postcode);
	
	//20190408 nhn (네이버지도 다음지도로 교체) 기존소스
	// var google_maps_ = $("#pb-cinema-maps");

	// var google_maps_module_ = new naver.maps.Map('pb-cinema-maps');
	// var latlng_ = new naver.maps.LatLng(parseFloat(edit_form_.find("[name='addr_lat']").val()), parseFloat(edit_form_.find("[name='addr_lng']").val()));


	// google_maps_module_.setCenter(latlng_);

	// var marker_ = new naver.maps.Marker({
  //       position: latlng_,
  //       map: google_maps_module_,
  //       // draggable : true
  //   });

  //   naver.maps.Event.addListener(google_maps_module_, 'click', function(e) {
	// 	marker_.setPosition(e.coord);


	// 	edit_form_.find("[name='addr_lat']").val(e.coord.y);
	// 	edit_form_.find("[name='addr_lng']").val(e.coord.x);
	// });

	// google_maps_.data('maps-module', google_maps_module_);
	// google_maps_.data('maps-marker', marker_);

	// _pb_cinema_manage_edit_update_map_marker();

	// 다음지도 변경 소스
	var daum_maps_ = $("#pb-cinema-maps");
	var default_latlng_ = new daum.maps.LatLng(edit_form_.find("[name='addr_lat']").val(), edit_form_.find("[name='addr_lng']").val());

	var daum_maps_module_ = new daum.maps.Map(daum_maps_[0], {
		center: default_latlng_,
		zoomable : false,
		level: 3,
	});
	
	var marker_ = new daum.maps.Marker({
		position: default_latlng_,
		map: daum_maps_module_
	});

	daum.maps.event.addListener(daum_maps_module_, 'click', function(mouseEvent) {        
    	// 클릭한 위도, 경도 정보를 가져옵니다 
		var latlng = mouseEvent.latLng; 
		// 마커 위치를 클릭한 위치로 옮깁니다
		marker_.setPosition(latlng);

		edit_form_.find("[name='addr_lat']").val(latlng.getLat());
		edit_form_.find("[name='addr_lng']").val(latlng.getLng());
	});
	
	daum_maps_.data('maps-module', daum_maps_module_);
	daum_maps_.data('maps-marker', marker_);

	//daum.maps.event.addListener(daum_maps_module_, 'center_changed', _pb_cinema_manage_edit_update_map_marker);
	_pb_cinema_manage_edit_update_map_marker();
		
	$("#pb-cinema-manage-edit-form-add-image-btn").pb_image_uploader_btn({
		callback : function(files_){
			if(files_.length <= 0) return;

			var target_editor_ = tinyMCE.get("pb-cinema-manage-edit-form-cinema_desc");

			$.each(files_, function(){
				target_editor_.execCommand('mceInsertRawHTML', false, '<img src="'+this['url']+'">');
			});
		}
	});


	$("#pb-cinema-manage-edit-form-add-image-btn-for-location").pb_image_uploader_btn({
		callback : function(files_){
			if(files_.length <= 0) return;

			var target_editor_ = tinyMCE.get("pb-cinema-manage-edit-form-replace_location_html");

			$.each(files_, function(){
				target_editor_.execCommand('mceInsertRawHTML', false, '<img src="'+this['url']+'">');
			});
		}
	});

	var replace_location_html_yn_change_callback_ = function(){
		$("[data-replace-location-html-group]").toggleClass("hidden", !$("#pb-cinema-manage-edit-form-replace_location_html_yn").prop("checked"));
	}

	$("#pb-cinema-manage-edit-form-replace_location_html_yn").click(function(){
		replace_location_html_yn_change_callback_();
		// return false;
	});

	replace_location_html_yn_change_callback_();

	$("[data-delete-btn]").click(function(){

		PB.confirm_q({
			title : "삭제확인",
			content : "해당영화관을 삭제처리하시겠습니까?",
			button1 : "삭제하기"
		}, function(c_){
			if(!c_) return;

			_pb_cinema_manage_edit_delete();

		});
	})
});

function _pb_cinema_manage_edit_update_map_marker(){
	$ = jQuery;
	var edit_form_ = $("#pb-cinema-manage-edit-form");
	var google_maps_ = $("#pb-cinema-maps");
	var google_maps_module_ = google_maps_.data('maps-module');
	var marker_ = google_maps_.data('maps-marker');

	var latlng_ = google_maps_module_.getCenter();

	marker_.setPosition(latlng_);
	
	// google_maps_module_.relayout();

	// edit_form_.find("[name='addr_lat']").val(latlng_.lat());
	// edit_form_.find("[name='addr_lng']").val(latlng_.lng());
	edit_form_.find("[name='addr_lat']").val(latlng_.getLat());
	edit_form_.find("[name='addr_lng']").val(latlng_.getLng());
}

function _pb_cinema_manage_edit_find_postcode(){
	$ = jQuery;
	var edit_form_ = $("#pb-cinema-manage-edit-form");

	edit_form_.find("#pb-cinema-manage-edit-form-addr_postcode").blur();
	edit_form_.find("#pb-cinema-manage-edit-form-addr").blur();

	new daum.Postcode({
		oncomplete: function(data_){
			// var address_ = (data_['autoJibunAddress'] || data_['jibunAddress']);
			var address_ = (data_['autoRoadAddress'] || data_['roadAddress']);
			edit_form_.find("#pb-cinema-manage-edit-form-addr_postcode").val(data_["zonecode"]);
			edit_form_.find("#pb-cinema-manage-edit-form-addr").val(address_);

			//20190408 nhn (네이버지도 다음지도로 교체) 기존소스
			// naver.maps.Service.geocode({ address: address_ }, function(status, response) {
			// 	if(status === naver.maps.Service.Status.ERROR) return;
			// 	var result_ = response.result;
			// 	var items_ = result_.items;
			// 	var coord_ = items_[0].point;
			// 	var google_maps_ = $("#pb-cinema-maps");
			// 	var google_maps_module_ = google_maps_.data('maps-module');
			// 		google_maps_module_.setCenter(coord_);
			// 	_pb_cinema_manage_edit_update_map_marker();
			// });

			var geocoder_ = new daum.maps.services.Geocoder();
			geocoder_.addressSearch(address_, function(result_, status_){
				var success_ = (status_ === daum.maps.services.Status.OK);
				edit_form_.find(".daum-map-frame").toggleClass("hidden", !success_);
				if(success_){
					var daum_maps_ = $("#pb-cinema-maps");
					var daum_maps_module_ = daum_maps_.data('maps-module');
						daum_maps_module_.setCenter(new daum.maps.LatLng(result_[0].y, result_[0].x));
					edit_form_.find("#pb-cinema-manage-edit-form-addr_dtl").focus();
				}
			});		
		}
	}).open();
}

function pb_cinema_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-cinema-manage-edit-form");

	if(edit_form_.data('add-new') === "Y"){
		PB.confirm_q({
			title : "상영관추가",
			content : "해당 상영관을 추가하시겠습니끼?",
			button1 : "추가하기"
		}, function(c_){
			if(!c_) return;
			_pb_cinema_manage_edit_submit();
		});
	}else{
		_pb_cinema_manage_edit_submit();
	}
}

function _pb_cinema_manage_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-cinema-manage-edit-form");
	var cinema_data_ = edit_form_.serialize_object();

	var logo_image_url_el_ = edit_form_.find("#pb-cinema-manage-edit-form-logo_image_url");
	var logo_image_url_json_ = logo_image_url_el_.pb_multiple_image_uploader().to_json();

	if(logo_image_url_json_.length > 0){
		cinema_data_['logo_image_url'] = logo_image_url_json_[0]['url'];	
	}else{
		cinema_data_['logo_image_url'] = null;
	}

	var seattable_el_ = edit_form_.find("#pb-cinema-manage-edit-form-seattable");
	var seattable_data_ = seattable_el_.pb_multiple_image_uploader().to_json();

	// cinema_data_['logo_image_url'] = logo_image_url_json_[0]['url'];
	
	var wp_editor_ = $("#pb-cinema-manage-edit-form-cinema_desc");
	var wp_editor_instance_ = tinyMCE.get(wp_editor_.attr("id"));


	if(wp_editor_instance_ && !wp_editor_.is(':visible')){
		cinema_data_[wp_editor_.attr("name")] = wp_editor_instance_.getContent();
	}else{
		cinema_data_[wp_editor_.attr("name")] = wp_editor_.val();
	}

	wp_editor_ = $("#pb-cinema-manage-edit-form-replace_location_html");
	wp_editor_instance_ = tinyMCE.get(wp_editor_.attr("id"));

	if(wp_editor_instance_ && !wp_editor_.is(':visible')){
		cinema_data_[wp_editor_.attr("name")] = wp_editor_instance_.getContent();
	}else{
		cinema_data_[wp_editor_.attr("name")] = wp_editor_.val();
	}


	PB.post({
		action : "cinema-update",
		cinema_data : cinema_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '상영관 추가,수정 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = response_json_.redirect_url;
		});

	}, true);
}

function _pb_cinema_manage_edit_delete(){
	$ = jQuery;
	var edit_form_ = $("#pb-cinema-manage-edit-form");
	var cinema_data_ = edit_form_.serialize_object();	

	PB.post({
		action : "cinema-delete",
		cinema_id : cinema_data_['cinema_id'],
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '상영관 추가,수정 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = response_json_.redirect_url;
		});

	}, true);

}