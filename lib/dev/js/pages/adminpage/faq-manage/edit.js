jQuery(document).ready(function($){

	var popup_form_ = $("#pb-faq-manage-edit-popup-form");

	popup_form_.validator();
	popup_form_.submit_handler(_pb_faq_manage_update);
});

function _pb_faq_manage_add(){
	$ = jQuery;
	var popup_ = $("#pb-faq-manage-edit-popup");
	var popup_form_ = $("#pb-faq-manage-edit-popup-form");
		popup_form_.toggleClass("edit-form", false);
		popup_form_.toggleClass("add-form", true);

	popup_form_.data('add-new', "Y");
	popup_form_[0].reset();

	popup_.modal("show");
}
function _pb_faq_manage_edit(id_){
	$ = jQuery;

	$ = jQuery;
	var popup_ = $("#pb-faq-manage-edit-popup");
	var popup_form_ = $("#pb-faq-manage-edit-popup-form");
		popup_form_.toggleClass("edit-form", true);
		popup_form_.toggleClass("add-form", false);

	popup_form_.data('add-new', "N");
	popup_form_[0].reset();

	var faq_data_ = JSON.parse($("[data-faq-data-id='"+id_+"']").val());
	
	popup_form_.find("[name='faq_id']").val(faq_data_['ID']);
	popup_form_.find("[name='faq_title']").val(faq_data_['faq_title']);
	popup_form_.find("[name='faq_desc']").val(faq_data_['faq_desc']);

	popup_.modal("show");
}

function _pb_faq_manage_update(){
	$ = jQuery;
	var popup_form_ = $("#pb-faq-manage-edit-popup-form");

	var image_url_hidden_ = $("#pb-faq-manage-edit-form-faq_image_url");

	if(popup_form_.data('add-new') === "Y"){
		PB.confirm_q({
			title : "FAQ추가",
			content : "FAQ를 추가하시겠습니끼?",
			button1 : "추가하기"
		}, function(c_){
			if(!c_) return;
			_pb_faq_manage_edit_submit();
		});
	}else{
		_pb_faq_manage_edit_submit();
	}
}

function _pb_faq_manage_edit_submit(){
	$ = jQuery;
	var popup_form_ = $("#pb-faq-manage-edit-popup-form");
	var faq_data_ = popup_form_.serialize_object();

	PB.post({
		action : "faq-update",
		faq_data : faq_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || 'FAQ 추가,수정 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : response_json_.success_title,
			content : response_json_.success_message,
		}, function(){
			document.location = document.location;
		});

	}, true);
}


function _pb_faq_manage_delete(id_){
	PB.confirm_q({
		title : "FAQ삭제확인",
		content : "해당 FAQ를 삭제하시겠습니까?",
		button1 : '삭제하기',
		button1_classes : "btn btn-black",
	}, function(c_){
		if(!c_) return;

		_pb_faq_manage_do_delete(id_);

	});
}
function _pb_faq_manage_do_delete(id_){
	$ = jQuery;

	PB.post({
		action : "faq-delete",
		faq_id : id_
	}, function(result_, response_json_){

		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || 'FAQ삭제 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "FAQ삭제완료",
			content : "FAQ삭제가 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	},true);
}