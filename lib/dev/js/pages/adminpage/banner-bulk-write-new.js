jQuery(document).ready(function($){
	var target_form_ = $("#pb-banner-bulk-write-form");

	var banner_image_uploaders_ = target_form_.find('[data-column-name="banner_image_url"]');

	$.each(banner_image_uploaders_, function(){
		$(this).pb_multiple_image_uploader({
			'maxlength' : 15,
			'modal_label' : "배너이미지 업로드",
			'use_thumbnail' : false,
		});
	});

	target_form_.submit_handler(function(){

		var banner_image_urls_ = $("#pb-banner-bulk-form-banner_image_url").pb_multiple_image_uploader().to_json();

		PB.confirm_q({
			title : "일괄등록확인",
			content : "입력한 내용을 일괄등록하시겠습니까?",
			button1  : "일괄등록하기"
		}, function(c_){
			if(!c_) return false;

			_pb_adminpage_banner_bulk_write(banner_image_urls_);

		});
	}); 

	$(function () {
		$('.button-checkbox').each(function () {
	
			// Settings
			var $widget = $(this),
				$button = $widget.find('button'),
				$checkbox = $widget.find('input:checkbox'),
				color = $button.data('color'),
				settings = {
					on: {
						icon: 'glyphicon glyphicon-check'
					},
					off: {
						icon: 'glyphicon glyphicon-unchecked'
					}
				};
	
			// Event Handlers
			$button.on('click', function () {
				$checkbox.prop('checked', !$checkbox.is(':checked'));
				$checkbox.triggerHandler('change');
				updateDisplay();
				allCheck();
			});
			$checkbox.on('change', function () {
				updateDisplay();
			});
	
			// Actions
			function updateDisplay() {
				var isChecked = $checkbox.is(':checked');
	
				// Set the button's state
				$button.data('state', (isChecked) ? "on" : "off");
	
				// Set the button's icon
				$button.find('.state-icon')
					.removeClass()
					.addClass('state-icon ' + settings[$button.data('state')].icon);
	
				// Update the button's color
				if (isChecked) {
					$button
						.removeClass('btn-default')
						.addClass('btn-' + color + ' active');
				}
				else {
					$button
						.removeClass('btn-' + color + ' active')
						.addClass('btn-default');
				}
			}
	
			// Initialization
			function init() {
	
				updateDisplay();
	
				// Inject the icon if applicable
				if ($button.find('.state-icon').length == 0) {
					$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
				}
			}
			init();

			// 전체 체크박스 체크 해제
			function allCheck() {
				//만약 전체 선택 체크박스가 체크된상태일경우 
				if($("#allCheck").prop("checked")) { 
					//해당화면에 전체 checkbox들을 체크해준다 
					$("input[type=checkbox]").prop("checked",true); 
				} else { 
					//해당화면에 모든 checkbox들의 체크를해제시킨다. 
						$("input[type=checkbox]").prop("checked",false); 
				} 
			}
		});
	});
});

function _pb_adminpage_banner_bulk_write(banner_image_urls_){
	$ = jQuery;

	var target_form_ = $("#pb-banner-bulk-write-form");

	var cinema_id_arr_ = [];

	target_form_.find("[name='cinema_id_list']:checked").each(function(){
		cinema_id_arr_.push($(this).val());
	})
	
	var cinema_links_ = $('input[name=link_url]').val();
	
	PB.post({
		action : "adminpage-banner-bulk-upload-new",
		cinema_id_arr : cinema_id_arr_,
		banner_image_urls :  banner_image_urls_,
		cinema_links : cinema_links_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || '에러발생',
				content : response_json_.error_message || '일괄등록 중 에러가 발생했습니다.',
			});
			return;
		}

		PB.alert_success({
			title : "등록성공",
			content : "배너 일괄등록이 완료되었습니다.",
		}, function(){
			document.location = document.location;
		});

	}, true);

}