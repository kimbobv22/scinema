jQuery(document).ready(function($){
	$('#pb-push-msg-manage-edit-form-scinema_id').multiselect({
		nonSelectedText: '-영화관선택-',
        nSelectedText: '선택함',
        allSelectedText: '전체영화관',
        buttonWidth : "100%",
        selectAllText: ' 전체선택',
        includeSelectAllOption : true,
	});

	var edit_form_ = $("#pb-adminpage-banner-manage-edit-form");

	edit_form_.validator();
	edit_form_.submit_handler(pb_banner_manage_update);

	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");

	var module_ = image_url_hidden_.pb_multiple_image_uploader({
		'maxlength' : 1,
		'modal_label' : "배너이미지 업로드",
		'use_thumbnail' : false,
	});
	
});


function pb_banner_manage_update(){
	$ = jQuery;

	var edit_form_ = $("#pb-adminpage-banner-manage-edit-form");
	var edit_data_ = edit_form_.serialize_object();

	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");
	var image_module_ = image_url_hidden_.pb_multiple_image_uploader();
	var image_urls_ = image_module_.to_json();

	if(image_urls_.length <= 0){
		PB.alert_error({
			title : "배너이미지확인",
			content : "배너이미지를 선택하세요"
		});
		return;
	}

	if(edit_data_.add_new === "Y"){
		PB.confirm_q({
			title : "배너추가확인",
			content : "배너를 추가하시겠습니까?",
			button1 : "등록하기"
		}, function(c_){
			if(!c_) return;
			_pb_banner_manage_update();	
		});
	}else{
		_pb_banner_manage_update();
	}
}

function _pb_banner_manage_update(){
	$ = jQuery;

	var edit_form_ = $("#pb-adminpage-banner-manage-edit-form");
	var edit_data_ = edit_form_.serialize_object();

	var image_url_hidden_ = $("#pb-banner-manage-edit-form-banner_image_url");
	var image_module_ = image_url_hidden_.pb_multiple_image_uploader();
	var image_urls_ = image_module_.to_json();

	edit_data_['banner_image_url'] = image_urls_[0]['url'];

	PB.post({
		action : "banner-manage-update",
		banner_data : edit_data_,
	},function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : "에러발생",
				content : "배너 수정/등록 중 에러가 발생했습니다."
			});
			return;
		}

		PB.alert_success({
			title : (edit_data_.add_new === "Y" ? "추가완료" : "수정완료"),
			content : (edit_data_.add_new === "Y" ? "배너가 성공적으로 추가되었습니다." : "배너정보가 수정되었습니다."),
		}, function(){
			document.location = response_json_.redirect_url
		});

	}, true);
}


function pb_banner_manage_delete(){
	$ = jQuery;

	PB.confirm_q({
		title : "배너삭제확인",
		content : "배너를 삭제하시겠습니까?",
		button1 : "삭제하기"
	}, function(c_){
		if(!c_) return;
		_pb_banner_manage_delete();	
	});
}


function _pb_banner_manage_delete(){
	$ = jQuery;

	var edit_form_ = $("#pb-adminpage-banner-manage-edit-form");
	var edit_data_ = edit_form_.serialize_object();

	PB.post({
		action : "banner-manage-delete",
		mst_id : edit_data_['ID'],
	},function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : "에러발생",
				content : "배너 삭제 중 에러가 발생했습니다."
			});
			return;
		}

		PB.alert_success({
			title : "삭제완료",
			content : "배너삭제가 완료되었습니다.",
		}, function(){
			document.location = response_json_.redirect_url
		});

	}, true);
}