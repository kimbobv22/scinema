jQuery(document).ready(function($){
	var movive_chart_type_links_ = $("#pb-movie-chart-type-menu [data-movie-chart-type]");
	var movive_chart_slider_ = $("#pb-home-movie-chart-slider");

	movive_chart_type_links_.click(function(){
		var target_link_ = $(this);
		_pb_home_load_movie_chart(target_link_.data("movie-chart-type"));

		$("#pb-movie-chart-type-menu > li").toggleClass("actived", false);
		target_link_.parent().toggleClass("actived", true);

		return false;
	});

	var xs_min_ = PB.responsive_info.xs_min;
	var xs_max_ = PB.responsive_info.xs_max;
	var sm_max_ = PB.responsive_info.sm_max;

	var breakpoints_ = {};
	breakpoints_[xs_min_] = {
		slidesPerView: 1,
		spaceBetween: 20
	};
	breakpoints_[xs_max_] = {
		slidesPerView: 2,
		spaceBetween: 20
	};
	breakpoints_[sm_max_] = {
		slidesPerView: 3,
		spaceBetween: 20
	};

	var movive_chart_slider_module_ = new Swiper(movive_chart_slider_[0], {
		speed: 400,
		spaceBetween: 30,
		slidesPerView: 5,
		breakpoints : breakpoints_,
		nextButton : ".next-btn",
		prevButton : ".prev-btn"
	}); 

	var window_el_ = $(window);

	window_el_.resize(function(){
		// movive_chart_slider_module_.params['centeredSlides'] = (window_el_.width() <= xs_min_);
		// movive_chart_slider_module_.update();
		// console.log(movive_chart_slider_module_.params);
	});
	// window_el_.resize();


	var partner_slider_ = $("#pb-home-partner-slider");

	breakpoints_[xs_min_] = {
		slidesPerView: 3,
		spaceBetween: 20
	};
	breakpoints_[xs_max_] = {
		slidesPerView: 4,
		spaceBetween: 20
	};
	breakpoints_[sm_max_] = {
		slidesPerView: 5,
		spaceBetween: 30
	};

	var partner_slider_module_ = new Swiper(partner_slider_[0], {
		speed: 400,
		spaceBetween: 30,
		slidesPerView: 8,
		breakpoints : breakpoints_
	}); 

	
});


function _pb_home_load_movie_chart(chart_type_){

}