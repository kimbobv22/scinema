jQuery(document).ready(function($){
				
	var login_form_ = $("#pb-user-login-form");

	login_form_.validator();
	login_form_.submit_handler(_pb_user_do_login);

	var findpass_modal_ = $("#pb-user-findpass-modal");

	findpass_modal_.on("shown.bs.modal", function(){
		findpass_modal_.find(":input[name='user_email']").val("").focus();
	});

	findpass_modal_.find("#pb-user-findpass-form").validator().submit_handler(_pb_user_do_findpass);

	login_form_.find(":input[name='login_or_email']").focus();
});

function _pb_user_do_login(){

	$ = jQuery;

	var login_form_ = $("#pb-user-login-form");
	var form_data_ = login_form_.serialize_object();

	PB.post({
		action : "user-login",
		login_data : pb_apply_filters('encrypt', form_data_, ['user_pass'])
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title :  response_json_.error_title || "에러발생",
				content : response_json_.error_message || "로그인 중 에러가 발생했습니다.",
			});
			return;
		}

		document.location = login_form_.attr('data-redirect-url');

	}, true);
}

function _pb_user_do_findpass(){
	$ = jQuery;
	var findpass_form_ = $("#pb-user-findpass-form");

	PB.post({
		action : "user-findpass",
		user_email : findpass_form_.find(":input[name='user_email']").val()
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title :  response_json_.error_title || "에러발생",
				content : response_json_.error_message || "비밀번호찾기 이메일인증 중 에러가 발생했습니다.",
			});
			return;
		}

		$("#pb-user-findpass-modal").modal("hide");

		PB.alert_success({
			title : "발송완료",
			content : "이메일을 확인하여 비밀번호를 재설정할 수 있습니다."
		});
	}, true);
}