jQuery(document).ready(function($){				
	var resetpass_form_ = $("#pb-user-resetpass-form");

	resetpass_form_.validator();
	resetpass_form_.submit_handler(_pb_user_resetpass);

	resetpass_form_.find(":input[name='user_pass']").focus();
});

function _pb_user_resetpass(){
	$ = jQuery;

	PB.confirm_q({
		title : "비밀번호재설정",
		content : "비밀번호를 재설정 하시겠습니까?",
		button1 : "재설정하기"
	},  function(c_){
		if(!c_) return;
		_pb_user_do_resetpass();
	});
}

function _pb_user_do_resetpass(){
	$ = jQuery;
	var resetpass_form_ = $("#pb-user-resetpass-form");
		resetpass_form_['user_pass_c'] = null;

	PB.post({
		action : "user-resetpass",
		request_data : pb_apply_filters('encrypt', resetpass_form_.serialize_object(), ["user_pass"])
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title :  response_json_.error_title || "에러발생",
				content : response_json_.error_message || "비밀번호 재설정 중 에러가 발생했습니다.",
			});
			return;
		}

		PB.alert_success({
			title : "변경완료",
			content : "비밀번호를 변경하였습니다. 로그인화면으로 이동합니다.",
		}, function(){
			document.location = resetpass_form_.data('login-url')
		});
	}, true);
}