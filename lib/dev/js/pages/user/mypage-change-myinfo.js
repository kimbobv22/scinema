jQuery(document).ready(function($){
				
	var update_form_ = $("#pb-user-change-myinfo-form");
	
	update_form_.validator();
	update_form_.submit_handler(_pb_user_change_myinfo);
	
	var birth_date_picker_ = $("#pb-user-change-myinfo-form-birth_yyyymmdd-wrap");

	if(birth_date_picker_.length > 0){
		// console.log(birth_date_picker_);
		birth_date_picker_.datetimepicker({
			viewMode: 'years',
			locale: 'ko',
			allowInputToggle : true,
			format: 'YYYY-MM-DD'
		});

		$("#pb-user-signup-form-phone1_1").change(function(){
			$("#pb-user-signup-form-phone1_2").focus();
		});
	}

	var change_myinfo_pass_modal_ = $("#pb-user-change-myinfo-pass-modal");
	var change_myinfo_pass_form_ = $("#pb-user-change-myinfo-pass-form");

	change_myinfo_pass_modal_.on("shown.bs.modal", function(){
		change_myinfo_pass_form_.find(":input[name='user_pass']").focus();
	});

	change_myinfo_pass_form_.validator();
	change_myinfo_pass_form_.submit_handler(_pb_user_do_change_myinfo);

	var change_pass_modal_ = $("#pb-user-change-pass-modal");
	var change_pass_form_ = $("#pb-user-change-pass-form");

	change_pass_modal_.on("show.bs.modal", function(){
		change_pass_form_[0].reset();		
	});
	change_pass_modal_.on("shown.bs.modal", function(){
		change_pass_form_.find(":input[name='current_pass']").focus();
	});

	change_pass_form_.validator();
	change_pass_form_.submit_handler(_pb_user_change_pass);

	var user_leave_modal_ = $("#pb-user-leave-modal");
	var user_leave_form_ = $("#pb-user-leave-form");
	
	user_leave_form_.validator();
	user_leave_form_.submit_handler(_pb_user_leave);	

	user_leave_modal_.on("show.bs.modal", function(){
		user_leave_form_[0].reset();
	});
});

function _pb_user_change_myinfo(){

	$ = jQuery;
	
	var change_myinfo_pass_modal_ = $("#pb-user-change-myinfo-pass-modal");
	var change_myinfo_pass_form_ = $("#pb-user-change-myinfo-pass-form");

	change_myinfo_pass_form_[0].reset();
	// change_myinfo_pass_form_.validator("reset");

	change_myinfo_pass_modal_.modal("show");
}

function _pb_user_do_change_myinfo(){
	$ = jQuery;

	var change_myinfo_pass_modal_ = $("#pb-user-change-myinfo-pass-modal");
	var change_myinfo_pass_form_ = $("#pb-user-change-myinfo-pass-form");
	var update_form_ = $("#pb-user-change-myinfo-form");

	var update_data_ = update_form_.serialize_object();
		update_data_['user_pass'] = change_myinfo_pass_form_.find(":input[name='user_pass']").val();

	update_data_['birth_yyyymmdd'] = moment(update_data_['birth_yyyymmdd'], 'YYYY-MM-DD').format('YYYYMMDD');

	if(!update_data_['mailing_recv_yn']) update_data_['mailing_recv_yn'] = "N";
	if(!update_data_['sms_recv_yn']) update_data_['sms_recv_yn'] = "N";

	PB.post({
		action : "user-change-myinfo",
		update_data : pb_apply_filters('encrypt', update_data_, ["user_pass"]),
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title :  response_json_.error_title || "에러발생",
				content : response_json_.error_message || "내정보변경 중 에러가 발생했습니다.",
			});
			return;
		}

		change_myinfo_pass_modal_.modal("hide");

		PB.alert_success({
			title : "변경완료",
			content : "내정보가 정상적으로 변경되었습니다."
		});
	}, true);
}

function _pb_user_change_pass(){
	$ = jQuery;

	var change_pass_modal_ = $("#pb-user-change-pass-modal");
	var change_pass_form_ = $("#pb-user-change-pass-form");

	// change_pass_modal_.modal("hide");

	var current_pass_ = change_pass_form_.find(":input[name='current_pass']").val();
	var new_pass_ = change_pass_form_.find(":input[name='new_pass']").val();

	PB.post({
		action : "user-change-password",
		update_data : pb_apply_filters('encrypt', {
			current_pass : current_pass_,
			new_pass : new_pass_,
		}, ["current_pass", "new_pass"]),
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title :  response_json_.error_title || "에러발생",
				content : response_json_.error_message || "비밀번호 중 에러가 발생했습니다.",
			});
			return;
		}

		change_pass_modal_.modal("hide");
		PB.alert_success({
			title : "변경완료",
			content : "비밀번호가 정상적으로 변경되었습니다. 다시 로그인 해주세요."
		}, function(){
			document.location = document.location;
		});
	}, true);
}

function _pb_user_leave(){
	PB.confirm_q({
		title : "회원탈퇴확인",
		content : "정말로 회원탈퇴을 진행하시겠습니까?",
		button1 : "탈퇴하기",
		button1Classes : "btn btn-black"
	}, function(c_){
		if(!c_) return;

		_pb_user_do_leave();
	});
}

function _pb_user_do_leave(){
	$ = jQuery;

	var user_leave_modal_ = $("#pb-user-leave-modal");
	var user_leave_form_ = $("#pb-user-leave-form");

	PB.post({
		action : "user-leave",
		request_data : pb_apply_filters('encrypt', user_leave_form_.serialize_object(), ["user_pass"])
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title :  response_json_.error_title || "에러발생",
				content : response_json_.error_message || "회원탈퇴처리 중 에러가 발생했습니다.",
			});
			return;
		}

		PB.alert_success({
			title :  "탈퇴완료",
			content : "회원탈퇴가 정상적으로 처리되었습니다. 홈화면으로 이동합니다.",
		}, function(){
			document.location = user_leave_form_.attr("data-home-url");
		});
	}, true);
}


function _pb_user_find_postcode(){
	$ = jQuery;
	var edit_form_ = $("#pb-user-change-myinfo-form");

	/*edit_form_.find("#pb-user-change-myinfo-form-addr_postcode").blur();
	edit_form_.find("#pb-user-change-myinfo-form-addr").blur();*/

	new daum.Postcode({
		oncomplete: function(data_){
			// var address_ = (data_['autoJibunAddress'] || data_['jibunAddress']);
			var address_ = (data_['autoRoadAddress'] || data_['roadAddress']);
			edit_form_.find("#pb-user-change-myinfo-form-addr_postcode").val(data_["zonecode"]);
			edit_form_.find("#pb-user-change-myinfo-form-addr").val(address_);
		}
	}).open();
}