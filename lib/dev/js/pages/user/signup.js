jQuery(document).ready(function($){
				
	var signup_form_ = $("#pb-user-signup-form");
	var need_auth_myself_ = signup_form_.find(":input[name='need_auth_myself']").val() === "Y";
	var need_auth_email_ = signup_form_.find(":input[name='need_auth_email']").val() === "Y";

	signup_form_.validator();
	signup_form_.submit_handler(_pb_user_signup);
	
	
	if(need_auth_email_){
		var email_auth_group_ = signup_form_.find("[data-email-auth-group]");	
		var autcode_send_btn_ = email_auth_group_.find(".btn-send-autcode");
		var email_input_ = email_auth_group_.find(":input[name='user_email']");

		autcode_send_btn_.click(function(){
			_pb_user_signup_send_authcode_for_email();
			return false;		
		});

		var email_authcode_group_ = signup_form_.find("[data-email-authcode-group]");	
		var autcode_check_btn_ = email_authcode_group_.find(".btn-check-autcode");
		var autcode_email_input_ = email_authcode_group_.find(":input[name='authcode_email']");

		autcode_check_btn_.click(function(){
			_pb_user_signup_check_authcode_for_email();
			return false;		
		});

		signup_form_.on('validated.bs.validator', function(){
			var email_errors_ = email_input_.data("bs.validator.errors");
			var disabled_ = (email_errors_ === undefined || email_errors_.length > 0);
			autcode_send_btn_.prop("disabled", disabled_);

			email_errors_ = autcode_email_input_.data("bs.validator.errors");
			var disabled_ = (email_errors_ === undefined || email_errors_.length > 0);
			autcode_check_btn_.prop("disabled", disabled_);
		});
	}

	if(need_auth_myself_){

	}else{

		var birth_date_picker_ = $("#pb-user-signup-form-birth_yyyymmdd-wrap");
		// console.log(birth_date_picker_);
		birth_date_picker_.datetimepicker({
			viewMode: 'years',
			locale: 'ko',
			allowInputToggle : true,
			format: 'YYYY-MM-DD'
		});

		$("#pb-user-signup-form-phone1_1").change(function(){
			$("#pb-user-signup-form-phone1_2").focus();
		});

	}
});


function _pb_user_signup_send_authcode_for_email(){
	$ = jQuery;
	var signup_form_ = $("#pb-user-signup-form");
	var email_auth_group_ = signup_form_.find("[data-email-auth-group]");	
	var autcode_send_btn_ = email_auth_group_.find(".btn-send-autcode");
	var email_input_ = email_auth_group_.find(":input[name='user_email']");


	if(autcode_send_btn_.hasClass("loading") || autcode_send_btn_.hasClass("complete")) return;

	var user_email_ = email_input_.val();

	autcode_send_btn_.toggleClass("loading", true);
	autcode_send_btn_.prop("disabled", true);
	email_input_.prop("readonly", true);

	PB.user.send_authcode_for_email(user_email_, function(result_, response_json_){
		if(!result_){
			PB.alert_error({
				title : response_json_.error_title || "에러발생",
				content : response_json_.error_message || "이메일 인증 중 에러가 발생했습니다",
			});

			autcode_send_btn_.toggleClass("loading", false);
			autcode_send_btn_.prop("disabled", false);
			email_input_.prop("readonly", false);

			return;
		}

		autcode_send_btn_.toggleClass("loading", false);
		autcode_send_btn_.prop("disabled", false);
		autcode_send_btn_.find(".normal").text("다시요청");

		var email_authcode_group_ = signup_form_.find("[data-email-authcode-group]");	
			email_authcode_group_.toggleClass('hidden', false);

		var authcode_email_input_ = email_authcode_group_.find(":input[name='authcode_email']");
			authcode_email_input_.toggleClass("hidden", false);
			authcode_email_input_.focus();
			signup_form_.validator("update");
	});
}

function _pb_user_signup_check_authcode_for_email(){
	$ = jQuery;
	var signup_form_ = $("#pb-user-signup-form");
	var email_authcode_group_ = signup_form_.find("[data-email-authcode-group]");	
	var autcode_check_btn_ = email_authcode_group_.find(".btn-check-autcode");
	var autcode_email_input_ = email_authcode_group_.find(":input[name='authcode_email']");

	var email_auth_group_ = signup_form_.find("[data-email-auth-group]");	
	var autcode_send_btn_ = email_auth_group_.find(".btn-send-autcode");
	var user_email_ = email_auth_group_.find(":input[name='user_email']").val();

	if(autcode_check_btn_.hasClass("loading") || autcode_check_btn_.hasClass("complete")) return;

	autcode_check_btn_.toggleClass("loading", true);
	autcode_check_btn_.prop("disabled", true);
	autcode_email_input_.prop("readonly", true);

	PB.user.check_authcode_for_email(user_email_, autcode_email_input_.val(), function(result_, response_json_){
		if(!result_){
			PB.alert_error({
				title : response_json_.error_title || "에러발생",
				content : response_json_.error_message || "이메일 인증 중 에러가 발생했습니다",
			});
			autcode_check_btn_.toggleClass("loading", false);
			autcode_check_btn_.prop("disabled", false);
			autcode_email_input_.prop("readonly", false);

			return;
		}

		email_authcode_group_.toggleClass("hidden", true);
		autcode_send_btn_.toggleClass("complete", true);
		signup_form_.find(":input[name='user_pass']").focus();
		signup_form_.find(":input[name='authcode_email_confirmed']").val("Y");
	});
}

function _pb_user_signup(){

	$ = jQuery;
	var signup_form_ = $("#pb-user-signup-form");
	var need_auth_myself_ = signup_form_.find(":input[name='need_auth_myself']").val() === "Y";
	var need_auth_email_ = signup_form_.find(":input[name='need_auth_email']").val() === "Y";

	if(need_auth_email_ && signup_form_.find(":input[name='authcode_email_confirmed']").val() !== "Y"){
		PB.alert_info({
			title : "이메일인증필요",
			content : "통합회원가입을 위해 이메일 인증을 진행하여 주세요"
		});
		return;
	}

	PB.confirm_q({
		title : "회원가입확인",
		content : "회원가입을 진행합니다. 계속하시겠습니까?",
		button1 : "가입하기"
	},function(c_){
		if(!c_) return;
		_pb_user_do_signup(signup_form_.serialize_object());
	});
}

function _pb_user_do_signup(form_data_){
	form_data_['user_pass_c'] = null;
	form_data_['birth_yyyymmdd'] = moment(form_data_['birth_yyyymmdd'], 'YYYY-MM-DD').format('YYYYMMDD');


	PB.post({
		action : "user-signup",
		signup_data : pb_apply_filters('encrypt', form_data_, ['user_pass'])
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title :  response_json_.error_title || "에러발생",
				content : response_json_.error_message || "회원가입처리 중 에러가 발생했습니다.",
			});
			return;
		}

		$ = jQuery;
		var signup_form_ = $("#pb-user-signup-form");

		if(signup_form_.find("[name='for_app']").val() === "Y"){
			document.location = "/mobile/close";
		}else{
			document.location = document.location;	
		}

		

	}, true);
}