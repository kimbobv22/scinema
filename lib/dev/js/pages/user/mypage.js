jQuery(document).ready(function($){

	$("[data-mypage-action='logout']").click(function(){

		var logout_url_ = $(this).attr("href");

		PB.confirm_q({
			title : "로그아웃확인",
			content : "로그아웃하시겠습니까?",
			button1 : "로그아웃",
		}, function(c_){
			if(!c_) return;

			document.location = logout_url_;
		});

		return false;
	});

});