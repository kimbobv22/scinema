if(typeof DTRYX == 'undefined'){
  DTRYX = new Object();
}
if(typeof DTRYX.Common == 'undefined'){
  DTRYX.Common = {
    /*기준URL 테스트용*/
    // BaseURL:'https://api-test.dtryx.com:30443/dtryx/cms',
    // MobileURL:'https://mobilebooking-test.dtryx.com',
    // HomeURL:'https://homebooking-test.dtryx.com',
    // MobileRsvURL:'https://mobilebooking-test.dtryx.com/bookinglist',
    // HomeRsvURL:'https://homebooking-test.dtryx.com/bookinglist',


    /*기준URL 서비스용*/
    BaseURL:'https://api.dtryx.com:30443/dtryx/cms',
    //MobileURL:'https://mobilebooking.dtryx.com',
    MobileURL:'https://mbooking.dtryx.com', 
    HomeURL:'https://homebooking.dtryx.com',
    //MobileRsvURL:'https://mobilebooking.dtryx.com/bookinglist',
    MobileRsvURL:'https://mbooking.dtryx.com/bookinglist', 
    HomeRsvURL:'https://homebooking.dtryx.com/bookinglist',

    ReservationCount:0, //예매 팝업 오픈 횟수
    LoadingCount:0,
    /*이미지기준URL*/
    BaseImg:'http://scinema.org',
    /*이미지사이즈*/
    ImgSize:'small',
    /*SSL접속키값*/
    Authorization:'Basic YXBpOkFkbWluMTIzIUAj',
    /*DataType*/
    ContentType:'application/json',
    /*현재판매일자*/
    SelDate:formatDate(new Date()),
    /*가맹점 고유키값*/
    CompanyGuID:'AF3747C8-628C-487E-BC1A-C00AC1A92C6D', /*작은영화관 고유값 키값*/
    /*작업자고유키*/
    WorkGuID:'',
    /*접속채널아이디*/
    ChannelCd:'homepage',
    /*요청언어타입*/
    EngVerYn:'N',
    /*판매일자*/
    SaleSDT:formatDate(new Date()),
    /*이전달*/
    PrevMonthDate:'',
    /*다음달*/
    NextMonthDate:'',
    /*최초영화관선택타입정보*/
    ICinemaCd:'',
    /*영화관코드*/
    CinemaCd:'',      /*특정영화관코드가 없을 경우 전체기준*/
    /*영화코드*/
    MovieCd:'',
    /*선택영화코드 */
    SelectedMovieCd:'',
    /*상영일자*/
    PlayDateList:[],
    /*예매가능 지역*/
    RegionList:[],
    /*회차별 상영시간표*/
    TimetableShowSeqList:[],
    /*지점별 상영시간표*/
    TimetableList:[],
    /*선택지역코드*/
    SelRegionCd:'all',  /*기본값은 전체*/
    ShowSeq:'',
    ScreenCd:'',
    /*회원정보*/
    AgreeType:'N',
    MemberYn:'',
    MemberNm:'',        /*회원이름*/
    MobileNoFull:'',    /*회원연락처*/
    BirthdayDT:'',      /*회원생년월일*/
    CertiNo:'',         /*비회원비밀번호*/
    MemberGuID:'',      /*회원고유아이디*/
    PublicID:'',
  }
}
$(document).ready(function(){
  //initTimetable();
});
/*실행초기화함수*/
function initTimetable(RegionCd, CinemaCd, MovieCd){
  try{
    /*기본값설정*/
    DTRYX.Common.RegionCd   = RegionCd||'';
    DTRYX.Common.CinemaCd   = CinemaCd||'';
    DTRYX.Common.ICinemaCd  = CinemaCd||'';
    DTRYX.Common.MovieCd    = MovieCd||'';

    var params = {
        CompanyGuID:DTRYX.Common.CompanyGuID,
        ChannelCd:DTRYX.Common.ChannelCd,
        EngVerYn:DTRYX.Common.EngVerYn
      };
      $.ajax({
          url:DTRYX.Common.BaseURL + '/online/company/company-check',
          headers : {"Content-Type" : DTRYX.Common.ContentType, "Authorization" : DTRYX.Common.Authorization},
          dataType:'json',
          data:params,
          type:"GET",
          success:function(data){
            //alert(data.Recordset.ShowSeqList[0].CinemaCd);
            if(data == undefined || data == null)
            {
              alert('회원사정보 요청오류 발생 다시 시도해주세요.');
              return;
            }

            if(data.RetCode.toUpperCase() == 'SUCCESS' && data.Recordset.length>0){
                DTRYX.Common.BrandCd = data.Recordset[0].BrandCd;
                DTRYX.Common.WorkGuID = data.Recordset[0].WorkGuID;
                DTRYX.Common.SaleSDT = data.Recordset[0].SaleSDT;
                DTRYX.Common.SelDate = data.Recordset[0].SaleSDT;

                /*영화코드 all일경우 전체 극장 상영시간표 처리*/
                if(CinemaCd.toUpperCase() == 'ALL'){
                  getRegionList();        //예매가능지역
                }
                else{
                  getPlayDateList();  //예매가능일자조회
                }
            }
            else{
              alert('[' + data.RetCode + ']' + data.RetMsg);
              return;
            }

          },
          error:function(e, o){
            alert('[' + e.status + '] ' + o);
          }
      })
  }
  catch(err){}
  finally{}
}
/*예매가능한 지역 조회함수*/
function getRegionList(){
  try{
    var params = {
        BrandCd:DTRYX.Common.BrandCd,
        CinemaCd:DTRYX.Common.CinemaCd,
        MovieCd:DTRYX.Common.MovieCd,
        PlaySDT:DTRYX.Common.SelDate,
        ChannelCd:DTRYX.Common.ChannelCd,
        WorkGuID:DTRYX.Common.WorkGuID,
        EngVerYn:DTRYX.Common.EngVerYn
      };
      $.ajax({
          url:DTRYX.Common.BaseURL + '/thirdparty/movie/third-party-type2-timetable-area-list',
          headers : {"Content-Type" : DTRYX.Common.ContentType, "Authorization" : DTRYX.Common.Authorization},
          dataType:'json',
          data:params,
          type:"GET",
          success:function(data){
            //alert(data.Recordset.ShowSeqList[0].CinemaCd);
            if(data == undefined || data == null)
            {
              alert('예매가능 지역 정보 요청오류 발생 다시 시도해주세요.');
              return;
            }

            if(data.RetCode.toUpperCase() == 'SUCCESS'){
                //drawTimeTable(data);
                DTRYX.Common.RegionList = data.Recordset;
                getPlayDateList();
            }
            else{
              alert('[' + data.RetCode + ']' + data.RetMsg);
              return;
            }

          },
          error:function(e, o){
            alert('[' + e.status + '] ' + o);
          }
      })
  }
  catch(err){}
  finally{}
}
/*상영가능일자 조회함수*/
function getPlayDateList(){
  try{
    var params = {
        BrandCd:DTRYX.Common.BrandCd,
        CinemaCd:DTRYX.Common.CinemaCd,
        MovieCd:DTRYX.Common.MovieCd,
        ChannelCd:DTRYX.Common.ChannelCd,
        WorkGuID:DTRYX.Common.WorkGuID,
        EngVerYn:DTRYX.Common.EngVerYn
      };
      $.ajax({
          url:DTRYX.Common.BaseURL + '/thirdparty/movie/third-party-type2-timetable-play-date-list',
          headers : {"Content-Type" : DTRYX.Common.ContentType, "Authorization" : DTRYX.Common.Authorization},
          dataType:'json',
          data:params,
          type:"GET",
          success:function(data){
            //alert(data.Recordset.ShowSeqList[0].CinemaCd);
            if(data == undefined || data == null)
            {
              alert('예매가능날짜 정보 요청오류 발생 다시 시도해주세요.');
              return;
            }

            if(data.RetCode.toUpperCase() == 'SUCCESS'){
                DTRYX.Common.PlayDateList = data.Recordset;
                if(data.RetCode.toUpperCase() == 'SUCCESS'
                && parseInt(data.RecordCount)>0){
                  // debugger;

                  // var usePlayDate = DTRYX.Common.PlayDateList.find(function(ddate) {
                  //   return ddate.HiddenYn.toUpperCase() == 'N'
                  // });
                  // var usePlayDate = DTRYX.Common.PlayDateList.find((ddate) =>{
                  //   return ddate.HiddenYn.toUpperCase() === 'N'
                  // });
                  var usePlayDate = null;
                  var idx=0;
                  for(var i=0; i<DTRYX.Common.PlayDateList.length; i++){
                    var item = DTRYX.Common.PlayDateList[i];
                    if(idx==0 && item.HiddenYn.toUpperCase() == 'N'){
                      usePlayDate = item;
                      idx++;
                    }
                  }
                  if(usePlayDate != undefined && usePlayDate != null){
                    DTRYX.Common.SelDate = usePlayDate.PlaySDT;
                  }
                  /*극장코드 all일경우 영화관 전체 상영회차 정보 처리 부분*/
                  if(DTRYX.Common.ICinemaCd.toUpperCase() == 'ALL'){
                    getTimeTableShowSeqInfo();
                  }
                  else{
                    getTimeTableInfo();
                  }
                }
                else{
                  //alert('예매가능일자가 없습니다.');
                  // 예매가능 일자가 없는경우 알럿 말고 css 없다고 표시 (nhn수정)
                  drawTimeTable();
                  return;
                }

            }
            else{
              alert('[' + data.RetCode + ']' + data.RetMsg);
              return;
            }

          },
          error:function(e, o){
            alert('[' + e.status + '] ' + o);
          }
      })
  }
  catch(err){}
  finally{}
}
/*상영시간표 조회함수(회차별)*/
function getTimeTableShowSeqInfo(){
  //console.log('getTimeTableShowSeqInfo')
  try{
    var params = {
        BrandCd:DTRYX.Common.BrandCd,
        CinemaCd:DTRYX.Common.ICinemaCd,
        MovieCd:DTRYX.Common.MovieCd,
        PlaySDT:DTRYX.Common.SelDate,
        //PlaySDT:'2019-04-04',
        RegionCd:DTRYX.Common.SelRegionCd,
        ChannelCd:DTRYX.Common.ChannelCd,
        WorkGuID:DTRYX.Common.WorkGuID,
        EngVerYn:DTRYX.Common.EngVerYn
      };

      $.ajax({
          url:DTRYX.Common.BaseURL + '/thirdparty/movie/third-party-type2-timetable-showseq-list',
          headers : {"Content-Type" : DTRYX.Common.ContentType, "Authorization" : DTRYX.Common.Authorization},
          dataType:'json',
          data:params,
          type:"GET",
          success:function(data){
            //alert(data.Recordset.ShowSeqList[0].CinemaCd);
            if(data == undefined || data == null)
            {
              alert('예매가능날짜 정보 요청오류 발생 다시 시도해주세요.');
              return;
            }

            if(data.RetCode.toUpperCase() == 'SUCCESS'){
                //drawTimeTable(data);
                DTRYX.Common.TimetableShowSeqList = data.Recordset;
                drawTimeTableShowSeqInfo();
            }
            else{
              alert('[' + data.RetCode + ']' + data.RetMsg);
              return;
            }

          },
          error:function(e, o){
            alert('[' + e.status + '] ' + o);
          }
      })
  }
  catch(err){}
  finally{}
}
/*상영시간표 조회함수(지점별)*/
function getTimeTableInfo(){
    try{
      //DTRYX.Common.SelDate = PlayDate;
      var params = {
          BrandCd:DTRYX.Common.BrandCd,
          CinemaCd:DTRYX.Common.ICinemaCd,
          MovieCd:DTRYX.Common.MovieCd,
          PlaySDT:DTRYX.Common.SelDate,
          ImgSize:DTRYX.Common.ImgSize,
          ChannelCd:DTRYX.Common.ChannelCd,
          WorkGuID:DTRYX.Common.WorkGuID,
          EngVerYn:DTRYX.Common.EngVerYn
        };
        $.ajax({
            url:DTRYX.Common.BaseURL + '/thirdparty/movie/third-party-type2-timetable-list',
            headers : {"Content-Type" : DTRYX.Common.ContentType, "Authorization" : DTRYX.Common.Authorization},
            dataType:'json',
            data:params,
            type:"GET",
            success:function(data){
              //alert(data.Recordset.ShowSeqList[0].CinemaCd);
              if(data == undefined || data == null)
              {
                alert('영화상영시간표 정보 요청오류 발생 다시 시도해주세요.');
                return;
              }

              if(data.RetCode.toUpperCase() == 'SUCCESS'){
                  //drawTimeTable(data);
                  DTRYX.Common.TimetableList = data.Recordset;
                  drawTimeTable();
              }
              else{
                alert('[' + data.RetCode + ']' + data.RetMsg);
                return;
              }

            },
            error:function(e, o){
              alert('[' + e.status + '] ' + o);
            }
        })
    }
    catch(err){}
    finally{}
}
/*상영시간표 작성함수(회차별)*/
function drawTimeTableShowSeqInfo(){
  //console.log('drawTimeTableShowSeqInfo');
  try{
    var TimetableShowSeqList = DTRYX.Common.TimetableShowSeqList||null;
    var layoutPlayDate = drawPlayDateList();
    var layoutRegion = drawRegion();
    var table           = [];
    $('#DTRYX-SCINEMA-PLAYDATE').html(layoutPlayDate.join(''));
    $('#DTRYX-SCINEMA-REGION').html(layoutRegion.join(''));

    //if(TimetableShowSeqList != null){
    if(TimetableShowSeqList.length>0){
      var screenChildren  = [],
          showSeqChildren = [],
          chkCinemaCd     = '',
          chkScreenCd     = '',
          item            = [];

          for(var i=0; i<TimetableShowSeqList.length; i++){
            item = TimetableShowSeqList[i];
            if(i==0){
              chkCinemaCd = item.CinemaCd;
              chkScreenCd = item.ScreenCd;
            }
            if(i>0 && chkCinemaCd != item.CinemaCd){
              //console.log('1')
              var entity = TimetableShowSeqList[i-1];
              screenChildren.push('<div class="s-tit">'+entity.CinemaNm+'</div>');
              screenChildren.push('<div class="s-lst">'+showSeqChildren.join('')+'</div>');
              table.push('<div class="con-row">'+screenChildren.join('')+'</div>');
              screenChildren = [];
              showSeqChildren = [];
              chkCinemaCd = item.CinemaCd;
              chkScreenCd = item.ScreenCd;
            }
            // else{
            //   if(i>0 && chkScreenCd != item.ScreenCd){
            //     //console.log('2')
            //     var entity = TimetableShowSeqList[i-1];
            //     screenChildren.push('<div class="s-tit">영월시네마</div>');
            //     screenChildren.push('<div class="s-lst">'+showSeqChildren.join('')+'</div>');
            //     chkScreenCd = item.ScreenCd;
            //     showSeqChildren = [];
            //   }
            // }

            showSeqChildren.push('<a href="#none" class="itm" onClick="DTRYXBookingJs(\''+item.CinemaCd+'\', \''+item.MovieCd+'\', \''+item.ShowSeq+'\', \''+item.PlaySDT+'\', \''+item.ScreenCd+'\')"><div class="tit">'+item.CinemaNm+'</div><p class="hname">'+item.ScreenNm+'</p><div class="desc"><p>'+item.ScreeningInfo+'</p><strong>'+item.StartTime+'</strong><span>잔여석 '+item.RemainSeatCnt+'석</span></div></a>');
          }

          if(showSeqChildren.length>0){
            screenChildren.push('<div class="s-tit">'+item.CinemaNm+'</div>');
            screenChildren.push('<div class="s-lst">'+showSeqChildren.join('')+'</div>');
            table.push('<div class="con-row">'+screenChildren.join('')+'</div>');
            $('#DTRYX-SCINAMA-NABI').show();
          }
    }
    else{
      //등록된 상영시간표가 없습니다.
      table.push('<div class="con-row2" style="text-align:center;height:150px;padding-top:60px;"}>등록된 상영시간표가 없습니다.</div>');
      if (DTRYX.Common.LoadingCount == 0) { $('#DTRYX-SCINAMA-NABI').hide(); }
    }
    $('#DTRYX-SCINEMA-TIMETABLE').html(table.join(''));
    //if(DTRYX.Common.LoadingCount==0){
      var mivieCnt = new Swiper('.day-cnt', {
        slidesPerView: 'auto',
        //centeredSlides: true,
        spaceBetween: 10,
  			initialSlide:0,
        prevButton: '.day-cnt .prev',
        nextButton: '.day-cnt .next'
      });


      var usePlayDate = null;
      var idx=0;
      for(var i=0; i<DTRYX.Common.PlayDateList.length; i++){
        var item = DTRYX.Common.PlayDateList[i];
        if(idx==0 && item.PlaySDT == DTRYX.Common.SelDate){
          usePlayDate = item;
          idx++;
        }
      }

      // var usePlayDate = DTRYX.Common.PlayDateList.find(function(ddate) {
      //   return ddate.PlaySDT == DTRYX.Common.SelDate
      // });
      // var usePlayDate = DTRYX.Common.PlayDateList.find((ddate) =>{
      //   return ddate.PlaySDT === DTRYX.Common.SelDate
      // });


      if(usePlayDate != undefined && usePlayDate != null){
        var rowNum = parseInt(usePlayDate.RowNo||0);
        mivieCnt.slideTo(rowNum-1, false,false);
      }
    //}
    DTRYX.Common.LoadingCount = 1;
  }
  catch(err){
    console.log(err);
  }
  finally{}
}
/*예매페이지 이전에 선택정보세팅*/
function DTRYXBookingJs(cinemaCd, movieCd, showSeq, playSDT, screenCd){
  try{
    DTRYX.Common.CinemaCd = cinemaCd;
    DTRYX.Common.SelectedMovieCd = movieCd;
    DTRYX.Common.ShowSeq = showSeq;
    DTRYX.Common.SelDate = playSDT;
    DTRYX.Common.ScreenCd = screenCd;

    DTRYXBooking(cinemaCd, movieCd, showSeq, playSDT, screenCd);
  }
  catch(err){}
  finally{}
}
/*시간표 작성함수 지점별*/
function drawTimeTable(){
  //console.log('drawTimeTableShowSeqInfo');
  try{
    var TimetableList = DTRYX.Common.TimetableList||null;
    var layoutPlayDate = drawPlayDateList();
    var table           = [];
    //var layoutRegion = drawRegion();

    $('#DTRYX-SCINEMA-PLAYDATE').html(layoutPlayDate.join(''));
    //$('#DTRYX-SCINEMA-REGION').html(layoutRegion.join(''));
    if(TimetableList.length>0){
          var screenChildren  = [],
              showSeqChildren = [],
              chkCinemaCd     = '',
              chkScreenCd     = '',
              item            = [];

          for(var i=0; i<TimetableList.length; i++){
            item = TimetableList[i];
            if(i==0){
              chkCinemaCd = item.CinemaCd;
              chkMovieCd = item.MovieCd;
              chkScreenCd = item.ScreenCd;
            }
            if(i>0 && chkMovieCd != item.MovieCd){
              //console.log('1')
              var entity = TimetableList[i-1];
              var Rating = entity.Rating,
                  RatingNm = '',
                  RatingCssNm = '';
              if(Rating == '00'){
                RatingNm = '전체';
                RatingCssNm = 'all';
              }
              else{
                RatingCssNm = Rating;
                if(Rating == '18') RatingNm = '청불';
                else RatingNm = Rating;
              }
              screenChildren.push('<div class="poster-image"><img src="'+entity.Url+'"></div>');
              screenChildren.push('<div class="poster-desc"><div class="movie-name"><i class="blt blt-'+RatingCssNm+'">'+RatingNm+'</i> '+entity.MovieNm+'</div><div class="s-lst">'+showSeqChildren.join('')+'</div></div>');
              table.push('<div class="con-row2">'+screenChildren.join('')+'</div>');
              screenChildren = [];
              showSeqChildren = [];
              chkCinemaCd = item.CinemaCd;
              chkMovieCd = item.MovieCd;
              chkScreenCd = item.ScreenCd;
            }
            showSeqChildren.push('<a href="#none" class="itm" onClick="DTRYXBookingJs(\''+item.CinemaCd+'\', \''+item.MovieCd+'\', \''+item.ShowSeq+'\', \''+item.PlaySDT+'\', \''+item.ScreenCd+'\')"><div class="tit">'+item.CinemaNm+'</div><p class="hname">'+item.ScreenNm+'</p><div class="desc"><p>'+item.ScreeningInfo+'</p><strong>'+item.StartTime+'</strong><span>잔여석 '+item.RemainSeatCnt+'석</span></div></a>');
          }

          if(showSeqChildren.length>0){
            var Rating = item.Rating,
                RatingNm = '',
                RatingCssNm = '';
            if(Rating == '00'){
              RatingNm = '전체';
              RatingCssNm = 'all';
            }
            else{
              RatingCssNm = Rating;
              if(Rating == '18') RatingNm = '청불';
              else RatingNm = Rating; 
            }
            screenChildren.push('<div class="poster-image"><img src="'+item.Url+'"></div>');
            screenChildren.push('<div class="poster-desc"><div class="movie-name"><i class="blt blt-'+RatingCssNm+'">'+RatingNm+'</i> '+item.MovieNm+'</div><div class="s-lst">'+showSeqChildren.join('')+'</div></div>');
            table.push('<div class="con-row2">'+screenChildren.join('')+'</div>');
            $('#DTRYX-SCINAMA-NABI').show();
          }
    }
    else{
      //등록된 상영시간표가 없습니다.
      table.push('<div class="con-row2" style="text-align:center;height:150px;padding-top:60px;"}>등록된 상영시간표가 없습니다.</div>');
      if (DTRYX.Common.LoadingCount == 0) { $('#DTRYX-SCINAMA-NABI').hide(); }
    }
    $('#DTRYX-SCINEMA-TIMETABLE').html(table.join(''));

    //if(DTRYX.Common.LoadingCount==0){
      var mivieCnt = new Swiper('.day-cnt', {
        slidesPerView: 'auto',
        //centeredSlides: true,
        spaceBetween: 10,
  			initialSlide:0,
        prevButton: '.day-cnt .prev',
        nextButton: '.day-cnt .next'
      });
      // var usePlayDate = DTRYX.Common.PlayDateList.find((ddate) =>{
      //   return ddate.PlaySDT === DTRYX.Common.SelDate
      // });
      // var usePlayDate = DTRYX.Common.PlayDateList.find(function(ddate) {
      //   return ddate.PlaySDT == DTRYX.Common.SelDate
      // });    
      
      var usePlayDate = null;
      var idx=0;
      for(var i=0; i<DTRYX.Common.PlayDateList.length; i++){
        var item = DTRYX.Common.PlayDateList[i];
        if(idx==0 && item.PlaySDT == DTRYX.Common.SelDate){
          usePlayDate = item;
          idx++;
        }
      }

      if(usePlayDate != undefined && usePlayDate != null){
        var rowNum = parseInt(usePlayDate.RowNo||0);
        mivieCnt.slideTo(rowNum-1, false,false);
      }
    //}
    DTRYX.Common.LoadingCount = 1;
  }
  catch(err){
    console.log(err);
  }
  finally{}
}

/*상영시간 작성함수*/
function drawPlayDateList(){
  //console.log('drawPlayDateList');
  try{
    var PlayDateList  = DTRYX.Common.PlayDateList||null,
        SelDate       = DTRYX.Common.SelDate||formatDate(new Date()),
        table         = [],
        children      = [],
        chkYearMonth  = '';

    if(PlayDateList != null){
      for(var i=0; i<PlayDateList.length; i++){
        var item = PlayDateList[i];
        var PlaySDT = item.PlaySDT||'',
            splitData = PlaySDT.split('-');
        var active = '';
        if(SelDate == PlaySDT){
          active = 'active';
        }
        var disClass = 'n';
        if(splitData.length==3){
          var yearMonth = splitData[0] + '' + splitData[1];
          var dayNm = getDayNm(PlaySDT);
          var dayIdx = getDayIdx(PlaySDT);
          var dayClass = 'week';
          if(dayIdx == 0){
            dayClass = 'sun';
            disClass = 'nsun'
          }
          else if(dayIdx == 6){
            dayClass = 'sat';
            disClass = 'nsat'
          }
          if(item.HiddenYn.toUpperCase() == 'Y'){
            disClass = 'ndis';
          }
          if(chkYearMonth != yearMonth){
            chkYearMonth = yearMonth;
            children.push('<div class="swiper-slide month"><div class="wrap">'+splitData[1]+'월</div></div>');
            children.push('<div class="swiper-slide day ' + active + '"><a class="wrap" href="#none" data-yyyy-mm-dd="'+PlaySDT+'" onClick="onClickPlayDate(\''+ PlaySDT +'\')"><div class="'+dayClass+'">'+dayNm+'</div><div class="'+disClass+'">'+splitData[2]+'</div></a></div>');
          }
          else{
            children.push('<div class="swiper-slide day ' + active + '"><a class="wrap" href="#none" data-yyyy-mm-dd="'+PlaySDT+'" onClick="onClickPlayDate(\''+ PlaySDT +'\')"><div class="'+dayClass+'">'+dayNm+'</div><div class="'+disClass+'">'+splitData[2]+'</div></a></div>');
          }
        }
      }
      table.push(children.join(''));

      return table;
    }
  }
  catch(err){}
  finally{}
}

/*지역생성함수*/
function drawRegion(){
  console.log('drawRegion');
  try{
    var RegionList  = DTRYX.Common.RegionList||null,
        SelRegionCd = DTRYX.Common.SelRegionCd||'all',
        table       = [],
        children    = [],
        //scinema_area_not = ["KR-26","KR-27","KR-29","KR-30","KR-49","KR-11","KR-31"];
        scinema_area_not = [];
    if(RegionList != null){
      for(var i=0; i<RegionList.length; i++){
        var item = RegionList[i],
            active = '';
        if(item.RegionCd == SelRegionCd){
          active = 'active';
        }
        // 20190419 nhn - 작은영화관에서 사용하지 않는 지역 안보이게 설정 (위에 함수로 추가함)
        if($.inArray(item.RegionCd, scinema_area_not) == -1) {
        children.push('<a href="#none" data-area-id="'+item.RegionCd+'" class="'+active+'" onClick="onClickRegion(\''+ item.RegionCd +'\')">'+item.RegionNm+'</a>');
        }
      }
      table.push(children.join(''));
      return table;
    }
  }
  catch(err){}
  finally{}
}
/*달력생성함수*/
function drawCalendar(){
  try{
    var counter = 1;
    var FebNumberOfDays = "";
    var year = '',
        month = '',
        day = '',
        dateNow = new Date(),
        thisDate = '',
        nextDate = '',
        prevDate = '',
        dayName = '',
        SelDate = DTRYX.Common.SelDate;

        if(SelDate == undefined || SelDate == ''){
          year = dateNow.getFullYear();
          month = dateNow.getMonth();
          day = dateNow.getDate();
        }
        else{
          var splitData = SelDate.split('-');
          if(splitData.length == 3){
            year = splitData[0];
            month =  parseInt(splitData[1])-1;
            day =  splitData[2];
          }
        }
        thisDate = new Date(year, month, 1);

        if (month == 1){
           if ( (year%100!=0) && (year%4==0) || (year%400==0)){
             FebNumberOfDays = 29;
           }else{
             FebNumberOfDays = 28;
           }
        }
        var dayPerMonth = ["31", ""+FebNumberOfDays+"","31","30","31","30","31","31","30","31","30","31"]
        var weekdays= thisDate.getDay();
        var weekdays2 = weekdays
        var numOfDays = dayPerMonth[month];
        var children = [];
        var table = [];
        nextDate = new Date(year, month+2, 1);
        prevDate = new Date(year, month, 1);
        nextDate = new Date(nextDate).toISOString().slice(0,8) + '01';
        prevDate = new Date(prevDate).toISOString().slice(0,10);
        month = month+1;
        var inc = 100000;

        DTRYX.Common.PrevMonthDate = prevDate;
        DTRYX.Common.NextMonthDate = nextDate;

        while (weekdays>0){
           children.push('<td></td>');
           weekdays--;
           inc++;
        }

        while (counter <= numOfDays){
          if (weekdays2 > 6){
            weekdays2 = 0;
            table.push('<tr>'+children+'</tr>');
            children = [];
            inc++;
          }
          var imgNm = counter;

          var thisDate = '';
          if(parseInt(counter)<10){
              if(parseInt(month)<10){
                thisDate = year + '-0' + month + '-0' + counter;
              }
              else{
                thisDate = year + '-' + month + '-0' + counter;
              }
          }
          else{
            if(parseInt(month)<10){
              thisDate = year + '-0' + month + '-' + counter;
            }
            else{
              thisDate = year + '-' + month + '-' + counter;
            }
          }

          //SaleSDT
          if (counter == day){
          //if (thisDate == SaleSDT){
            imgNm = imgNm + "_on";
            var imgurl = 'https://www.cinecube.co.kr/common/images/main/num_'+imgNm+'.png';
            children.push('<td class="on"><a href="javascript:onSelectedDate(\''+thisDate+'\')"><img src="'+imgurl+'" alt="'+thisDate+'"/></a></td>');
          }
          else{
            var imgurl = 'https://www.cinecube.co.kr/common/images/main/num_'+imgNm+'.png';
            children.push('<td><a href="javascript:onSelectedDate(\''+thisDate+'\')"><img src="'+imgurl+'" alt="'+thisDate+'"/></a></td>');
          }
          weekdays2++;
          counter++;
          inc++;
        }
        if(weekdays2<=7){
          for(var i=weekdays2; i<=7; i++){
            children.push('<td></td>');
            inc++;
          }
          table.push('<tr>'+children+'</tr>');
          inc++;
        }

        return table;

  }
  catch(err){}
  finally{}
}
/*날짜 YYYY-MM-DD 형식으로 생성해주는 함수*/
function formatDate(date) {
  var d     = new Date(date),
      month = '' + (d.getMonth() + 1),
      day   = '' + d.getDate(),
      year  = d.getFullYear();
  if (month.length < 2){
    month = '0' + month;
  }

  if (day.length < 2){
    day = '0' + day;
  }

  return [year, month, day].join('-');
}
/*요일구하는 함수*/
function getDayNm(thisdate){
  var week = new Array('일', '월', '화', '수', '목', '금', '토');
  var today = new Date(thisdate).getDay();
  var todayLabel = week[today];
  return todayLabel;
}

function getDayIdx(thisdate){
  var today = new Date(thisdate).getDay();
  return today;
}

/*상영일자 클릭처리 함수*/
function onClickPlayDate(playDate){
  console.log('onClickPlayDate');
  try{
    
    DTRYX.Common.SelDate=playDate;
    if(DTRYX.Common.ICinemaCd.toUpperCase() == 'ALL'){
      getTimeTableShowSeqInfo();
    }
    else{
      getTimeTableInfo();
    }
  }
  catch(err){}
  finally{}
}
/*영화관 지역정보 클릭처리 함수*/
function onClickRegion(regionCd){
  try{
    DTRYX.Common.SelRegionCd=regionCd;
    if(DTRYX.Common.ICinemaCd.toUpperCase() == 'ALL'){
      getTimeTableShowSeqInfo();
    }
    else{
      getTimeTableInfo();
    }
  }
  catch(err){}
  finally{}
}
/*예매 모바일, desktop 분기처리함수*/
function DTRYXBooking(cinemaCd, movieCd, showSeq, playSDT, screenCd){
  try{
    var ChannelCd = 'homepage';
    /*모바일사이즈여부 체크*/
    var w = window.innerWidth;
    /*모바일사이즈*/
    if(w < 784){
      DTRYX.Common.ChannelCd='mobileweb';
      var url = getDtryxBookingURL('MOBILE');
      window.open(url, '_blank');
    }
    else{
      DTRYX.Common.ChannelCd='homepage';
      /*회원정보존재여부*/
      if(DTRYX.Common.MemberNm == ''
        || DTRYX.Common.MobileNoFull == ''
        || DTRYX.Common.BirthdayDT == ''){
          $('#popReg').bPopup();
      }
      else{
        var url = getDtryxBookingURL('HOME');
        BookingOpen(url);
      }
    }
  }
  catch(err){
    console.log(err);
  }
  finally{}
}
/*예매내역확인 (모바일,홈페이지 분기처리함수)*/
function DTRYXBookingRsv(){
  try{
    var ChannelCd = 'homepage';
    /*모바일사이즈여부 체크*/
    var w = window.innerWidth;
    /*모바일사이즈*/
    if(w < 784){
      DTRYX.Common.ChannelCd='mobileweb';
      var url = getDtryxBookingRsvURL('MOBILE');
      window.open(url, '_blank');
    }
    else{
      DTRYX.Common.ChannelCd='homepage';
      /*회원정보존재여부*/
      if(DTRYX.Common.MemberNm == ''
        || DTRYX.Common.MobileNoFull == ''
        || DTRYX.Common.BirthdayDT == ''){
          $('#popRegR').bPopup();
      }
      else{
        var url = getDtryxBookingRsvURL('HOME');
        BookingOpenR(url);
      }
    }
  }
  catch(err){
    console.log(err);
  }
  finally{}
}

/*예매URL생성함수*/
function getDtryxBookingURL(URLType){
  try{
    var URL = '';
    var MemberYn = 'N';
    // if(DTRYX.Common.AgreeType != '' && DTRYX.Common.AgreeType.toUpperCase() != 'N'){
    //   MemberYn = 'Y';
    // }
    if(DTRYX.Common.MemberNm != ''){
      MemberYn = 'Y';
    }
    if(URLType.toUpperCase() == 'MOBILE'){
      URL = DTRYX.Common.MobileURL;
    }
    else if(URLType.toUpperCase() == 'HOME'){
      URL = DTRYX.Common.HomeURL;
    }
    else{
      alert('예매타입이 잘못되었습니다.');
      return;
    }
    var params = 'MemberYn='+MemberYn;
    params = params + '&MemberNm='+DTRYX.Common.MemberNm;
    params = params + '&BirthdayDT='+DTRYX.Common.BirthdayDT;
    params = params + '&MobileNoFull='+DTRYX.Common.MobileNoFull;
    params = params + '&CertiNo='+DTRYX.Common.CertiNo;
    params = params + '&MemberGuID='+DTRYX.Common.MemberGuID;
    params = params + '&PublicID='+DTRYX.Common.PublicID;
    params = params + '&ChannelCd='+DTRYX.Common.ChannelCd;
    params = params + '&CompanyGuID='+DTRYX.Common.CompanyGuID;
    params = params + '&CinemaCd='+DTRYX.Common.CinemaCd;
    params = params + '&MovieCd='+DTRYX.Common.SelectedMovieCd;
    params = params + '&PlaySDT='+DTRYX.Common.SelDate;
    params = params + '&ScreenCd='+DTRYX.Common.ScreenCd;
    params = params + '&ShowSeq='+DTRYX.Common.ShowSeq;

    URL = URL + '/?params=' + escape(params);
    return URL;
  }
  catch(err){
    console.log(err);
  }
  finally{}
}
/*예매내역확인 URL 생성함수*/
function getDtryxBookingRsvURL(URLType){
  try{
    var URL = '';
    var MemberYn = 'N';
    // if(DTRYX.Common.AgreeType != '' && DTRYX.Common.AgreeType.toUpperCase() != 'N'){
    //   MemberYn = 'Y';
    // }
    if(DTRYX.Common.MemberNm != ''){
      MemberYn = 'Y';
    }
    if(URLType.toUpperCase() == 'MOBILE'){
      URL = DTRYX.Common.MobileRsvURL;
    }
    else if(URLType.toUpperCase() == 'HOME'){
      URL = DTRYX.Common.HomeRsvURL;
    }
    else{
      alert('예매타입이 잘못되었습니다.');
      return;
    }
    var params = 'MemberYn='+MemberYn;
    params = params + '&MemberNm='+DTRYX.Common.MemberNm;
    params = params + '&BirthdayDT='+DTRYX.Common.BirthdayDT;
    params = params + '&MobileNoFull='+DTRYX.Common.MobileNoFull;
    params = params + '&CertiNo='+DTRYX.Common.CertiNo;
    params = params + '&MemberGuID='+DTRYX.Common.MemberGuID;
    params = params + '&PublicID='+DTRYX.Common.PublicID;
    params = params + '&ChannelCd='+DTRYX.Common.ChannelCd;
    params = params + '&CompanyGuID='+DTRYX.Common.CompanyGuID;
    params = params + '&CinemaCd='+DTRYX.Common.CinemaCd;

    URL = URL + '?params=' + escape(params);
    return URL;
  }
  catch(err){
    console.log(err);
  }
  finally{}
}
/*비회원 예매 관련 정보체크함수*/
function DTRYXNonMemberBooking(){
  console.log('DTRYXNonMemberBooking')
  var AgreeChk = $('#popReg').find('input[name="AgreeChk"]').is(":checked");
  var MemberNm = $('#popReg').find('input[name="MemberNm"]').val();
  var MobileNo01 = $('#popReg').find('select[name="MobileNo01"]').val();
  var MobileNo02 = $('#popReg').find('input[name="MobileNo02"]').val();
  var MobileNo03 = $('#popReg').find('input[name="MobileNo03"]').val();
  var BirthdayDT = $('#popReg').find('input[name="BirthdayDT"]').val();
  var CertiNo = $('#popReg').find('input[name="CertiNo"]').val();

  /*Validation Check*/
  if(AgreeChk == false){
    alert('개인정보수집에 동의하셔야 예매를 하실 수 있습니다.');
    $('#popReg').find('input[name="AgreeChk"]').focus();
    return;
  }
  if(MemberNm == ''){
    alert('이름을 입력해주세요.');
    $('#popReg').find('input[name="MemberNm"]').focus();
    return;
  }
  if(MobileNo01 == ''){
    alert('휴대전화 앞자리를 선택해주세요.');
    $('#popReg').find('select[name="MobileNo01"]').focus();
    return;
  }
  if(MobileNo02 == ''){
    alert('휴대전화 두번째를 입력해주세요.');
    $('#popReg').find('input[name="MobileNo02"]').focus();
    return;
  }
  if(MobileNo02.legend<3){
    alert('휴대전화 두번째 정보를 다시 확인해주세요.');
    $('#popReg').find('input[name="MobileNo02"]').focus();
    return;
  }
  if(MobileNo03 == ''){
    alert('휴대전화 세번째를 입력해주세요.');
    $('#popReg').find('input[name="MobileNo03"]').focus();
    return;
  }
  if(BirthdayDT == ''){
    alert('생년월일을 입력해주세요.');
    $('#popReg').find('input[name="BirthdayDT"]').focus();
    return;
  }
  if(CertiNo == ''){
    alert('비밀번호를 입력해주세요.');
    $('#popReg').find('input[name="CertiNo"]').focus();
    return;
  }
  // DTRYX.Common.AgreeType = 'Y';
  // DTRYX.Common.MemberYn = 'N';
  // DTRYX.Common.MemberNm = MemberNm;
  // DTRYX.Common.BirthdayDT = BirthdayDT;
  // DTRYX.Common.MobileNoFull = MobileNo01+MobileNo02+MobileNo03;
  // DTRYX.Common.CertiNo = CertiNo;

  // var url = getDtryxBookingURL('HOME');

  var params = 'MemberYn=N';
      params = params + '&MemberNm='+MemberNm;
      params = params + '&BirthdayDT='+BirthdayDT;
      params = params + '&MobileNoFull='+MobileNo01+MobileNo02+MobileNo03;
      params = params + '&CertiNo='+CertiNo;
      params = params + '&MemberGuID=';
      params = params + '&PublicID=';
      params = params + '&ChannelCd='+DTRYX.Common.ChannelCd;
      params = params + '&CompanyGuID='+DTRYX.Common.CompanyGuID;
      params = params + '&CinemaCd='+DTRYX.Common.CinemaCd;
      params = params + '&MovieCd='+DTRYX.Common.SelectedMovieCd;
      params = params + '&PlaySDT='+DTRYX.Common.SelDate;
      params = params + '&ScreenCd='+DTRYX.Common.ScreenCd;
      params = params + '&ShowSeq='+DTRYX.Common.ShowSeq;

  var url = DTRYX.Common.HomeURL + '/?params=' + escape(params);
  BookingOpen(url);
  callBpopClose('#popReg');
}
/*비회원 예매확인 정보체크함수*/
function DTRYXNonMemberReservation(){
  console.log('DTRYXNonMemberReservation')

  try{
    /**예매팝업 카운트 증가처리 */
    var ReservationCount = parseInt(DTRYX.Common.ReservationCount);
    DTRYX.Common.ReservationCount = ReservationCount++;
  
    var AgreeChk = $('#popRegR').find('input[name="AgreeChk"]').is(":checked");
    var MemberNm = $('#popRegR').find('input[name="MemberNm"]').val();
    var MobileNo01 = $('#popRegR').find('select[name="MobileNo01"]').val();
    var MobileNo02 = $('#popRegR').find('input[name="MobileNo02"]').val();
    var MobileNo03 = $('#popRegR').find('input[name="MobileNo03"]').val();
    var BirthdayDT = $('#popRegR').find('input[name="BirthdayDT"]').val();
    var CertiNo = $('#popRegR').find('input[name="CertiNo"]').val();

    /*Validation Check*/
    if(AgreeChk == false){
      alert('개인정보수집에 동의하셔야 예매를 확인 하실 수 있습니다.');
      $('#popRegR').find('input[name="AgreeChk"]').focus();
      return;
    }
    if(MemberNm == ''){
      alert('이름을 입력해주세요.');
      $('#popRegR').find('input[name="MemberNm"]').focus();
      return;
    }
    if(MobileNo01 == ''){
      alert('휴대전화 앞자리를 선택해주세요.');
      $('#popRegR').find('select[name="MobileNo01"]').focus();
      return;
    }
    if(MobileNo02 == ''){
      alert('휴대전화 두번째를 입력해주세요.');
      $('#popRegR').find('input[name="MobileNo02"]').focus();
      return;
    }
    if(MobileNo02.legend<3){
      alert('휴대전화 두번째 정보를 다시 확인해주세요.');
      $('#popRegR').find('input[name="MobileNo02"]').focus();
      return;
    }
    if(MobileNo03 == ''){
      alert('휴대전화 세번째를 입력해주세요.');
      $('#popRegR').find('input[name="MobileNo03"]').focus();
      return;
    }
    if(BirthdayDT == ''){
      alert('생년월일을 입력해주세요.');
      $('#popRegR').find('input[name="BirthdayDT"]').focus();
      return;
    }
    if(CertiNo == ''){
      alert('비밀번호를 입력해주세요.');
      $('#popRegR').find('input[name="CertiNo"]').focus();
      return;
    }
    // DTRYX.Common.AgreeType = 'Y';
    // DTRYX.Common.MemberYn = 'N';
    // DTRYX.Common.MemberNm = MemberNm;
    // DTRYX.Common.BirthdayDT = BirthdayDT;
    // DTRYX.Common.MobileNoFull = MobileNo01+MobileNo02+MobileNo03;
    // DTRYX.Common.CertiNo = CertiNo;

    // var url = getDtryxBookingURL('HOME');
    var MobileNoFull = MobileNo01+MobileNo02+MobileNo03;
    var params = {
      //BrandCd:DTRYX.Common.BrandCd,
      BrandCd:'scinema',
      KindCd:'now',
      MemberYn:'N',
      MemberGuID:'',
      MemberNm:MemberNm,
      PublicID:'',
      MobileNo:MobileNoFull,
      BirthdayDT:BirthdayDT,
      CertiNo:CertiNo,
      ImgSize:'small',
      ChannelCd:DTRYX.Common.CinemaCd,
      WorkGuID:DTRYX.Common.WorkGuID,
      EngVerYn:'N',
      Page:1,
      Limit:10
      };
    
      $.ajax({
        url:DTRYX.Common.BaseURL + '/online/pay/tran-search',
        headers : {"Content-Type" : DTRYX.Common.ContentType, "Authorization" : DTRYX.Common.Authorization},
        dataType:'json',
        data:params,
        type:"GET",
        success:function(data){
          //alert(data.Recordset.ShowSeqList[0].CinemaCd);
          if(data == undefined || data == null)
          {
            alert('예매정보가 없습니다.');
            return;
          }

          if(data.RetCode.toUpperCase() == 'SUCCESS' && data.Recordset.length>0){
            DTRYXNonMemberRsvForward();
          }
          else{
            alert('예매정보가 없습니다.');
            return;
          }
        },
        error:function(e, o){
          alert('[' + e.status + '] ' + o);
        }
    })        
}
catch(err){}
finally{}
}

/*비회원 예약페이지로 이동 -20190822 추가*/
function DTRYXNonMemberRsvForward(){

  try{
  
    var AgreeChk = $('#popRegR').find('input[name="AgreeChk"]').is(":checked");
    var MemberNm = $('#popRegR').find('input[name="MemberNm"]').val();
    var MobileNo01 = $('#popRegR').find('select[name="MobileNo01"]').val();
    var MobileNo02 = $('#popRegR').find('input[name="MobileNo02"]').val();
    var MobileNo03 = $('#popRegR').find('input[name="MobileNo03"]').val();
    var BirthdayDT = $('#popRegR').find('input[name="BirthdayDT"]').val();
    var CertiNo = $('#popRegR').find('input[name="CertiNo"]').val();
  
    var params = 'MemberYn=N';
    params = params + '&MemberNm='+MemberNm;
    params = params + '&BirthdayDT='+BirthdayDT;
    params = params + '&MobileNoFull='+MobileNo01+MobileNo02+MobileNo03;
    params = params + '&CertiNo='+CertiNo;
    params = params + '&MemberGuID=';
    params = params + '&PublicID=';
    params = params + '&ChannelCd='+DTRYX.Common.ChannelCd;
    params = params + '&CompanyGuID='+DTRYX.Common.CompanyGuID;
    params = params + '&CinemaCd='+DTRYX.Common.CinemaCd;
  
  var url = DTRYX.Common.HomeRsvURL + '?params=' + escape(params);
  
  // $(".bPopup .pop-if").hide();
  // $(".bPopup .pop-ifR").show();
  //callBpopClose('#bPopupPopIf');
  BookingOpenR(url);
  callBpopClose('#popRegR');
  }
  catch(err){}
  finally{} 
}

/*예매팝업 닫기처리함수*/
function callBpopClose(classId){
  try{
    var popup = $(classId).bPopup();
    if(popup.length>1){
      for(var i=(popup.length-1);i>0;i--){
        if (typeof popup[i].remove === 'function') {
          popup[i].remove();
        } else {
          popup[i].parentNode.removeChild(popup[i]);
        } 
      }
    }
    popup.close();
  }
  catch(err){console.log('err==>'+err)}
  finally{}
}
/*예매내역출력처리함수*/
function BookingOpenR(url){
  $('.pop-if').bPopup({
    content:'iframe',
    contentContainer:'.content',
    iframeAttr:('scrolling="yes" frameborder="0" width="100%" height="100%"'), 
    loadUrl:url,
    modalClose: false,
    opacity: 0.8
  });

  if (DTRYX.Common.MemberGuID === '') {
    $('.pop-if').css('top', '80px');
  }
}
/*빠른예매 Desk Top처리*/
function BookingOpen(url){
  $('.pop-if').bPopup({
    content:'iframe',
    contentContainer:'.content',
    loadUrl:url,
    modalClose: false,
    opacity: 0.8
  });

  if (DTRYX.Common.MemberGuID === '') {
    $('.pop-if').css('top', '80px');
  }
}
/**빠른예매 닫기 */
function callBpopClose2(classId){
  try{
    popRemoveIframe();
    var popup = $(classId).bPopup();
    if(popup.length>1){
      for(var i=(popup.length-1);i>0;i--){
        if (typeof popup[i].remove === 'function') {
          popup[i].remove();
        } else {
          popup[i].parentNode.removeChild(popup[i]);
        }
      }
    }
    popup.close();
  }
  catch(err){console.log('err==>'+err)}
  finally{}
}
/**빠른예매 중복방지 */
function popRemoveIframe(){
  try{
    var ipopup = $('.pop-if>.content>.b-iframe');
    if(ipopup.length>0){
      for(var i=0;i<ipopup.length;i++){
        if (typeof ipopup[i].remove === 'function') {
          ipopup[i].remove();
        } else {
          ipopup[i].parentNode.removeChild(ipopup[i]);
        }        
      }
    }
  }
  catch(err){}
  finally{}
}
 