(function($){

	var _pb_movie_screen_timetable = (function(target_, options_){
		this._target = target_;
		this._options = $.extend({
			// 'show_area_id' : true,
		}, options_);

		this._days_slider = this._target.find(".movie-days-slider");
		this._days_slider_module = new Swiper(this._days_slider[0], {
			speed: 400,
			spaceBetween: 15,
			prevButton: this._days_slider.find(".slider-button-prev")[0],
			nextButton: this._days_slider.find(".slider-button-next")[0],
			slidesPerView : "auto",
			slideToClickedSlide : true,
			centeredSlides : true,
			clickable : true,
		});

		moment.locale('ko');

		var default_target_date_ = (this._target.attr("data-default-target-date") ? moment(this._target.attr("data-default-target-date"), "YYYYMMDD") : moment());
		
		this._selected_data = {
			'target_date' : default_target_date_,
			'screen_id' : (this._target.attr("data-default-screen-id") ? this._target.attr("data-default-screen-id") : null),
			'open_id' : (this._target.attr("data-default-open-id") ? this._target.attr("data-default-open-id") : null),
			'area_id' : (this._target.attr("data-default-area-id") ? this._target.attr("data-default-area-id") : null),
		};

		var day_srt_date_ = moment();
		var last_month_ = null;

		for(var index_=0; index_<20; ++index_){

			var is_today_ = this._selected_data['target_date'].isSame(day_srt_date_, "day");

			if(!last_month_ || !last_month_.isSame(day_srt_date_, 'month')){
				last_month_ = day_srt_date_.clone();
				var month_html_ = '<div class="swiper-slide">' +
					'<div class="month-item"><div class="wrap">' +
						'<div class="month">'+last_month_.format('MMM')+'</div>' +
					'</div></div>' +
				'</div>';	

				this._days_slider_module.appendSlide(month_html_);
			}

			var day_name_ = day_srt_date_.format("ddd");
			var day_ = day_srt_date_.format("DD");

			var day_html_ = '<div class="swiper-slide ">' + 
				'<a class="day-item '+(is_today_ ? "active" : "")+'" href="#" data-yyyymmdd="'+day_srt_date_.format("YYYYMMDD")+'"><div class="wrap">' + 
					'<div class="day-name">'+day_srt_date_.format('dd')+'</div>' + 
					'<div class="day">'+day_srt_date_.format('DD')+'</div>' + 
				'</div></a>' +
			'</div>';

			this._days_slider_module.appendSlide(day_html_);

			day_srt_date_.add(1, 'days');
		}

		var day_items_ = this._days_slider.find(".day-item");

		var that_ = this;
		day_items_.each(function(){
			var day_item_ = $(this);

			day_item_.click($.proxy(function(event_){
				this.target_date(moment($(event_.currentTarget).attr("data-yyyymmdd"), "YYYYMMDD"), true);
				return false;
			}, that_));
		});

		this._cinema_area_list = this._target.find(".cinema-area-list");
		var area_items_ = this._cinema_area_list.find(".area-item");

		area_items_.click($.proxy(function(event_){
			var area_item_ = $(event_.currentTarget);
			var area_id_ = area_item_.attr("data-area-id");
			this.area_id(area_id_, true);

			return false;
		}, this));

		this._selected_data['area_id'] = this._cinema_area_list.find(".area-item.active").attr("data-area-id");
		this._timetable_list = this._target.find(".timetable-list");
		this.find_cinema();

		this._target.data("pb-movie-screen-timetable-module", this);
	});

	_pb_movie_screen_timetable.prototype.target = (function(){
		return this._target;
	});

	_pb_movie_screen_timetable.prototype.options = (function(options_){
		if(options_ !== undefined){
			this._options = $.extend(true, this._options, options_);
		}

		return this._options;
	});

	_pb_movie_screen_timetable.prototype.target_date = (function(target_date_, do_find_){
		if(target_date_ !== undefined){
			this._selected_data['target_date'] = target_date_.clone();

			this._days_slider.find(".day-item").toggleClass("active", false);
			this._days_slider.find(".day-item[data-yyyymmdd='"+this._selected_data['target_date'].format("YYYYMMDD")+"']").toggleClass("active", true);

			if((do_find_ !== undefined ? do_find_ : true)){
				this.find_cinema();
			}
		}

		return this._selected_data['target_date'];
	});

	_pb_movie_screen_timetable.prototype.area_id = (function(area_id_, do_find_){
		if(area_id_ !== undefined){
			this._selected_data['area_id'] = area_id_;

			this._cinema_area_list.find(".area-item").toggleClass("active", false);
			this._cinema_area_list.find(".area-item[data-area-id='"+this._selected_data['area_id']+"']").toggleClass("active", true);

			if((do_find_ !== undefined ? do_find_ : true)){
				this.find_cinema();
			}
		}

		return this._selected_data['target_date'];
	});

	_pb_movie_screen_timetable.prototype.find_cinema = (function(){

		if(this._ajax_instance){
			this._ajax_instance.abort();
		}

		this._timetable_list.empty();
		this._timetable_list.append("<div class='loading-wrap'><div class='pb-loading-indicator'></div></div>")

		this._ajax_instance = PB.post({
			action : "movie-screen-load-timetable",
			target_date : this._selected_data['target_date'].format("YYYYMMDD"),
			area_id : this._selected_data['area_id'],
			screen_id : this._selected_data['screen_id'],
			open_id : this._selected_data['open_id'],
		}, $.proxy(function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				this._timetable_list.empty();
				this._timetable_list.append("<div class='message'>시간표를 검색 중 에러가 발생하였습니다.</div>");
				return;
			}	

			this._timetable_list.empty();

			if(response_json_.timetable.length <= 0){
				this._timetable_list.append("<div class='message'>등록된 상영시간표가 없습니다.</div>");
				return;
			}

			var last_cinema_id_ = null;
			var last_hall_ = null;
			var last_timetable_row_ = null;
			var module_ = this;


			var timetable_ = [];

			$.each(response_json_.timetable, function(key_, schedule_data_){
				timetable_.push(schedule_data_);
			});

			timetable_.sort(function(a_, b_){
				var akey_ = a_['cinema_name']+a_['hall']+a_['time'];
				var bkey_ = b_['cinema_name']+b_['hall']+b_['time'];
				return akey_.localeCompare(bkey_);

			});

			$.each(timetable_, function(schedule_data_){
				var schedule_data_ = this;

				if(last_cinema_id_ !== schedule_data_['cinema_id'] || last_hall_ !== schedule_data_['hall']){
					var row_html_ = '<div class="timetable-item">' +
						'<div class="col col-cinema-info"><div class="cinema-info-table">' +
							(schedule_data_['cinema_logo_image_url'] ? '<div class="col cinema-image"><img src="'+schedule_data_['cinema_logo_image_url']+'"></div>' : '<div class="col cinema-image"></div>') +
							'<div class="col cinema-name">'+schedule_data_['cinema_name']+'</div>' +
							'<div class="col hall-name">'+schedule_data_['hall']+'</div>' +
						'</div></div>' +
						'<div class="col col-timetable"></div>' +
						'</div>';	

					last_timetable_row_ = $(row_html_);
					module_._timetable_list.append(last_timetable_row_);

					last_cinema_id_ = schedule_data_['cinema_id'];
					last_hall_ = schedule_data_['hall'];
				}


				if(schedule_data_["available_yn"] === "N" || schedule_data_["seat"] === "매진"){
					var timetable_item_ = $('<span class="pb-timetable-item expired">'+schedule_data_['time']+'</span>');
					last_timetable_row_.find(".col-timetable").append(timetable_item_);	
				}else if(schedule_data_["external_link"] !== undefined && schedule_data_["external_link"] !== "" && schedule_data_["external_link"] !== null){
					var timetable_item_ = $('<a href="'+schedule_data_["external_link"]+'" class="pb-timetable-item" target="_blank">'+
							schedule_data_["external_link_text"] +
						'</a>');

					timetable_item_.data("original-url", schedule_data_["external_link"]);
					last_timetable_row_.find(".col-timetable").append(timetable_item_);

					timetable_item_.click($.proxy(function(event_){
						var target_link_ = $(event_.currentTarget);
						var original_url_ = target_link_.data("original-url");

						var target_date_moment_ = this.target_date();

						target_link_.attr("href", original_url_+"?in_ss_date="+target_date_moment_.format("YYYY-MM-DD"));

						return true;

					}, module_));
				}else{
					var timetable_item_ = $('<a href="#" data-schedule-id="'+schedule_data_['id']+'" class="pb-timetable-item">'+
						schedule_data_['time']+
						' <small>'+schedule_data_['seat']+'</small>'+
						'</a>');

					last_timetable_row_.find(".col-timetable").append(timetable_item_);
					timetable_item_.data("cinema-id", schedule_data_['cinema_id']);

					timetable_item_.click($.proxy(function(event_){
						var target_btn_ =  $(event_.currentTarget);
						var schedule_id_ = target_btn_.attr("data-schedule-id");
						var cinema_id_ = target_btn_.data("cinema-id");
						this.do_ticketing(schedule_id_, cinema_id_);
						return false;
					}, module_));
				}

					

			});

		}, this));

	});

	//XTicket 임시코드
	_pb_movie_screen_timetable.prototype.do_ticketing = (function(schedule_id_, cinema_id_){
		PB.movie.open_xticket_popup(schedule_id_, cinema_id_);		
	});

	$.fn.pb_movie_screen_timetable = (function(options_){
		var module_ = this.data('pb-movie-screen-timetable-module');
		if(module_) return module_;
		return new _pb_movie_screen_timetable(this, options_);
	});

})(jQuery);