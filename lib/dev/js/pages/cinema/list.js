jQuery(document).ready(function($){
	var list_form_ = $("#pb-cinema-list-form");
	var area_link_items_ = $("[data-location-link]");

	area_link_items_.click(function(){
		var area_id_ = $(this).attr("data-location-link");

		list_form_.find("[name='area_id'] option").prop("selected", false);
		list_form_.find("[name='area_id'] option[value='"+area_id_+"']").prop("selected", true);
		list_form_.find("[name='keyword']").val("");

		list_form_.submit();

		return false;
	});
});