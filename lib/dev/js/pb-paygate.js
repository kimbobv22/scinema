$(function($){

	PB.paygate = $.extend(PB.paygate,{
		request : function(params_, callback_){

			params_ = $.extend({
				pg : 	PBVAR['paygate_pg_company'],
				escrow : (PBVAR['paygate_escrow'] === "Y"),
				m_redirect_url : PBVAR['paygate_iamport_payment_redirect_page_id']
			}, params_);
		
			PB.indicator(true, function(){
				IMP.request_pay(params_, function(response_){
					PB.indicator(false, function(){
						callback_.apply(this,[(response_.success === true), response_]);	
					});
				});
			});
		}
	});

	IMP.init(PBVAR['paygate_user_code']);

});