jQuery(document).ready(function($){
	
	var edit_form_ = $("#pb-templated-edit-edit-form");
	edit_form_.data('adminpage-menu-id', edit_form_.attr("data-adminpage-menu-id"));

	edit_form_.validator();
	edit_form_.submit_handler(pb_templated_edit_submit);

	edit_form_.find("[data-remove-btn]").click(function(){
		pb_templated_edit_remove();
		return false;
	});

	var menu_id_ = edit_form_.data('adminpage-menu-id');

	var datepicker_list_ = $("[data-adminpage-datepicker]");

	if(datepicker_list_.length > 0){
		datepicker_list_.each(function(){
			var datepicker_ = $(this);

			datepicker_.datetimepicker({
				locale : datepicker_.attr("data-locale"),
				format : datepicker_.attr("data-format")
			});
		});
	}

	var editor_image_upload_btns_ = edit_form_.find("[data-wp-editor-image-upload-btn]");

	editor_image_upload_btns_.each(function(){
		var target_btn_ = $(this);
	
		target_btn_.pb_image_uploader_btn({
			callback : $.proxy(function(files_){
				var t_target_btn_ = $(this);
				var editor_id_ = t_target_btn_.attr("data-wp-editor-image-upload-btn");
				var editor_instance_ = tinyMCE.get(editor_id_);

				$.each(files_, function(){
					editor_instance_.execCommand('mceInsertRawHTML', false, '<img src="'+this['url']+'">');
				});

				
			}, target_btn_)
		});
	});


	var imagepicker_list_ = $("[data-adminpage-template-imagepicker]");
	imagepicker_list_.each(function(){
		var imagepicker_ = $(this);

		console.log(imagepicker_);

			imagepicker_.pb_multiple_image_uploader({
				'max_width' : imagepicker_.attr("data-maxwidth"),
				'maxlength' : imagepicker_.attr("data-maxlength"),
				'modal_label' : imagepicker_.attr("data-upload-label"),
				'change' : function(){
					var module_ = this;
					var target_input_el_ = $(module_._target.attr("data-adminpage-template-imagepicker"));

					var data_type_ = this._target.attr("data-type");


					if(data_type_ === "string"){
						var target_data_ = this.to_json();

						if(target_data_.length <= 0){
							target_input_el_.val("");
						}else{
							var image_data_ = target_data_[0];
							target_input_el_.val(image_data_['url']);
						}
					}else{
						target_input_el_.val(module_._target.val());
					}

				}
			});

	});

	function _pb_adminpage_templated_build_file_item(el_){
		$ = jQuery;
		var target_el_ = $(el_);

		target_el_.find("[data-file-item-delete]").click(function(event_){

			PB.confirm_q({
				title : "삭제확인",
				content : "첨부파일을 삭제하시겠습니까?",
				button1 : "삭제하기"
			}, function(confirmed_){
				if(!confirmed_) return;

				$(event_.currentTarget).closest(".file-item").remove();
			});

			return false;
		});
	}

	var filepicker_list_ = $("[data-adminpage-template-filepicker]");
	filepicker_list_.each(function(){
		var filepicker_ = $(this);

			filepicker_.pb_multiple_file_uploader({
				'maxlength' : filepicker_.attr("data-maxlength"),
				'modal_label' : filepicker_.attr("data-upload-label"),
				'change' : function(){
					var module_ = this;
					var target_input_el_ = $(module_._target.attr("data-adminpage-template-filepicker"));

					var data_type_ = this._target.attr("data-type");


					if(data_type_ === "string"){
						var target_data_ = this.to_json();

						if(target_data_.length <= 0){
							target_input_el_.val("");
						}else{
							var image_data_ = target_data_[0];
							target_input_el_.val(image_data_['url']);
						}
					}else{
						target_input_el_.val(module_._target.val());
					}

				}
			});

	});


	var ajaxselect_list_ = $("[data-adminpage-ajaxselect]");

	ajaxselect_list_.each(function(){
		var selectize_el_ = $(this);

		var default_options_ = selectize_el_.attr("data-default-data");

		if(default_options_ && default_options_ !== ""){
			default_options_ = [JSON.parse(default_options_)];
		}else{
			default_options_ = [];
		}

		var additional_params_ = selectize_el_.attr("data-additional-params");
			additional_params_ = additional_params_.split("|");


		selectize_el_.selectize({
			valueField : selectize_el_.attr("data-value-field"),
			labelField : selectize_el_.attr("data-label-field"),
			searchField : selectize_el_.attr("data-label-field"),
			create: false,
			options : default_options_,
			load : $.proxy(function(query_, callback_){
				if(!query_.length) return callback_();

				var form_data_ = edit_form_.serialize_object();
				var t_additional_params_ = {};

				for(var p_index_=0;p_index_<this['additional_params'].length;++p_index_){
					var param_name_ = this['additional_params'][p_index_];

					t_additional_params_[param_name_] = form_data_[param_name_];
				}

				PB.post($.extend(t_additional_params_,{
					action : "adminpage-templated-selectize-load",
					menu_id : menu_id_,
					column_name : this['selectize_el'].attr("name"),
					query : query_
				}), $.proxy(function(result_, response_json_){
					var t_callback_ = this['callback'];
					var t_selectize_el_ = this['selectize_el'];

					if(!result_ || response_json_.success !== true){
						t_callback_();
						return;
					}

					t_callback_(response_json_.options);

				},{selectize_el : selectize_el_, callback : callback_}));
			},{selectize_el : selectize_el_, additional_params : additional_params_}),
			render : {
				option : $.proxy(function(item_, escape_){

					var render_template_html_ = this.attr("data-render-template");

					$.each(item_, function(key_, value_){
						var replace_regex_ = new RegExp("\\{\\$"+key_+"\\}", 'gi');

						render_template_html_ = render_template_html_.replace(replace_regex_, escape_(value_));
					});

					return render_template_html_;
				},selectize_el_)
			}
		});

	});
	$('#select-repo').selectize({
    valueField: 'url',
    labelField: 'name',
    searchField: 'name',
    create: false,
    render: {
        option: function(item, escape) {
            return '<div>' +
                '<span class="title">' +
                    '<span class="name"><i class="icon ' + (item.fork ? 'fork' : 'source') + '"></i>' + escape(item.name) + '</span>' +
                    '<span class="by">' + escape(item.username) + '</span>' +
                '</span>' +
                '<span class="description">' + escape(item.description) + '</span>' +
                '<ul class="meta">' +
                    (item.language ? '<li class="language">' + escape(item.language) + '</li>' : '') +
                    '<li class="watchers"><span>' + escape(item.watchers) + '</span> watchers</li>' +
                    '<li class="forks"><span>' + escape(item.forks) + '</span> forks</li>' +
                '</ul>' +
            '</div>';
        }
    },
    score: function(search) {
        var score = this.getScoreFunction(search);
        return function(item) {
            return score(item) * (1 + Math.min(item.watchers / 100, 1));
        };
    },
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: 'https://api.github.com/legacy/repos/search/' + encodeURIComponent(query),
            type: 'GET',
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.repositories.slice(0, 10));
            }
        });
    }
});

});


function pb_templated_edit_serialize_object(){
	$ = jQuery;
	var edit_form_ = $("#pb-templated-edit-edit-form");
	var target_data_ = edit_form_.serialize_object();

	return pb_apply_filters('pb_templated_edit_serialize_object', target_data_);
}

function pb_templated_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-templated-edit-edit-form");

	if(!window._pb_adminpage_template_wp_editor_requireds){
		window._pb_adminpage_template_wp_editor_requireds = {};
	}

	var wp_editor_has_error_ = false;

	$.each(_pb_adminpage_template_wp_editor_requireds, function(id_, required_message_){
		editor_el_ = $("#"+id_);
		editor_instance_ = tinyMCE.get(id_);
		var editor_content_ = null;

		if(editor_instance_ && !editor_el_.is(':visible')){
			editor_content_ = editor_instance_.getContent();
		}else{
			editor_content_ = editor_el_.val();
		}
			
		if(editor_content_ === null || editor_content_.length <= 0){
			PB.alert_error({
				title : required_message_.error_title,
				content : required_message_.error_message,
			}, function(){
				//todo
			});
			wp_editor_has_error_ = true;
			return false;
		}

	});

	if(wp_editor_has_error_){
		return;
	}


	if(edit_form_.data('add-new') === "Y"){
		PB.confirm_q({
			title : _pb_templated_edit_label_var['add-confirm-title'],
			content : _pb_templated_edit_label_var['add-confirm-message'],
			button1 : _pb_templated_edit_label_var['add']
		}, function(c_){
			if(!c_) return;
			_pb_templated_edit_submit();
		});
	}else{
		_pb_templated_edit_submit();
	}

	
}

function _pb_templated_edit_submit(){
	$ = jQuery;
	var edit_form_ = $("#pb-templated-edit-edit-form");
	var menu_id_ = edit_form_.data('adminpage-menu-id');
	var target_data_ = pb_templated_edit_serialize_object();
	var add_new_ = edit_form_.data('add-new') === "Y";

	var label_success_title_ = add_new_ ? _pb_templated_edit_label_var['add-success-title'] : _pb_templated_edit_label_var['edit-success-title'];
	var label_success_message_ = add_new_ ? _pb_templated_edit_label_var['add-success-message'] : _pb_templated_edit_label_var['edit-success-message'];

	var label_error_title_ = add_new_ ? _pb_templated_edit_label_var['add-error-title'] : _pb_templated_edit_label_var['edit-error-title'];
	var label_error_message_ = add_new_ ? _pb_templated_edit_label_var['add-error-message'] : _pb_templated_edit_label_var['edit-error-message'];

	var wp_editors_ = $(".pb-adminpage-templates-wp-editor");

	if(wp_editors_.length > 0){
		wp_editors_.each(function(){
			var wp_editor_ = $(this);
			var wp_editor_instance_ = tinyMCE.get(wp_editor_.attr("id"));

			if(wp_editor_instance_ && !wp_editor_.is(':visible')){
				target_data_[wp_editor_.attr("name")] = wp_editor_instance_.getContent();
			}else{
				target_data_[wp_editor_.attr("name")] = wp_editor_.val();
			}
		});
	}
/*
	$.each(_pb_adminpage_template_wp_editor_requireds, function(id_, required_message_){
		editor_instance_ = tinyMCE.get(id_);

		var editor_content_ = editor_instance_.getContent();
			
		if(editor_content_ === null || editor_content_.length <= 0){
			PB.alert_error({
				title : required_message_.error_title,
				content : required_message_.error_message,
			}, function(){
				//todo
			});
			wp_editor_has_error_ = true;
			return false;
		}

	});
*/

	PB.post({
		action : "adminpage-templated-edit",
		menu_id : menu_id_,
		add_new : (add_new_ ? "Y" : "N"),
		target_data : target_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || label_error_title_,
				content : response_json_.error_message || label_error_message_,
			});
			return;
		}

		PB.alert_success({
			title : label_success_title_,
			content : label_success_message_,
		}, function(){
			if(edit_form_.attr("data-redirect-to-list") === "Y" && add_new_){
				document.location = edit_form_.attr("data-list-url");
			}else if(edit_form_.attr("data-edit-redirect-to-list") === "Y"){
				document.location = edit_form_.attr("data-list-url");
			}else{
				document.location = response_json_.redirect_url;	
			}
		});

	}, true);
}

function pb_templated_edit_remove(){
	$ = jQuery;
	var edit_form_ = $("#pb-templated-edit-edit-form");

	PB.confirm_q({
		title : _pb_templated_edit_label_var['remove-confirm-title'],
		content : _pb_templated_edit_label_var['remove-confirm-message'],
		button1 : _pb_templated_edit_label_var['remove']
	}, function(c_){
		if(!c_) return;
		_pb_templated_edit_remove();
	});
}

function _pb_templated_edit_remove(){
	$ = jQuery;
	var edit_form_ = $("#pb-templated-edit-edit-form");
	var menu_id_ = edit_form_.data('adminpage-menu-id');
	var target_data_ = pb_templated_edit_serialize_object();
	
	var label_success_title_ = _pb_templated_edit_label_var['remove-success-title'];
	var label_success_message_ = _pb_templated_edit_label_var['remove-success-message'];

	var label_error_title_ = _pb_templated_edit_label_var['remove-error-title'];
	var label_error_message_ = _pb_templated_edit_label_var['remove-error-message'];

	PB.post({
		action : "adminpage-templated-remove",
		menu_id : menu_id_,
		target_data : target_data_,
	}, function(result_, response_json_){
		if(!result_ || response_json_.success !== true){
			PB.alert_error({
				title : response_json_.error_title || label_error_title_,
				content : response_json_.error_message || label_error_message_,
			});
			return;
		}

		PB.alert_success({
			title : label_success_title_,
			content : label_success_message_,
		}, function(){
			document.location = response_json_.redirect_url;
		});

	}, true);	
}