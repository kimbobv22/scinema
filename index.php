<?php
get_header();
if(have_posts()) :
	while(have_posts()) :
		the_post();

		$content_template_ = apply_filters('pb_content_template', 'content');
		$use_full_path_ = apply_filters('pb_content_template_use_fullpath', false, $content_template_);

		if($use_full_path_){
			include_once $content_template_.".php";
		}else{
			get_template_part('content-templates/'.$content_template_);	
		}

		break;
		
	endwhile;

else :
	get_template_part('content-templates/content','noitem');
endif;

get_footer();

?>