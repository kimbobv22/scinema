<?php
/**
 * Template Name: 홈화면
 *
 * @author 폴앤브로
 */

get_header();
?>

<?php

while ( have_posts() ) :
	the_post();
	get_template_part('content-templates/content', 'home');
endwhile;

get_footer();
?>