<?php
/**
 * Template Name: 본사 오시는길
 *
 * @author 폴앤브로
 */

get_header();
?>

<?php

while ( have_posts() ) :
	the_post();
	get_template_part('content-templates/content', 'location');
endwhile;

get_footer();
?>