<?php
	global $pb_board;
	$board_id_ = $pb_board->board_id;
	$board_title_ = $pb_board->board_name;
?>
<div class="pb-board-list-frame">
	
	<div class="pb-board-top-label">
		<h3 class="board-title"><?=$board_title_?></h3>
		<div class="board-desc">
			<?php if(pb_is_admin()){ ?>
				<span class="text-danger">관리자모드</span>
			<?php } ?>
		</div>
	</div>
	<form id="pb-board-list-form" method="get" action="<?=get_permalink()?>">
		
		
		<?php
			$list_table_ = new PB_qna_table("pb-qna-table", "pb-board-table");
			echo $list_table_->html();
		?>
	</form>

</div>