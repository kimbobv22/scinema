<?php

class PB_qna_table extends PBListTable{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 15;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$sticky_posts_ = pb_board_post_list(array(
			'board_id' => $pb_board->board_id,
			'only_sticky' => true,
		));;

		$posts_ = pb_board_post_list(array(
			'board_id' => $pb_board->board_id,
			'keyword' => $keyword_,
			'limit' => array($offset_, $per_page_),
			'without_sticky' => true,
		));

		$posts_ = array_merge($sticky_posts_, $posts_);

		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_board_post_list(array(
				'board_id' => $pb_board->board_id,
				'keyword' => $keyword_,
				'just_count' => true,
				'without_sticky' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $posts_,

			'html_default_class' => 'table table-hover',
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function row_attributes($items_, $row_index_){
		if($items_->sticky === "Y"){
			return array('class' => 'sticky');			
		}

		return array();
	}

	function columns(){
		return array(
			"type" => "",
			"qna_type" => "문의분류",
			"post_title" => "문의사항",
			"view_count" => "조회",
			"wrt_name" => "작성자",
			"post_date" => "문의일자",
		);
	}

	function column_value($item_, $column_name_){

		global $pb_board;
		$row_index_ = $this->current_row();

		switch($column_name_){
			case "type" :

				if($item_->sticky === "Y"){
					return '<i class="icon fa fa-tag" aria-hidden="true"></i>';
				}

				return "";
			case "qna_type" :

				if($item_->sticky === "Y"){
					return "공지";
				}

				return $item_->custom_col_03;
			case "view_count" :
				ob_start();
?>

				<?=$item_->view_count?>

<?php
					
				return ob_get_clean();
			case "post_title" :
				ob_start();
?>

				<a href="<?=pb_board_post_view_url(get_the_ID(), $item_->post_id)?>">
					<span class="qna-type">
						<?php
							if($item_->sticky === "Y"){
								echo  "공지";
							}else{
								echo $item_->custom_col_03;
							}
						?>

					</span>
					<?=stripslashes($item_->title)?>
				</a>
			

			<?php if($item_->sticky === "N"){ 
				if($item_->comment_count > 0){ ?>
					<span class="badge answer">답변완료</span>
				<?php }else{ ?>
					<span class="badge no-answer">답변대기</span>
				<?php }
				}
			?>
	
	<div class="icon-wrap">
<?php

	if($item_->sticky === "Y"){ ?>
		<i class="icon fa fa-tag" aria-hidden="true"></i>
	<?php } ?> 

	</div>

	<?php
					
				return ob_get_clean();
			case "wrt_name" :
				ob_start();
?>

				<?=$item_->wrt_name?>

<?php
					
				return ob_get_clean();

			case "post_date" :
				ob_start();
?>
				
				
				<div class="date-item view-count"><i class="icon fa fa-eye" aria-hidden="true"></i> <?=number_format($item_->view_count)?></div>
				<div class="date-item wrt-date"><i class="icon fa fa-clock-o" aria-hidden="true"></i> <?=$item_->wrt_date_ymdhi?></div>
				<div class="date-item wrt-name"><i class="icon fa fa-user" aria-hidden="true"></i> <?=$item_->wrt_name?></div>

<?php
					
				return ob_get_clean();

			default : 
				return '';
			break;
		}
	}

	function norowdata(){
		return "검색된 문의사항이 없습니다.";	
	}
	
}


	add_filter('pb-listtable-before-pagenav-pb-qna-table', function(){
		global $pb_board;
		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
	?>

	<div class="search-frame">
		<div class="row">
			<div class="col-xs-3 col-sm-8">
				<?php if(pb_board_can_write($pb_board->board_id)){ ?>
					
					<?php if(pb_is_admin()){ ?>
						<a href="<?=pb_board_post_write_url(get_the_ID())?>" class="btn btn-default btn-sm">공지등록</a>
					<?php }else{ ?>
						<a href="<?=pb_board_post_write_url(get_the_ID())?>" class="btn btn-default btn-sm">문의하기</a>
					<?php } ?>

				<?php } ?>
			</div>
			<div class="col-xs-9 col-sm-4">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">검색</button>
					</span>
				</div>
			</div>
		</div>
	</div>

	<?php
});
	
add_action('pb_board_library', function(){ //global
	wp_enqueue_style("pb-board-qna-common", (pb_library_url() . 'css/boards/qna.css'));
	wp_enqueue_script("pb-board-qna-common", (pb_library_url() . 'js/boards/qna/common.js'), array("pb-all-main"));
});

add_action('pb_board_library_list', function(){
	wp_enqueue_script("pb-board-qna-list", (pb_library_url() . 'js/boards/qna/list.js'), array('pb-board-qna-common'));
});

add_action('pb_board_library_view', function(){
	wp_enqueue_script("pb-board-qna-view", (pb_library_url() . 'js/boards/default/view.js'), array('pb-board-qna-common'));
});

add_action('pb_board_library_write', function(){
	wp_enqueue_script("pb-board-default-write", (pb_library_url() . 'js/boards/default/write.js'), array('pb-board-qna-common'));
	wp_enqueue_script("pb-board-qna-write", (pb_library_url() . 'js/boards/qna/write.js'), array('pb-board-default-write'));
});

add_action('pb_board_library_update', function(){
	do_action('pb_board_library_write');
});
add_action('pb_board_library_update', function(){
	do_action('pb_board_library_write');
});

add_action('pb_board_library_password', function(){
	wp_enqueue_script("pb-board-qna-password", (pb_library_url() . 'js/boards/default/password.js'), array('pb-board-qna-common'));
});

add_action('pb_board_library_password_for_update', function(){
	wp_enqueue_script("pb-board-qna-password-for-update", (pb_library_url() . 'js/boards/default/update-password.js'), array('pb-board-qna-common'));
});

?>