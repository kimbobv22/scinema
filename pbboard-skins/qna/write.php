<?php

	global $pb_board, $pb_board_post, $pb_board_qna_list_url, $pb_board_qna_default_type;

	if(!strlen($pb_board_qna_list_url)){
		$pb_board_qna_list_url = get_permalink(get_the_ID());
	}

	$board_title_ = $pb_board->board_name;
	$attachment_count_ = $pb_board->attachment_limit;

	if(empty($pb_board_post)){
		$pb_board_post = (object)array(
			'post_id' => null,
			'add_new' => true,
			'title' => null, 
			'content' => null,
			'sticky' => 'N',
			'secret' => 'N',
			'attachment_files' => null,
			'feature_image_url' => null,
			'wrt_name' => null,
			'custom_col_01' => null,
			'custom_col_02' => null,
			'custom_col_03' => isset($_GET['qna_type']) ? $_GET['qna_type'] : null,
		);

		if(!strlen($pb_board_post->custom_col_03)){
			$pb_board_post->custom_col_03 = $pb_board_qna_default_type;
		}

		if(!strlen($pb_board_post->custom_col_03)){
			$pb_board_post->custom_col_03 = get_post_meta(get_the_ID(), "pb_board_qna_default_type", true);
		}
	}

	$has_feature_image_ = strlen($pb_board_post->feature_image_url);

	$attachment_files_ = unserialize($pb_board_post->attachment_files);
	if(empty($attachment_files_)){
		$attachment_files_ = array();
	}

	$add_new_ = (isset($pb_board_post->add_new) && $pb_board_post->add_new);

	if($add_new_){
		$ajax_url_ = 'pb-board-post-add-new';
	}else{
		$ajax_url_ = 'pb-board-post-update';
	}

	$t_qna_ctg_ = explode(PHP_EOL, $pb_board->custom_col_03);
	$qna_ctg_ = array();
	foreach($t_qna_ctg_ as $ctg_){
		if(!strlen($ctg_)) continue;
		$ctg_ = trim($ctg_);
		$qna_ctg_[$ctg_] = $ctg_;
	}


?>
<script type="text/javascript">

<?php if(pb_is_admin()){ ?>
var PBBOARD_WRITE_VAR = {
<?php if($add_new_){ ?>
	write_confirm_popup_title : "작성확인",
	write_confirm_popup_content : "공지를 등록합니다. 계속하시겠습니까?",
	write_confirm_popup_button1 : "등록하기",
	
	write_error_popup_title : "에러발생",
	write_error_popup_content : "등록 중 에러가 발생했습니다.",
<?php }else{ ?>
	write_confirm_popup_title : "작성확인",
	write_confirm_popup_content : "작성하신 내용으로 수정합니다. 계속하시겠습니까?",
	write_confirm_popup_button1 : "수정하기",
	
	write_error_popup_title : "에러발생",
	write_error_popup_content : "수정 중 에러가 발생했습니다.",
<?php } ?>
}
<?php }else{ ?>
var PBBOARD_WRITE_VAR = {
<?php if($add_new_){ ?>
	write_confirm_popup_title : "작성확인",
	write_confirm_popup_content : "문의사항을 등록합니다. 계속하시겠습니까?",
	write_confirm_popup_button1 : "등록하기",
	
	write_error_popup_title : "에러발생",
	write_error_popup_content : "등록 중 에러가 발생했습니다.",
<?php }else{ ?>
	write_confirm_popup_title : "작성확인",
	write_confirm_popup_content : "작성하신 내용으로 수정합니다. 계속하시겠습니까?",
	write_confirm_popup_button1 : "수정하기",
	
	write_error_popup_title : "에러발생",
	write_error_popup_content : "수정 중 에러가 발생했습니다.",
<?php } ?>
}
<?php } ?>	

</script>

<div class="pb-board-write-frame">
	
	<div class="pb-board-top-label">
		<h3 class="board-title"><?=$board_title_?></h3>
		<a href="<?=$pb_board_qna_list_url?>" class="list-btn btn btn-default btn-xs">목록으로</a>
	</div>

	<div class="board-content-wrap pb-board-write"><form class="pb-board-write-form" method="post" id="pb-board-write-form" data-board-page-url="<?=$pb_board_qna_list_url?>" data-attachment-limit="<?=$attachment_count_?>" data-ajax-action="<?=$ajax_url_?>">

		<input type="hidden" name="board_id" value="<?=$pb_board->board_id?>">
		<input type="hidden" name="post_id" value="<?=$pb_board_post->post_id?>">
		
		<div class="pb-board-header">	
			<div>
				<div class="form-group">

				<?php if(count($qna_ctg_) > 0 && !pb_is_admin()){ ?>
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<select class="form-control" name="custom_col_03" required data-error="문의분류를 선택하세요" >
								<option value="">-문의분류-</option>
								<?php foreach($qna_ctg_ as $ctg_){ ?>
									<option value="<?=$ctg_?>" <?=selected($ctg_, $pb_board_post->custom_col_03)?> ><?=$ctg_?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-9">
							<div class="form-margin visible-xs"></div>
							<input type="text" name="title" class="form-control" placeholder="제목을 입력하세요" required data-error="제목을 입력하세요" value="<?=stripcslashes($pb_board_post->title)?>">
						</div>
					</div>
				<?php }else{ ?>
						<input type="hidden" name="custom_col_03" value="일반문의">
						<input type="text" name="title" class="form-control" placeholder="제목을 입력하세요" required data-error="제목을 입력하세요" value="<?=stripcslashes($pb_board_post->title)?>">
				<?php } ?>
					<div class="help-block with-errors"></div>
				</div>
			</div>

		</div>

		<div class="pb-board-body">
			
			<a href="#" data-toggle="modal" data-target="#pb-board-write-imageupload-popup" class="image-insert-btn btn btn-sm btn-default btn-fileupload "><i class=" label-icon glyphicon glyphicon-plus"></i>이미지삽입</a>

			<div class="board-content-editor-wrap"><div class="form-group">
			<?php wp_editor(stripcslashes($pb_board_post->content),"content", array(
				'media_buttons' => false,
				'quicktags' => false,
				'wpautop' => false,
			)) ?>
			<div class="help-block with-errors"></div>
			</div></div>
		</div>

<?php if($attachment_count_ > 0){ ?>
		<div class="pb-board-files">
			
			<h4>첨부파일등록 <small class="help-block-inline">(최대 <?=number_format($attachment_count_)?>개)</small></h4>
			<ul class="files-list" id="pb-board-write-upload-file-list">

<?php foreach($attachment_files_ as $attachment_file_){ ?>
		<li class='file-item'>
			<input type='hidden' value='<?=$attachment_file_['url']?>' name='attachment[]' data-name='<?=$attachment_file_['name']?>' data-new="N">
			<i class="icon fa fa-file-o" aria-hidden="true"></i>
			<span><?=$attachment_file_['name']?></span>
			<a href="#" data-file-item-delete class="delete-btn"><i class="fa fa-times" aria-hidden="true"></i></a>
		</li>
<?php } ?>
			</ul>

			<a href="#" data-toggle="modal" data-target="#pb-board-write-fileupload-popup" class="file-insert-btn btn btn-sm btn-default btn-fileupload"><i class="label-icon glyphicon glyphicon-plus"></i>첨부파일추가</a>

		</div>
<?php } ?>

<?php if(!is_user_logged_in()){ ?>
		<div class="pb-board-author">
			
			<h4>작성자정보</h4>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-board-write-form-custom-col-01">이메일</label>
						<input type="text" name="custom_col_01"  class="form-control input-sm" id="pb-board-write-form-custom-col-01" placeholder="이메일 입력" required data-error="이메일을 입력하세요" <?=$add_new_ ? "" : "readonly"?> value="<?=$pb_board_post->custom_col_01?>" >
						<small class="help-block with-errors"></small>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-board-write-form-custom-col-02">연락처</label>
						<input type="text" name="custom_col_02" class="form-control input-sm" id="pb-board-write-form-custom-col-02" placeholder="연락처 입력" required data-error="연락처를 입력하세요" <?=$add_new_ ? "" : "readonly"?> value="<?=$pb_board_post->custom_col_02?>" >
						<small class="help-block with-errors"></small>
					</div>
				</div>
			</div>
			<div class="row">
				
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-board-write-form-user-name">작성자명</label>
						<input type="text" name="wrt_name" class="form-control input-sm" id="pb-board-write-form-user-name" placeholder="작성자명 입력" required data-error="작성자명을 입력하세요" <?=$add_new_ ? "" : "readonly"?> value="<?=$pb_board_post->wrt_name?>" >
						<small class="help-block with-errors"></small>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-board-write-form-pass">
							<?=$add_new_ ? "암호" : "새로운 암호"?><small class="help-block-inline">* 수정을 위해 필요합니다.</small>
						</label>
						<input type="password" name="wrt_pass" class="form-control form-control-sm input-sm" id="pb-board-write-form-pass" placeholder="암호 입력" required data-error="암호를 입력하세요" >
						<input type="hidden" name="secret_pass">
						<small class="help-block with-errors"></small>
					</div>
				</div>
			</div>


		</div>
<?php } ?>


		<div class="pb-board-footer">
			<div class="row">
				<div class="col-xs-8">
					<div class="form-group">	
				<?php if(pb_is_admin()){ ?>
					

					<input type="hidden" name="sticky" value="Y">
				
				<?php }else{ ?>
					
					<input type="hidden" name="sticky" value="N">

				<?php if($pb_board->use_secret === "Y" && $pb_board->use_only_secret === "N"){ ?>
			
					<label class="checkbox-inline secret-checkbox">
						<input type="checkbox" name="secret" value="Y" id="pb-board-write-form-secret-checkbox" <?=checked($pb_board_post->secret, "Y")?> > 비밀글
					</label>
					
				<?php }

				} ?>
				
					</div>
				</div>
				
				<div class="col-xs-4 text-right">
				<?php if($add_new_){ ?>
					<button type="submit" class="btn btn-primary btn-md">등록하기</button>
				<?php }else{ ?>
					<a href="<?=pb_board_post_view_url(get_the_ID())?>" class="btn btn-default btn-md">취소</a>
					<button type="submit" class="btn btn-primary btn-md">수정하기</button>
				<?php } ?>
				</div>
			</div>
		</div>
	</form></div>

	
	
</div>


<div class="pb-imageupload-dropzone-modal modal" id="pb-board-write-imageupload-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<input id="pb-board-write-upload-image" type="file" name="files[]" multiple accept="image/*">
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>

<div class="pb-fileupload-dropzone-modal modal" id="pb-board-write-fileupload-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<input id="pb-board-write-upload-file" type="file" name="files[]" accept="image/*,.zip,.pdf,.hwp,.xls,.xlsx,.doc,.docx,*.ppt,*.pptx" multiple maxlength="<?=$attachment_count_?>">
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>