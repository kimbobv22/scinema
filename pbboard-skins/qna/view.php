<?php

	global $pb_board, $pb_board_post;
	$board_title_ = $pb_board->board_name;

	$attachment_files_ = unserialize($pb_board_post->attachment_files);
	if(gettype($attachment_files_) !== "array"){
		$attachment_files_ = array();
	}
?>
<script type="text/javascript">
var PBBOARD_LIST_URL = "<?=get_permalink(get_the_ID())?>";
</script>
<div class="pb-board-view-frame">
	
	<div class="pb-board-top-label">
		<h3 class="board-title"><?=$board_title_?></h3>
		<a href="<?=get_permalink(get_the_ID())?>" class="list-btn btn btn-default btn-xs">목록으로</a>
	</div>

	<div class="board-content-wrap pb-board-view">
		<div class="pb-board-header">
			<h3 class="title">
				<?=stripslashes($pb_board_post->title)?>
				<small class="qna-type">
					<?=$pb_board_post->custom_col_03?>
				</small>		
			</h3>
			<div class="subinfo">
				<div class="item">
					<i class="icon fa fa-user" aria-hidden="true"></i> <?=strlen($pb_board_post->wrt_name) ? $pb_board_post->wrt_name : $pb_board_post->user_display_name?>
				</div>
				<div class="item">
					<i class="icon fa fa-clock-o" aria-hidden="true"></i><?=$pb_board_post->wrt_date_ymdhi?>
				</div>
				<div class="item">
					<i class="icon fa fa-eye" aria-hidden="true"></i><?=number_format($pb_board_post->view_count)?>
				</div>
				<div class="item">
					<?php if($pb_board_post->secret === "Y"){ ?>
						<i class="icon fa fa-lock" aria-hidden="true"></i>
					<?php } ?>
				</div>
			</div>
		</div>
		

		<div class="pb-board-body">
			<?=stripslashes($pb_board_post->content)?>
		</div>
		<?php if(pb_is_admin() && $pb_board_post->wrt_id != get_current_user_id()){ ?>
		<div class="pb-board-author-info">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">작성자정보</h3></div>
				<div class="panel-body">
					<address>
  <strong><?=$pb_board_post->wrt_name?></strong><br>
		  <?=$pb_board_post->custom_col_01?><br>
		  <?=$pb_board_post->custom_col_02?>
</address>	
				</div>
			</div>			
		</div>
		<?php } ?>
		
		<?php if(count($attachment_files_) > 0){ ?>
		<div class="pb-board-footer">
			<ul class="attachment-files">
			<?php foreach($attachment_files_ as $attachment_file_){ ?>
				<li><a href="<?=$attachment_file_['url']?>" target="_blank"><i class="fa fa-file-o" aria-hidden="true"></i> <?=$attachment_file_['name']?></i></a></li>
			<?php } ?>
			</ul>
		</div>
		<?php } ?>
		<div class="board-content-bottom-label text-right">
			<a href="javascript:pb_pbboard_view_remove(<?=$pb_board_post->post_id?>)" class="btn btn-sm btn-default">삭제</a>
			<a href="<?=pb_board_post_update_url(get_the_ID(), $pb_board_post->post_id)?>" class="btn btn-sm btn-default">수정</a>
		</div>

	</div>

	
<?php 
	
	$comments_ = pb_board_comment_list(array(
		"post_id" => $pb_board_post->post_id,
		'orderby' => "ORDER BY wrt_date asc"
	));

?>

<div class="pb-board-comment-list <?=count($comments_) > 0 ? "" : "no-comment"?>" id="pb-board-comment-list">


<?php
	foreach($comments_ as $comment_){
		pb_board_comment_draw_item($comment_);
	}
?>

</div>

<?php if(pb_board_can_comment($pb_board->board_id)){ ?>
	<div class="pb-board-comment-write"><form class="pb-board-comment-write-form" id="pb-board-comment-write-form">
		<input type="hidden" name="post_id" value="<?=$pb_board_post->post_id?>">
			
		<div class="comment-form-wrap">
			<div class="input-wrap">

			<h3>답변달기</h3>

<?php if(!is_user_logged_in()){ ?>
				<div class="author-form-wrap">

					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label for="pb-board-comment-write-form-user-name">작성자명</label>
								<input type="text" name="wrt_name" class="form-control input-sm" id="pb-board-comment-write-form-user-name" placeholder="작성자명 입력" required data-error="작성자명을 입력하세요">
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label for="pb-board-comment-write-form-pass">암호</label>
								<input type="password" name="wrt_pass" class="form-control form-control-sm input-sm" id="pb-board-comment-write-form-pass" placeholder="암호 입력" required data-error="암호를 입력하세요">
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>

 <?php } ?>
				<div class="form-group">
					<label for="pb-board-comment-write-form-comment">답변</label>
					<textarea name="content" class="form-control comment-input input-sm" required data-error="답변을 입력하여 주세요." placeholder="댓글을 입력하세요!" id="pb-board-comment-write-form-comment"></textarea>
					<div class="help-block with-errors"></div>
				</div>

				<div class="button-wrap text-right">
					<button type="submit" class="btn btn-default submit-btn btn-sm">답변달기</button>	
				</div>
			</div>
		</div>
			
		
	</form></div>
	
<?php } ?>
</div>