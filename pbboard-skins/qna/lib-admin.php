<?php

add_action('pb_board_admin_fields_qna', function($board_post_){
?>

<tr>
	<th><label>알림받을 이메일</label></th>
	<td>
		<input type="text" name="custom_col_01" class="large-text" placeholder="이메일 입력" value="<?=$board_post_->custom_col_01?>">
		<p class="help-text">*비워두면 알림을 받지 않습니다.</p>
	</td>
</tr>

<tr>
	<th><label>이메일로 답변회신</label></th>
	<td>
		<p><label><input type="radio" name="custom_col_02" value="Y" <?=checked($board_post_->custom_col_02, "Y")?> > 예</label></p>
		<p><label><input type="radio" name="custom_col_02" value="N" <?=checked($board_post_->custom_col_02, "N")?> > 아니요</label></p>
		<p class="help-text">*답변회신 체크 시, 문의자 이메일로 답변을 회신합니다.</p>
	</td>
</tr>
<tr>
	<th><label>문의구분</label></th>
	<td>
		<textarea name="custom_col_03" placeholder="줄바꿈으로 구분"><?=$board_post_->custom_col_03?></textarea>
		<p class="help-text">
			*줄바꿈으로 구분
		</p>
	</td>
</tr>

<?php
});

add_action('pb_board_admin_page_metabox_fields_qna', function($post_, $board_data_){

	$t_qna_ctg_ = explode(PHP_EOL, $board_data_->custom_col_03);
	$qna_ctg_ = array();
	foreach($t_qna_ctg_ as $ctg_){
		if(!strlen($ctg_)) continue;
		$ctg_ = trim($ctg_);
		$qna_ctg_[$ctg_] = $ctg_;
	}

	if(isset($post_)){
		$qna_default_type_ = get_post_meta($post_->ID, "pb_board_qna_default_type", true);	
	}else{ //new
		$qna_default_type_ = null;
	}
	

?>

<div>
	<label><code>문의구분 기본값</code></label>
	<p>
		<select class="larget-text" name="pb_board_qna_default_type">
			<option value="">-문의구분선택-</option>
		<?php foreach($qna_ctg_ as $qna_){ ?>
		
			<option value="<?=$qna_?>" <?=selected($qna_default_type_, $qna_)?> ><?=$qna_?></option>
		<?php }?>

		</select>
	</p>
</div>

<?php
},10,2);


add_action('edit_post', function($post_id_, $post_){
	if($post_->post_type !== "page") return;
	if(!isset($_POST['pb_board_qna_default_type'])){
		return;
	}

	$default_value_ = isset($_POST['pb_board_qna_default_type']) ? $_POST['pb_board_qna_default_type'] : null;
	update_post_meta($post_id_, 'pb_board_qna_default_type', $default_value_);	
},10,2);

?>