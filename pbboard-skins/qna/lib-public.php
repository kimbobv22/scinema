<?php

add_action('pb_board_post_wrote', function($post_id_){
	$post_data_ = pb_board_post($post_id_);
	$board_data_ = pb_board($post_data_->board_id);

	ob_start();


	if(!strlen($board_data_->custom_col_01)) return;
?>

<h2><?=$post_data_->title?></h2>
<small><?=$post_data_->wrt_name?> | <?=$post_data_->custom_col_01?> | <?=$post_data_->custom_col_02?></small>

<h3>문의내용</h3>
<p><?=stripcslashes($post_data_->content)?></p>

<?php

	$email_html_ = ob_get_clean();

	wp_mail($board_data_->custom_col_01, "[".$post_data_->custom_col_03."] ".$post_data_->title, $email_html_, array('Content-Type: text/html; charset=UTF-8'));
});


function _pb_board_qna_write_form( $atts ) {
	$atts = shortcode_atts( array(
		'board_id' => '',
		'qna_type' => '',
		'board_url' => '',
	), $atts, 'pb_board_qna_write_form' );


	wp_enqueue_style("pb-board-qna-common", (pb_library_url() . 'css/boards/qna.css'));
	wp_enqueue_script("pb-board-qna-common", (pb_library_url() . 'js/boards/qna/common.js'), array("pb-all-main"));
	wp_enqueue_script("pb-board-default-write", (pb_library_url() . 'js/boards/default/write.js'), array('pb-board-qna-common'));
	wp_enqueue_script("pb-board-qna-write", (pb_library_url() . 'js/boards/qna/write.js'), array('pb-board-default-write'));

	global $pb_board, $pb_board_post;

	$pb_board = pb_board($atts['board_id']);

	global $pb_board_qna_list_url, $pb_board_qna_default_type;
	$pb_board_qna_default_type = $atts['qna_type'];
	$pb_board_qna_list_url = $atts['board_url'];

	ob_start();
	get_template_part('pbboard-skins/'.$pb_board->type.'/'.PBBOARD_ACTION_TYPE_WRITE);

	$pb_board = null;
	$pb_board_post = null;
	$pb_board_qna_default_type = null;
	$pb_board_qna_list_url = null;

	return ob_get_clean();
}
add_shortcode('pb_board_qna_write_form', '_pb_board_qna_write_form');


add_action('init', function(){
	if(!defined('WPB_VC_VERSION')){
		return;
	}

	$board_list_ = pb_board_list();
	$board_options_ = array();

	foreach($board_list_ as $board_){
		if($board_->type !== "qna") continue;
		$board_options_[$board_->board_name] = $board_->board_id;
	}

	vc_map(array(
		"name" => '문의하기폼(게시판)',
		"description" => '문의하기폼(게시판)',
		"base" => "pb_board_qna_write_form",
		"icon" => PB_THEME_DIR_URL.'img/vc-symbol.png',
		"content_element" => true,
		'params' => array(
			array(
				'type' => 'dropdown',
				'heading' => '게시판',
				'param_name' => 'board_id',
				"holder" => "div",
				"value" => $board_options_,
			),
			array(
				'type' => 'textfield',
				'heading' => '문의분류',
				'param_name' => 'qna_type',
				"holder" => "div",
			),
			array(
				'type' => 'textfield',
				'heading' => '게시판URL',
				'param_name' => 'board_url',
				"holder" => "div",
			)
		)
	));

});

?>