<?php
	global $pb_board_comment;

	$profile_image_ = null;
	if(isset($pb_board_comment->wrt_id) && $pb_board_comment->wrt_id > 0){
		$profile_image_ = get_avatar_url($pb_board_comment->wrt_id);
	}

	$current_user_id_ = get_current_user_id();
	$is_mine_ = (is_user_logged_in() && $current_user_id_ == $pb_board_comment->wrt_id);

?>
<div class="pb-board-comment-item <?=$is_mine_ ? "mycomment" : ""?>" data-comment-id="<?=$pb_board_comment->comment_id?>" data-post-id="<?=$pb_board_comment->post_id?>">
	
	<div class="comment-media">
		<div class="profile-image">
			<?php if($profile_image_){ ?>
				<img src="<?=$profile_image_?>">
			<?php } ?>
		</div>
	</div>
	<div class="comment-body">
		<div class="comment-info">
			<div class="username"><?=$pb_board_comment->wrt_name?></div>
			<div class="comment-date"><?=$pb_board_comment->wrt_date?></div>
		</div>
		<div class="comment" data-comment-content><?=nl2br($pb_board_comment->content)?></div>
		<?php 

			if(($pb_board_comment->wrt_id > 0 && $current_user_id_ == $pb_board_comment->wrt_id )
				|| ($pb_board_comment->wrt_id == 0)){ ?>

				<div class="button-row">
					<a href="" data-comment-update-btn="<?=$pb_board_comment->comment_id?>" class="text-primary update-link">
						<i class="fa fa-pencil" aria-hidden="true"></i>
					</a>
					<a href="" data-comment-delete-btn="<?=$pb_board_comment->comment_id?>" class="text-primary delete-link">
						<i class="fa fa-trash-o" aria-hidden="true"></i>
					</a>
				</div>
			<?php }else if(pb_is_admin()){ ?>
				<div class="button-row">
					<a href="" data-comment-delete-btn="<?=$pb_board_comment->comment_id?>" class="text-primary delete-link">
						<i class="fa fa-trash-o" aria-hidden="true"></i>
					</a>
				</div>
			<?php } ?>
	</div>
</div>