<?php

	global $pb_board, $pb_board_post;

	$board_title_ = $pb_board->board_name;
	$attachment_count_ = $pb_board->attachment_limit;


	if(empty($pb_board_post)){
		$pb_board_post = (object)array(
			'add_new' => true,
			'title' => null, 
			'content' => null,
			'sticky' => 'N',
			'secret' => 'N',
			'attachment_files' => null,
			'wrt_name' => null,
		);
	}

	$attachment_files_ = unserialize($pb_board_post->attachment_files);
	if(empty($attachment_files_)){
		$attachment_files_ = array();
	}

	$add_new_ = (isset($pb_board_post->add_new) && $pb_board_post->add_new);

	if($add_new_){
		$ajax_url_ = 'pb-board-post-add-new';
	}else{
		$ajax_url_ = 'pb-board-post-update';
	}
?>
<script type="text/javascript">
var PBBOARD_WRITE_VAR = {
<?php if($add_new_){ ?>
	write_confirm_popup_title : "작성확인",
	write_confirm_popup_content : "작성하신 글을 등록합니다. 계속하시겠습니까?",
	write_confirm_popup_button1 : "등록하기",
	
	write_error_popup_title : "에러발생",
	write_error_popup_content : "글 등록 중 에러가 발생했습니다.",
<?php }else{ ?>
	write_confirm_popup_title : "작성확인",
	write_confirm_popup_content : "작성하신 글로 수정합니다. 계속하시겠습니까?",
	write_confirm_popup_button1 : "수정하기",
	
	write_error_popup_title : "에러발생",
	write_error_popup_content : "글 수정 중 에러가 발생했습니다.",
<?php } ?>
}	

</script>

<div class="pb-board-write-frame">
	
	<div class="pb-board-top-label">
		<h3 class="board-title"><?=$board_title_?></h3>
		<a href="<?=get_permalink(get_the_ID())?>" class="list-btn btn btn-default btn-xs">목록으로</a>
	</div>

	<div class="board-content-wrap pb-board-write"><form class="pb-board-write-form" method="post" id="pb-board-write-form" data-board-page-url="<?=get_permalink()?>" data-attachment-limit="<?=$attachment_count_?>" data-ajax-action="<?=$ajax_url_?>">

		<input type="hidden" name="board_id" value="<?=$pb_board->board_id?>">
		<input type="hidden" name="post_id" value="<?=$pb_board_post->post_id?>">
		
		<div class="pb-board-header">	
			<div>
				<div class="form-group">
					<input type="text" name="title" class="form-control" placeholder="제목을 입력하세요" required data-error="제목을 입력하세요" value="<?=stripslashes($pb_board_post->title)?>">
					<div class="help-block with-errors"></div>
				</div>
			</div>

		</div>

		<div class="pb-board-body">
			
			<a href="#" data-toggle="modal" data-target="#pb-board-write-imageupload-popup" class="image-insert-btn btn btn-sm btn-default btn-fileupload "><i class=" label-icon glyphicon glyphicon-plus"></i>이미지삽입</a>

			<div class="board-content-editor-wrap"><div class="form-group">
			<?php wp_editor(stripslashes($pb_board_post->content),"content", array(
				'media_buttons' => false,
				'quicktags' => false,
				'wpautop' => false,
			)) ?>
			<div class="help-block with-errors"></div>
			</div></div>
		</div>

<?php if($attachment_count_ > 0){ ?>
		<div class="pb-board-files">
			
			<h4>첨부파일등록 <small class="help-block-inline">(최대 <?=number_format($attachment_count_)?>개)</small></h4>
			<ul class="files-list" id="pb-board-write-upload-file-list">

<?php foreach($attachment_files_ as $attachment_file_){ ?>
		<li class='file-item'>
			<input type='hidden' value='<?=$attachment_file_['url']?>' name='attachment[]' data-name='<?=$attachment_file_['name']?>'>
			<i class="icon fa fa-file-o" aria-hidden="true"></i>
			<span><?=$attachment_file_['name']?></span>
			<a href="#" data-file-item-delete class="delete-btn"><i class="fa fa-times" aria-hidden="true"></i></a>
		</li>
<?php } ?>
			</ul>

			<a href="#" data-toggle="modal" data-target="#pb-board-write-fileupload-popup" class="file-insert-btn btn btn-sm btn-default btn-fileupload"><i class="label-icon glyphicon glyphicon-plus"></i>첨부파일추가</a>

		</div>
<?php } ?>

<?php if(!is_user_logged_in()){ ?>
		<div class="pb-board-author">
			
			<h4>작성자정보</h4>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-board-write-form-user-name">작성자명</label>
						<input type="text" name="wrt_name" class="form-control input-sm" id="pb-board-write-form-user-name" placeholder="작성자명 입력" required data-error="작성자명을 입력하세요" <?=$add_new_ ? "" : "readonly"?> value="<?=$pb_board_post->wrt_name?>" >
						<small class="help-block with-errors"></small>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="pb-board-write-form-pass">
							<?=$add_new_ ? "암호" : "새로운 암호"?><small class="help-block-inline">* 수정을 위해 필요합니다.</small>
						</label>
						<input type="password" name="wrt_pass" class="form-control form-control-sm input-sm" id="pb-board-write-form-pass" placeholder="암호 입력" required data-error="암호를 입력하세요" >
						<small class="help-block with-errors"></small>
					</div>
				</div>
			</div>


		</div>
<?php } ?>


		<div class="pb-board-footer">
			<div class="row">
				<div class="col-xs-8">
					<div class="form-group">	
				<?php if(pb_is_admin()){ ?>
					

					<label class="checkbox-inline sticky-checkbox">
						<input type="checkbox" name="sticky" value="Y"> 공지
					</label>
				
				<?php } ?>

				<?php if($pb_board->use_secret === "Y" && $pb_board->use_only_secret === "N"){ ?>

				
					<label class="checkbox-inline secret-checkbox">
						<input type="checkbox" name="secret" value="Y" id="pb-board-write-form-secret-checkbox" <?=checked($pb_board_post->secret, "Y")?> > 비밀글
					</label>
					<?php if(!is_user_logged_in()){ ?>
					<input type="password" name="secret_pass" placeholder="암호 입력" class="form-control input-sm wrt-pass-input" data-secret-toggle required data-error="암호를 입력하세요">
					<?php } ?>
				
				<?php } ?>

				<?php if($pb_board->use_only_secret === "Y"){ ?>
					<input type="password" name="secret_pass" placeholder="암호 입력" class="form-control input-sm wrt-pass-input" required data-error="암호를 입력하세요">
				<?php } ?>


					</div>
				</div>
				
				<div class="col-xs-4 text-right">
				<?php if($add_new_){ ?>
					<button type="submit" class="btn btn-primary btn-md">등록하기</button>
				<?php }else{ ?>
					<a href="<?=pb_board_post_view_url(get_the_ID())?>" class="btn btn-default btn-md">취소</a>
					<button type="submit" class="btn btn-primary btn-md">수정하기</button>
				<?php } ?>
				</div>
			</div>
		</div>
	</form></div>

	
	
</div>
<div class="pb-imageupload-dropzone-modal modal" id="pb-board-write-imageupload-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<input id="pb-board-write-upload-image" type="file" name="files[]" multiple accept="image/*">
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>

<div class="pb-fileupload-dropzone-modal modal" id="pb-board-write-fileupload-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<input id="pb-board-write-upload-file" type="file" name="files[]" accept="image/*,.zip,.pdf,.hwp,.xls,.xlsx,.doc,.docx,*.ppt,*.pptx" multiple maxlength="<?=$attachment_count_?>">
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>