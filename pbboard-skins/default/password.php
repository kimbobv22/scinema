<?php
	global $pb_board, $pb_board_post, $pb_board_post_redirect_url;
?>
<div class="pb-board-post-password-frame">

	<div class="pb-board-top-label">
		<h3 class="board-title"><?=$pb_board->board_name?></h3>
		<a href="<?=get_permalink(get_the_ID())?>" class="list-btn btn btn-default btn-xs">목록으로</a>
	</div>

	<div class="board-content-wrap pb-board-view"><form class="pb-board-post-password-form" method="post" id="pb-board-post-password-form">
	
		<input type="hidden" name="post_id" value="<?=$pb_board_post->post_id?>">

		<div class="pb-board-header">
			<h3 class="title"><?=$pb_board_post->title?></h3>
			<div class="subinfo">
				<div class="item">
					<i class="icon fa fa-user" aria-hidden="true"></i> <?=strlen($pb_board_post->wrt_name) ? $pb_board_post->wrt_name : $pb_board_post->user_display_name?>
				</div>
				<div class="item">
					<i class="icon fa fa-clock-o" aria-hidden="true"></i><?=$pb_board_post->wrt_date_ymdhi?>
				</div>
				<div class="item">
					<?php if($pb_board_post->secret === "Y"){ ?>
						<i class="icon fa fa-lock" aria-hidden="true"></i>
					<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="password-input-wrap">
			<i class="icon fa fa-lock"></i>
			<p class="help-block">게시물 접근을 위해 암호를 입력해주세요</p>

			<div class="form-group">
				<div class="input-group input-group-sm">
					<input type="password" class="form-control" placeholder="암호 입력" required data-error="암호를 입력하세요" name="secret_pass">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-default" type="button">인증하기</button>
					</span>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>

	</form></div>
</div>