<?php


class PB_board_gallery_table extends PBListGrid{

	function prepare(){
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 6;
		$offset_ = $this->offset($page_index_, $per_page_);

		global $pb_board;

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";
		
		$sticky_posts_ = pb_board_post_list(array(
			'board_id' => $pb_board->board_id,
			'only_sticky' => true,
		));;

		$posts_ = pb_board_post_list(array(
			'board_id' => $pb_board->board_id,
			'keyword' => $keyword_,
			'limit' => array($offset_, $per_page_),
			'without_sticky' => true,
		));

		$posts_ = array_merge($sticky_posts_, $posts_);

		return array(
			"page_index" => $page_index_,
			"per_page" => $per_page_,
			
			'total_count' => pb_board_post_list(array(
				'board_id' => $pb_board->board_id,
				'keyword' => $keyword_,
				'just_count' => true,
				'without_sticky' => true,
			)),
			
			"hide_pagenav" => false,
			'pagenav_number' => true,
			"pagenav_count" => 5,
			// 'hide_header' => true,

			'items' => $posts_,
		);
	}

	function items($args_){
		return $args_['items'];
	}

	function render_item($item_){

		?>


			<div class="pb-gallery-item">
				
				<div class="feature-image-wrap">
					<img src="<?=$item_->feature_image_url?>">
				</div>
				<div class="title-wrap">
					<H4 class="title"><a href="<?=pb_board_post_view_url(get_the_ID(), $item_->post_id)?>"><?=stripslashes($item_->title)?></a></H4>
					<div class="author-info">
						<div class="author-item wrt-name"><i class="icon fa fa-user" aria-hidden="true"></i> <?=$item_->wrt_name?></div>
						<div class="author-item wrt-date"><i class="icon fa fa-clock-o" aria-hidden="true"></i> <?=$item_->wrt_date_ymdhi?></div>
					</div>
				</div>

			</div>
			
		
		<?php
		
	}

	function noitemdata(){
		return "검색된 글이 없습니다.";	
	}
	
}


	add_filter('pb-listgrid-before-pagenav-pb-board-table', function(){
		global $pb_board;
		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : null;
	?>

	<div class="search-frame">
		<div class="row">
			<div class="col-xs-3 col-sm-8">
				<?php if(pb_board_can_write($pb_board->board_id)){ ?>
					<a href="<?=pb_board_post_write_url(get_the_ID())?>" class="btn btn-default btn-sm">글쓰기</a>
				<?php } ?>
			</div>
			<div class="col-xs-9 col-sm-4">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="검색어 입력" name="keyword" value="<?=$keyword_?>">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">검색</button>
					</span>
				</div>
			</div>
		</div>
	</div>

	<?php
});
	
add_action('pb_board_library', function(){ //global
	wp_enqueue_style("pb-board-gallery-common", (pb_library_url() . 'css/boards/gallery.css'));
	wp_enqueue_script("pb-board-gallery-common", (pb_library_url() . 'js/boards/gallery/common.js'), array("pb-all-main"));
});

add_action('pb_board_library_list', function(){
	wp_enqueue_script("pb-board-gallery-list", (pb_library_url() . 'js/boards/gallery/list.js'), array('pb-board-gallery-common'));
});

add_action('pb_board_library_view', function(){
	wp_enqueue_script("pb-board-gallery-view", (pb_library_url() . 'js/boards/default/view.js'), array('pb-board-gallery-common'));
});

add_action('pb_board_library_write', function(){
	wp_enqueue_script("pb-board-default-write", (pb_library_url() . 'js/boards/default/write.js'), array('pb-board-gallery-common'));
	wp_enqueue_script("pb-board-gallery-write", (pb_library_url() . 'js/boards/gallery/write.js'), array('pb-board-default-write'));
});

add_action('pb_board_library_update', function(){
	do_action('pb_board_library_write');
});
add_action('pb_board_library_update', function(){
	do_action('pb_board_library_write');
});

add_action('pb_board_library_password', function(){
	wp_enqueue_script("pb-board-gallery-password", (pb_library_url() . 'js/boards/default/password.js'), array('pb-board-gallery-common'));
});

add_action('pb_board_library_password_for_update', function(){
	wp_enqueue_script("pb-board-gallery-password-for-update", (pb_library_url() . 'js/boards/default/update-password.js'), array('pb-board-gallery-common'));
});

?>