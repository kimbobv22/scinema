# 작은영화관본사 테마
작은영화관 본사를 위한 워드프레스 서버테마입니다. 이 테마는 상업용 테마이며, 작은영화관 외 허가없이 개인,단체에서 사용,수정을 금합니다.

##필수플러그인
###PB - git clone https://kimbobv22@bitbucket.org/kimbobv22/pb.git
###PBSCCommon - git clone https://kimbobv22@bitbucket.org/kimbobv22/pbsccommon.git

##주의
반드시 테마폴더명을 scinema 로 설정해야합니다.

##개발전 환경설정
Git clone 후 반드시 NPM으로 Grunt 를 설치해야 합니다.  

    > npm init
    > npm install --save-dev
    
##Grunt 빌드
js,css,less 등 리소스파일이 변경될 경우 반드시 **Grunt**를 수행해야 합니다.

    > grunt
    
##Grunt 자동화
js,css,less 등 리소스파일이 변경될 경우 자동으로 빌드할 수 있습니다.

    > grunt watch
    
    
##개발정보
개발사 : 폴앤브로  
개발사 홈페이지 : [pnbro.com](http://pnbro.com)
*개발모듈(PHP 소스원본) 및 라이브러리(*.js)에 대한 라이센스는 폴앤브로(제이그룹)이 소유하고 있으며 계약되지 않은 개인,단체,기업 이 사용할 수 없습니다. 또한 어떠한 사유에서도 해당 모듈의 판매 및 수익활동을 금합니다. 2017.06.27