<?php
	$skins_options_ = pb_skins_options();
	$part_options_ = pb_skins_part_options('footer');

?>
<div class="container">
	

	<div class="row">
		<div class="col-sm-9 col-xs-12">
			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['address']?></div>
				<div class="company-info"><?=$part_options_['contact1']?></div>
			</div>
			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['manager']?></div>
				<div class="company-info"><?=$part_options_['bizinfo']?></div>
			</div>

			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['headoffice_address']?></div>
				<div class="company-info"><?=$part_options_['headoffice_email']?></div>
			</div>

		</div>
		<div class="col-sm-3 col-xs-12 hidden-xs">
			<img src="<?=pb_skins_footer_logo_url()?>" class="footer-logo">
		</div>
	</div>
	<DIV class="form-margin-lg"></DIV>
	<div class="row">
		
		
		<div class="col-sm-6 col-xs-12 col-sm-push-6">
			<ul class="footer-menu-list">
				<li><a href="<?=pb_email_policy_url()?>">이메일 무단수집거부</a></li>
				<li class="menu-item"><a href="<?=pb_location_url()?>">오시는길</a></li>
				<!-- <li><a href="<?=network_site_url()?>">작은영화관 사회적협동조합</a></li> -->
				<li><a href="http://scinema.org">작은영화관 사회적협동조합</a></li>
			</ul>
			<DIV class="form-margin-lg visible-xs"></DIV>
			<div class="clearfix"></div>
		</div>
		<DIV class="col-sm-6 col-xs-12 col-sm-pull-6">

			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['copyrights']?></div>
			</div>

		</DIV>
	</div>

</div>