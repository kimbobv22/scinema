<?php
	$skins_options_ = pb_skins_options();
	$part_options_ = pb_skins_part_options('footer');



?>

<div class="top-wrap"><div class="container">
	
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<ul class="footer-menu-list">
				<li><a href="<?=pb_email_policy_url()?>">이메일 무단수집거부</a></li>
				<li class="menu-item"><a href="<?=pb_location_url()?>">오시는길</a></li>
				<!-- <li><a href="<?=network_site_url()?>">작은영화관 사회적협동조합</a></li> -->
				<li><a href="http://scinema.org">작은영화관 사회적협동조합</a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<DIV class="col-sm-6 col-xs-12">

			<div class="customer-info">
				<span class="title">Customer Service</span>
				<span class="text"><?=$part_options_['customer-concat']?></span>
				<span class="subtext"><?=$part_options_['customer-time']?></span>
			</div>

		</DIV>
	</div>
</div></div>

<div class="middle-wrap"><div class="container">
	<div class="row">
		<div class="col-sm-9 col-xs-12">
			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['address']?></div>
				<div class="company-info"><?=$part_options_['contact1']?></div>
			</div>
			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['manager']?></div>
				<div class="company-info"><?=$part_options_['bizinfo']?></div>
			</div>

			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['headoffice_address']?></div>
				<div class="company-info"><?=$part_options_['headoffice_email']?></div>
			</div>
			<div class="company-info-wrap">
				<div class="company-info"><?=$part_options_['copyrights']?></div>
			</div>

		</div>
		<div class="col-sm-3 col-xs-12 hidden-xs">
			<img src="<?=pb_skins_footer_logo_url()?>" class="footer-logo">
		</div>
	</div>
	
</div></div>

<div class="bottom-wrap"><div class="container">
	
	<?php if(strlen($part_options_['outlink1_url'])){ ?>
		<a href="<?=$part_options_['outlink1_url']?>" class="outlink" target="_blank"><img src="<?=$part_options_['outlink1_image']?>"></a>
	<?php } ?>	
	<?php if(strlen($part_options_['outlink2_url'])){ ?>
		<a href="<?=$part_options_['outlink2_url']?>" class="outlink" target="_blank"><img src="<?=$part_options_['outlink2_image']?>"></a>
	<?php } ?>
</div></div>