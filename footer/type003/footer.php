<?php
	$skins_options_ = pb_skins_options();
	$part_options_ = pb_skins_part_options('footer');

?>

<div class="container">


	<div class="footer-container-row">
	
	<div class="row">
		<div class="col-md-3  col-sm-12 col-xs-12">
			<img src="<?=pb_skins_footer_logo_url()?>" class="footer-logo" style="max-width:<?=$part_options_['logowidth']?>px;">
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12">
			
			
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="footer-item company-info-wrap">
						<h4 class="title">영화관정보</h4>
						<ul>
							<li><strong><?=$part_options_['bizname']?></strong></li>
							<li><?=$part_options_['bizinfo']?></li>
							<li><?=$part_options_['address']?></li>
							<li><?=$part_options_['copyrights']?></li>
						</ul>
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					
					<div class="footer-item site-map-wrap">
						<h4 class="title">고객센터</h4>
						<ul>
							<li><?=$part_options_['contact1']?></li>
							<li><?=$part_options_['contact2']?></li>
							<li><?=$part_options_['contact3']?></li>
							
						</ul>
						
					</div>
				</div>
			</div>

			<ul class="footer-menu-list">
				<li><a href="<?=pb_email_policy_url()?>">이메일 무단수집거부</a></li>
				<li class="menu-item"><a href="<?=pb_location_url()?>">오시는길</a></li>
				<!-- <li><a href="<?=network_site_url()?>">작은영화관 사회적협동조합</a></li> -->
				<li><a href="http://scinema.org">작은영화관 사회적협동조합</a></li>
			</ul>
					
		</div>
	</div></div>

</div>